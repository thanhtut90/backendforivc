﻿<%@ Application Language="C#" %>
<%@ Import Namespace="StandardMobile" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>


<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);
    }
    void Application_BeginRequest(object sender, EventArgs e)
    {
        Functionality fn = new Functionality();

        string AppKey = Global.AppKey;
        string MasterScretKey = Global.MasterScretKey;
        string isProduction = Global.isProduction;
        string NotificationTitle = Global.NotificationTile;
        string EventDuration = Global.EventDuration;



        isProduction = fn.GetDataByCommand("Select config_isProduction from tb_configuration", "config_isProduction");
        Global.isProduction = isProduction;
        if(isProduction == "true")
        {
            AppKey = fn.GetDataByCommand("Select config_product_appkey from tb_configuration", "config_product_appkey");
            Global.AppKey = AppKey;
            MasterScretKey = fn.GetDataByCommand("Select config_product_mastersecretkey from tb_configuration", "config_product_mastersecretkey");
            Global.MasterScretKey = MasterScretKey;
        }
        else
        {
            AppKey = fn.GetDataByCommand("Select config_dev_appkey from tb_configuration", "config_dev_appkey");
            Global.AppKey = AppKey;
            MasterScretKey = fn.GetDataByCommand("Select config_dev_mastersecretkey from tb_configuration", "config_dev_mastersecretkey");
            Global.MasterScretKey = MasterScretKey;
        }

        NotificationTitle = fn.GetDataByCommand("Select config_name from tb_configuration", "config_name");
        Global.NotificationTile = NotificationTitle;

        var sd = fn.GetDataByCommand("Select config_startdate from WesiteConfiguration", "config_startdate");
        var ed=fn.GetDataByCommand("Select config_closingdate from WesiteConfiguration", "config_closingdate");
        EventDuration = sd + " - " + ed;
        Global.EventDuration = EventDuration;
    }
    public static void RegisterRoutes(System.Web.Routing.RouteCollection routes)
    {

        routes.MapPageRoute("", "apiv1/login", "~/WebService/login.aspx");
        routes.MapPageRoute("", "apiv1/register", "~/WebService/register.aspx");
        routes.MapPageRoute("", "apiv1/forgotPassword", "~/WebService/forgotPassword.aspx");
        routes.MapPageRoute("", "apiv1/EditProfile", "~/WebService/EditProfile.aspx");
        routes.MapPageRoute("", "apiv1/VerifyPhone", "~/WebService/verifyPhone.aspx");
        routes.MapPageRoute("", "apiv1/ClinicList", "~/WebService/ClinicList.aspx");
        routes.MapPageRoute("", "apiv1/DoctorList", "~/WebService/DoctorList.aspx");
        routes.MapPageRoute("", "apiv1/SlotList", "~/WebService/slotList.aspx");
        routes.MapPageRoute("", "apiv1/AppointSubmit", "~/WebService/appointSubmit.aspx");
        routes.MapPageRoute("", "apiv1/SlotList", "~/WebService/slotList.aspx");
        routes.MapPageRoute("", "apiv1/AppointmentStatusEdit", "~/WebService/appointmentStatusEdit.aspx");
        routes.MapPageRoute("", "apiv1/AppointmentList", "~/WebService/appointmentList.aspx");
        routes.MapPageRoute("", "apiv1/RewardPointHistoryList", "~/WebService/rewardpointHistory.aspx");
    }

</script>
