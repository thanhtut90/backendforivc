﻿<%@ WebHandler Language="C#" Class="msgshow" %>

using System.Data;
using System.Web;
using System.Xml;
using App.DAL;
public class msgshow : IHttpHandler
{
    DAL_Data daldata = new DAL_Data();
    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["meetid"] != null)
        {

            string meetid = context.Request.QueryString["meetid"].ToString();
            
            DataTable dt = new DataTable("message");
            dt.Columns.Add(new DataColumn("QAID", typeof(string)));
            dt.Columns.Add(new DataColumn("SessionID", typeof(string)));
            dt.Columns.Add(new DataColumn("QATitle", typeof(string)));
            dt.Columns.Add(new DataColumn("QANum", typeof(string)));
            var lst = daldata.selectLQABYSIDStatusType(meetid, "1", "1");
            if (lst.Rows.Count > 0)
            {
                foreach (DataRow drlst in lst.Rows)
                {
                    DataRow dr = dt.NewRow();
                    dr["QAID"] = drlst["LQAID"];
                    dr["SessionID"] = drlst["SessionID"];
                    dr["QATitle"] = drlst["LQATitle"];
                    dr["QANum"] = drlst["LQANum"];

                    dt.Rows.Add(dr);

                }
            }




            if (dt != null)
            {
                XmlTextWriter xml = new XmlTextWriter(context.Response.OutputStream, context.Response.ContentEncoding);
                dt.WriteXml(xml);
                xml.Flush();
                context.Response.ContentType = "text/xml";
                context.Response.End();
            }
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}