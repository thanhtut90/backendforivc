﻿<%@ WebHandler Language="C#" Class="msgadmin" %>
using System.Data;
using System.Web;
using System.Xml;
using App.DAL;

public class msgadmin : IHttpHandler
{
     DAL_Data daldata = new DAL_Data();
    public void ProcessRequest (HttpContext context)
    {
        if (context.Request.QueryString["meetid"] != null )
        {
            DataTable dt = new DataTable("message");
            string meetid = context.Request.QueryString["meetid"].ToString();
            var lst = daldata.selectLQABYSIDStatus(meetid, "0");
            dt.Columns.Add(new DataColumn("QAID", typeof(string)));
            dt.Columns.Add(new DataColumn("SessionID", typeof(string)));
            dt.Columns.Add(new DataColumn("QATitle", typeof(string)));
            dt.Columns.Add(new DataColumn("QANum", typeof(string)));
            dt.Columns.Add(new DataColumn("QAStatus", typeof(string)));
            dt.Columns.Add(new DataColumn("StatusType", typeof(string)));
            dt.Columns.Add(new DataColumn("QAType", typeof(string)));
             if (lst.Rows.Count > 0)
            {
                foreach (DataRow drlst in lst.Rows)
                {
                    DataRow dr = dt.NewRow();
                    dr["QAID"] = drlst["LQAID"];
                    dr["SessionID"] = drlst["UserReference"];
                    dr["QATitle"] = drlst["LQATitle"];
                    dr["QANum"] = drlst["LQANum"];
                    string QAStatus;
                    string Status;
                    if (drlst["LQAID"].ToString() == "1")
                    {
                        QAStatus = "<img src='../sys_Images/approve2.png' width='249' height='66' />";
                        Status = "1";
                    }
                    else if (drlst["LQAID"].ToString()== "2")
                    {
                        QAStatus = "<img src='../sys_Images/refuse.png' width='249' height='66' />";
                        Status = "2";
                    }
                    else
                    {
                        QAStatus = "<img src='../sys_Images/pending.png' width='249' height='66' />";
                        Status = "0";
                    }
                    dr["QAStatus"] = QAStatus;
                    dr["StatusType"] = Status;
                    dr["QAType"] = drlst["QAType"];
                    dt.Rows.Add(dr);

                }
            }

            if (dt != null)
            {
                XmlTextWriter xml = new XmlTextWriter(context.Response.OutputStream, context.Response.ContentEncoding);
                dt.WriteXml(xml);
                xml.Flush();
                context.Response.ContentType = "text/xml";
                context.Response.End();
            }
        }

    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}