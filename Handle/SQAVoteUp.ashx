﻿<%@ WebHandler Language="C#" Class="SQAVoteUp" %>


using System;
using System.Web;
using System.Data;
using System.Text;
using App.DAL;
using App.Common;
public class SQAVoteUp : IHttpHandler {

    DAL_Data daldata = new DAL_Data();
    public void ProcessRequest(HttpContext context)
    {
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.Expires = 0;
        HttpContext.Current.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
        HttpContext.Current.Response.AddHeader("pragma", "no-cache");
        HttpContext.Current.Response.AddHeader("cache-control", "private");
        HttpContext.Current.Response.CacheControl = "no-cache";
        context.Response.ContentType = "text/plain";
        if (context.Request.Form["QAID"] != null && context.Request.Form["PType"] != null)
        {

            string PType = context.Request.Form["PType"].ToString();
            string QAID = context.Request.Form["QAID"].ToString();
            if (PType.ToString() == "3")
            {
                    daldata.UpdateLQAType(QAID,"1");

            }
            else if (PType.ToString() == "4")
            {
                    daldata.UpdateLQAType(QAID,"0");
            }
            else
            {
                daldata.UpdateLQAStatus(QAID,PType);
            }

            context.Response.Write("2");

            //DataTable lst = daldata.SelectLiveQAByID(QAID);
            //if (lst.Rows.Count>0)
            //{
            //    StringBuilder str = new StringBuilder();
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add("LQAID");
            //    dt.Columns.Add("SessionID");
            //    dt.Columns.Add("LQATitle");
            //    dt.Columns.Add("LQAStatus");
            //    dt.Columns.Add("LQANum");
            //    dt.Columns.Add("dtime");
            //    dt.Columns.Add("QAType");
            //    dt.Columns.Add("UserReference");
            //    DataRow dr = dt.NewRow();
            //    dr["LQAID"] = lst.Rows[0]["LQAID"];
            //    dr["SessionID"]=lst.Rows[0]["SessionID"];
            //    dr["LQATitle"]=lst.Rows[0]["LQATitle"];              
            //    dr["UserReference"]=lst.Rows[0]["UserReference"];
            //}

        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}