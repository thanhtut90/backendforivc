﻿<%@ WebHandler Language="C#" Class="msg" %>
using System.Data;
using System.Web;
using System.Xml;
using App.DAL;
public class msg : IHttpHandler {

    public void ProcessRequest (HttpContext context)
    {
        DAL_Data daldata = new DAL_Data();
        if (context.Request.QueryString["meetid"] != null  && context.Request.QueryString["Uid"] != null )
        {
            DataTable dt = new DataTable("message");
            dt.Columns.Add(new DataColumn("QAID", typeof(string)));
            dt.Columns.Add(new DataColumn("SessionID", typeof(string)));
            dt.Columns.Add(new DataColumn("QATitle", typeof(string)));
            dt.Columns.Add(new DataColumn("QANum", typeof(string)));
            dt.Columns.Add(new DataColumn("Likes", typeof(string)));
            string meetid = context.Request.QueryString["meetid"].ToString();
            string Uid = context.Request.QueryString["Uid"].ToString();

            DataTable dtlst = daldata.selectLQABYSIDStatus(meetid, "1");
            if (dtlst.Rows.Count > 0)
            {              
                foreach(DataRow drlst in dtlst.Rows)
                {
                    DataRow dr = dt.NewRow();
                    dr["QAID"] = drlst["LQAID"];
                    dr["SessionID"] = drlst["SessionID"];
                    dr["QATitle"] = drlst["LQATitle"];
                    dr["QANum"] = drlst["LQANum"];

                    var dtv = daldata.SelectLQAVoteByQAIDUF(Uid, drlst["LQAID"].ToString());
                    if (dtv.Rows.Count > 0)//if (dtv != null)
                    {
                        dr["Likes"] = "1";
                    }
                    else
                    {
                        dr["Likes"] = "0";

                    }
                    dt.Rows.Add(dr);
                }
            }

            if (dt != null)
            {
                XmlTextWriter xml = new XmlTextWriter(context.Response.OutputStream, context.Response.ContentEncoding);
                dt.WriteXml(xml);
                xml.Flush();
                context.Response.ContentType = "text/xml";
                context.Response.End();
            }
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}