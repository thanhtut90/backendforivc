﻿<%@ WebHandler Language="C#" Class="SQAVoteInsert" %>

using System;
using System.Web;
using System.Data;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;

public class SQAVoteInsert : IHttpHandler {
    Functionality dl = new Functionality();
    public void ProcessRequest(HttpContext context)
    {
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.Expires = 0;
        HttpContext.Current.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1);
        HttpContext.Current.Response.AddHeader("pragma", "no-cache");
        HttpContext.Current.Response.AddHeader("cache-control", "private");
        HttpContext.Current.Response.CacheControl = "no-cache";
        context.Response.ContentType = "text/plain";
        if (context.Request.Form["QAID"] != null && context.Request.Form["Uid"] != null )
        {

            string Uid = context.Request.Form["Uid"].ToString();
            string QAID = context.Request.Form["QAID"].ToString();            

            string sql = "SELECT * from LQAVote where UserReference='" + Uid + "' and LQAID='" + QAID+"'";
            DataTable dtv = dl.GetDatasetByCommand(sql,"sdt").Tables[0];
            if (dtv.Rows.Count > 0)
            {
                context.Response.Write("1");
            }
            else
            {
                StringBuilder str = new StringBuilder();
                str.Append("update LQA set LQANum=LQANum+1 where LQAID='" + QAID+ "'");
                dl.ExecuteSQL(str.ToString());
                //LQAV
                GenKey gk = new GenKey();
                string fkey = string.Empty; string fid = string.Empty;
                fkey = gk.getKey("LQAV");
                fid = "LQAV" + fkey;
                StringBuilder str1 = new StringBuilder();
                str1.Append("insert into LQAVote (UserReference,LQAID)");
                str1.Append(" values ");
                str1.Append("(@UserReference,@LQAID)");
                SqlParameter[] parms1 ={
                                       new SqlParameter("@UserReference",SqlDbType.NVarChar),
                                       new SqlParameter("@LQAID",SqlDbType.NVarChar),
                                       new SqlParameter("@LQAVoteID",SqlDbType.NVarChar)
                                  };

                parms1[0].Value = Uid;
                parms1[1].Value = QAID;
                parms1[2].Value = fid;
                int addcount = dl.ExecuteNonQuery(str1.ToString(), parms1);
                gk.SaveKey("LQAV", int.Parse(fkey));
                context.Response.Write("2");
            }

        }

    }




    public bool IsReusable {
        get {
            return false;
        }
    }

}