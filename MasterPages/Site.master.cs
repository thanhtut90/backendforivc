﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using System.Data;

public partial class SiteMaster : MasterPage
{
    private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
    private string _antiXsrfTokenValue;
    protected string profileimg;
    DAL_Config dalconfig = new DAL_Config();
    DAL_Data daldata = new DAL_Data();
    protected void Page_Init(object sender, EventArgs e)
    {
        // The code below helps to protect against XSRF attacks
        var requestCookie = Request.Cookies[AntiXsrfTokenKey];
        Guid requestCookieGuidValue;
        if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
        {
            // Use the Anti-XSRF token from the cookie
            _antiXsrfTokenValue = requestCookie.Value;
            Page.ViewStateUserKey = _antiXsrfTokenValue;
        }
        else
        {
            // Generate a new Anti-XSRF token and save to the cookie
            _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
            Page.ViewStateUserKey = _antiXsrfTokenValue;

            var responseCookie = new HttpCookie(AntiXsrfTokenKey)
            {
                HttpOnly = true,
                Value = _antiXsrfTokenValue
            };
            if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
            {
                responseCookie.Secure = true;
            }
            Response.Cookies.Set(responseCookie);
        }

        Page.PreLoad += master_Page_PreLoad;
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Set Anti-XSRF token
            ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
            ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
        }
        else
        {
            // Validate the Anti-XSRF token
            if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
            {
                throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
            }
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["id"] != null && Session["name"] != null && Session["role"] != null && Session["img"] != null)
        {
            string id = Session["id"].ToString();
            string name = Session["name"].ToString();
            string role= Session["role"].ToString();
            lblusername1.Text = name;
            lblusername2.Text = name;
            lblusername3.Text = name;
            profileimg = Session["img"].ToString();
            GetSurveyConf();
            GetDataForms();
            GetRefForms();
            if (role =="1")
            {
                #region Super Administrator
                //liacc.Visible = true;
                limobconfig.Visible = true;
                liwsconfig.Visible = false;
                lirefform.Visible = true;
                lidynform.Visible = true;
                lipolling.Visible = true;
                liqa.Visible = true;
                lisurvey.Visible = true;
                lidynapi.Visible = true;
                libkconfig.Visible = true;

                //liexhibition.Visible = false;
                //lisponsors.Visible = false;
                //ligeneralinfo.Visible = false;
                

                #endregion
            }
            else if(role =="2")
            {
                #region CORPIT Administrator
                //liacc.Visible = true;
                limobconfig.Visible = true;
                liwsconfig.Visible = false;
                lirefform.Visible = true;
                lidynform.Visible = true;
                lipolling.Visible = true;
                liqa.Visible = true;
                lisurvey.Visible = true;
                lidynapi.Visible = true;
                libkconfig.Visible = false;
                //liexhibition.Visible = false;
                //lisponsors.Visible = false;
                //ligeneralinfo.Visible = false;


                #endregion
            }
            else if (role == "3")
            {
                #region WEBSITE Administrator
                //liacc.Visible = false;
                limobconfig.Visible = false;
                liwsconfig.Visible = false;
                lirefform.Visible = true;
                lidynform.Visible = true;
                lipolling.Visible = true;
                liqa.Visible = true;
                lisurvey.Visible = true;
                lidynapi.Visible = false;
                libkconfig.Visible = false;
                //liexhibition.Visible = false;
                //lisponsors.Visible = false;
                //ligeneralinfo.Visible = false;

                #endregion
            }
            else if(role=="4")
            {
                #region Organizer
                //liexhibition.Visible = true;
                //lisponsors.Visible = true;
                //ligeneralinfo.Visible = true;
                lipolling.Visible = true;
                liqa.Visible = true;
                lisurvey.Visible = true;
                liorg.Visible = false;

                lidashboard.Visible = false;                
                limobconfig.Visible = false;
                liwsconfig.Visible = false;
                lirefform.Visible = false;
                lidynform.Visible = false;               
                lidynapi.Visible = false;
                libkconfig.Visible = false;
                
                #endregion
            }

        }
        else
        {
            Response.Redirect("../Login.aspx");
        }
    }

    protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
    {
        Context.GetOwinContext().Authentication.SignOut();
    }
    protected void GetRefForms()
    {
        try
        {
            DataTable dt = daldata.GetRefForms();
            rpRef.DataSource = dt;
            rpRef.DataBind();
        }
        catch(Exception ex) { }
    }
    protected void GetDataForms()
    {
        try
        {
            DataTable dt = daldata.GetDataForms();
            rpData.DataSource = dt;
            rpData.DataBind();
        }
        catch (Exception ex) { }
    }
    //rpsurveyconf
    protected void GetSurveyConf()
    {
        try
        {
            DataTable dt = daldata.getConferenceForSurvey();
            rpsurveyconf.DataSource = dt;
            rpsurveyconf.DataBind();
        }
        catch (Exception ex) { }
    }
}