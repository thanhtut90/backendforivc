﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="MobileMenuConfiguration.aspx.cs" Inherits="Admin_MenuConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Default">
        <TargetControls>
            <telerik:TargetControl ControlID="txthtmlstr" Skin="Metro" />
            <telerik:TargetControl ControlID="txthtmlstr2" Skin="Metro" />
        </TargetControls>
    </telerik:RadSkinManager>
     <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Menu Configuration Form</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                  <div class="form-group">                      
                      <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                      </div>
                    </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Name (En)</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtnameen" class="form-control" placeholder="Enter Name" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Name (Cn)</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtnamecn" class="form-control" placeholder="Enter Name" runat="server"></asp:TextBox>
                    </div>
                </div>                                      
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Order Number</label>
                    <div class="col-sm-6">
                         <asp:TextBox ID="lblmorder" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Is Child</label>
                    <div class="col-sm-6">
                        <asp:CheckBox ID="chkisChild" runat="server" OnCheckedChanged="chkisChild_CheckedChanged" Text="&nbsp;&nbsp;Is Child" AutoPostBack="true"/>
                    </div>
                </div> 
                  <div class="form-group" id="pmenu" runat="server" visible="false">
                      <label for="inputEmail3" class="col-sm-2 control-label">Parent Menu</label>
                      <div class="col-sm-6">
                          <asp:DropDownList ID="ddlparent" CssClass="form-control" OnSelectedIndexChanged="ddlparent_SelectedIndexChanged" runat="server"></asp:DropDownList>
                      </div>
                   </div>
                <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Page Type</label>
                      <div class="col-sm-6">
                          <label style="font-style:italic;color:grey">If there is no linkage with any of this page type, please select "Blank".</label>
                          <asp:DropDownList ID="ddlpagetype" CssClass="form-control" runat="server">
                          <asp:ListItem Value="">Choose Page Type</asp:ListItem>
                          </asp:DropDownList>
                      </div>
                   </div>      
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">If Page type is Web Link, Please provide a link here</label>
                    <div class="col-sm-6">
                         <asp:TextBox ID="txtweblink" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>                              
                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Menu Type</label>
                      <div class="col-sm-6">
                          <asp:CheckBoxList ID="chkmenutype" runat="server">
                              <asp:ListItem>Quick Menu</asp:ListItem>
                              <asp:ListItem>Side Menu</asp:ListItem>
                          </asp:CheckBoxList>                          
                      </div>
                   </div>  
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Menu Icon</label>
                      <div class="col-sm-6">                       
                        <asp:Label ID="lblmicon" class="form-control" CssClass="form-control" runat="server"></asp:Label>
                       <asp:FileUpload ID="fupicon" runat="server" />
                      </div>
                    </div>   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Accessed By</label>
                      <div class="col-sm-6">
                          <asp:CheckBoxList ID="chkpermission" runat="server">
                              <asp:ListItem>VIP</asp:ListItem>
                              <asp:ListItem>Exhibitor</asp:ListItem>
                              <asp:ListItem>Delegate</asp:ListItem>
                              <asp:ListItem>Visitor</asp:ListItem>
                              <asp:ListItem>Guest</asp:ListItem>
                          </asp:CheckBoxList>
                      </div>
                   </div>               
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">HTML String (Default Lang)</label>
                      <div class="col-sm-6">                       
                        <telerik:RadEditor runat="server" EditModes="Design" ID="txthtmlstr" ></telerik:RadEditor>
                      </div>
                    </div>
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">HTML String (Other Lang)</label>
                      <div class="col-sm-6">                       
                        <telerik:RadEditor runat="server" EditModes="Design" ID="txthtmlstr2" ></telerik:RadEditor>
                      </div>
                    </div>
                   <div class="box-footer">                                
                        <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        &nbsp;&nbsp;
                        <asp:Button ID="btnDelete" OnClick="btnDelete_Click" class="btn btn-danger" runat="server" Text="Delete" Visible="false" />
                  </div>
                  <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                        <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="true" PageSize="20">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="m_id" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Editspp" CommandArgument='<%#Eval("m_id") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("m_id") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>                     
                                </telerik:GridTemplateColumn> 
                                <telerik:GridBoundColumn  DataField="m_TitleEn" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Menu Title" SortExpression="m_TitleEn" UniqueName="m_TitleEn" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>    
                                <telerik:GridBoundColumn  DataField="m_OrderID" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Menu Sequence" SortExpression="m_OrderID" UniqueName="m_OrderID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>  
                                 <telerik:GridBoundColumn  DataField="m_PageType" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Page Type" SortExpression="m_PageType" UniqueName="m_PageType" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                                                                            
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="View Child Menu">
                                    <ItemTemplate>                                        
                                            <%# GetChildMenu(Eval("m_id").ToString()) %>
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>      
                                <telerik:GridBoundColumn  DataField="m_menuType" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Menu Type" SortExpression="m_menuType" UniqueName="m_menuType" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>    
                                 <telerik:GridBoundColumn  DataField="m_weblink" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Web link" SortExpression="m_weblink" UniqueName="m_weblink" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                         
                                <telerik:GridBoundColumn  DataField="m_htmlString" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Brief" SortExpression="m_htmlString" UniqueName="m_htmlString" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                                                  
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    </div>              
                </div>
                  </div>
              <!-- /.box-body -->
             
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>

