﻿using System;
using System.Data;
using System.Linq;
using App.DAL;
using App.Object;
using App.Common;

public partial class Admin_SystemConfiguration : System.Web.UI.Page
{
    DAL dal = new DAL();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSysConfigData();
        }
    }

    private void BindSysConfigData()
    {
        try
        {
            WesiteConfiguration wc = dalf.selectAllWebsiteConfig();
            if (wc !=null)
            {
                txtfullname.Text = wc.config_projname;
                txtprefix.Text = wc.config_prefixname;
                txtclosingmessage.Text = wc.config_closingmessage;
               
                txtsmtpclient.Text = wc.config_smtpclient;
                txtsmtpusername.Text = wc.config_smtpusername;
                txtsmtppassword.Text = wc.config_smtppassword;
             
                lblbanner.Text = wc.config_banner;
                lblicon.Text = wc.config_siteicon;
                rdoshowbanner.SelectedValue = wc.config_isShowBanner.ToString();                
                txtcopyright.Text = wc.config_copyright;
                rdolayout.SelectedValue = wc.config_menulayout;


                string str = wc.config_startdate + " - " + wc.config_closingdate;
                if (!string.IsNullOrEmpty(str))
                {
                    duration.Visible = false;
                    daterange.Text = str;
                    daterange.Visible = true;
                    lnkreset.Visible = true;
                }
               
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void lnkreset_Click(object sender, EventArgs e)
    {
        daterange.Visible = false;
        duration.Visible = true;
        lnkreset.Visible = false;
        daterange.Text = "";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string startdate = string.Empty; string enddate = string.Empty; string value = string.Empty; string sd = string.Empty; string ed = string.Empty;
            string Fvalue = this.Request.Form.Get("n_test");
            string lblval = daterange.Text.Trim();
            if (!string.IsNullOrEmpty(Fvalue))
            {
                value = Fvalue;
            }
            else
            {
                value = lblval;
            }

         
            if (!string.IsNullOrEmpty(value))
            {
                string[] values = value.Split('-');
                string[] startdatetime = values[0].Split(' ');                
                string[] arrstartdate = startdatetime[0].Split('/');
                startdate = arrstartdate[1] + "/" + arrstartdate[0] + "/" + arrstartdate[2];
                string starttime = startdatetime[1] + startdatetime[2];

                string[] enddatetime = values[1].Split(' ');                
                string[] arrenddate = enddatetime[1].Split('/');
                string endtime = enddatetime[2] + enddatetime[3];
                enddate=arrenddate[1] + "/" + arrenddate[0] + "/" + arrenddate[2];

                string fullname = txtfullname.Text.Trim();
                string prefix = txtprefix.Text.Trim();
               
                string smtpclient = txtsmtpclient.Text.Trim();
                string smtpusername = txtsmtpusername.Text.Trim();
                string smtppassword = txtsmtppassword.Text.Trim();
              
                string copyright= txtcopyright.Text.Trim();
                string isshowbanner = rdoshowbanner.SelectedValue;
                string layout = rdolayout.SelectedValue;
                string closingmessage = txtclosingmessage.Text.Trim();                
                string ipaddress = txtip.Text.Trim();
                string domain = txtdomain.Text.Trim();

                string appIcon = string.Empty, banner = string.Empty;
                if (fupicon.HasFile == false && !String.IsNullOrEmpty(lblicon.Text))
                {
                    appIcon = lblicon.Text;
                }
                else if (fupicon.HasFile == true && String.IsNullOrEmpty(lblicon.Text))
                {
                    appIcon = UploadIcon("SiteIcon");
                }
                if (fupbanner.HasFile == false && !String.IsNullOrEmpty(lblbanner.Text))
                {
                    banner = lblbanner.Text;
                }
                else if (fupicon.HasFile == true && String.IsNullOrEmpty(lblbanner.Text))
                {
                    banner = UploadBanner("Banner");
                }
                
                int res = 0;
                int ID = dalf.CheckExistWS(fullname, prefix);
                WesiteConfiguration WCobj = new WesiteConfiguration();
                if (ID == 0)
                {
                    #region New
                    
                    WCobj.config_projname = fullname;
                    WCobj.config_prefixname = prefix;
                    WCobj.config_startdate = Convert.ToDateTime(startdate);
                    WCobj.config_closingdate = Convert.ToDateTime(enddate);
                    WCobj.config_closingmessage = closingmessage;

                    WCobj.config_smtpclient = smtpclient;
                    WCobj.config_smtpusername = smtpusername;
                    WCobj.config_smtppassword = smtppassword;

                    WCobj.config_banner = banner;
                    WCobj.config_siteicon = appIcon;
                    WCobj.config_isShowBanner =isshowbanner=="1"? true :false;
                    WCobj.config_copyright = copyright;
                    WCobj.config_menulayout = rdolayout.SelectedValue;
                    WCobj.config_deleteflag = false;
                    WCobj.config_createddate = DateTime.Now;
                    res = dalf.insertWebsiteConfigData(WCobj);
                    #endregion
                }
                else
                {
                    #region Update
                    WCobj.config_id = ID;
                    WCobj.config_projname = fullname;
                    WCobj.config_prefixname = prefix;
                    WCobj.config_startdate = Convert.ToDateTime(startdate);
                    WCobj.config_closingdate = Convert.ToDateTime(enddate);
                    WCobj.config_closingmessage = closingmessage;

                    WCobj.config_smtpclient = smtpclient;
                    WCobj.config_smtpusername = smtpusername;
                    WCobj.config_smtppassword = smtppassword;

                    WCobj.config_banner = banner;
                    WCobj.config_siteicon = appIcon;
                    WCobj.config_isShowBanner = isshowbanner == "1" ? true : false;
                    WCobj.config_copyright = copyright;
                    WCobj.config_menulayout = rdolayout.SelectedValue;
                    WCobj.config_deleteflag = false;
                    WCobj.config_createddate = DateTime.Now;
                    res = dalf.UpdateWebsiteConfigData(WCobj);
                    #endregion
                }
                

                if (res == 100)
                {
                    //Duplicate
                    MessageShow("Duplicate Data", "Warning");
                }
                else if (res != 0 && res != 100)
                {
                    //success                                        
                    DAL_Data daldata = new DAL_Data();
                    string tablename = "tb_Configuration";
                    daldata.UpdateDataVersion(tablename);

                    MessageShow("Success", "Success");
                    BindSysConfigData();
                }
                else
                {
                    //fail
                    MessageShow("Fail", "Warning");
                }
            }


        }
        catch (Exception ex) { MessageShow(ex.ToString(), "Warning"); }
    }

    private string UploadBanner(string fname)
    {
        string imagefilename = string.Empty;
        try
        {
            string ext = System.IO.Path.GetExtension(fupbanner.FileName).ToString().ToLower();
            if (ext != ".png" && ext != ".jpg" && ext != ".gif" && ext != ".jpeg")
            {
                msg.Text = "Invalid Image Type";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            }
            else
            {
                noti.Visible = false;
            }

            string filename = fname + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + ext;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\downimg").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\downimg\\") + filename);
            fupbanner.SaveAs(savePath);
            imagefilename = filename;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private string UploadIcon(string fname)
    {
        string imagefilename = string.Empty;
        try
        {
            string ext = System.IO.Path.GetExtension(fupicon.FileName).ToString().ToLower();
            if (ext != ".png" && ext != ".jpg" && ext != ".gif" && ext != ".jpeg")
            {
                msg.Text = "Invalid Image Type";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            }
            else
            {
                noti.Visible = false;
            }

            string filename = fname + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + ext;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\downimg").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\downimg\\") + filename);
            fupicon.SaveAs(savePath);
            imagefilename = filename;
        }
        catch (Exception ex) { }
        return imagefilename;
    }

    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }
}