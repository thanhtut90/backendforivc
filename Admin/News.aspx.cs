﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_News : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblNews";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdNews.ExportSettings.IgnorePaging = true;
                grdNews.ExportSettings.ExportOnlyData = true;
                grdNews.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdNews.ExportSettings.FileName = "News";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtmenu = daldata.selectNewsMenu();

            ddlmenu.DataSource = dtmenu;
            ddlmenu.DataTextField = "m_TitleEn";
            ddlmenu.DataValueField = "m_id";
            ddlmenu.DataBind();
            ListItem lst = new ListItem("Select Menu", "");
            ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("n_ID");
                dt.Columns.Add("n_title");
                dt.Columns.Add("n_content");
                dt.Columns.Add("n_picture");
                dt.Columns.Add("menuID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["n_ID"] = hfid.Value.ToString();
                    dr["n_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["n_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["n_picture"] = imagename == "" ? lblfloorplan.Text : imagename;
                    dr["menuID"] = ddlmenu.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateNews(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdNews_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["n_ID"] = "";
                    dr["n_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["n_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["n_picture"] = imagename == "" ? lblfloorplan.Text : imagename;
                    dr["menuID"] = ddlmenu.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveNews(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdNews_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtTitle.Text.Trim()))
            {
                lblerrNewsTitle.Visible = true;
                rtn = false;
            }else
            {
                lblerrNewsTitle.Visible = false;
            }
            if (ddlmenu.SelectedValue=="")
            {
                lblerrdll.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdll.Visible = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdNews.ExportSettings.ExportOnlyData = true;
        this.grdNews.ExportSettings.IgnorePaging = true;
        this.grdNews.ExportSettings.OpenInNewWindow = true;
        this.grdNews.MasterTableView.ExportToExcel();

        grdNews.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdNews.ExportSettings.FileName = "News";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdNews_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getNews();
            grdNews.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdNews_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdNews.CurrentPageIndex + 1;
    }
    protected void grdNews_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getNewsbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getNews();
            }


            grdNews.DataSource = dt;
            grdNews.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdNews_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdNews.ExportSettings.ExportOnlyData = true;
            this.grdNews.ExportSettings.IgnorePaging = true;
            this.grdNews.ExportSettings.OpenInNewWindow = true;
            this.grdNews.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getNewslstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["n_ID"].ToString();
                    txtTitle.Text = dt.Rows[0]["n_title"].ToString();
                    txtContent.Content = dt.Rows[0]["n_content"].ToString();
                    ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                    lblfloorplan.Text = dt.Rows[0]["n_picture"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteNewsByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdNews_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdNews_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtTitle.Text = "";
        txtContent.Content = "";
        ddlmenu.SelectedIndex = 0;
        lblfloorplan.Text = "";
    }
}