﻿using System;
using System.Data;
using System.Linq;


public partial class Admin_MobileSystemConfigaspx : System.Web.UI.Page
{
    DAL dal = new DAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindSysConfigData();
            BindLanguage();
        }
    }

    private void BindLanguage()
    {
        DataTable dt = dal.getLang();
        if(dt.Rows.Count>0)
        {
            chkLang.DataSource = dt;
            chkLang.DataValueField = "ID";
            chkLang.DataTextField = "Language";
            chkLang.DataBind();
        }
    }

    private void BindSysConfigData()
    {
        try
        {
            DataTable dt = dal.getSysConfigData();
            if (dt.Rows.Count > 0)
            {
                txtname.Text = dt.Rows[0]["config_name"].ToString();
                chkLang.SelectedValue = dt.Rows[0]["config_language"].ToString();
                lblappicon.Text = dt.Rows[0]["config_appIcon"].ToString();
                txtvenue.Text = dt.Rows[0]["config_venue"].ToString();
                txtvregurl.Text = dt.Rows[0]["config_visRegURL"].ToString();
                rdolayout.SelectedValue = dt.Rows[0]["config_homePageLayout"].ToString();
                txtiVersion.Text = dt.Rows[0]["config_iOSVersion"].ToString();
                txtidownloadlink.Text= dt.Rows[0]["config_iOsDownloadLink"].ToString();
                txtaVersion.Text = dt.Rows[0]["config_AndroidVersion"].ToString();
                txtadownloadlink.Text = dt.Rows[0]["config_AndroidDownloadLink"].ToString();
                txtdappkey.Text= dt.Rows[0]["config_dev_appkey"].ToString();
                txtdmastersecret.Text = dt.Rows[0]["config_dev_mastersecretkey"].ToString();
                txtpappkey.Text = dt.Rows[0]["config_product_appkey"].ToString();
                txtpmastersecret.Text = dt.Rows[0]["config_product_mastersecretkey"].ToString();              
                var accounts = dt.Rows[0]["config_enableAccount"].ToString();
                string[] arrslt = accounts.Split('/');
                if (arrslt.Count() > 0)
                {
                    for (int i = 0; i < arrslt.Count(); i++)
                    {
                        for (int j = 0; j < chkenableAccount.Items.Count; j++)
                        {
                            if (chkenableAccount.Items[j].Value == arrslt[i].ToString())
                            {
                                chkenableAccount.Items[j].Selected = true;
                            }

                        }
                    }
                }
                string str = dt.Rows[0]["config_startDate"].ToString() + " - " + dt.Rows[0]["config_endDate"].ToString();
                if (!string.IsNullOrEmpty(str))
                {
                    duration.Visible = false;
                    daterange.Text = str;
                    daterange.Visible = true;
                    lnkreset.Visible = true;
                }
                string ssurvey = dt.Rows[0]["config_surveyStartDate"].ToString();
                if (!string.IsNullOrEmpty(ssurvey))
                {
                    ssdate.Visible = false;
                    lblss.Text = ssurvey;
                    lblss.Visible = true;
                    lnkresetss.Visible = true;
                }
                string esurvey = dt.Rows[0]["config_surveyEndDate"].ToString();
                if (!string.IsNullOrEmpty(esurvey))
                {
                    sedate.Visible = false;
                    lbles.Text = esurvey;
                    lbles.Visible = true;
                    lnkresetes.Visible = true;
                }
            }
        }
        catch (Exception ex) { throw ex; }
    }

    protected void lnkreset_Click(object sender, EventArgs e)
    {
        daterange.Visible = false;
        duration.Visible = true;
        lnkreset.Visible = false;
        daterange.Text = "";
    }
    protected void lnkresetes_Click(object sender, EventArgs e)
    {
        lbles.Visible = false;
        sedate.Visible = true;
        lnkresetes.Visible = false;
        lbles.Text = "";
    }
    protected void lnkresetss_Click(object sender, EventArgs e)
    {
        lblss.Visible = false;
        ssdate.Visible = true;
        lnkresetss.Visible = false;
        lblss.Text = "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string startdate = string.Empty; string enddate = string.Empty; string value = string.Empty; string sd = string.Empty; string ed = string.Empty;
            string Fvalue = this.Request.Form.Get("n_test");
            string lblval = daterange.Text.Trim();
            if (!string.IsNullOrEmpty(Fvalue))
            {
                value = Fvalue;
            }
            else
            {
                value = lblval;
            }

            var sursd = this.Request.Form.Get("sdate");
            if (!string.IsNullOrEmpty(sursd))
            {
                sd = sursd.ToString();
            }
            else
            {
                sd = lblss.Text.Trim();
            }
            var sured = this.Request.Form.Get("edate");
            if (!string.IsNullOrEmpty(sured))
            {
                ed = sured.ToString();
            }
            else
            {
                ed = lbles.Text.Trim();
            }
            if (!string.IsNullOrEmpty(value))
            {
                string[] values = value.Split('-');
                string[] startdatetime = values[0].Split(' ');
                startdate = startdatetime[0];
                string starttime = startdatetime[1] + startdatetime[2];
                string[] enddatetime = values[1].Split(' ');
                enddate = enddatetime[1];
                string endtime = enddatetime[2] + enddatetime[3];

                string language = chkLang.SelectedValue;
                string name = txtname.Text.Trim();
                string venue = txtvenue.Text.Trim();
                string visRegUrl = txtvregurl.Text.Trim();
                string layout = rdolayout.SelectedValue;
                string iOSVersion = txtiVersion.Text.Trim();
                string iOSDownloadLink = txtidownloadlink.Text.Trim();
                string AndroidVersion = txtaVersion.Text.Trim();
                string AndroidDownloadLink = txtadownloadlink.Text.Trim();                
                string dappkey = txtdappkey.Text.Trim();
                string dmastersecret = txtdmastersecret.Text.Trim();
                string pappkey = txtpappkey.Text.Trim();
                string pmastersecret = txtpmastersecret.Text.Trim();
                string appIcon = string.Empty;
                if (fupIcon.HasFile == false && !String.IsNullOrEmpty(lblappicon.Text))
                {
                    appIcon = lblappicon.Text;
                }
                else if (fupIcon.HasFile == true && String.IsNullOrEmpty(lblappicon.Text))
                {
                    appIcon = UploadImageFile("AppIcon");
                }

                string enableAccount = string.Empty;
                for (int i = 0; i < chkenableAccount.Items.Count; i++)
                {
                    if (i != chkenableAccount.Items.Count - 1)
                    {
                        if (chkenableAccount.Items[i].Selected == true)
                        {
                            enableAccount += chkenableAccount.Items[i].Value + "/";
                        }
                    }
                    else
                    {
                        if (chkenableAccount.Items[i].Selected == true)
                        {
                            enableAccount += chkenableAccount.Items[i].Value;
                        }
                    }

                }
                int res = 0;

                res = dal.insertSysConfigData(name, language, startdate, enddate, enableAccount, appIcon, venue, sd, ed, visRegUrl, layout,iOSVersion,iOSDownloadLink,AndroidVersion,AndroidDownloadLink,dappkey,dmastersecret, pappkey, pmastersecret);

                if (res == 100)
                {
                    //Duplicate
                    MessageShow("Duplicate Data", "Warning");
                }
                else if (res == 1)
                {
                    //success                                        
                    MessageShow("Success", "Success");
                    BindSysConfigData();
                }
                else
                {
                    //fail
                    MessageShow("Fail", "Warning");
                }
            }


        }
        catch (Exception ex) { MessageShow(ex.ToString(), "Warning"); }
    }

    private string UploadImageFile(string fname)
    {
        string imagefilename = string.Empty;
        try
        {
            string ext = System.IO.Path.GetExtension(fupIcon.FileName).ToString().ToLower();
            if (ext != ".png" && ext != ".jpg" && ext != ".gif" && ext != ".jpeg")
            {
                msg.Text = "Invalid Image Type";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            }
            else
            {
                noti.Visible = false;
            }

            string filename = fname + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + ext;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\downimg").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\downimg\\") + filename);
            fupIcon.SaveAs(savePath);
            imagefilename = filename;
        }
        catch (Exception ex) { }
        return imagefilename;
    }

    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }
}