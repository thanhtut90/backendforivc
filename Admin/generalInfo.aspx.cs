﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_generalInfo : System.Web.UI.Page
{

    DAL_Data daldata = new DAL_Data();
    string tablename = "tblGeneralInfo";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdgeneral.ExportSettings.IgnorePaging = true;
                grdgeneral.ExportSettings.ExportOnlyData = true;
                grdgeneral.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdgeneral.ExportSettings.FileName = "GeneralInfo";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtmenu = daldata.selectGeneralInfoMenu();
            DataTable dtMainGeneralInfo = daldata.getmaingeneralinfo();

            ddlmenu.DataSource = dtmenu;
            ddlmenu.DataTextField = "m_TitleEn";
            ddlmenu.DataValueField = "m_id";
            ddlmenu.DataBind();
            ListItem lst1 = new ListItem("Select Menu", "");
            ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst1);

            ddlmain.DataSource = dtMainGeneralInfo;
            ddlmain.DataTextField = "gi_Title";
            ddlmain.DataValueField = "gi_ID";
            ddlmain.DataBind();
            ListItem lst2 = new ListItem("Select Main Title", "");
            ddlmain.Items.Insert(ddlmain.Items.Count - ddlmain.Items.Count, lst2);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("gi_ID");
                dt.Columns.Add("gi_title");
                dt.Columns.Add("gi_content");
                dt.Columns.Add("gi_icon");
                dt.Columns.Add("gi_seq");
                dt.Columns.Add("gi_mainID");
                dt.Columns.Add("menuID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");


                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["gi_ID"] = hfid.Value.ToString();
                    dr["gi_title"] = txttitle.Text.Trim();
                    dr["gi_content"] = txtContent.Content;
                    dr["gi_icon"] = imagename == "" ? lblicon.Text : imagename;
                    dr["gi_seq"] = txtSeq.Text.Trim();
                    dr["gi_mainID"] = ddlmain.SelectedValue;
                    dr["menuID"] = ddlmenu.SelectedValue;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    
                    int res = daldata.UpdateGeneralInfo(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdgeneral_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["gi_ID"] = "";
                    dr["gi_title"] = txttitle.Text.Trim();
                    dr["gi_content"] = txtContent.Content;
                    dr["gi_icon"] = imagename == "" ? lblicon.Text : imagename;
                    dr["gi_seq"] = txtSeq.Text.Trim();
                    dr["gi_mainID"] = ddlmain.SelectedValue;
                    dr["menuID"] = ddlmenu.SelectedValue;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveGeneralInfo(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdgeneral_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txttitle.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdgeneral.ExportSettings.ExportOnlyData = true;
        this.grdgeneral.ExportSettings.IgnorePaging = true;
        this.grdgeneral.ExportSettings.OpenInNewWindow = true;
        this.grdgeneral.MasterTableView.ExportToExcel();

        grdgeneral.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdgeneral.ExportSettings.FileName = "GeneralInfo";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdgeneral_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getGeneralInfo();
            grdgeneral.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdgeneral_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdgeneral.CurrentPageIndex + 1;
    }
    protected void grdgeneral_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getGeneralInfobyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getGeneralInfo();
            }


            grdgeneral.DataSource = dt;
            grdgeneral.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdgeneral_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdgeneral.ExportSettings.ExportOnlyData = true;
            this.grdgeneral.ExportSettings.IgnorePaging = true;
            this.grdgeneral.ExportSettings.OpenInNewWindow = true;
            this.grdgeneral.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getGeneralInfoByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["gi_ID"].ToString();
                    txttitle.Text = dt.Rows[0]["gi_title"].ToString();                    
                    txtContent.Content= dt.Rows[0]["gi_content"].ToString();
                    txtSeq.Text = dt.Rows[0]["gi_seq"].ToString();
                    ddlmain.SelectedValue = dt.Rows[0]["gi_mainID"].ToString();
                    ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                    lblicon.Text= dt.Rows[0]["gi_icon"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteGeneralInfoByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdgeneral_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdgeneral_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }

    public string getGeneralInfoTitleByID(string ID)
    {
        string rtn = string.Empty;
        if(!string.IsNullOrEmpty(ID))
            rtn = daldata.getgeneralinfiByID(ID);
        
        return rtn;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txttitle.Text = "";
        txtContent.Content = "";
        txtSeq.Text = "";
        ddlmain.SelectedIndex = 0;
        ddlmenu.SelectedIndex = 0;
        lblicon.Text = "";
    }
}