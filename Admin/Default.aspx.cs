﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void proceed_click(object sender, EventArgs e)
    {
        try
        {
            string sql1 = "select e.ec_ID,e.ec_hall,e.ec_booth,v.vb_name from tblExhibitingCompany e join tblVenue_Booth v on e.ec_booth=v.vb_ID";
            DataTable dt1 = fn.GetDatasetByCommand(sql1, "sdt").Tables[0];
            if(dt1.Rows.Count > 0)
            {
                foreach(DataRow dr in dt1.Rows)
                {
                    string ec_ID= dr["ec_ID"].ToString();
                    string vb_name = dr["vb_name"].ToString();
                    string ec_hall = dr["ec_hall"].ToString();
                    string sql2 = string.Format("select vb_ID from tblVenue_Booth where vb_name='{0}' and vb_location='{1}'", vb_name, ec_hall);
                    string vb_ID = fn.GetDataByCommand(sql2, "vb_ID");

                    string sql3 = string.Format("update tblExhibitingCompany set ec_booth='{0}' where ec_ID='{1}'", vb_ID, ec_ID);
                    fn.ExecuteSQL(sql3);
                }
            }
        }
        catch(Exception ex) { }
    }
    
}