﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Moments : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblMoments";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdMoments.ExportSettings.IgnorePaging = true;
                grdMoments.ExportSettings.ExportOnlyData = true;
                grdMoments.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdMoments.ExportSettings.FileName = "Moments";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {


            //DataTable dtName = new DataTable();

            ////Add Columns to Table
            //dtName.Columns.Add(new DataColumn("MomentsType"));
            //dtName.Columns.Add(new DataColumn("MomentsID"));

            ////Now Add Values
            //dtName.Rows.Add("Select Moments Type", "0");
            //dtName.Rows.Add("Exhibitor", "Exhibitor");
            //dtName.Rows.Add("Degelate", "Degelate");
            //dtName.Rows.Add("Visitor", "Visitor");
            //dtName.Rows.Add("VIP", "VIP");
            //dtName.Rows.Add("Organizer", "Organizer");

            ////At Last Bind datatable to dropdown.
            //ddlAcc.DataSource = dtName;
            //ddlAcc.DataTextField = dtName.Columns["MomentsType"].ToString();
            //ddlAcc.DataValueField = dtName.Columns["MomentsID"].ToString();
            //ddlAcc.DataBind();


            //DataTable dtSName = new DataTable();

            ////Add Columns to Table
            //dtSName.Columns.Add(new DataColumn("SalType"));
            //dtSName.Columns.Add(new DataColumn("SalID"));

            ////Now Add Values
            //dtSName.Rows.Add("Select Salutation", "0");
            //dtSName.Rows.Add("Sir", "Sir");
            //dtSName.Rows.Add("Mr", "Mr");
            //dtSName.Rows.Add("Ms", "Ms");
            //dtSName.Rows.Add("Mrs", "Mrs");

            ////At Last Bind datatable to dropdown.
            //ddlSal.DataSource = dtSName;
            //ddlSal.DataTextField = dtSName.Columns["SalType"].ToString();
            //ddlSal.DataValueField = dtSName.Columns["SalID"].ToString();
            //ddlSal.DataBind();

        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("m_ID");
                dt.Columns.Add("m_txtcomment");
                dt.Columns.Add("m_photocomment");
                dt.Columns.Add("m_status");
                dt.Columns.Add("m_addedtime");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["m_ID"] = hfid.Value.ToString();
                    dr["m_txtcomment"] = daldata.replaceForSQL(txtTextcomment.Text.Trim());
                    dr["m_photocomment"] = imagename == "" ? lblphotocomment.Text : imagename;
                    dr["m_status"] = rdbtnIsActivated.SelectedValue;
                    dr["m_addedtime"] = daldata.replaceForSQL(txtAddTime.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateMoments(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdMoments_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["m_ID"] = "";
                    dr["m_txtcomment"] = daldata.replaceForSQL(txtTextcomment.Text.Trim());
                    dr["m_photocomment"] = imagename == "" ? lblphotocomment.Text : imagename;
                    dr["m_status"] = rdbtnIsActivated.SelectedValue;
                    dr["m_addedtime"] = daldata.replaceForSQL(txtAddTime.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.SaveMoments(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdMoments_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtTextcomment.Text.Trim()) || string.IsNullOrWhiteSpace(txtTextcomment.Text.Trim()))
            {
                lblerrTextcomment.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrTextcomment.Visible = false;
            }
            if (string.IsNullOrEmpty(txtAddTime.Text.Trim()) || string.IsNullOrWhiteSpace(txtAddTime.Text.Trim()))
            {
                lblerrAddTime.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrAddTime.Visible = false;
            }
          

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdMoments.ExportSettings.ExportOnlyData = true;
        this.grdMoments.ExportSettings.IgnorePaging = true;
        this.grdMoments.ExportSettings.OpenInNewWindow = true;
        this.grdMoments.MasterTableView.ExportToExcel();

        grdMoments.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdMoments.ExportSettings.FileName = "Moments";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdMoments_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getMoments();
            grdMoments.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdMoments_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdMoments.CurrentPageIndex + 1;
    }
    protected void grdMoments_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getMomentsbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getMoments();
            }


            grdMoments.DataSource = dt;
            grdMoments.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdMoments_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdMoments.ExportSettings.ExportOnlyData = true;
            this.grdMoments.ExportSettings.IgnorePaging = true;
            this.grdMoments.ExportSettings.OpenInNewWindow = true;
            this.grdMoments.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getMomentslstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["m_ID"].ToString();
                    txtTextcomment.Text = dt.Rows[0]["m_txtcomment"].ToString();
                    lblphotocomment.Text = dt.Rows[0]["m_photocomment"].ToString();
                    rdbtnIsActivated.SelectedValue = dt.Rows[0]["m_status"].ToString();
                    txtAddTime.Text = dt.Rows[0]["m_addedtime"].ToString();

                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteMomentsByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdMoments_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdMoments_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }

    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtTextcomment.Text = "";
        txtAddTime.Text = "";
        lblphotocomment.Text = "";       
        rdbtnIsActivated.SelectedValue = "Approved";


    }
}