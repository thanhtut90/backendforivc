﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="EmailConfig.aspx.cs" Inherits="Admin_EmailConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Web20">
        <TargetControls>
            <telerik:TargetControl ControlID="txtemailcontent" Skin="Metro" />
        </TargetControls>
    </telerik:RadSkinManager>
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Email / Reminder Configuration Form</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                  <div class="form-group">                      
                      <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                      </div>
                    </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email Templates</label>                    
                    <div class="col-sm-6">
                    <asp:DropDownList ID="ddletemplates" CssClass="form-control" OnSelectedIndexChanged="ddletemplates_selectedindexchanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                    </div>
                </div> 
                <div class="form-group">                      
                    <div class="col-sm-12">
                        <hr class="alert-info"    />
                    </div>   
                </div>                  
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email Template Title</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtemailname" class="form-control" required="" placeholder="Enter Email Template Title" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">From Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtfromname" class="form-control" required="" placeholder="Enter From Name" runat="server"></asp:TextBox>
                    </div>
                </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">From Email</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtfromemail" class="form-control" required="" placeholder="Enter From Email" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email (CC)</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtcc" class="form-control" required="" placeholder="Enter Email (CC)" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email (BCC)</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtbcc" class="form-control" required="" placeholder="Enter Email (BCC)" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email Subject</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtemailsubject" class="form-control" required="" placeholder="Enter Email Subject" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Parameters (key:value^Seperator)</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtparameter" class="form-control" required="" placeholder="Enter Parameters (key:value^Seperator)" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Email Content</label>
                    <div class="col-sm-9">                    
                        <telerik:RadEditor runat="server" EditModes="Design" ID="txtemailcontent" Width="100%" Height="1000px"></telerik:RadEditor>
                    </div>
                </div>                  


                  </div>
              <!-- /.box-body -->
              <div class="box-footer">                                
                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                <asp:Button ID="btncancel" OnClick="btncancel_Click" class="btn btn-default" runat="server" Text="Cancel" />
              </div>
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>

