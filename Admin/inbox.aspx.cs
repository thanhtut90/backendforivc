﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_inbox : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblInbox";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdInbox.ExportSettings.IgnorePaging = true;
                grdInbox.ExportSettings.ExportOnlyData = true;
                grdInbox.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdInbox.ExportSettings.FileName = "Inbox";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("i_ID");
                dt.Columns.Add("i_title");
                dt.Columns.Add("i_content");
                dt.Columns.Add("i_date");
                dt.Columns.Add("i_time");
                dt.Columns.Add("i_isPush");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("organizer");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["i_ID"] = hfid.Value.ToString();
                    dr["i_title"] = daldata.replaceForSQL(txtInboxTitle.Text.Trim());
                    dr["i_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["i_date"] = daldata.replaceForSQL(txtDate.Text.Trim());
                    dr["i_time"] = daldata.replaceForSQL(txtTime.Text.Trim());
                    dr["i_isPush"] = hfispush.Value.ToString() == "True" ? "1" : "0";
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["organizer"] = daldata.replaceForSQL(txtOrganizerName.Text.Trim());

                    int res = daldata.UpdateInbox(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdInbox_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["i_ID"] = "";
                    dr["i_title"] = daldata.replaceForSQL(txtInboxTitle.Text.Trim());
                    dr["i_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["i_date"] = daldata.replaceForSQL(txtDate.Text.Trim());
                    dr["i_time"] = daldata.replaceForSQL(txtTime.Text.Trim());
                    dr["i_isPush"] = "0";
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["organizer"] = daldata.replaceForSQL(txtOrganizerName.Text.Trim());
                    int res = daldata.SaveInbox(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdInbox_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtInboxTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtInboxTitle.Text.Trim()))
            {
                lblerrInboxTitle.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtDate.Text.Trim()) || string.IsNullOrWhiteSpace(txtDate.Text.Trim()))
            {
                lblerrDate.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtTime.Text.Trim()) || string.IsNullOrWhiteSpace(txtTime.Text.Trim()))
            {
                lblerrTime.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdInbox.ExportSettings.ExportOnlyData = true;
        this.grdInbox.ExportSettings.IgnorePaging = true;
        this.grdInbox.ExportSettings.OpenInNewWindow = true;
        this.grdInbox.MasterTableView.ExportToExcel();

        grdInbox.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdInbox.ExportSettings.FileName = "Inbox";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdInbox_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getInbox();
            grdInbox.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdInbox_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdInbox.CurrentPageIndex + 1;
    }
    protected void grdInbox_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getInboxbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getInbox();
            }

            grdInbox.DataSource = dt;
            grdInbox.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdInbox_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdInbox.ExportSettings.ExportOnlyData = true;
            this.grdInbox.ExportSettings.IgnorePaging = true;
            this.grdInbox.ExportSettings.OpenInNewWindow = true;
            this.grdInbox.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getInboxlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["i_ID"].ToString();
                    txtInboxTitle.Text = dt.Rows[0]["i_title"].ToString();
                    txtContent.Content = dt.Rows[0]["i_content"].ToString();
                    txtDate.Text = dt.Rows[0]["i_date"].ToString();
                    txtTime.Text = dt.Rows[0]["i_time"].ToString();
                    hfispush.Value= dt.Rows[0]["i_isPush"].ToString();
                    txtOrganizerName.Text = dt.Rows[0]["organizer"] != DBNull.Value ? dt.Rows[0]["organizer"].ToString() : "";
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteInboxByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdInbox_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdInbox_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        hfispush.Value = "";
        txtKeyword.Text = "";
        txtInboxTitle.Text = "";
        txtContent.Content= "";
        txtDate.Text = "";
        txtTime.Text = "";
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrInboxTitle.Visible = false;
        lblerrDate.Visible = false;
        lblerrTime.Visible = false;
    }

    protected void btnPushNoti_Click(object sender, EventArgs e)
    {
        //if (hfid.Value != "")
        //{
        //    string id = hfid.Value.Trim();
        //    Response.Redirect("~/Admin/PushNortification.aspx?id=" + id + "&rid=" + id);
        //}
    }

    protected void lnkPushNoti_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "PushNoti")
            {
                string pid = del.CommandArgument.ToString();
                Response.Redirect("~/Admin/PushNortification.aspx?id=" + pid + "&rid=" + pid);
            }
        }
        catch(Exception ex)
        { }
    }
}