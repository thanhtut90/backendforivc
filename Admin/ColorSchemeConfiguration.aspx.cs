﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
public partial class Admin_ColorScheme : System.Web.UI.Page
{
    Functionality dl = new Functionality();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    int csID = 1, orderres = 0;
    string icon = "", name = "", chk = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (Session["AdminName"] == null)
            //{
            //    Response.Redirect("Timeout.aspx");
            //}
            
            if (csID !=0)
            {
                if (Request.QueryString["Edit"] == "1")
                {

                    csname.Enabled = true;
                    bg1c.Enabled = true;
                    bg1t.Enabled = true;
                    bg2c.Enabled = true;
                    bg2t.Enabled = true;
                    fc1c.Enabled = true;
                    fc1t.Enabled = true;
                    fc2c.Enabled = true;
                    fc2t.Enabled = true;
                    fc3c.Enabled = true;
                    fc3t.Enabled = true;
                    bc1c.Enabled = true;
                    bc1t.Enabled = true;
                    bc2c.Enabled = true;
                    bc2t.Enabled = true;
                    mc1c.Enabled = true;
                    mc1t.Enabled = true;
                    mc2c.Enabled = true;
                    mc2t.Enabled = true;
                    cc1c.Enabled = true;
                    cc1t.Enabled = true;
                    cc2c.Enabled = true;
                    cc2t.Enabled = true;
                }

                else if (Request.QueryString["Edit"] == "0")
                {
                    csname.Enabled = false;
                    bg1c.Enabled = false;
                    bg1t.Enabled = false;
                    bg2c.Enabled = false;
                    bg2t.Enabled = false;
                    fc1c.Enabled = false;
                    fc1t.Enabled = false;
                    fc2c.Enabled = false;
                    fc2t.Enabled = false;
                    fc3c.Enabled = false;
                    fc3t.Enabled = false;
                    bc1c.Enabled = false;
                    bc1t.Enabled = false;
                    bc2c.Enabled = false;
                    bc2t.Enabled = false;
                    mc1c.Enabled = false;
                    mc1t.Enabled = false;
                    mc2c.Enabled = false;
                    mc2t.Enabled = false;
                    cc1c.Enabled = false;
                    cc1t.Enabled = false;
                    cc2c.Enabled = false;
                    cc2t.Enabled = false;
                }

                //csID = Convert.ToInt32(Request.QueryString["csID"]);
                DataTable dt = dl.GetDatasetByCommand("select * from tb_ColorScheme where ID=" + csID + "","sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    csname.Text = dt.Rows[0]["CS_name"].ToString();
                    string[] bg1 = dt.Rows[0]["CS_BG1"].ToString().Split('/');

                    bg1c.Text = bg1[0];
                    bg1t.Text = bg1[1];

                    string[] bg2 = dt.Rows[0]["CS_BG2"].ToString().Split('/');

                    bg2c.Text = bg2[0];
                    bg2t.Text = bg2[1];

                    string[] fc1 = dt.Rows[0]["CS_F1"].ToString().Split('/');

                    fc1c.Text = fc1[0];
                    fc1t.Text = fc1[1];

                    string[] fc2 = dt.Rows[0]["CS_F2"].ToString().Split('/');

                    fc2c.Text = fc2[0];
                    fc2t.Text = fc2[1];

                    string[] fc3 = dt.Rows[0]["CS_F3"].ToString().Split('/');

                    fc3c.Text = fc3[0];
                    fc3t.Text = fc3[1];

                    string[] b1 = dt.Rows[0]["CS_B1"].ToString().Split('/');

                    bc1c.Text = b1[0];
                    bc1t.Text = b1[1];

                    string[] b2 = dt.Rows[0]["CS_B2"].ToString().Split('/');

                    bc2c.Text = b2[0];
                    bc2t.Text = b2[1];

                    string[] m1 = dt.Rows[0]["CS_M1"].ToString().Split('/');

                    mc1c.Text = m1[0];
                    mc1t.Text = m1[1];

                    string[] m2 = dt.Rows[0]["CS_M2"].ToString().Split('/');

                    mc2c.Text = m2[0];
                    mc2t.Text = m2[1];

                    string[] c1 = dt.Rows[0]["CS_C1"].ToString().Split('/');

                    cc1c.Text = c1[0];
                    cc1t.Text = c1[1];

                    string[] c2 = dt.Rows[0]["CS_C2"].ToString().Split('/');

                    cc2c.Text = c2[0];
                    cc2t.Text = c2[1];
                }

            }
        }
    }



    protected void Button1_Click(object sender, EventArgs e)//****
    {
        //if (Session["AdminName"] == null)
        //{
        //    Response.Redirect("Timeout.aspx");
        //}

        DateTime dtime = DateTime.Now;
        StringBuilder str = new StringBuilder();
        if (csID !=0)
        {
           // csID = Convert.ToInt32(Request.QueryString["csID"]);
            str.Append("Update tb_ColorScheme set CS_BG1=@CS_BG1,CS_BG2=@CS_BG2,CS_F1=@CS_F1,CS_F2=@CS_F2,CS_F3=@CS_F3,CS_B1=@CS_B1,CS_B2=@CS_B2,CS_M1=@CS_M1,CS_M2=@CS_M2,CS_C1=@CS_C1,CS_C2=@CS_C2,updatedDate=@updatedDate,CS_name=@CS_name where ID=" + csID + "");
            SqlParameter[] parms ={

                                        new SqlParameter("@CS_BG1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_BG2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F3",SqlDbType.VarChar),
                                        new SqlParameter("@CS_B1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_B2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_M1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_M2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_C1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_C2",SqlDbType.VarChar),
                                        new SqlParameter("@updatedDate",SqlDbType.VarChar),
                                        new SqlParameter("@CS_name",SqlDbType.VarChar)
                                      };

            parms[0].Value = bg1c.Text.Trim() + " / " + bg1t.Text.Trim();
            parms[1].Value = bg2c.Text.Trim() + " / " + bg2t.Text.Trim();
            parms[2].Value = fc1c.Text.Trim() + " / " + fc1t.Text.Trim();
            parms[3].Value = fc2c.Text.Trim() + " / " + fc2t.Text.Trim();
            parms[4].Value = fc3c.Text.Trim() + " / " + fc3t.Text.Trim();
            parms[5].Value = bc1c.Text.Trim() + " / " + bc1t.Text.Trim();
            parms[6].Value = bc2c.Text.Trim() + " / " + bc2t.Text.Trim();
            parms[7].Value = mc1c.Text.Trim() + " / " + mc1t.Text.Trim();
            parms[8].Value = mc2c.Text.Trim() + " / " + mc2t.Text.Trim();
            parms[9].Value = cc1c.Text.Trim() + " / " + cc1t.Text.Trim();
            parms[10].Value = cc2c.Text.Trim() + " / " + cc2t.Text.Trim();

            parms[11].Value = dalf.ConvertToTimestamp(DateTime.Now);
            parms[12].Value = csname.Text.Trim();
            orderres = dl.ExecuteNonQuery(str.ToString(), parms);           
        }
        else
        {
            str.Append("insert into tb_ColorScheme (CS_BG1,CS_BG2,CS_F1,CS_F2,CS_F3,CS_B1,CS_B2,CS_M1,CS_M2,CS_C1,CS_C2,Status,updatedDate,CS_name)");
            str.Append(" values ");
            str.Append("(@CS_BG1,@CS_BG2,@CS_F1,@CS_F2,@CS_F3,@CS_B1,@CS_B2,@CS_M1,@CS_M2,@CS_C1,@CS_C2,@Status,@updatedDate,@CS_name)");
            SqlParameter[] parms2 ={
                                        new SqlParameter("@CS_BG1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_BG2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_F3",SqlDbType.VarChar),
                                        new SqlParameter("@CS_B1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_B2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_M1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_M2",SqlDbType.VarChar),
                                        new SqlParameter("@CS_C1",SqlDbType.VarChar),
                                        new SqlParameter("@CS_C2",SqlDbType.VarChar),
                                        new SqlParameter("@Status",SqlDbType.VarChar),
                                        new SqlParameter("@updatedDate",SqlDbType.VarChar),
                                        new SqlParameter("@CS_name",SqlDbType.VarChar)
                                  };

            parms2[0].Value = bg1c.Text.Trim() + " / " + bg1t.Text.Trim();
            parms2[1].Value = bg2c.Text.Trim() + " / " + bg2t.Text.Trim();
            parms2[2].Value = fc1c.Text.Trim() + " / " + fc1t.Text.Trim();
            parms2[3].Value = fc2c.Text.Trim() + " / " + fc2t.Text.Trim();
            parms2[4].Value = fc3c.Text.Trim() + " / " + fc3t.Text.Trim();
            parms2[5].Value = bc1c.Text.Trim() + " / " + bc1t.Text.Trim();
            parms2[6].Value = bc2c.Text.Trim() + " / " + bc2t.Text.Trim();
            parms2[7].Value = mc1c.Text.Trim() + " / " + mc1t.Text.Trim();
            parms2[8].Value = mc2c.Text.Trim() + " / " + mc2t.Text.Trim();
            parms2[9].Value = cc1c.Text.Trim() + " / " + cc1t.Text.Trim();
            parms2[10].Value = cc2c.Text.Trim() + " / " + cc2t.Text.Trim();
            parms2[11].Value = "Add";          
            parms2[12].Value = dalf.ConvertToTimestamp(DateTime.Now);
            parms2[13].Value = csname.Text.Trim();
            orderres = dl.ExecuteNonQuery(str.ToString(), parms2);
        }
        if (orderres > 0)
        {
            DAL_Data daldata = new DAL_Data();
            string tablename = "tb_ColorScheme";
            daldata.UpdateDataVersion(tablename);

            Response.Write("<script language='javascript'>alert('Updated');window.location.href='ColorSchemeConfiguration.aspx?csID=1&Edit=1';</script>");
            Response.End();
        }
    }

    protected void lnkedit_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("ColorSchemeConfiguration.aspx?csID=1&Edit=1");
    }

    public string FilterSpecial(string str)
    {
        if (str == "") 
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");

            return str;
        }
    }

    public DataTable GetDt()
    {
        StringBuilder sql = new StringBuilder();

        sql.Append("SELECT * from tb_ColorScheme where Status <> 'delete' order by ID desc ");

        DataTable dtcollList = dl.GetDatasetByCommand(sql.ToString(),"sdt").Tables[0];

        return dtcollList;
    }
}