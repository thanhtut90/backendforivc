﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="reward.aspx.cs" Inherits="Admin_reward" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdfloorplan" />
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Home Page Info</h3>
                    <div style="float: right">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lnkDownload" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>
                </div>
                <div class="box-body">
                    <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;">
                        <div class="col-md-5">
                            <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />
                        </div>
                        <div class="col-md-6">
                            <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdReward" runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdReward_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdReward_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdReward_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="rw_ID" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                                        <Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1  %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("rw_ID") %>' OnClick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")' CommandName="Delete" CommandArgument='<%#Eval("rw_ID") %>' OnClick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="rw_title1" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Main Title" SortExpression="rw_title1" UniqueName="rw_title1" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="rw_title2" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Sub Title" SortExpression="rw_title2" UniqueName="rw_title2" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                              <telerik:GridBoundColumn DataField="rw_image1" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Image 1" SortExpression="rw_image1" UniqueName="rw_image1" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="rw_image2" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Image 2" SortExpression="rw_image2" UniqueName="rw_image2" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                             <telerik:GridBoundColumn DataField="rw_password" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Reward Code" SortExpression="rw_password" UniqueName="rw_password" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                              <telerik:GridBoundColumn DataField="rw_content1" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Content 1" SortExpression="rw_content1" UniqueName="rw_content1" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="rw_content2" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Content 2" SortExpression="rw_content2" UniqueName="rw_content2" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Main Title</label>
                                <div class="col-sm-6">
                                    <asp:HiddenField ID="hfid" runat="server" />

                                    <asp:TextBox ID="txtRewardTitle" CssClass="form-control" placeholder="Enter Main Title" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrRewardTitle" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Sub Title</label>

                                <div class="col-sm-6" style="left: 0px; top: 0px">
                                    <asp:TextBox ID="txtstitle" CssClass="form-control" placeholder="Enter sub title" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrstitle" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Image 1</label>
                                <div class="col-sm-3">
                                <asp:FileUpload ID="fupImg1" runat="server" /><asp:Label ID="lblImg1" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-3">

                                </div>
                            </div>
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Image 2</label>
                                <div class="col-sm-3">
                                <asp:FileUpload ID="fupImg2" runat="server" /><asp:Label ID="lblImg2" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-3">

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Reward Code</label>

                                <div class="col-sm-6" style="left: 0px; top: 0px">
                                    <asp:TextBox ID="txtRewardPassword" CssClass="form-control" placeholder="Enter Reward Code" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrRewardPassword" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Content 1</label>
                                <div class="col-sm-6">
                                    <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtContent" SkinID="DefaultSetOfTools" Width="90%">
                                        <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            EnableAsyncUpload="true"></ImageManager>
                                        <Tools>
                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="JustifyLeft" />
                                                <telerik:EditorTool Name="JustifyCenter" />
                                                <telerik:EditorTool Name="JustifyRight" />
                                                <telerik:EditorTool Name="JustifyFull" />
                                            </telerik:EditorToolGroup>

                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="Bold" />
                                                <telerik:EditorTool Name="Underline" />
                                                <telerik:EditorTool Name="Italic" />
                                            </telerik:EditorToolGroup>

                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="InsertTable" />
                                            </telerik:EditorToolGroup>
                                        </Tools>
                                    </telerik:RadEditor>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Content 2</label>
                                <div class="col-sm-6">
                                    <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtContent2" SkinID="DefaultSetOfTools" Width="90%">
                                        <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            EnableAsyncUpload="true"></ImageManager>
                                        <Tools>
                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="JustifyLeft" />
                                                <telerik:EditorTool Name="JustifyCenter" />
                                                <telerik:EditorTool Name="JustifyRight" />
                                                <telerik:EditorTool Name="JustifyFull" />
                                            </telerik:EditorToolGroup>

                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="Bold" />
                                                <telerik:EditorTool Name="Underline" />
                                                <telerik:EditorTool Name="Italic" />
                                            </telerik:EditorToolGroup>

                                            <telerik:EditorToolGroup>
                                                <telerik:EditorTool Name="InsertTable" />
                                            </telerik:EditorToolGroup>
                                        </Tools>
                                    </telerik:RadEditor>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

