﻿using System;
using System.Collections.Generic;
using App.DAL;
using System.Data;
using Jiguang.JPush;
using Jiguang.JPush.Model;
using System.Web.Script.Serialization;
using System.Collections;
using System.Net;

public partial class Admin_PushNortification : System.Web.UI.Page
{
    
    Functionality fn = new Functionality();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL dal = new DAL();
   // private static JPushClient client = new JPushClient(Global.AppKey, Global.MasterScretKey);
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Request.QueryString["s"] !=null)
            {
                SchedulePush();
            }
            else
            {
                string pid = Request.QueryString["id"].ToString();
                string rowid = Request.QueryString["rid"].ToString();
                Push(pid, rowid);
            }
          
        }
    }

    private void SchedulePush()
    {
        try
        {
            Hashtable htt = new Hashtable();
            DateTime today = DateTime.Now;//13/3/2018 6:25:59 PM
            string[] arrtoday = today.ToShortDateString().ToString().Split('/');
            string tdd = arrtoday[1].ToString();
            if (tdd.Length == 1)
            {
                tdd = string.Format("0{0}", tdd);
            }
            string tdm = arrtoday[0].ToString();
            if (tdm.Length == 1)
            {
                tdm = string.Format("0{0}", tdm);
            }
            string tdy = arrtoday[2].ToString();
            string tDate = tdd + "/" + tdm + "/" + tdy;

            string time = today.TimeOfDay.ToString();//17:55:29.1659914
            string[] arrtime = time.Split(':');
            string hh = arrtime[0].ToString();
            string mm = arrtime[1].ToString();
            string ss = arrtime[2].ToString();
            string tTime = hh + ":" + mm;
            DataTable dt = selectCurrentInbox(tDate, tTime);// dal.selectAllInbox();
            if(dt.Rows.Count>0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    string pdate = dr["i_date"].ToString();//14/03/2018	
                    string pTime = dr["i_time"].ToString();//09:00:00
                    string status= dr["i_isPush"].ToString();
                    if(status == "False")
                    {
                        //string[] arrpdate = pdate.Split('/');
                        //string pdd = arrpdate[0].ToString();

                        //DateTime today = DateTime.Now;//13/3/2018 6:25:59 PM
                        //string[] arrtoday = today.ToString().Split('/');
                        //string tdd = arrtoday[1].ToString();

                        //string time = today.TimeOfDay.ToString();//17:55:29.1659914
                        //string[] arrtime = time.Split(':');
                        //string hh = arrtime[0].ToString();
                        //string mm = arrtime[1].ToString();
                        //string ss = arrtime[2].ToString();
                        //string tTime = hh + ":" + mm;

                        string rowid = dr["i_ID"].ToString();
                        //if (pdd == tdd && pTime == tTime)
                        if(pdate == tDate && pTime == tTime)
                        {
                            Push(dr["i_ID"].ToString(), rowid);
                        }
                        else
                        {
                            htt = new Hashtable();
                            htt.Add("Status", "000");
                            //htt.Add("message", "Not in the Time Interval" + " | InboxDate:" + pdd + " | todayDate:" + tdd + " | InboxTime:" + pTime + " | TodayTime:" + tTime);
                            htt.Add("message", "Not in the Time Interval" + " | InboxDate:" + pdate + " | todayDate:" + tDate + " | InboxTime:" + pTime + " | TodayTime:" + tTime);
                            JavaScriptSerializer ser = new JavaScriptSerializer();
                            String jsonStr = ser.Serialize(htt);
                            Response.Write(jsonStr);
                        }
                    }
                }
            }
            else
            {
                htt = new Hashtable();
                htt.Add("Status", "000");
                htt.Add("message", "No records" + " | todayDate:" + tDate + " | TodayTime:" + tTime);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                String jsonStr = ser.Serialize(htt);
                Response.Write(jsonStr);
            }
        }
        catch(Exception ex) { }
    }

    public void Push(string pid,string rowid)
    {
        Hashtable htt = new Hashtable();
        
        List<string> registrationIDs = new List<string>();
        Dictionary<string, string> customizedValues = new Dictionary<string, string>();
        var sqlmessage = "select i_title from tblInbox where i_ID='" + pid + "'";
        var sqlcontent = "select i_content from tblInbox where i_ID='" + pid + "'";


        string title = fn.GetDataByCommand(sqlmessage, "i_title");
        string content = fn.GetDataByCommand(sqlcontent, "i_content");
        string type = "6" + rowid;// => to make it dynamic "6"

        if (!string.IsNullOrEmpty(title) && !string.IsNullOrEmpty(content))
        {   
            customizedValues.Add("newsID", type);
            string msg = PushMessageByRegistrationID(title, content, type, registrationIDs, rowid.ToString(), customizedValues);
            if (Request.QueryString["s"] != null)
            {
                if (!string.IsNullOrEmpty(msg))
                {
                    UpdatePushStatus(pid, rowid);
                    htt = new Hashtable();
                    htt.Add("Status", "200");
                    //htt.Add("message", "success");
                    htt.Add("message", "success" + " | ID:" + pid + " ");
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    String jsonStr = ser.Serialize(htt);
                    Response.Write(jsonStr);
                }
                else
                {
                    htt = new Hashtable();
                    htt.Add("Status", "000");
                    //htt.Add("message", "fail");
                    htt.Add("message", "fail" + " | ID:" + pid + " ");
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    String jsonStr = ser.Serialize(htt);
                    Response.Write(jsonStr);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(msg))
                {
                    UpdatePushStatus(pid, rowid);//***
                    Response.Write("<script language='javascript'>window.location.href='Inbox.aspx?id=" + pid + "&&n=suc';</script>");
                }
                else
                {
                    Response.Write("<script language='javascript'>window.location.href='Inbox.aspx?id=" + pid + "&&n=fail';</script>");
                }
            }
            
        }
    }
    private void UpdatePushStatus(string pid, string rowid)
    {
        try
        {
            DAL_Data daldata = new DAL_Data();
            string sql = "update tblInbox set i_isPush=1 where i_ID='" + pid + "'";
            fn.ExecuteSQL(sql);
            daldata.UpdateDataVersion("tblInbox");
        }
        catch(Exception ex) { }
    }
    public static string PushMessageByRegistrationID(string title, string content, string type, List<string> registrationIDs, string InboxID, Dictionary<string, string> customizedValues = null)
    {
        ServicePointManager.Expect100Continue = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        JPushClient client = new JPushClient(Global.AppKey, Global.MasterScretKey);
        PushPayload pushPayload = PushObject_All_All_Alert(title, type, InboxID);
        var result = client.SendPush(pushPayload);
        
        return result.Content.ToString();
    }
    public static PushPayload PushObject_All_All_Alert(string content, string type, string InboxID)
    {
        PushPayload pushPayload = new PushPayload();
        pushPayload.Platform = "all";
        pushPayload.Audience = "all";
        pushPayload.Notification = new Notification
        {
            Alert = content,
            Android = new Android
            {
                Alert = content,
                //Title = type

            },
            IOS = new IOS
            {
                Alert = content,
                Badge = "+1"
            }
        };

        pushPayload.Message = new Message
        {
            //Title = type,
            Content = content,
            Extras = new Dictionary<string, string>
            {
                ["newsID"] = type
            }
        };       

        pushPayload.Options = new Options
        {
            IsApnsProduction = false
        };
        return pushPayload;
    }
    #region getData
    public DataTable selectCurrentInbox(string tDate, string tTime)
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from tblInbox where deleteFlag=0 and i_isPush=0 And i_date='" + tDate + "' And i_time='" + tTime + "'";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }
    #endregion
    #region Old version
    /* 
    using cn.jpush.api.push.notification;
 using cn.jpush.api;
 using cn.jpush.api.push.mode;
    public static string PushMessageByRegistrationID(string title, string content, string type, List<string> registrationIDs, string InboxID, Dictionary<string, string> customizedValues = null)
     {


         JPushClient client = new JPushClient(app_key, MasterSecret);
         PushPayload payload = PushObject_All_All_Alert(content, type, InboxID);
         var result = client.SendPush(payload);
         return result.ResponseResult.responseContent.ToString();
     }

     public static PushPayload PushObject_All_All_Alert(string content, string type, string InboxID)
     {
         PushPayload pushPayload = new PushPayload();
         pushPayload.platform = Platform.all();
         pushPayload.audience = Audience.all();
         pushPayload.notification = new Notification().setAlert(content).setAndroid(new AndroidNotification().AddExtra("title", type)).setIos(new IosNotification().AddExtra("newsID", type));

         pushPayload.message = Message.content(content);//**** Android
         pushPayload.message.title = type;//**** Android       

         pushPayload.options.apns_production = true;
         return pushPayload;
     }*/
    #endregion
}