﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_innovation : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblInnovation";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdInnovation.ExportSettings.IgnorePaging = true;
                grdInnovation.ExportSettings.ExportOnlyData = true;
                grdInnovation.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdInnovation.ExportSettings.FileName = "Innovation";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtExhCom = daldata.getExhibitingCompanies();
            ddlExhibitor.DataSource = dtExhCom;
            ddlExhibitor.DataTextField = "ec_name";
            ddlExhibitor.DataValueField = "ec_ID";
            ddlExhibitor.DataBind();
            ListItem lstExhCom = new ListItem("Select Exhibitor", "");
            ddlExhibitor.Items.Insert(ddlExhibitor.Items.Count - ddlExhibitor.Items.Count, lstExhCom);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("Ino_ID");
                dt.Columns.Add("Ino_productName");
                dt.Columns.Add("Ino_QCategories");
                dt.Columns.Add("Ino_productInfo");
                dt.Columns.Add("Ino_productPic");
                dt.Columns.Add("Ino_certification");
                dt.Columns.Add("Ino_region");
                dt.Columns.Add("Ino_rating");
                dt.Columns.Add("Ino_exhID");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string message = string.Empty;

                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                if (fupImage.HasFile == true)
                {
                    filename = fupImage.FileName.Substring(0, fupImage.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupImage.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }

                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["Ino_ID"] = hfid.Value.ToString();
                    dr["Ino_productName"] = daldata.replaceForSQL(txtProductName.Text.Trim());
                    dr["Ino_QCategories"] = daldata.replaceForSQL(txtQCategories.Text.Trim());
                    dr["Ino_productInfo"] = daldata.replaceForSQL(txtProductInformation.Text.Trim());
                    dr["Ino_productPic"] = imagename == "" ? lblImage.Text : imagename;
                    dr["Ino_certification"] = daldata.replaceForSQL(txtCertification.Text.Trim());
                    dr["Ino_region"] = daldata.replaceForSQL(txtRegion.Text.Trim());
                    dr["Ino_rating"] = daldata.replaceForSQL(txtRating.Text.Trim());
                    dr["Ino_exhID"] = ddlExhibitor.SelectedValue.ToString();
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateInnovation(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdInnovation_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["Ino_ID"] = "";
                    dr["Ino_productName"] = daldata.replaceForSQL(txtProductName.Text.Trim());
                    dr["Ino_QCategories"] = daldata.replaceForSQL(txtQCategories.Text.Trim());
                    dr["Ino_productInfo"] = daldata.replaceForSQL(txtProductInformation.Text.Trim());
                    dr["Ino_productPic"] = imagename == "" ? lblImage.Text : imagename;
                    dr["Ino_certification"] = daldata.replaceForSQL(txtCertification.Text.Trim());
                    dr["Ino_region"] = daldata.replaceForSQL(txtRegion.Text.Trim());
                    dr["Ino_rating"] = daldata.replaceForSQL(txtRating.Text.Trim());
                    dr["Ino_exhID"] = ddlExhibitor.SelectedValue.ToString();
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveInnovation(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdInnovation_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtProductName.Text.Trim()) || string.IsNullOrWhiteSpace(txtProductName.Text.Trim()))
            {
                lblerrProductName.Visible = true;
                rtn = false;
            }
            if (ddlExhibitor.SelectedItem.Value == "")
            {
                lblerrExhibitor.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdInnovation.ExportSettings.ExportOnlyData = true;
        this.grdInnovation.ExportSettings.IgnorePaging = true;
        this.grdInnovation.ExportSettings.OpenInNewWindow = true;
        this.grdInnovation.MasterTableView.ExportToExcel();

        grdInnovation.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdInnovation.ExportSettings.FileName = "Innovation";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdInnovation_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getInnovation();
            grdInnovation.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdInnovation_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdInnovation.CurrentPageIndex + 1;
    }
    protected void grdInnovation_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getInnovationbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getInnovation();
            }

            grdInnovation.DataSource = dt;
            grdInnovation.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdInnovation_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdInnovation.ExportSettings.ExportOnlyData = true;
            this.grdInnovation.ExportSettings.IgnorePaging = true;
            this.grdInnovation.ExportSettings.OpenInNewWindow = true;
            this.grdInnovation.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getInnovationlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["Ino_ID"].ToString();
                    txtProductName.Text = dt.Rows[0]["Ino_productName"].ToString();

                    txtQCategories.Text = dt.Rows[0]["Ino_QCategories"].ToString();
                    txtProductInformation.Text = dt.Rows[0]["Ino_productInfo"].ToString();
                    lblImage.Text = dt.Rows[0]["Ino_productPic"].ToString();
                    txtCertification.Text = dt.Rows[0]["Ino_certification"].ToString();
                    txtRegion.Text = dt.Rows[0]["Ino_region"].ToString();
                    txtRating.Text = dt.Rows[0]["Ino_rating"].ToString();
                    string exhID = dt.Rows[0]["Ino_exhID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(exhID))
                        {
                            ListItem listItem = ddlExhibitor.Items.FindByValue(exhID);
                            if (listItem != null)
                            {
                                ddlExhibitor.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteInnovationByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdInnovation_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdInnovation_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtProductName.Text = "";
        txtQCategories.Text = "";
        txtProductInformation.Text = "";
        lblImage.Text = "";
        txtCertification.Text = "";
        txtRegion.Text = "";
        txtRating.Text = "";
        ddlExhibitor.SelectedIndex = 0;
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrProductName.Visible = false;
        lblerrQCategories.Visible = false;
        lblerrProductInformation.Visible = false;
        lblerrCertification.Visible = false;
        lblerrRegion.Visible = false;
        lblerrRating.Visible = false;
        lblerrExhibitor.Visible = false;
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupImage.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public string getFileLink(string filename)
    {
        string filelink = string.Empty;
        if (!string.IsNullOrEmpty(filename))
        {
            filelink = Constant.URL + Constant.filesavingurlview + filename;
        }

        return filelink;
    }
}