﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;
using System.Data;
using System.Text;

public partial class Admin_Dashboard : System.Web.UI.Page
{
    public string visitors;
    public string exhibitors;
    public string downloadappcount;
    public string mostpopularexhibitor;
    public string eventduration;
    protected int sum;
    protected string Vhtmltb;
    protected string Ehtmltb;

    DAL_FrontEnd dalf = new DAL_FrontEnd();
    Functionality fn = new Functionality();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            //visitors = dalf.SelectVisitorCount();
            //exhibitors = dalf.SelectExhibitorCount();
            //downloadappcount = dalf.SelectActivatedUserCount();
            //mostpopularexhibitor = "Corporate IT Solutions PTE LTD";
            //eventduration = Global.EventDuration;

            //ChartAppointmentByDay.DataSource = BindAppointmentByEventDayReportChart();
            //ChartAppointmentByDay.DataBind();

            //ChartProductofInterest.DataSource = BindTop5ProductofInterest();
            //ChartProductofInterest.DataBind();

            //string VisitorReportData = BindSurveyReport("V");
            //Vhtmltb = VisitorReportData;

            //string ExhibitorReportData = BindSurveyReport("E");
            //Ehtmltb = ExhibitorReportData;            

            //ChartTop5Exhi.DataSource= BindTop5Exhibitors();
            //ChartTop5Exhi.DataBind();
        }
    }
   // public DataTable BindAppointmentByEventDayReportChart()
   // {
   //     DataTable dt = dalf.selectBusinessMatchingReportByDay("BP10031");
   //     for(int i=0;i<dt.Rows.Count;i++)
   //     {
   //         ChartAppointmentByDay.PlotArea.XAxis.Items.Add(dt.Rows[i]["Desc"].ToString());
   //         ChartAppointmentByDay.PlotArea.XAxis.LabelsAppearance.RotationAngle = 40;
   //     }
       
   //     ChartAppointmentByDay.PlotArea.XAxis.TitleAppearance.Text = "Exhibition Days";
   //     ChartAppointmentByDay.PlotArea.XAxis.TitleAppearance.Position = Telerik.Web.UI.HtmlChart.AxisTitlePosition.Center;
   //     ChartAppointmentByDay.PlotArea.XAxis.TitleAppearance.RotationAngle = 0;
   //     ChartAppointmentByDay.PlotArea.XAxis.LabelsAppearance.Skip = 0;
   //     ChartAppointmentByDay.PlotArea.XAxis.LabelsAppearance.Step = 1;
   //     ChartAppointmentByDay.PlotArea.XAxis.AxisCrossingValue = 0;
   //     ChartAppointmentByDay.PlotArea.XAxis.Color = System.Drawing.Color.Black;
   //     ChartAppointmentByDay.PlotArea.XAxis.MajorTickType = Telerik.Web.UI.HtmlChart.TickType.Outside;
   //     ChartAppointmentByDay.PlotArea.XAxis.MinorTickType = Telerik.Web.UI.HtmlChart.TickType.Outside;
   //     ChartAppointmentByDay.PlotArea.XAxis.Reversed = false;

   //     ChartAppointmentByDay.PlotArea.YAxis.TitleAppearance.Text = "No of Appointments";
   //     ChartAppointmentByDay.PlotArea.YAxis.TitleAppearance.Position = Telerik.Web.UI.HtmlChart.AxisTitlePosition.Center;
   //     ChartAppointmentByDay.PlotArea.YAxis.TitleAppearance.RotationAngle = 0;
   //     ChartAppointmentByDay.PlotArea.YAxis.LabelsAppearance.Skip = 0;
   //     ChartAppointmentByDay.PlotArea.YAxis.LabelsAppearance.Step = 1;
   //     ChartAppointmentByDay.PlotArea.YAxis.AxisCrossingValue = 0;
   //     ChartAppointmentByDay.PlotArea.YAxis.Color = System.Drawing.Color.Black;
   //     ChartAppointmentByDay.PlotArea.YAxis.MajorTickType = Telerik.Web.UI.HtmlChart.TickType.Outside;
   //     ChartAppointmentByDay.PlotArea.YAxis.MajorTickSize = 1; 
   //     ChartAppointmentByDay.PlotArea.YAxis.MinorTickType = Telerik.Web.UI.HtmlChart.TickType.Outside;
   //     ChartAppointmentByDay.PlotArea.YAxis.MajorTickSize = 1;
   //     ChartAppointmentByDay.PlotArea.YAxis.Reversed = false;
   //     ChartAppointmentByDay.PlotArea.YAxis.MinValue = 0;
   //     ChartAppointmentByDay.PlotArea.YAxis.Step = 1;
    
   //     return dt;
   // }
   // public DataTable BindTop5ProductofInterest()
   // {
   //     DataTable dt = new DataTable();
   //     dt.Columns.Add("Total");
   //     dt.Columns.Add("Desc");

   //     Double catCount;
   //     Double TotalCount = 3; // total product count
   //     for (int i = 0; i < 5; i++)
   //     {
   //         catCount = i;
   //         Double caldbscan = Math.Round((catCount / TotalCount) * 100, 2);
   //         DataRow drow = dt.NewRow();
   //         drow["Desc"] = "Category";
   //         drow["Total"] = i;
   //         dt.Rows.Add(drow);
   //     }
   //     if (dt.Rows.Count == 0)
   //     {
   //         DataRow drow = dt.NewRow();
   //         drow["Desc"] = "NO Record";
   //         drow["Total"] = "100";
   //     }
   //     return dt;
   // }
   // public string BindSurveyReport(string UserType)
   // {        
   //     int icount = 0;

   //     StringBuilder stb = new StringBuilder();

   //     string sql = "select * from SurveyCate  where UserType='" + UserType + "' order by SurveyCateenum asc";
   //     DataTable dtv = fn.GetDatasetByCommand(sql,"sdt").Tables[0];
   //     if (dtv.Rows.Count > 0)
   //     {            
   //         foreach (DataRow dr in dtv.Rows)
   //         {
   //             string SurveyCateTitle = dr["SurveyCateTitle"].ToString();

   //             int SurveyCateId = int.Parse(dr["SurveyCateId"].ToString());
   //             stb.Append("<tr>");
   //             stb.Append("<td colspan='3' bgcolor='#E1E1E1'><strong class='bodypar'>" + SurveyCateTitle + "</strong></td>");
   //             stb.Append("</tr>");
   //             string sql1 = "select * from SurveyList where SurveyCateId=" + SurveyCateId + " ";
   //             DataTable dtv1 = fn.GetDatasetByCommand(sql1,"sdt").Tables[0];
   //             if (dtv1.Rows.Count > 0)
   //             {
   //                 foreach (DataRow dr1 in dtv1.Rows)
   //                 {
   //                     var count = fn.GetDataByCommand("select count(SurveycateId) as Count from SurveyList where SurveyCateId=" + SurveyCateId + "", "Count").ToString();
   //                     if (!String.IsNullOrEmpty(count))
   //                     {
   //                         icount = int.Parse(count);
   //                     }

   //                     string SurveyTitle = dr1["SurveyTitle"].ToString();

   //                     string SurveyKeyNo = dr1["SurveyKeyNo"].ToString();

   //                     string sqlnum = "SELECT count(*) as Count from SurveyQA where  (" + SurveyKeyNo + "='1' or " + SurveyKeyNo + "<>'') ";// and Uid in (select UserID from NewUserList) ";
   //                     int quessum = Convert.ToInt32(fn.GetDataByCommand(sqlnum,"Count"));
   //                     string preques = ((double)quessum / icount * 100).ToString("F2") + "%";

   //                     stb.Append("<tr>");
   //                     stb.Append("<td width='329'><span class='bodypar'>" + SurveyTitle + "</span></td>");
   //                     stb.Append("<td width='52'><span class='bodypar'>" + quessum + "</span></td>");
   //                     stb.Append("<td width='53'><span class='bodypar'>" + preques + "</span></td>");

   //                     stb.Append("</tr>");                        
   //                 }

   //             }
   //         }
   //     }
   //     return stb.ToString();
   // }
   // public DataTable BindTop5Exhibitors()
   //{
   //     DataTable dt = new DataTable();
   //     dt = dalf.selectTop5AppointmentExhi();
   //     if(dt.Rows.Count>0)
   //     {
   //         mostpopularexhibitor = dt.Rows[0]["ExhiID"].ToString();
   //         for (int i = 0; i < dt.Rows.Count; i++)
   //         {
   //             ChartTop5Exhi.PlotArea.XAxis.Items.Add(dt.Rows[i]["ExhiID"].ToString());
   //             ChartTop5Exhi.PlotArea.XAxis.LabelsAppearance.RotationAngle = 20;

   //         }
   //         ChartTop5Exhi.PlotArea.XAxis.TitleAppearance.Text = "Exhibitors";

   //         ChartTop5Exhi.PlotArea.YAxis.TitleAppearance.Text = "No of Appointments";
   //         ChartTop5Exhi.PlotArea.YAxis.Step = 1;
   //     }
        
   //     return dt;
   // }
   // protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
   // {
   //     try
   //     {           
   //         DataTable res = dalf.selectlatestAppointment();
   //         grdFML.DataSource = res;
   //     }
   //     catch (Exception ex) { }
   // }
   // protected void grdFML_NeedDSource()
   // {
   //     try
   //     {
   //         DataTable res = dalf.selectlatestAppointment();
   //         grdFML.DataSource = res;
   //         grdFML.DataBind();

   //     }
   //     catch (Exception ex) { }
   // }
   // public string getVisitor(string vid)
   // {
   //     string rtn = string.Empty;       

   //     var sal = dalf.selectContentByCIDRID("BC10003", vid);
   //     var fname= dalf.selectContentByCIDRID("BC10004", vid);
   //     var lname = dalf.selectContentByCIDRID("BC10005", vid);

   //     rtn = sal + " " + fname + " " + lname;
   //     return rtn;
   // }
   // public string getExhibitor(string eid)
   // {
   //     string rtn = string.Empty;        
   //     rtn= dalf.selectContentByCIDRID("BC10119", eid);
   //     return rtn;
   // }
   // public string getStatus(string status)
   // {
   //     string str = string.Empty;
   //     if(status =="0")
   //     {
   //         str= "<span class='label label-warning'>Pending</span>";
   //     }
   //     else if(status =="1")
   //     {
   //         str = "<span class='label label-success'>Approved</span>";
   //     }
   //     else if(status =="2")
   //     {
   //         str = "<span class='label label-danger'>Rejected</span>";
   //     }
   //     return str;

   // }
   
}