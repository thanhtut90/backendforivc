﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;
using System.Text;

public partial class Admin_MenuConfiguration : System.Web.UI.Page
{
    DAL dal = new DAL();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data daldata = new DAL_Data();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindPageType();
            BindParentMenu();
            BindOrderNum();
            if(Request.QueryString["id"] != null)
            {
                btnDelete.Visible = true;
                string id = Request.QueryString["id"].ToString();
                //tb_Menu obj = new tb_Menu();
                //obj = dal.selectMenuByID(id);
                //if(obj != null)
                DataTable dtMenu = daldata.selectMenuByID(id);
                if(dtMenu.Rows.Count > 0)
                {
                    //id,nameen,namecn,ordernum,parentID,pagetype,menutype, micon, htmlstr,permssion
                    txtnamecn.Text = dtMenu.Rows[0]["m_TitleCn"] != DBNull.Value ? dtMenu.Rows[0]["m_TitleCn"].ToString() : "";
                    txtnameen.Text = dtMenu.Rows[0]["m_TitleEn"] != DBNull.Value ? dtMenu.Rows[0]["m_TitleEn"].ToString() : "";
                    lblmorder.Text = dtMenu.Rows[0]["m_OrderID"] != DBNull.Value ? dtMenu.Rows[0]["m_OrderID"].ToString() : "";
                    string parentMenu= dtMenu.Rows[0]["m_parentMenuID"] != DBNull.Value ? dtMenu.Rows[0]["m_parentMenuID"].ToString() : "";
                    if (!string.IsNullOrEmpty(parentMenu))
                    {
                        chkisChild.Checked = true;
                        ddlparent.SelectedValue = parentMenu;
                        pmenu.Visible = true;
                    }
                    BindPageTypeForEdit(id);
                    string pName = dtMenu.Rows[0]["m_PageType"] != DBNull.Value ? dtMenu.Rows[0]["m_PageType"].ToString() : "";
                    string ptype = dal.selectPageTypeIDByName(pName);
                    ddlpagetype.SelectedValue = ptype;
                    //chkmenutype
                    var menutypes = dtMenu.Rows[0]["m_menuType"] != DBNull.Value ? dtMenu.Rows[0]["m_menuType"].ToString() : "";
                    if(menutypes.Contains('/'))
                    {
                        string[] arrslt = menutypes.Split('/');
                        if (arrslt.Count() > 0)
                        {
                            for (int i = 0; i < arrslt.Count(); i++)
                            {
                                for (int j = 0; j < chkmenutype.Items.Count; j++)
                                {
                                    if (chkmenutype.Items[j].Value == arrslt[i].ToString())
                                    {
                                        chkmenutype.Items[j].Selected = true;
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < chkmenutype.Items.Count; j++)
                        {
                            if (chkmenutype.Items[j].Value == menutypes)
                            {
                                chkmenutype.Items[j].Selected = true;
                            }

                        }
                    }
  
                    lblmicon.Text = dtMenu.Rows[0]["m_IconUrl"] != DBNull.Value ? dtMenu.Rows[0]["m_IconUrl"].ToString() : "";
                    //chkpermission
                    var permission = dtMenu.Rows[0]["m_permissioncontrol"] != DBNull.Value ? dtMenu.Rows[0]["m_permissioncontrol"].ToString() : "";
                    if(permission.Contains('/'))
                    {
                        string[] arr = permission.Split('/');
                        if (arr.Count() > 0)
                        {
                            for (int i = 0; i < arr.Count(); i++)
                            {
                                for (int j = 0; j < chkpermission.Items.Count; j++)
                                {
                                    if (chkpermission.Items[j].Value == arr[i].ToString())
                                    {
                                        chkpermission.Items[j].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0; j < chkpermission.Items.Count; j++)
                        {
                            if (chkpermission.Items[j].Value == permission.ToString())
                            {
                                chkpermission.Items[j].Selected = true;
                            }
                        }
                    }

                    txthtmlstr.Content = dtMenu.Rows[0]["m_htmlString"] != DBNull.Value ? dtMenu.Rows[0]["m_htmlString"].ToString() : "";
                    txthtmlstr2.Content = dtMenu.Rows[0]["m_htmlString2"] != DBNull.Value ? dtMenu.Rows[0]["m_htmlString2"].ToString() : "";
                    txtweblink.Text = dtMenu.Rows[0]["m_weblink"] != DBNull.Value ? dtMenu.Rows[0]["m_weblink"].ToString() : "";
                    btnSubmit.Text = "Update";

                }
            }
        }
    }
    private void BindOrderNum()
    {
        var num = dal.getMenuOrderNum();
        lblmorder.Text = num;
    }
    private void BindParentMenu()
    {
        try
        {
            DataTable dt = dal.getParentMenu();
            if (dt.Rows.Count > 0)
            {
                ddlparent.DataSource = dt;
                ddlparent.DataTextField = "m_TitleEn";
                ddlparent.DataValueField = "m_id";
                ddlparent.DataBind();
                ListItem lst = new ListItem("Choose Parent Menu", "");
                ddlparent.Items.Insert(ddlparent.Items.Count - ddlparent.Items.Count, lst);

            }
            else
            {
                ListItem lst = new ListItem("No Data", "");
                ddlparent.Items.Insert(ddlparent.Items.Count - ddlparent.Items.Count, lst);
            }
        }
        catch { }
    }
    private void BindPageType()
    {
        try
        {
            DataTable dt = dal.getPageType();
            if (dt.Rows.Count > 0)
            {

                ddlpagetype.DataSource = dt;
                ddlpagetype.DataTextField = "PT_Name";
                ddlpagetype.DataValueField = "ID";
                ddlpagetype.DataBind();
                ListItem lst = new ListItem("Choose Page Type", "");
                ddlpagetype.Items.Insert(ddlpagetype.Items.Count - ddlpagetype.Items.Count, lst);

            }
            else
            {
                ListItem lst = new ListItem("No Data", "");
                ddlpagetype.Items.Insert(ddlpagetype.Items.Count - ddlpagetype.Items.Count, lst);
            }
        }
        catch (Exception ex){ throw ex; }
    }
    private void BindPageTypeForEdit(string ID)
    {
        try
        {
            //List<PageType> dt = dal.getPageTypeForEdit(ID);
            DataTable dt = dal.getPageTypeForEdit(ID);
            if (dt.Rows.Count > 0)
            {
                ddlpagetype.DataSource = dt;
                ddlpagetype.DataTextField = "PT_Name";
                ddlpagetype.DataValueField = "ID";
                ddlpagetype.DataBind();
                ListItem lst = new ListItem("Choose Page Type", "");
                ddlpagetype.Items.Insert(ddlpagetype.Items.Count - ddlpagetype.Items.Count, lst);

            }
            else
            {
                ListItem lst = new ListItem("No Data", "");
                ddlpagetype.Items.Insert(ddlpagetype.Items.Count - ddlpagetype.Items.Count, lst);
            }
        }
        catch (Exception ex) { throw ex; }
    }
    protected void chkisChild_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (chkisChild.Checked == true)
            {
                pmenu.Visible = true;                
            }
            else
            {
                pmenu.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void ddlparent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlparent.SelectedValue  != "")
        {
            BindOrderNumSub(ddlparent.SelectedValue.ToString());
        }
    }
    private void BindOrderNumSub(string parentID)
    {
        var num = dal.getMenuOrderNumSub(parentID);
        lblmorder.Text = num;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
       
        if(!String.IsNullOrEmpty(txtnameen.Text) && !String.IsNullOrEmpty(ddlpagetype.SelectedValue) && !String.IsNullOrEmpty(chkmenutype.SelectedValue))
        {
            noti.Visible = false;
            string nameen = txtnameen.Text.Trim();
            string namecn = txtnamecn.Text.Trim();
            string pagetype = ddlpagetype.SelectedItem.ToString();

                       
            List<string> menuselectedValues = chkmenutype.Items.Cast<ListItem>()
               .Where(li => li.Selected)
               .Select(li => li.Value)
               .ToList();
            string menutype = string.Empty;
            if (menuselectedValues.Count > 0)
            {
                foreach (var s in menuselectedValues)
                {
                    menutype += s.ToString() + "/";
                }
            }
            if (menutype != "")
            {
                var lastcoma = menutype.Length - 1;
                menutype = menutype.Remove(lastcoma);
            }

            string ordernum = lblmorder.Text;
            string parentID = string.Empty;            
            List<string> selectedValues = chkpermission.Items.Cast<ListItem>()
               .Where(li => li.Selected)
               .Select(li => li.Value)
               .ToList();
            string permssion = string.Empty;
            if(selectedValues.Count>0)
            {
                foreach(var s in selectedValues)
                {
                    permssion += s.ToString() + "/";
                }                
            }
            if (permssion != "")
            {
                var lastcoma = permssion.Length - 1;
                permssion = permssion.Remove(lastcoma);
            }
            if (chkisChild.Checked == true)
            {
                parentID = ddlparent.SelectedValue;
            }
            string htmlstr = txthtmlstr.Content;
            string htmlstr2 = txthtmlstr2.Content;
            string micon = string.Empty;
            if (fupicon.HasFile == false && !String.IsNullOrEmpty(lblmicon.Text))
            {
                micon = lblmicon.Text;
            }
            else if (fupicon.HasFile == true)// && String.IsNullOrEmpty(lblmicon.Text))
            {
                micon = UploadImageFile("AppIcon");
            }
            string id = string.Empty;
            if(Request.QueryString["id"] != null)
            {
                id = Request.QueryString["id"].ToString();
                string upagetype = dal.selectPageTypeByMenuID(id);// dal.selectMenuByID(id).m_PageType.ToString();
                dal.updatePageTypeUsage(upagetype);
            }
            string weblink = txtweblink.Text.Trim();
            int res = dal.insertMenu(id,nameen,namecn,ordernum,parentID,pagetype,menutype, micon, htmlstr, htmlstr2, permssion,weblink);
            if(res>0)
            {
                string tablename = "tb_Menu";
                daldata.UpdateDataVersion(tablename);
                ClearData();
                grdFML_NeedDSource();
                BindParentMenu();
                BindOrderNum();
                Response.Redirect("MobileMenuConfiguration.aspx");
                //Page_Load(null, null);
            }
        }
        else
        {
            msg.Text = "Please input Name (En)/choose Page Type/Menu Type.";
            noti.Visible = true;
            noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
        }
    }
    private void ClearData()
    {
        txtnameen.Text = string.Empty;
        txtnamecn.Text = string.Empty;
        txthtmlstr.Content = string.Empty;
        lblmorder.Text = string.Empty;
        lblmicon.Text = string.Empty;
        chkmenutype.ClearSelection();
        ddlpagetype.SelectedValue = "";
        ddlparent.SelectedIndex = 0;
        chkisChild.Checked = false;
        chkpermission.ClearSelection();
        pmenu.Visible = false;
        txtweblink.Text = string.Empty;

    }
    private string UploadImageFile(string fname)
    {
        string imagefilename = string.Empty;
        try
        {
            string ext = System.IO.Path.GetExtension(fupicon.FileName).ToString().ToLower();
            if (ext != ".png" && ext != ".jpg" && ext != ".gif" && ext != ".jpeg")
            {
                msg.Text = "Invalid Image Type";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            }
            else
            {
                noti.Visible = false;
            }

            string filename = fname + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + ext;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\sys_Images\\Mobile\\").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\sys_Images\\Mobile\\") + filename);
            fupicon.SaveAs(savePath);
            imagefilename = filename;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }
    protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = dal.selectMenu();
            grdFML.DataSource = dt;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    protected void grdFML_NeedDSource()
    {
        try
        {
            DataTable dt = dal.selectMenu();
            grdFML.DataSource = dt;
            grdFML.DataBind();

        }
        catch (Exception ex) { }
    }
    public string getParentMenuName(string menuID)
    {
        string rtn = string.Empty;
        rtn = dal.getMenuTitleByID(menuID);
        if(rtn =="0")
        {
            rtn = "";
        }
        return rtn;
    }
    public string GetChildMenu(string pmenuID)
    {
        StringBuilder tb = new StringBuilder();
        if (pmenuID != "0")
        {
            //var dt = dal.selectChildMenuByPID(pmenuID);
            DataTable dt = dal.selectChildMenuByPID(pmenuID);
            if (dt.Rows.Count > 0)
            {

                tb.Append("<table border='0' cellspacing='2' cellpadding='2'>");

                foreach (DataRow dr in dt.Rows)
                {
                    tb.Append("<tr>");
                    tb.Append("<td width='60px'>" + (dr["m_TitleEn"] != DBNull.Value ? dr["m_TitleEn"].ToString() : "") + "</td>");
                    tb.Append("<td><a href='MobileMenuConfiguration.aspx?id=" + (dr["m_id"] != DBNull.Value ? dr["m_id"].ToString() : "") + "'><i class='fa fa-pencil-square-o'></i></a></td>");
                    //tb.Append("<td vlign='top'><asp:LinkButton ID='lnkCEdit' runat='server' CommandName='Editspp' CommandArgument='" + d.m_id + "' onclick='lnkEdit_Onclick'><i class='fa fa-pencil-square-o'></i></asp:LinkButton></td>");
                    //tb.Append("<td vlign='top'><asp:LinkButton ID='lnkCEdit' runat='server' CommandName='Editspp' CommandArgument='" + d.m_id + "' onclick='lnkEdit_Onclick'><i class='fa fa-pencil-square-o'></i></asp:LinkButton></td>");
                    tb.Append("</tr>");
                }
                tb.Append("</table>");
            }
        }

        return tb.ToString();
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        string id = ((LinkButton)sender).CommandArgument;
        if (!string.IsNullOrEmpty(id))
        {
            Response.Redirect("MobileMenuConfiguration.aspx?id=" + id);
        }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        LinkButton del = sender as LinkButton; int chkFE = 0;
        if (del.CommandName == "Delete")
        {
            string ID = del.CommandArgument.ToString();
            //chkFE = dalf.CheckMenuUsage(ID);
            //if (chkFE == 0)
            {
                DataTable dtMenu = daldata.selectMenuByID(ID);
                if (dtMenu.Rows.Count > 0)
                {
                    string pageType = dtMenu.Rows[0]["m_PageType"] != DBNull.Value ? dtMenu.Rows[0]["m_PageType"].ToString() : "";
                    dal.updatePageTypeUsage(pageType);
                }
                int res = daldata.DeleteMenuByID(ID);
                if (res == 1)
                {
                    
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Success !";
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    msg.Text = "Fail !";
                }
            }
            //else
            //{
            //    noti.Visible = true;
            //    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            //    msg.Text = "This item is used in FrontEnd. CANNOT BE DELETED !";
            //}

            grdFML_NeedDSource();
        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            string ID = Request.QueryString["id"].ToString();
            //chkFE = dalf.CheckMenuUsage(ID);
            //if (chkFE == 0)
            {
                DataTable dtMenu = daldata.selectMenuByID(ID);
                if (dtMenu.Rows.Count > 0)
                {
                    string pageType = dtMenu.Rows[0]["m_PageType"] != DBNull.Value ? dtMenu.Rows[0]["m_PageType"].ToString() : "";
                    dal.updatePageTypeUsage(pageType);
                }
                int res = daldata.DeleteMenuByID(ID);
                if (res == 1)
                {

                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Success !";
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    msg.Text = "Fail !";
                }
            }
            //else
            //{
            //    noti.Visible = true;
            //    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            //    msg.Text = "This item is used in FrontEnd. CANNOT BE DELETED !";
            //}
            Response.Write("<script language='javascript'>alert('Deleted');window.location.href='MobileMenuConfiguration.aspx';</script>");
            Response.End();
        }
    }
}