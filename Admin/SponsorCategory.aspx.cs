﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class Admin_SponsorCategory : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblSponsorCategory";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdSponsorCategory.ExportSettings.IgnorePaging = true;
                grdSponsorCategory.ExportSettings.ExportOnlyData = true;
                grdSponsorCategory.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdSponsorCategory.ExportSettings.FileName = "SponsorCategory";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            //DataTable dtmenu = daldata.selectFloorplanMenu();

            //ddlmenu.DataSource = dtmenu;
            //ddlmenu.DataTextField = "m_TitleEn";
            //ddlmenu.DataValueField = "m_id";
            //ddlmenu.DataBind();
            //ListItem lst = new ListItem("Select Menu", "");
            //ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                lblerrName.Text = string.Empty;
                lblerrNo.Text = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("sponsorCat_ID");
                dt.Columns.Add("sponsorCat_name");
                dt.Columns.Add("sponsorCat_seq");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                string namestr = daldata.replaceForSQL(txtCategory.Text.Trim());
                int seqno = int.Parse(daldata.replaceForSQL(txtSeq.Text.Trim()));
                //string[] date = datestr.Split('/');
                //string[] monthArr = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                //int monthno = int.Parse(date[0]) - 1;
                //string day = date[1].TrimStart('0');
                //string month = monthArr[monthno];
                //string dayMonth = day + " " + month;
                //string year = date[2];
                //string imagename = "";
                //string filename = string.Empty;
                //string extension = string.Empty;
                //string path = Constant.filesavingurl;
                //string message = string.Empty;
                //if (fupForm.HasFile == true)
                //{
                //    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                //    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                //    imagename = UploadIcon(filename, extension, path, ref message);
                //}
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["sponsorCat_ID"] = hfid.Value.ToString();
                    dr["sponsorCat_name"] = namestr;
                    dr["sponsorCat_seq"] = seqno;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateSponsorCategory(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdSponsorCategory_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["sponsorCat_ID"] = "";
                    dr["sponsorCat_name"] = namestr;
                    dr["sponsorCat_seq"] = seqno;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveSponsorCategoryGetID(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdSponsorCategory_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }

    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdSponsorCategory.ExportSettings.ExportOnlyData = true;
        this.grdSponsorCategory.ExportSettings.IgnorePaging = true;
        this.grdSponsorCategory.ExportSettings.OpenInNewWindow = true;
        this.grdSponsorCategory.MasterTableView.ExportToExcel();

        grdSponsorCategory.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdSponsorCategory.ExportSettings.FileName = "SponsorCategory";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        string seqno = daldata.getLastSponsorCategorySequence();
        int no = int.Parse(seqno) + 1;
        txtSeq.Text = no.ToString();
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdSponsorCategory_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getSponsorCategory();
            grdSponsorCategory.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdSponsorCategory_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdSponsorCategory.CurrentPageIndex + 1;
    }
    protected void grdSponsorCategory_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getSponsorCategorybyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getSponsorCategory();
            }


            grdSponsorCategory.DataSource = dt;
            grdSponsorCategory.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdSponsorCategory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdSponsorCategory.ExportSettings.ExportOnlyData = true;
            this.grdSponsorCategory.ExportSettings.IgnorePaging = true;
            this.grdSponsorCategory.ExportSettings.OpenInNewWindow = true;
            this.grdSponsorCategory.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getSponsorCategorylstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["sponsorCat_ID"].ToString();
                    txtCategory.Text = dt.Rows[0]["sponsorCat_name"].ToString();
                    txtSeq.Text = dt.Rows[0]["sponsorCat_seq"].ToString();
                    //ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteSponsorCategoryByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdSponsorCategory_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdSponsorCategory_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }

    //private string UploadIcon(string filename, string extension, string path, ref string message)
    //{
    //    string imagefilename = string.Empty; string messages = string.Empty;
    //    try
    //    {
    //        if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
    //        {
    //            messages = "Invalid Image Type";
    //        }

    //        string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

    //        string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
    //        if (!System.IO.Directory.Exists(CreatePath))
    //        {
    //            System.IO.Directory.CreateDirectory(CreatePath);
    //        }

    //        string savePath = Server.MapPath((path) + fname);
    //        fupForm.SaveAs(savePath);
    //        imagefilename = fname;
    //    }
    //    catch (Exception ex) { }
    //    return imagefilename;
    //}


    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtCategory.Text = "";

        //ddlmenu.SelectedIndex = 0;
        //lblfloorplan.Text = "";
    }

    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtCategory.Text.Trim()) || string.IsNullOrWhiteSpace(txtCategory.Text.Trim()))
            {
                lblerrName.Text = "**Required"; lblerrName.Visible = true; rtn = false;
            }
            else
            { lblerrName.Visible = false; }

            if (string.IsNullOrEmpty(txtSeq.Text.Trim()) || string.IsNullOrWhiteSpace(txtSeq.Text.Trim()))
            {
                lblerrNo.Text = "**Required";
                lblerrNo.Visible = true;
                rtn = false;
            }
            else
            {

                string text = txtSeq.Text;

                DataTable dtseq = daldata.getSponsorCategoryAllSequence();
                int number;
                if (!int.TryParse(text, out number))
                {
                    lblerrNo.Text = "** Please specify number only";
                    lblerrNo.Visible = true; rtn = false;
                }
                else if (int.Parse(text) <= 0)
                {
                    lblerrNo.Text = "** Sequence start from '1'";
                    lblerrNo.Visible = true; rtn = false;
                }
                else
                {
                    if (dtseq.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtseq.Rows)
                        {
                            if (!string.IsNullOrEmpty(hfid.Value))
                            {
                                DataTable dt = daldata.getSponsorCategorylstByID(hfid.Value);
                                if (int.Parse(text) == int.Parse(dr["sponsorCat_seq"].ToString()) && text != dt.Rows[0]["sponsorCat_seq"].ToString())
                                {
                                    lblerrNo.Text = "Sequence No. is already exist.";
                                    lblerrNo.Visible = true; rtn = false;
                                }
                            }else
                            {
                                if (int.Parse(text) == int.Parse(dr["sponsorCat_seq"].ToString()))
                                {
                                    lblerrNo.Text = "Sequence No. is already exist.";
                                    lblerrNo.Visible = true; rtn = false;
                                }
                            }

                        }
                    }

                }

            }

        }
        catch (Exception ex) { }
        return rtn;

    }
}