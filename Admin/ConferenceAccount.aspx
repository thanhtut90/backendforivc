﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="ConferenceAccount.aspx.cs" Inherits="Admin_ConferenceAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdfloorplan" />
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Conference Account</h3>
                    <div style="float:right">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>
                </div>
                <div class="box-body">
                    <div id="divsearch" runat="server" class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                            <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />
                        </div>
                        <div class="col-md-6">
                            <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                    </div>
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdConferenceAccount"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdConferenceAccount_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdConferenceAccount_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdConferenceAccount_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="caccID" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true"/><Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                               <%# Container.ItemIndex + 1  %>
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <%--<telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("caccID") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("caccID") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>--%>
                                            <telerik:GridBoundColumn  DataField="c_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Conference" SortExpression="c_title" UniqueName="c_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="a_fullname" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Account" SortExpression="a_fullname" UniqueName="a_fullname" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="a_type" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Account Type" SortExpression="a_type" UniqueName="a_type" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <%--<telerik:GridBoundColumn  DataField="sorting" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Sequence" SortExpression="sorting" UniqueName="sorting" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>--%>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <asp:HiddenField ID="hfid" runat="server" />
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Conference</label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlConference" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:Label ID="lblerrConference" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Choose Account Type</label>
                                <div class="col-sm-6">
                                    <asp:DropDownList ID="ddlType" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="Speaker" Value="S"></asp:ListItem>
                                        <asp:ListItem Text="Delegate/Participant" Value="D"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="lblerrType" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Account</label>
                                <div class="col-sm-6">
                                    <asp:ListBox ID="lstAccount" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>
                                    <asp:Label ID="lblerrAccount" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Sequence</label>
                                <div class="col-sm-6">
                                <asp:TextBox ID="txtSeq" class="form-control" TextMode="Number" runat="server"></asp:TextBox>
                                </div>
                            </div>--%>
                        </div>
                        <div class="box-footer">
                           <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
</asp:Content>

