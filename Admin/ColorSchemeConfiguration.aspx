﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="ColorSchemeConfiguration.aspx.cs" Inherits="Admin_ColorScheme" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Color Scheme Configuration Form</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                  <div class="form-group">                      
                      <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                      </div>
                    </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">ColorScheme Name</label>
                    <div class="col-sm-5">
                    <asp:TextBox ID="csname" class="form-control" placeholder="Enter Name" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1">
                        
                    </div>
                </div> 
                                    
                <div class="form-group">
                <label class="col-sm-2 control-label">1st Background Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="bg1c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="bg1t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="Button5" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender4" TargetControlID="bg1c" PopupButtonID="Button5" PopupPosition="TopRight" SampleControlID="bg1c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div> 

                <div class="form-group">
                <label class="col-sm-2 control-label">2nd Background Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="bg2c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="bg2t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton1" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender1" TargetControlID="bg2c" PopupButtonID="LinkButton1" PopupPosition="TopRight" SampleControlID="bg2c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">1st Font Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="fc1c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="fc1t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton2" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender2" TargetControlID="fc1c" PopupButtonID="LinkButton2" PopupPosition="TopRight" SampleControlID="fc1c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">2nd Font Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="fc2c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="fc2t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton3" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender3" TargetControlID="fc2c" PopupButtonID="LinkButton3" PopupPosition="TopRight" SampleControlID="fc2c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">3rd Font Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="fc3c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="fc3t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton4" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender5" TargetControlID="fc3c" PopupButtonID="LinkButton4" PopupPosition="TopRight" SampleControlID="fc3c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">1st Button Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="bc1c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="bc1t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton5" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender6" TargetControlID="bc1c" PopupButtonID="LinkButton5" PopupPosition="TopRight" SampleControlID="bc1c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">2nd Button Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="bc2c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="bc2t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton6" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender7" TargetControlID="bc2c" PopupButtonID="Button5" PopupPosition="TopRight" SampleControlID="bc2c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                
                <div class="form-group">
                <label class="col-sm-2 control-label">1st Margin Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="mc1c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="mc1t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton9" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender10" TargetControlID="mc1c" PopupButtonID="LinkButton9" PopupPosition="TopRight" SampleControlID="mc1c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">2nd Margin Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="mc2c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="mc2t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton10" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender11" TargetControlID="mc2c" PopupButtonID="LinkButton10" PopupPosition="TopRight" SampleControlID="mc2c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                  <div class="form-group">
                <label class="col-sm-2 control-label">1st Column Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="cc1c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="cc1t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton7" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender8" TargetControlID="cc1c" PopupButtonID="LinkButton7" PopupPosition="TopRight" SampleControlID="cc1c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                <div class="form-group">
                <label class="col-sm-2 control-label">2nd Column Color:</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="cc2c" placeholder="Enter Color" class="form-control" runat="server" ></asp:TextBox>                          
                        <br />
                        <asp:TextBox ID="cc2t" placeholder="Enter Transparency" class="form-control" runat="server" ></asp:TextBox>                            
                    </div>
                    <div class="col-sm-2">
                    <asp:LinkButton ID="LinkButton8" Enabled="false" runat="server"><span class="fa fa-eyedropper">&nbsp; Pick Color</span></asp:LinkButton>
                    <cc1:ColorPickerExtender ID="ColorPickerExtender9" TargetControlID="cc2c" PopupButtonID="LinkButton8" PopupPosition="TopRight" SampleControlID="cc2c" Enabled="True" runat="server"></cc1:ColorPickerExtender>
                    </div>
                </div>
                  </div>
              <!-- /.box-body -->
              <div class="box-footer">                                
                <asp:Button ID="btnSubmit" onclick="Button1_Click" class="btn btn-info" runat="server" Text="Submit" />
              </div>
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>

