﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddCoordinator.aspx.cs" Inherits="Admin_AddCoordinator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link rel="stylesheet" href="../Content/css/jquery.Jcrop.min.css" type="text/css" />
    <script src="../Content/js/jquery.min.js"></script>
    <script src="../Content/js/jquery.Jcrop.min.js"></script>
<style type="text/css">
    .button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 3px 20px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 14px;
    margin: 2px 1px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.4s;
    cursor: pointer;
}

.button1 {
    background-color: white;
    color: #788898;
    border: 2px solid #555555;
}



</style>
    <script type="text/javascript">
    jQuery(function ($) {

        $('#target').Jcrop({
            onChange: showCoords,
            onSelect: showCoords
        });

    });

    function showCoords(c) {
        $('#x').val(c.x);
        $('#y').val(c.y);
        $('#w').val(c.w);
        $('#h').val(c.h);
    };
</script>
</head>
<body>
    <form id="form1" runat="server">
        <br /><br /><br />
        <div>
           <img style="margin-top: 5vh" src="<%=profileimg %>" id="target" /></div>
<div style="position:fixed;top:2px;left:0px;right:0px;height:70px;background-color:lightslategrey;display:inline-block;" class="jc_coords">
    <!-- <form onsubmit="return false;"> -->
      <div class="row">
          <div class="form-group col-sm-10">
              <br />
               <div style="float:left;width:5%">
                  <asp:Label ID="hello" runat="server" ForeColor="#788898">Hi</asp:Label>
                   </div>
               <div style="float:left">
                  <label for="x">X</label>
                  <asp:TextBox ID="x" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
              <div style="float:left">
                  <label for="x">Y</label>
                  <asp:TextBox ID="y" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
             <div style="float:left">
                  <label for="x">W</label>
                  <asp:TextBox ID="w" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
              <div style="float:left;width:12%">
                  <label for="x">H</label>
                  <asp:TextBox ID="h" runat="server" CssClass="form-control"></asp:TextBox>
              </div>
                 <div style="float:left;width:6%">
               <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save"  CssClass="button button1" />
                   </div>
               <div style="float:left;width:8%">
              <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" CssClass="button button1" />
                  
           
              </div>
               <div style="float:left;width:10%">
                  <asp:Label ID="lblerr" runat="server" ForeColor="Red" Visible="false">* Required</asp:Label>
                   </div>
          </div>
   
          

    <!-- </form> -->
    

</div>
    </form>
</body>
</html>
