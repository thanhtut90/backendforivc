﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class Admin_TimeList : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblTimeList";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdTimeList.ExportSettings.IgnorePaging = true;
                grdTimeList.ExportSettings.ExportOnlyData = true;
                grdTimeList.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdTimeList.ExportSettings.FileName = "TimeList";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            //DataTable dtmenu = daldata.selectFloorplanMenu();

            //ddlmenu.DataSource = dtmenu;
            //ddlmenu.DataTextField = "m_TitleEn";
            //ddlmenu.DataValueField = "m_id";
            //ddlmenu.DataBind();
            //ListItem lst = new ListItem("Select Menu", "");
            //ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                lblerr.Text = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("tl_ID");
                dt.Columns.Add("tl_slot");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                string datestr = daldata.replaceForSQL(txtNewstypeName.Text.Trim());
                //string[] date = datestr.Split('/');
                //string[] monthArr = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                //int monthno = int.Parse(date[0]) - 1;
                //string day = date[1].TrimStart('0');
                //string month = monthArr[monthno];
                //string dayMonth = day + " " + month;
                //string year = date[2];
                //string imagename = "";
                //string filename = string.Empty;
                //string extension = string.Empty;
                //string path = Constant.filesavingurl;
                //string message = string.Empty;
                //if (fupForm.HasFile == true)
                //{
                //    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                //    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                //    imagename = UploadIcon(filename, extension, path, ref message);
                //}
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["tl_ID"] = hfid.Value.ToString();
                    dr["tl_slot"] = datestr;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateTimeList(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdTimeList_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["tl_ID"] = "";
                    dr["tl_slot"] = datestr;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveTimeListGetID(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdTimeList_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
            else
            {
                lblerr.Text = "**Required";
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtNewstypeName.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdTimeList.ExportSettings.ExportOnlyData = true;
        this.grdTimeList.ExportSettings.IgnorePaging = true;
        this.grdTimeList.ExportSettings.OpenInNewWindow = true;
        this.grdTimeList.MasterTableView.ExportToExcel();

        grdTimeList.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdTimeList.ExportSettings.FileName = "TimeList";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdTimeList_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getTimeList();
            grdTimeList.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdTimeList_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdTimeList.CurrentPageIndex + 1;
    }
    protected void grdTimeList_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getTimeListbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getTimeList();
            }


            grdTimeList.DataSource = dt;
            grdTimeList.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdTimeList_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdTimeList.ExportSettings.ExportOnlyData = true;
            this.grdTimeList.ExportSettings.IgnorePaging = true;
            this.grdTimeList.ExportSettings.OpenInNewWindow = true;
            this.grdTimeList.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getTimeListlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["tl_ID"].ToString();
                    txtNewstypeName.Text = dt.Rows[0]["tl_slot"].ToString();
                    //ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteTimeListByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdTimeList_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdTimeList_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    //private string UploadIcon(string filename, string extension, string path, ref string message)
    //{
    //    string imagefilename = string.Empty; string messages = string.Empty;
    //    try
    //    {
    //        if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
    //        {
    //            messages = "Invalid Image Type";
    //        }

    //        string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

    //        string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
    //        if (!System.IO.Directory.Exists(CreatePath))
    //        {
    //            System.IO.Directory.CreateDirectory(CreatePath);
    //        }

    //        string savePath = Server.MapPath((path) + fname);
    //        fupForm.SaveAs(savePath);
    //        imagefilename = fname;
    //    }
    //    catch (Exception ex) { }
    //    return imagefilename;
    //}
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtNewstypeName.Text = "";
        //ddlmenu.SelectedIndex = 0;
        //lblfloorplan.Text = "";
    }
}