﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="MobileSystemConfigaspx.aspx.cs" Inherits="Admin_MobileSystemConfigaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Mobile App Configuration Form</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                  <div class="form-group">                      
                      <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                      </div>
                    </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtname" class="form-control" placeholder="Enter Name" runat="server"></asp:TextBox>
                    </div>
                </div>                   
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Duration:</label>
                      <div class="col-sm-6">
                            <div class="input-group" runat="server" id="duration">
                              <div class="input-group-addon">
                                <i class="fa fa-clock-o"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="reservationtime" name="n_test" >                                
                            </div>
                          <asp:Label ID="daterange" class="form-control" visible="false" runat="server"></asp:Label>
                          <asp:LinkButton ID="lnkreset" Visible="false" OnClick="lnkreset_Click" runat="server">Reset</asp:LinkButton>
                          <label id="lblerr" style="color:red" runat="server" visible="false">* Please Set Duration</label>
                      </div>
                  </div> 
                  <div class="form-group" runat="server">
                      <label for="inputPassword3" class="col-sm-2 control-label">Enable Language</label>
                      <div class="col-sm-6">
                        <asp:RadioButtonList ID="chkLang" runat="server"></asp:RadioButtonList>
                      </div>
                    </div> 
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Enable Account</label>
                      <div class="col-sm-6">
                          <asp:CheckBoxList ID="chkenableAccount" runat="server">
                              <asp:ListItem Value="Visitor">&nbsp;Visitor</asp:ListItem>
                              <asp:ListItem Value="Delegate">&nbsp;Delegate</asp:ListItem>
                              <asp:ListItem Value="Exhibitor">&nbsp;Exhibitor</asp:ListItem>
                              <asp:ListItem Value="Guest">&nbsp;Guest</asp:ListItem>
                          </asp:CheckBoxList>
                      </div>
                   </div>
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">App Key (Dev)</label>
                        <div class="col-sm-2">
                        <asp:TextBox ID="txtdappkey" class="form-control"  placeholder="Enter App Key" runat="server"></asp:TextBox>
                      </div>
                      <label for="inputPassword2" class="col-sm-2 control-label">Master Secret (Dev)</label>
                      <div class="col-sm-2">
                        <asp:TextBox ID="txtdmastersecret" class="form-control"  placeholder="Enter Master Secret" runat="server"></asp:TextBox>
                      </div>
                    </div> 
                  <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">App Key (Producttion)</label>
                        <div class="col-sm-2">
                        <asp:TextBox ID="txtpappkey" class="form-control"  placeholder="Enter App Key" runat="server"></asp:TextBox>
                      </div>
                      <label for="inputPassword2" class="col-sm-2 control-label">Master Secret (Producttion)</label>
                      <div class="col-sm-2">
                        <asp:TextBox ID="txtpmastersecret" class="form-control"  placeholder="Enter Master Secret" runat="server"></asp:TextBox>
                      </div>
                    </div> 
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">App Icon (1024 x 1024)</label>
                      <div class="col-sm-6">                       
                        <asp:Label ID="lblappicon" class="form-control" runat="server"></asp:Label>
                       <asp:FileUpload ID="fupIcon" runat="server" />
                      </div>
                    </div>
                   
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Location</label>
                      <div class="col-sm-6">
                       <asp:TextBox ID="txtvenue" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                      </div>
                   </div>
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">Start Date (Survey)</label>
                      <div class="col-sm-6">
                      <div class="input-group date" id="ssdate" runat="server">
                        <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="sdatepicker" name="sdate">
                      </div>
                          <asp:Label ID="lblss" class="form-control" visible="false" runat="server"></asp:Label>
                          <asp:LinkButton ID="lnkresetss" Visible="false" OnClick="lnkresetss_Click" runat="server">Reset</asp:LinkButton>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-2 control-label">End Date (Survey)</label>
                      <div class="col-sm-6">
                       <div class="input-group date" id="sedate" runat="server">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control pull-right" id="edatepicker" name="edate">
                       </div>
                    <asp:Label ID="lbles" class="form-control" visible="false" runat="server"></asp:Label>
                    <asp:LinkButton ID="lnkresetes" Visible="false" OnClick="lnkresetes_Click" runat="server">Reset</asp:LinkButton>
                      </div>
                    </div>                   
                  <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Visitor Registration URL</label>
                      <div class="col-sm-6">
                        <asp:TextBox ID="txtvregurl" class="form-control"  placeholder="Enter Visitor Registration URL" runat="server"></asp:TextBox>
                      </div>
                    </div>
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Home Page Layout</label>
                      <div class="col-sm-6">
                          <asp:RadioButtonList ID="rdolayout" runat="server">
                              <asp:ListItem Value="1">&nbsp;Quick Menu</asp:ListItem>
                              <asp:ListItem Value="2">&nbsp;Button Menu</asp:ListItem>
                          </asp:RadioButtonList>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">iOs Version & Download Link</label>
                      <div class="col-sm-1">
                        <asp:TextBox ID="txtiVersion" class="form-control"  placeholder="Version" runat="server"></asp:TextBox>
                      </div>
                      <div class="col-sm-5">
                        <asp:TextBox ID="txtidownloadlink" class="form-control"  placeholder="Enter Download Link" runat="server"></asp:TextBox>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Android Version & Download Link</label>
                        <div class="col-sm-1">
                        <asp:TextBox ID="txtaVersion" class="form-control"  placeholder="Version" runat="server"></asp:TextBox>
                      </div>
                      <div class="col-sm-5">
                        <asp:TextBox ID="txtadownloadlink" class="form-control"  placeholder="Enter Download Link" runat="server"></asp:TextBox>
                      </div>
                    </div>
                  </div>
              <!-- /.box-body -->
              <div class="box-footer">                                
                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
              </div>
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>

