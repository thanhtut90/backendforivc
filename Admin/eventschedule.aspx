﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="eventschedule.aspx.cs" Inherits="Admin_eventschedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdEvent" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">                      
                        <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>   
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Event Schedule</h3>
                    <div style="float:right">                            
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>                        
                </div>                                       
                <div class="box-body"> 
                    <div id="divsearch" runat="server" class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                        <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Event Title" runat="server"></asp:TextBox>                    
                        </div>
                        <div class="col-md-1">                        
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />   
                        </div>
                        <div class="col-md-6">
                        <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                        
                    </div>  
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdEvent"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdEvent_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdEvent_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdEvent_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="e_ID" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true"/>
                                        <Columns> 
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                               <%# Container.ItemIndex + 1  %>  
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn>     
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">                                    
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("e_ID") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>                                    
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("e_ID") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>                                                                                                           
                                                </ItemTemplate>                     
                                            </telerik:GridTemplateColumn> 
                                            <telerik:GridBoundColumn  DataField="e_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Event Title" SortExpression="e_title" UniqueName="e_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>  
                                            <telerik:GridBoundColumn  DataField="e_date" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Date" SortExpression="e_date" UniqueName="e_date" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="e_time" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Time" SortExpression="e_time" UniqueName="e_time" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="e_venue" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Venue(Booth)" SortExpression="e_venue" UniqueName="e_venue" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="e_eventcategory" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Event Category" SortExpression="e_eventcategory" UniqueName="e_eventcategory" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="e_brief" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Brief" SortExpression="e_brief" UniqueName="e_brief" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="Image" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Image" SortExpression="Image" UniqueName="Image" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">                            
                            <div class="form-group">                               
                                <label for="inputEmail3" class="col-sm-1 control-label">Date</label>
                                <div class="col-sm-3">
                                <asp:TextBox ID="txtdate" class="form-control"  placeholder="Enter Date" runat="server"></asp:TextBox>
                                </div>
                                <label for="inputEmail3" class="col-sm-1 control-label">Time</label>
                                <div class="col-sm-3">
                                <asp:TextBox ID="txttime" class="form-control"  placeholder="0000-0000" runat="server"></asp:TextBox>
                                </div>
                                <label for="inputEmail3" class="col-sm-1 control-label">Venue</label>
                                <div class="col-sm-3">
                                <asp:DropDownList ID="ddlvenue" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                                
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Event</label>
                                <div class="col-sm-6"><asp:HiddenField ID="hfid" runat="server" />
                                <asp:TextBox ID="txtEvent" class="form-control"  placeholder="Enter Event" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Event Category</label>
                                <div class="col-sm-6"><asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:TextBox ID="txtEventCategory" class="form-control"  placeholder="Enter Event Category" runat="server"></asp:TextBox>
                                </div>
                            </div>
                           <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
                                <div class="col-sm-3">
                                <asp:FileUpload ID="fupForm" runat="server" /><asp:Label ID="lblicon" runat="server"></asp:Label>
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Brief</label>
                                <div class="col-sm-6">
                                <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtbrief" SkinID="DefaultSetOfTools" Width="90%">                                   
                                        <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            EnableAsyncUpload="true"></ImageManager>
                                        <Tools>
                                            <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="JustifyLeft" />
                                            <telerik:EditorTool Name="JustifyCenter" />
                                            <telerik:EditorTool Name="JustifyRight" />
                                            <telerik:EditorTool Name="JustifyFull" />
                                        </telerik:EditorToolGroup>
                                           
                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="Bold" />
                                            <telerik:EditorTool Name="Underline" />
                                            <telerik:EditorTool Name="Italic" />
                                        </telerik:EditorToolGroup>

                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="InsertTable"/>
                                        </telerik:EditorToolGroup>
                                        </Tools>
                                    </telerik:RadEditor>    
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">                                
                           <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
</asp:Content>

