﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using System.Collections.Generic;
using System.Net;
using RestSharp;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;
using System.Web.Script.Serialization;

public partial class Admin_ConferenceAccount : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblConferenceAccount";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();
                bindAccount();

                grdConferenceAccount.ExportSettings.IgnorePaging = true;
                grdConferenceAccount.ExportSettings.ExportOnlyData = true;
                grdConferenceAccount.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdConferenceAccount.ExportSettings.FileName = "ConferenceAccount";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtConf = getConference();
            for (int x = 0; x < dtConf.Rows.Count; x++)
            {
                ddlConference.Items.Add(dtConf.Rows[x]["c_title"].ToString() + "[" + dtConf.Rows[x]["c_date"] + "-" + dtConf.Rows[x]["c_stime"] + "-" + dtConf.Rows[x]["c_etime"] + "]");
                ddlConference.Items[x].Value = dtConf.Rows[x]["c_ID"].ToString();
            }
            //ddlConference.DataSource = dtConf;
            //ddlConference.DataTextField = "c_title";
            //ddlConference.DataValueField = "c_ID";
            //ddlConference.DataBind();
            ListItem lstConf = new ListItem("Select Conference", "");
            ddlConference.Items.Insert(ddlConference.Items.Count - ddlConference.Items.Count, lstConf);
        }
        catch (Exception ex) { }
    }
    private void bindAccount()
    {DataTable dtAccount = new DataTable();
        dtAccount = getAccount(ddlType.SelectedItem.Value);
        lstAccount.DataSource = dtAccount;
        lstAccount.DataTextField = "a_fullname";
        lstAccount.DataValueField = "a_ID";
        lstAccount.DataBind();
        //ListItem lstAcc = new ListItem("Select Account", "");
        //lstAccount.Items.Insert(lstAccount.Items.Count - lstAccount.Items.Count, lstAcc);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string selectedAccounts = "";
            string selectedAccountTypes = "";
            if (Validation(ref selectedAccounts, ref selectedAccountTypes))
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("confaccount_ID");
                dt.Columns.Add("confaccount_ConferenceID");
                dt.Columns.Add("confaccount_AccountID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("sorting");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    #region update (Comment update into tblConferenceAccount table on 28-4-2020 th according to Sithu)
                    ////Update
                    //DataRow dr = dt.NewRow();
                    //dr["confaccount_ID"] = hfid.Value.ToString();
                    //dr["confaccount_ConferenceID"] = ddlConference.SelectedValue.ToString();
                    //dr["confaccount_AccountID"] = selectedAccounts;
                    //dr["lang"] = "1";
                    //dr["deleteFlag"] = false;
                    //dr["UpdatedDate"] = DateTime.Now;
                    //dr["sorting"] = !string.IsNullOrEmpty(txtSeq.Text) && !string.IsNullOrWhiteSpace(txtSeq.Text) ? txtSeq.Text.Trim() : "99";
                    //int res = daldata.UpdateConferenceAccount(dr);
                    //if (res == 1)
                    //{
                    //    daldata.UpdateDataVersion(tablename);
                    //    //#region call zoom api
                    //    //string errMsg = "";
                    //    //UpdateDataAPI(ddlConference.SelectedValue.ToString(), selectedAccounts, ref errMsg);
                    //    //if (!string.IsNullOrEmpty(errMsg))
                    //    //{
                    //    //    noti.Visible = true;
                    //    //    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    //    //    msg.Text = "Update API error!=>" + errMsg;
                    //    //}
                    //    //#endregion
                    //    //else
                    //    {
                    //        grdConferenceAccount_NeedDSource("");
                    //        divadd.Visible = false;
                    //        divsearch.Visible = true;
                    //        divlist.Visible = true;
                    //        noti.Visible = true;
                    //        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    //        msg.Text = "Update Success !";

                    //        clearControls();
                    //    }
                    //}
                    #endregion
                }
                else
                {
                    #region insert (Comment insert into tblConferenceAccount table according to Sithu)
                    ////Insert
                    //DataRow dr = dt.NewRow();
                    //dr["confaccount_ID"] = "";
                    //dr["confaccount_ConferenceID"] = ddlConference.SelectedValue.ToString();
                    //dr["confaccount_AccountID"] = selectedAccounts;
                    //dr["lang"] = "1";
                    //dr["deleteFlag"] = false;
                    //dr["UpdatedDate"] = DateTime.Now;
                    //dr["sorting"] = !string.IsNullOrEmpty(txtSeq.Text) && !string.IsNullOrWhiteSpace(txtSeq.Text) ? txtSeq.Text.Trim() : "99";
                    //string confaccountID = "";
                    //int res = daldata.SaveConferenceAccount(dr, ref confaccountID);
                    //if (res == 1)
                    //{
                    //    daldata.UpdateDataVersion(tablename);
                    #region call zoom api
                    string errMsg = "";
                        SaveDataAPI(ddlConference.SelectedValue.ToString(), selectedAccounts, selectedAccountTypes, ref errMsg);
                        if (!string.IsNullOrEmpty(errMsg))
                        {
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Saving API error!=>" + errMsg;
                            //int res1 = daldata.DeleteConferenceAccountByID(confaccountID);
                        }
                        #endregion
                        else
                        {
                            grdConferenceAccount_NeedDSource("");
                            divadd.Visible = false;
                            divsearch.Visible = true;
                            divlist.Visible = true;
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Save Success !";

                            clearControls();
                        }
                    //}
                    #endregion
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation(ref string selectedAccounts, ref string selectedAccountTypes)
    {
        bool rtn = true;
        try
        {
            if (ddlConference.SelectedItem.Value == "")
            {
                lblerrConference.Visible = true;
                rtn = false;
            }

            StringBuilder sb = new StringBuilder();
            //StringBuilder sbType = new StringBuilder();
            int selectCount = 0;
            foreach (ListItem item in lstAccount.Items)
            {
                if (item.Selected == true)
                {
                    string[] str = item.Value.ToString().Split(';');
                    if (str.Length > 0)
                    {
                        selectCount++;
                        sb.Append(str[0]);
                        sb.Append(",");
                        #region no use
                        //sbType.Append(str[1]);
                        ////if (sbType.ToString().Contains(str[1]))
                        ////{ }
                        ////else
                        ////{
                        ////    //////sbType.Replace(str[1], str[1]);
                        ////    sbType.Append(str[1]);
                        ////    sbType.Append(",");
                        ////}
                        #endregion
                    }
                }
            }
            selectedAccounts = sb.ToString().TrimEnd(',');
            selectedAccountTypes = ddlType.SelectedItem.Value;//sbType.ToString().TrimEnd(',');
            if (selectCount <= 0 )
            {
                lblerrAccount.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdConferenceAccount.ExportSettings.ExportOnlyData = true;
        this.grdConferenceAccount.ExportSettings.IgnorePaging = true;
        this.grdConferenceAccount.ExportSettings.OpenInNewWindow = true;
        this.grdConferenceAccount.MasterTableView.ExportToExcel();

        grdConferenceAccount.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdConferenceAccount.ExportSettings.FileName = "ConferenceAccount";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdConferenceAccount_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = getGrantAccess();//daldata.getConferenceAccount();
            grdConferenceAccount.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceAccount_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdConferenceAccount.CurrentPageIndex + 1;
    }
    protected void grdConferenceAccount_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = getGrantAccessbyKeywords(searchkey);//daldata.getConferenceAccountbyKeywords(searchkey);
            }
            else
            {
                dt = getGrantAccess();//daldata.getConferenceAccount();
            }

            grdConferenceAccount.DataSource = dt;
            grdConferenceAccount.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceAccount_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdConferenceAccount.ExportSettings.ExportOnlyData = true;
            this.grdConferenceAccount.ExportSettings.IgnorePaging = true;
            this.grdConferenceAccount.ExportSettings.OpenInNewWindow = true;
            this.grdConferenceAccount.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getConferenceAccountlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["confaccount_ID"].ToString();
                    string confID = dt.Rows[0]["confaccount_ConferenceID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(confID))
                        {
                            ListItem listItem = ddlConference.Items.FindByValue(confID);
                            if (listItem != null)
                            {
                                ddlConference.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string accID = dt.Rows[0]["confaccount_AccountID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(accID))
                        {
                            string[] strArray = accID.Split(',');
                            List<string> list = new List<string>(strArray);
                            foreach (ListItem item in lstAccount.Items)
                            {
                                if (list.Contains(item.Value))
                                    item.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    //txtSeq.Text = dt.Rows[0]["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dt.Rows[0]["sorting"].ToString()) ? dt.Rows[0]["sorting"].ToString() : "99") : "99";
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteConferenceAccountByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdConferenceAccount_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdConferenceAccount_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        ddlConference.SelectedIndex = 0;
        lstAccount.SelectedIndex = 0;
        setInvisibleErrMsg();
        //txtSeq.Text = "";
    }
    private void setInvisibleErrMsg()
    {
        lblerrConference.Visible = false;
        lblerrAccount.Visible = false;
    }

    #region API call
    private void SaveDataAPI(string mobconfID, string accountIDs, string accountTypes, ref string errMsg)
    {
        errMsg = "";
        string accessToken = HttpGetToken("http://203.127.83.146/iVC_API/api/token", ref errMsg);
        if (!string.IsNullOrEmpty(errMsg))
        {
            //Response.Write(errMsg);
            //Response.End();
        }
        else
        {
            try
            {
                daldata.saveToken(accessToken);

                errMsg = "";
                string saveURI = "http://203.127.83.146/iVC_API/api/ConferencePermission";
                errMsg = HttpPostWithAuthSaveConference(saveURI, accessToken, mobconfID, accountIDs, accountTypes);
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                //Response.Write(ex.Message);
                //Response.End();
            }
        }
    }
    public string HttpGetToken(string URI, ref string errorMessage)
    {
        string json = "";
        try
        {
            var client = new RestClient(URI);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddParameter("text/plain", "{\n    \"username\": \"stw\",\n    \"password\": \"admin\",\n    \"LoginType\": 0\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
            string returnJson = response.Content;
            //// parse the json response so that we can get at the key/value pairs
            //dynamic api = JObject.Parse(returnJson);
            //// Parsing JSON content into element-node JObject
            var jObject = JObject.Parse(returnJson);
            //if (jObject["data"].Count() > 0)
            {
                var jObject1 = JObject.Parse(jObject["data"].ToString());
                var accessToken = Convert.ToString(jObject1["access_token"]);
                json = accessToken;
            }

            return json;
        }
        catch (Exception ex)
        {
            if (ex.GetType().Name == "WebException")
            {
                errorMessage = ex.Message;
            }
            else
            {
            }
            return "";
        }
    }
    public string HttpPostWithAuthSaveConference(string URI, string accessToken, string mobconfID, string accountIDs, string accountTypes)
    {
        string errorMessage = "";

        if (string.IsNullOrWhiteSpace(accessToken))
        {
            errorMessage = "Please ensure that the access token is not empty!";
        }
        else
        {
            try
            {
                string[] str = accountIDs.Split(',');
                //string[] strType = accountTypes.Split(',');
                Hashtable ht = new Hashtable();
                ht.Add("cid", mobconfID);
                ht.Add("users", str);
                ht.Add("userType", accountTypes);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                String jsonStr = ser.Serialize(ht);

                var client = new RestClient(URI);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + accessToken);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonStr, ParameterType.RequestBody);//"{\r\n\t\"cid\":\"" + mobconfID + "\",\r\n\t\"users\":\"" + jsonAccountIDs + "\"\r\n\t\r\n}"

                IRestResponse response = client.Execute(request);
                string returnJson = response.Content;
                //// parse the json response so that we can get at the key/value pairs
                //dynamic api = JObject.Parse(returnJson);
                //// grab the values and do your assertions :-)
                var jObject = JObject.Parse(returnJson);
                var status = Convert.ToString(jObject["success"].ToString());
                var data = Convert.ToString(jObject["data"].ToString());
                var message = Convert.ToString(jObject["message"].ToString());
                if (status == "True")
                {
                }
                else
                {
                    errorMessage = message;//"API calling return error!";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "WebException")
                {
                    var response = (HttpWebResponse)((ex as WebException).Response);

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new WebException();
                    }
                }
                else
                {
                    errorMessage = ex.Message;
                }
            }
        }

        return errorMessage;
    }
    private void UpdateDataAPI(string mobconfID, string accountIDs, ref string errMsg)
    {
        errMsg = "";
        string accessToken = HttpGetToken("http://203.127.83.146/iVC_API/api/token", ref errMsg);
        if (!string.IsNullOrEmpty(errMsg))
        {
            //Response.Write(errMsg);
            //Response.End();
        }
        else
        {
            try
            {
                daldata.saveToken(accessToken);

                errMsg = "";
                string saveURI = "http://203.127.83.146/iVC_API/api/ConferencePermission";
                errMsg = HttpPostWithAuthUpdateConference(saveURI, accessToken, mobconfID, accountIDs);
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                //Response.Write(ex.Message);
                //Response.End();
            }
        }
    }
    public string HttpPostWithAuthUpdateConference(string URI, string accessToken, string mobconfID, string accountIDs)
    {
        string errorMessage = "";

        if (string.IsNullOrWhiteSpace(accessToken))
        {
            errorMessage = "Please ensure that the access token is not empty!";
        }
        else
        {
            try
            {
                string[] str = accountIDs.Split(',');
                Hashtable ht = new Hashtable();
                ht.Add("cid", mobconfID);
                ht.Add("users", str);
                JavaScriptSerializer ser = new JavaScriptSerializer();
                String jsonStr = ser.Serialize(ht);

                var jsonAccountIDs = JsonConvert.SerializeObject(accountIDs);//////***
                var client = new RestClient(URI);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + accessToken);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonStr, ParameterType.RequestBody);// "{\r\n\t\"cid\":\"" + mobconfID + "\",\r\n\t\"users\":\"" + jsonAccountIDs + "\"\r\n\t\r\n}"
                IRestResponse response = client.Execute(request);
                string returnJson = response.Content;
                //// parse the json response so that we can get at the key/value pairs
                //dynamic api = JObject.Parse(returnJson);
                //// grab the values and do your assertions :-)
                var jObject = JObject.Parse(returnJson);
                var status = Convert.ToString(jObject["success"].ToString());
                var data = Convert.ToString(jObject["data"].ToString());
                var message = Convert.ToString(jObject["message"].ToString());
                if (status == "True")
                {
                }
                else
                {
                    errorMessage = message;//"API calling return error!";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "WebException")
                {
                    var response = (HttpWebResponse)((ex as WebException).Response);

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new WebException();
                    }
                }
                else
                {
                    errorMessage = ex.Message;
                }
            }
        }

        return errorMessage;
    }
    #endregion

    #region get list
    public DataTable getGrantAccess()
    {
        DataTable dt = new DataTable();
        try
        {
            Functionality fn = new Functionality();
            dt = fn.GetDatasetByCommand("Select cu.Id as caccID,c.c_title as c_title,a.a_fullname as a_fullname,a.a_type as a_type From tbl_ConferenceUser as cu "
                                    + " Left Join tblAccount as a On cu.UserId=a.a_ID "
                                    + " Left Join tblConference as c On cu.WebinarId=c.c_ID "
                                    + " where c.deleteFlag=0 And a.deleteFlag= 0 "
                                    + " Order By c.c_title"
                                    , "sdt").Tables[0];
        }
        catch (Exception ex) { }
        return dt;
    }
    public DataTable getGrantAccessbyKeywords(string searchkey)
    {
        DataTable dt = new DataTable();
        try
        {
            Functionality fn = new Functionality();
            var sql = "Select cu.Id as caccID,c.c_title as c_title,a.a_fullname as a_fullname,a.a_type as a_type From tbl_ConferenceUser as cu "
                    + " Left Join tblAccount as a On cu.UserId=a.a_ID "
                    + " Left Join tblConference as c On cu.WebinarId=c.c_ID "
                    + " where c.deleteFlag=0 And a.deleteFlag= 0 "
                    + " And (c.c_title Like '%" + searchkey + "%' Or a.a_fullname Like '%" + searchkey + "%')"
                    + " Order By c.c_title";
            dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

        }
        catch (Exception ex) { }
        return dt;
    }
    public DataTable getAccount(string type)
    {
        DataTable dt = new DataTable();
        try
        {
            Functionality fn = new Functionality();
            dt = fn.GetDatasetByCommand("select f.a_fullname as a_fullname,"//+Case When f.a_type='S' Then '(Speaker)' When f.a_type='D' Then '(Delegate)' Else '' End
                                    + " f.a_ID as a_ID from tblAccount f where f.deleteFlag=0 And f.a_type In ('" + type + "') Order By f.a_fullname", "sdt").Tables[0];//+';'+f.a_type
        }
        catch (Exception ex) { }
        return dt;
    }
    public DataTable getConference()
    {
        DataTable dt = new DataTable();
        try
        {
            Functionality fn = new Functionality();
            StringBuilder str = new StringBuilder();
            str.Append("select c.c_ID,c.c_brief,cc.cc_categoryname as c_cc,c.c_date,c.c_stime,c.c_etime,c.c_liveQA,c.c_title,ccc.c_title as c_parentConfID,vb.vb_name as c_venue,c.updatedDate from tblConference c");
            str.Append(" left join tblVenue_Booth vb on c.c_venue=vb.vb_ID");
            str.Append(" left join tblConferenceCategory cc on c.c_cc=cc.cc_ID");
            str.Append(" left join tblConference ccc on c.c_parentConfID=ccc.c_ID");
            str.Append(" where c.deleteFlag=0 And c.ZoomID<>'' Order By c.c_title Asc");
            dt = fn.GetDatasetByCommand(str.ToString(), "sdt").Tables[0];
        }
        catch (Exception ex) { }
        return dt;
    }
    #endregion

    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        bindAccount();
    }
}