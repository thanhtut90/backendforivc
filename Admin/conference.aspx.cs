﻿using App.DAL;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
public partial class Admin_conference : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    Functionality fn = new Functionality();
    string tablename = "tblConference";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdconference.ExportSettings.IgnorePaging = true;
                grdconference.ExportSettings.ExportOnlyData = true;
                grdconference.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdconference.ExportSettings.FileName = "Conference";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtvenue = daldata.getBooth();

            ddlvenue.DataSource = dtvenue;
            ddlvenue.DataTextField = "vb_name";
            ddlvenue.DataValueField = "vb_ID";
            ddlvenue.DataBind();
            ListItem lst1 = new ListItem("Select Venue (Booth)", "");
            ddlvenue.Items.Insert(ddlvenue.Items.Count - ddlvenue.Items.Count, lst1);

            DataTable dtconfcategory = daldata.getConferenceCategory();

            ddlcategory.DataSource = dtconfcategory;
            ddlcategory.DataTextField = "cc_categoryname";
            ddlcategory.DataValueField = "cc_ID";
            ddlcategory.DataBind();
            ListItem lst2 = new ListItem("Select Conference Category", "");
            ddlcategory.Items.Insert(ddlcategory.Items.Count - ddlcategory.Items.Count, lst2);

            DataTable dtmainconf = daldata.getMainConf();

            ddlmainconf.DataSource = dtmainconf;
            ddlmainconf.DataTextField = "c_title";
            ddlmainconf.DataValueField = "c_ID";
            ddlmainconf.DataBind();
            ListItem lst3 = new ListItem("Select Main Conference", "");
            ddlmainconf.Items.Insert(ddlmainconf.Items.Count - ddlmainconf.Items.Count, lst3);

            DataTable dtZoomConfiguration = daldata.getZoomConfiguration();
            ddlConfiguration.DataSource = dtZoomConfiguration;
            ddlConfiguration.DataTextField = "ConfigName";
            ddlConfiguration.DataValueField = "ConfigId";
            ddlConfiguration.DataBind();
            ListItem lst4 = new ListItem("Select Configuration", "");
            ddlConfiguration.Items.Insert(ddlConfiguration.Items.Count - ddlConfiguration.Items.Count, lst4);

            #region systemtimezones no use
            //int itemcount = 0;
            //foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
            //{
            //    ddlTimeZone.Items.Add(z.DisplayName);
            //    ddlTimeZone.Items[itemcount].Value = z.Id;
            //    itemcount++;
            //}
            #endregion
            DataTable dtTimeZones = daldata.getTimeZones();
            ddlTimeZone.DataSource = dtTimeZones;
            ddlTimeZone.DataTextField = "Time";//DisplayName
            ddlTimeZone.DataValueField = "DisplayName";
            ddlTimeZone.DataBind();
            ListItem lst5 = new ListItem("Select Time Zone", "");
            ddlTimeZone.Items.Insert(ddlTimeZone.Items.Count - ddlTimeZone.Items.Count, lst5);


            DataTable dtday = daldata.getDay();
            txtdate.DataSource = dtday;
            txtdate.DataTextField = "d_date";
            txtdate.DataValueField = "d_date";
            txtdate.DataBind();
            //ListItem lst6 = new ListItem("Select Date", "");
            //txtdate.Items.Insert(txtdate.Items.Count - txtdate.Items.Count, lst6);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                #region date picker comment
                //string strdate = txtdate.Text.Trim();
                //if (!fn.validateDate(strdate))
                //{
                //    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Please enter valid date (dd/mm/yyyy).');", true);
                //    return;
                //}

                //string conf_date = "";
                //string datestr = daldata.replaceForSQL(txtdate.Text.Trim());
                //string[] date = datestr.Split('/');
                //if(date.Length > 0)
                //{
                //    conf_date = date[2] + "/" + date[1] + "/" + date[0];
                //}
                //if(string.IsNullOrEmpty(conf_date))
                //{
                //    conf_date = txtdate.Text.Trim();
                //}
                #endregion
                string conf_date = txtdate.SelectedValue;

                DataTable dt = new DataTable();
                dt.Columns.Add("c_ID");
                dt.Columns.Add("c_cc");
                dt.Columns.Add("c_date");
                dt.Columns.Add("c_stime");
                dt.Columns.Add("c_etime");
                dt.Columns.Add("c_title");
                dt.Columns.Add("c_venue");
                dt.Columns.Add("c_brief");
                dt.Columns.Add("c_liveQA");
                dt.Columns.Add("c_parentConfID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("c_Polling");
                dt.Columns.Add("c_Survey");
                dt.Columns.Add("c_TimeZone");
                dt.Columns.Add("ZoomId");
                dt.Columns.Add("c_Password");
                dt.Columns.Add("ConfigId");

                if (hfid.Value != "")
                {
                    string configuration = ddlConfiguration.SelectedValue.ToString();
                    //Update
                    DataRow dr = dt.NewRow();

                    dr["c_ID"] = hfid.Value.ToString();
                    dr["c_cc"] = ddlcategory.SelectedValue.ToString();
                    dr["c_date"] = daldata.replaceForSQL(conf_date);
                    dr["c_stime"] = daldata.replaceForSQL(txtstime.Text.Trim());
                    dr["c_etime"] = daldata.replaceForSQL(txtetime.Text.Trim());
                    dr["c_title"] = daldata.replaceForSQL(txtconference.Text.Trim());
                    dr["c_venue"] = ddlvenue.SelectedValue.ToString();
                    dr["c_brief"] = daldata.replaceForSQL(txtbrief.Content.Trim());
                    dr["c_liveQA"] = rodliveQA.SelectedValue.ToString();
                    dr["c_parentConfID"] = ddlmainconf.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["c_Polling"] = rdopolling.SelectedValue.ToString();
                    dr["c_Survey"] = rdosurvey.SelectedValue.ToString();
                    dr["c_TimeZone"] = daldata.replaceForSQL(ddlTimeZone.SelectedItem.Value.ToString());// txtTimeZone.Text.Trim());
                    dr["ZoomId"] = hfZoomID.Value.ToString();
                    dr["c_Password"] = daldata.replaceForSQL(txtPassword.Text.Trim());
                    dr["ConfigId"] = daldata.replaceForSQL(configuration);

                    int res = daldata.UpdateConference(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        #region call zoom api
                        string errMsg = "";
                        UpdateDataAPI(hfid.Value.ToString(), hfZoomID.Value.ToString(), dr["c_title"].ToString(), dr["c_date"].ToString(), dr["c_stime"].ToString(), dr["c_etime"].ToString(), dr["c_TimeZone"].ToString(), dr["c_Password"].ToString(), dr["c_brief"].ToString(), configuration.ToString(), ref errMsg);
                        if (!string.IsNullOrEmpty(errMsg))
                        {
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Update API error!=>" + errMsg;
                        }
                        #endregion
                        else
                        {
                            grdconference_NeedDSource("");
                            divadd.Visible = false;
                            divsearch.Visible = true;
                            divlist.Visible = true;
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Update Success !";

                            clearControls();
                        }
                    }
                }
                else
                {
                    string configuration = ddlConfiguration.SelectedValue.ToString();
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["c_ID"] = "";
                    dr["c_cc"] = ddlcategory.SelectedValue.ToString();
                    dr["c_date"] = daldata.replaceForSQL(conf_date);
                    dr["c_stime"] = daldata.replaceForSQL(txtstime.Text.Trim());
                    dr["c_etime"] = daldata.replaceForSQL(txtetime.Text.Trim());
                    dr["c_title"] = daldata.replaceForSQL(txtconference.Text.Trim());
                    dr["c_venue"] = ddlvenue.SelectedValue.ToString();
                    dr["c_brief"] = daldata.replaceForSQL(txtbrief.Content.Trim());
                    dr["c_liveQA"] = rodliveQA.SelectedValue.ToString();
                    dr["c_parentConfID"] = ddlmainconf.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["c_Polling"] = rdopolling.SelectedValue.ToString(); 
                    dr["c_Survey"] = rdosurvey.SelectedValue.ToString();
                    dr["c_TimeZone"] = daldata.replaceForSQL(ddlTimeZone.SelectedItem.Value.ToString());// txtTimeZone.Text.Trim());
                    dr["ZoomId"] = "";
                    dr["c_Password"] = daldata.replaceForSQL(txtPassword.Text.Trim());
                    dr["ConfigId"] = daldata.replaceForSQL(configuration);

                    string confID = "";
                    int res = daldata.SaveConference(dr, ref confID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        #region call zoom api
                        string errMsg = "";
                        SaveDataAPI(confID, dr["c_title"].ToString(), dr["c_date"].ToString(), dr["c_stime"].ToString(), dr["c_etime"].ToString(), dr["c_TimeZone"].ToString(), dr["c_Password"].ToString(), dr["c_brief"].ToString(), configuration.ToString(), ref errMsg);
                        if (!string.IsNullOrEmpty(errMsg))
                        {
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Saving API error!=>" + errMsg;
                            int res1 = daldata.DeleteConferenceByID(confID);
                        }
                        #endregion
                        else
                        {
                            grdconference_NeedDSource("");
                            divadd.Visible = false;
                            divsearch.Visible = true;
                            divlist.Visible = true;
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Save Success !";

                            clearControls();
                        }
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtconference.Text.Trim()))
            {
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtdate.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdconference.ExportSettings.ExportOnlyData = true;
        this.grdconference.ExportSettings.IgnorePaging = true;
        this.grdconference.ExportSettings.OpenInNewWindow = true;
        this.grdconference.MasterTableView.ExportToExcel();

        grdconference.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdconference.ExportSettings.FileName = "Conference";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        //BindControlData();
        clearControls();
    }
    protected void grdconference_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getConference();
            grdconference.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdconference_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdconference.CurrentPageIndex + 1;
    }
    protected void grdconference_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getConferencebyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getConference();
            }


            grdconference.DataSource = dt;
            grdconference.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdconference_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdconference.ExportSettings.ExportOnlyData = true;
            this.grdconference.ExportSettings.IgnorePaging = true;
            this.grdconference.ExportSettings.OpenInNewWindow = true;
            this.grdconference.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getConferenceByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["c_ID"].ToString();
                    try
                    {
                        string selectedDate = "";
                        string conf_date = dt.Rows[0]["c_date"].ToString();
                        #region datepicker comment
                        //string[] date = conf_date.Split('/');
                        //if (date.Length > 0)
                        //{
                        //    selectedDate = date[2] + "/" + date[1] + "/" + date[0];
                        //}
                        //txtdate.Text = selectedDate;
                        #endregion
                        if (!String.IsNullOrEmpty(conf_date))
                        {
                            ListItem listItem = txtdate.Items.FindByValue(conf_date);
                            if (listItem != null)
                            {
                                txtdate.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch(Exception ex)
                    { }
                    txtstime.Text = dt.Rows[0]["c_stime"].ToString();
                    txtetime.Text = dt.Rows[0]["c_etime"].ToString();
                    txtconference.Text = dt.Rows[0]["c_title"].ToString();
                    txtbrief.Content = dt.Rows[0]["c_brief"].ToString();
                    ddlvenue.SelectedValue = dt.Rows[0]["c_venue"].ToString();
                    ddlcategory.SelectedValue = dt.Rows[0]["c_cc"].ToString();
                    ddlmainconf.SelectedValue = dt.Rows[0]["c_parentConfID"].ToString();
                    rodliveQA.SelectedValue= dt.Rows[0]["c_LiveQA"].ToString() == "True" ? "1" : "0";
                    rdopolling.SelectedValue = dt.Rows[0]["c_polling"].ToString() == "True" ? "1" : "0";
                    rdosurvey.SelectedValue = dt.Rows[0]["c_survey"].ToString() == "True" ? "1" : "0";
                    //txtTimeZone.Text = dt.Rows[0]["c_TimeZone"].ToString();
                    string timezone = dt.Rows[0]["c_TimeZone"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(timezone))
                        {
                            ListItem listItem = ddlTimeZone.Items.FindByValue(timezone);
                            if (listItem != null)
                            {
                                ddlTimeZone.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    hfZoomID.Value = dt.Rows[0]["ZoomId"] != DBNull.Value ? dt.Rows[0]["ZoomId"].ToString() : "";
                    txtPassword.Text = dt.Rows[0]["c_Password"] != DBNull.Value ? dt.Rows[0]["c_Password"].ToString() : "";
                    string ConfigId = dt.Rows[0]["ConfigId"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(ConfigId))
                        {
                            ListItem listItem = ddlConfiguration.Items.FindByValue(ConfigId);
                            if (listItem != null)
                            {
                                ddlConfiguration.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteConferenceByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdconference_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdconference_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtconference.Text = "";
        ddlcategory.SelectedIndex = 0;
        ddlvenue.SelectedIndex = 0;
        ddlmainconf.SelectedIndex = 0;
        //txtdate.Text = "";
        txtdate.SelectedIndex = 0;
        txtstime.Text = "";
        txtetime.Text = "";
        txtbrief.Content = "";
        //txtTimeZone.Text = "";
        ddlTimeZone.SelectedIndex = 0;
        hfZoomID.Value = "";
        txtPassword.Text = "";
        ddlConfiguration.SelectedIndex = 0;
    }

    #region API call
    private void SaveDataAPI(string mobconfID, string topic, string conf_date, string start_time, string duration, string timezone, string password, string agenda, string configuration, ref string errMsg)
    {
        errMsg = "";
        string accessToken = HttpGetToken("http://203.127.83.146/iVC_API/api/token", ref errMsg);
        if(!string.IsNullOrEmpty(errMsg))
        {
            //Response.Write(errMsg);
            //Response.End();
        }
        else
        {
            try
            {
                daldata.saveToken(accessToken);

                errMsg = "";
                string saveURI = "http://203.127.83.146/iVC_API/api/conference/" + mobconfID;
                errMsg = HttpPostWithAuthSaveConference(saveURI, accessToken, mobconfID, topic, conf_date, start_time, duration, timezone, password, agenda, configuration);
            }
            catch(Exception ex)
            {
                errMsg = ex.Message;
                //Response.Write(ex.Message);
                //Response.End();
            }
        }
    }
    public string HttpGetToken(string URI, ref string errorMessage)
    {
        string json = "";
        try
        {
            #region no use old
            //string postData = "{\n    \"username\": \"stw\",\n    \"password\": \"admin\",\n    \"LoginType\": 0\n}";
            ////specify to use TLS 1.2 as default connection
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //WebRequest request = WebRequest.Create(URI);
            //// Set the Method property of the request to POST.
            //request.Method = "Get";
            //// Set the ContentType property of the WebRequest.
            //request.ContentType = "application/raw";
            //// content
            //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            //request.ContentLength= byteArray.Length;
            //// Get the request stream.
            //var response = request.GetResponse();

            //System.IO.Stream stream = response.GetResponseStream();
            //System.IO.StreamReader sr = new System.IO.StreamReader(stream);
            //json = sr.ReadToEnd().Trim();
            #endregion

            var client = new RestClient(URI);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddParameter("text/plain", "{\n    \"username\": \"stw\",\n    \"password\": \"admin\",\n    \"LoginType\": 0\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
            string returnJson = response.Content;
            //// parse the json response so that we can get at the key/value pairs
            //dynamic api = JObject.Parse(returnJson);
            //// Parsing JSON content into element-node JObject
            var jObject = JObject.Parse(returnJson);
            //if (jObject["data"].Count() > 0)
            {
                var jObject1 = JObject.Parse(jObject["data"].ToString());
                var accessToken = Convert.ToString(jObject1["access_token"]);
                json = accessToken;
            }

            return json;
        }
        catch (Exception ex)
        {
            if (ex.GetType().Name == "WebException")
            {
                errorMessage = ex.Message;
            }
            else
            {
            }
            return "";
        }
    }
    public string HttpPostWithAuthSaveConference(string URI, string accessToken, string mobconfID, string topic, string conf_date, string start_time, string duration, string timezone, string password, string agenda, string configuration)
    {
        string errorMessage = "";

        if (string.IsNullOrWhiteSpace(accessToken))
        {
            errorMessage = "Please ensure that the access token is not empty!";
        }
        else
        {
            try
            {
                #region no use old
                ////specify to use TLS 1.2 as default connection
                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                //WebRequest request = WebRequest.Create(URI);
                //request.Headers.Clear();
                //request.Headers.Add(HttpRequestHeader.Authorization, string.Format("Bearer {0}", tbAccessToken.Text));
                //request.Method = "GET";

                //var response = (HttpWebResponse)request.GetResponse();

                //System.IO.Stream stream = response.GetResponseStream();
                //System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                //json = sr.ReadToEnd().Trim();
                #endregion

                var client = new RestClient(URI);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + accessToken);
                request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", "{\r\n\t\"topic\":\"" + topic + "\",\r\n\t\"start_time\":\"" + start_time + "\",\r\n\t\"duration\":" + duration + ",\r\n\t\"timezone\":\"" + timezone + "\",\r\n\t\"password\":\"" + password + "\",\r\n\t\"agenda\":\"" + topic + "\",\r\n\t\"configurationId\":" + configuration + "\",\r\n\t\"DeleteFlag\":\"False\"\r\n\t\r\n}", ParameterType.RequestBody);
                request.AddParameter("application/json", "{\r\n\t\"topic\":\"" + topic + "\",\r\n\t\"Conf_Date\":\"" + conf_date + "\",\r\n\t\"start_time\":\"" + start_time + "\",\r\n\t\"duration\":" + duration + ",\r\n\t\"timezone\":\"" + timezone + "\",\r\n\t\"password\":\"" + password + "\",\r\n\t\"agenda\":\"" + topic + "\"\r\n\t\r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string returnJson = response.Content;
                //// parse the json response so that we can get at the key/value pairs
                //dynamic api = JObject.Parse(returnJson);
                //// grab the values and do your assertions :-)
                var jObject = JObject.Parse(returnJson);
                var status = Convert.ToString(jObject["success"].ToString());
                var zoomConfID = Convert.ToString(jObject["data"].ToString());
                var message = Convert.ToString(jObject["message"].ToString());
                if (status == "True")
                {
                    int isSuccessSave = daldata.saveZoomConfID(mobconfID, zoomConfID, configuration);
                    if(isSuccessSave > 0)
                    {

                    }
                    else
                    {
                        errorMessage = "Saving ZoomID error!";
                    }
                }
                else
                {
                    errorMessage = "API calling return error!";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "WebException")
                {
                    var response = (HttpWebResponse)((ex as WebException).Response);

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new WebException();
                    }
                }
                else
                {
                    errorMessage = ex.Message;
                }
            }
        }

        return errorMessage;
    }
    private void UpdateDataAPI(string mobconfID, string zoomconfID, string topic, string conf_date, string start_time, string duration, string timezone, string password, string agenda, string configuration, ref string errMsg)
    {
        errMsg = "";
        string accessToken = HttpGetToken("http://203.127.83.146/iVC_API/api/token", ref errMsg);
        if (!string.IsNullOrEmpty(errMsg))
        {
            //Response.Write(errMsg);
            //Response.End();
        }
        else
        {
            try
            {
                daldata.saveToken(accessToken);

                errMsg = "";
                string saveURI = "http://203.127.83.146/iVC_API/api/conference/" + mobconfID + "";
                errMsg = HttpPostWithAuthUpdateConference(saveURI, accessToken, mobconfID, topic, conf_date, start_time, duration, timezone, password, agenda, configuration);
            }
            catch (Exception ex)
            {
                errMsg = ex.Message;
                //Response.Write(ex.Message);
                //Response.End();
            }
        }
    }
    public string HttpPostWithAuthUpdateConference(string URI, string accessToken, string mobconfID, string topic, string conf_date, string start_time, string duration, string timezone, string password, string agenda, string configuration)
    {
        string errorMessage = "";

        if (string.IsNullOrWhiteSpace(accessToken))
        {
            errorMessage = "Please ensure that the access token is not empty!";
        }
        else
        {
            try
            {
                var client = new RestClient(URI);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + accessToken);
                request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", "{\r\n\t\"topic\":\"" + topic + "\",\r\n\t\"start_time\":\"" + start_time + "\",\r\n\t\"duration\":" + duration + ",\r\n\t\"timezone\":\"" + timezone + "\",\r\n\t\"password\":\"" + password + "\",\r\n\t\"agenda\":\"" + topic + "\",\r\n\t\"configurationId\":" + configuration + "\",\r\n\t\"DeleteFlag\":\"False\"\r\n\t\r\n}", ParameterType.RequestBody);
                request.AddParameter("application/json", "{\r\n\t\"topic\":\"" + topic + "\",\r\n\t\"Conf_Date\":\"" + conf_date + "\",\r\n\t\"start_time\":\"" + start_time + "\",\r\n\t\"duration\":" + duration + ",\r\n\t\"timezone\":\"" + timezone + "\",\r\n\t\"password\":\"" + password + "\",\r\n\t\"agenda\":\"" + topic + "\"\r\n\t\r\n}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                string returnJson = response.Content;
                //// parse the json response so that we can get at the key/value pairs
                //dynamic api = JObject.Parse(returnJson);
                //// grab the values and do your assertions :-)
                var jObject = JObject.Parse(returnJson);
                var status = Convert.ToString(jObject["success"].ToString());
                var zoomConfID = Convert.ToString(jObject["data"].ToString());
                var message = Convert.ToString(jObject["message"].ToString());
                if (status == "True")
                {
                    //int isSuccessSave = daldata.saveZoomConfID(mobconfID, zoomConfID, configuration);
                    //if (isSuccessSave > 0)
                    //{

                    //}
                    //else
                    //{
                    //    errorMessage = "Saving ZoomID error!";
                    //}
                }
                else
                {
                    errorMessage = "API calling return error!";
                }
            }
            catch (Exception ex)
            {
                if (ex.GetType().Name == "WebException")
                {
                    var response = (HttpWebResponse)((ex as WebException).Response);

                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new WebException();
                    }
                }
                else
                {
                    errorMessage = ex.Message;
                }
            }
        }

        return errorMessage;
    }
    #endregion
}