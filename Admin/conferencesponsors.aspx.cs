﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_conferencesponsors : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblConferenceSponsors";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdConferenceSponsor.ExportSettings.IgnorePaging = true;
                grdConferenceSponsor.ExportSettings.ExportOnlyData = true;
                grdConferenceSponsor.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdConferenceSponsor.ExportSettings.FileName = "ConferenceSponsor";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtConf = daldata.getConference();
            ddlConference.DataSource = dtConf;
            ddlConference.DataTextField = "c_title";
            ddlConference.DataValueField = "c_ID";
            ddlConference.DataBind();
            ListItem lstConf = new ListItem("Select Conference", "");
            ddlConference.Items.Insert(ddlConference.Items.Count - ddlConference.Items.Count, lstConf);

            DataTable dtSpn = daldata.getSponsor();
            ddlSponsor.DataSource = dtSpn;
            ddlSponsor.DataTextField = "s_name";
            ddlSponsor.DataValueField = "s_ID";
            ddlSponsor.DataBind();
            ListItem lstSpn = new ListItem("Select Sponsor", "");
            ddlSponsor.Items.Insert(ddlSponsor.Items.Count - ddlSponsor.Items.Count, lstSpn);

            DataTable dtSpnCat = daldata.getsponsorcategory();
            ddlSponsorCategory.DataSource = dtSpnCat;
            ddlSponsorCategory.DataTextField = "sponsorCat_name";
            ddlSponsorCategory.DataValueField = "sponsorCat_ID";
            ddlSponsorCategory.DataBind();
            ListItem lstConfCat = new ListItem("Select Sponsor Category", "");
            ddlSponsorCategory.Items.Insert(ddlSponsorCategory.Items.Count - ddlSponsorCategory.Items.Count, lstConfCat);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("csponsor_ID");
                dt.Columns.Add("csponsor_sponsorID");
                dt.Columns.Add("csponsor_scategory");
                dt.Columns.Add("csponsor_conferenceID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["csponsor_ID"] = hfid.Value.ToString();
                    dr["csponsor_conferenceID"] = ddlConference.SelectedValue.ToString();
                    dr["csponsor_sponsorID"] = ddlSponsor.SelectedValue.ToString();
                    dr["csponsor_scategory"] = ddlSponsorCategory.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateConferenceSponsor(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceSponsor_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["csponsor_ID"] = "";
                    dr["csponsor_conferenceID"] = ddlConference.SelectedValue.ToString();
                    dr["csponsor_sponsorID"] = ddlSponsor.SelectedValue.ToString();
                    dr["csponsor_scategory"] = ddlSponsorCategory.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveConferenceSponsor(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceSponsor_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (ddlConference.SelectedItem.Value == "")
            {
                lblerrConference.Visible = true;
                rtn = false;
            }
            if (ddlSponsor.SelectedItem.Value == "")
            {
                lblerrSponsor.Visible = true;
                rtn = false;
            }
            if (ddlSponsorCategory.SelectedItem.Value == "")
            {
                lblerrSponsorCategory.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdConferenceSponsor.ExportSettings.ExportOnlyData = true;
        this.grdConferenceSponsor.ExportSettings.IgnorePaging = true;
        this.grdConferenceSponsor.ExportSettings.OpenInNewWindow = true;
        this.grdConferenceSponsor.MasterTableView.ExportToExcel();

        grdConferenceSponsor.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdConferenceSponsor.ExportSettings.FileName = "ConferenceSponsor";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdConferenceSponsor_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getConferenceSponsor();
            grdConferenceSponsor.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceSponsor_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdConferenceSponsor.CurrentPageIndex + 1;
    }
    protected void grdConferenceSponsor_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getConferenceSponsorbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getConferenceSponsor();
            }

            grdConferenceSponsor.DataSource = dt;
            grdConferenceSponsor.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceSponsor_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdConferenceSponsor.ExportSettings.ExportOnlyData = true;
            this.grdConferenceSponsor.ExportSettings.IgnorePaging = true;
            this.grdConferenceSponsor.ExportSettings.OpenInNewWindow = true;
            this.grdConferenceSponsor.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getConferenceSponsorlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["csponsor_ID"].ToString();
                    string confID = dt.Rows[0]["csponsor_conferenceID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(confID))
                        {
                            ListItem listItem = ddlConference.Items.FindByValue(confID);
                            if (listItem != null)
                            {
                                ddlConference.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string spnID = dt.Rows[0]["csponsor_sponsorID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(spnID))
                        {
                            ListItem listItem = ddlSponsor.Items.FindByValue(spnID);
                            if (listItem != null)
                            {
                                ddlSponsor.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string spncatID = dt.Rows[0]["csponsor_scategory"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(spncatID))
                        {
                            ListItem listItem = ddlSponsorCategory.Items.FindByValue(spncatID);
                            if (listItem != null)
                            {
                                ddlSponsorCategory.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteConferenceSponsorByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdConferenceSponsor_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdConferenceSponsor_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        ddlConference.SelectedIndex = 0;
        ddlSponsor.SelectedIndex = 0;
        ddlSponsorCategory.SelectedIndex = 0;
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrConference.Visible = false;
        lblerrSponsor.Visible = false;
        lblerrSponsorCategory.Visible = false;
    }
}