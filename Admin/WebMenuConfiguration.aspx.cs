﻿using System;
using System.Data;
using System.Web.UI.WebControls;

public partial class Admin_WebMenuConfiguration : System.Web.UI.Page
{
    DAL dal = new DAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            BindParentMenu();
            BindOrderNum();
            if(Request.QueryString["id"] != null)
            {
                DataTable dt = dal.getWebsiteMenuByID(Request.QueryString["id"].ToString());
                if(dt.Rows.Count>0)
                {
                    txtmenutitle.Text = dt.Rows[0]["m_title"].ToString();
                    txturl.Text= dt.Rows[0]["m_pagetoredirect"].ToString();
                    lblmorder.Text= dt.Rows[0]["m_sortorder"].ToString();
                    if(!String.IsNullOrEmpty(dt.Rows[0]["m_parentmenuID"].ToString()))
                    {
                        chkisChild.Checked = true;
                        pmenu.Visible = true;
                        ddlparent.SelectedValue = dt.Rows[0]["m_parentmenuID"].ToString();
                    }
                    lblmicon.Text = dt.Rows[0]["m_IconUrl"].ToString();
                    btnSubmit.Text = "Update";
                }
            }
        }
    }
    private void BindOrderNum()
    {
        var num = dal.getWebsiteMenuOrderNum();
        lblmorder.Text = num;
    }
    private void BindParentMenu()
    {
        try
        {
            DataTable dt = dal.getWebsiteParentMenu();
            if (dt.Rows.Count > 0)
            {
                ddlparent.DataSource = dt;
                ddlparent.DataTextField = "m_title";
                ddlparent.DataValueField = "m_id";
                ddlparent.DataBind();
                ListItem lst = new ListItem("Choose Parent Menu", "");
                ddlparent.Items.Insert(ddlparent.Items.Count - ddlparent.Items.Count, lst);

            }
            else
            {
                ListItem lst = new ListItem("No Data", "");
                ddlparent.Items.Insert(ddlparent.Items.Count - ddlparent.Items.Count, lst);
            }
        }
        catch { }
    }
    protected void chkisChild_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (chkisChild.Checked == true)
            {
                pmenu.Visible = true;
            }
            else
            {
                pmenu.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void ddlparent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlparent.SelectedValue != "")
        {
            BindOrderNumSub(ddlparent.SelectedValue.ToString());
        }
    }
    private void BindOrderNumSub(string parentID)
    {
        var num = dal.getWebsiteMenuOrderNumSub(parentID);
        lblmorder.Text = num;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(txtmenutitle.Text) && !String.IsNullOrEmpty(txturl.Text))
        {
            string menutitle = txtmenutitle.Text.Trim();
            string redirectURL = txturl.Text.Trim();
         
            string ordernum = lblmorder.Text;
            string parentID = string.Empty;
            if (chkisChild.Checked == true)
            {
                parentID = ddlparent.SelectedValue;
            }
            
            string micon = string.Empty;
            if (fupicon.HasFile == false && !String.IsNullOrEmpty(lblmicon.Text))
            {
                micon = lblmicon.Text;
            }
            else if (fupicon.HasFile == true && String.IsNullOrEmpty(lblmicon.Text))
            {
                micon = UploadImageFile("AppIcon");
            }
            string id = string.Empty;
            if (Request.QueryString["id"] != null)
            {
                id = Request.QueryString["id"].ToString();
            }
            int res = dal.insertWebsiteMenu(id, menutitle, redirectURL, ordernum, parentID,micon);
            if(res == 1)
            {
                Response.Redirect("WebMenuConfiguration.aspx");
            }
        }
    }

    private string UploadImageFile(string fname)
    {
        string imagefilename = string.Empty;
        try
        {
            string ext = System.IO.Path.GetExtension(fupicon.FileName).ToString().ToLower();
            if (ext != ".png" && ext != ".jpg" && ext != ".gif" && ext != ".jpeg")
            {
                msg.Text = "Invalid Image Type";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
            }
            else
            {
                noti.Visible = false;
            }

            string filename = fname + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + ext;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~~\\sys_Images\\Website\\").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~~\\sys_Images\\Website\\") + filename);
            fupicon.SaveAs(savePath);
            imagefilename = filename;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }

    public string getParentMenu(string parentMenuID)
    {
        string rtn = string.Empty;
        rtn = dal.getWebParentMenuByID(parentMenuID);
        return rtn;
    }
    protected void grdMenu_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {            
            DataTable dt = dal.getWebsiteMenu();
            grdMenu.DataSource = dt;           
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    protected void grdMenu_NeedDSource()
    {
        try
        {
            DataTable dt = dal.getWebsiteMenu();
            grdMenu.DataSource = dt;
            grdMenu.DataBind();

        }
        catch (Exception ex) { }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        string id = ((LinkButton)sender).CommandArgument;
        if (!string.IsNullOrEmpty(id))
        {
            Response.Redirect("WebMenuConfiguration.aspx?id=" + id);
        }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        LinkButton del = sender as LinkButton; int chkFE = 0;
        if (del.CommandName == "Delete")
        {
            string ID = del.CommandArgument.ToString();
            chkFE = dal.CheckUsage(ID);
            if (chkFE == 0)
            {
                int res = dal.DeleteWebsiteMenu(int.Parse(ID));
                if (res == 1)
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Success !";                    
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    msg.Text = "Fail !";
                }
            }
            else
            {
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                msg.Text = "This item is used in FrontEnd. CANNOT BE DELETED !";
            }

            grdMenu_NeedDSource();
        }
    }
}