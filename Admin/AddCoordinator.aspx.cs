﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AddCoordinator : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected string profileimg;
    protected string id;
    protected string img;
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (Request.QueryString["id"] != null && Request.QueryString["img"] != null)
        {
            id= Request.QueryString["id"].ToString();
            img = Request.QueryString["img"].ToString();
             profileimg = Constant.URL+Constant.filesavingurlview+img;    
            //profileimg = "http://thaifex.event-reg.biz/thaifex2018/Images/FrontEnd_Photos/IE1v22018-05-16-05-18-39.jpg";
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("venuebooth.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(id)) {
                if (Validation())
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("vb_ID");
                    dt.Columns.Add("vb_width");
                    dt.Columns.Add("vb_height");
                    dt.Columns.Add("vb_x");
                    dt.Columns.Add("vb_y");
                    dt.Columns.Add("UpdatedDate");
                    DataRow dr = dt.NewRow();
                    dr["vb_ID"] = id;                  
                    dr["vb_width"] = w.Text;
                    dr["vb_height"] = h.Text;
                    dr["vb_x"] = x.Text;
                    dr["vb_y"] = y.Text;
                    dr["UpdatedDate"] = DateTime.Now;

                        int res = daldata.UpdateBoothCoordinator(dr);
                        if (res == 1)
                        {
                        Response.Write("<script>alert('Successfully Inserted!!!');</script>");
                        Response.Redirect("venuebooth.aspx");
                    }
                    }
                }
            }catch (Exception ex)
        {

        }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(x.Text.Trim()) && string.IsNullOrEmpty(y.Text.Trim()) && string.IsNullOrEmpty(w.Text.Trim()) && string.IsNullOrEmpty(h.Text.Trim()))
            {
                rtn = false;
                lblerr.Visible = true;
            }
            else
            {
                lblerr.Visible = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
}