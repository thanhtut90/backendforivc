﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="Newsletter.aspx.cs" Inherits="Admin_Newsletter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
 <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdNewsletter" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">                      
                        <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>   
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">News Letter</h3>
                    <div style="float:right">                            
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lnkDownload" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>                        
                </div>                                       
                <div class="box-body"> 
                    <div id="divsearch" runat="server" class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                        <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>                    
                        </div>
                        <div class="col-md-1">                        
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />   
                        </div>
                        <div class="col-md-6">
                        <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                        
                    </div>  
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdNewsletter"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdNewsletter_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdNewsletter_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdNewsletter_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="nl_ID" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true"/>
                                        <Columns> 
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                               <%# Container.ItemIndex + 1  %>  
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn>     
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">                                    
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("nl_ID") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>                                    
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("nl_ID") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>                                                                                                           
                                                </ItemTemplate>                     
                                            </telerik:GridTemplateColumn>                              
                                            <telerik:GridBoundColumn  DataField="nl_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Title" SortExpression="nl_title" UniqueName="nl_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>   
                                             <telerik:GridBoundColumn DataField="nl_issue" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Issue" SortExpression="nl_issue" UniqueName="nl_issue" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>  
                                                <telerik:GridBoundColumn DataField="nl_doissue" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Date of Issue" SortExpression="nl_doissue" UniqueName="nl_doissue" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>  
                                            <telerik:GridBoundColumn  DataField="nl_cover" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Picture" SortExpression="nl_cover" UniqueName="nl_cover" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                              <telerik:GridBoundColumn DataField="nl_dlink" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Download Link" SortExpression="nl_dlink" UniqueName="nl_dlink" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>  
                                            <telerik:GridBoundColumn  DataField="m_TitleEn" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Menu" SortExpression="ec_country" UniqueName="ec_country" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>   
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">News Letter Title</label>
                                <div class="col-sm-6"><asp:HiddenField ID="hfid" runat="server" />
                                <asp:TextBox ID="txtNLtitle" class="form-control"  placeholder="Enter Title" runat="server"></asp:TextBox>
                                      <asp:Label ID="lblerrtitle" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>   
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Issue</label>
                                <div class="col-sm-6"><asp:HiddenField ID="HiddenField1" runat="server" />
                                <asp:TextBox ID="txtIssue" class="form-control"  placeholder="Enter Issue" runat="server"></asp:TextBox>
                                     <asp:Label ID="lblerrIssue" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>      
                             <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Date of Issue</label>
                                <div class="col-sm-6"><asp:HiddenField ID="HiddenField2" runat="server" />
                                <asp:TextBox ID="txtDateIssue" class="form-control"  placeholder="Enter Date of Issue" runat="server"></asp:TextBox>
                                     <asp:Label ID="lblerrDateIssue" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>                              
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">News Cover</label>
                                <div class="col-sm-3">
                                <asp:FileUpload ID="fupForm" runat="server" /><asp:Label ID="lblfloorplan" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-3">

                                </div>
                            </div>
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Download Link</label>
                                <div class="col-sm-6"><asp:HiddenField ID="HiddenField3" runat="server" />
                                <asp:TextBox ID="txtDownlink" class="form-control"  placeholder="Enter Download Link" runat="server"></asp:TextBox>
                                </div>
                            </div>         
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Menu</label>
                                <div class="col-sm-6">
                                <asp:DropDownList ID="ddlmenu" runat="server" CssClass="form-control" ></asp:DropDownList>
                                      <asp:Label ID="lblerrdll" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>                          
                        </div>
                        <div class="box-footer">                                
                           <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
</asp:Content>


