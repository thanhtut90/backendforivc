﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Eposter : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblE-Poster";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdEPoster.ExportSettings.IgnorePaging = true;
                grdEPoster.ExportSettings.ExportOnlyData = true;
                grdEPoster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdEPoster.ExportSettings.FileName = "E-Poster";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtvenue = daldata.getBooth();

            ddlvenue.DataSource = dtvenue;
            ddlvenue.DataTextField = "vb_name";
            ddlvenue.DataValueField = "vb_ID";
            ddlvenue.DataBind();
            ListItem lst1 = new ListItem("Select Venue (Booth)", "0");
            ddlvenue.Items.Insert(ddlvenue.Items.Count - ddlvenue.Items.Count, lst1);


            DataTable dtauthor = daldata.getSpeaker();

            ddlAuthor.DataSource = dtauthor;
            ddlAuthor.DataTextField = "s_fullname";
            ddlAuthor.DataValueField = "s_ID";
            ddlAuthor.DataBind();
            ListItem lst2 = new ListItem("Select Venue (Booth)", "0");
            ddlAuthor.Items.Insert(ddlAuthor.Items.Count - ddlAuthor.Items.Count, lst2);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ep_ID");
                dt.Columns.Add("ep_title");
                dt.Columns.Add("ep_category");
                dt.Columns.Add("ep_author");
                dt.Columns.Add("ep_coauthor");
                dt.Columns.Add("ep_venue");
                dt.Columns.Add("ep_duration");
                dt.Columns.Add("ep_file");
                dt.Columns.Add("ep_abstract");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");


                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();

                    dr["ep_ID"] = hfid.Value.ToString();
                    dr["ep_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["ep_category"] = daldata.replaceForSQL(txtCategory.Text.Trim());
                    dr["ep_author"] = ddlAuthor.SelectedValue;
                    dr["ep_coauthor"] = daldata.replaceForSQL(txtCoauthor.Text.Trim());
                    dr["ep_venue"] = ddlvenue.SelectedValue;
                    dr["ep_duration"] = daldata.replaceForSQL(txtDuration.Text.Trim());
                    dr["ep_file"] = imagename == "" ? lblfile.Text : imagename;                 
                    dr["ep_abstract"] = daldata.replaceForSQL(txtbrief.Content.Trim());                 
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;


                    int res = daldata.UpdateEPoster(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdEPoster_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["ep_ID"] = "";
                    dr["ep_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["ep_category"] = daldata.replaceForSQL(txtCategory.Text.Trim());
                    dr["ep_author"] = ddlAuthor.SelectedValue;
                    dr["ep_coauthor"] = daldata.replaceForSQL(txtCoauthor.Text.Trim());
                    dr["ep_venue"] = ddlvenue.SelectedValue;
                    dr["ep_duration"] = daldata.replaceForSQL(txtDuration.Text.Trim());
                    dr["ep_file"] = imagename == "" ? lblfile.Text : imagename;
                    dr["ep_abstract"] = daldata.replaceForSQL(txtbrief.Content.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;


                    int res = daldata.SaveEPoster(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdEPoster_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtTitle.Text.Trim()))
            {
                lblerrTitle.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrTitle.Visible = false;
            }
            if (string.IsNullOrEmpty(txtCategory.Text.Trim()) || string.IsNullOrWhiteSpace(txtCategory.Text.Trim()))
            {
                lblerrCategory.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrCategory.Visible = false;
            }
            //if (string.IsNullOrEmpty(txtCoauthor.Text.Trim()) || string.IsNullOrWhiteSpace(txtCoauthor.Text.Trim()))
            //{
                //lblerrcoauthor.Visible = true;
                //rtn = false;
            //}
            //else
            //{
                //lblerrcoauthor.Visible = false;
            //}
            if (string.IsNullOrEmpty(txtDuration.Text.Trim()) || string.IsNullOrWhiteSpace(txtDuration.Text.Trim()))
            {
                lblerrDuration.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrDuration.Visible = false;
            }
            if (ddlAuthor.SelectedValue == "0")
            {
                lblerrdllA.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdllA.Visible = false;
            }
            if (ddlvenue.SelectedValue == "0")
            {
                lblerrdllV.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdllV.Visible = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdEPoster.ExportSettings.ExportOnlyData = true;
        this.grdEPoster.ExportSettings.IgnorePaging = true;
        this.grdEPoster.ExportSettings.OpenInNewWindow = true;
        this.grdEPoster.MasterTableView.ExportToExcel();

        grdEPoster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdEPoster.ExportSettings.FileName = "E-Poster";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdEPoster_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getEPoster();
            grdEPoster.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdEPoster_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdEPoster.CurrentPageIndex + 1;
    }
    protected void grdEPoster_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getEPosterbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getEPoster();
            }


            grdEPoster.DataSource = dt;
            grdEPoster.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdEPoster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdEPoster.ExportSettings.ExportOnlyData = true;
            this.grdEPoster.ExportSettings.IgnorePaging = true;
            this.grdEPoster.ExportSettings.OpenInNewWindow = true;
            this.grdEPoster.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getEPosterlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["ep_ID"].ToString();
                    txtTitle.Text = dt.Rows[0]["ep_title"].ToString();
                    txtCategory.Text = dt.Rows[0]["ep_category"].ToString();
                    txtCoauthor.Text = dt.Rows[0]["ep_coauthor"].ToString();
                    txtDuration.Text = dt.Rows[0]["ep_duration"].ToString();
                    lblfile.Text = dt.Rows[0]["ep_file"].ToString();
                    txtbrief.Content = dt.Rows[0]["ep_abstract"].ToString();
                    ddlAuthor.SelectedValue = dt.Rows[0]["ep_author"].ToString();
                    ddlvenue.SelectedValue = dt.Rows[0]["ep_venue"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteEPosterByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdEPoster_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdEPoster_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtTitle.Text = "";
        txtCategory.Text = "";
        ddlAuthor.SelectedIndex = 0;
        txtCoauthor.Text = "";
        ddlvenue.SelectedIndex = 0;
        txtDuration.Text = "";
        txtbrief.Content = "";
        lblfile.Text = "";
    }
}