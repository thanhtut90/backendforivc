﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="SystemConfiguration.aspx.cs" Inherits="Admin_SystemConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Website Configuration Form</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                <div class="form-group">                      
                    <div class="col-sm-12">
                    <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                </div>   
                    </div>
                </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Website Full Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtfullname" class="form-control" placeholder="Enter Website Full Name" runat="server"></asp:TextBox>
                    </div>
                </div>        
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Website Prefix Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtprefix" class="form-control" placeholder="Enter Website Prefix Name" runat="server"></asp:TextBox>
                    </div>
                </div>              
                <div class="form-group">
                <label class="col-sm-2 control-label">Duration</label>
                    &nbsp;<div class="col-sm-6">
                        <div class="input-group" runat="server" id="duration">
                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control pull-right" id="reservationtime" name="n_test" >                                
                        </div>
                        <asp:Label ID="daterange" class="form-control" visible="false" runat="server"></asp:Label>
                        <asp:LinkButton ID="lnkreset" Visible="false" OnClick="lnkreset_Click" runat="server">Reset</asp:LinkButton>
                        <label id="lblerr" style="color:red" runat="server" visible="false">* Please Set Duration</label>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Home Page Layout</label>
                    <div class="col-sm-6">
                        <asp:RadioButtonList ID="rdolayout" runat="server">
                            <asp:ListItem Value="1">&nbsp;H-Menu</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;V-Menu</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Banner (1024 x 1024)</label>
                    <div class="col-sm-6">                       
                    <asp:Label ID="lblbanner" class="form-control" runat="server"></asp:Label>
                    <asp:FileUpload ID="fupbanner" runat="server" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Site Icon (1024 x 1024)</label>
                    <div class="col-sm-6">                       
                    <asp:Label ID="lblicon" class="form-control" runat="server"></asp:Label>
                    <asp:FileUpload ID="fupicon" runat="server" />
                    </div>
                </div>                                   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Show Page Banner ?</label>
                    <div class="col-sm-6">
                        <asp:RadioButtonList ID="rdoshowbanner" runat="server">
                            <asp:ListItem Value="1">&nbsp;Yes</asp:ListItem>
                            <asp:ListItem Value="2">&nbsp;No</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">SMTP Client</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtsmtpclient" class="form-control" placeholder="Enter SMTP Client" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">SMTP User Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtsmtpusername" class="form-control" placeholder="Enter SMTP User Name" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">SMTP Password</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtsmtppassword" TextMode="Password" class="form-control" placeholder="Enter SMTP Password" runat="server"></asp:TextBox>
                    </div>
                </div>            
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Closing Message</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtclosingmessage" class="form-control"  placeholder="Enter Closing Message" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Copyright</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtcopyright" class="form-control"  placeholder="Enter Copyright" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">IP Address</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtip" class="form-control"  placeholder="Enter IP Address" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Domain Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtdomain" class="form-control"  placeholder="Enter Domain Name" runat="server"></asp:TextBox>
                    </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">                                
                <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
              </div>
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>

