﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_conferencespeakers : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblConferenceSpeakers";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdConferenceSpeaker.ExportSettings.IgnorePaging = true;
                grdConferenceSpeaker.ExportSettings.ExportOnlyData = true;
                grdConferenceSpeaker.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdConferenceSpeaker.ExportSettings.FileName = "ConferenceSpeaker";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtCat = daldata.getspeakercategory();
            ddlCategory.DataSource = dtCat;
            ddlCategory.DataTextField = "speakerCat_name";
            ddlCategory.DataValueField = "speakerCat_ID";
            ddlCategory.DataBind();
            ListItem lstCat = new ListItem("Select Category", "");
            ddlCategory.Items.Insert(ddlCategory.Items.Count - ddlCategory.Items.Count, lstCat);

            DataTable dtConf = daldata.getConference();
            for (int x = 0; x < dtConf.Rows.Count; x++)
            {
                ddlConference.Items.Add(dtConf.Rows[x]["c_title"].ToString() + "[" + dtConf.Rows[x]["c_date"] + "-" + dtConf.Rows[x]["c_stime"] + "-" + dtConf.Rows[x]["c_etime"] + "]");
                ddlConference.Items[x].Value = dtConf.Rows[x]["c_ID"].ToString();
            }
            //ddlConference.DataSource = dtConf;
            //ddlConference.DataTextField = "c_title";
            //ddlConference.DataValueField = "c_ID";
            //ddlConference.DataBind();
            ListItem lstConf = new ListItem("Select Conference", "");
            ddlConference.Items.Insert(ddlConference.Items.Count - ddlConference.Items.Count, lstConf);

            DataTable dtSpk = daldata.getSpeaker();
            ddlSpeaker.DataSource = dtSpk;
            ddlSpeaker.DataTextField = "s_FullName";
            ddlSpeaker.DataValueField = "s_ID";
            ddlSpeaker.DataBind();
            ListItem lstSpk = new ListItem("Select Speaker", "");
            ddlSpeaker.Items.Insert(ddlSpeaker.Items.Count - ddlSpeaker.Items.Count, lstSpk);

            DataTable dtConfCat = daldata.getConferenceCategory();
            ddlConferenceCategory.DataSource = dtConfCat;
            ddlConferenceCategory.DataTextField = "cc_categoryname";
            ddlConferenceCategory.DataValueField = "cc_ID";
            ddlConferenceCategory.DataBind();
            ListItem lstConfCat = new ListItem("Select Conference Category", "");
            ddlConferenceCategory.Items.Insert(ddlConferenceCategory.Items.Count - ddlConferenceCategory.Items.Count, lstConfCat);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("cspeaker_ID");
                dt.Columns.Add("cspeaker_category");
                dt.Columns.Add("cspeaker_conferenceID");
                dt.Columns.Add("cspeaker_speakerID");
                dt.Columns.Add("cspeaker_confcategory");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("sorting");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["cspeaker_ID"] = hfid.Value.ToString();
                    dr["cspeaker_category"] = ddlCategory.SelectedValue.ToString();
                    dr["cspeaker_conferenceID"] = ddlConference.SelectedValue.ToString();
                    dr["cspeaker_speakerID"] = ddlSpeaker.SelectedValue.ToString();
                    dr["cspeaker_confcategory"] = ddlConferenceCategory.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["sorting"] = !string.IsNullOrEmpty(txtseq.Text) && !string.IsNullOrWhiteSpace(txtseq.Text) ? txtseq.Text.Trim() : "99";
                    int res = daldata.UpdateConferenceSpeaker(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceSpeaker_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["cspeaker_ID"] = "";
                    dr["cspeaker_category"] = daldata.replaceForSQL(ddlCategory.SelectedValue.ToString());
                    dr["cspeaker_conferenceID"] = ddlConference.SelectedValue.ToString();
                    dr["cspeaker_speakerID"] = ddlSpeaker.SelectedValue.ToString();
                    dr["cspeaker_confcategory"] = ddlConferenceCategory.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["sorting"] = !string.IsNullOrEmpty(txtseq.Text) && !string.IsNullOrWhiteSpace(txtseq.Text) ? txtseq.Text.Trim() : "99";
                    int res = daldata.SaveConferenceSpeaker(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceSpeaker_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (ddlCategory.SelectedItem.Value == "")
            {
                lblerrCategory.Visible = true;
                rtn = false;
            }
            if (ddlConference.SelectedItem.Value == "")
            {
                lblerrConference.Visible = true;
                rtn = false;
            }
            if (ddlSpeaker.SelectedItem.Value == "")
            {
                lblerrSpeaker.Visible = true;
                rtn = false;
            }
            if (ddlConferenceCategory.SelectedItem.Value == "")
            {
                lblerrConferenceCategory.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdConferenceSpeaker.ExportSettings.ExportOnlyData = true;
        this.grdConferenceSpeaker.ExportSettings.IgnorePaging = true;
        this.grdConferenceSpeaker.ExportSettings.OpenInNewWindow = true;
        this.grdConferenceSpeaker.MasterTableView.ExportToExcel();

        grdConferenceSpeaker.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdConferenceSpeaker.ExportSettings.FileName = "ConferenceSpeaker";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdConferenceSpeaker_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getConferenceSpeaker();
            grdConferenceSpeaker.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceSpeaker_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdConferenceSpeaker.CurrentPageIndex + 1;
    }
    protected void grdConferenceSpeaker_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getConferenceSpeakerbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getConferenceSpeaker();
            }

            grdConferenceSpeaker.DataSource = dt;
            grdConferenceSpeaker.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceSpeaker_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdConferenceSpeaker.ExportSettings.ExportOnlyData = true;
            this.grdConferenceSpeaker.ExportSettings.IgnorePaging = true;
            this.grdConferenceSpeaker.ExportSettings.OpenInNewWindow = true;
            this.grdConferenceSpeaker.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getConferenceSpeakerlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["cspeaker_ID"].ToString();
                    string catID = dt.Rows[0]["cspeaker_category"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(catID))
                        {
                            ListItem listItem = ddlCategory.Items.FindByValue(catID);
                            if (listItem != null)
                            {
                                ddlCategory.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string confID = dt.Rows[0]["cspeaker_conferenceID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(confID))
                        {
                            ListItem listItem = ddlConference.Items.FindByValue(confID);
                            if (listItem != null)
                            {
                                ddlConference.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string spkID = dt.Rows[0]["cspeaker_speakerID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(spkID))
                        {
                            ListItem listItem = ddlSpeaker.Items.FindByValue(spkID);
                            if (listItem != null)
                            {
                                ddlSpeaker.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string confcatID = dt.Rows[0]["cspeaker_confcategory"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(confcatID))
                        {
                            ListItem listItem = ddlConferenceCategory.Items.FindByValue(confcatID);                            
                            if (listItem != null)
                            {
                                ddlConferenceCategory.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    txtseq.Text = dt.Rows[0]["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dt.Rows[0]["sorting"].ToString()) ? dt.Rows[0]["sorting"].ToString() : "99") : "99";
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteConferenceSpeakerByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdConferenceSpeaker_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdConferenceSpeaker_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtCategory.Text = "";
        ddlConference.SelectedIndex = 0;
        ddlSpeaker.SelectedIndex = 0;
        ddlConferenceCategory.SelectedIndex = 0;
        setInvisibleErrMsg();
        txtseq.Text = "";
    }
    private void setInvisibleErrMsg()
    {
        lblerrCategory.Visible = false;
        lblerrConference.Visible = false;
        lblerrSpeaker.Visible = false;
        lblerrConferenceCategory.Visible = false;
    }
}