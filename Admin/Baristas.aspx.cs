﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Baristas : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblBaristas";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdbaristas.ExportSettings.IgnorePaging = true;
                grdbaristas.ExportSettings.ExportOnlyData = true;
                grdbaristas.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdbaristas.ExportSettings.FileName = "Baristas";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("s_ID");
                dt.Columns.Add("s_fullname");
                dt.Columns.Add("s_job");
                dt.Columns.Add("s_company");
                dt.Columns.Add("s_email");
                dt.Columns.Add("s_mobile");
                dt.Columns.Add("s_address");
                dt.Columns.Add("s_country");
                dt.Columns.Add("s_bio");
                dt.Columns.Add("s_profilepic");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");



                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();

                    dr["s_ID"] = hfid.Value.ToString();
                    dr["s_fullname"] = daldata.replaceForSQL(txtBaristas.Text.Trim());
                    dr["s_job"] = daldata.replaceForSQL(txtjob.Text.Trim());
                    dr["s_company"] = daldata.replaceForSQL(txtcompany.Text.Trim());
                    dr["s_email"] = daldata.replaceForSQL(txtemail.Text.Trim());
                    dr["s_mobile"] = daldata.replaceForSQL(txtmobile.Text.Trim());
                    dr["s_address"] = daldata.replaceForSQL(txtaddress.Text.Trim());
                    dr["s_country"] = daldata.replaceForSQL(txtcountry.Text.Trim());
                    dr["s_bio"] = daldata.replaceForSQL(txtbio.Content.Trim());
                    dr["s_profilepic"] = imagename == "" ? lblLogo.Text : imagename;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;


                    int res = daldata.UpdateBaristas(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdbaristas_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["s_ID"] = "";
                    dr["s_fullname"] = daldata.replaceForSQL(txtBaristas.Text.Trim());
                    dr["s_job"] = daldata.replaceForSQL(txtjob.Text.Trim());
                    dr["s_company"] = daldata.replaceForSQL(txtcompany.Text.Trim());
                    dr["s_email"] = daldata.replaceForSQL(txtemail.Text.Trim());
                    dr["s_mobile"] = daldata.replaceForSQL(txtmobile.Text.Trim());
                    dr["s_address"] = daldata.replaceForSQL(txtaddress.Text.Trim());
                    dr["s_country"] = daldata.replaceForSQL(txtcountry.Text.Trim());
                    dr["s_bio"] = daldata.replaceForSQL(txtbio.Content.Trim());
                    dr["s_profilepic"] = imagename == "" ? lblLogo.Text : imagename;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.SaveBaristas(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdbaristas_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtBaristas.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdbaristas.ExportSettings.ExportOnlyData = true;
        this.grdbaristas.ExportSettings.IgnorePaging = true;
        this.grdbaristas.ExportSettings.OpenInNewWindow = true;
        this.grdbaristas.MasterTableView.ExportToExcel();

        grdbaristas.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdbaristas.ExportSettings.FileName = "Baristas";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdbaristas_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getBaristas();
            grdbaristas.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdbaristas_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdbaristas.CurrentPageIndex + 1;
    }
    protected void grdbaristas_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getBaristasbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getBaristas();
            }


            grdbaristas.DataSource = dt;
            grdbaristas.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdbaristas_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdbaristas.ExportSettings.ExportOnlyData = true;
            this.grdbaristas.ExportSettings.IgnorePaging = true;
            this.grdbaristas.ExportSettings.OpenInNewWindow = true;
            this.grdbaristas.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getBaristasByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["s_ID"].ToString();
                    txtBaristas.Text = dt.Rows[0]["s_fullname"].ToString();
                    txtjob.Text = dt.Rows[0]["s_job"].ToString();
                    txtcompany.Text = dt.Rows[0]["s_company"].ToString();
                    txtemail.Text = dt.Rows[0]["s_email"].ToString();
                    txtmobile.Text = dt.Rows[0]["s_mobile"].ToString();
                    txtaddress.Text = dt.Rows[0]["s_address"].ToString();
                    txtcountry.Text = dt.Rows[0]["s_country"].ToString();
                    txtbio.Content = dt.Rows[0]["s_bio"].ToString();
                    lblLogo.Text = dt.Rows[0]["s_profilepic"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteBaristasByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdbaristas_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdbaristas_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public string getFileLink(string filename)
    {
        string filelink = string.Empty;
        if (!string.IsNullOrEmpty(filename))
        {
            filelink = Constant.URL + Constant.filesavingurlview + filename;
        }

        return filelink;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtBaristas.Text = "";
        txtjob.Text = "";
        txtcompany.Text = "";
        txtemail.Text = "";
        txtmobile.Text = "";
        txtaddress.Text = "";
        txtcountry.Text = "";
        txtbio.Content = "";
        lblLogo.Text = "";
    }
}