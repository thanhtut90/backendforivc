﻿using System;
using System.Data;
using App.Object;
using App.DAL;
using App.Common;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Web;

public partial class Admin_UploadFrontEndData : System.Web.UI.Page
{
    #region Declaraion
    DAL dal = new DAL();
    DAL_Parent dalp = new DAL_Parent();
    DAL_Child dalc = new DAL_Child();

    DAL_FrontEnd dalf = new DAL_FrontEnd();
    ChildModel cmodel = new ChildModel();
    ParentModel pmodel = new ParentModel();
    BackEnd_Parent Pobj = new BackEnd_Parent();
    BackEnd_Child Cobj = new BackEnd_Child();
    FrontEnd Fobj = new FrontEnd();
    GenKey gk = new GenKey();
    DAL_Data daldata = new DAL_Data();

    private static string _sponsorCategory = "s_category";
    private static string _isExh = "s_exhID";
    private static string _sponsorName = "s_name";
    private static string _conferenceCategory = "c_cc";
    private static string _cdate = "c_date";
    private static string _conStartTime = "c_stime";
    private static string _conEndTime = "c_etime";
    private static string _cvenue = "c_venue";
    private static string _evenue = "e_venue";
    private static string _ec_booth = "ec_booth";
    private static string _mainConference = "c_parentConfID";
    private static string _innExh = "Ino_exhID";
    private static string _locationmenuID = "MU10014";
    private static string _hall = "ec_hall";
    private static string _exhmarketseg = "ec_marketseg";
    private static string _exhproduct = "ec_product";
    private static string _exhtrends = "ec_trends";
    private static string _hall1 = "Impact Hall 1";
    private static string _hall2 = "Impact Hall 2";
    private static string _hall3 = "Impact Hall 3";
    private static string _hall4 = "Impact Hall 4";
    private static string _hall5 = "Impact Hall 5";
    private static string _hall6 = "Impact Hall 6";
    private static string _hall7 = "Impact Hall 7";
    private static string _hall8 = "Impact Hall 8";
    private static string[] _strhall1 = new string[] { "IH1" };
    private static string[] _strhall2 = new string[] { "IH2" };
    private static string[] _strhall3 = new string[] { "IH3" };
    private static string[] _strhall4 = new string[] { "IH4" };
    private static string[] _strhall5 = new string[] { "IH5" };
    private static string[] _strhall6 = new string[] { "IH6" };
    private static string[] _strhall7 = new string[] { "IH7" };
    private static string[] _strhall8 = new string[] { "IH8" };
    private static string _hallC1 = "Challenger 1";
    private static string _hallC2 = "Challenger 2";
    private static string _hallC3 = "Challenger 3";
    private static string _strhallC1 = "C1";
    private static string _strhallC2 = "C2";
    private static string _strhallC3 = "C3";
    private static string _conspkConferenceID = "cspeaker_conferenceID";
    private static string _conspkSpeakerID = "cspeaker_speakerID";
    private static string _conspkConferenceCategory = "cspeaker_confcategory";
    private static string _conspkSpeakerCategory = "cspeaker_category";
    private static string _conspnConferenceID = "csponsor_conferenceID";
    private static string _conspnSponsorID = "csponsor_sponsorID";
    private static string _conspnSponsorCategory = "csponsor_scategory";

    private static string _Ino_QCategories = "Ino_QCategories";
    private static string _Ino_certification = "Ino_certification";
    private static string _Ino_region = "Ino_region";

    private static string _DentalSector = "DentalSector";
    private static string _DentalPractice = "DentalPractice";
    private static string _DentalLab = "DentalLab";
    private static string _SubProductList_InfectionCntrl = "SubProductList_InfectionCntrl";
    private static string _SrvsInfoCommAndOrg = "SrvsInfoCommAndOrg";
    private static string _Distributors = "Distributors";
    private static string _ProductDescription = "ProductDescription";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindDDL();
            #region Alert Message
            if (Request.QueryString["s"] != null)
            {
                if (Request.QueryString["rtn"].ToString() == "suc")
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Save Successful!')", true);
                }
                else
                {
                    var message = Request.QueryString["Msg"].ToString();
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('" + message + "')", true);                   
                }
            }            
            #endregion
        }
    }
    #endregion

    #region Bind Parent
    private void BindDDL()
    {
        try
        {
            DataTable dttablelist = daldata.getAllActivatedTable();
            ddlparent.DataSource = dttablelist;
            ddlparent.DataValueField = "a_activatedFormName";
            ddlparent.DataTextField = "a_tableName";
            ddlparent.DataBind();
            ListItem itm = new ListItem("Select Form Below", "0");
            ddlparent.Items.Insert(ddlparent.Items.Count - ddlparent.Items.Count, itm);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Submit Data
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Functionality fn = new Functionality();
            if (Session["DT"] != null && ddlparent.SelectedValue != "")
            {
                DateTime dtime = DateTime.Now;
                StringBuilder InsertStr = new StringBuilder();
                StringBuilder UpdateStr = new StringBuilder();
                string timestamp = dalf.ConvertToTimestamp(dtime);
                DataTable dt = Session["DT"] as DataTable;
                int result = 0;
                if (dt.Rows.Count > 0)
                {
                    string tablename = ddlparent.SelectedValue.ToString();

                    foreach (DataRow dr in dt.Rows)
                    {
                        string checkvalue = string.Empty; string checkkey = string.Empty;
                        #region tblExhibitingCompany
                        if (tablename == "tblExhibitingCompany")
                        {
                            string template = HttpContext.Current.Server.MapPath("~/Template/IDEMProductListTemplate.html");
                            string readTemplate = System.IO.File.ReadAllText(template);
                            string productHTML = HttpContext.Current.Server.HtmlDecode(readTemplate);
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _ec_booth)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string locHallID = string.Empty;
                                        if (!string.IsNullOrEmpty(dr[_hall].ToString().Trim()))
                                        {
                                            //string hallName = getLocationHallName(dr[_hall].ToString().Trim());
                                            //string lochallName = !string.IsNullOrEmpty(hallName) ? hallName : dr[_hall].ToString().Trim();
                                            string lochallName = daldata.getHallByID(dr[_hall].ToString().Trim());
                                            //locHallID = daldata.getHallIDByName(lochallName);
                                            locHallID = dr[_hall].ToString().Trim();
                                            if (locHallID == "0")
                                            {
                                                #region add LogcationHall
                                                DataTable dtL = new DataTable();
                                                dtL.Columns.Add("l_ID");
                                                dtL.Columns.Add("l_name");
                                                dtL.Columns.Add("l_image");
                                                dtL.Columns.Add("menuID");
                                                dtL.Columns.Add("lang");
                                                dtL.Columns.Add("deleteFlag");
                                                dtL.Columns.Add("UpdatedDate");
                                                DataRow drL = dtL.NewRow();
                                                drL["l_ID"] = "";
                                                drL["l_name"] = daldata.replaceForSQL(lochallName);
                                                drL["l_image"] = "";
                                                drL["menuID"] = _locationmenuID;
                                                drL["lang"] = "1";
                                                drL["deleteFlag"] = false;
                                                drL["UpdatedDate"] = DateTime.Now;
                                                int res = daldata.SaveHall(drL);
                                                locHallID = daldata.getHallIDByName(lochallName);
                                                #endregion
                                            }
                                        }
                                        string venuID = string.Empty;
                                        if (!string.IsNullOrEmpty(locHallID))
                                        {
                                            venuID = daldata.getBoothIDByNameAndLocationHall(dr[dataColumn].ToString().Trim(), locHallID);
                                        }
                                        else
                                        {
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                        }
                                        if (venuID == "0")
                                        {
                                            #region add Venu
                                            DataTable dtV = new DataTable();
                                            dtV.Columns.Add("vb_ID");
                                            dtV.Columns.Add("vb_name");
                                            dtV.Columns.Add("vb_location");
                                            dtV.Columns.Add("vb_width");
                                            dtV.Columns.Add("vb_height");
                                            dtV.Columns.Add("vb_x");
                                            dtV.Columns.Add("vb_y");
                                            dtV.Columns.Add("lang");
                                            dtV.Columns.Add("deleteFlag");
                                            dtV.Columns.Add("UpdatedDate");
                                            DataRow drV = dtV.NewRow();
                                            drV["vb_ID"] = "";
                                            drV["vb_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drV["vb_location"] = locHallID;
                                            drV["vb_width"] = "";
                                            drV["vb_height"] = "";
                                            drV["vb_x"] = "";
                                            drV["vb_y"] = "";
                                            drV["lang"] = "1";
                                            drV["deleteFlag"] = false;
                                            drV["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveBooth(drV);
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = venuID == "0" ? "" : venuID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if(dataColumn.ColumnName == _hall)
                                {
                                    string locHallID = string.Empty;
                                    if (!string.IsNullOrEmpty(dr[_hall].ToString().Trim()))
                                    {
                                        string hallName = getLocationHallName(dr[_hall].ToString().Trim());
                                        string lochallName = !string.IsNullOrEmpty(hallName) ? hallName : dr[_hall].ToString().Trim();
                                        locHallID = daldata.getHallIDByName(lochallName);
                                        if (locHallID == "0")
                                        {
                                            #region add LogcationHall
                                            DataTable dtL = new DataTable();
                                            dtL.Columns.Add("l_ID");
                                            dtL.Columns.Add("l_name");
                                            dtL.Columns.Add("l_image");
                                            dtL.Columns.Add("menuID");
                                            dtL.Columns.Add("lang");
                                            dtL.Columns.Add("deleteFlag");
                                            dtL.Columns.Add("UpdatedDate");
                                            DataRow drL = dtL.NewRow();
                                            drL["l_ID"] = "";
                                            drL["l_name"] = daldata.replaceForSQL(lochallName);
                                            drL["l_image"] = "";
                                            drL["menuID"] = _locationmenuID;
                                            drL["lang"] = "1";
                                            drL["deleteFlag"] = false;
                                            drL["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveHall(drL);
                                            locHallID = daldata.getHallIDByName(lochallName);
                                            #endregion
                                        }
                                        dr[dataColumn] = locHallID == "0" ? "" : locHallID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else
                                {
                                    if (dataColumn.ColumnName == _exhmarketseg || dataColumn.ColumnName == _exhproduct || dataColumn.ColumnName == _exhtrends)
                                    {
                                        dr[dataColumn] = dr[dataColumn].ToString().Trim().Replace("; ",";");
                                    }
                                    else if(dataColumn.ColumnName == _DentalSector || dataColumn.ColumnName == _DentalPractice
                                        || dataColumn.ColumnName == _DentalLab || dataColumn.ColumnName == _SubProductList_InfectionCntrl 
                                        || dataColumn.ColumnName == _SrvsInfoCommAndOrg || dataColumn.ColumnName == _Distributors
                                        || dataColumn.ColumnName == _ProductDescription)
                                    {
                                        string productList = dr[dataColumn].ToString().Trim().Replace("; ", ";");
                                        ConvertProductListToHtmlList(productList, dataColumn.ColumnName, ref productHTML);
                                    }
                                    else
                                    {
                                        dr[dataColumn] = dr[dataColumn].ToString().Trim();
                                    }

                                }
                            }
                            dr["ec_productHTML"] = productHTML.ToString().Trim();/*added on 20-2-2020 th*/

                            //checkvalue = dr["ec_name"].ToString().Trim();
                            //checkkey = "ec_name";
                            //string checkhall = dr["ec_hall"].ToString().Trim();
                            //string checkbooth = dr["ec_booth"].ToString().Trim();
                            //string whereclause = " And ec_hall='" + checkhall + "' And ec_booth='" + checkbooth + "'";
                            string checkClientExhID = dr["clientID"].ToString().Trim();
                            string whereclause = " And clientID='" + checkClientExhID + "'";
                            bool isExist = !string.IsNullOrEmpty(checkClientExhID) ? daldata.CheckExistExhByClientID(tablename, whereclause) : false;
                            //daldata.CheckExist(tablename, checkkey, checkvalue, whereclause)
                            if (isExist == true)
                            {
                                //Update
                                result += daldata.UpdateExhibitingCompany(dr, true);
                                updateProductHTML(dr["clientID"].ToString().Trim(), productHTML.ToString().Trim());/*added on 20-2-2020 th*/
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveExhibitingCompany(dr, true);
                                updateProductHTML(dr["clientID"].ToString().Trim(), productHTML.ToString().Trim());/*added on 20-2-2020 th*/
                            }
                        }
                        #endregion

                        #region tblSponsor
                        if (tablename == "tblSponsor")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _sponsorCategory)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string spnCatID = daldata.getsponsorcategoryIDByName(dr[dataColumn].ToString().Trim());
                                        if (spnCatID == "0")
                                        {
                                            #region add Conference Category
                                            spnCatID = string.Empty;
                                            DataTable dtSC = new DataTable();
                                            dtSC.Columns.Add("sponsorCat_ID");
                                            dtSC.Columns.Add("sponsorCat_name");
                                            dtSC.Columns.Add("sponsorCat_seq");
                                            dtSC.Columns.Add("lang");
                                            dtSC.Columns.Add("deleteFlag");
                                            dtSC.Columns.Add("UpdatedDate");
                                            DataRow drSC = dtSC.NewRow();
                                            drSC["sponsorCat_ID"] = "";
                                            drSC["sponsorCat_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drSC["sponsorCat_seq"] = "99";
                                            drSC["lang"] = "1";
                                            drSC["deleteFlag"] = false;
                                            drSC["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveSponsorCategoryGetID(drSC, ref spnCatID);
                                            #endregion
                                        }
                                        dr[dataColumn] = spnCatID == "0" ? "" : spnCatID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _isExh)
                                {
                                    if (dr[dataColumn].ToString().Trim() == "1")
                                    {
                                        string extID = daldata.getexhibitingcompanyIDByName(dr[_sponsorName].ToString().Trim());
                                        if (extID == "0")
                                        {
                                            #region add ExhibitingCompany
                                            DataTable dtE = new DataTable();
                                            dtE.Columns.Add("ec_ID");
                                            dtE.Columns.Add("ec_name");
                                            dtE.Columns.Add("ec_address");
                                            dtE.Columns.Add("ec_country");
                                            dtE.Columns.Add("ec_email");
                                            dtE.Columns.Add("ec_contactno");
                                            dtE.Columns.Add("ec_website");
                                            dtE.Columns.Add("ec_brochure");
                                            dtE.Columns.Add("ec_logo");
                                            dtE.Columns.Add("ec_companyprofile");
                                            dtE.Columns.Add("ec_companyType");
                                            dtE.Columns.Add("ec_hall");
                                            dtE.Columns.Add("ec_booth");
                                            dtE.Columns.Add("contactpersonID");
                                            dtE.Columns.Add("ec_product");
                                            dtE.Columns.Add("lang");
                                            dtE.Columns.Add("deleteFlag");
                                            dtE.Columns.Add("UpdatedDate");
                                            dtE.Columns.Add("ec_marketseg");
                                            dtE.Columns.Add("ec_trends");
                                            DataRow drE = dtE.NewRow();
                                            drE["ec_ID"] = "";
                                            drE["ec_name"] = daldata.replaceForSQL(dr[_sponsorName].ToString().Trim());
                                            drE["ec_address"] = "";
                                            drE["ec_country"] = "";
                                            drE["ec_email"] = "";
                                            drE["ec_contactno"] = "";
                                            drE["ec_website"] = "";
                                            drE["ec_brochure"] = "";
                                            drE["ec_logo"] = "";
                                            drE["ec_companyprofile"] = "";
                                            drE["ec_companyType"] = "";
                                            drE["ec_hall"] = "";
                                            drE["ec_booth"] = "";
                                            drE["contactpersonID"] = "";
                                            drE["ec_product"] = "";
                                            drE["lang"] = "";
                                            drE["deleteFlag"] = false;
                                            drE["UpdatedDate"] = DateTime.Now;
                                            drE["ec_marketseg"] = "";
                                            drE["ec_trends"] = "";
                                            int res = daldata.SaveExhibitingCompany(drE);
                                            extID = daldata.getexhibitingcompanyIDByName(dr[_sponsorName].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = extID == "0" ? "" : extID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conferenceCategory)
                                {
                                }
                                else
                                {
                                    dr[dataColumn] = dr[dataColumn].ToString().Trim();
                                }
                            }
                            #endregion

                            checkvalue = dr["s_name"].ToString().Trim();
                            checkkey = "s_name";

                            bool isExist = daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["s_ID"] = daldata.getSponsorIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateSponsor(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveSponsor(dr);
                            }
                        }
                        #endregion

                        #region tblConference
                        if (tablename == "tblConference")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _conferenceCategory)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conCatID = daldata.getConferenceCategoryIDbyName(dr[dataColumn].ToString().Trim());
                                        if (conCatID == "0")
                                        {
                                            #region add Conference Category
                                            conCatID = string.Empty;
                                            DataTable dtCC = new DataTable();
                                            dtCC.Columns.Add("cc_ID");
                                            dtCC.Columns.Add("cc_categoryname");
                                            dtCC.Columns.Add("cc_seq");
                                            dtCC.Columns.Add("lang");
                                            dtCC.Columns.Add("deleteFlag");
                                            dtCC.Columns.Add("UpdatedDate");
                                            DataRow drCC = dtCC.NewRow();
                                            drCC["cc_ID"] = "";
                                            drCC["cc_categoryname"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drCC["cc_seq"] = "99";
                                            drCC["lang"] = "1";
                                            drCC["deleteFlag"] = false;
                                            drCC["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveConferenceCategoryGetID(drCC, ref conCatID);
                                            #endregion
                                        }
                                        dr[dataColumn] = conCatID == "0" ? "" : conCatID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _cvenue)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string locHallID = string.Empty;
                                        if (!string.IsNullOrEmpty(dr[_hall].ToString().Trim()))
                                        {
                                            string hallName = getLocationHallName(dr[_hall].ToString().Trim());
                                            string lochallName = !string.IsNullOrEmpty(hallName) ? hallName : dr[_hall].ToString().Trim();
                                            locHallID = daldata.getHallIDByName(lochallName);
                                            if (locHallID == "0")
                                            {
                                                #region add LogcationHall
                                                DataTable dtL = new DataTable();
                                                dtL.Columns.Add("l_ID");
                                                dtL.Columns.Add("l_name");
                                                dtL.Columns.Add("l_image");
                                                dtL.Columns.Add("menuID");
                                                dtL.Columns.Add("lang");
                                                dtL.Columns.Add("deleteFlag");
                                                dtL.Columns.Add("UpdatedDate");
                                                DataRow drL = dtL.NewRow();
                                                drL["l_ID"] = "";
                                                drL["l_name"] = daldata.replaceForSQL(lochallName);
                                                drL["l_image"] = "";
                                                drL["menuID"] = _locationmenuID;
                                                drL["lang"] = "1";
                                                drL["deleteFlag"] = false;
                                                drL["UpdatedDate"] = DateTime.Now;
                                                int res = daldata.SaveHall(drL);
                                                locHallID = daldata.getHallIDByName(lochallName);
                                                #endregion
                                            }
                                        }
                                        string venuID = string.Empty;
                                        if (!string.IsNullOrEmpty(locHallID))
                                        {
                                            venuID = daldata.getBoothIDByNameAndLocationHall(dr[dataColumn].ToString().Trim(), locHallID);
                                        }
                                        else
                                        {
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                        }
                                        if (venuID == "0")
                                        {
                                            #region add Venu
                                            DataTable dtV = new DataTable();
                                            dtV.Columns.Add("vb_ID");
                                            dtV.Columns.Add("vb_name");
                                            dtV.Columns.Add("vb_location");
                                            dtV.Columns.Add("vb_width");
                                            dtV.Columns.Add("vb_height");
                                            dtV.Columns.Add("vb_x");
                                            dtV.Columns.Add("vb_y");
                                            dtV.Columns.Add("lang");
                                            dtV.Columns.Add("deleteFlag");
                                            dtV.Columns.Add("UpdatedDate");
                                            DataRow drV = dtV.NewRow();
                                            drV["vb_ID"] = "";
                                            drV["vb_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drV["vb_location"] = locHallID;
                                            drV["vb_width"] = "";
                                            drV["vb_height"] = "";
                                            drV["vb_x"] = "";
                                            drV["vb_y"] = "";
                                            drV["lang"] = "1";
                                            drV["deleteFlag"] = false;
                                            drV["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveBooth(drV);
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = venuID == "0" ? "" : venuID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _mainConference)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                        if (conID == "0")
                                        {
                                            #region add MainConference
                                            DataTable dtMC = new DataTable();
                                            dtMC.Columns.Add("c_ID");
                                            dtMC.Columns.Add("c_cc");
                                            dtMC.Columns.Add("c_date");
                                            dtMC.Columns.Add("c_stime");
                                            dtMC.Columns.Add("c_etime");
                                            dtMC.Columns.Add("c_title");
                                            dtMC.Columns.Add("c_venue");
                                            dtMC.Columns.Add("c_brief");
                                            dtMC.Columns.Add("c_liveQA");
                                            dtMC.Columns.Add("c_parentConfID");
                                            dtMC.Columns.Add("lang");
                                            dtMC.Columns.Add("deleteFlag");
                                            dtMC.Columns.Add("UpdatedDate");
                                            DataRow drMC = dtMC.NewRow();
                                            drMC["c_ID"] = "";
                                            drMC["c_cc"] = "";
                                            drMC["c_date"] = daldata.replaceForSQL(dr[_cdate].ToString().Trim());
                                            drMC["c_stime"] = "";
                                            drMC["c_etime"] = "";
                                            drMC["c_title"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drMC["c_venue"] = daldata.replaceForSQL(dr[_cvenue].ToString().Trim());
                                            drMC["c_brief"] = "";
                                            drMC["c_liveQA"] = "0";
                                            drMC["c_parentConfID"] = "";
                                            drMC["lang"] = "1";
                                            drMC["deleteFlag"] = false;
                                            drMC["UpdatedDate"] = DateTime.Now;
                                            string confID = "";
                                            int res = daldata.SaveConference(drMC, ref confID);
                                            conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = conID == "0" ? "" : conID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _hall)
                                {
                                    string locHallID = string.Empty;
                                    if (!string.IsNullOrEmpty(dr[_hall].ToString().Trim()))
                                    {
                                        string hallName = getLocationHallName(dr[_hall].ToString().Trim());
                                        string lochallName = !string.IsNullOrEmpty(hallName) ? hallName : dr[_hall].ToString().Trim();
                                        locHallID = daldata.getHallIDByName(lochallName);
                                        if (locHallID == "0")
                                        {
                                            #region add LogcationHall
                                            DataTable dtL = new DataTable();
                                            dtL.Columns.Add("l_ID");
                                            dtL.Columns.Add("l_name");
                                            dtL.Columns.Add("l_image");
                                            dtL.Columns.Add("menuID");
                                            dtL.Columns.Add("lang");
                                            dtL.Columns.Add("deleteFlag");
                                            dtL.Columns.Add("UpdatedDate");
                                            DataRow drL = dtL.NewRow();
                                            drL["l_ID"] = "";
                                            drL["l_name"] = daldata.replaceForSQL(lochallName);
                                            drL["l_image"] = "";
                                            drL["menuID"] = _locationmenuID;
                                            drL["lang"] = "1";
                                            drL["deleteFlag"] = false;
                                            drL["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveHall(drL);
                                            locHallID = daldata.getHallIDByName(lochallName);
                                            #endregion
                                        }
                                        dr[dataColumn] = locHallID == "0" ? "" : locHallID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                #region Date&Time(Comment)
                                //else if (dataColumn.ColumnName == _cdate)
                                //{
                                //    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                //    {
                                //        string dateID = daldata.getDayIDbyName(dr[dataColumn].ToString().Trim());
                                //        if (dateID == "0")
                                //        {
                                //            #region add Date/Day
                                //            dateID = string.Empty;
                                //            DataTable dtD = new DataTable();
                                //            dtD.Columns.Add("d_ID");
                                //            dtD.Columns.Add("d_key");
                                //            dtD.Columns.Add("d_date");
                                //            dtD.Columns.Add("d_year");
                                //            dtD.Columns.Add("deleteFlag");
                                //            dtD.Columns.Add("UpdatedDate");
                                //            DataRow drD = dtD.NewRow();
                                //            drD["d_ID"] = "";
                                //            string daykey = new String(dr[dataColumn].ToString().Trim().Where(Char.IsDigit).ToArray());
                                //            drD["d_key"] = daykey;
                                //            drD["d_date"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                //            drD["d_year"] = "2018";
                                //            drD["deleteFlag"] = false;
                                //            drD["UpdatedDate"] = DateTime.Now;
                                //            int res = daldata.SaveDayGetID(drD, ref dateID);
                                //            #endregion
                                //        }
                                //        dr[dataColumn] = dateID == "0" ? "" : dateID;
                                //    }
                                //    else
                                //    {
                                //        dr[dataColumn] = "";
                                //    }
                                //}
                                //else if (dataColumn.ColumnName == _conStartTime || dataColumn.ColumnName == _conStartTime)
                                //{
                                //    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                //    {
                                //        string timeID = daldata.getTimeIDbyName(dr[dataColumn].ToString().Trim());
                                //        if (timeID == "0")
                                //        {
                                //            #region add Time
                                //            timeID = string.Empty;
                                //            DataTable dtT = new DataTable();
                                //            dtT.Columns.Add("tl_ID");
                                //            dtT.Columns.Add("tl_slot");
                                //            dtT.Columns.Add("lang");
                                //            dtT.Columns.Add("deleteFlag");
                                //            dtT.Columns.Add("UpdatedDate");
                                //            DataRow drT = dtT.NewRow();
                                //            drT["tl_ID"] = "";
                                //            drT["tl_slot"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                //            drT["lang"] = "1";
                                //            drT["deleteFlag"] = false;
                                //            drT["UpdatedDate"] = DateTime.Now;
                                //            int res = daldata.SaveTimeGetID(drT, ref timeID);
                                //            #endregion
                                //        }
                                //        dr[dataColumn] = timeID == "0" ? "" : timeID;
                                //    }
                                //    else
                                //    {
                                //        dr[dataColumn] = "";
                                //    }
                                //}
                                #endregion
                                else
                                {
                                    dr[dataColumn] = dr[dataColumn].ToString().Trim();
                                }
                            }
                            #endregion

                            checkvalue = dr["c_title"].ToString().Trim();
                            checkkey = "c_title";

                            bool isExist = daldata.CheckExistConference(checkvalue, dr["c_date"].ToString().Trim(), dr["c_cc"].ToString().Trim());// daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["c_ID"] = daldata.getConferenceIDByNameDate(checkvalue, dr["c_date"].ToString().Trim(), dr["c_cc"].ToString().Trim());
                                //Update
                                result += daldata.UpdateConference(dr);
                            }
                            else
                            {
                                //Insert
                                string confID = "";
                                result += daldata.SaveConference(dr, ref confID);
                            }
                        }
                        #endregion

                        #region tblEvent
                        if (tablename == "tblEvent")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _evenue)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string locHallID = string.Empty;
                                        if (!string.IsNullOrEmpty(dr[_hall].ToString().Trim()))
                                        {
                                            string hallName = getLocationHallName(dr[_hall].ToString().Trim());
                                            string lochallName = !string.IsNullOrEmpty(hallName) ? hallName : dr[_hall].ToString().Trim();
                                            locHallID = daldata.getHallIDByName(lochallName);
                                            if (locHallID == "0")
                                            {
                                                #region add LogcationHall
                                                DataTable dtL = new DataTable();
                                                dtL.Columns.Add("l_ID");
                                                dtL.Columns.Add("l_name");
                                                dtL.Columns.Add("l_image");
                                                dtL.Columns.Add("menuID");
                                                dtL.Columns.Add("lang");
                                                dtL.Columns.Add("deleteFlag");
                                                dtL.Columns.Add("UpdatedDate");
                                                DataRow drL = dtL.NewRow();
                                                drL["l_ID"] = "";
                                                drL["l_name"] = daldata.replaceForSQL(lochallName);
                                                drL["l_image"] = "";
                                                drL["menuID"] = _locationmenuID;
                                                drL["lang"] = "1";
                                                drL["deleteFlag"] = false;
                                                drL["UpdatedDate"] = DateTime.Now;
                                                int res = daldata.SaveHall(drL);
                                                locHallID = daldata.getHallIDByName(lochallName);
                                                #endregion
                                            }
                                        }
                                        string venuID = string.Empty;
                                        if (!string.IsNullOrEmpty(locHallID))
                                        {
                                            venuID = daldata.getBoothIDByNameAndLocationHall(dr[dataColumn].ToString().Trim(), locHallID);
                                        }
                                        else
                                        {
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                        }
                                        if (venuID == "0")
                                        {
                                            #region add Venu
                                            DataTable dtV = new DataTable();
                                            dtV.Columns.Add("vb_ID");
                                            dtV.Columns.Add("vb_name");
                                            dtV.Columns.Add("vb_location");
                                            dtV.Columns.Add("vb_width");
                                            dtV.Columns.Add("vb_height");
                                            dtV.Columns.Add("vb_x");
                                            dtV.Columns.Add("vb_y");
                                            dtV.Columns.Add("lang");
                                            dtV.Columns.Add("deleteFlag");
                                            dtV.Columns.Add("UpdatedDate");
                                            DataRow drV = dtV.NewRow();
                                            drV["vb_ID"] = "";
                                            drV["vb_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drV["vb_location"] = locHallID;
                                            drV["vb_width"] = "";
                                            drV["vb_height"] = "";
                                            drV["vb_x"] = "";
                                            drV["vb_y"] = "";
                                            drV["lang"] = "1";
                                            drV["deleteFlag"] = false;
                                            drV["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveBooth(drV);
                                            venuID = daldata.getBoothIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = venuID == "0" ? "" : venuID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                #region Date(Comment)
                                //else if (dataColumn.ColumnName == _date)
                                //{
                                //    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                //    {
                                //        string dateID = daldata.getDayIDbyName(dr[dataColumn].ToString().Trim());
                                //        if (dateID == "0")
                                //        {
                                //            #region add Date/Day
                                //            dateID = string.Empty;
                                //            DataTable dtD = new DataTable();
                                //            dtD.Columns.Add("d_ID");
                                //            dtD.Columns.Add("d_key");
                                //            dtD.Columns.Add("d_date");
                                //            dtD.Columns.Add("d_year");
                                //            dtD.Columns.Add("deleteFlag");
                                //            dtD.Columns.Add("UpdatedDate");
                                //            DataRow drD = dtD.NewRow();
                                //            drD["d_ID"] = "";
                                //            string daykey = new String(dr[dataColumn].ToString().Trim().Where(Char.IsDigit).ToArray());
                                //            drD["d_key"] = daykey;
                                //            drD["d_date"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                //            drD["d_year"] = "2018";
                                //            drD["deleteFlag"] = false;
                                //            drD["UpdatedDate"] = DateTime.Now;
                                //            int res = daldata.SaveDayGetID(drD, ref dateID);
                                //            #endregion
                                //        }
                                //        dr[dataColumn] = dateID == "0" ? "" : dateID;
                                //    }
                                //    else
                                //    {
                                //        dr[dataColumn] = "";
                                //    }
                                //}
                                #endregion
                                else
                                {
                                    dr[dataColumn] = dr[dataColumn].ToString().Trim();
                                }
                            }
                            #endregion

                            checkvalue = dr["e_title"].ToString().Trim();
                            checkkey = "e_title";

                            bool isExist = daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["e_ID"] = daldata.getEventIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateEvent(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveEvent(dr);
                            }
                        }
                        #endregion

                        #region tblInnovation
                        if (tablename == "tblInnovation")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _innExh)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string extID = daldata.getexhibitingcompanyIDByName(dr[dataColumn].ToString().Trim());
                                        if (extID == "0")
                                        {
                                            #region add ExhibitingCompany
                                            DataTable dtE = new DataTable();
                                            dtE.Columns.Add("ec_ID");
                                            dtE.Columns.Add("ec_name");
                                            dtE.Columns.Add("ec_address");
                                            dtE.Columns.Add("ec_country");
                                            dtE.Columns.Add("ec_email");
                                            dtE.Columns.Add("ec_contactno");
                                            dtE.Columns.Add("ec_website");
                                            dtE.Columns.Add("ec_brochure");
                                            dtE.Columns.Add("ec_logo");
                                            dtE.Columns.Add("ec_companyprofile");
                                            dtE.Columns.Add("ec_companyType");
                                            dtE.Columns.Add("ec_hall");
                                            dtE.Columns.Add("ec_booth");
                                            dtE.Columns.Add("contactpersonID");
                                            dtE.Columns.Add("ec_product");
                                            dtE.Columns.Add("lang");
                                            dtE.Columns.Add("deleteFlag");
                                            dtE.Columns.Add("UpdatedDate");
                                            dtE.Columns.Add("ec_marketseg");
                                            dtE.Columns.Add("ec_trends");
                                            DataRow drE = dtE.NewRow();
                                            drE["ec_ID"] = "";
                                            drE["ec_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drE["ec_address"] = "";
                                            drE["ec_country"] = "";
                                            drE["ec_email"] = "";
                                            drE["ec_contactno"] = "";
                                            drE["ec_website"] = "";
                                            drE["ec_brochure"] = "";
                                            drE["ec_logo"] = "";
                                            drE["ec_companyprofile"] = "";
                                            drE["ec_companyType"] = "";
                                            drE["ec_hall"] = "";
                                            drE["ec_booth"] = "";
                                            drE["contactpersonID"] = "";
                                            drE["ec_product"] = "";
                                            drE["lang"] = "";
                                            drE["deleteFlag"] = false;
                                            drE["UpdatedDate"] = DateTime.Now;
                                            drE["ec_marketseg"] = "";
                                            drE["ec_trends"] = "";
                                            int res = daldata.SaveExhibitingCompany(drE);
                                            extID = daldata.getexhibitingcompanyIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = extID == "0" ? "" : extID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else
                                {
                                    if (dataColumn.ColumnName == _Ino_QCategories || dataColumn.ColumnName == _Ino_certification || dataColumn.ColumnName == _Ino_region)
                                    {
                                        dr[dataColumn] = dr[dataColumn].ToString().Trim().Replace("; ", ";").TrimEnd(';');
                                    }
                                    else
                                    {
                                        dr[dataColumn] = dr[dataColumn].ToString().Trim();
                                    }
                            }
                        }
                            #endregion
                            checkvalue = dr["Ino_productName"].ToString().Trim();
                            checkkey = "Ino_productName";

                            bool isExist = daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["Ino_ID"] = daldata.getInnovationIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateInnovation(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveInnovation(dr);
                            }
                        }
                        #endregion

                        #region tblBaristas
                        if (tablename == "tblBaristas")
                        {
                            checkvalue = dr["s_fullname"].ToString().Trim();
                            checkkey = "s_fullname";

                            bool isExist = daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["s_ID"] = daldata.getBaristasIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateBaristas(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveBaristas(dr);
                            }
                        }
                        #endregion

                        #region tblSpeaker
                        if (tablename == "tblSpeaker")
                        {
                            checkvalue = dr["s_fullname"].ToString().Trim();
                            checkkey = "s_fullname";

                            bool isExist = daldata.CheckExist(tablename, checkkey, checkvalue);
                            if (isExist == true)
                            {
                                dr["s_ID"] = daldata.getSpeakerIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateSpeaker(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveSpeaker(dr);
                            }
                        }
                        #endregion

                        #region tblConferenceSpeakers
                        if (tablename == "tblConferenceSpeakers")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _conspkConferenceID)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                        if (conID == "0")
                                        {
                                            #region add Conference
                                            DataTable dtC = new DataTable();
                                            dtC.Columns.Add("c_ID");
                                            dtC.Columns.Add("c_cc");
                                            dtC.Columns.Add("c_date");
                                            dtC.Columns.Add("c_stime");
                                            dtC.Columns.Add("c_etime");
                                            dtC.Columns.Add("c_title");
                                            dtC.Columns.Add("c_venue");
                                            dtC.Columns.Add("c_brief");
                                            dtC.Columns.Add("c_liveQA");
                                            dtC.Columns.Add("c_parentConfID");
                                            dtC.Columns.Add("lang");
                                            dtC.Columns.Add("deleteFlag");
                                            dtC.Columns.Add("UpdatedDate");
                                            DataRow drC = dtC.NewRow();
                                            drC["c_ID"] = "";
                                            drC["c_cc"] = "";
                                            drC["c_date"] = "";
                                            drC["c_stime"] = "";
                                            drC["c_etime"] = "";
                                            drC["c_title"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drC["c_venue"] = "";
                                            drC["c_brief"] = "";
                                            drC["c_liveQA"] = "0";
                                            drC["c_parentConfID"] = "";
                                            drC["lang"] = "1";
                                            drC["deleteFlag"] = false;
                                            drC["UpdatedDate"] = DateTime.Now;
                                            string confID = "";
                                            int res = daldata.SaveConference(drC, ref confID);
                                            conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = conID == "0" ? "" : conID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conspkSpeakerID)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conID = daldata.getSpeakerIDByName(dr[dataColumn].ToString().Trim());
                                        if (conID == "0")
                                        {
                                            #region add Speaker
                                            DataTable dtS = new DataTable();
                                            dtS.Columns.Add("s_ID");
                                            dtS.Columns.Add("s_fullname");
                                            dtS.Columns.Add("s_job");
                                            dtS.Columns.Add("s_company");
                                            dtS.Columns.Add("s_email");
                                            dtS.Columns.Add("s_mobile");
                                            dtS.Columns.Add("s_address");
                                            dtS.Columns.Add("s_country");
                                            dtS.Columns.Add("s_bio");
                                            dtS.Columns.Add("s_profilepic");
                                            dtS.Columns.Add("lang");
                                            dtS.Columns.Add("deleteFlag");
                                            dtS.Columns.Add("UpdatedDate");
                                            DataRow drS = dtS.NewRow();
                                            drS["s_ID"] = "";
                                            drS["s_fullname"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drS["s_job"] = "";
                                            drS["s_company"] = "";
                                            drS["s_email"] = "";
                                            drS["s_mobile"] = "";
                                            drS["s_address"] = "";
                                            drS["s_country"] = "";
                                            drS["s_bio"] = "";
                                            drS["s_profilepic"] = "";
                                            drS["lang"] = "1";
                                            drS["deleteFlag"] = false;
                                            drS["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveSpeaker(drS);
                                            conID = daldata.getSpeakerIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = conID == "0" ? "" : conID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conspkConferenceCategory)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conCatID = daldata.getConferenceCategoryIDbyName(dr[dataColumn].ToString().Trim());
                                        if (conCatID == "0")
                                        {
                                            #region add Conference Category
                                            conCatID = string.Empty;
                                            DataTable dtCC = new DataTable();
                                            dtCC.Columns.Add("cc_ID");
                                            dtCC.Columns.Add("cc_categoryname");
                                            dtCC.Columns.Add("cc_seq");
                                            dtCC.Columns.Add("lang");
                                            dtCC.Columns.Add("deleteFlag");
                                            dtCC.Columns.Add("UpdatedDate");
                                            DataRow drCC = dtCC.NewRow();
                                            drCC["cc_ID"] = "";
                                            drCC["cc_categoryname"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drCC["cc_seq"] = "99";
                                            drCC["lang"] = "1";
                                            drCC["deleteFlag"] = false;
                                            drCC["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveConferenceCategoryGetID(drCC, ref conCatID);
                                            #endregion
                                        }
                                        dr[dataColumn] = conCatID == "0" ? "" : conCatID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conspkSpeakerCategory)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string spkCatID = daldata.getSpeakerCategoryIDbyName(dr[dataColumn].ToString().Trim());
                                        if (spkCatID == "0")
                                        {
                                            #region add Speaker Category
                                            spkCatID = string.Empty;
                                            DataTable dtSC = new DataTable();
                                            dtSC.Columns.Add("speakerCat_ID");
                                            dtSC.Columns.Add("speakerCat_name");
                                            dtSC.Columns.Add("speakerCat_seq");
                                            dtSC.Columns.Add("lang");
                                            dtSC.Columns.Add("deleteFlag");
                                            dtSC.Columns.Add("UpdatedDate");
                                            DataRow drCC = dtSC.NewRow();
                                            drCC["speakerCat_ID"] = "";
                                            drCC["speakerCat_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drCC["speakerCat_seq"] = "99";
                                            drCC["lang"] = "1";
                                            drCC["deleteFlag"] = false;
                                            drCC["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveSpeakerCategoryGetID(drCC, ref spkCatID);
                                            #endregion
                                        }
                                        dr[dataColumn] = spkCatID == "0" ? "" : spkCatID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                            }
                            #endregion

                            checkvalue = dr["cspeaker_category"].ToString().Trim();
                            checkkey = "cspeaker_category";

                            string isExistConID = daldata.GetConferenceSpeakerID(dr[_conspkConferenceID].ToString().Trim(), dr[_conspkSpeakerID].ToString().Trim(), checkvalue, dr[_conspkConferenceCategory].ToString().Trim());
                            if (!string.IsNullOrEmpty(isExistConID) && isExistConID != "0")
                            {
                                dr["cspeaker_ID"] = isExistConID;// daldata.getSpeakerIDByName(checkvalue);
                                //Update
                                result += daldata.UpdateConferenceSpeaker(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveConferenceSpeaker(dr);
                            }
                        }
                        #endregion

                        #region tblConferenceSponsors
                        if (tablename == "tblConferenceSponsors")
                        {
                            #region checkData
                            foreach (DataColumn dataColumn in dt.Columns)
                            {
                                if (dataColumn.ColumnName == _conspnConferenceID)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                        if (conID == "0")
                                        {
                                            #region add Conference
                                            DataTable dtC = new DataTable();
                                            dtC.Columns.Add("c_ID");
                                            dtC.Columns.Add("c_cc");
                                            dtC.Columns.Add("c_date");
                                            dtC.Columns.Add("c_stime");
                                            dtC.Columns.Add("c_etime");
                                            dtC.Columns.Add("c_title");
                                            dtC.Columns.Add("c_venue");
                                            dtC.Columns.Add("c_brief");
                                            dtC.Columns.Add("c_liveQA");
                                            dtC.Columns.Add("c_parentConfID");
                                            dtC.Columns.Add("lang");
                                            dtC.Columns.Add("deleteFlag");
                                            dtC.Columns.Add("UpdatedDate");
                                            DataRow drC = dtC.NewRow();
                                            drC["c_ID"] = "";
                                            drC["c_cc"] = "";
                                            drC["c_date"] = "";
                                            drC["c_stime"] = "";
                                            drC["c_etime"] = "";
                                            drC["c_title"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drC["c_venue"] = "";
                                            drC["c_brief"] = "";
                                            drC["c_liveQA"] = "0";
                                            drC["c_parentConfID"] = "";
                                            drC["lang"] = "1";
                                            drC["deleteFlag"] = false;
                                            drC["UpdatedDate"] = DateTime.Now;
                                            string confID = "";
                                            int res = daldata.SaveConference(drC, ref confID);
                                            conID = daldata.getConferenceIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = conID == "0" ? "" : conID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conspnSponsorID)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string spnID = daldata.getSponsorIDByName(dr[dataColumn].ToString().Trim());
                                        if (spnID == "0")
                                        {
                                            #region add Sponsor
                                            DataTable dtSPN = new DataTable();
                                            dtSPN.Columns.Add("s_ID");
                                            dtSPN.Columns.Add("s_exhID");
                                            dtSPN.Columns.Add("menuID");
                                            dtSPN.Columns.Add("s_name");
                                            dtSPN.Columns.Add("s_content");
                                            dtSPN.Columns.Add("s_logo");
                                            dtSPN.Columns.Add("s_category");
                                            dtSPN.Columns.Add("s_seq");
                                            dtSPN.Columns.Add("lang");
                                            dtSPN.Columns.Add("deleteFlag");
                                            dtSPN.Columns.Add("UpdatedDate");
                                            DataRow drSPN = dtSPN.NewRow();
                                            drSPN["s_ID"] = "";
                                            drSPN["s_exhID"] = "";
                                            drSPN["menuID"] = "";
                                            drSPN["s_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drSPN["s_content"] = "";
                                            drSPN["s_logo"] = "";
                                            drSPN["s_category"] = "";
                                            drSPN["s_seq"] = "99";
                                            drSPN["lang"] = "1";
                                            drSPN["deleteFlag"] = false;
                                            drSPN["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveSponsor(drSPN);
                                            spnID = daldata.getSponsorIDByName(dr[dataColumn].ToString().Trim());
                                            #endregion
                                        }
                                        dr[dataColumn] = spnID == "0" ? "" : spnID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                                else if (dataColumn.ColumnName == _conspnSponsorCategory)
                                {
                                    if (!string.IsNullOrEmpty(dr[dataColumn].ToString().Trim()))
                                    {
                                        string spnCatID = daldata.getsponsorcategoryIDByName(dr[dataColumn].ToString().Trim());
                                        if (spnCatID == "0")
                                        {
                                            #region add Conference Category
                                            spnCatID = string.Empty;
                                            DataTable dtSC = new DataTable();
                                            dtSC.Columns.Add("sponsorCat_ID");
                                            dtSC.Columns.Add("sponsorCat_name");
                                            dtSC.Columns.Add("sponsorCat_seq");
                                            dtSC.Columns.Add("lang");
                                            dtSC.Columns.Add("deleteFlag");
                                            dtSC.Columns.Add("UpdatedDate");
                                            DataRow drSC = dtSC.NewRow();
                                            drSC["sponsorCat_ID"] = "";
                                            drSC["sponsorCat_name"] = daldata.replaceForSQL(dr[dataColumn].ToString().Trim());
                                            drSC["sponsorCat_seq"] = "99";
                                            drSC["lang"] = "1";
                                            drSC["deleteFlag"] = false;
                                            drSC["UpdatedDate"] = DateTime.Now;
                                            int res = daldata.SaveSponsorCategoryGetID(drSC, ref spnCatID);
                                            #endregion
                                        }
                                        dr[dataColumn] = spnCatID == "0" ? "" : spnCatID;
                                    }
                                    else
                                    {
                                        dr[dataColumn] = "";
                                    }
                                }
                            }
                            #endregion

                            checkvalue = dr["csponsor_conferenceID"].ToString().Trim();
                            checkkey = "csponsor_conferenceID";

                            string isExistConID = daldata.GetConferenceSponsorID(dr[_conspnConferenceID].ToString().Trim(), dr[_conspnSponsorID].ToString().Trim(), dr[_conspnSponsorCategory].ToString().Trim());
                            if (!string.IsNullOrEmpty(isExistConID) && isExistConID != "0")
                            {
                                dr["csponsor_ID"] = isExistConID;
                                //Update
                                result += daldata.UpdateConferenceSponsor(dr);
                            }
                            else
                            {
                                //Insert
                                result += daldata.SaveConferenceSponsor(dr);
                            }
                        }
                        #endregion
                    }
                }
                grdForm.Visible = false;
                btnSave.Visible = false;
                //Response.Redirect("UploadFrontEndData.aspx?rtn=suc");
                if (result > 0)
                {
                    msg.Text = "Uploaded count : " + result;
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Import Data
    protected void btnImportData_Click(object sender, EventArgs e)
    {
        try
        {
            if (fupForm.HasFile == false)
            {
                msg.Text = "No Records for Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                return;
            }
            else
            {
                noti.Visible = false;
            }

            string IsXls = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
            if (IsXls != ".xls" && IsXls != ".xlsx")
            {
                msg.Text = "No Records for Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                return;
            }
            else
            {
                noti.Visible = false;
            }
                        
            string filename = System.IO.Path.GetFileNameWithoutExtension(fupForm.FileName) + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + IsXls;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\upfiles").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\upfiles\\") + filename);
            fupForm.SaveAs(savePath);

            DataTable dt = getDataTableFromExcel(savePath, filename);
            int rowsnum = dt.Rows.Count;
            if (rowsnum == 0)
            {
                msg.Text = "Invalid Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                btnSave.Visible = false;
                grdForm.DataSource = null;
                grdForm.DataBind();
            }
            else
            {
                Session["DT"] = dt;

                grdForm.DataSource = dt;
                grdForm.DataBind();
                btnSave.Visible = true;
                noti.Visible = false;
                grdForm.Visible = true;
                btnImport.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Response.Write("<script language='javascript'>alert('" + ex.Message.ToString() + "');window.location.href='UploadFrontEndData.aspx';</script>");
            Response.End();
            throw;
        }
    }
    #endregion

    #region Download Template
    protected void btnDownloadTemplate_Click(object sender, EventArgs e)
    {
        if (ddlparent.SelectedValue != "0")
        {
            DataTable dyndt = GenerateDynamicExcelTemplate();
            string filename = ddlparent.SelectedItem.ToString().Replace(" ","") + "Template";
            dalf.DataTableToExcel(dyndt, filename);
        }
    }
    #endregion

    #region Dynamic Excel Template 
    private DataTable GenerateDynamicExcelTemplate()
    {
        StringBuilder sb = new StringBuilder();
        DataTable dyndt = new DataTable();
        if (ddlparent.SelectedValue != "0")
        {
            string tablename = ddlparent.SelectedValue.ToString();
            
            DataRow dr1 = dyndt.NewRow();
            DataTable columnsdt = daldata.GetColumnNamesByTable(tablename);
           
            if (columnsdt.Rows.Count >0)
            {
                dyndt.Columns.Add("SrNo");//*
                foreach (DataRow dr in columnsdt.Rows)
                {
                    if (!dyndt.Columns.Contains(dr["COLUMN_NAME"].ToString()))
                    {
                        dyndt.Columns.Add(dr["COLUMN_NAME"].ToString());
                    }
                }
                if(tablename == "tblConference")
                {
                    dyndt.Columns.Add("ec_hall");
                }
                if(tablename == "tblExhibitingCompany")
                {
                    dyndt.Columns.Add(_DentalSector);
                    dyndt.Columns.Add(_DentalPractice);
                    dyndt.Columns.Add(_DentalLab);
                    dyndt.Columns.Add(_SubProductList_InfectionCntrl);
                    dyndt.Columns.Add(_SrvsInfoCommAndOrg);
                    dyndt.Columns.Add(_Distributors);
                    dyndt.Columns.Add(_ProductDescription);
                }
            }
            for (int a = 0; a < 15; a++)
            {
                DataRow dra = dyndt.NewRow();
                for (int c = 0; c < dyndt.Columns.Count; c++)
                {
                    dra[dyndt.Columns[c]] = "";
                }
                dyndt.Rows.Add(dra);
            }
            
            dyndt.AcceptChanges();
        }
        return dyndt;
    }
    #endregion

    #region getDataTableFromExcel
    private System.Data.DataTable getDataTableFromExcel(string filePath, string filename)
    {
        DataTable dt = new DataTable();
        Microsoft.Office.Interop.Excel.Application appExl;
        Microsoft.Office.Interop.Excel.Workbook workbook;
        Microsoft.Office.Interop.Excel.Worksheet NwSheet;
        Microsoft.Office.Interop.Excel.Range ShtRange;
        appExl = new Microsoft.Office.Interop.Excel.ApplicationClass();
        try
        {

            workbook = appExl.Workbooks.Open(filePath);

            NwSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets.get_Item(1);
            int Cnum = 0;
            int Rnum = 0;

            ShtRange = NwSheet.UsedRange;            
            DataTable dyndt = GenerateDynamicExcelTemplate();
            if (dyndt.Columns.Count > 0)
            {
                foreach (DataColumn dc in dyndt.Columns)
                {
                    dt.Columns.Add(dc.ColumnName);
                }
            }

            int i = 1;
            for (Rnum = 2; Rnum <= ShtRange.Rows.Count; Rnum++)
            {
                i = 1;
                DataRow dr = dt.NewRow();
                for (Cnum = 1; Cnum <= ShtRange.Columns.Count; Cnum++)
                {
                    try
                    {
                        if ((ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value != null)
                        {
                            if ((ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString() != "")
                            {
                                dr[Cnum - i] = (ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString();
                            }
                            else
                            {
                                i = 2;
                            }
                        }
                        else
                        {
                            dr[Cnum - i] = "";
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();
               
            }

            workbook.Close();
            appExl.Quit();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

        RemoveNullColumnFromDataTable(dt);

        return dt;
    }
    public static void RemoveNullColumnFromDataTable(DataTable dt)
    {
        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            if (dt.Rows[i][1] == DBNull.Value)
                dt.Rows[i].Delete();
        }
        dt.AcceptChanges();
    }

    #endregion

    #region GridView Events
    protected void grdForm_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdForm.CurrentPageIndex + 1;
    }
    protected void grdForm_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();//dal.getAllDoctors();
            grdForm.DataSource = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdForm_NeedDSource()
    {
        try
        {
            DataTable dt = new DataTable();// dal.getAllDoctors();
            dt = Session["DT"] as DataTable;
            grdForm.DataSource = dt;
            grdForm.DataBind();
        }
        catch (Exception ex) { }
    }
    #endregion

    #region Obselete
    //     if (dt.Columns.Contains("clientID"))
    //                        {
    //                            #region Has Client ID
    //                            string fcrowid = string.Empty;
    //    string clientID = dr["clientID"].ToString();
    //    int checkExist = dal.CheckRecordExist(pid, clientID, ref fcrowid);
    //                            if (checkExist == 0)
    //                            {
    //                                string rkey = string.Empty; rowid = string.Empty;
    //                                rkey = gk.getKey("RID");
    //                                rowid = "RID" + rkey;
    //                                #region Save
    //                                if (dt.Columns.Count > 0)
    //                                {
    //                                    foreach (DataColumn dc in dt.Columns)
    //                                    {
    //                                        string colname = dc.ColumnName;
    //                                        if (colname != "SrNo" && colname != "Language")
    //                                        {
    //                                            Fobj = new FrontEnd();
    //    string fkey = string.Empty; string fid = string.Empty;
    //    fkey = gk.getKey("FE");
    //                                            fid = "FE" + fkey;

    //                                            content = dr[colname].ToString();
    //    cid = dalc.SelectByName(colname, pid).c_id;
    //                                            inputtype = dalc.SelectByName(colname, pid).c_inputtype.Value;
    //                                            sortorder = dalc.SelectByName(colname, pid).c_sortorder.Value;

    //                                            Fobj.f_id = fid;
    //                                            Fobj.f_lang = int.Parse(dr["Language"].ToString());
    //                                            Fobj.f_pid = pid;
    //                                            Fobj.f_c_id = cid;
    //                                            Fobj.f_inputtype = inputtype;
    //                                            Fobj.f_label = colname;
    //                                            Fobj.f_sortorder = sortorder;
    //                                            if (inputtype == 7 || inputtype == 9 || inputtype == 11)
    //                                            {
    //                                                contentID = dalf.selectIDbyContent(content);
    //                                                content = contentID.ToString();
    //                                            }
    //Fobj.f_content = content;
    //                                            Fobj.f_entrycount = 1;
    //                                            Fobj.f_c_rowid = rowid;
    //                                            Fobj.f_c_isFinish = true;
    //                                            Fobj.f_createddate = timestamp;
    //                                            Fobj.f_deleteflag = false;
    //                                            int res = dalf.Save(Fobj);
    //                                            if (res == 1)
    //                                            {
    //                                                gk.SaveKey("FE", int.Parse(fkey));
    //                                            }


    //                                        }
    //                                    }
    //                                }
    //                                gk.SaveKey("RID", int.Parse(rkey));
    //                                #endregion
    //                            }
    //                            else
    //                            {
    //                                #region Update
    //                                rowid = fcrowid;
    //                                if (dt.Columns.Count > 0)
    //                                {
    //                                    foreach (DataColumn dc in dt.Columns)
    //                                    {
    //                                        string colname = dc.ColumnName;
    //                                        if (colname != "SrNo" && colname != "Language")
    //                                        {
    //                                            content = dr[colname].ToString();
    //cid = dalc.SelectByName(colname, pid).c_id;
    //                                            inputtype = dalc.SelectByName(colname, pid).c_inputtype.Value;
    //                                            sortorder = dalc.SelectByName(colname, pid).c_sortorder.Value;
    //                                            string fid = dal.CheckRecordExistByLine(pid, cid, rowid, dr["Language"].ToString());
    //                                            if (!string.IsNullOrEmpty(fid))
    //                                            {
    //                                                Fobj = new FrontEnd();
    //Fobj.f_id = fid;
    //                                                Fobj.f_lang = int.Parse(dr["Language"].ToString());
    //                                                Fobj.f_pid = pid;
    //                                                Fobj.f_c_id = cid;
    //                                                Fobj.f_inputtype = inputtype;
    //                                                Fobj.f_label = colname;
    //                                                Fobj.f_sortorder = sortorder;
    //                                                if (inputtype == 7 || inputtype == 9 || inputtype == 11)
    //                                                {
    //                                                    contentID = dalf.selectIDbyContent(content);
    //                                                    content = contentID.ToString();
    //                                                }
    //                                                Fobj.f_content = content;
    //                                                Fobj.f_entrycount = 1;
    //                                                Fobj.f_c_rowid = rowid;
    //                                                Fobj.f_c_isFinish = true;
    //                                                Fobj.f_createddate = timestamp;
    //                                                Fobj.f_deleteflag = false;
    //                                                int res = dalf.update(Fobj);
    //                                            }

    //                                        }
    //                                    }
    //                                }
    //                                #endregion
    //                            }
    //                            #endregion
    //                        }
    //                        else
    //                        {
    //                            string rkey = string.Empty; rowid = string.Empty;
    //                            rkey = gk.getKey("RID");
    //                            rowid = "RID" + rkey;
    //                            #region Save
    //                            if (dt.Columns.Count > 0)
    //                            {
    //                                foreach (DataColumn dc in dt.Columns)
    //                                {
    //                                    string colname = dc.ColumnName;
    //                                    if (colname != "SrNo" && colname != "Language")
    //                                    {
    //                                        Fobj = new FrontEnd();
    //string fkey = string.Empty; string fid = string.Empty;
    //fkey = gk.getKey("FE");
    //                                        fid = "FE" + fkey;

    //                                        content = dr[colname].ToString();
    //cid = dalc.SelectByName(colname, pid).c_id;
    //                                        inputtype = dalc.SelectByName(colname, pid).c_inputtype.Value;
    //                                        sortorder = dalc.SelectByName(colname, pid).c_sortorder.Value;

    //                                        Fobj.f_id = fid;
    //                                        Fobj.f_lang = int.Parse(dr["Language"].ToString());
    //                                        Fobj.f_pid = pid;
    //                                        Fobj.f_c_id = cid;
    //                                        Fobj.f_inputtype = inputtype;
    //                                        Fobj.f_label = colname;
    //                                        Fobj.f_sortorder = sortorder;
    //                                        if (inputtype == 7 || inputtype == 9 || inputtype == 11)
    //                                        {
    //                                            if(!string.IsNullOrEmpty(content))
    //                                            {
    //                                                contentID = dalf.selectIDbyContent(content);
    //                                                content = contentID.ToString();
    //                                            }
    //                                            else
    //                                            {
    //                                                content = "";
    //                                            }

    //                                        }
    //                                        Fobj.f_content = content;
    //                                        Fobj.f_entrycount = 1;
    //                                        Fobj.f_c_rowid = rowid;
    //                                        Fobj.f_c_isFinish = true;
    //                                        Fobj.f_createddate = timestamp;
    //                                        Fobj.f_deleteflag = false;
    //                                        int res = dalf.Save(Fobj);
    //                                        if (res == 1)
    //                                        {
    //                                            gk.SaveKey("FE", int.Parse(fkey));
    //                                        }


    //                                    }
    //                                }
    //                            }
    //                            gk.SaveKey("RID", int.Parse(rkey));
    //                            #endregion
    //                        }
    #endregion

    #region getLocationHallName
    private string getLocationHallName(string locationName)
    {
        string result = string.Empty;
        try
        {
            if(_strhall1.Contains(locationName))
            {
                result = _hall1;
            }
            else if (_strhall2.Contains(locationName))
            {
                result = _hall2;
            }
            else if (_strhall3.Contains(locationName))
            {
                result = _hall3;
            }
            else if (_strhall4.Contains(locationName))
            {
                result = _hall4;
            }
            if (_strhall5.Contains(locationName))
            {
                result = _hall5;
            }
            else if (_strhall6.Contains(locationName))
            {
                result = _hall6;
            }
            else if (_strhall7.Contains(locationName))
            {
                result = _hall7;
            }
            else if (_strhall8.Contains(locationName))
            {
                result = _hall8;
            }
            else if(locationName == _strhallC1)
            {
                result = _hallC1;
            }
            else if (locationName == _strhallC2)
            {
                result = _hallC2;
            }
            else if (locationName == _strhallC3)
            {
                result = _hallC3;
            }
        }
        catch(Exception ex)
        { }
        return result;
    }
    #endregion

    #region Convert Product List to Html list
    private void ConvertProductListToHtmlList(string productlist, string productPara, ref string productListHTML)
    {
        try
        {
            string result = "";
            if (!string.IsNullOrEmpty(productlist))
            {
                string[] strProductList = productlist.Split(';');
                if (strProductList.Length > 0)
                {
                    foreach(var str in strProductList)
                    {
                        result += "<li>" + str + "</li>";
                    }
                }

                if (string.IsNullOrEmpty(result))
                {
                    result = "<li></li>";
                }
            }

            productListHTML = productListHTML.Replace("%" + productPara.Trim() + "%", result.Trim());
        }
        catch(Exception ex)
        { }
    }
    private void updateProductHTML(string exhClientID, string productListHTML)
    {
        try
        {
            Functionality fn = new Functionality();
            string sqlUpdate = "Update tblExhibitingCompany Set ec_productHTML='" + fn.solveSQL(productListHTML) + "' Where clientID='" + exhClientID + "'";
            fn.ExecuteSQL(sqlUpdate);
        }
        catch (Exception ex)
        { }
    }
    #endregion
}

