﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_eventschedule : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblEvent";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdEvent.ExportSettings.IgnorePaging = true;
                grdEvent.ExportSettings.ExportOnlyData = true;
                grdEvent.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdEvent.ExportSettings.FileName = "Event";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtvenue = daldata.getBooth();

            ddlvenue.DataSource = dtvenue;
            ddlvenue.DataTextField = "vb_name";
            ddlvenue.DataValueField = "vb_ID";
            ddlvenue.DataBind();
            ListItem lst1 = new ListItem("Select Venue (Booth)", "");
            ddlvenue.Items.Insert(ddlvenue.Items.Count - ddlvenue.Items.Count, lst1);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("e_ID");
                dt.Columns.Add("e_image");
                dt.Columns.Add("e_date");
                dt.Columns.Add("e_time");
                dt.Columns.Add("e_title");
                dt.Columns.Add("e_venue");
                dt.Columns.Add("e_brief");
                dt.Columns.Add("e_eventcategory");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");


                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();

                    dr["e_ID"] = hfid.Value.ToString();
                    dr["e_image"] = imagename == "" ? lblicon.Text : imagename;
                    dr["e_date"] = daldata.replaceForSQL(txtdate.Text.Trim());
                    dr["e_time"] = daldata.replaceForSQL(txttime.Text.Trim());
                    dr["e_title"] = daldata.replaceForSQL(txtEvent.Text.Trim());
                    dr["e_venue"] = ddlvenue.SelectedValue;
                    dr["e_brief"] = daldata.replaceForSQL(txtbrief.Content.Trim());
                    dr["e_eventcategory"] = daldata.replaceForSQL(txtEventCategory.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;


                    int res = daldata.UpdateEvent(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdEvent_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["e_ID"] = "";
                    dr["e_image"] = imagename == "" ? lblicon.Text : imagename;
                    dr["e_date"] = daldata.replaceForSQL(txtdate.Text.Trim());
                    dr["e_time"] = daldata.replaceForSQL(txttime.Text.Trim());
                    dr["e_title"] = daldata.replaceForSQL(txtEvent.Text.Trim());
                    dr["e_venue"] = ddlvenue.SelectedValue;
                    dr["e_brief"] = daldata.replaceForSQL(txtbrief.Content.Trim());
                    dr["e_eventcategory"] = daldata.replaceForSQL(txtEventCategory.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.SaveEvent(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdEvent_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtEvent.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdEvent.ExportSettings.ExportOnlyData = true;
        this.grdEvent.ExportSettings.IgnorePaging = true;
        this.grdEvent.ExportSettings.OpenInNewWindow = true;
        this.grdEvent.MasterTableView.ExportToExcel();

        grdEvent.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdEvent.ExportSettings.FileName = "Event";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdEvent_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getEvent();
            grdEvent.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdEvent_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdEvent.CurrentPageIndex + 1;
    }
    protected void grdEvent_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getEventbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getEvent();
            }


            grdEvent.DataSource = dt;
            grdEvent.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdEvent_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdEvent.ExportSettings.ExportOnlyData = true;
            this.grdEvent.ExportSettings.IgnorePaging = true;
            this.grdEvent.ExportSettings.OpenInNewWindow = true;
            this.grdEvent.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getEventByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["e_ID"].ToString();
                    txtdate.Text = dt.Rows[0]["e_date"].ToString();
                    txttime.Text = dt.Rows[0]["e_time"].ToString();
                    txtEvent.Text = dt.Rows[0]["e_title"].ToString();
                    lblicon.Text = dt.Rows[0]["e_image"].ToString();
                    txtbrief.Content = dt.Rows[0]["e_brief"].ToString();
                    txtEventCategory.Text = dt.Rows[0]["e_eventcategory"].ToString();
                    ddlvenue.SelectedValue = dt.Rows[0]["e_venue"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteEventByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdEvent_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdEvent_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtdate.Text = "";
        txttime.Text = "";
        ddlvenue.SelectedIndex = 0;
        txtEvent.Text = "";
        txtEventCategory.Text = "";
        txtbrief.Content = "";
        lblicon.Text = "";
    }
}