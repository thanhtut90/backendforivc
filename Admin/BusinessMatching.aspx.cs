﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_BusinessMatching : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblBusinessMatching";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdBM.ExportSettings.IgnorePaging = true;
                grdBM.ExportSettings.ExportOnlyData = true;
                grdBM.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdBM.ExportSettings.FileName = "BusinessMatching";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {


            //DataTable dtName = new DataTable();

            ////Add Columns to Table
            //dtName.Columns.Add(new DataColumn("BusinessMatchingType"));
            //dtName.Columns.Add(new DataColumn("BusinessMatchingID"));

            ////Now Add Values
            //dtName.Rows.Add("Select BusinessMatching Type", "0");
            //dtName.Rows.Add("Exhibitor", "Exhibitor");
            //dtName.Rows.Add("Degelate", "Degelate");
            //dtName.Rows.Add("Visitor", "Visitor");
            //dtName.Rows.Add("VIP", "VIP");
            //dtName.Rows.Add("Organizer", "Organizer");

            ////At Last Bind datatable to dropdown.
            //ddlAcc.DataSource = dtName;
            //ddlAcc.DataTextField = dtName.Columns["BusinessMatchingType"].ToString();
            //ddlAcc.DataValueField = dtName.Columns["BusinessMatchingID"].ToString();
            //ddlAcc.DataBind();


            //DataTable dtSName = new DataTable();

            ////Add Columns to Table
            //dtSName.Columns.Add(new DataColumn("SalType"));
            //dtSName.Columns.Add(new DataColumn("SalID"));

            ////Now Add Values
            //dtSName.Rows.Add("Select Salutation", "0");
            //dtSName.Rows.Add("Sir", "Sir");
            //dtSName.Rows.Add("Mr", "Mr");
            //dtSName.Rows.Add("Ms", "Ms");
            //dtSName.Rows.Add("Mrs", "Mrs");

            ////At Last Bind datatable to dropdown.
            //ddlSal.DataSource = dtSName;
            //ddlSal.DataTextField = dtSName.Columns["SalType"].ToString();
            //ddlSal.DataValueField = dtSName.Columns["SalID"].ToString();
            //ddlSal.DataBind();

        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("bm_ID");
                dt.Columns.Add("bm_vid");
                dt.Columns.Add("bm_eid");
                dt.Columns.Add("bm_date");
                dt.Columns.Add("bm_time");
                dt.Columns.Add("bm_status");
                dt.Columns.Add("bm_ecomment");
                dt.Columns.Add("bm_reqtime");
                dt.Columns.Add("bm_vcomment");
                dt.Columns.Add("bm_interestedProduct");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["bm_ID"] = hfid.Value.ToString();
                    dr["bm_vid"] = daldata.replaceForSQL(txtVisitorID.Text.Trim());
                    dr["bm_eid"] = daldata.replaceForSQL(txtExhID.Text.Trim());
                    dr["bm_date"] = daldata.replaceForSQL(txtDate.Text.Trim());
                    dr["bm_time"] = daldata.replaceForSQL(txtTimeslot.Text.Trim());
                    dr["bm_status"] = daldata.replaceForSQL(txtStatus.Text.Trim());
                    dr["bm_ecomment"] = daldata.replaceForSQL(txtEComment.Text.Trim());
                    dr["bm_reqtime"] = daldata.replaceForSQL(txtReqTime.Text.Trim());
                    dr["bm_vcomment"] = daldata.replaceForSQL(txtVComment.Text.Trim());
                    dr["bm_interestedProduct"] = daldata.replaceForSQL(txtVEProduct.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateBusinessMatching(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdBM_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["bm_ID"] = "";
                    dr["bm_vid"] = daldata.replaceForSQL(txtVisitorID.Text.Trim());
                    dr["bm_eid"] = daldata.replaceForSQL(txtExhID.Text.Trim());
                    dr["bm_date"] = daldata.replaceForSQL(txtDate.Text.Trim());
                    dr["bm_time"] = daldata.replaceForSQL(txtTimeslot.Text.Trim());
                    dr["bm_status"] = daldata.replaceForSQL(txtStatus.Text.Trim());
                    dr["bm_ecomment"] = daldata.replaceForSQL(txtEComment.Text.Trim());
                    dr["bm_reqtime"] = daldata.replaceForSQL(txtReqTime.Text.Trim());
                    dr["bm_vcomment"] = daldata.replaceForSQL(txtVComment.Text.Trim());
                    dr["bm_interestedProduct"] = daldata.replaceForSQL(txtVEProduct.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveBusinessMatching(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdBM_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtVisitorID.Text.Trim()) || string.IsNullOrWhiteSpace(txtVisitorID.Text.Trim()))
            {
                lblerrVisitorID.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrVisitorID.Visible = false;
            }
            if (string.IsNullOrEmpty(txtExhID.Text.Trim()) || string.IsNullOrWhiteSpace(txtExhID.Text.Trim()))
            {
                lblerrExhID.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrExhID.Visible = false;
            }
            if (string.IsNullOrEmpty(txtDate.Text.Trim()) || string.IsNullOrWhiteSpace(txtDate.Text.Trim()))
            {
                lblerrDate.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrDate.Visible = false;
            }
            if (string.IsNullOrEmpty(txtTimeslot.Text.Trim()) || string.IsNullOrWhiteSpace(txtTimeslot.Text.Trim()))
            {
                lblerrTimeslot.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrTimeslot.Visible = false;
            }
            if (string.IsNullOrEmpty(txtStatus.Text.Trim()) || string.IsNullOrWhiteSpace(txtStatus.Text.Trim()))
            {
                lblerrStatus.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrStatus.Visible = false;
            }
            if (string.IsNullOrEmpty(txtReqTime.Text.Trim()) || string.IsNullOrWhiteSpace(txtReqTime.Text.Trim()))
            {
                lblerrReqTime.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrReqTime.Visible = false;
            }
            if (string.IsNullOrEmpty(txtEComment.Text.Trim()) || string.IsNullOrWhiteSpace(txtEComment.Text.Trim()))
            {
                lblerrEComment.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrEComment.Visible = false;
            }
            if (string.IsNullOrEmpty(txtVEProduct.Text.Trim()) || string.IsNullOrWhiteSpace(txtVEProduct.Text.Trim()))
            {
                lblerrVEProduct.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrVEProduct.Visible = false;
            }
          

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdBM.ExportSettings.ExportOnlyData = true;
        this.grdBM.ExportSettings.IgnorePaging = true;
        this.grdBM.ExportSettings.OpenInNewWindow = true;
        this.grdBM.MasterTableView.ExportToExcel();

        grdBM.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdBM.ExportSettings.FileName = "BusinessMatching";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdBM_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getBusinessMatching();
            grdBM.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdBM_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdBM.CurrentPageIndex + 1;
    }
    protected void grdBM_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getBusinessMatchingbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getBusinessMatching();
            }


            grdBM.DataSource = dt;
            grdBM.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdBM_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdBM.ExportSettings.ExportOnlyData = true;
            this.grdBM.ExportSettings.IgnorePaging = true;
            this.grdBM.ExportSettings.OpenInNewWindow = true;
            this.grdBM.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getBusinessMatchinglstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["bm_ID"].ToString();
                    txtVisitorID.Text = dt.Rows[0]["bm_vid"].ToString();
                    txtExhID.Text = dt.Rows[0]["bm_eid"].ToString();
                    txtDate.Text = dt.Rows[0]["bm_date"].ToString();
                    txtStatus.Text = dt.Rows[0]["bm_status"].ToString();
                    txtTimeslot.Text = dt.Rows[0]["bm_time"].ToString();
                    txtEComment.Text = dt.Rows[0]["bm_ecomment"].ToString();
                    txtReqTime.Text = dt.Rows[0]["bm_reqtime"].ToString();
                    txtVComment.Text = dt.Rows[0]["bm_vcomment"].ToString();
                    txtVEProduct.Text = dt.Rows[0]["bm_interestedProduct"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteBusinessMatchingByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdBM_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdBM_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }

    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtVisitorID.Text = "";
        txtEComment.Text = "";
        txtExhID.Text = "";
        txtDate.Text = "";
        txtStatus.Text = "";
        txtTimeslot.Text = "";
        txtReqTime.Text = "";
        txtVComment.Text = "";
        txtVEProduct.Text = "";


    }
}