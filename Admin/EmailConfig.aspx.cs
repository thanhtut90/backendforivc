﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;

public partial class Admin_EmailConfig : System.Web.UI.Page
{
    DAL_Email dal = new DAL_Email();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            BindDDL();
        }
    }

    private void BindDDL()
    {
        List<tb_ConfigEmail> lst = dal.SelectAllEmailTemplate();
        ddletemplates.DataSource = lst;
        ddletemplates.DataValueField = "em_id";
        ddletemplates.DataTextField = "em_Title";
        ddletemplates.DataBind();
        ListItem a = new ListItem("Select Template to View / Update", "");
        ddletemplates.Items.Insert(ddletemplates.Items.Count - ddletemplates.Items.Count, a);

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        int isSuc = 0;
        if (!string.IsNullOrEmpty(ddletemplates.SelectedValue))
        {
            tb_ConfigEmail obj = new tb_ConfigEmail()
            {
                em_Title = txtemailname.Text.Trim(),
                em_Subject = txtemailsubject.Text.Trim(),
                em_FromEmail = txtfromemail.Text.Trim(),
                em_FromName = txtfromname.Text.Trim(),
                em_Content = txtemailcontent.Content.ToString(),
                em_id=int.Parse(ddletemplates.SelectedValue.ToString()),
                em_bcc = txtbcc.Text.Trim(),
                em_cc = txtcc.Text.Trim(),
                em_parameters = txtparameter.Text.Trim()
            };
            isSuc = dal.UpdateConfigEmail(obj);
        }
        else
        {
            tb_ConfigEmail obj = new tb_ConfigEmail()
            {
                em_Title = txtemailname.Text.Trim(),
                em_Subject = txtemailsubject.Text.Trim(),
                em_FromEmail = txtfromemail.Text.Trim(),
                em_FromName = txtfromname.Text.Trim(),
                em_Content = txtemailcontent.Content.ToString(),
                em_bcc = txtbcc.Text.Trim(),
                em_cc=txtcc.Text.Trim(),
                em_parameters=txtparameter.Text.Trim()
            };
            isSuc = dal.SaveConfigEmail(obj);
        }
        
        if (isSuc == 1)
        {
            BindDDL();
            Reset();
            btnSubmit.Text = "Submit";
        }
    }

    private void Reset()
    {
        txtemailname.Text = string.Empty;
        txtemailsubject.Text = string.Empty;
        txtfromemail.Text = string.Empty;
        txtfromname.Text = string.Empty;
        txtcc.Text = string.Empty;
        txtbcc.Text = string.Empty;
        txtparameter.Text = string.Empty;
        txtemailcontent.Content = string.Empty;
    }

    protected void ddletemplates_selectedindexchanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(ddletemplates.SelectedValue))
        {
            int em_id = int.Parse(ddletemplates.SelectedValue.ToString());
            tb_ConfigEmail obj = dal.selectEmailTemplateByID(em_id);
            if(obj !=null)
            {
                txtemailname.Text = obj.em_Title;
                txtemailsubject.Text = obj.em_Subject;
                txtfromemail.Text = obj.em_FromEmail;
                txtfromname.Text = obj.em_FromName;
                txtemailcontent.Content = obj.em_Content;
                txtcc.Text = obj.em_cc;
                txtbcc.Text = obj.em_bcc;
                txtparameter.Text = obj.em_parameters;
                btnSubmit.Text = "Update";
            }
        }
    }
    protected void btncancel_Click(object sender,EventArgs e)
    {
        ddletemplates.SelectedValue = "";
        Reset();
    }
}
