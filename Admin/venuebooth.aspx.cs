﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_venuebooth : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblVenue_Booth";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdbooth.ExportSettings.IgnorePaging = true;
                grdbooth.ExportSettings.ExportOnlyData = true;
                grdbooth.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdbooth.ExportSettings.FileName = "VenueBooth";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }

    private void BindControlData()
    {
        try
        {
            DataTable dtfloorplan = daldata.getHall();

            ddlfloorplan.DataSource = dtfloorplan;
            ddlfloorplan.DataTextField = "l_name";
            ddlfloorplan.DataValueField = "l_ID";
            ddlfloorplan.DataBind();
            ListItem lst = new ListItem("Select Floor", "");
            ddlfloorplan.Items.Insert(ddlfloorplan.Items.Count - ddlfloorplan.Items.Count, lst);
        }
        catch (Exception ex) { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("vb_ID");
                dt.Columns.Add("vb_name");
                dt.Columns.Add("vb_location");
                dt.Columns.Add("vb_width");
                dt.Columns.Add("vb_height");
                dt.Columns.Add("vb_x");
                dt.Columns.Add("vb_y");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");



                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["vb_ID"] = hfid.Value.ToString();
                    dr["vb_name"] = txtboothname.Text;
                    dr["vb_location"] = ddlfloorplan.SelectedValue.ToString();
                    dr["vb_width"] = txtw.Text;
                    dr["vb_height"] = txth.Text;
                    dr["vb_x"] = txtx.Text;
                    dr["vb_y"] = txty.Text;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    bool isExist = daldata.checkupdateExistBooth(txtboothname.Text, ddlfloorplan.SelectedValue.ToString(), hfid.Value.ToString());
                    if (isExist)
                    {
                        Response.Write("<script>alert('Already exist.');</script>");
                        return;
                    }
                    else
                    {
                        int res = daldata.UpdateBooth(dr);
                        if (res == 1)
                        {
                            daldata.UpdateDataVersion(tablename);
                            grdbooth_NeedDSource("");
                            divadd.Visible = false;
                            divsearch.Visible = true;
                            divlist.Visible = true;
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Update Success !";

                            clearControls();
                        }
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["vb_ID"] = "";
                    dr["vb_name"] = txtboothname.Text;
                    dr["vb_location"] = ddlfloorplan.SelectedValue.ToString();
                    dr["vb_width"] = txtw.Text;
                    dr["vb_height"] = txth.Text;
                    dr["vb_x"] = txtx.Text;
                    dr["vb_y"] = txty.Text;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    bool isExist = daldata.checksaveExistBooth(txtboothname.Text, ddlfloorplan.SelectedValue.ToString());
                    if (isExist)
                    {
                        Response.Write("<script>alert('Already exist.');</script>");
                        return;
                    }
                    else
                    {
                        int res = daldata.SaveBooth(dr);
                        if (res == 1)
                        {
                            daldata.UpdateDataVersion(tablename);
                            grdbooth_NeedDSource("");
                            divadd.Visible = false;
                            divsearch.Visible = true;
                            divlist.Visible = true;
                            noti.Visible = true;
                            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                            msg.Text = "Save Success !";

                            clearControls();
                        }
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtboothname.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdbooth.ExportSettings.ExportOnlyData = true;
        this.grdbooth.ExportSettings.IgnorePaging = true;
        this.grdbooth.ExportSettings.OpenInNewWindow = true;
        this.grdbooth.MasterTableView.ExportToExcel();

        grdbooth.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdbooth.ExportSettings.FileName = "VenueBooth";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdbooth_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getBooth();
            grdbooth.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdbooth_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdbooth.CurrentPageIndex + 1;
    }
    protected void grdbooth_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getBoothbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getBooth();
            }


            grdbooth.DataSource = dt;
            grdbooth.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdbooth_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdbooth.ExportSettings.ExportOnlyData = true;
            this.grdbooth.ExportSettings.IgnorePaging = true;
            this.grdbooth.ExportSettings.OpenInNewWindow = true;
            this.grdbooth.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getBoothlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["vb_id"].ToString();
                    txtboothname.Text = dt.Rows[0]["vb_name"].ToString();
                    txtw.Text= dt.Rows[0]["vb_width"].ToString();
                    txth.Text = dt.Rows[0]["vb_height"].ToString();
                    txtx.Text = dt.Rows[0]["vb_x"].ToString();
                    txty.Text = dt.Rows[0]["vb_y"].ToString();
                    ddlfloorplan.SelectedValue= dt.Rows[0]["vb_location"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteBoothByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdbooth_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdbooth_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtboothname.Text = "";
        txtw.Text = "";
        txth.Text = "";
        txtx.Text = "";
        txty.Text = "";
        ddlfloorplan.SelectedIndex = 0;
    }

    protected void lnkAddCoor_Click(object sender, EventArgs e)
    {
        try
        {        
            string str = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(str))
            {
                string[] a = str.Split('/');
                string id = a[0];
                string img = a[1];
                Response.Redirect("AddCoordinator.aspx?id="+id+"&img="+img);
            }
        }
        catch (Exception ex) { }
    }
}