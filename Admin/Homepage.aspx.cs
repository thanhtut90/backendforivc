﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Homepage : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblHomePage";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdHomepage.ExportSettings.IgnorePaging = true;
                grdHomepage.ExportSettings.ExportOnlyData = true;
                grdHomepage.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdHomepage.ExportSettings.FileName = "Homepage";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("hp_ID");
                dt.Columns.Add("hp_mtitle");
                dt.Columns.Add("hp_stitle");
                dt.Columns.Add("hp_content");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["hp_ID"] = hfid.Value.ToString();
                    dr["hp_mtitle"] = daldata.replaceForSQL(txtHomepageTitle.Text.Trim());
                    dr["hp_stitle"] = daldata.replaceForSQL(txtstitle.Text.Trim());
                    dr["hp_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateHomepage(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdHomepage_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["hp_ID"] = "";
                    dr["hp_mtitle"] = daldata.replaceForSQL(txtHomepageTitle.Text.Trim());
                    dr["hp_stitle"] = daldata.replaceForSQL(txtstitle.Text.Trim());
                    dr["hp_content"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveHomepage(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdHomepage_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtHomepageTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtHomepageTitle.Text.Trim()))
            {
                lblerrHomepageTitle.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtstitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtstitle.Text.Trim()))
            {
                lblerrstitle.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdHomepage.ExportSettings.ExportOnlyData = true;
        this.grdHomepage.ExportSettings.IgnorePaging = true;
        this.grdHomepage.ExportSettings.OpenInNewWindow = true;
        this.grdHomepage.MasterTableView.ExportToExcel();

        grdHomepage.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdHomepage.ExportSettings.FileName = "Homepage";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdHomepage_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getHomepage();
            grdHomepage.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdHomepage_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdHomepage.CurrentPageIndex + 1;
    }
    protected void grdHomepage_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getHomepagebyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getHomepage();
            }

            grdHomepage.DataSource = dt;
            grdHomepage.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdHomepage_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdHomepage.ExportSettings.ExportOnlyData = true;
            this.grdHomepage.ExportSettings.IgnorePaging = true;
            this.grdHomepage.ExportSettings.OpenInNewWindow = true;
            this.grdHomepage.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getHomepagelstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["hp_ID"].ToString();
                    txtHomepageTitle.Text = dt.Rows[0]["hp_mtitle"].ToString();
                    txtContent.Content = dt.Rows[0]["hp_content"].ToString();
                    txtstitle.Text = dt.Rows[0]["hp_stitle"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteHomepageByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdHomepage_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdHomepage_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtHomepageTitle.Text = "";
        txtContent.Content = "";
        txtstitle.Text = "";
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrHomepageTitle.Visible = false;
        lblerrstitle.Visible = false;
    }
}