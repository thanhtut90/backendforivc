﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="conference.aspx.cs" Inherits="Admin_conference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script src="../Content/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#MainContent_txtstime").timepicker({
                showInputs: false, //Set showInputs to false
                showMeridian: false,
                minuteStep: 1
            });
        })
        //$(function () {
        //    $("#MainContent_txtdate").datepicker({
        //        defaultDate: moment(),
        //        format: 'dd/mm/yyyy'
        //    }).on('keypress', function (e) {
        //        e.preventDefault();
        //        return false;
        //    });
        //})
    </script>
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdconference" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">                      
                        <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>   
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Conference</h3>
                    <div style="float:right">                            
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>                        
                </div>                                       
                <div class="box-body"> 
                    <div id="divsearch" runat="server" class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                        <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Conference Name" runat="server"></asp:TextBox>                    
                        </div>
                        <div class="col-md-1">                        
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />   
                        </div>
                        <div class="col-md-6">
                        <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                        
                    </div>  
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdconference"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdconference_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdconference_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdconference_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="c_ID" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true"/>
                                        <Columns> 
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                               <%# Container.ItemIndex + 1  %>  
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn>     
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">                                    
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("c_ID") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>                                    
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("c_ID") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>                                                                                                           
                                                </ItemTemplate>                     
                                            </telerik:GridTemplateColumn>                              
                                            <telerik:GridBoundColumn  DataField="c_cc" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Conference Category" SortExpression="c_cc" UniqueName="c_cc" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                            <telerik:GridBoundColumn  DataField="c_date" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Date" SortExpression="c_date" UniqueName="c_date" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="c_stime" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Start Time" SortExpression="c_stime" UniqueName="c_stime" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="c_etime" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="End Time" SortExpression="c_etime" UniqueName="c_etime" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="c_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Conference Title" SortExpression="c_title" UniqueName="ec_contactno" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>                       
                                            <telerik:GridBoundColumn  DataField="c_venue" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Venue(Booth)" SortExpression="c_venue" UniqueName="c_venue" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                            <telerik:GridBoundColumn  DataField="c_brief" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Brief" SortExpression="c_brief" UniqueName="c_brief" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                            <telerik:GridBoundColumn  DataField="c_liveQA" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Use in Live QA" SortExpression="c_liveQA" UniqueName="c_liveQA" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                            <telerik:GridBoundColumn  DataField="c_parentConfID" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Main Conference" SortExpression="c_parentConfID" UniqueName="c_parentConfID" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                              
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">                            
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Conference Category</label>
                                <div class="col-sm-2">
                                <asp:DropDownList ID="ddlcategory" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
                                <div class="col-sm-2">
                                    <%--<asp:TextBox ID="txtdate" class="form-control"  placeholder="Enter Date" runat="server"></asp:TextBox>--%>
                                    <asp:DropDownList ID="txtdate" runat="server" CssClass="form-control" ></asp:DropDownList>
                                    <%--<asp:TextBox ID="txtdate" data-provide="date" class="form-control datepicker" placeholder="Choose Date"  runat="server"
                                        ></asp:TextBox> --%>
                                    <%--onclick="$(this).datepicker().datepicker('show')"--%>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Start Time</label>
                                <div class="col-sm-2 input-group bootstrap-timepicker timepicker" style="padding-left:15px;">
                                    <%--<asp:TextBox ID="txtstime" class="form-control"  placeholder="0000" runat="server"></asp:TextBox>--%>
                                       <asp:TextBox ID="txtstime" data-provide="timepicker" CssClass="form-control timepicker" runat="server"></asp:TextBox>
                                        <%--<span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>--%>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">Duration</label><%--End Time--%>
                                <div class="col-sm-2">
                                    <asp:TextBox ID="txtetime" class="form-control"  placeholder="00" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Time Zone</label>
                                <div class="col-sm-6">
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <%--<asp:TextBox ID="txtTimeZone" class="form-control"  placeholder="Enter Time Zone" runat="server"></asp:TextBox>--%>
                                    <asp:DropDownList ID="ddlTimeZone" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Configuration</label>
                                <div class="col-sm-2">
                                    <asp:DropDownList ID="ddlConfiguration" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-6"><asp:HiddenField ID="HiddenField2" runat="server" />
                                <asp:TextBox ID="txtPassword" class="form-control"  placeholder="Enter Password" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Conference</label>
                                <div class="col-sm-6"><asp:HiddenField ID="hfid" runat="server" /><asp:HiddenField ID="hfZoomID" runat="server" />
                                <asp:TextBox ID="txtconference" class="form-control"  placeholder="Enter Conference" runat="server"></asp:TextBox>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Venue</label>
                                <div class="col-sm-2">
                                <asp:DropDownList ID="ddlvenue" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">Use in Live QA</label>
                                <div class="col-sm-2">
                                <asp:RadioButtonList ID="rodliveQA" RepeatColumns="2" RepeatDirection="Horizontal" runat="server"><asp:ListItem Value="1">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem></asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Use in Polling</label>
                                <div class="col-sm-2">
                                <asp:RadioButtonList ID="rdopolling" RepeatColumns="2" RepeatDirection="Horizontal" runat="server"><asp:ListItem Value="1">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem></asp:RadioButtonList>
                                </div>
                                <label for="inputEmail3" class="col-sm-2 control-label">Use in Survey</label>
                                <div class="col-sm-2">
                                <asp:RadioButtonList ID="rdosurvey" RepeatColumns="2" RepeatDirection="Horizontal" runat="server"><asp:ListItem Value="1">Yes</asp:ListItem> <asp:ListItem Value="0">No</asp:ListItem></asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Main Conference</label>
                                <div class="col-sm-6">
                                <asp:DropDownList ID="ddlmainconf" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Brief</label>
                                <div class="col-sm-6">
                                <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtbrief" SkinID="DefaultSetOfTools" Width="90%">                                   
                                        <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            EnableAsyncUpload="true"></ImageManager>
                                        <Tools>
                                            <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="JustifyLeft" />
                                            <telerik:EditorTool Name="JustifyCenter" />
                                            <telerik:EditorTool Name="JustifyRight" />
                                            <telerik:EditorTool Name="JustifyFull" />
                                        </telerik:EditorToolGroup>
                                           
                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="Bold" />
                                            <telerik:EditorTool Name="Underline" />
                                            <telerik:EditorTool Name="Italic" />
                                        </telerik:EditorToolGroup>

                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="InsertTable"/>
                                        </telerik:EditorToolGroup>
                                        </Tools>
                                    </telerik:RadEditor>    
                                </div>
                            </div>
                        
                            </div>
                        </div>
                        <div class="box-footer">                                
                           <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
</asp:Content>

