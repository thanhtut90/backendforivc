﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_bottombanner : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblBottomBanner";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdBottomBanner.ExportSettings.IgnorePaging = true;
                grdBottomBanner.ExportSettings.ExportOnlyData = true;
                grdBottomBanner.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdBottomBanner.ExportSettings.FileName = "BottomBanner";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("bb_ID");
                dt.Columns.Add("bb_title");
                dt.Columns.Add("bb_image");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string message = string.Empty;

                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                if (fupImage.HasFile == true)
                {
                    filename = fupImage.FileName.Substring(0, fupImage.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupImage.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }

                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["bb_ID"] = hfid.Value.ToString();
                    dr["bb_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["bb_image"] = imagename == "" ? lblImage.Text : imagename;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateBottomBanner(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdBottomBanner_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["bb_ID"] = "";
                    dr["bb_title"] = daldata.replaceForSQL(txtTitle.Text.Trim());
                    dr["bb_image"] = imagename == "" ? lblImage.Text : imagename;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveBottomBanner(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdBottomBanner_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtTitle.Text.Trim()))
            {
                lblerrTitle.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdBottomBanner.ExportSettings.ExportOnlyData = true;
        this.grdBottomBanner.ExportSettings.IgnorePaging = true;
        this.grdBottomBanner.ExportSettings.OpenInNewWindow = true;
        this.grdBottomBanner.MasterTableView.ExportToExcel();

        grdBottomBanner.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdBottomBanner.ExportSettings.FileName = "BottomBanner";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdBottomBanner_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getBottomBanner();
            grdBottomBanner.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdBottomBanner_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdBottomBanner.CurrentPageIndex + 1;
    }
    protected void grdBottomBanner_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getBottomBannerbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getBottomBanner();
            }

            grdBottomBanner.DataSource = dt;
            grdBottomBanner.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdBottomBanner_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdBottomBanner.ExportSettings.ExportOnlyData = true;
            this.grdBottomBanner.ExportSettings.IgnorePaging = true;
            this.grdBottomBanner.ExportSettings.OpenInNewWindow = true;
            this.grdBottomBanner.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getBottomBannerlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["bb_ID"].ToString();
                    txtTitle.Text = dt.Rows[0]["bb_title"].ToString();
                    lblImage.Text = dt.Rows[0]["bb_image"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteBottomBannerByID(ID);
                    if (res == 1)
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdBottomBanner_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdBottomBanner_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtTitle.Text = "";
        lblImage.Text = "";
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrTitle.Visible = false;
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupImage.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public string getFileLink(string filename)
    {
        string filelink = string.Empty;
        if (!string.IsNullOrEmpty(filename))
        {
            filelink = Constant.URL + Constant.filesavingurlview + filename;
        }

        return filelink;
    }
}