﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="UploadFrontEndData_UpdateBoth.aspx.cs" Inherits="Admin_UploadFrontEndData_UpdateBoth" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                        <h3 class="box-title"><asp:Label ID="lblformname" Text="Data Uploading" runat="server"></asp:Label></h3>
                </div>            
                <div class="box-body"> 
                    <div class="row">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">                      
                               <div class="col-sm-12">
                               <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                               <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                               </div>   
                               </div>
                            </div>
                          <div class="form-group">                      
                               <div class="col-sm-12">
                               <div class="alert alert-success alert-dismissable">                            
                               <center><b>Please Upload Reference Form before Data Form.</b></center>
                               </div>   
                               </div>
                            </div> 
                         <div class="form-group">                      
                               <div class="col-sm-12">                        
                                <fieldset>
                                    <legend><h4>Step-1 : Download Template</h4></legend>
                                    <div class="form-group">
                                      <label for="inputPassword3" class="col-sm-2 control-label">Choose Form</label>
                                      <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlparent" CssClass="form-control" runat="server"></asp:DropDownList>                           
                                      </div>
                                      <div class="col-sm-7">
                                        <asp:Button ID="btnDownloadTemplate" OnClick="btnDownloadTemplate_Click" class="btn btn-info" runat="server" Text="Download Template" />
                                      </div>
                                    </div>
                                </fieldset>
                               </div>
                         </div> 
                         <div class="form-group">                      
                               <div class="col-sm-12">      
                                <fieldset>
                                    <legend><h4>Step-2 : Import Data</h4></legend>
                                    <div class="form-group">  
                                        <label for="inputPassword3" class="col-sm-2 control-label">Upload File</label>                             
                                        <div class="col-sm-3">
                                            <asp:FileUpload ID="fupForm" runat="server" />
                                        </div>
                                        <div class="col-sm-7">
                                            <asp:Button ID="btnImport" OnClick="btnImportData_Click" class="btn btn-info" runat="server" Text="Import Data" />
                                        </div>
                                    </div>
                                </fieldset>                          
                              </div>
                            </div> 
                        </div>      
                    </div>                          
                    <div class="row">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="dataTable_wrapper" style="overflow-y:auto;"><br />
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdForm"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Visible="false" AllowPaging="false" PageSize="50" Font-Size="Small"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="false" OnPageIndexChanged="grdForm_PageIndexChanged" OnNeedDataSource="grdForm_NeedDSource" AutoGenerateColumns="true" HeaderStyle-Font-Bold="true">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="true" DataKeyNames="SrNo" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                                    </MasterTableView>
                                </telerik:RadGrid>      
                            </div>
                        </div>      
                    </div>
                </div>
                <div class="box-footer">                                
                <asp:Button ID="btnSave" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" visible="false"/>
              </div>
          </div>
        </div>
    </section>
</asp:Content>

