﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Newsletter : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblNewsletter";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdNewsletter.ExportSettings.IgnorePaging = true;
                grdNewsletter.ExportSettings.ExportOnlyData = true;
                grdNewsletter.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdNewsletter.ExportSettings.FileName = "Newsletter";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtmenu = daldata.selectNewsletterMenu();

            ddlmenu.DataSource = dtmenu;
            ddlmenu.DataTextField = "m_TitleEn";
            ddlmenu.DataValueField = "m_id";
            ddlmenu.DataBind();
            ListItem lst = new ListItem("Select Menu", "");
            ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("nl_ID");
                dt.Columns.Add("nl_title");
                dt.Columns.Add("nl_issue");
                dt.Columns.Add("nl_doissue");
                dt.Columns.Add("nl_cover");
                dt.Columns.Add("nl_dlink");
                dt.Columns.Add("menuID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["nl_ID"] = hfid.Value.ToString();
                    dr["nl_title"] = daldata.replaceForSQL(txtNLtitle.Text.Trim());
                    dr["nl_issue"] = daldata.replaceForSQL(txtIssue.Text.Trim());
                    dr["nl_doissue"] = daldata.replaceForSQL(txtDateIssue.Text.Trim());
                    dr["nl_cover"] = imagename == "" ? lblfloorplan.Text : imagename;
                    dr["nl_dlink"] = daldata.replaceForSQL(txtDownlink.Text.Trim());
                    dr["menuID"] = ddlmenu.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;



                    int res = daldata.UpdateNewsletter(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdNewsletter_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["nl_ID"] = "";
                    dr["nl_title"] = daldata.replaceForSQL(txtNLtitle.Text.Trim());
                    dr["nl_issue"] = daldata.replaceForSQL(txtIssue.Text.Trim());
                    dr["nl_doissue"] = daldata.replaceForSQL(txtDateIssue.Text.Trim());
                    dr["nl_cover"] = imagename == "" ? lblfloorplan.Text : imagename;
                    dr["nl_dlink"] = daldata.replaceForSQL(txtDownlink.Text.Trim());
                    dr["menuID"] = ddlmenu.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveNewsletter(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdNewsletter_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtNLtitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtNLtitle.Text.Trim()))
            {
                lblerrtitle.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrtitle.Visible = false;
            }
            if (string.IsNullOrEmpty(txtIssue.Text.Trim()) || string.IsNullOrWhiteSpace(txtIssue.Text.Trim()))
            {
                lblerrIssue.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrIssue.Visible = false;
            }
            if (string.IsNullOrEmpty(txtDateIssue.Text.Trim()) || string.IsNullOrWhiteSpace(txtDateIssue.Text.Trim()))
            {
                lblerrDateIssue.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrDateIssue.Visible = false;
            }
            if (ddlmenu.SelectedValue == "")
            {
                lblerrdll.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdll.Visible = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdNewsletter.ExportSettings.ExportOnlyData = true;
        this.grdNewsletter.ExportSettings.IgnorePaging = true;
        this.grdNewsletter.ExportSettings.OpenInNewWindow = true;
        this.grdNewsletter.MasterTableView.ExportToExcel();

        grdNewsletter.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdNewsletter.ExportSettings.FileName = "Newsletter";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdNewsletter_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getNewsletter();
            grdNewsletter.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdNewsletter_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdNewsletter.CurrentPageIndex + 1;
    }
    protected void grdNewsletter_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getNewsletterbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getNewsletter();
            }


            grdNewsletter.DataSource = dt;
            grdNewsletter.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdNewsletter_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdNewsletter.ExportSettings.ExportOnlyData = true;
            this.grdNewsletter.ExportSettings.IgnorePaging = true;
            this.grdNewsletter.ExportSettings.OpenInNewWindow = true;
            this.grdNewsletter.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getNewsletterlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["nl_ID"].ToString();
                    txtNLtitle.Text = dt.Rows[0]["nl_title"].ToString();
                    txtIssue.Text = dt.Rows[0]["nl_issue"].ToString();
                    txtDateIssue.Text = dt.Rows[0]["nl_doissue"].ToString();
                    txtDownlink.Text = dt.Rows[0]["nl_dlink"].ToString();
                    ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                    lblfloorplan.Text = dt.Rows[0]["nl_cover"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteNewsletterByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdNewsletter_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdNewsletter_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtNLtitle.Text = "";
        txtDownlink.Text = "";
        txtIssue.Text = "";
        txtDateIssue.Text = "";
        ddlmenu.SelectedIndex = 0;
        lblfloorplan.Text = "";
    }
}