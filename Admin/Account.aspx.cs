﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_Account : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblAccount";
    string tablenameContactList = "ContactList";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdAccount.ExportSettings.IgnorePaging = true;
                grdAccount.ExportSettings.ExportOnlyData = true;
                grdAccount.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdAccount.ExportSettings.FileName = "Account";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtName = new DataTable();

            //Add Columns to Table
            dtName.Columns.Add(new DataColumn("AccountType"));
            dtName.Columns.Add(new DataColumn("AccountID"));

            //Now Add Values
            dtName.Rows.Add("Select Account Type", "0");
            dtName.Rows.Add("Delegate", "D");
            dtName.Rows.Add("Speaker", "S");
            dtName.Rows.Add("Exhibitor", "E");

            //At Last Bind datatable to dropdown.
            ddlAcc.DataSource = dtName;
            ddlAcc.DataTextField = dtName.Columns["AccountType"].ToString();
            ddlAcc.DataValueField = dtName.Columns["AccountID"].ToString();
            ddlAcc.DataBind();


            DataTable dtSName = new DataTable();

            //Add Columns to Table
            dtSName.Columns.Add(new DataColumn("SalType"));
            dtSName.Columns.Add(new DataColumn("SalID"));

            //Now Add Values
            dtSName.Rows.Add("Select Salutation", "0");
            dtSName.Rows.Add("Sir", "Sir");
            dtSName.Rows.Add("Mr", "Mr");
            dtSName.Rows.Add("Ms", "Ms");
            dtSName.Rows.Add("Mrs", "Mrs");

            //At Last Bind datatable to dropdown.
            ddlSal.DataSource = dtSName;
            ddlSal.DataTextField = dtSName.Columns["SalType"].ToString();
            ddlSal.DataValueField = dtSName.Columns["SalID"].ToString();
            ddlSal.DataBind();

        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("a_ID");
                dt.Columns.Add("a_fullname");
                dt.Columns.Add("a_type");
                dt.Columns.Add("a_sal");
                dt.Columns.Add("a_fname");
                dt.Columns.Add("a_lname");
                dt.Columns.Add("a_designation");
                dt.Columns.Add("a_email");
                dt.Columns.Add("a_company");
                dt.Columns.Add("a_addressOfc");
                dt.Columns.Add("a_addressHome");
                dt.Columns.Add("a_country");
                dt.Columns.Add("a_Tel");
                dt.Columns.Add("a_Mobile");
                dt.Columns.Add("a_LoginName");
                dt.Columns.Add("a_Password");
                dt.Columns.Add("a_isActivated");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("a_AttendDay");
                dt.Columns.Add("Biography");
                dt.Columns.Add("ProfilePic");

                string selectedDayValue = "";
                for (int i = 0; i < chkDay.Items.Count; i++)
                {
                    if (chkDay.Items[i].Selected)
                    {
                        selectedDayValue += chkDay.Items[i].Value + ",";
                    }
                }
                selectedDayValue = selectedDayValue.TrimEnd(',');

                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }

                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["a_ID"] = hfid.Value.ToString();
                    dr["a_fullname"] = daldata.replaceForSQL(txtFullName.Text.Trim());
                    dr["a_type"] = ddlAcc.SelectedItem.Value;
                    dr["a_sal"] = ddlSal.SelectedItem.Value;
                    dr["a_fname"] = daldata.replaceForSQL(txtFirstName.Text.Trim());
                    dr["a_lname"] = daldata.replaceForSQL(txtLastName.Text.Trim());
                    dr["a_designation"] = daldata.replaceForSQL(txtDesignation.Text.Trim());
                    dr["a_email"] = daldata.replaceForSQL(txtEmail.Text.Trim());
                    dr["a_company"] = daldata.replaceForSQL(txtCompany.Text.Trim());
                    dr["a_addressOfc"] = daldata.replaceForSQL(txtAddOfc.Text.Trim());
                    dr["a_addressHome"] = daldata.replaceForSQL(txtAddHome.Text.Trim());
                    dr["a_country"] = daldata.replaceForSQL(txtCountry.Text.Trim());
                    dr["a_Tel"] = daldata.replaceForSQL(txtTelephone.Text.Trim());
                    dr["a_Mobile"] = daldata.replaceForSQL(txtMobile.Text.Trim());
                    dr["a_LoginName"] = daldata.replaceForSQL(txtLoginName.Text.Trim());
                    dr["a_Password"] = daldata.replaceForSQL(txtPassword.Text.Trim());
                    dr["a_isActivated"] = rdbtnIsActivated.SelectedValue;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["a_AttendDay"] = selectedDayValue;
                    dr["Biography"] = daldata.replaceForSQL(txtbio.Content.Trim());
                    dr["ProfilePic"] = imagename == "" ? lblLogo.Text : imagename;

                    int res = daldata.UpdateAccount(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        daldata.UpdateDataVersion(tablenameContactList);
                        grdAccount_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["a_ID"] = "";
                    dr["a_fullname"] = daldata.replaceForSQL(txtFullName.Text.Trim());
                    dr["a_type"] = ddlAcc.SelectedItem.Value;
                    dr["a_sal"] = ddlSal.SelectedItem.Value;
                    dr["a_fname"] = daldata.replaceForSQL(txtFirstName.Text.Trim());
                    dr["a_lname"] = daldata.replaceForSQL(txtLastName.Text.Trim());
                    dr["a_designation"] = daldata.replaceForSQL(txtDesignation.Text.Trim());
                    dr["a_email"] = daldata.replaceForSQL(txtEmail.Text.Trim());
                    dr["a_company"] = daldata.replaceForSQL(txtCompany.Text.Trim());
                    dr["a_addressOfc"] = daldata.replaceForSQL(txtAddOfc.Text.Trim());
                    dr["a_addressHome"] = daldata.replaceForSQL(txtAddHome.Text.Trim());
                    dr["a_country"] = daldata.replaceForSQL(txtCountry.Text.Trim());
                    dr["a_Tel"] = daldata.replaceForSQL(txtTelephone.Text.Trim());
                    dr["a_Mobile"] = daldata.replaceForSQL(txtMobile.Text.Trim());
                    dr["a_LoginName"] = daldata.replaceForSQL(txtLoginName.Text.Trim());
                    dr["a_Password"] = daldata.replaceForSQL(txtPassword.Text.Trim());
                    dr["a_isActivated"] =rdbtnIsActivated.SelectedValue;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    dr["a_AttendDay"] = selectedDayValue;
                    dr["Biography"] = daldata.replaceForSQL(txtbio.Content.Trim());
                    dr["ProfilePic"] = imagename == "" ? lblLogo.Text : imagename;
                    int res = daldata.SaveAccount(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        daldata.UpdateDataVersion(tablenameContactList);
                        grdAccount_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtFullName.Text.Trim()) || string.IsNullOrWhiteSpace(txtFullName.Text.Trim()))
            {
                lblerrfullName.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrfullName.Visible = false;
            }
            if (string.IsNullOrEmpty(txtFirstName.Text.Trim()) || string.IsNullOrWhiteSpace(txtFirstName.Text.Trim()))
            {
                lblerrfname.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrfname.Visible = false;
            }
            if (string.IsNullOrEmpty(txtLastName.Text.Trim()) || string.IsNullOrWhiteSpace(txtLastName.Text.Trim()))
            {
                lblerrlname.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrlname.Visible = false;
            }
            if (string.IsNullOrEmpty(txtDesignation.Text.Trim()) || string.IsNullOrWhiteSpace(txtDesignation.Text.Trim()))
            {
                lblerrdesignation.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdesignation.Visible = false;
            }
            if (string.IsNullOrEmpty(txtEmail.Text.Trim()) || string.IsNullOrWhiteSpace(txtEmail.Text.Trim()))
            {
                lblerremail.Visible = true;
                rtn = false;
            }
            else
            {
                lblerremail.Visible = false;
            }
            if (string.IsNullOrEmpty(txtAddOfc.Text.Trim()) || string.IsNullOrWhiteSpace(txtAddOfc.Text.Trim()))
            {
                lblerrAddOfc.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrAddOfc.Visible = false;
            }
            if (string.IsNullOrEmpty(txtCountry.Text.Trim()) || string.IsNullOrWhiteSpace(txtCountry.Text.Trim()))
            {
                lblerrCountry.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrCountry.Visible = false;
            }
            if (string.IsNullOrEmpty(txtCompany.Text.Trim()) || string.IsNullOrWhiteSpace(txtCompany.Text.Trim()))
            {
                lblerrcompany.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrcompany.Visible = false;
            }
            if (string.IsNullOrEmpty(txtMobile.Text.Trim()) || string.IsNullOrWhiteSpace(txtMobile.Text.Trim()))
            {
                lblerrMobile.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrMobile.Visible = false;
            }
            if (string.IsNullOrEmpty(txtLoginName.Text.Trim()) || string.IsNullOrWhiteSpace(txtLoginName.Text.Trim()))
            {
                lblerrLoginName.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrLoginName.Visible = false;
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()) || string.IsNullOrWhiteSpace(txtPassword.Text.Trim()))
            {
                lblerrPassword.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrPassword.Visible = false;
            }
            if (ddlAcc.SelectedValue == "0")
            {
                lblerrdllA.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdllA.Visible = false;
            }
            if (ddlSal.SelectedValue == "0")
            {
                lblerrdllS.Visible = true;
                rtn = false;
            }
            else
            {
                lblerrdllS.Visible = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdAccount.ExportSettings.ExportOnlyData = true;
        this.grdAccount.ExportSettings.IgnorePaging = true;
        this.grdAccount.ExportSettings.OpenInNewWindow = true;
        this.grdAccount.MasterTableView.ExportToExcel();

        grdAccount.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdAccount.ExportSettings.FileName = "Account";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdAccount_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getAccount();
            grdAccount.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdAccount_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdAccount.CurrentPageIndex + 1;
    }
    protected void grdAccount_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getAccountbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getAccount();
            }

            grdAccount.DataSource = dt;
            grdAccount.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdAccount_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdAccount.ExportSettings.ExportOnlyData = true;
            this.grdAccount.ExportSettings.IgnorePaging = true;
            this.grdAccount.ExportSettings.OpenInNewWindow = true;
            this.grdAccount.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getAccountlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["a_ID"].ToString();
                    txtFullName.Text= dt.Rows[0]["a_fullname"].ToString();
                    txtFirstName.Text = dt.Rows[0]["a_fname"].ToString();
                    txtLastName.Text = dt.Rows[0]["a_lname"].ToString();
                    txtDesignation.Text= dt.Rows[0]["a_designation"].ToString();
                    txtCompany.Text = dt.Rows[0]["a_company"].ToString();
                    string type = dt.Rows[0]["a_type"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(type))
                        {
                            ListItem listItem = ddlAcc.Items.FindByValue(type);
                            if (listItem != null)
                            {
                                ddlAcc.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string sal = dt.Rows[0]["a_sal"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(sal))
                        {
                            ListItem listItem = ddlSal.Items.FindByValue(sal);
                            if (listItem != null)
                            {
                                ddlSal.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    txtEmail.Text = dt.Rows[0]["a_email"].ToString();
                    txtAddOfc.Text = dt.Rows[0]["a_addressOfc"].ToString();
                    txtAddHome.Text = dt.Rows[0]["a_addressHome"].ToString();
                    txtCountry.Text = dt.Rows[0]["a_country"].ToString();
                    txtTelephone.Text = dt.Rows[0]["a_Tel"].ToString();
                    txtMobile.Text = dt.Rows[0]["a_Mobile"].ToString();
                    txtLoginName.Text = dt.Rows[0]["a_LoginName"].ToString();
                    txtPassword.Text = dt.Rows[0]["a_Password"].ToString();

                    try
                    {
                        rdbtnIsActivated.SelectedValue = dt.Rows[0]["a_isActivated"] != DBNull.Value ? (!string.IsNullOrEmpty(dt.Rows[0]["a_isActivated"].ToString()) ? dt.Rows[0]["a_isActivated"].ToString() : "False") : "False";
                    }
                    catch(Exception ex)
                    { }
                    string attendDay= dt.Rows[0]["a_AttendDay"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(attendDay))
                        {
                            string[] strattendDay = attendDay.Split(',');
                            if(strattendDay.Length > 0)
                            {
                                foreach(var day in strattendDay)
                                {
                                    ListItem listItem = chkDay.Items.FindByValue(day);
                                    if (listItem != null)
                                    {
                                        listItem.Selected = true;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    txtbio.Content = dt.Rows[0]["Biography"].ToString();
                    lblLogo.Text = dt.Rows[0]["ProfilePic"].ToString();
                    if (!string.IsNullOrEmpty(lblLogo.Text) && !string.IsNullOrWhiteSpace(lblLogo.Text))
                    {
                        btnDeleteImage.Visible = true;
                    }
                    else
                    {
                        btnDeleteImage.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteAccountByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        daldata.UpdateDataVersion(tablenameContactList);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdAccount_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdAccount_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
   
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtFullName.Text = "";
        txtCompany.Text = "";
        txtFirstName.Text = "";
        txtLastName.Text = "";
        ddlAcc.SelectedValue = "0";
        ddlSal.SelectedValue = "0";
        txtEmail.Text = "";
        txtDesignation.Text = "";
        txtAddOfc.Text = "";
        txtAddHome.Text = "";
        txtCountry.Text = "";
        txtTelephone.Text = "";
        txtMobile.Text = "";
        txtLoginName.Text = "";
        txtPassword.Text = "";
        rdbtnIsActivated.SelectedValue = "True";
        chkDay.ClearSelection();

        txtbio.Content = "";
        lblLogo.Text = "";
        divDeleteImageMsg.Visible = false;
    }

    #region upload
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    public string getFileLink(string filename)
    {
        string filelink = string.Empty;
        if (!string.IsNullOrEmpty(filename))
        {
            filelink = Constant.URL + Constant.filesavingurlview + filename;
        }

        return filelink;
    }
    protected void btnDeleteImage_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hfid.Value))
        {
            string accID = hfid.Value.Trim();
            if (!string.IsNullOrEmpty(lblLogo.Text) && !string.IsNullOrWhiteSpace(lblLogo.Text))
            {
                int issuccess = daldata.DeleteAccountImageByID(accID);
                if (issuccess > 0)
                {
                    DataTable dt = daldata.getAccountlstByID(accID);
                    if (dt.Rows.Count > 0)
                    {
                        lblLogo.Text = dt.Rows[0]["ProfilePic"].ToString();
                        if (!string.IsNullOrEmpty(lblLogo.Text) && !string.IsNullOrWhiteSpace(lblLogo.Text))
                        {
                            btnDeleteImage.Visible = true;
                        }
                        else
                        {
                            btnDeleteImage.Visible = false;
                        }
                    }
                    divDeleteImageMsg.Visible = true;
                }
            }
        }
    }
    #endregion
}