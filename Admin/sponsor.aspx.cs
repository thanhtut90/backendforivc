﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_sponsor : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblSponsor";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdsponsor.ExportSettings.IgnorePaging = true;
                grdsponsor.ExportSettings.ExportOnlyData = true;
                grdsponsor.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdsponsor.ExportSettings.FileName = "Sponsor";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtmenu = daldata.selectSponsornMenu();

            ddlmenu.DataSource = dtmenu;
            ddlmenu.DataTextField = "m_TitleEn";
            ddlmenu.DataValueField = "m_id";
            ddlmenu.DataBind();
            ListItem lst1 = new ListItem("Select Menu", "");
            ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst1);

            DataTable dtsponsor = daldata.getsponsorcategory();

            ddlcategory.DataSource = dtsponsor;
            ddlcategory.DataTextField = "sponsorCat_name";
            ddlcategory.DataValueField = "sponsorCat_ID";
            ddlcategory.DataBind();
            ListItem lst2 = new ListItem("Select Sponsor Category", "");
            ddlcategory.Items.Insert(ddlcategory.Items.Count - ddlcategory.Items.Count, lst2);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("s_ID");
                dt.Columns.Add("s_exhID");
                dt.Columns.Add("menuID");
                dt.Columns.Add("s_name");
                dt.Columns.Add("s_content");
                dt.Columns.Add("s_logo");
                dt.Columns.Add("s_category");
                dt.Columns.Add("s_seq");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");


                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupForm.HasFile == true)
                {
                    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }
                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();

                    dr["s_ID"] = hfid.Value.ToString();
                    dr["s_exhID"] = txtexhid.Text.Trim();
                    dr["menuID"] = ddlmenu.SelectedValue;
                    dr["s_name"] = daldata.replaceForSQL(txtsponsor.Text.Trim());
                    dr["s_content"] = daldata.replaceForSQL(txtContent.Content);
                    dr["s_logo"] = imagename == "" ? lblLogo.Text : imagename;
                    dr["s_category"] = ddlcategory.SelectedValue;
                    dr["s_seq"] = txtseq.Text.Trim();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateSponsor(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdsponsor_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["s_ID"] = "";
                    dr["s_exhID"] = txtexhid.Text.Trim();
                    dr["menuID"] = ddlmenu.SelectedValue;
                    dr["s_name"] = daldata.replaceForSQL(txtsponsor.Text.Trim());
                    dr["s_content"] = daldata.replaceForSQL(txtContent.Content);
                    dr["s_logo"] = imagename == "" ? lblLogo.Text : imagename;
                    dr["s_category"] = ddlcategory.SelectedValue;
                    dr["s_seq"] = txtseq.Text.Trim();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveSponsor(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdsponsor_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtsponsor.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdsponsor.ExportSettings.ExportOnlyData = true;
        this.grdsponsor.ExportSettings.IgnorePaging = true;
        this.grdsponsor.ExportSettings.OpenInNewWindow = true;
        this.grdsponsor.MasterTableView.ExportToExcel();

        grdsponsor.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdsponsor.ExportSettings.FileName = "Sponsor";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdsponsor_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getSponsor();
            grdsponsor.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdsponsor_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdsponsor.CurrentPageIndex + 1;
    }
    protected void grdsponsor_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getSponsorbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getSponsor();
            }


            grdsponsor.DataSource = dt;
            grdsponsor.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdsponsor_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdsponsor.ExportSettings.ExportOnlyData = true;
            this.grdsponsor.ExportSettings.IgnorePaging = true;
            this.grdsponsor.ExportSettings.OpenInNewWindow = true;
            this.grdsponsor.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getSponsorByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["s_ID"].ToString();
                    txtsponsor.Text = dt.Rows[0]["s_name"].ToString();
                    txtContent.Content= dt.Rows[0]["s_content"].ToString();
                    txtseq.Text= dt.Rows[0]["s_seq"].ToString();
                    txtexhid.Text= dt.Rows[0]["s_exhID"].ToString();
                    lblLogo.Text= dt.Rows[0]["s_logo"].ToString();
                    ddlmenu.SelectedValue= dt.Rows[0]["menuID"].ToString();
                    ddlcategory.SelectedValue= dt.Rows[0]["s_category"].ToString();

                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteSponsorByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdsponsor_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdsponsor_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupForm.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtsponsor.Text = "";
        txtContent.Content = "";
        ddlcategory.SelectedIndex = 0;
        txtseq.Text = "";
        ddlmenu.SelectedIndex = 0;
        txtexhid.Text = "";
        lblLogo.Text = "";
    }
}