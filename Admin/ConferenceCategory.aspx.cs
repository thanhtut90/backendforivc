﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class Admin_ConferenceCategory : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblConferenceCategory";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdConferenceCategory.ExportSettings.IgnorePaging = true;
                grdConferenceCategory.ExportSettings.ExportOnlyData = true;
                grdConferenceCategory.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdConferenceCategory.ExportSettings.FileName = "ConferenceCategory";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string id = hfid.Value;
            if (Validation())
            {
                lblerrName.Text = string.Empty;
                lblerrNo.Text = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("cc_ID");
                dt.Columns.Add("cc_categoryname");
                dt.Columns.Add("cc_seq");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                string namestr = daldata.replaceForSQL(txtCategory.Text.Trim());
                int seqno = int.Parse(daldata.replaceForSQL(txtSeq.Text.Trim()));
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["cc_ID"] = hfid.Value.ToString();
                    dr["cc_categoryname"] = namestr;
                    dr["cc_seq"] = seqno;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.UpdateConferenceCategory(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceCategory_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["cc_ID"] = "";
                    dr["cc_categoryname"] = namestr;
                    dr["cc_seq"] = seqno;
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    string ccID = "";
                    int res = daldata.SaveConferenceCategoryGetID(dr, ref ccID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdConferenceCategory_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }

    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdConferenceCategory.ExportSettings.ExportOnlyData = true;
        this.grdConferenceCategory.ExportSettings.IgnorePaging = true;
        this.grdConferenceCategory.ExportSettings.OpenInNewWindow = true;
        this.grdConferenceCategory.MasterTableView.ExportToExcel();
        grdConferenceCategory.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdConferenceCategory.ExportSettings.FileName = "ConferenceCategory";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        string seqno = daldata.getLastConferenceCategorySequence();
        int no = int.Parse(seqno) + 1;
        txtSeq.Text = no.ToString();
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdConferenceCategory_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getConferenceCategory();
            grdConferenceCategory.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceCategory_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdConferenceCategory.CurrentPageIndex + 1;
    }
    protected void grdConferenceCategory_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getConferenceCategorybyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getConferenceCategory();
            }

            grdConferenceCategory.DataSource = dt;
            grdConferenceCategory.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdConferenceCategory_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdConferenceCategory.ExportSettings.ExportOnlyData = true;
            this.grdConferenceCategory.ExportSettings.IgnorePaging = true;
            this.grdConferenceCategory.ExportSettings.OpenInNewWindow = true;
            this.grdConferenceCategory.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getConferenceCategorylstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["cc_ID"].ToString();
                    txtCategory.Text = dt.Rows[0]["cc_categoryname"].ToString();
                    txtSeq.Text = dt.Rows[0]["cc_seq"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteConferenceCategoryByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdConferenceCategory_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdConferenceCategory_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtCategory.Text = "";
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtCategory.Text.Trim()) || string.IsNullOrWhiteSpace(txtCategory.Text.Trim()))
            {
                lblerrName.Text = "**Required"; lblerrName.Visible = true; rtn = false;
            }
            else
            { lblerrName.Visible = false; }

            if (string.IsNullOrEmpty(txtSeq.Text.Trim()) || string.IsNullOrWhiteSpace(txtSeq.Text.Trim()))
            {
                lblerrNo.Text = "**Required";
                lblerrNo.Visible = true;
                rtn = false;
            }
            else
            {
                string text = txtSeq.Text;
                           
                DataTable dtseq = daldata.getConferenceCategoryAllSequence();
                int number;
                if (!int.TryParse(text, out number))
                {
                    lblerrNo.Text = "** Please specify number only";
                    lblerrNo.Visible = true; rtn = false;
                }
                else if (int.Parse(text) <= 0)
                {
                    lblerrNo.Text = "** Sequence start from '1'";
                    lblerrNo.Visible = true; rtn = false;
                }
                else
                {
                    if (dtseq.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dtseq.Rows)
                        {
                            if (!string.IsNullOrEmpty(hfid.Value))
                            {
                                DataTable dt = daldata.getConferenceCategorylstByID(hfid.Value);

                                if (int.Parse(text) == int.Parse(dr["cc_seq"].ToString()) && text != dt.Rows[0]["cc_seq"].ToString())
                                {
                                    lblerrNo.Text = "Sequence No. is already exist.";
                                    lblerrNo.Visible = true; rtn = false;
                                }
                            }else
                            {
                                if (int.Parse(text) == int.Parse(dr["cc_seq"].ToString()))
                                {
                                    lblerrNo.Text = "Sequence No. is already exist.";
                                    lblerrNo.Visible = true; rtn = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
}