﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Admin_Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <%-- <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server"></telerik:RadAjaxManager>       
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard        
      </h1>

    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Visitors</span>
              <span class="info-box-number"><%=visitors %></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Exhibitors</span>
              <span class="info-box-number"><%=exhibitors %></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="icon ion-ios-cloud-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Download Apps</span>
              <span class="info-box-number"><%=downloadappcount %></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Most Popular Exhibitor</span>                 
              
              <span class="info-box-number"><%=mostpopularexhibitor %></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Appointment Report by Exhibition Days</h3>            
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-wrench"></i></button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <p class="text-center">
                    <strong><%=eventduration %></strong>
                  </p>

                  <div class="chart">
                      <telerik:RadHtmlChart runat="server" ID="ChartAppointmentByDay"  CssClass="fb-sized">
                        <PlotArea>
                            <Series>
                                <telerik:LineSeries DataFieldY="Total"  Name="Appointments" >
                                    <LabelsAppearance DataFormatString="{0}">
                                    </LabelsAppearance>
                                    <TooltipsAppearance Color="White" DataFormatString="{0}"></TooltipsAppearance>
                                </telerik:LineSeries>
                            </Series>                                                      
                        </PlotArea>                     
                    </telerik:RadHtmlChart>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                <div class="col-md-6">  
                    <p class="text-center">
                        <strong>Top 5 Exhibitors Having Most Appointments</strong>
                    </p>                    
                    <div class="chart">
                        <telerik:RadHtmlChart runat="server" ID="ChartTop5Exhi" Skin="Silk">
                            <PlotArea>
                                <Series>
                                    <telerik:ColumnSeries DataFieldY="Count">
                                        <LabelsAppearance DataFormatString="{0}">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" DataFormatString="{0}"></TooltipsAppearance>
                                    </telerik:ColumnSeries>
                                </Series>                                                      
                            </PlotArea>                     
                        </telerik:RadHtmlChart>
                    </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
           
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Survey Report</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#V" data-toggle="tab">Visitor</a></li>
                          <li><a href="#E" data-toggle="tab">Exhibitor</a></li>
                          
                        </ul>
                        <div class="tab-content">
                          <div class="active tab-pane" id="V">                
                              <div class="pad">
                                <div class="table-responsive">
                                    <table class="table no-margin">                     
                                          <%=Vhtmltb %>
                                    </table>
                                </div>
                              </div>
                          </div>
                          <!-- /.tab-pane -->
                          <div class="tab-pane" id="E">
                              <div class="pad">
                                <div class="table-responsive">
                                    <table class="table no-margin">                     
                                          <%=Ehtmltb %>
                                    </table>
                                </div>
                              </div>
                          </div>
                          <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                      </div>

                </div>
                <!-- /.col -->               
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
          <!-- /.row -->

          <!-- TABLE: LATEST ORDERS -->
          
          <!-- /.box -->
        </div>
        <!-- /.col -->

        
        <!-- /.col -->
      </div>

        <div class="row">
            <div class="col-md-8">
                <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Latest Appointments</h3>

                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                      <%--  <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="false" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="false" PageSize="10" class="table no-margin">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="id" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  

                                <telerik:GridTemplateColumn  DataField="vid" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Visitor">
                                    <ItemTemplate>
                                           <%--<%# getVisitor(Eval("vid").ToString())%>   \                                 
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn  DataField="eid" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Exhibitor">
                                    <ItemTemplate>
                                         <%--  <%# getExhibitor(Eval("eid").ToString())%>                                    
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>                                                                      
                                <telerik:GridBoundColumn  DataField="date" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Date" SortExpression="date" UniqueName="date" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn> 
                                <telerik:GridBoundColumn  DataField="time" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Time" SortExpression="time" UniqueName="time" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>   
                                <telerik:GridTemplateColumn  DataField="status" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Status">
                                    <ItemTemplate>
                                          <%-- <%# getStatus(Eval("status").ToString())%>                                  
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>                                                      
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    </div>                
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <%--<a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
                    <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Appointments</a>
                </div>
                <!-- /.box-footer -->
                </div>
            </div>
            <div class="col-md-4">          
                <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Top 5 Most Interested Products</h3>

                    <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                    <div class="col-md-12">
                        <div class="chart-responsive">                    
                            <telerik:RadHtmlChart runat="server" ID="ChartProductofInterest"  CssClass="fb-sized">
                            <PlotArea>
                                <Series>
                                    <telerik:PieSeries StartAngle="45" DataFieldY="Total" ExplodeField="IsExploded" Name="PieSeriesName"
                                        NameField="Desc">
                                        <LabelsAppearance DataFormatString="{0}%">
                                        </LabelsAppearance>
                                        <TooltipsAppearance Color="White" DataFormatString="{0}%"></TooltipsAppearance>
                                    </telerik:PieSeries>
                                </Series>
                                <YAxis>
                                </YAxis>
                            </PlotArea>                        
                        </telerik:RadHtmlChart>
                        </div>
                        <!-- ./chart-responsive -->
                    </div>
                    <!-- /.col -->

                    </div>
                    <!-- /.row -->
                </div>
                </div>
            </div>
        </div>
            
      <!-- /.row -->
    </section>
    <!-- /.content -->
  <%--</div>--%>


</asp:Content>

