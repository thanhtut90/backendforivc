﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="WebMenuConfiguration.aspx.cs" Inherits="Admin_WebMenuConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Default">
        <TargetControls>
            <telerik:TargetControl ControlID="txthtmlstr" Skin="Metro" />
            <telerik:TargetControl ControlID="grdMenu" Skin="Metro" />
        </TargetControls>
    </telerik:RadSkinManager>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Menu Configuration Form</h3>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Menu Title</label>
                    <div class="col-sm-6">
                        <asp:TextBox ID="txtmenutitle" class="form-control" placeholder="Enter Menu Title" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Redirect URL</label>
                    <div class="col-sm-6">
                        <asp:TextBox ID="txturl" class="form-control" placeholder="Enter Redirect URL" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Order Number</label>
                    <div class="col-sm-6">
                        <asp:Label ID="lblmorder" CssClass="form-control" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Is Child</label>
                    <div class="col-sm-6">
                        <asp:CheckBox ID="chkisChild" runat="server" OnCheckedChanged="chkisChild_CheckedChanged" Text="&nbsp;&nbsp;Is Child" AutoPostBack="true" />
                    </div>
                </div>
                <div class="form-group" id="pmenu" runat="server" visible="false">
                    <label for="inputEmail3" class="col-sm-2 control-label">Parent Menu</label>
                    <div class="col-sm-6">
                        <asp:DropDownList ID="ddlparent" CssClass="form-control" OnSelectedIndexChanged="ddlparent_SelectedIndexChanged" runat="server"></asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Menu Icon</label>
                    <div class="col-sm-6">
                        <asp:Label ID="lblmicon" class="form-control" CssClass="form-control" runat="server"></asp:Label>
                        <asp:FileUpload ID="fupicon" runat="server" />
                    </div>
                </div>
                <div class="box-footer">
                    <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                </div>
                <!-- /.box-footer -->
            </div>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                    <telerik:RadGrid RenderMode="Lightweight" ID="grdMenu"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdMenu_NeedDSource" AllowPaging="true" PageSize="20">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="m_id" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Editspp" CommandArgument='<%#Eval("m_id") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("m_id") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>                     
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn  DataField="m_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Menu Title" SortExpression="m_title" UniqueName="m_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn> 
                                <telerik:GridBoundColumn  DataField="m_pagetoredirect" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Redirect URL" SortExpression="m_pagetoredirect" UniqueName="m_pagetoredirect" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                                                                                                              
                                <telerik:GridTemplateColumn  DataField="m_parentmenuID" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Parent Menu">
                                    <ItemTemplate>
                                            <%# getParentMenu(Eval("m_parentmenuID").ToString()) %>
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>      
                                <telerik:GridBoundColumn  DataField="m_sortorder" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Sort Order" SortExpression="m_sortorder" UniqueName="m_sortorder" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                            
                                <telerik:GridBoundColumn  DataField="m_IconUrl" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Icon" SortExpression="m_IconUrl" UniqueName="m_IconUrl" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                     
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
            </div>
        </div>

    </div>
</asp:Content>

