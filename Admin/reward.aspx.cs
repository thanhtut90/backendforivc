﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_reward : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblReward";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                grdReward.ExportSettings.IgnorePaging = true;
                grdReward.ExportSettings.ExportOnlyData = true;
                grdReward.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdReward.ExportSettings.FileName = "Reward";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("rw_ID");
                dt.Columns.Add("rw_title1");
                dt.Columns.Add("rw_title2");
                dt.Columns.Add("rw_image1");
                dt.Columns.Add("rw_image2");
                dt.Columns.Add("rw_password");
                dt.Columns.Add("rw_content1");
                dt.Columns.Add("rw_content2");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string imagename1 = "";
                string imagename2 = "";
                string filename1 = string.Empty;
                string filename2 = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupImg1.HasFile == true)
                {
                    filename1 = fupImg1.FileName.Substring(0, fupImg1.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupImg1.FileName).ToString().ToLower();
                    imagename1 = UploadIcon(filename1, extension, path,fupImg1, ref message);
                }

                if (fupImg2.HasFile == true)
                {
                    filename2 = fupImg2.FileName.Substring(0, fupImg2.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupImg2.FileName).ToString().ToLower();
                    imagename2 = UploadIcon(filename2, extension, path, fupImg2, ref message);
                }
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["rw_ID"] = hfid.Value.ToString();
                    dr["rw_title1"] = daldata.replaceForSQL(txtRewardTitle.Text.Trim());
                    dr["rw_title2"] = daldata.replaceForSQL(txtstitle.Text.Trim());
                    dr["rw_image1"] = imagename1 == "" ? lblImg1.Text : imagename1;
                    dr["rw_image2"] = imagename2 == "" ? lblImg2.Text : imagename2;
                    dr["rw_password"] = daldata.replaceForSQL(txtRewardPassword.Text.Trim());
                    dr["rw_content1"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["rw_content2"] = daldata.replaceForSQL(txtContent2.Content.Trim());                   
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateReward(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdReward_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["rw_ID"] = "";
                    dr["rw_title1"] = daldata.replaceForSQL(txtRewardTitle.Text.Trim());
                    dr["rw_title2"] = daldata.replaceForSQL(txtstitle.Text.Trim());
                    dr["rw_image1"] = imagename1 == "" ? lblImg1.Text : imagename1;
                    dr["rw_image2"] = imagename2 == "" ? lblImg2.Text : imagename2;
                    dr["rw_password"] = daldata.replaceForSQL(txtRewardPassword.Text.Trim());
                    dr["rw_content1"] = daldata.replaceForSQL(txtContent.Content.Trim());
                    dr["rw_content2"] = daldata.replaceForSQL(txtContent2.Content.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveReward(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdReward_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        lnkAdd.Visible = true;
                        lnkDownload.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtRewardTitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtRewardTitle.Text.Trim()))
            {
                lblerrRewardTitle.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtstitle.Text.Trim()) || string.IsNullOrWhiteSpace(txtstitle.Text.Trim()))
            {
                lblerrstitle.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtRewardPassword.Text.Trim()) || string.IsNullOrWhiteSpace(txtRewardPassword.Text.Trim()))
            {
                lblerrRewardPassword.Visible = true;
                rtn = false;
            }
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdReward.ExportSettings.ExportOnlyData = true;
        this.grdReward.ExportSettings.IgnorePaging = true;
        this.grdReward.ExportSettings.OpenInNewWindow = true;
        this.grdReward.MasterTableView.ExportToExcel();

        grdReward.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdReward.ExportSettings.FileName = "Reward";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;
        lnkAdd.Visible = false;
        lnkDownload.Visible = false;
        clearControls();
    }
    protected void grdReward_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getReward();
            grdReward.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdReward_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdReward.CurrentPageIndex + 1;
    }
    protected void grdReward_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getRewardbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getReward();
            }

            grdReward.DataSource = dt;
            grdReward.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdReward_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdReward.ExportSettings.ExportOnlyData = true;
            this.grdReward.ExportSettings.IgnorePaging = true;
            this.grdReward.ExportSettings.OpenInNewWindow = true;
            this.grdReward.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            lnkDownload.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getRewardlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["rw_ID"].ToString();
                    txtRewardTitle.Text = dt.Rows[0]["rw_title1"].ToString();
                    txtstitle.Text = dt.Rows[0]["rw_title2"].ToString();
                    lblImg1.Text = dt.Rows[0]["rw_image1"].ToString();
                    lblImg2.Text = dt.Rows[0]["rw_image2"].ToString();
                    txtRewardPassword.Text = dt.Rows[0]["rw_password"].ToString();
                    txtContent.Content = dt.Rows[0]["rw_content1"].ToString();
                    txtContent2.Content = dt.Rows[0]["rw_content2"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteRewardByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdReward_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdReward_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtRewardTitle.Text = "";
        txtstitle.Text = "";
        lblImg1.Text = "";
        lblImg2.Text = "";
        txtRewardPassword.Text = "";
        txtContent2.Content = "";
        txtContent.Content = "";
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrRewardTitle.Visible = false;
        lblerrstitle.Visible = false;
        lblerrRewardPassword.Visible = false;
    }
    private string UploadIcon(string filename, string extension, string path, FileUpload fup, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fup.SaveAs(savePath);

            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }

}