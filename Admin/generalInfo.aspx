﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="generalInfo.aspx.cs" Inherits="Admin_generalInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdgeneral" />  
            <telerik:TargetControl ControlID="txtContent"/>   
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">                      
                        <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>   
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">General Information</h3>
                    <div style="float:right">                            
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>                        
                </div>                                       
                <div class="box-body"> 
                    <div id="divsearch" runat="server" class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                        <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter General Information Title" runat="server"></asp:TextBox>                    
                        </div>
                        <div class="col-md-1">                        
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />   
                        </div>
                        <div class="col-md-6">
                        <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                        
                    </div>  
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdgeneral"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdgeneral_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdgeneral_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdgeneral_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="gi_ID" CommandItemDisplay="Top">
                                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true"/>
                                        <Columns> 
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>
                                               <%# Container.ItemIndex + 1  %>  
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn>     
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">                                    
                                                <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("gi_ID") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>                                    
                                                <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("gi_ID") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>                                                                                                           
                                                </ItemTemplate>                     
                                            </telerik:GridTemplateColumn>                              
                                            <telerik:GridBoundColumn  DataField="gi_title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Title" SortExpression="gi_title" UniqueName="gi_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>     
                                            <telerik:GridBoundColumn  DataField="gi_content" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Content" SortExpression="gi_content" UniqueName="gi_content" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="gi_icon" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Icon" SortExpression="gi_icon" UniqueName="gi_icon" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn  DataField="gi_seq" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Sequence" SortExpression="gi_seq" UniqueName="gi_seq" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn DataField="gi_mainID" HeaderText="Main Title" UniqueName="gi_mainID" HeaderStyle-Font-Bold="true">
                                            <ItemTemplate>  
                                                <%# getGeneralInfoTitleByID(Eval("gi_mainID").ToString()) %>
                                            </ItemTemplate>
                                            </telerik:GridTemplateColumn> 
                                            <telerik:GridBoundColumn  DataField="menuID" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                                HeaderText="Menu" SortExpression="ec_country" UniqueName="ec_country" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>   
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-6"><asp:HiddenField ID="hfid" runat="server" />
                                <asp:TextBox ID="txttitle" class="form-control"  placeholder="Enter Floor Name" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Content</label>
                                <div class="col-sm-6">
                                    <telerik:RadEditor RenderMode="Lightweight" runat="server" ID="txtContent" SkinID="DefaultSetOfTools" Width="90%">                                   
                                        <ImageManager ViewPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            UploadPaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            DeletePaths="~/Editor/images/UserDir/Marketing,~/Editor/images/UserDir/PublicRelations"
                                            EnableAsyncUpload="true"></ImageManager>
                                        <Tools>
                                            <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="JustifyLeft" />
                                            <telerik:EditorTool Name="JustifyCenter" />
                                            <telerik:EditorTool Name="JustifyRight" />
                                            <telerik:EditorTool Name="JustifyFull" />
                                        </telerik:EditorToolGroup>
                                           
                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="Bold" />
                                            <telerik:EditorTool Name="Underline" />
                                            <telerik:EditorTool Name="Italic" />
                                        </telerik:EditorToolGroup>

                                        <telerik:EditorToolGroup>
                                            <telerik:EditorTool Name="InsertTable"/>
                                        </telerik:EditorToolGroup>
                                        </Tools>
                                    </telerik:RadEditor>                               
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Icon</label>
                                <div class="col-sm-3">
                                <asp:FileUpload ID="fupForm" runat="server" /><asp:Label ID="lblicon" runat="server"></asp:Label>
                                </div>                                
                            </div>
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Sequence</label>
                                <div class="col-sm-6">
                                <asp:TextBox ID="txtSeq" class="form-control" TextMode="Number" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Main Title</label>
                                <div class="col-sm-6">
                                <asp:DropDownList ID="ddlmain" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Menu</label>
                                <div class="col-sm-6">
                                <asp:DropDownList ID="ddlmenu" runat="server" CssClass="form-control" ></asp:DropDownList>
                                </div>
                            </div>                          
                        </div>
                        <div class="box-footer">                                
                           <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </section>
</asp:Content>

