﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="BusinessMatching.aspx.cs" Inherits="Admin_BusinessMatching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
  <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdBM" />
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Account</h3>
                    <div style="float: right">
                        <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="lnkDownload" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>
                </div>
                <div class="box-body">
                    <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;">
                        <div class="col-md-5">
                            <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />
                        </div>
                        <div class="col-md-6">
                            <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>

                    </div>
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdBM" runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdBM_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdBM_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdBM_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="bm_ID" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                                        <Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1  %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-success btn-sm" CommandName="Editspp" CommandArgument='<%#Eval("bm_ID") %>' OnClick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i> Edit</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-warning btn-sm" OnClientClick='return confirm("Delete?")' CommandName="Delete" CommandArgument='<%#Eval("bm_ID") %>' OnClick="linkdel_Click"><i class="fa fa-trash-o"></i> Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="bm_vid" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="VisitorID" SortExpression="bm_vid" UniqueName="bm_vid" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>                                           
                                             <telerik:GridBoundColumn DataField="bm_eid" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="ExhibitorID" SortExpression="bm_eid" UniqueName="bm_eid" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>                                          
                                            <telerik:GridBoundColumn DataField="bm_date" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Date" SortExpression="bm_date" UniqueName="bm_date" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="bm_time" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Time Slot" SortExpression="bm_time" UniqueName="bm_time" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="bm_status" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Status" SortExpression="bm_status" UniqueName="bm_status" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="bm_ecomment" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Exhibitor's Comment" SortExpression="bm_ecomment" UniqueName="bm_ecomment" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="bm_reqtime" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Requested Time" SortExpression="bm_reqtime" UniqueName="bm_reqtime" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="bm_vcomment" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Visitor's Comment" SortExpression="bm_vcomment" UniqueName="bm_vcomment" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>                                           
                                            <telerik:GridBoundColumn DataField="bm_interestedProduct" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Visitor's Interest of Exhibitor Product" SortExpression="bm_interestedProduct" UniqueName="bm_interestedProduct" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>                                           

                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Visitor ID</label>
                                <div class="col-sm-6">
                                    <asp:HiddenField ID="hfid" runat="server" />
                                    <asp:TextBox ID="txtVisitorID" class="form-control" placeholder="Enter Visitor ID" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrVisitorID" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>                           
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Exhibitor ID</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtExhID" class="form-control" placeholder="Enter Exhibitor ID" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrExhID" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Date</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtDate" class="form-control" placeholder="Enter Date" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrDate" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Time Slot</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtTimeslot" class="form-control" placeholder="Enter Time Slot" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrTimeslot" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Status</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtStatus" class="form-control" placeholder="Enter Status" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrStatus" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Exhibitor's Comment</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtEComment" class="form-control" placeholder="Enter Exhibitor's Comment" runat="server"></asp:TextBox>
                                     <asp:Label ID="lblerrEComment" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Requested Time </label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtReqTime" class="form-control" placeholder="Enter Requested Time " runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrReqTime" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Visitor's Comment</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtVComment" class="form-control" placeholder="Enter Visitor's Comment" runat="server"></asp:TextBox>
                                </div>
                            </div>                                              
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Visitor's Interest of Exhibitor Product</label>
                                <div class="col-sm-6">
                                    <asp:TextBox ID="txtVEProduct" class="form-control" placeholder="Enter Visitor's Interest of Exhibitor Product" runat="server"></asp:TextBox>
                                    <asp:Label ID="lblerrVEProduct" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                                </div>
                            </div>
                           

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>



