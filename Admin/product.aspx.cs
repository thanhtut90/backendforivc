﻿using App.DAL;
using System;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_product : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblProduct";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdProduct.ExportSettings.IgnorePaging = true;
                grdProduct.ExportSettings.ExportOnlyData = true;
                grdProduct.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdProduct.ExportSettings.FileName = "Product";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dtMainPrd = daldata.getMainProduct();
            ddlMainProduct.DataSource = dtMainPrd;
            ddlMainProduct.DataTextField = "p_name";
            ddlMainProduct.DataValueField = "p_ID";
            ddlMainProduct.DataBind();
            ListItem lstMainPrd = new ListItem("Select Main Product", "");
            ddlMainProduct.Items.Insert(ddlMainProduct.Items.Count - ddlMainProduct.Items.Count, lstMainPrd);

            DataTable dtPrdCat = daldata.getProductCategoryMarketSeg();
            ddlProductCategory.DataSource = dtPrdCat;
            ddlProductCategory.DataTextField = "pc_name";
            ddlProductCategory.DataValueField = "pc_ID";
            ddlProductCategory.DataBind();
            ListItem lstPrdCat = new ListItem("Select Product Category", "");
            ddlProductCategory.Items.Insert(ddlProductCategory.Items.Count - ddlProductCategory.Items.Count, lstPrdCat);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                setInvisibleErrMsg();

                DataTable dt = new DataTable();
                dt.Columns.Add("p_ID");
                dt.Columns.Add("sororder");
                dt.Columns.Add("p_name");
                dt.Columns.Add("p_category");
                dt.Columns.Add("p_mainproductID");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");

                string message = string.Empty;
                if (hfid.Value != "")
                {
                    //Update
                    DataRow dr = dt.NewRow();
                    dr["p_ID"] = hfid.Value.ToString();
                    dr["sororder"] = daldata.replaceForSQL(txtSortOrder.Text.Trim());
                    dr["p_name"] = daldata.replaceForSQL(txtProductName.Text.Trim());
                    dr["p_category"] = ddlProductCategory.SelectedValue.ToString();
                    dr["p_mainproductID"] = ddlMainProduct.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;

                    int res = daldata.UpdateProduct(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdProduct_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["p_ID"] = "";
                    dr["sororder"] = daldata.replaceForSQL(txtSortOrder.Text.Trim());
                    dr["p_name"] = daldata.replaceForSQL(txtProductName.Text.Trim());
                    dr["p_category"] = ddlProductCategory.SelectedValue.ToString();
                    dr["p_mainproductID"] = ddlMainProduct.SelectedValue.ToString();
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    int res = daldata.SaveProduct(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdProduct_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtProductName.Text.Trim()) || string.IsNullOrWhiteSpace(txtProductName.Text.Trim()))
            {
                lblerrProductName.Visible = true;
                rtn = false;
            }
            if (string.IsNullOrEmpty(txtSortOrder.Text.Trim()) || string.IsNullOrWhiteSpace(txtSortOrder.Text.Trim()))
            {
                lblerrSortOrder.Visible = true;
                rtn = false;
            }
            //if (ddlMainProduct.SelectedItem.Value == "")
            //{
            //    lblerrMainProduct.Visible = true;
            //    rtn = false;
            //}
            //if (ddlProductCategory.SelectedItem.Value == "")
            //{
            //    lblerrProductCategory.Visible = true;
            //    rtn = false;
            //}
        }
        catch (Exception ex) { }
        return rtn;
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdProduct.ExportSettings.ExportOnlyData = true;
        this.grdProduct.ExportSettings.IgnorePaging = true;
        this.grdProduct.ExportSettings.OpenInNewWindow = true;
        this.grdProduct.MasterTableView.ExportToExcel();

        grdProduct.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdProduct.ExportSettings.FileName = "Product";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdProduct_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getProduct();
            grdProduct.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdProduct_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdProduct.CurrentPageIndex + 1;
    }
    protected void grdProduct_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getProductbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getProduct();
            }

            grdProduct.DataSource = dt;
            grdProduct.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdProduct_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdProduct.ExportSettings.ExportOnlyData = true;
            this.grdProduct.ExportSettings.IgnorePaging = true;
            this.grdProduct.ExportSettings.OpenInNewWindow = true;
            this.grdProduct.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getProductlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["p_ID"].ToString();
                    txtProductName.Text = dt.Rows[0]["p_name"].ToString();
                    txtSortOrder.Text = dt.Rows[0]["sororder"].ToString();
                    string mainprdID = dt.Rows[0]["p_mainproductID"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(mainprdID))
                        {
                            ListItem listItem = ddlMainProduct.Items.FindByValue(mainprdID);
                            if (listItem != null)
                            {
                                ddlMainProduct.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    string prdcatID = dt.Rows[0]["p_category"].ToString();
                    try
                    {
                        if (!String.IsNullOrEmpty(prdcatID))
                        {
                            ListItem listItem = ddlProductCategory.Items.FindByValue(prdcatID);
                            if (listItem != null)
                            {
                                ddlProductCategory.ClearSelection();
                                listItem.Selected = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteProductByID(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdProduct_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                noti.Visible = false;
                grdProduct_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtProductName.Text = "";
        txtSortOrder.Text = "";
        ddlMainProduct.SelectedIndex = 0;
        ddlProductCategory.SelectedIndex = 0;
        setInvisibleErrMsg();
    }
    private void setInvisibleErrMsg()
    {
        lblerrProductName.Visible = false;
        lblerrMainProduct.Visible = false;
        lblerrProductCategory.Visible = false;
    }
}