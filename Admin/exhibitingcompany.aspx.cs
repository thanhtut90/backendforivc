﻿using App.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Admin_exhibitingcompany : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string tablename = "tblExhibitingCompany";
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Session["id"] !=null)
            {
                BindControlData();

                grdExh.ExportSettings.IgnorePaging = true;
                grdExh.ExportSettings.ExportOnlyData = true;
                grdExh.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdExh.ExportSettings.FileName = "ExhibitingCompany";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            DataTable dthall = daldata.getHall();            
           
            DataTable dtProduct = daldata.getSubProduct();
            DataTable dtmarketseg = daldata.getProductCategoryMarketSeg();
            ListItem lst = new ListItem("Select","");

            ddlhall.DataSource = dthall;
            ddlhall.DataTextField = "l_name";
            ddlhall.DataValueField = "l_ID";
            ddlhall.DataBind();
            ddlhall.Items.Insert(ddlhall.Items.Count - ddlhall.Items.Count, lst);
            ddlboothno.Items.Insert(ddlboothno.Items.Count - ddlboothno.Items.Count, lst);

            chkproducts.DataSource = dtProduct;
            chkproducts.DataTextField = "p_name";
            chkproducts.DataValueField = "p_ID";
            chkproducts.DataBind();

            chkmarketseg.DataSource = dtmarketseg;
            chkmarketseg.DataTextField = "pc_name";
            chkmarketseg.DataValueField = "pc_ID";
            chkmarketseg.DataBind();
        }
        catch(Exception ex) { }
    }
    protected void btnSubmit_Click(object sender,EventArgs e)
    {
        try
        {
            if(Validation())
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("ec_ID");
                dt.Columns.Add("ec_name");
                dt.Columns.Add("ec_address");
                dt.Columns.Add("ec_country");
                dt.Columns.Add("ec_email");
                dt.Columns.Add("ec_contactno");
                dt.Columns.Add("ec_website");
                dt.Columns.Add("ec_brochure");
                dt.Columns.Add("ec_logo");
                dt.Columns.Add("ec_companyprofile");
                dt.Columns.Add("ec_companyType");
                dt.Columns.Add("ec_hall");
                dt.Columns.Add("ec_booth");
                dt.Columns.Add("contactpersonID");
                dt.Columns.Add("ec_product");
                dt.Columns.Add("lang");
                dt.Columns.Add("deleteFlag");
                dt.Columns.Add("UpdatedDate");
                dt.Columns.Add("ec_marketseg");
                dt.Columns.Add("ec_trends");
                dt.Columns.Add("clientID");
                string imagename = "";
                string filename = string.Empty;
                string extension = string.Empty;
                string path = Constant.filesavingurl;
                string message = string.Empty;
                if (fupLogo.HasFile == true)
                {
                    filename = fupLogo.FileName.Substring(0, fupLogo.FileName.LastIndexOf("."));
                    extension = System.IO.Path.GetExtension(fupLogo.FileName).ToString().ToLower();
                    imagename = UploadIcon(filename, extension, path, ref message);
                }

                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    dr["ec_ID"] = hfid.Value.ToString();
                    dr["ec_name"] = daldata.replaceForSQL(txtexhibitingcompany.Text.Trim());
                    dr["ec_address"] = daldata.replaceForSQL(txtaddress.Text.Trim());
                    dr["ec_country"] = daldata.replaceForSQL(txtcountry.Text.Trim());
                    dr["ec_email"] = daldata.replaceForSQL(txtemail.Text.Trim());
                    dr["ec_contactno"] = daldata.replaceForSQL(txttel.Text.Trim());
                    dr["ec_website"] = daldata.replaceForSQL(txtwebsite.Text.Trim());
                    dr["ec_brochure"] = daldata.replaceForSQL(txtbrochure.Text.Trim());
                    dr["ec_logo"] = imagename == "" ? lbllogo.Text : imagename;
                    dr["ec_companyprofile"] = daldata.replaceForSQL(txtcompanyprofile.Text.Trim());
                    dr["ec_companyType"] = daldata.replaceForSQL(txtcompanytype.Text.Trim());
                    dr["ec_hall"] = daldata.replaceForSQL(ddlhall.SelectedItem.Text);
                    dr["ec_booth"] = daldata.replaceForSQL(ddlboothno.SelectedItem.Text);
                    dr["contactpersonID"] = daldata.replaceForSQL(txtContactPerson.Text.Trim());                    
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    
                    string product = string.Empty;string marketseg = string.Empty;/*string trends = string.Empty;*/
                    for (int i = 0; i < chkproducts.Items.Count; i++)
                    {
                        if (chkproducts.Items[i].Selected == true)
                        {
                            product += chkproducts.Items[i].Text + ";";
                        }

                    }
                    product = product.TrimEnd(',');

                    for (int i = 0; i < chkmarketseg.Items.Count; i++)
                    {
                        if (chkmarketseg.Items[i].Selected == true)
                        {
                            marketseg += chkmarketseg.Items[i].Text + ";";
                        }

                    }
                    marketseg = marketseg.TrimEnd(';');
                   
                    dr["ec_product"] = product;
                    dr["ec_marketseg"] = marketseg;
                    dr["ec_trends"] = daldata.replaceForSQL(txttrends.Text.Trim());
                    dr["clientID"] = daldata.replaceForSQL(txtclientID.Text.Trim());
                    int res = daldata.UpdateExhibitingCompany(dr);
                    if(res ==1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdExh_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["ec_ID"] = "";
                    dr["ec_name"] = daldata.replaceForSQL(txtexhibitingcompany.Text.Trim());
                    dr["ec_address"] = daldata.replaceForSQL(txtaddress.Text.Trim());
                    dr["ec_country"] = daldata.replaceForSQL(txtcountry.Text.Trim());
                    dr["ec_email"] = daldata.replaceForSQL(txtemail.Text.Trim());
                    dr["ec_contactno"] = daldata.replaceForSQL(txttel.Text.Trim());
                    dr["ec_website"] = daldata.replaceForSQL(txtwebsite.Text.Trim());
                    dr["ec_brochure"] = daldata.replaceForSQL(txtbrochure.Text.Trim());
                    dr["ec_logo"] = imagename == "" ? lbllogo.Text : imagename;
                    dr["ec_companyprofile"] = daldata.replaceForSQL(txtcompanyprofile.Text.Trim());
                    dr["ec_companyType"] = daldata.replaceForSQL(txtcompanytype.Text.Trim());
                    dr["ec_hall"] = daldata.replaceForSQL(ddlhall.SelectedItem.Text);
                    dr["ec_booth"] = daldata.replaceForSQL(ddlboothno.SelectedItem.Text);
                    dr["contactpersonID"] = daldata.replaceForSQL(txtContactPerson.Text.Trim());
                    dr["lang"] = "1";
                    dr["deleteFlag"] = false;
                    dr["UpdatedDate"] = DateTime.Now;
                    string product = string.Empty; string marketseg = string.Empty;/*string trends = string.Empty;*/
                    for (int i = 0; i < chkproducts.Items.Count; i++)
                    {
                        if (chkproducts.Items[i].Selected == true)
                        {
                            product += chkproducts.Items[i].Text + ";";
                        }

                    }
                    product = product.TrimEnd(',');

                    for (int i = 0; i < chkmarketseg.Items.Count; i++)
                    {
                        if (chkmarketseg.Items[i].Selected == true)
                        {
                            marketseg += chkmarketseg.Items[i].Text + ";";
                        }

                    }
                    marketseg = marketseg.TrimEnd(';');

                    dr["ec_product"] = product;
                    dr["ec_marketseg"] = marketseg;
                    dr["ec_trends"] = daldata.replaceForSQL(txttrends.Text.Trim());
                    dr["clientID"] = daldata.replaceForSQL(txtclientID.Text.Trim());
                    int res = daldata.SaveExhibitingCompany(dr);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        grdExh_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
        }catch(Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if(string.IsNullOrEmpty(txtexhibitingcompany.Text.Trim()))
            {
                rtn = false;
            }
           
        }
        catch(Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender,EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdExh.ExportSettings.ExportOnlyData = true;
        this.grdExh.ExportSettings.IgnorePaging = true;
        this.grdExh.ExportSettings.OpenInNewWindow = true;
        this.grdExh.MasterTableView.ExportToExcel();      
        
        grdExh.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdExh.ExportSettings.FileName = "ExhibitingCompany";
    }
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        divadd.Visible = true;
        divlist.Visible = false;
        divsearch.Visible = false;

        clearControls();
    }
    protected void grdExh_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getExhibitingCompanies();
            grdExh.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdExh_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdExh.CurrentPageIndex + 1;
    }
    protected void grdExh_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getExhibitingCompaniesbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.getExhibitingCompanies();
            }


            grdExh.DataSource = dt;
            grdExh.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdExh_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;            
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdExh.ExportSettings.ExportOnlyData = true;
            this.grdExh.ExportSettings.IgnorePaging = true;
            this.grdExh.ExportSettings.OpenInNewWindow = true;
            this.grdExh.MasterTableView.ExportToExcel();
        }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getExhibitingCompanyByID(id);
                if (dt.Rows.Count > 0)
                {
                    hfid.Value = dt.Rows[0]["ec_ID"].ToString();
                    txtclientID.Text = dt.Rows[0]["clientID"].ToString();
                    txtexhibitingcompany.Text = dt.Rows[0]["ec_name"].ToString();
                    txtaddress.Text = dt.Rows[0]["ec_address"].ToString();
                    txtcountry.Text = dt.Rows[0]["ec_country"].ToString();
                    txtemail.Text = dt.Rows[0]["ec_email"].ToString();
                    txttel.Text = dt.Rows[0]["ec_contactno"].ToString();
                    txtwebsite.Text = dt.Rows[0]["ec_website"].ToString();
                    txtbrochure.Text = dt.Rows[0]["ec_brochure"].ToString();
                    lbllogo.Text = dt.Rows[0]["ec_logo"].ToString();
                    txtcompanyprofile.Text = dt.Rows[0]["ec_companyprofile"].ToString();
                    txtcompanytype.Text = dt.Rows[0]["ec_companyType"].ToString();
                    ddlhall.SelectedValue = dt.Rows[0]["ec_hall"].ToString();
                    BindBooth(dt.Rows[0]["ec_hall"].ToString());
                    ddlboothno.SelectedValue = dt.Rows[0]["ec_booth"].ToString();
                    txtContactPerson.Text = dt.Rows[0]["contactpersonID"].ToString();
                    txttrends.Text = dt.Rows[0]["ec_trends"].ToString();
                    if (!string.IsNullOrEmpty(dt.Rows[0]["ec_product"].ToString()))
                    {
                        string selectedproduct = dt.Rows[0]["ec_product"].ToString();
                        string[] arrselectedproduct = selectedproduct.Split(';');
                        if (arrselectedproduct.Count() > 0)
                        {
                            for (int i = 0; i < arrselectedproduct.Count(); i++)
                            {
                                for (int j = 0; j < chkproducts.Items.Count; j++)
                                {
                                    if (chkproducts.Items[j].Text == arrselectedproduct[i].ToString())
                                    {
                                        chkproducts.Items[j].Selected = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            //chkproducts.Items[0].Selected = true;
                        }
                    }
                    if (!string.IsNullOrEmpty(dt.Rows[0]["ec_marketseg"].ToString()))
                    {
                        string selectedec_marketseg = dt.Rows[0]["ec_marketseg"].ToString();
                        string[] arrselectedec_marketseg = selectedec_marketseg.Split(';');
                        if (arrselectedec_marketseg.Count() > 0)
                        {
                            for (int i = 0; i < arrselectedec_marketseg.Count(); i++)
                            {
                                for (int j = 0; j < chkmarketseg.Items.Count; j++)
                                {
                                    if (chkmarketseg.Items[j].Text == arrselectedec_marketseg[i].ToString())
                                    {
                                        chkmarketseg.Items[j].Selected = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex) { }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Delete")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    int res = daldata.DeleteExhibitingCompany(ID);
                    if (res == 1)
                    {
                        daldata.UpdateDataVersion(tablename);
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Success !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdExh_NeedDSource("");
            }
        }
        catch(Exception ex) { }
    }    
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if(!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdExh_NeedDSource(txtKeyword.Text.Trim());
            }
        }catch(Exception ex) { }
    }
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtexhibitingcompany.Text = "";
        txtaddress.Text = "";
        txtcountry.Text = "";
        txtemail.Text = "";
        txttel.Text = "";
        txtwebsite.Text = "";
        txtbrochure.Text = "";
        lbllogo.Text = "";
        txtcompanyprofile.Text = "";
        txtcompanytype.Text = "";
        ddlhall.SelectedValue = "";
        ddlboothno.SelectedValue = "";
        txtContactPerson.Text = "";
        chkproducts.ClearSelection();
        chkmarketseg.ClearSelection();
        txttrends.Text = "";
    }
    private string UploadIcon(string filename, string extension, string path, ref string message)
    {
        string imagefilename = string.Empty; string messages = string.Empty;
        try
        {
            if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
            {
                messages = "Invalid Image Type";
            }

            string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath((path) + fname);
            fupLogo.SaveAs(savePath);
            imagefilename = fname;
        }
        catch (Exception ex) { }
        return imagefilename;
    }
    protected void ddlhall_SelectedIndexChanged(object sender,EventArgs e)
    {
        try
        {
            if(ddlhall.SelectedValue != "")
            {
                string hallid = ddlhall.SelectedValue.ToString();
                BindBooth(hallid);
            }
        }catch(Exception ex) { }
    }

    private void BindBooth(string hallid)
    {
        try
        {
            
            DataTable dtBooth = daldata.getBoothByHallID(hallid);
            ddlboothno.DataSource = dtBooth;
            ddlboothno.DataTextField = "vb_name";
            ddlboothno.DataValueField = "vb_ID";
            ddlboothno.DataBind();
            ListItem lst = new ListItem("Select", "");
            ddlboothno.Items.Insert(ddlboothno.Items.Count - ddlboothno.Items.Count, lst);
        }
        catch(Exception ex) { }
    }
}