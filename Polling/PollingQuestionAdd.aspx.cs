﻿using System;
using App.DAL;
using App.Common;

public partial class PollingQuestionAdd : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    PollingQandA Pobj = new PollingQandA();
    string sessionID;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sessionID"] != null)
            {
                sessionID = Request.QueryString["sessionID"].ToString();
                if(Request.QueryString["questionID"] != null)
                {
                    string qid = Request.QueryString["questionID"].ToString();
                    var pqa = dalf.SelectPollingQAbyQIDSID(qid, sessionID);
                    if(pqa != null)
                    {
                        txt_question.Text = pqa.question;
                        ddl_answer.SelectedValue = pqa.noOfAns.ToString();
                        txtAns1.Text = pqa.answer1;
                        txtAns2.Text = pqa.answer2;
                        txtAns3.Text = pqa.answer3;
                        txtAns4.Text = pqa.answer4;
                        txtAns5.Text = pqa.answer5;
                        txtAns6.Text = pqa.answer6;
                        txtAns7.Text = pqa.answer7;
                        txtAns8.Text = pqa.answer8;
                        txtAns9.Text = pqa.answer9;
                        VisibleInvisible(pqa.noOfAns.Value);
                        btnAdd.Text = "Update";
                    }
                }
            }
        }
    }
       
    protected void ddl_answer_SelectedIndexChanged(object sender, EventArgs e)
    {
        int noOfAnswer = Convert.ToInt32(ddl_answer.SelectedValue);

        VisibleInvisible(noOfAnswer);
    }

    private void VisibleInvisible(int noOfAnswer)
    {
        if (noOfAnswer == 1)
        {
            txtAns2.Text = "";
            txtAns3.Text = "";
            txtAns4.Text = "";
            txtAns5.Text = "";
            txtAns6.Text = "";
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = false;
            answerrow3.Visible = false;
            answerrow4.Visible = false;
            answerrow5.Visible = false;
            answerrow6.Visible = false;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 2)
        {
            txtAns3.Text = "";
            txtAns4.Text = "";
            txtAns5.Text = "";
            txtAns6.Text = "";
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = false;
            answerrow4.Visible = false;
            answerrow5.Visible = false;
            answerrow6.Visible = false;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 3)
        {
            txtAns4.Text = "";
            txtAns5.Text = "";
            txtAns6.Text = "";
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = false;
            answerrow5.Visible = false;
            answerrow6.Visible = false;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 4)
        {
            txtAns5.Text = "";
            txtAns6.Text = "";
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = false;
            answerrow6.Visible = false;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 5)
        {
            txtAns6.Text = "";
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = true;
            answerrow6.Visible = false;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 6)
        {
            txtAns7.Text = "";
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = true;
            answerrow6.Visible = true;
            answerrow7.Visible = false;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 7)
        {
            txtAns8.Text = "";
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = true;
            answerrow6.Visible = true;
            answerrow7.Visible = true;
            answerrow8.Visible = false;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 8)
        {
            txtAns9.Text = "";

            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = true;
            answerrow6.Visible = true;
            answerrow7.Visible = true;
            answerrow8.Visible = true;
            answerrow9.Visible = false;
        }

        if (noOfAnswer == 9)
        {
            answerrow1.Visible = true;
            answerrow2.Visible = true;
            answerrow3.Visible = true;
            answerrow4.Visible = true;
            answerrow5.Visible = true;
            answerrow6.Visible = true;
            answerrow7.Visible = true;
            answerrow8.Visible = true;
            answerrow9.Visible = true;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            string answer1 = FilterSpecial(txtAns1.Text.Trim());
            string answer2 = FilterSpecial(txtAns2.Text.Trim());
            string answer3 = FilterSpecial(txtAns3.Text.Trim());
            string answer4 = FilterSpecial(txtAns4.Text.Trim());
            string answer5 = FilterSpecial(txtAns5.Text.Trim());
            string answer6 = FilterSpecial(txtAns6.Text.Trim());
            string answer7 = FilterSpecial(txtAns7.Text.Trim());
            string answer8 = FilterSpecial(txtAns8.Text.Trim());
            string answer9 = FilterSpecial(txtAns9.Text.Trim());
            if (Request.QueryString["sessionID"] != null)
            {
                sessionID = Request.QueryString["sessionID"].ToString();
                if(Request.QueryString["questionID"] != null)
                {
                    #region Update
                    Pobj.question = FilterSpecial(txt_question.Text.Trim());
                    Pobj.sessionID = sessionID;
                    Pobj.noOfAns = int.Parse(ddl_answer.SelectedValue.ToString());
                    Pobj.answer1 = answer1;
                    Pobj.answer2 = answer2;
                    Pobj.answer3 = answer3;
                    Pobj.answer4 = answer4;
                    Pobj.answer5 = answer5;
                    Pobj.answer6 = answer6;
                    Pobj.answer7 = answer7;
                    Pobj.answer8 = answer8;
                    Pobj.answer9 = answer9;
                    Pobj.questionID = Request.QueryString["questionID"].ToString();
                    dalf.UpdatePollingQA(Pobj);
                    #endregion
                }
                else
                {
                    #region Insert
                    GenKey gk = new GenKey();
                    string fkey = string.Empty; string fid = string.Empty;
                    fkey = gk.getKey("PQA");
                    fid = "PQA" + fkey;
                    Pobj.questionID = fid;
                    Pobj.question = FilterSpecial(txt_question.Text.Trim());
                    Pobj.sessionID = sessionID;
                    Pobj.noOfAns = int.Parse(ddl_answer.SelectedValue.ToString());
                    Pobj.pollingStatus = 0;
                    Pobj.answer1 = answer1;
                    Pobj.answer2 = answer2;
                    Pobj.answer3 = answer3;
                    Pobj.answer4 = answer4;
                    Pobj.answer5 = answer5;
                    Pobj.answer6 = answer6;
                    Pobj.answer7 = answer7;
                    Pobj.answer8 = answer8;
                    Pobj.answer9 = answer9;
                    dalf.SavePollingQA(Pobj);

                    gk.SaveKey("PQA", int.Parse(fkey));
                    #endregion
                }
                Response.Redirect("PollingQuestionFacilitator.aspx?SessionID=" + sessionID, false);
            }
        }
        catch(Exception ex)
        { throw ex; }
    }

    public string FilterSpecial(string str)
    {
        if (str == "") 
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");

            return str;
        }
    }
}