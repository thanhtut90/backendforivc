﻿using System;
using App.DAL;
using System.Data;

public partial class Polling : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data daldata = new DAL_Data();
    protected string sessionid = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {          
            if(Request.QueryString["sessionID"] != null )
            {
                sessionid = Request.QueryString["sessionID"].ToString();
                DataTable dt = daldata.getConferenceByID(sessionid);
                if(dt.Rows.Count >0)
                {

                    lblsession.Text = dt.Rows[0]["c_title"].ToString();
                    lblDate.Text = dt.Rows[0]["c_date"].ToString();                   
                    lbltime.Text = dt.Rows[0]["c_stime"].ToString() + " - " + dt.Rows[0]["c_etime"].ToString();
                }            
                
            }
        }
    }
}