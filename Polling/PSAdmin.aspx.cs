﻿using System;
using App.DAL;
using System.Text;
using System.Data;

public partial class PSAdmin : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    string sessionID;
    protected string htmlstr;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (Request.QueryString["SessionID"] != null)
            {
                sessionID = Request.QueryString["SessionID"].ToString();
            }
            StringBuilder str = new StringBuilder();
            lbl_sessionTitle.Text = daldata.getConferenceByID(sessionID).Rows[0]["c_title"].ToString();
            DataTable sessionlst = daldata.selectPollingQABySID(sessionID);
            if(sessionlst.Rows.Count>0)
            {
                int i = 0;
                foreach (DataRow dr in sessionlst.Rows)
                {
                    i = i + 1;
                    str.Append("<tr class='color4'>");
                    str.Append("<td align='center'>" + i + "</td>");
                    str.Append("<td><a href='PSCheck.aspx?questionID=" + dr["questionID"].ToString() + "&sessionID=" + sessionID + "'>" + dr["question"].ToString() + "</a></td>");
                    str.Append("</tr>");
                }
                htmlstr = str.ToString();
            }
            
        }
    }  
}