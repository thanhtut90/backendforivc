﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PollingQuestionFacilitator.aspx.cs" Inherits="Polling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><asp:Label ID="lbl_sessionTitle" runat="server"></asp:Label></h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                <div class="form-group">                      
                    <div class="col-sm-8">
                        <a href="PollingQuestionAdd.aspx?SessionID=<%=sessionID %>" style="float:right" class="btn btn-info">Add</a>
                    </div>   
                </div>   
                <div class="form-group">                    
                    <div class="col-sm-8">
                     <table border="0" id="tabuser" class="table"">
                        <tr bgcolor="#336666" style="color:white;font-weight:bold">
                          <td width="6%" align="center">S/N</td>
                          <td width="73%">Question </td>
                          <td colspan="2" align="center">Edit / Delete</td>
                        </tr>
                         <%=htmlstr %>
                    </table>
                    </div>
                </div>
                <div class="box-footer">                                
                    <a href="PollingFacilitator.aspx?sessionID=<%=sessionID %>" class="btn btn-default">Back</a>             
                </div>
              </div>
          </div> 

</asp:Content>
