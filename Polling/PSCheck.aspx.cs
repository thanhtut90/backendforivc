﻿using System;
using System.Linq;
using System.Data;
using System.Text;
using App.DAL;
public partial class PSCheck : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    public string questionAns;
    string sessionID;
    string questionID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sessionID"] != null && Request.QueryString["questionID"] != null)
            {
                sessionID =Request.QueryString["sessionID"].ToString();
                questionID = Request.QueryString["questionID"].ToString();

                var PQA = dalf.SelectPollingQAbyQIDSID(questionID, sessionID);

                int noOfAns = PQA.noOfAns.Value;
                questionAns += " <span class='title' style='font-size:18px;color:#303030;font-weight:bold;line-height:35px;text-align:justify'>" + PQA.question + "</span>";

                var PR = dalf.SelectPollingResutBySIDQID(sessionID, questionID);
                int totalcount = PR.Count;
                questionAns += "<table cellpadding='0px' cellspacing='0px'>";
                for (int i = 1; i <= noOfAns; i++)
                {
                    string option = "answer" + i;
                    if (option == "answer1")
                    {
                        option = PQA.answer1;
                    }
                    else if (option == "answer2")
                    {
                        option = PQA.answer2;
                    }
                    else if (option == "answer3")
                    {
                        option = PQA.answer3;
                    }
                    else if (option == "answer4")
                    {
                        option = PQA.answer4;
                    }
                    else if (option == "answer5")
                    {
                        option = PQA.answer5;
                    }
                    else if (option == "answer6")
                    {
                        option = PQA.answer6;
                    }
                    else if (option == "answer7")
                    {
                        option = PQA.answer7;
                    }
                    else if (option == "answer8")
                    {
                        option = PQA.answer8;
                    }
                    else if (option == "answer9")
                    {
                        option = PQA.answer9;
                    }
                    StringBuilder sqlOptionCount = new StringBuilder();
                    sqlOptionCount.Append("Select * from PollingResults where voteSession=" + sessionID + " and voteQuestion=" + questionID + " and voteAnswer = " + i + ";");
                    int count = dalf.PRAnsCount(sessionID, questionID, i);

                    double percent;
                    if (count > 0)
                    {
                        percent = Math.Round((double)count / (double)totalcount * 100);

                    }
                    else
                    {
                        percent = 0;
                    }

                    questionAns += "<tr><td valign='top' width='2px'><span  style='font-size:18px;color:#303030;'>" + i + ". " + "</span></td><td><span  style='font-size:18px;color:#303030;'>" + option + "</span><br />";
                    //double Wtotal;
                    //if (percent == 0)
                    //{
                    //    Wtotal = 1;
                    //    questionAns += "<br/><div class='barpad'><div style='font-size:18px;color:#303030;width:" + percent + "%;line-height:20px;color:#303030 ;text-align:center;'>" + percent + "%</div></div>";
                    //}
                    //else
                    //{
                    //    Wtotal = percent;
                    //    questionAns += "<br/><div class='barpad'><div style='font-size:18px; width:" + (percent * 5) + "px;height:20px;line-height:20px;background-color:#BA2221;color:white;text-align:center;'>" + percent + "%</div></div>";
                    //}

                }
                questionAns += "</td></tr></table>";
            }            
            
        }
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        sessionID = Request.QueryString["sessionID"].ToString();
        Response.Redirect("PSAdmin.aspx?SessionID=" + sessionID );
    }
    
}