﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/MasterPages/Site.master" CodeFile="Polling.aspx.cs" Inherits="Polling" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">     
     <script type="text/javascript">
         function RowDataBound(sender, e)
         {
            var colorder = Convert.ToInt32("1");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var rowid = e.Row.Cells[colorder].Text;
                e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a href='FrontEndForm_Add.aspx' class='btn btn-primary'></a>"));
            }
        }
    </script>
    
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdDynamic" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                        <asp:HiddenField ID="hdPID" runat="server" />
                        <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                        <h3 class="box-title"><asp:Label ID="lblformname" runat="server"></asp:Label></h3>
                </div>                                       
                <div class="box-body">                           
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <%-- <asp:GridView ID="grdDynamic" PageSize="30" GridLines="Horizontal" Font-Size="Small"
                                AutoGenerateColumns="False" runat="server" class="table" Width="100%"  onrowdatabound="GridView_RowDataBound" 
                                AllowPaging="True" onpageindexchanging="grdDynamic_PageIndexChanging" HeaderStyle-HorizontalAlign="Left" CellPadding="4" BackColor="White" BorderColor="#336666" BorderWidth="1px" BorderStyle="None">
                                <EmptyDataTemplate>
                                    <label>There is no data to display.</label>
                                </EmptyDataTemplate>
                                     <FooterStyle BackColor="White" ForeColor="#333333" />

                                     <HeaderStyle HorizontalAlign="Left" BackColor="#336666" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                     <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                     <RowStyle BackColor="White" ForeColor="#333333" />
                                     <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                     <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                     <SortedAscendingHeaderStyle BackColor="#487575" />
                                     <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                     <SortedDescendingHeaderStyle BackColor="#275353" />
                                </asp:GridView>--%>
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdPolling" runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdPolling_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdPolling_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="c_ID" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                                        <Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1  %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                   <a href="PSAdmin.aspx?sessionID=<%# Eval("c_ID") %>" class="btn btn-success btn-sm">View Q&A</a> |
                                                   <a href="PollingFacilitator.aspx?sessionID=<%# Eval("c_ID") %>" class="btn btn-info btn-sm">Polling Facilitator</a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="c_title" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Tile" SortExpression="c_title" UniqueName="c_title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="c_date" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Date" SortExpression="c_date" UniqueName="c_date" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="c_stime" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Start Time" SortExpression="c_stime" UniqueName="c_starttime" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="c_etime" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="End Time" SortExpression="c_etime" UniqueName="c_etime" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>

                                            <telerik:GridTemplateColumn DataField="c_ID" HeaderText="QRCode" UniqueName="QRCode" 
                                                HeaderStyle-Width="25%" HeaderStyle-Font-Bold="true" Exportable="true">
                                                <ItemTemplate>
                                                    <a target="_blank" href='<%# Constant.URL + "QRImg/" + Eval("c_ID").ToString() + ".Png" %>'><%# getQRCode(Eval("c_ID").ToString())%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>   
                        </div> 
                    </div>
                </div>
          </div>
        </div>
    </section>
    
 </asp:Content>
