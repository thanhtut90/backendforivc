﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PSCheck.aspx.cs" Inherits="PSCheck" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
  
 <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Polling Question and Answers</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                    <div class="form-group">                      
                      <div class="col-sm-12">
                           <%=questionAns %>
                      </div>
                    </div>
              </div>              
              <div class="box-footer">                                
                <asp:Button ID="btnBack" runat="server" onclick="btnBack_Click" CssClass="btn btn-default"  Text="Back" />
              </div>              
            </div>
           </div>
</asp:Content>
