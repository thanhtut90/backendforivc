﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PollingSpeakerPanel.aspx.cs" Inherits="Polling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
  <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><asp:Label ID="lbl_sessionTitle" runat="server"></asp:Label></h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                    <div class="form-group">                      
                      <div class="col-sm-12">
                          <table border="0" id="tabuser" class="table" style="width:98%">
                            <tr bgcolor="#336666" style="color:white;font-weight:bold">
                              <td width="5%" align="center">S/N</td>
                              <td width="70%">Question</td>
                              <td width="10%">Polling Status</td>
                              <td width="25%">Edit/Delete</td>
                            </tr>
                            <%=htmlstr %>  
                          </table>
                      </div>
                    </div>
              </div>              
              <div class="box-footer">                                                
                  <a href="PollingFacilitator.aspx?id=BP10014&sessionID=<%=rowid %>" class="btn btn-default">Back</a>
              </div>              
            </div>
           </div> 
    
    
   
</asp:Content>
