﻿using System;
using App.DAL;
using System.Text;
using System.Data;

public partial class Polling : System.Web.UI.Page
{

    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data daldata = new DAL_Data();
    string sessionID;
    string speakerID;
    protected string htmlstr;
    protected string rowid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (Request.QueryString["SessionID"] == null)
                {
                    Response.Redirect("~/Polling/Polling.aspx");
                }
                else
                {
                    StringBuilder str = new StringBuilder();
                    sessionID = Request.QueryString["SessionID"].ToString();
                    rowid = sessionID;
                    DataTable dt = daldata.selectPollingQABySID(sessionID);
                    if (dt.Rows.Count > 0)
                    {
                        lbl_sessionTitle.Text = daldata.getConferenceByID(sessionID).Rows[0]["c_title"].ToString();
                        int i = 0;
                        foreach(DataRow dr in dt.Rows)
                        {
                            i++;
                            str.Append("<tr class='color4'>");
                            str.Append("<td align='center'>" + i + "</td>");
                            str.Append("<td>" + dr["question"] + "</td>");
                            str.Append("<td>");
                            string pollingStatus = dr["pollingStatus"].ToString();
                            if (pollingStatus == "0")
                            {
                                str.Append("New");
                            }
                            else if (pollingStatus == "1")
                            {
                                str.Append("Ongoing");
                            }
                            else if (pollingStatus == "2")
                            {
                                str.Append("Closed");
                            }
                            else
                            {
                                str.Append("");
                            }
                            str.Append("</td>");
                            str.Append("<td>");


                            DataTable pslist = daldata.selectPollingQABySIDPS1(sessionID);
                            if (pslist.Rows.Count > 0)
                            {
                                if (dr["questionID"].ToString() == pslist.Rows[0]["questionID"].ToString())
                                {
                                    str.Append("<a href='PollingResult.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "'>Result  |</a>");
                                    str.Append("<a href='PollingScreenUpdate.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "&UPType=2'>   Close</a>");
                                }
                                else
                                {
                                    str.Append("<a href='PollingResult.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "'>Result</a>");
                                }
                            }
                            else
                            {
                                if (pollingStatus == "2")
                                {

                                    str.Append("<a href='PollingScreenUpdate.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "&UPType=0'>Restart   |</a>");
                                    str.Append("<a href='PollingResult.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "'>   Result</a>");
                                }
                                else
                                {
                                    str.Append("<a href='PollingScreenUpdate.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "&UPType=1'>Start   |</a>");
                                    str.Append("<a href='PollingResult.aspx?questionID=" + dr["questionID"] + "&sessionID=" + sessionID + "'>   Result</a>");
                                }

                            }
                            str.Append("</td>");
                            str.Append("</tr>");
                        }
                        htmlstr = str.ToString();
                    }
                    
                }
            }
            catch(Exception ex) { }            
        }
    }    
}