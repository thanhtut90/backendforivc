﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using App.DAL;
using App.Common;
using App.Object;
using System.IO;

public partial class Polling : System.Web.UI.Page
{
    //DAL_FrontEnd dalfp = new DAL_FrontEnd();
    //DAL_Parent dalp = new DAL_Parent();
    //DAL_Child dalc = new DAL_Child();
    //ChildModel cmodel = new ChildModel();
    //ParentModel pmodel = new ParentModel();
    //BackEnd_Parent Pobj = new BackEnd_Parent();
    //BackEnd_Child Cobj = new BackEnd_Child();
    //List<BackEnd_Child> ShowHeaderList = new List<BackEnd_Child>();

    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //string pid = "BP10014";
            //hdPID.Value = pid.ToString();
            //ShowHeader(pid);
            //BindTableForGrid(pid);
            BindData();
            lblformname.Text = "Polling Sessions";
        }
    }

    private void BindData()
    {
        try
        {
            DataTable dt = daldata.getConferenceForPolling();
        }
        catch(Exception ex) { }
    }
    protected void grdPolling_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();           
            dt = daldata.getConferenceForPolling();            
            grdPolling.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdPolling_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdPolling.CurrentPageIndex + 1;
    }
    protected void grdPolling_NeedDSource()
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.getConferenceForPolling();
            grdPolling.DataSource = dt;
            grdPolling.DataBind();
        }
        catch (Exception ex) { }
    }






















    //#region ShowHeader
    //private void ShowHeader(string pid)
    //{
    //    try
    //    {
    //        ShowHeaderList = dalc.SelectByPId(pid);
    //        hdRowCount.Text = (ShowHeaderList.Count() + 1).ToString();

    //        lblformname.Text = "Polling Sessions";
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //#endregion

    //#region DynamicGridHeader
    //private void DynamicGridHeader()
    //{
    //    grdDynamic.Columns.Clear();

    //    BoundField bfield1 = new BoundField();
    //    bfield1.HeaderText = "No";
    //    bfield1.DataField = "No";
    //    bfield1.HeaderStyle.Width = new Unit("5%");
    //    grdDynamic.Columns.Add(bfield1);
    //    BoundField bfield2 = new BoundField();
    //    bfield2.HeaderStyle.Width = new Unit("15%");
    //    bfield2.HeaderText = "Action";
    //    bfield2.DataField = "Action";
    //    grdDynamic.Columns.Add(bfield2);
    //    foreach (var item in ShowHeaderList)
    //    {
    //        BoundField bfield = new BoundField();
    //        bfield.HeaderText = item.c_label;
    //        bfield.DataField = item.c_label;
    //        grdDynamic.Columns.Add(bfield);
    //    }


    //}
    //#endregion

    //#region BindTableForGrid
    //private DataTable BindTableForGrid(string pid)
    //{
    //    DataTable dt = new DataTable();
    //    try
    //    {
    //        int no = 1;

    //        DataRow dr;
    //        if (ShowHeaderList.Count > 0)
    //        {
    //            #region close a while
    //            int count = ShowHeaderList.Count();
    //            dt.Columns.Add("No", typeof(string));
    //            dt.Columns.Add("Action", typeof(string));

    //            foreach (var hd in ShowHeaderList)
    //            {
    //                dt.Columns.Add(hd.c_label, typeof(string));
    //            }
                
    //            //var lst = dalfp.selectQASessions();
    //            var lstresult = dalfp.SelectByPId(pid);
    //            var lstresult1 = dalfp.SelectDistinctRowID(lstresult);

    //            if (lstresult1.Count > 0)
    //            {
    //                foreach (var en in lstresult1)
    //                {
    //                    var lstresult2 = dalfp.SelectByParRowID(lstresult, pid, en.f_c_rowid);
    //                    if (lstresult2.Count > 0)
    //                    {
    //                        int fieldCount = 0;
    //                        if (ShowHeaderList.Count > 0)
    //                        {
    //                            fieldCount = ShowHeaderList.Count();
    //                        }

    //                        #region Real Data into DataTable
    //                        if (fieldCount > 0)
    //                        {
    //                            var lstRealResult = dalfp.SelectByFieldCount(lstresult2, fieldCount);

    //                            if (lstRealResult.Count > 0)
    //                            {
    //                                string rowid = string.Empty;
    //                                foreach (var oneRow in lstRealResult)
    //                                {
    //                                    if (oneRow.Count > 0)
    //                                    {
    //                                        dr = dt.NewRow();
    //                                        dr["No"] = no;
    //                                        foreach (var chl in oneRow)
    //                                        {
    //                                            rowid = chl.f_c_rowid;
    //                                            if (chl.f_inputtype == 13)
    //                                            {

    //                                                if (!string.IsNullOrEmpty(chl.f_content))
    //                                                {
    //                                                    if (chl.f_content.Contains("jbg") || chl.f_content.Contains("png") || chl.f_content.Contains("jpeg"))
    //                                                    {
    //                                                        string img = "<img src='../Images/FrontEnd_Photos/" + chl.f_content + "' width='150' height='150'/>";
    //                                                        img = img.Replace("'", "\"");

    //                                                        dr[chl.f_label] = img;
    //                                                    }
    //                                                    else
    //                                                    {
    //                                                        dr[chl.f_label] = chl.f_content;
    //                                                    }
    //                                                }
    //                                                else
    //                                                {
    //                                                    dr[chl.f_label] = chl.f_content;
    //                                                }


    //                                            }
    //                                            else if (chl.f_inputtype == 2)
    //                                            {
    //                                                dr[chl.f_label] = "xxxxxx";
    //                                            }
    //                                            else if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
    //                                            {
    //                                                if (!string.IsNullOrEmpty(chl.f_content))
    //                                                {
    //                                                    string fid = chl.f_content;
    //                                                    string fcontent = dalfp.selectContentByID(fid);
    //                                                    dr[chl.f_label] = fcontent;
    //                                                }
    //                                                else
    //                                                {
    //                                                    dr[chl.f_label] = "";
    //                                                }
    //                                            }
    //                                            else
    //                                            {
    //                                                dr[chl.f_label] = chl.f_content;
    //                                            }


    //                                        }
    //                                        no++;
    //                                        dr["Action"] = rowid;
    //                                        dt.Rows.Add(dr);
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        #endregion
    //                    }
    //                }
    //            }
    //            //if(pid == 10020)
    //            //{
    //            //    dalfp.RemoveOutdatedResult(dt);
    //            //}

    //            #endregion
                
    //            grdDynamic.DataSource = null;
    //            DynamicGridHeader();
    //            grdDynamic.DataSource = dt;
    //            grdDynamic.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //    return dt;
    //}
    //#endregion

    //#region GridView_RowDataBound
    //protected void GridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    //{
    //    //***hdPID.Value=BackendParentID

    //    int colorder = Convert.ToInt32("1");
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        string rowid = e.Row.Cells[colorder].Text;

    //        e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a runat='server'"
    //            + "href='PSAdmin.aspx?id=" + hdPID.Value + "&&rid=" + rowid
    //            + "'>View Q & A</a>&nbsp;&nbsp;|"));
    //        e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a runat='server'"
    //            + "href='PollingFacilitator.aspx?id=" + hdPID.Value + "&&rid=" + rowid
    //            + "'>Polling Facilitator</a>&nbsp;&nbsp;"));


    //    }
    //}


    //#endregion

    //#region grdDynamic_PageIndexChanging
    //protected void grdDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    string pid = string.Empty;
    //    if (Request.QueryString["id"] != null)
    //    {
    //        var id = Request.QueryString["id"].ToString();
    //        pid = id;
    //    }
    //    grdDynamic.PageIndex = e.NewPageIndex;
    //    BindTableForGrid(pid);
    //    grdDynamic.DataBind();
    //}
    //#endregion


    protected void lnkpsadmin_click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        Session["SessionID"] = id;
        Response.Redirect("PSAdmin.aspx?sessionID=" + id);
    }

    protected void lnkpfas_click(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        Session["SessionID"] = id;
        Response.Redirect("PollingFacilitator.aspx?sessionID=" + id);
    }
    protected void lnkpol_click(object sender, EventArgs e)
    {
        Session["SessionID"] = "";
        int id = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        Session["SessionID"] = id;
        Response.Redirect("voteadmin.aspx?SessionID=" + id);
    }

    protected void lnkpolling_click(object sender, EventArgs e)
    {
        Session["SessionID"] = "";
        int id = Convert.ToInt32(((LinkButton)sender).CommandArgument);
        Session["SessionID"] = id;
        Response.Redirect("Polling.aspx");
    }

    

    //protected void lnkpspkr_click(object sender,EventArgs e)
    //{
    //    if (Session["ShowID"].ToString() != "" && Session["SessionID"] != null)
    //    {
    //        string ShowID = Session["ShowID"].ToString();
    //        int id = int.Parse(Session["SessionID"].ToString());
    //        Response.Redirect("~/Polling/PollingSpeaker.aspx?sessionID=" + id+"&ShowID="+ShowID);
    //    }
    //    else
    //    {
    //        Response.Redirect("../Admin/Timeout.aspx");
    //    }

    //}
    protected void lnkpspkrpan_click(object sender, EventArgs e)
    {
        if (Session["SessionID"] != null)
        {
            int id = int.Parse(Session["SessionID"].ToString());
            Response.Redirect("PollingSpeakerPanel.aspx?sessionID=" + id);
        }
        else
        {
            Response.Redirect("../Admin/Timeout.aspx");
        }
    }

    string qrpath = "~/QRImg/Polling/";
    public string getQRCode(string sessionID)
    {
        string qrCodeLink = "";
        try
        {
            if (!string.IsNullOrEmpty(sessionID) && !string.IsNullOrWhiteSpace(sessionID))
            {
                #region make qrcode
                string currentUrl = Constant.URL + "apiv1/LiveQA?SessionID=" + sessionID;
                GenQRCode gen_qr = new GenQRCode();
                string gen_qrcodeUrl = gen_qr.genQROnlyOneParam(currentUrl, sessionID, qrpath);
                if (!string.IsNullOrEmpty(gen_qrcodeUrl))
                {
                    if (File.Exists(Server.MapPath(gen_qrcodeUrl)))
                    {
                        qrCodeLink = Constant.URL + "QRImg/Polling/" + sessionID + ".Png";
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        { }

        return qrCodeLink;
    }

}