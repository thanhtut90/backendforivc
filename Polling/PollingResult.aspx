﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PollingResult.aspx.cs" Inherits="apiv1_PollingResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=yes" />
    <link href="../Content/backlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding-left:0px;padding-right:0px;">
            <img alt="header" src="../sys_Images/CET_PollingBanner.png" class="img-responsive"/>
        </div>
        <div id="Div1">
            <div id="food">
                <%=questionAns %>
            </div>
        </div>
        <div style="margin-right: -120px;display:none;">
            <br />
            <div class="row">
                <div class="col-sm-12">
						<br /><br />
						<div class="row pull-right">
							<div class="col-md-12 col-sm-12">
								<asp:Image ID="imgQRCode" runat="server" ImageUrl="#" CssClass="img-responsive" Width="250" />
							</div>
						</div>
                </div>
            </div>
        </div>
    </form>   
</body>
</html>
