﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using App.DAL;
using App.Common;
using System.Data.Entity;
using Telerik.Web.UI;

public partial class PollingSummaryReport : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            GKeyMaster.ExportSettings.IgnorePaging = true;
            GKeyMaster.ExportSettings.ExportOnlyData = true;
            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
            GKeyMaster.ExportSettings.FileName = "PolllingReport";
        }
    }
    protected void GKeyMaster_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dtResult = getPollingData();
            GKeyMaster.DataSource = dtResult;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    private DataTable getPollingData()
    {
        DataTable dtResult = new DataTable();
        try
        {
            dtResult.Columns.Add("Session Title");
            dtResult.Columns.Add("Date");
            dtResult.Columns.Add("Time");
            dtResult.Columns.Add("Question");
            dtResult.Columns.Add("Answer");
            dtResult.Columns.Add("Count");
            dtResult.Columns.Add("Percentage");
            int noofans = 0;
            int.TryParse(fn.GetDataByCommand("Select Max(noOfAns) noOfAns From PollingQandA GROUP BY noOfAns", "noOfAns"), out noofans);
            string sqlPollingConference = "select * from tblConference where c_Polling=1 and deleteFlag=0";
            DataTable dtPC = fn.GetDatasetByCommand(sqlPollingConference, "ds").Tables[0];
            if (dtPC.Rows.Count > 0)
            {
                foreach (DataRow drPC in dtPC.Rows)
                {
                    string sessionID = drPC["c_ID"].ToString();
                    string ConfTitle = drPC["c_title"].ToString();
                    string ConfDate = drPC["c_date"].ToString();
                    string ConfTime = drPC["c_stime"].ToString() + "-" + drPC["c_etime"].ToString();
                    string sqlPollingQA = "select * from PollingQandA where sessionID='" + sessionID + "'";
                    DataTable dtPollingQA = fn.GetDatasetByCommand(sqlPollingQA, "ds").Tables[0];
                    if (dtPollingQA.Rows.Count > 0)
                    {
                        foreach (DataRow drPQA in dtPollingQA.Rows)
                        {
                            string questionID = drPQA["questionID"].ToString();
                            string QTitle = drPQA["question"].ToString();
                            var PR = dalf.SelectPollingResutBySIDQID(sessionID, questionID);
                            int totalcount = PR.Count;
                            for (int x = 1; x <= noofans; x++)
                            {
                                DataRow drR = dtResult.NewRow();
                                drR["Session Title"] = ConfTitle;
                                drR["Date"] = ConfDate;
                                drR["Time"] = ConfTime;
                                drR["Question"] = QTitle;
                                drR["Answer"] = drPQA["answer" + x].ToString();
                                int anscount = dalf.PRAnsCount(sessionID, questionID, x);
                                double percent;
                                if (anscount > 0)
                                {
                                    percent = Math.Round((double)anscount / (double)totalcount * 100);
                                }
                                else
                                {
                                    percent = 0;
                                }
                                drR["Count"] = anscount;
                                drR["Percentage"] = percent + "%";
                                dtResult.Rows.Add(drR);
                            }
                        }
                    }
                }
            }
        }
        catch(Exception ex)
        { }

        return dtResult;
    }
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
    }
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;
    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        this.GKeyMaster.ExportSettings.ExportOnlyData = true;
        this.GKeyMaster.ExportSettings.IgnorePaging = true;
        this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
        this.GKeyMaster.MasterTableView.ExportToExcel();
        GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        GKeyMaster.ExportSettings.FileName = "PolllingReport";
    }
    protected void GKeyMaster_PreRender(object sender, EventArgs e)
    {
        for (int rowIndex = GKeyMaster.Items.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridDataItem row = GKeyMaster.Items[rowIndex];
            GridDataItem previousRow = GKeyMaster.Items[rowIndex + 1];
            if (row["Session Title"].Text == previousRow["Session Title"].Text)
            {
                row["Session Title"].RowSpan = previousRow["Session Title"].RowSpan < 2 ? 2 : previousRow["Session Title"].RowSpan + 1;
                previousRow["Session Title"].Visible = false;
            }
            if (row["Date"].Text == previousRow["Date"].Text)
            {
                row["Date"].RowSpan = previousRow["Date"].RowSpan < 2 ? 2 : previousRow["Date"].RowSpan + 1;
                previousRow["Date"].Visible = false;
            }
            if (row["Time"].Text == previousRow["Time"].Text)
            {
                row["Time"].RowSpan = previousRow["Time"].RowSpan < 2 ? 2 : previousRow["Time"].RowSpan + 1;
                previousRow["Time"].Visible = false;
            }
            if (row["Question"].Text == previousRow["Question"].Text)
            {
                row["Question"].RowSpan = previousRow["Question"].RowSpan < 2 ? 2 : previousRow["Question"].RowSpan + 1;
                previousRow["Question"].Visible = false;
            }
        }
    }
}