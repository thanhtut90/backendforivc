﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PSAdmin.aspx.cs" Inherits="PSAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
      <script type="javascript">
        $(document).ready(function () {

            $("#tabuser tr:even").attr("className", "color3");
            $("#tabuser tr:odd").attr("className", "color4");
            $(this).find("tr:first").attr("className", "color2");
        })
    </script>


     <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><asp:Label ID="lbl_sessionTitle" runat="server"></asp:Label></h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                    <div class="form-group">                      
                      <div class="col-sm-12">
                          <table border="0" id="tabuser" class="table" style="width:98%">
                            <tr bgcolor="#336666" class="color2">
                                <td width="5%" align="center">S/N</td>
                                <td>Question</td> 
                            </tr>
                            <%=htmlstr %>                
                            </table>
                      </div>
                    </div>
              </div>              
              <div class="box-footer">                                
                <a href="Polling.aspx" class="btn btn-default">Back</a>
              </div>              
            </div>
           </div>
</asp:Content>
