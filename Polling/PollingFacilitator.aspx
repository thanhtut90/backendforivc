﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PollingFacilitator.aspx.cs" Inherits="Polling" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title"><asp:Label ID="lblsession" runat="server"></asp:Label></h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">  
                   <div class="form-group">                      
                      <div class="col-sm-12">                        
                        <b>
                        Date : <asp:Label ID="lblDate" runat="server"></asp:Label><br /><br />
                        Time : <asp:Label ID="lbltime" runat="server"></asp:Label>
                        </b>
                      </div>
                    </div>                   
                    <div class="form-group">                      
                      <div class="col-sm-12">
                        <a href="PollingQuestionFacilitator.aspx?SessionID=<%=sessionid%>" class="btn btn-info">Question</a>&nbsp;&nbsp;&nbsp;
                        <a href="PollingSpeakerPanel.aspx?SessionID=<%=sessionid %>" class="btn btn-info">Session</a>
                      </div>
                    </div>
              </div>
      </div>
      </div>    
    </asp:Content>
