﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using System.IO;

public partial class apiv1_PollingResult : System.Web.UI.Page
{
    string sessionID;
    string questionID;
    public string questionAns;
    protected string showid = "MobCET2019";
    string qrpath = "~/QRImg/";
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sessionID"] != null && Request.QueryString["questionID"] != null)
            {
                try
                {
                    sessionID = Request.QueryString["sessionID"].ToString();
                    questionID = Request.QueryString["questionID"].ToString();

                    var PQA = dalf.SelectPollingQAbyQIDSID(questionID, sessionID);// "00000000");//sessionID

                    int noOfAns = PQA.noOfAns.Value;
                    questionAns += " <span class='title' style='font-size:18px;color:#303030;font-weight:bold;line-height:35px;text-align:justify'>" + PQA.question + "</span>";

                    var PR = dalf.SelectPollingResutBySIDQID(sessionID, questionID);
                    int totalcount = PR.Count;
                    questionAns += "<table cellpadding='0px' cellspacing='0px'>";
                    for (int i = 1; i <= noOfAns; i++)
                    {
                        string option = "answer" + i;
                        if(option == "answer1")
                        {
                            option = PQA.answer1;
                        }
                        else if (option == "answer2")
                        {
                            option = PQA.answer2;
                        }
                        else if (option == "answer3")
                        {
                            option = PQA.answer3;
                        }
                        else if (option == "answer4")
                        {
                            option = PQA.answer4;
                        }
                        else if (option == "answer5")
                        {
                            option = PQA.answer5;
                        }
                        else if (option == "answer6")
                        {
                            option = PQA.answer6;
                        }
                        else if (option == "answer7")
                        {
                            option = PQA.answer7;
                        }
                        else if (option == "answer8")
                        {
                            option = PQA.answer8;
                        }
                        else if (option == "answer9")
                        {
                            option = PQA.answer9;
                        }
                        StringBuilder sqlOptionCount = new StringBuilder();
                        sqlOptionCount.Append("Select * from PollingResults where voteSession=" + sessionID + " and voteQuestion=" + questionID + " and voteAnswer = " + i + ";");
                        int count = dalf.PRAnsCount(sessionID, questionID, i);

                        double percent;
                        if (count > 0)
                        {
                            percent = Math.Round((double)count / (double)totalcount * 100);

                        }
                        else
                        {
                            percent = 0;
                        }

                        questionAns += "<tr><td valign='top' width='2px'><span  style='font-size:18px;color:#303030;'>" + i + ". " + "</span></td><td><span  style='font-size:18px;color:#303030;'>" +option+"</span><br />";
                        double Wtotal;
                        if (percent == 0)
                        {
                            Wtotal = 1;
                            questionAns += "<br/><div class='barpad'><div style='font-size:18px;color:#303030;width:" + percent + "%;line-height:20px;color:#303030 ;text-align:center;'>" + percent + "% (" + count + ")</div></div>";
                        }
                        else
                        {
                            Wtotal = percent;
                            questionAns += "<br/><div class='barpad'><div style='font-size:18px; width:" + (percent) + "%;height:45px;line-height:45px;background-color:#f2ac24;color:black;text-align:left;border:solid 1px #b2b2b2;padding-left:10px;'>" + percent + "% (" + count + ")</div></div>";
                            //questionAns += "<br/><div class='barpad'><div style='font-size:18px; width:" + (percent * 5) + "px;height:30px;line-height:30px;background-color:#f2ac24;color:black;text-align:left;'>" + percent + "% (" + count + ")</div></div>";
                        }

                    }
                    questionAns += "</td></tr></table>";

                    #region make qrcode
                    string currentUrl = Constant.URL + "apiv1/Polling?SessionID=" + sessionID;
                    string qrID = questionID + "_" + sessionID;
                    GenQRCode gen_qr = new GenQRCode();
                    string gen_qrcodeUrl = gen_qr.genQROnlyOneParam(currentUrl, qrID, qrpath);
                    //QRcodeMaker qMaker = new QRcodeMaker();
                    //string gen_qrcodeUrl = qMaker.CreateQRcodeOnlyOneParam(currentUrl, showid);
                    imgQRCode.Visible = false;
                    if (!string.IsNullOrEmpty(gen_qrcodeUrl))
                    {
                        //if (IsUrlExist(gen_qrcodeUrl))
                        if (File.Exists(Server.MapPath(gen_qrcodeUrl)))
                        {
                            imgQRCode.Visible = true;
                            imgQRCode.ImageUrl = gen_qrcodeUrl;
                        }
                    }
                    #endregion
                }
                catch { }
            }
        }
    }
}