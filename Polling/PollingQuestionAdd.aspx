﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="PollingQuestionAdd.aspx.cs" Inherits="PollingQuestionAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server"> 
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Polling Question</h3>              
            </div>
      <div class="form-horizontal">
              <div class="box-body">     
                  <div class="form-group">                      
                      <div class="col-sm-12">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                      </div>
                    </div>   
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Question</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txt_question" class="form-control" required="" placeholder="Enter Question" runat="server"></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group">
                    <label for="noa" class="col-sm-2 control-label">No of Answer</label>
                    <div class="col-sm-6">
                        <div class="input-control select full-size">
                        <asp:DropDownList ID="ddl_answer" runat="server" AutoPostBack="True" required="" onselectedindexchanged="ddl_answer_SelectedIndexChanged">
                            <asp:ListItem>Select Number</asp:ListItem>
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    </div>
                </div>
                <div class="form-group" id="answerrow1" visible="true" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 1</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns1" class="form-control" placeholder="Enter Answer 1" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow2" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 2</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns2" class="form-control" placeholder="Enter Answer 2" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow3" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 3</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns3" class="form-control" placeholder="Enter Answer 3" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow4" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 4</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns4" class="form-control" placeholder="Enter Answer 4" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow5" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 5</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns5" class="form-control" placeholder="Enter Answer 5" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow6" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 6</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns6" class="form-control" placeholder="Enter Answer 6" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow7" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 7</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns7" class="form-control" placeholder="Enter Answer 7" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow8" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 8</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns8" class="form-control" placeholder="Enter Answer 8" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                <div class="form-group" id="answerrow9" visible="false" runat="server">
                    <label for="inputEmail3" class="col-sm-2 control-label">Answer 9</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtAns9" class="form-control" placeholder="Enter Answer 9" runat="server" ></asp:TextBox>
                    </div>
                </div> 
                  </div>
              <!-- /.box-body -->
              <div class="box-footer">                                
                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" class="btn btn-info" runat="server" Text="Add" />                
              </div>
              <!-- /.box-footer -->
            </div>
           </div>
</asp:Content>
