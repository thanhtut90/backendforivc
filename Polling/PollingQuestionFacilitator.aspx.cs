﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using App.DAL;
public partial class Polling : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data daldata = new DAL_Data();
    protected string sessionID;
    protected string htmlstr;
    protected string rowid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["sessionID"] != null && Request.QueryString["command"] != "delete")
            {
                sessionID = Request.QueryString["sessionID"].ToString();
                DataTable dt = daldata.getConferenceByID(sessionID);
                if (dt.Rows.Count > 0)
                {
                    lbl_sessionTitle.Text = dt.Rows[0]["c_title"].ToString();
                    DataTable sessionlst = daldata.selectPollingQABySID(sessionID);
                    if(dt.Rows.Count>0)
                    {
                        int i = 0;
                        StringBuilder str = new StringBuilder();
                        foreach(DataRow dr in sessionlst.Rows)
                        {
                            i++;
                            str.Append("<tr class='color4'>");
                            str.Append("<td align='center'>" + i + "</td>");
                            str.Append("<td>" + dr["question"].ToString() + "</td>");
                            str.Append("<td align='center'><a href='PollingQuestionAdd.aspx?SessionID=" + sessionID + "&QuestionID=" + dr["questionID"].ToString() + "' >Edit</a></td>");
                            //str.Append("<td align='center'><a href='PollingQuestionFacilitator.aspx?SessionID=" + s.sessionID + "&QuestionID=" + s.questionID + "&command=delete' >Delete</a></td></td>");
                            str.Append("</tr>");
                        }
                        htmlstr = str.ToString();
                    }
                }
            }
            if(Request.QueryString["sessionID"] != null && Request.QueryString["QuestionID"] != null && Request.QueryString["command"] == "delete")
            {
                string sid = Request.QueryString["sessionID"].ToString();
                string qid = Request.QueryString["QuestionID"].ToString();
                Delete(sid, qid);
            }
        }
    }
    //<asp:LinkButton ID = 'lnkDelete' CommandName='Delete' CommandArgument='s.sessionID_s.questionID' onclick='lnkdelete_Click' runat='server'>Delete</asp:LinkButton>
   
    private void Delete(string sessionID,string questionID)
    {
        int res = dalf.DeletePollingQuestion(sessionID, questionID);
        if (res == 1)
        {
            Response.Redirect("PollingQuestionFacilitator.aspx?sessionID=" + sessionID + "");
        }
    }

}
