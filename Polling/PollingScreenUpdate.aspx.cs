﻿using System;
using App.DAL;
using App.Common;

public partial class PollingScreenUpdate : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    
    string sessionID;
    string questionID;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["SessionID"] == null)
        {
            Response.Redirect("~/Polling/Polling.aspx");
        }
        else
        {
            try
            {
                sessionID = Request.QueryString["sessionID"].ToString();
                questionID = Request.QueryString["questionID"].ToString();
                int UPType = int.Parse(Request.QueryString["UPType"].ToString());
                if (!IsPostBack)
                {
                    if (UPType == 1)
                    {
                        int cmdcount1 = dalf.DeletePollingResult(sessionID, questionID);
                        if (cmdcount1 > 0)
                        {
                            Response.Redirect("PollingSpeakerPanel.aspx?sessionID=" + sessionID + "");
                        }
                    }
                    dalf.UpdatePollingResultStatus(UPType, questionID);


                    //   if (UPType == 1)
                    //{
                    //    Response.Redirect("PollingScreenQues.aspx?sessionID=" + sessionID + "&questionID=" + questionID + "");
                    //}
                    //else
                    //{
                        Response.Redirect("PollingSpeakerPanel.aspx?SessionID=" + sessionID);
                    //}
                }
            }
            catch { }            
        }

    }
}