﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SQAdmin.aspx.cs" Inherits="SQAdmin" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Content/backlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Content/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <style type="text/css" >
    .timebox { min-height:80px; overflow: hidden; position:relative; margin-bottom: 10px; font-family:Arial;}
    .fl{ float:left; width:160px; position:absolute; left:0px;top:0px; z-index:2;}
    .fr{ float:left; margin-left:120px;}
    .timebox span { background: #52b7c9; color: #af2295;text-align: center;  padding:5px 10px;  display: inline-block; }
    .timebox b { font-size: 22px; }
    </style>
    <script type="text/javascript" src="../Content/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script type="text/javascript">

        $(function () {
            timestamp = 0;
            updateMsg(); 
        })

        function updateMsg() { 
            $.post("../Handle/msgadmin.ashx?meetid=" + $("#meetid").val(), {
                time: timestamp
            }, function (xml) {
                $("#loading").remove();
                    
                addMessages(xml);
            });
            setTimeout('updateMsg()',10000);
        }            

        function addMessages(xml) {
            if ($("status", xml).text() == "2") return;
            timestamp = $("time", xml).text();
            var htmlcode = "";
            htmlcode += "<div class='showbox'>";
            $("message", xml).each(function () {

                var QAID = $("QAID", this).text();
                var SessionID = $("SessionID", this).text();
                var QATitle = $("QATitle", this).text();
                var QANum = $("QANum", this).text();
                var QAStatus = $("QAStatus", this).text();
                htmlcode += "<div class='show clearfix'>";
                htmlcode += "<div class='showL'>";

                htmlcode += "" + QAStatus + "<br />";
                if ($("StatusType", this).text() == "1") {
                    htmlcode += "<img src='../sys_Images/approveGray.png' width='129' height='46' />";
                }
                else {
                    htmlcode += "<a href='' onclick=\"sendVote1('" + QAID + "')\"><img src='../sys_Images/approve.png' width='129' height='46' /></a>";
                }
                htmlcode += " <a href='' onclick=\"sendVote2('" + QAID + "')\"><img src='../sys_Images/decline.png' width='129' height='46' /></a>";
                htmlcode += "</div>";
                htmlcode += "<i>";

                if ($("StatusType", this).text() == "1") {
                    if ($("QAType", this).text() == "1") {
                        htmlcode += "<a href='' onclick=\"sendVote4('" + QAID + "')\"><img src='../sys_Images/showAct.png' width='100' height='80' /></a>";

                    }
                    else {
                        htmlcode += "<a href='' onclick=\"sendVote3('" + QAID + "')\"><img src='../sys_Images/show.png' width='100' height='80' /></a>";
                    }
                }
                else {
                    htmlcode += "<img src='../sys_Images/show.png' width='100' height='80' />";
                }

                if ($("StatusType", this).text() == "1") {
                    htmlcode += "<br>";
                }
                else {
                    htmlcode += "<br><a href='../LiveQA/editQA.aspx?QAID=" + QAID + "' target='_blank'><img src='../sys_Images/editGray .png' width='100' height='40' /></a>";
                }
                htmlcode += "</i>";
                htmlcode += "<span>[" + QANum + " Votes] " + QATitle + "</span></div>";

            });
            htmlcode += "</div>";
            $("#messagewindow").html(htmlcode);
        }
        function sendVote1(QAID) {


            $.ajax({
                type: "POST",
                url: "../Handle/SQAVoteUp.ashx",

                data: "PType=1&QAID=" + QAID,

                success: function (data) {
                    if (data == "2") {
                        alert("success");

                        return false;
                    }
                   

                }
            });
        }

        function sendVote2(QAID) {

            alert("decline");
            $.ajax({
                type: "POST",
                url: "../Handle/SQAVoteUp.ashx",

                data: "PType=2&QAID=" + QAID,

                success: function (data) {
                    if (data == "2") {
                        alert("success");

                        return false;
                    }
                    

                }
            });
        }


        function sendVote3(QAID) {
            $.ajax({
                type: "POST",
                url: "../Handle/SQAVoteUp.ashx",

                data: "PType=3&QAID=" + QAID,

                success: function (data) {
                    if (data == "2") {
                        alert("success");
                        return false;
                    }


                }
            });
        }

        function sendVote4(QAID) {


            $.ajax({
                type: "POST",
                url: "../Handle/SQAVoteUp.ashx",

                data: "PType=4&QAID=" + QAID,

                success: function (data) {
                    if (data == "2") {
                        alert("success");

                        return false;
                    }


                }
            });
        }
    </script>
</head>
<body>
    <%--<form id="frm" runat="server">
    
    <div style="width:100%;margin:10px auto; ">--%>
        <div id="messagewindow"><span id="loading">Loading...</span></div>&nbsp;&nbsp;
        <input type="hidden" name="meetid" id="meetid" value="<%=meetid%>" />
        <input  type="hidden" name="LID" id="LID" />
    <%--</div>
    </form>--%>

    
    




<%--<div id="Div1"><span id="Span1">Loading...</span></div>&nbsp;&nbsp;
<input type="hidden" name="meetid" id="Hidden1" value="<%=meetid%>" />

<input  type="hidden" name="LID" id="Hidden2" />
</div>--%>
</body>
</html>
