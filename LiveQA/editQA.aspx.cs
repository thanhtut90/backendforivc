﻿using System;
using App.DAL;
using App.Common;

public partial class editQA : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["QAID"] != null)
            {
                string QAID = Request.QueryString["QAID"].ToString();                
                var lst = daldata.SelectLiveQAByID(QAID);
                QATitle.Text = lst.Rows[0]["LQATitle"].ToString();                
            }
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string QAID = Request.QueryString["QAID"].ToString();
      
        daldata.UpdateLQATitle(QAID, QATitle.Text.Trim());
        Response.Write("<script language='javascript'>alert('Updated');window.location.href='SQAList.aspx';</script>");
        Response.End();
    }

    public string FilterSpecial(string str)
    {
        if (str == "")
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");
            return str;
        }
    }
}