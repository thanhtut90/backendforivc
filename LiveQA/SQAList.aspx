﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="SQAList.aspx.cs" Inherits="SQAList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">   
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                        <asp:HiddenField ID="hdPID" runat="server" />
                        <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                        <h3 class="box-title">Live Q & A Sessions</h3>
                </div>                                       
                <div class="box-body">                           
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <asp:GridView ID="grdSQA" DataKeyNames="f_id" PageSize="50" GridLines="Horizontal" 
                class="table" Width="98%" AutoGenerateColumns="False" runat="server" CellPadding="4" BackColor="White" BorderColor="#336666" BorderWidth="1px" BorderStyle="None">
            <Columns> 
                <asp:TemplateField HeaderText="Title" >
                    <ItemTemplate>
                        <%# Eval("f_content")%>
                    </ItemTemplate>
                    
                </asp:TemplateField> 

                <asp:TemplateField HeaderText="Pending" >
                    <ItemTemplate>
                      <a href="SQAdmin1.aspx?meetid=<%#Eval("f_c_rowid") %>" class="label label-warning">Pending</a>
                    </ItemTemplate>                    
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Approved" >
                    <ItemTemplate>
                      <a href="SQAdmin.aspx?meetid=<%#Eval("f_c_rowid") %>" class="label label-success">Approved</a>
                    </ItemTemplate>                    
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Decline" >
                    <ItemTemplate>
                      <a href="SQAdmin2.aspx?meetid=<%#Eval("f_c_rowid") %>" class="label label-danger">Declined</a>
                    </ItemTemplate>                    
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Show" >
                    <ItemTemplate>
                      <a href="SQAShow.aspx?meetid=<%#Eval("f_c_rowid") %>" class="label label-info">Show</a>
                    </ItemTemplate>                    
                </asp:TemplateField>
               
                     </Columns>
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    
    
     
   
</asp:Content>

