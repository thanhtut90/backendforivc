﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using System.IO;

public partial class LiveQA_LiveQAList : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {
                BindControlData();

                grdSQA.ExportSettings.IgnorePaging = true;
                grdSQA.ExportSettings.ExportOnlyData = true;
                grdSQA.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdSQA.ExportSettings.FileName = "Day";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            //DataTable dtmenu = daldata.selectFloorplanMenu();

            //ddlmenu.DataSource = dtmenu;
            //ddlmenu.DataTextField = "m_TitleEn";
            //ddlmenu.DataValueField = "m_id";
            //ddlmenu.DataBind();
            //ListItem lst = new ListItem("Select Menu", "");
            //ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
            //    lblerr.Text = string.Empty;
            //    DataTable dt = new DataTable();
            //    dt.Columns.Add("d_ID");
            //    dt.Columns.Add("d_key");
            //    dt.Columns.Add("d_date");
            //    dt.Columns.Add("d_year");
            //    dt.Columns.Add("deleteFlag");
            //    dt.Columns.Add("UpdatedDate");
            //    string datestr = daldata.replaceForSQL(txtdate.Text.Trim());
            //    string[] date = datestr.Split('/');
            //    string[] monthArr = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            //    int monthno = int.Parse(date[0]) - 1;
            //    string day = date[1].TrimStart('0');
            //    string month = monthArr[monthno];
            //    string dayMonth = day + " " + month;
            //    string year = date[2];
            //    //string imagename = "";
            //    //string filename = string.Empty;
            //    //string extension = string.Empty;
            //    //string path = Constant.filesavingurl;
            //    //string message = string.Empty;
            //    //if (fupForm.HasFile == true)
            //    //{
            //    //    filename = fupForm.FileName.Substring(0, fupForm.FileName.LastIndexOf("."));
            //    //    extension = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
            //    //    imagename = UploadIcon(filename, extension, path, ref message);
            //    //}
            //    if (hfid.Value != "")
            //    {

            //        //Update
            //        DataRow dr = dt.NewRow();
            //        dr["d_ID"] = hfid.Value.ToString();
            //        dr["d_key"] = day;
            //        dr["d_date"] = dayMonth;
            //        dr["d_year"] = year;
            //        dr["deleteFlag"] = false;
            //        dr["UpdatedDate"] = DateTime.Now;



            //        int res = daldata.UpdateDay(dr);
            //        if (res == 1)
            //        {
            //            grdSQA_NeedDSource("");
            //            divadd.Visible = false;
            //            divsearch.Visible = true;
            //            divlist.Visible = true;
            //            noti.Visible = true;
            //            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
            //            msg.Text = "Update Success !";

            //            clearControls();
            //        }
            //    }
            //    else
            //    {
            //        //Insert
            //        DataRow dr = dt.NewRow();
            //        dr["d_ID"] = "";
            //        dr["d_key"] = day;
            //        dr["d_date"] = dayMonth;
            //        dr["d_year"] = year;
            //        dr["deleteFlag"] = false;
            //        dr["UpdatedDate"] = DateTime.Now;
            //        int res = daldata.SaveDayGetID(dr);
            //        if (res == 1)
            //        {
            //            grdSQA_NeedDSource("");
            //            divadd.Visible = false;
            //            divsearch.Visible = true;
            //            divlist.Visible = true;
            //            noti.Visible = true;
            //            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
            //            msg.Text = "Save Success !";

            //            clearControls();
            //        }
              //  }
            }
            else
            {
               // lblerr.Text = "**Required";
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            //if (string.IsNullOrEmpty(txtdate.Text.Trim()))
            //{
            //    rtn = false;
            //}

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdSQA.ExportSettings.ExportOnlyData = true;
        this.grdSQA.ExportSettings.IgnorePaging = true;
        this.grdSQA.ExportSettings.OpenInNewWindow = true;
        this.grdSQA.MasterTableView.ExportToExcel();

        grdSQA.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdSQA.ExportSettings.FileName = "Day";
    }
    //protected void lnkAdd_OnClick(object sender, EventArgs e)
    //{
    //    divadd.Visible = true;
    //    divlist.Visible = false;
    //    divsearch.Visible = false;

    //    clearControls();
    //}
    protected void grdSQA_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();
            dt = daldata.selectQASessions();
            grdSQA.DataSource = dt;
            Session["dt"] = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdSQA_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdSQA.CurrentPageIndex + 1;
    }
    protected void grdSQA_NeedDSource(string searchkey)
    {
        try
        {
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getSQAbyKeywords(searchkey);
            }
            else
            {
                dt = daldata.selectQASessions();
            }


            grdSQA.DataSource = dt;
            grdSQA.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdSQA_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdSQA.ExportSettings.ExportOnlyData = true;
            this.grdSQA.ExportSettings.IgnorePaging = true;
            this.grdSQA.ExportSettings.OpenInNewWindow = true;
            this.grdSQA.MasterTableView.ExportToExcel();
        }
    }
    //protected void lnkEdit_Onclick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        divadd.Visible = true;
    //        divlist.Visible = false;
    //        divsearch.Visible = false;
    //        string id = ((LinkButton)sender).CommandArgument;
    //        if (!string.IsNullOrEmpty(id))
    //        {
    //            DataTable dt = daldata.getDaylstByID(id);
    //            if (dt.Rows.Count > 0)
    //            {
    //                string date = dt.Rows[0]["d_date"].ToString() + " " + dt.Rows[0]["d_year"].ToString();
    //                hfid.Value = dt.Rows[0]["d_ID"].ToString();
    //                txtdate.Text = date;
    //                //ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
    //            }
    //        }
    //    }
    //    catch (Exception ex) { }
    //}
    protected void linkshow_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Show")
            {
                string ID = del.CommandArgument.ToString();
                Response.Redirect("SQAShow.aspx?meetid=" + ID);
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdSQA_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    //private string UploadIcon(string filename, string extension, string path, ref string message)
    //{
    //    string imagefilename = string.Empty; string messages = string.Empty;
    //    try
    //    {
    //        if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
    //        {
    //            messages = "Invalid Image Type";
    //        }

    //        string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

    //        string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
    //        if (!System.IO.Directory.Exists(CreatePath))
    //        {
    //            System.IO.Directory.CreateDirectory(CreatePath);
    //        }

    //        string savePath = Server.MapPath((path) + fname);
    //        fupForm.SaveAs(savePath);
    //        imagefilename = fname;
    //    }
    //    catch (Exception ex) { }
    //    return imagefilename;
    //}
    //private void clearControls()
    //{
    //    hfid.Value = "";
    //    txtKeyword.Text = "";
    //    txtdate.Text = "";
    //    //ddlmenu.SelectedIndex = 0;
    //    //lblfloorplan.Text = "";
    //}

    string qrpath = "~/QRImg/LiveQA/";
    public string getQRCode(string sessionID)
    {
        string qrCodeLink = "";
        try
        {
            if(!string.IsNullOrEmpty(sessionID) && !string.IsNullOrWhiteSpace(sessionID))
            {
                #region make qrcode
                string currentUrl = Constant.URL + "apiv1/LiveQA?SessionID=" + sessionID;
                GenQRCode gen_qr = new GenQRCode();
                string gen_qrcodeUrl = gen_qr.genQROnlyOneParam(currentUrl, sessionID, qrpath);
                if (!string.IsNullOrEmpty(gen_qrcodeUrl))
                {
                    if (File.Exists(Server.MapPath(gen_qrcodeUrl)))
                    {
                        qrCodeLink = Constant.URL + "QRImg/LiveQA/" + sessionID + ".Png";
                    }
                }
                #endregion
            }
        }
        catch(Exception ex)
        { }

        return qrCodeLink;
    }
}