﻿using System;
using System.Data;
using System.Text;
using App.DAL;
public partial class SQAList : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var lst = dalf.selectQASessions();           

            grdSQA.DataSource = lst;
            grdSQA.DataBind();
           
        }
    }
}