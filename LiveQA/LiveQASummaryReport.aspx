﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="LiveQASummaryReport.aspx.cs" Inherits="LiveQASummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Default">
        <TargetControls>
            <telerik:TargetControl ControlID="grdCln" />    
        </TargetControls>
    </telerik:RadSkinManager>
     <div class="box box-info">
        <div class="box-body">
            <div class="form-group">
                <div class="col-sm-12">
                    <h3 class="box-title">Live Q & A Report</h3>
                    <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                    <div style="float:right">
                        <asp:LinkButton ID="lnkDownload" runat="server" class="btn btn-info" OnClick="lnkDownload_Click"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                    </div>  
                </div>
            </div>             
            <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                        <telerik:RadGrid RenderMode="Lightweight" ID="GKeyMaster"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" 
                         Font-Size="Small" OnItemCommand="GKeyMaster_ItemCommand"
                            PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="GKeyMaster_NeedDataSource" AllowPaging="true" PageSize="100" 
                         OnPageIndexChanged="GKeyMaster_PageIndexChanged" OnPreRender="GKeyMaster_PreRender">
                             <ClientSettings AllowKeyboardNavigation="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="true" CommandItemDisplay="TopAndBottom" HeaderStyle-BackColor="#d9d9d9"
                                HeaderStyle-Font-Bold="true" >
                            <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />                                            
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>              
                </div>
            </div>
        </div>
     </div>
</asp:Content>


