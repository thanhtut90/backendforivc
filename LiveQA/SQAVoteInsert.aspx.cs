﻿using System;
using App.DAL;
using App.Common;
public partial class SQAVoteInsert : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.Form["QAID"] != null && Request.Form["Uid"]!=null)
            {

                string Uid = Request.Form["Uid"].ToString();
                string QAID = Request.Form["QAID"].ToString();
                string showid = Request.Form["showid"].ToString();
                string sql = "SELECT * from SQAVote where Uid="+int.Parse(Uid)+" and QAID="+int.Parse(QAID);
                var LQAVoteRes = daldata.SelectLQAVoteByQAIDUF(Uid, QAID);
                if(LQAVoteRes != null)
                { 
                    daldata.UpdateLQA(QAID);

                    daldata.SaveLQAVote(QAID, Uid);
                }
            }
        }
    }
}