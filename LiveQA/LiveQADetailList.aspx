﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="LiveQADetailList.aspx.cs" Inherits="LiveQA_LiveQADetailList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdSQADetail" />
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Live Q & A Sessions</h3>

                </div>
                <div class="box-body">
                    <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;">
                        <div class="col-md-5">
                            <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />
                        </div>
                        <div class="col-md-6">
                            <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>

                    </div>
                    <%-- <asp:UpdatePanel runat="server">
                                      <ContentTemplate>--%>
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdSQADetail" runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdSQADetail_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdSQADetail_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdSQADetail_PageIndexChanged" OnItemCreated="grdSQADetail_ItemCreated">
                                    <ClientSettings AllowKeyboardNavigation="true" >
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="LQAID" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                                        <Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1  %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="show"
                                                HeaderText="Show On Screen" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td style="width:100px">                                                               
                                                                <asp:LinkButton ID="lnkShow" runat="server" class='<%#checkShowCSS(Eval("QAType").ToString())%>' CommandName="show" CommandArgument='<%#Eval("LQAID") %>' OnClick="lnkShow_Onclick" Enabled='<%#checkShowStatus(Eval("LQAStatus").ToString())%>'><i class="material-icons"  style="font-size:20px">airplay</i></asp:LinkButton></td>                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="LQANum" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Vote Count" SortExpression="LQANum" UniqueName="LQANum" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LQATitle" FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true"
                                                HeaderText="Question Title" SortExpression="LQATitle" UniqueName="LQATitle" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Status" SortExpression="LQAStatus" UniqueName="LQAStatus"  HeaderStyle-Font-Bold="true"
                                                FilterControlAltText="Filter LQAStatus">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_status" runat="server"><%#GetStatus(Eval("LQAStatus").ToString())%></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td style="width:100px">
                                                                <asp:LinkButton ID="lnkApprove" runat="server" class="btn btn-success btn-sm" CommandName="Approve" CommandArgument='<%#Eval("LQAID") %>' OnClick="lnkApprove_Onclick" Enabled='<%#checkApproveStatus(Eval("LQAStatus").ToString())%>'><span class="glyphicon glyphicon-ok"></span> Approve</asp:LinkButton></td>
                                                             <td style="width:100px">
                                                                <asp:LinkButton ID="lnkReject" runat="server" class="btn btn-danger btn-sm" OnClientClick='return confirm("Declined?")' CommandName="Declined" CommandArgument='<%#Eval("LQAID") %>' OnClick="lnkReject_Click" Enabled='<%#checkDeclinedStatus(Eval("LQAStatus").ToString())%>'><span class="glyphicon glyphicon-remove"></span> Declined</asp:LinkButton></td>
                                                             <td style="width:100px">
                                                                <asp:LinkButton ID="lnkEdit" runat="server" class="btn btn-warning btn-sm" CommandName="Edit" CommandArgument='<%#Eval("LQAID") %>' OnClick="linkedite_Click"><span class="glyphicon glyphicon-pencil"></span> Edit</asp:LinkButton>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                       
                            </div>
                        </div>
                    </div>
                    <div id="divadd" runat="server" visible="false" class="row">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Question Title</label>
                                <div class="col-sm-4">
                                    <asp:HiddenField ID="hfid" runat="server" />
                                    <asp:TextBox ID="txtQtitle" class="form-control" placeholder="Choose Date" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label>
                                </div>
                            </div>

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Update" />
                        </div>
                    </div>
            <%--    </ContentTemplate>
                                          </asp:UpdatePanel>--%>
                </div>
            </div>
        </div>
        </div>
    </section>
</asp:Content>



