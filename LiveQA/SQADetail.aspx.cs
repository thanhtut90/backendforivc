﻿using System;
using System.Data;
using App.DAL;
public partial class SQADetail : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["QAID"] != null)
        {
            if (!IsPostBack)
            {
                string QAID = Request.QueryString["QAID"].ToString();
                var lst = daldata.SelectLiveQAByID(QAID);
                LabDetail.Text = lst.Rows[0]["LQATitle"].ToString();
            }
        }
    }
}