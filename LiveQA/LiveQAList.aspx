﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="LiveQAList.aspx.cs" Inherits="LiveQA_LiveQAList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>

    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdSQA" />
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                            </div>
                        </div>
                    </div>

                    <asp:HiddenField ID="hdPID" runat="server" />
                    <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                    <h3 class="box-title">Live Q & A Sessions</h3>

                </div>
                <div class="box-body">
                    <div id="divsearch" runat="server" class="row" style="margin-bottom: 10PX;">
                        <div class="col-md-5">
                            <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>
                        </div>
                        <div class="col-md-1">
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />
                        </div>
                        <div class="col-md-6">
                            <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>

                    </div>
                    <div id="divlist" runat="server" visible="true" class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdSQA" runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="Small" OnItemCommand="grdSQA_ItemCommand"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdSQA_NeedDSource" MasterTableView-AllowSorting="true" AllowPaging="true" PageSize="100" OnPageIndexChanged="grdSQA_PageIndexChanged">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="False" DataKeyNames="c_ID" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="true" />
                                        <Columns>
                                            <telerik:GridTemplateColumn DataField="SrNo" HeaderText="SrNo" UniqueName="SrNo" HeaderStyle-Width="5%" HeaderStyle-Font-Bold="true">
                                                <ItemTemplate>
                                                    <%# Container.ItemIndex + 1  %>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridHyperLinkColumn HeaderStyle-Font-Bold="true" HeaderText="Session Title" DataTextField="c_title"  DataNavigateUrlFields="c_ID" DataNavigateUrlFormatString="LiveQADetailList.aspx?sID={0}" UniqueName="HyperLinkColumn">
                                            </telerik:GridHyperLinkColumn>

                                            <telerik:GridTemplateColumn DataField="c_ID" HeaderText="QRCode" UniqueName="QRCode" 
                                                HeaderStyle-Width="25%" HeaderStyle-Font-Bold="true" Exportable="true">
                                                <ItemTemplate>
                                                    <a target="_blank" href='<%# Constant.URL + "QRImg/" + Eval("c_ID").ToString() + ".Png" %>'><%# getQRCode(Eval("c_ID").ToString())%></a>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix" HeaderStyle-Font-Bold="true" UniqueName="Actions"
                                                HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkShow" runat="server" class="btn btn-info btn-sm" CommandName="Show" CommandArgument='<%#Eval("c_ID") %>' OnClick="linkshow_Click"> Show</asp:LinkButton>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>



