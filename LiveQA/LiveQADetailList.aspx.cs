﻿using System;
using App.DAL;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class LiveQA_LiveQADetailList : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["id"] != null)
            {                

                BindControlData();

                grdSQADetail.ExportSettings.IgnorePaging = true;
                grdSQADetail.ExportSettings.ExportOnlyData = true;
                grdSQADetail.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
                grdSQADetail.ExportSettings.FileName = "LiveQ&A";
            }
            else
            {
                Response.Redirect(Constant.URL + "login.aspx");
            }
        }
    }
    private void BindControlData()
    {
        try
        {
            //DataTable dtmenu = daldata.selectFloorplanMenu();

            //ddlmenu.DataSource = dtmenu;
            //ddlmenu.DataTextField = "m_TitleEn";
            //ddlmenu.DataValueField = "m_id";
            //ddlmenu.DataBind();
            //ListItem lst = new ListItem("Select Menu", "");
            //ddlmenu.Items.Insert(ddlmenu.Items.Count - ddlmenu.Items.Count, lst);
        }
        catch (Exception ex) { }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                string SessionID = Request.QueryString["sID"];
                lblerr.Text = string.Empty;
                DataTable dt = new DataTable();
                dt.Columns.Add("LQAID");
                dt.Columns.Add("SessionID");
                dt.Columns.Add("LQATitle");
                dt.Columns.Add("LQAStatus");
                dt.Columns.Add("LQANum");
                dt.Columns.Add("dtime");
                dt.Columns.Add("QAType");
                dt.Columns.Add("UserReference");
                string datestr = daldata.replaceForSQL(txtQtitle.Text.Trim());

                if (hfid.Value != "")
                {

                    //Update
                    DataRow dr = dt.NewRow();
                    string LQAID = hfid.Value.ToString();
                    dr["LQAID"] = LQAID;
                    dr["SessionID"] = SessionID;
                    dr["LQATitle"] = datestr;
                    dr["LQAStatus"] = 1;
                    dr["LQANum"] = 1;
                    dr["dtime"] = DateTime.Now;
                    dr["QAType"] = 1;
                    dr["UserReference"] = SessionID;


                    int res = daldata.UpdateLQATitle(dr);
                    if (res == 1)
                    {
                        grdSQADetail_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Update Success !";

                        clearControls();
                    }
                }
                else
                {
                    //Insert
                    DataRow dr = dt.NewRow();
                    dr["LQAID"] = "";
                    dr["SessionID"] = SessionID;
                    dr["LQATitle"] = datestr;
                    dr["LQAStatus"] = 1;
                    dr["LQANum"] = 1;
                    dr["dtime"] = DateTime.Now;
                    dr["QAType"] = 1;
                    dr["UserReference"] = SessionID;
                    int res = daldata.SaveDayGetID(dr);
                    if (res == 1)
                    {
                        grdSQADetail_NeedDSource("");
                        divadd.Visible = false;
                        divsearch.Visible = true;
                        divlist.Visible = true;
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Save Success !";

                        clearControls();
                    }
                }
            }
            else
            {
                lblerr.Text = "**Required";
            }
        }
        catch (Exception ex) { }
    }
    private bool Validation()
    {
        bool rtn = true;
        try
        {
            if (string.IsNullOrEmpty(txtQtitle.Text.Trim()))
            {
                rtn = false;
            }

        }
        catch (Exception ex) { }
        return rtn;

    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        this.grdSQADetail.ExportSettings.ExportOnlyData = true;
        this.grdSQADetail.ExportSettings.IgnorePaging = true;
        this.grdSQADetail.ExportSettings.OpenInNewWindow = true;
        this.grdSQADetail.MasterTableView.ExportToExcel();

        grdSQADetail.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        grdSQADetail.ExportSettings.FileName = "Day";
    }
    //protected void lnkAdd_OnClick(object sender, EventArgs e)
    //{
    //    divadd.Visible = true;
    //    divlist.Visible = false;
    //    divsearch.Visible = false;

    //    clearControls();
    //}
    protected void grdSQADetail_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string SessionID = Request.QueryString["sID"];
            DataTable dt = new DataTable();
            dt = daldata.selectQADetail(SessionID);
            grdSQADetail.DataSource = dt;
            Session["dt"] = dt;


        }
        catch (Exception ex) { }
    }
    protected void grdSQADetail_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        Session["pgno"] = grdSQADetail.CurrentPageIndex + 1;
    }
    protected void grdSQADetail_NeedDSource(string searchkey)
    {
        try
        {
            string SessionID = Request.QueryString["sID"];
            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(searchkey))
            {
                dt = daldata.getSQADetailbyKeywords(searchkey, SessionID);
            }
            else
            {
                dt = daldata.selectQADetail(SessionID);
            }


            grdSQADetail.DataSource = dt;
            grdSQADetail.DataBind();
        }
        catch (Exception ex) { }
    }
    protected void grdSQADetail_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {

        if (e.CommandName == "ExportToExcel")
        {
            RadGrid rg = (RadGrid)sender;
            rg.MasterTableView.GetColumn("Actions").Display = false;
            rg.MasterTableView.GetColumn("SrNo").Display = true;
            this.grdSQADetail.ExportSettings.ExportOnlyData = true;
            this.grdSQADetail.ExportSettings.IgnorePaging = true;
            this.grdSQADetail.ExportSettings.OpenInNewWindow = true;
            this.grdSQADetail.MasterTableView.ExportToExcel();
        }

    }
    protected void grdSQADetail_ItemCreated(object sender, Telerik.Web.UI.GridItemEventArgs e)
    {

        if (e.Item is GridDataItem)
        {
            GridDataItem gridItem = e.Item as GridDataItem;
            foreach (GridColumn column in grdSQADetail.MasterTableView.RenderColumns)
            {
                if (column is GridBoundColumn)
                {
                    //this line will show a tooltip based on the CustomerID data field
                    string id = gridItem.OwnerTableView.DataKeyValues[gridItem.ItemIndex]["LQAID"].ToString();
                    string type = daldata.getQATypeByID(id);
                    if (type == "0")
                    {
                        gridItem["show"].ToolTip = "Show on screen";
                    }else
                    {
                        gridItem["show"].ToolTip = "Don't Show on screen";
                    }

                    //This is in case you wish to display the column name instead of data field.
                    //gridItem[column.UniqueName].ToolTip = "Tooltip: " + column.UniqueName;
                }
            }
        }
    }
    protected void lnkShow_Onclick(object sender, EventArgs e)
    {
        try
        {

            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                string type = string.Empty;
                string QAType = daldata.getQATypeByID(id);
                if (QAType == "1")
                {
                    type = "0";
                    daldata.UpdateLQAType(id, type);
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Don't Show on Screen !";
                }
                else
                {
                    type = "1";
                    daldata.UpdateLQAType(id, type);
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Show on Screen !";
                }
              
                grdSQADetail_NeedDSource("");
            }
        }
        catch (Exception ex)
        {
            noti.Visible = true;
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
            msg.Text = "Fail !";
        }
    }
    protected void lnkApprove_Onclick(object sender, EventArgs e)
    {
        try
        {

            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                int chk = 0;
                if (chk == 0)
                {
                    string status = "1";
                    int res = daldata.UpdateStatus(id, status);
                    if (res == 1)
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Successfully Approved !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdSQADetail_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void lnkReject_Click(object sender, EventArgs e)
    {
        try
        {

            LinkButton del = sender as LinkButton;
            if (del.CommandName == "Declined")
            {
                string ID = del.CommandArgument.ToString();
                int chk = 0;
                if (chk == 0)
                {
                    string status = "2";
                    int res = daldata.UpdateStatus(ID, status);
                    if (res == 1)
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                        msg.Text = "Successfully Declined !";
                    }
                    else
                    {
                        noti.Visible = true;
                        noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                        msg.Text = "Fail !";
                    }
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                    msg.Text = "This item is used. CANNOT BE DELETED !";
                }

                grdSQADetail_NeedDSource("");
            }
        }
        catch (Exception ex) { }
    }
    protected void linkedite_Click(object sender, EventArgs e)
    {
        try
        {
            divadd.Visible = true;
            divlist.Visible = false;
            divsearch.Visible = false;
            string id = ((LinkButton)sender).CommandArgument;
            if (!string.IsNullOrEmpty(id))
            {
                DataTable dt = daldata.getQAlstByID(id);
                if (dt.Rows.Count > 0)
                {
                    string qtitle = dt.Rows[0]["LQATitle"].ToString();
                    hfid.Value = dt.Rows[0]["LQAID"].ToString();
                    txtQtitle.Text = qtitle;
                    //ddlmenu.SelectedValue = dt.Rows[0]["menuID"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
            {
                grdSQADetail_NeedDSource(txtKeyword.Text.Trim());
            }
        }
        catch (Exception ex) { }
    }
    //private string UploadIcon(string filename, string extension, string path, ref string message)
    //{
    //    string imagefilename = string.Empty; string messages = string.Empty;
    //    try
    //    {
    //        if (extension != ".png" && extension != ".jpg" && extension != ".gif" && extension != ".jpeg")
    //        {
    //            messages = "Invalid Image Type";
    //        }

    //        string fname = filename + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + extension;

    //        string CreatePath = System.Web.HttpContext.Current.Server.MapPath(path).ToString();
    //        if (!System.IO.Directory.Exists(CreatePath))
    //        {
    //            System.IO.Directory.CreateDirectory(CreatePath);
    //        }

    //        string savePath = Server.MapPath((path) + fname);
    //        fupForm.SaveAs(savePath);
    //        imagefilename = fname;
    //    }
    //    catch (Exception ex) { }
    //    return imagefilename;
    //}
    private void clearControls()
    {
        hfid.Value = "";
        txtKeyword.Text = "";
        txtQtitle.Text = "";
    }

    public string GetStatus(string no)

    {
        string res = string.Empty;
        if (no == "0")
        {
            res = "Pending";

        }
        else if (no == "1")
        {
            res = "Approved";
        }
        else
        {
            res = "Declined";
        }
        return res;
    }

    public bool checkApproveStatus(string no)

    {
        bool res = true;
        if (no == "1")
        {
            res = false;
        }
        return res;
    }
    public bool checkDeclinedStatus(string no)

    {
        bool res = true;
        if (no == "2")
        {
            res = false;
        }
        return res;
    }
    public bool checkShowStatus(string no)

    {
        bool res = true;
        if (no == "0" || no == "2")
        {
            res = false;
        }
        return res;
    }
    public string checkShowCSS(string no)

    {
        string res = string.Empty;
        if (no == "1")
        {
            res = "btn btn-info btn-sm";
        }
        else if (no == "0")
        {
            res = "btn btn-default btn-sm";
        }
       
        return res;
    }
}