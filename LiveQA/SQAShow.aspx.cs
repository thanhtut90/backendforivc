﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class SQAShow : System.Web.UI.Page
{
    protected string meetid;
    protected string showid = "MobCET2019";
    string qrpath = "~/QRImg/";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["meetid"] != null)
        {
            meetid = Request.QueryString["meetid"].ToString();

            #region make qrcode
            string currentUrl = Constant.URL + "apiv1/LiveQA?SessionID=" + meetid;
            GenQRCode gen_qr = new GenQRCode();
            string gen_qrcodeUrl = gen_qr.genQROnlyOneParam(currentUrl, meetid, qrpath);
            //QRcodeMaker qMaker = new QRcodeMaker();
            //string gen_qrcodeUrl = qMaker.CreateQRcodeOnlyOneParam(currentUrl, showid);
            imgQRCode.Visible = false;
            if (!string.IsNullOrEmpty(gen_qrcodeUrl))
            {
                //if (IsUrlExist(gen_qrcodeUrl))
                if(File.Exists(Server.MapPath(gen_qrcodeUrl)))
                {
                    imgQRCode.Visible = true;
                    imgQRCode.ImageUrl = gen_qrcodeUrl;
                }
            }
            #endregion
        }
    }

    public bool IsUrlExist(string url, int timeOutMs = 1000)
    {
        WebRequest webRequest = WebRequest.Create(url);
        webRequest.Method = "HEAD";
        webRequest.Timeout = timeOutMs;

        try
        {
            var response = webRequest.GetResponse();
            /* response is `200 OK` */
            response.Close();
        }
        catch (Exception ex)
        {
            /* Any other response */
            return false;
        }

        return true;
    }
}