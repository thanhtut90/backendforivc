﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using App.DAL;
using App.Common;
using System.Data.Entity;
using Telerik.Web.UI;

public partial class LiveQASummaryReport : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            GKeyMaster.ExportSettings.IgnorePaging = true;
            GKeyMaster.ExportSettings.ExportOnlyData = true;
            GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
            GKeyMaster.ExportSettings.FileName = "LiveQASummaryReport";
        }
    }
    protected void GKeyMaster_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dtResult = getPollingData();
            GKeyMaster.DataSource = dtResult;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    private DataTable getPollingData()
    {
        DataTable dtResult = new DataTable();
        try
        {
            dtResult.Columns.Add("Session Title");
            dtResult.Columns.Add("Date");
            dtResult.Columns.Add("Time");
            dtResult.Columns.Add("Question");
            dtResult.Columns.Add("Vote Count");
            string sqlList = "Select * From LQA as qa Inner Join tblConference as c On qa.SessionID=c.c_ID Where c.deleteFlag=0";
            DataTable dtList = fn.GetDatasetByCommand(sqlList, "ds").Tables[0];
            if (dtList.Rows.Count > 0)
            {
                foreach (DataRow dr in dtList.Rows)
                {
                    string sessionID = dr["c_ID"].ToString();
                    string ConfTitle = dr["c_title"].ToString();
                    string ConfDate = dr["c_date"].ToString();
                    string ConfTime = dr["c_stime"].ToString() + "-" + dr["c_etime"].ToString();
                    string QTitle = dr["LQATitle"].ToString();
                    string VoteCount = dr["LQANum"].ToString();
                    DataRow drR = dtResult.NewRow();
                    drR["Session Title"] = ConfTitle;
                    drR["Date"] = ConfDate;
                    drR["Time"] = ConfTime;
                    drR["Question"] = QTitle;
                    drR["Vote Count"] = VoteCount;
                    dtResult.Rows.Add(drR);
                }
            }
        }
        catch(Exception ex)
        { }

        return dtResult;
    }
    protected void GKeyMaster_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        if (e.CommandName == "ExportToExcel")
        {
            this.GKeyMaster.ExportSettings.ExportOnlyData = true;
            this.GKeyMaster.ExportSettings.IgnorePaging = true;
            this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
            this.GKeyMaster.MasterTableView.ExportToExcel();
        }
    }
    protected void GKeyMaster_PageIndexChanged(object sender, GridPageChangedEventArgs e)
    {
        Session["pgno"] = GKeyMaster.CurrentPageIndex + 1;
    }
    protected void lnkDownload_Click(object sender, EventArgs e)
    {
        this.GKeyMaster.ExportSettings.ExportOnlyData = true;
        this.GKeyMaster.ExportSettings.IgnorePaging = true;
        this.GKeyMaster.ExportSettings.OpenInNewWindow = true;
        this.GKeyMaster.MasterTableView.ExportToExcel();
        GKeyMaster.ExportSettings.Excel.Format = (GridExcelExportFormat)Enum.Parse(typeof(GridExcelExportFormat), "Xlsx");
        GKeyMaster.ExportSettings.FileName = "LiveQASummaryReport";
    }
    protected void GKeyMaster_PreRender(object sender, EventArgs e)
    {
        for (int rowIndex = GKeyMaster.Items.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridDataItem row = GKeyMaster.Items[rowIndex];
            GridDataItem previousRow = GKeyMaster.Items[rowIndex + 1];
            if (row["Session Title"].Text == previousRow["Session Title"].Text)
            {
                row["Session Title"].RowSpan = previousRow["Session Title"].RowSpan < 2 ? 2 : previousRow["Session Title"].RowSpan + 1;
                previousRow["Session Title"].Visible = false;
            }
            if (row["Date"].Text == previousRow["Date"].Text)
            {
                row["Date"].RowSpan = previousRow["Date"].RowSpan < 2 ? 2 : previousRow["Date"].RowSpan + 1;
                previousRow["Date"].Visible = false;
            }
            if (row["Time"].Text == previousRow["Time"].Text)
            {
                row["Time"].RowSpan = previousRow["Time"].RowSpan < 2 ? 2 : previousRow["Time"].RowSpan + 1;
                previousRow["Time"].Visible = false;
            }
        }
    }
}