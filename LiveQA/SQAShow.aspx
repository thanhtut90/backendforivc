﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SQAShow.aspx.cs" Inherits="SQAShow" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
   <%-- <link href="../Content/backlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Content/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="Content/js/jquery-2.1.3.min.js" type="text/javascript"></script>--%>
    <link href="../Content/backlayout.css" rel="stylesheet" type="text/css" />
    <link href="../Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style type="text/css" >
    .timebox { min-height:80px; overflow: hidden; position:relative; margin-bottom: 10px; font-family:Arial;}
    .fl{ float:left; width:160px; position:absolute; left:0px;top:0px; z-index:2;}
    .fr{ float:left; margin-left:120px;}
    .timebox span { background: #52b7c9; color: #af2295;text-align: center;  padding:5px 10px;  display: inline-block; }
    .timebox b { font-size: 22px; }
    
    .square2{ width:100%;min-height:285px; margin:100px auto;background:#fff;padding:8px 8px 8px 8px ; float:left;margin:10px;border:solid 5px #b2b2b2;}
    .square2 .sq2 {width:100%;min-height:240px; color:#222; font-size:24px; text-align:left;padding:0px 0px 0px;line-height:30px; position:relative; overflow:hidden;}/*background:#808285;padding:20px 20px 0px;*/
    .square2 span.time { position:absolute; left:20px;bottom:20px;}
    </style>
    <script type="text/javascript" src="../Content/plugins/jQuery/jQuery-2.2.0.min.js"></script>

    <script type="text/javascript">
         
       $(function () {           
           timestamp = 0;
           updateMsg();
       })
       function updateMsg() {           
           $.post("../Handle/msgshow.ashx?meetid=" + $("#meetid").val() + "&LID=", {
               time: timestamp
           }, function (xml) {
               $("#loading").remove();

               addMessages(xml);
           });
           setTimeout('updateMsg()', 4000);
       }
        

       function addMessages(xml) {
         if ($("status", xml).text() == "2") return;
         timestamp = $("time", xml).text();
         var htmlcode = "";
         var i = 0;
         $("message", xml).each(function () {
             i = i + 1;
             var QAID = $("QAID", this).text();
             var SessionID = $("SessionID", this).text();
             var QATitle = $("QATitle", this).text();
             var QANum = "";

             if (QANum > 1) {
                 QANum += " Votes: ";
             }
             else {
                 QANum += " Vote: ";
             }

             QANum += $("QANum", this).text();
             
             htmlcode += "<div class='col-md-6'><a href='SQADetail.aspx?QAID=" + QAID + "' target='_blank'><div class='square2 clearfix'>";
             htmlcode += "<div class='sq2'>";
             htmlcode += "" + i + ".  " + QATitle + "";
             htmlcode += "<br/></div><div style='text-align:right;color:#222;'> " + QANum + "";
             htmlcode += "</div>";
             
             

             htmlcode += "</div></a></div>";
         });
         $("#messagewindow").html(htmlcode);
     }
    </script>
</head>
<body>
    <form id="frm" runat="server">
        <div style="padding-left:0px;padding-right:0px;">
            <img alt="header" src="../sys_Images/CET_QABanner.png" class="img-responsive"/>
        </div>
        <div class="container">
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <section class="content">
                        <div class="row">
                            <div id="squarebox" style="clear:both;height:1px; margin-top:-1px; width: 85%;">
                                <div id="messagewindow"><span id="loading">Loading...</span></div>
                                <input type="hidden" name="meetid" id="meetid" value="<%=meetid%>" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div style="display:none;">
            <br />
            <div class="row">
                <div class="col-sm-12">
                    <section class="content">
                        <br /><br />
                        <div class="row pull-right">
                            <div class="col-md-12 col-sm-12">
                                <asp:Image ID="imgQRCode" runat="server" ImageUrl="#" CssClass="img-responsive" Width="250" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
