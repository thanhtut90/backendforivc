﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using App.DAL;

public partial class ExhibitorScanList : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    DAL_DataIScan daliScan = new DAL_DataIScan();
    protected void Page_Load(object sender, EventArgs e)
    {
        Hashtable ht = new Hashtable();
        ArrayList eventList = new ArrayList();
        if (!IsPostBack)
        {
            if (Request.QueryString["Regno"] != null)//Request.Form["Regno"] != null
            {
                string Regno = Request.QueryString["Regno"].ToString();
                try
                {
                    List<ExhibitorProductObj> lstExhObj = getExhibitorScanListByExhScanDeviceCode(Regno);
                    ht.Add("data", lstExhObj);
                    ht.Add("status", "Success");
                    eventList.Add(ht);
                }
                catch
                {
                    List<ExhibitorProductObj> lstExhObj = new List<ExhibitorProductObj>();
                    ht.Add("data", lstExhObj);
                    ht.Add("status", "Error");
                    eventList.Add(ht);
                }
            }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(eventList);
            Response.Write(jsonStr);
        }
    }

   
    public string FilterSpecial(string str)//特殊字符过滤函数
    {
        if (str == "") //如果字符串为空，直接返回。
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");

            return str;
        }
    }

    private List<ExhibitorProductObj> getExhibitorScanListByExhScanDeviceCode(string Regno)
    {
        List<ExhibitorProductObj> lstExhObj = new List<ExhibitorProductObj>();
        try
        {
            DataSet ds = new DataSet();
            ds = fn.GetDatasetByCommand("Select * From tblVisitorExhibitorProduct Where Regno='" + Regno + "'", "ds");
            DataTable distinctTable = ds.Tables[0];
            distinctTable = RemoveDuplicateRows(distinctTable, "ExhName", "ExhEmail");
            if(distinctTable.Rows.Count > 0)
            {
                foreach (DataRow dr in distinctTable.Rows)
                {
                    ExhibitorProductObj exObj = new ExhibitorProductObj();
                    exObj.ExhName = dr["ExhName"] != DBNull.Value ? dr["ExhName"].ToString() : "";
                    exObj.ExhCountry = dr["ExhCountry"] != DBNull.Value ? dr["ExhCountry"].ToString() : "";
                    exObj.ExhContactPerson = dr["ExhContactPerson"] != DBNull.Value ? dr["ExhContactPerson"].ToString() : "";
                    exObj.ExhEmail = dr["ExhEmail"] != DBNull.Value ? dr["ExhEmail"].ToString() : "";
                    exObj.ExhBoothNo = dr["ExhBoothNo"] != DBNull.Value ? dr["ExhBoothNo"].ToString() : "";
                    exObj.ExhProductName = dr["ExhProductName"] != DBNull.Value ? dr["ExhProductName"].ToString() : "";
                    lstExhObj.Add(exObj);
                }
            }
        }
        catch(Exception ex)
        { }

        return lstExhObj;
    }
    public DataTable RemoveDuplicateRows(DataTable dTable, string ExhName, string ExhEmail)
    {
        Hashtable hTable = new Hashtable();
        ArrayList duplicateList = new ArrayList();

        string Visitorname = "";
        foreach (DataRow drow in dTable.Rows)
        {
            if ((hTable.Contains(drow[ExhName])) && (hTable.Contains(drow[ExhEmail])))
                duplicateList.Add(drow);
            else
            {
                hTable.Remove(drow[ExhName]);
                hTable.Remove(drow[ExhEmail]);
                hTable.Add(drow[ExhName], string.Empty);
                hTable.Add(drow[ExhEmail], string.Empty);
                Visitorname = ExhName.ToString();
            }
        }

        foreach (DataRow dRow in duplicateList)
            dTable.Rows.Remove(dRow);

        return dTable;
    }

}
