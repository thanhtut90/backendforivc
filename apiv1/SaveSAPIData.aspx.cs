﻿using System;
using System.Linq;
using App.DAL;
using System.Collections;
using System.Data;
using System.Web.Script.Serialization;

public partial class apiv1_SaveSAPIData : System.Web.UI.Page
{
    Functionality dl = new Functionality();
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        //ArrayList eventList = new ArrayList();
        Hashtable htt = new Hashtable();
        if (!IsPostBack)
        {
            string name = string.Empty;
            string date = string.Empty;
            string time = string.Empty;

            if (this.Request.QueryString["Name"] != null && this.Request.QueryString["Date"] != null && this.Request.QueryString["Time"] != null)
            {
                name = this.Request.QueryString["Name"].ToString();
                date = this.Request.QueryString["Date"].ToString();
                time = this.Request.QueryString["Time"].ToString();
            }
            if (this.Request.Form["Name"] != null && this.Request.Form["Date"] != null && this.Request.Form["Time"] != null)
            {
                name = this.Request.Form["Name"].ToString();
                date = this.Request.Form["Date"].ToString();
                time = this.Request.Form["Time"].ToString();
            }

            if(!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(date) && !string.IsNullOrEmpty(time))
            {
                int isSuccess = 0;
                try
                {
                    string sql1 = "insert into tb_LogSAPIData (Name,Date,Time) values ('"
                                    + daldata.replaceForSQL(name) + "','" + daldata.replaceForSQL(date) + "','" + daldata.replaceForSQL(time) + "')";
                    dl.ExecuteSQL(sql1);
                    isSuccess = 1;

                    htt = new Hashtable();
                    htt.Add("Status", "200");
                    htt.Add("message", "Success");
                    //eventList.Add(htt);
                }
                catch(Exception ex)
                {
                    htt = new Hashtable();
                    htt.Add("Status", "406");
                    htt.Add("message", "Error");
                    //eventList.Add(htt);
                }
            }
            else
            {
                htt = new Hashtable();
                htt.Add("Status", "503");
                htt.Add("message", "Empty data");
                //eventList.Add(htt);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        String jsonStr = ser.Serialize(htt);//eventList
        Response.Write(jsonStr);
    }
}