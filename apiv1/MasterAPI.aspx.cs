﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Common;
using App.DAL;
using App.Object;
using System.Web.Script.Serialization;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

public partial class apiv1_MasterAPI : System.Web.UI.Page
{
    //IsabellaFormEntities db = new IsabellaFormEntities();
    //DAL_FrontEnd dalf = new DAL_FrontEnd();
    //DAL_Child dalc = new DAL_Child();
    //DAL_Parent dalp = new DAL_Parent();
    DAL dal = new DAL();
    DAL_Data daldata = new DAL_Data();
    GenKey gk = new GenKey();
    Functionality fn = new Functionality();
    FunctionalityISan fnIScan = new FunctionalityISan();
    DAL_DataIScan daliScan = new DAL_DataIScan();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            ArrayList rtnlist = new ArrayList();
            Hashtable htt = new Hashtable();
            string page = string.Empty;
            string langcode = string.Empty;
            int lang = 0;
            if (Request.QueryString["Page"]!=null && Request.QueryString["Lang"] !=null)
            {
                page = Request.QueryString["Page"].ToString();
                langcode = Request.QueryString["Lang"].ToString();
            }
            if (Request.Form["Page"] != null && Request.Form["Lang"] != null)
            {
                page = Request.Form["Page"].ToString();
                langcode = Request.Form["Lang"].ToString();
            }
            lang = dal.getLangIDBycode(langcode);
            string sql = string.Empty;

            int isExistAPI = 0;

            #region DataVersion
            if (page == "dataversion")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select page,a_dataVersion from tblActivatedForms where deleteFlag=0";

                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("Page", dr["page"].ToString());
                        ht.Add("dataVersion", dr["a_dataVersion"].ToString());                      

                        rtnlist.Add(ht);
                    }                    
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Day
            if (page == "Day")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblDay where deleteFlag=0";                
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='"+page+"'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("d_ID", dr["d_ID"].ToString());
                        ht.Add("d_key", dr["d_key"].ToString());
                        ht.Add("d_date", dr["d_date"].ToString());
                        ht.Add("d_year", dr["d_year"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region NewsType
            else if (page == "NewsType")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblNewsType where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("nt_ID", dr["nt_ID"].ToString());
                        ht.Add("nt_name", dr["nt_name"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region SpeakerCategory
            else if (page == "SpeakerCategory")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblSpeakerCategory where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("speakerCat_ID", dr["speakerCat_ID"].ToString());
                        ht.Add("speakerCat_name", dr["speakerCat_name"].ToString());
                        ht.Add("speakerCat_seq", dr["speakerCat_seq"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region SponsorCategory
            else if (page == "SponsorCategory")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblSponsorCategory where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("sponsorCat_ID", dr["sponsorCat_ID"].ToString());
                        ht.Add("sponsorCat_name", dr["sponsorCat_name"].ToString());
                        ht.Add("sponsorCat_seq", dr["sponsorCat_seq"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ConferenceCategory
            else if (page == "ConferenceCategory")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblConferenceCategory where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("cc_ID", dr["cc_ID"].ToString());
                        ht.Add("cc_categoryname", dr["cc_categoryname"].ToString());
                        ht.Add("cc_seq", dr["cc_seq"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region LocationFloorplan
            else if (page == "LocationFloorplan")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblLocation_Floorplan where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("l_ID", dr["l_ID"].ToString());
                        ht.Add("l_name", dr["l_name"].ToString());
                        ht.Add("l_image", !string.IsNullOrEmpty(dr["l_image"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["l_image"].ToString() : "");
                        ht.Add("menuID", dr["menuID"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ProductCategory
            else if (page == "ProductCategory")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblProductCategory where deleteFlag=0 Order By sortorder";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("pc_ID", dr["pc_ID"].ToString());
                        ht.Add("pc_name", dr["pc_name"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region TimeList
            else if (page == "TimeList")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblTimeList where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("tl_ID", dr["tl_ID"].ToString());
                        ht.Add("tl_slot", dr["tl_slot"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Account
            else if (page == "Account")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblAccount where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("a_ID", dr["a_ID"].ToString());
                        ht.Add("a_fullname", dr["a_fullname"].ToString());
                        ht.Add("a_type", dr["a_type"].ToString());
                        ht.Add("a_sal", dr["a_sal"].ToString());
                        ht.Add("a_fname", dr["a_fname"].ToString());
                        ht.Add("a_lname", dr["a_lname"].ToString());
                        ht.Add("a_designation", dr["a_designation"].ToString());
                        ht.Add("a_email", dr["a_email"].ToString());
                        ht.Add("a_company", dr["a_company"].ToString());
                        ht.Add("a_addressOfc", dr["a_addressOfc"].ToString());
                        ht.Add("a_addressHome", dr["a_addressHome"].ToString());
                        ht.Add("a_country", dr["a_country"].ToString());
                        ht.Add("a_Tel", dr["a_Tel"].ToString());
                        ht.Add("a_Mobile", dr["a_Mobile"].ToString());
                        ht.Add("a_loginName", dr["a_loginName"].ToString());
                        //ht.Add("a_Password", dr["a_Password"].ToString());
                        ht.Add("a_isActivated", dr["a_isActivated"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());
                        ht.Add("a_AttendDay", dr["a_AttendDay"] != DBNull.Value ? dr["a_AttendDay"].ToString() : "");

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ContactList
            else if (page == "ContactList")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblAccount where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        string userid = dr["a_ID"].ToString();
                        string a_type = dr["a_type"] != DBNull.Value ? dr["a_type"].ToString() : "";
                        string a_fullname = dr["a_fullname"] != DBNull.Value ? dr["a_fullname"].ToString() : "";
                        string a_fname = dr["a_fname"] != DBNull.Value ? dr["a_fname"].ToString() : "";
                        string a_lname = dr["a_lname"] != DBNull.Value ? dr["a_lname"].ToString() : "";
                        string a_sal = dr["a_sal"] != DBNull.Value ? dr["a_sal"].ToString() : "";
                        string a_designation = dr["a_designation"] != DBNull.Value ? dr["a_designation"].ToString() : "";
                        string a_email = dr["a_email"] != DBNull.Value ? dr["a_email"].ToString() : "";
                        string a_company = dr["a_company"] != DBNull.Value ? dr["a_company"].ToString() : "";
                        string a_addressOfc = dr["a_addressOfc"] != DBNull.Value ? dr["a_addressOfc"].ToString() : "";
                        string a_addressHome = dr["a_addressHome"] != DBNull.Value ? dr["a_addressHome"].ToString() : "";
                        string a_country = dr["a_country"] != DBNull.Value ? dr["a_country"].ToString() : "";
                        string a_Tel = dr["a_Tel"] != DBNull.Value ? dr["a_Tel"].ToString() : "";
                        string a_Mobile = dr["a_Mobile"] != DBNull.Value ? dr["a_Mobile"].ToString() : "";
                        string a_loginName = dr["a_loginName"] != DBNull.Value ? dr["a_loginName"].ToString() : "";
                        string a_isActivated = dr["a_isActivated"] != DBNull.Value ? dr["a_isActivated"].ToString() : "";
                        string a_AttendDay = dr["a_AttendDay"] != DBNull.Value ? dr["a_AttendDay"].ToString() : "";
                        string a_profilepic = dr["a_profilepic"] != DBNull.Value ? dr["a_profilepic"].ToString() : "";
                        //string a_Password = dr["a_Password"] != DBNull.Value ? dr["a_Password"].ToString() : "";
                        ht.Add("UserID", userid);
                        ht.Add("UserType", a_type);
                        ht.Add("FullName", a_fullname);
                        ht.Add("FirstName", a_fname);
                        ht.Add("LastName", a_lname);
                        ht.Add("Salutation", a_sal);
                        ht.Add("Designation", a_designation);
                        ht.Add("Email", a_email);
                        ht.Add("Company", a_company);
                        ht.Add("AddressOfCompany", a_addressOfc);
                        ht.Add("AddressHome", a_addressHome);
                        ht.Add("Country", a_country);
                        ht.Add("Tel", a_Tel);
                        ht.Add("Mobile", a_Mobile);
                        //ht.Add("LoginName", a_loginName);
                        ht.Add("isActivated", a_isActivated);
                        ht.Add("AttendDay", a_AttendDay);
                        ht.Add("PhotoUrl", !string.IsNullOrEmpty(a_profilepic) ? Constant.URL + Constant.filesavingurlview + a_profilepic : "");

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region News
            else if (page == "News")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblNews where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("n_ID", dr["n_ID"].ToString());
                        ht.Add("n_title", dr["n_title"].ToString());
                        ht.Add("n_content", dr["n_content"].ToString());
                        ht.Add("n_picture", dr["n_picture"].ToString());
                        ht.Add("menuID", dr["menuID"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Sponsor
            else if (page == "Sponsor")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblSponsor as s Left Join tblSponsorCategory as sc On s.s_category=sc.sponsorCat_ID where s.deleteFlag=0";//"select * from tblSponsor where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("s_ID", dr["s_ID"].ToString());
                        ht.Add("s_exhID", dr["s_exhID"].ToString());
                        ht.Add("menuID", dr["menuID"].ToString());
                        ht.Add("s_seq", dr["s_seq"].ToString()); 
                        ht.Add("s_category", daldata.getsponsorcategoryNameByID(dr["s_category"].ToString()));
                        ht.Add("s_name", dr["s_name"].ToString());
                        ht.Add("s_logo", !string.IsNullOrEmpty(dr["s_logo"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["s_logo"].ToString() : "");
                        ht.Add("s_content", dr["s_content"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());
                        ht.Add("s_categorysequence", dr["sponsorCat_seq"] != DBNull.Value ? dr["sponsorCat_seq"].ToString() : "");

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Conference
            else if (page == "Conference")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select tblConference.*,tblConferenceCategory.cc_seq from tblConference Left Join tblConferenceCategory On tblConference.c_cc=tblConferenceCategory.cc_ID where tblConference.deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();
                        ht.Add("c_ID", dr["c_ID"].ToString());
                        ht.Add("c_cc", dr["c_cc"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["c_cc"].ToString()) ? (daldata.getConferenceCategoryNamebyID(dr["c_cc"].ToString())) : "") : "");
                        ht.Add("c_date", dr["c_date"].ToString());
                        ht.Add("c_stime", dr["c_stime"].ToString());
                        ht.Add("c_title", dr["c_title"].ToString());
                        ht.Add("c_venue", dr["c_venue"].ToString());
                        ht.Add("c_liveQA", dr["c_liveQA"] != DBNull.Value ? dr["c_liveQA"].ToString() : "0");
                        ht.Add("c_Polling", dr["c_Polling"] != DBNull.Value ? dr["c_Polling"].ToString() : "0");
                        ht.Add("c_Survey", dr["c_Survey"] != DBNull.Value ? dr["c_Survey"].ToString() : "0");
                        ht.Add("c_brief", dr["c_brief"].ToString());
                        ht.Add("c_parentconfID", dr["c_parentconfID"].ToString());
                        ht.Add("c_etime", dr["c_etime"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());
                        ht.Add("Category Seq", dr["cc_seq"] == DBNull.Value ? "99" : dr["cc_seq"].ToString());

                        string date = dr["c_date"].ToString();
                        sql = "select d_year from tblDay where deleteFlag=0 And d_date='" + date + "'";
                        string year = fn.GetDataByCommand(sql, "d_year");
                        ht.Add("c_year", year);

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Event
            else if (page == "Event")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblEvent where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("e_ID", dr["e_ID"].ToString());
                        ht.Add("e_image", !string.IsNullOrEmpty(dr["e_image"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["e_image"].ToString() : "");
                        ht.Add("e_date", dr["e_date"].ToString());
                        ht.Add("e_time", dr["e_time"].ToString());
                        ht.Add("e_title", dr["e_title"].ToString());
                        ht.Add("e_venue", dr["e_venue"].ToString());
                        ht.Add("e_brief", dr["e_brief"].ToString());
                        ht.Add("e_eventcategory", dr["e_eventcategory"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Venuebooth
            else if (page == "VenueBooth")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblVenue_Booth where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("vb_ID", dr["vb_ID"].ToString());
                        ht.Add("vb_name", dr["vb_name"].ToString());
                        ht.Add("vb_location", dr["vb_location"].ToString());// daldata.getHallByID(dr["vb_location"].ToString()));
                        ht.Add("vb_width", dr["vb_width"].ToString());
                        ht.Add("vb_height", dr["vb_height"].ToString());
                        ht.Add("vb_x", dr["vb_x"].ToString());
                        ht.Add("vb_y", dr["vb_y"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ConferenceSpeakers
            else if (page == "ConferenceSpeakers")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblConferenceSpeakers as cs Inner join tblSpeakerCategory as sc On cs.cspeaker_category=sc.speakerCat_ID where cs.deleteFlag=0 and sc.deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();
                        ht.Add("cspeaker_ID", dr["cspeaker_ID"].ToString());
                        ht.Add("cspeaker_category", dr["speakerCat_name"].ToString());
                        ht.Add("speakerCat_seq", dr["speakerCat_seq"].ToString());
                        ht.Add("cspeaker_conferenceID", dr["cspeaker_conferenceID"].ToString());
                        ht.Add("cspeaker_speakerID", dr["cspeaker_speakerID"].ToString());
                        ht.Add("cspeaker_confcategory", dr["cspeaker_confcategory"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());
                        ht.Add("speaker_sequence", dr["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["sorting"].ToString()) ? dr["sorting"].ToString() : "99") : "99");

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ConferenceSponsors
            else if (page == "ConferenceSponsors")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblConferenceSponsors where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("csponsor_ID", dr["csponsor_ID"].ToString());
                        ht.Add("csponsor_sponsorID", dr["csponsor_sponsorID"].ToString());
                        ht.Add("csponsor_scategory", dr["csponsor_scategory"].ToString());
                        ht.Add("csponsor_conferenceID", dr["csponsor_conferenceID"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region GeneralInfo
            else if (page == "GeneralInfo")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblGeneralInfo where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("gi_ID", dr["gi_ID"].ToString());
                        ht.Add("gi_title", dr["gi_title"].ToString());
                        ht.Add("gi_content", dr["gi_content"].ToString());
                        ht.Add("gi_icon", dr["gi_icon"].ToString());
                        ht.Add("gi_mainID", daldata.getgeneralinfiByID(dr["gi_mainID"].ToString()));// dr["gi_mainID"].ToString());
                        ht.Add("gi_seq", dr["gi_seq"].ToString());
                        ht.Add("menuID", dr["menuID"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());


                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region E-Poster
            else if (page == "EPoster")
            {
                isExistAPI += 1;
                sql = string.Empty;
                //sql = "select * from [tblE-Poster] where deleteFlag=0";
                sql = "select spk.s_fullname speakername,v.vb_name venuName,e.* from [tblE-Poster] as e "
                        + " Left Join tblSpeaker as spk On e.ep_author=spk.s_ID "
                        + " Left Join tblVenue_Booth as v On e.ep_venue=v.vb_ID "
                        + " where e.deleteFlag=0 "
                        + " And spk.deleteFlag=0 And v.deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("ep_ID", dr["ep_ID"].ToString());
                        ht.Add("ep_title", dr["ep_title"].ToString());
                        ht.Add("ep_category", dr["ep_category"].ToString());
                        ht.Add("ep_author", dr["speakername"] != DBNull.Value ? dr["speakername"].ToString() : "");//dr["ep_author"].ToString());
                        ht.Add("ep_coauthor", dr["ep_coauthor"].ToString());
                        ht.Add("ep_venue", dr["venuName"] != DBNull.Value ? dr["venuName"].ToString() : "");//dr["ep_venue"].ToString());
                        ht.Add("ep_duration", dr["ep_duration"].ToString());
                        ht.Add("ep_file", !string.IsNullOrEmpty(dr["ep_file"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["ep_file"].ToString() : "");
                        ht.Add("ep_abstract", dr["ep_abstract"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ConfigurationImages
            else if (page == "ConfigurationImages")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblConfigurationImages where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("img_ID", dr["img_ID"].ToString());
                        ht.Add("img_key", dr["img_key"].ToString());
                        ht.Add("img_file", !string.IsNullOrEmpty(dr["img_file"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["img_file"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region HomePage
            else if (page == "HomePage")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblHomePage where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {                   
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("hp_ID", dr["hp_ID"].ToString());
                        ht.Add("hp_mtitle", dr["hp_mtitle"].ToString());
                        ht.Add("hp_stitle", dr["hp_stitle"].ToString());
                        ht.Add("hp_content", dr["hp_content"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ExhibitingCompany
            else if (page == "ExhibitingCompany")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblExhibitingCompany where deleteFlag=0";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("ec_ID", dr["ec_ID"].ToString());
                        ht.Add("ec_name", dr["ec_name"].ToString());
                        ht.Add("ec_address", dr["ec_address"].ToString());
                        ht.Add("ec_country", dr["ec_country"].ToString());
                        ht.Add("ec_email", dr["ec_email"].ToString());
                        ht.Add("ec_contactno", dr["ec_contactno"].ToString());
                        ht.Add("ec_website", dr["ec_website"].ToString());
                        ht.Add("ec_brochure", dr["ec_brochure"].ToString());
                        ht.Add("ec_logo", dr["ec_logo"].ToString());
                        ht.Add("ec_companyprofile", dr["ec_companyprofile"].ToString());
                        ht.Add("ec_companytype", dr["ec_companytype"].ToString());
                        ht.Add("ec_hall", daldata.getHallByID(dr["ec_hall"].ToString()));
                        ht.Add("ec_booth", dr["ec_booth"].ToString());//daldata.getBoothByID(
                        ht.Add("contactpersonID", dr["contactpersonID"].ToString());
                        ht.Add("ec_product", dr["ec_product"].ToString());
                        ht.Add("ec_productHTML", dr["ec_productHTML"] != DBNull.Value ? dr["ec_productHTML"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        ht.Add("ec_marketseg", dr["ec_marketseg"].ToString());
                        ht.Add("ec_trends", dr["ec_trends"].ToString());

                        string isPromo = dr["isPromotion"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["isPromotion"].ToString()) ?
                                (dr["isPromotion"].ToString() == "1" || dr["isPromotion"].ToString().ToLower() == "true" || dr["isPromotion"].ToString().ToLower() == "yes" ? "True" : "False")
                                : "False")
                                : "False";
                        ht.Add("isPromotion", isPromo);

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ExhibitorPromotionList
            //else if (page == "ExhibitorPromotionList")
            //{
            //    isExistAPI += 1;
            //    sql = string.Empty;
            //    sql = "select * from tblExhibitingCompany where deleteFlag=0";
            //    string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='ExhibitingCompany'", "dataversion");
            //    DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            //    if (dt.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dt.Rows)
            //        {
            //            Hashtable ht = new Hashtable();

            //            ht.Add("ec_ID", dr["ec_ID"].ToString());
            //            ht.Add("ec_name", dr["ec_name"].ToString());
            //            ht.Add("ec_address", dr["ec_address"].ToString());
            //            ht.Add("ec_country", dr["ec_country"].ToString());
            //            ht.Add("ec_email", dr["ec_email"].ToString());
            //            ht.Add("ec_contactno", dr["ec_contactno"].ToString());
            //            ht.Add("ec_website", dr["ec_website"].ToString());
            //            ht.Add("ec_brochure", dr["ec_brochure"].ToString());
            //            ht.Add("ec_logo", dr["ec_logo"].ToString());
            //            ht.Add("ec_companyprofile", dr["ec_companyprofile"].ToString());
            //            ht.Add("ec_companytype", dr["ec_companytype"].ToString());
            //            ht.Add("ec_hall", daldata.getHallByID(dr["ec_hall"].ToString()));
            //            ht.Add("ec_booth", dr["ec_booth"].ToString());//daldata.getBoothByID(
            //            ht.Add("contactpersonID", dr["contactpersonID"].ToString());
            //            ht.Add("ec_product", dr["ec_product"].ToString());
            //            ht.Add("ec_productHTML", dr["ec_productHTML"] != DBNull.Value ? dr["ec_productHTML"].ToString() : "");
            //            ht.Add("deleteFlag", dr["deleteFlag"].ToString());
            //            ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

            //            ht.Add("ec_marketseg", dr["ec_marketseg"].ToString());
            //            ht.Add("ec_trends", dr["ec_trends"].ToString());

            //            string isPromo = dr["isPromotion"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["isPromotion"].ToString()) ?
            //                    (dr["isPromotion"].ToString() == "1" || dr["isPromotion"].ToString().ToLower() == "true" || dr["isPromotion"].ToString().ToLower() == "yes" ? "True" : "False")
            //                    : "False")
            //                    : "False";
            //            ht.Add("isPromotion", isPromo);

            //            rtnlist.Add(ht);
            //        }
            //        htt.Add("dataversion", dataversion);
            //        htt.Add("data", rtnlist);
            //        htt.Add("Status", "200");
            //        htt.Add("message", "success");
            //    }
            //    else
            //    {
            //        htt.Add("dataversion", dataversion);
            //        htt.Add("data", rtnlist);
            //        htt.Add("Status", "200");
            //        htt.Add("message", "success");
            //    }
            //}
            #endregion

            #region ExhibitingCompanyDetails
            else if (page == "ExhibitingCompanyDetails")
            {
                string exhibitingCompanayID = string.Empty;
                if (Request.Form["ExhibitorID"] != null)
                {
                    exhibitingCompanayID = Request.Form["ExhibitorID"].ToString();
                }

                if (Request.QueryString["ExhibitorID"] != null)
                {
                    exhibitingCompanayID = Request.QueryString["ExhibitorID"].ToString();
                }

                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblExhibitingCompany where deleteFlag=0 And ec_ID='" + exhibitingCompanayID + "'";
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("ec_ID", dr["ec_ID"].ToString());
                        ht.Add("ec_name", dr["ec_name"].ToString());
                        ht.Add("ec_address", dr["ec_address"].ToString());
                        ht.Add("ec_country", dr["ec_country"].ToString());
                        ht.Add("ec_email", dr["ec_email"].ToString());
                        ht.Add("ec_contactno", dr["ec_contactno"].ToString());
                        ht.Add("ec_website", dr["ec_website"].ToString());
                        ht.Add("ec_brochure", dr["ec_brochure"].ToString());
                        ht.Add("ec_logo", dr["ec_logo"].ToString());
                        ht.Add("ec_companyprofile", dr["ec_companyprofile"].ToString());
                        ht.Add("ec_companytype", dr["ec_companytype"].ToString());
                        ht.Add("ec_hall", daldata.getHallByID(dr["ec_hall"].ToString()));
                        ht.Add("ec_booth", dr["ec_booth"].ToString());//daldata.getBoothByID(
                        ht.Add("contactpersonID", dr["contactpersonID"].ToString());
                        ht.Add("ec_product", dr["ec_product"].ToString());
                        ht.Add("ec_productHTML", dr["ec_productHTML"] != DBNull.Value ? dr["ec_productHTML"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        ht.Add("ec_marketseg", dr["ec_marketseg"].ToString());
                        ht.Add("ec_trends", dr["ec_trends"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Product
            else if (page == "Product")
            {
                isExistAPI += 1;
                sql = string.Empty;

                sql = "select * from tblProduct where deleteFlag=0";                
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("p_ID", dr["p_ID"].ToString());
                        ht.Add("p_name", dr["p_name"].ToString());
                        string pcategory = daldata.getproductcategorybyID(dr["p_category"].ToString());
                        ht.Add("p_category", pcategory == "0" ? "" : pcategory);
                        string mainproduct = daldata.getproductbyID(dr["p_mainproductID"].ToString());
                        ht.Add("p_mainproductID", dr["p_mainproductID"].ToString());// mainproduct == "0" ? "" : mainproduct);
                        ht.Add("sororder", dr["sororder"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion 

            #region BusinessMatching
            else if (page == "BusinessMatching")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblBusinessMatching where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("bm_ID", dr["bm_ID"].ToString());
                        ht.Add("bm_vid", dr["bm_vid"].ToString());
                        ht.Add("bm_eid", dr["bm_eid"].ToString());
                        ht.Add("bm_date", dr["bm_date"].ToString());
                        ht.Add("bm_time", dr["bm_time"].ToString());
                        ht.Add("bm_status", dr["bm_status"].ToString());
                        ht.Add("bm_ecomment", dr["bm_ecomment"].ToString());
                        ht.Add("bm_reqtime", dr["bm_reqtime"].ToString());
                        ht.Add("bm_vcomment", dr["bm_vcomment"].ToString());
                        ht.Add("bm_interestedProduct", dr["bm_interestedProduct"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Newsletter
            else if (page == "Newsletter")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblNewsletter where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("nl_ID", dr["nl_ID"].ToString());
                        ht.Add("nl_title", dr["nl_title"].ToString());
                        ht.Add("nl_issue", dr["nl_issue"].ToString());
                        ht.Add("nl_doissue", dr["nl_doissue"].ToString());
                        ht.Add("nl_cover", dr["nl_cover"].ToString());
                        ht.Add("nl_dlink", dr["nl_dlink"].ToString());
                        ht.Add("menuID", dr["menuID"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region ScrollingBanner
            else if (page == "ScrollingBanner")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblScrollingBanner where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("sb_ID", dr["sb_ID"].ToString());
                        ht.Add("sb_title", dr["sb_title"].ToString());
                        ht.Add("sb_image", !string.IsNullOrEmpty(dr["sb_image"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["sb_image"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region BottomBanner
            else if (page == "BottomBanner")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblBottomBanner where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("bb_ID", dr["bb_ID"].ToString());
                        ht.Add("bb_title", dr["bb_title"].ToString());
                        ht.Add("bb_image", !string.IsNullOrEmpty(dr["bb_image"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["bb_image"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Speaker
            else if (page == "Speaker")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblSpeaker where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("s_ID", dr["s_ID"].ToString());
                        ht.Add("s_fullname", dr["s_fullname"].ToString());
                        ht.Add("s_job", dr["s_job"].ToString());
                        ht.Add("s_company", dr["s_company"].ToString());
                        ht.Add("s_email", dr["s_email"].ToString());
                        ht.Add("s_mobile", dr["s_mobile"].ToString());
                        ht.Add("s_address", dr["s_address"].ToString());
                        ht.Add("s_country", dr["s_country"].ToString());
                        ht.Add("s_bio", dr["s_bio"].ToString());
                        ht.Add("s_profilepic", !string.IsNullOrEmpty(dr["s_profilepic"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["s_profilepic"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Baristas
            else if (page == "Baristas")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblBaristas where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("s_ID", dr["s_ID"].ToString());
                        ht.Add("s_fullname", dr["s_fullname"].ToString());
                        ht.Add("s_job", dr["s_job"].ToString());
                        ht.Add("s_company", dr["s_company"].ToString());
                        ht.Add("s_email", dr["s_email"].ToString());
                        ht.Add("s_mobile", dr["s_mobile"].ToString());
                        ht.Add("s_address", dr["s_address"].ToString());
                        ht.Add("s_country", dr["s_country"].ToString());
                        ht.Add("s_bio", dr["s_bio"].ToString());
                        ht.Add("s_profilepic", !string.IsNullOrEmpty(dr["s_profilepic"].ToString()) ? Constant.URL + Constant.filesavingurlview + dr["s_profilepic"].ToString() : "");
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Inbox
            else if (page == "Inbox")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblInbox where deleteFlag=0 and i_isPush=1";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("i_ID", dr["i_ID"].ToString());
                        ht.Add("i_title", dr["i_title"].ToString());
                        ht.Add("i_content", dr["i_content"].ToString());
                        ht.Add("i_date", dr["i_date"].ToString());
                        ht.Add("i_time", dr["i_time"].ToString());
                        ht.Add("i_isPush", dr["i_isPush"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedTime", dr["UpdatedDate"].ToString());
                        ht.Add("from", dr["organizer"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Bookmark
            else if (page == "myBookmark")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblBookmark where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("bmark_ID", dr["bmark_ID"].ToString());
                        ht.Add("bmark_userid", dr["bmark_userid"].ToString());
                        ht.Add("bmark_exhid", dr["bmark_exhid"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Reward
            else if (page == "Reward")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblReward where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("rw_ID", dr["rw_ID"].ToString());
                        ht.Add("rw_title1", dr["rw_title1"].ToString());
                        ht.Add("rw_title2", dr["rw_title2"].ToString());
                        ht.Add("rw_image1", dr["rw_image1"].ToString());
                        ht.Add("rw_image2", dr["rw_image2"].ToString());
                        ht.Add("rw_password", dr["rw_password"].ToString());
                        ht.Add("rw_content1", dr["rw_content1"].ToString());
                        ht.Add("rw_content2", dr["rw_content2"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Moments
            else if (page == "Moments")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblMoments where deleteFlag=0";

                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    sql = string.Empty;
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("m_ID", dr["m_ID"].ToString());
                        ht.Add("m_txtcomment", dr["m_txtcomment"].ToString());
                        ht.Add("m_photocomment", dr["m_photocomment"].ToString());
                        ht.Add("m_status", dr["m_status"].ToString());
                        ht.Add("m_addedtime", dr["m_addedtime"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region SpecialEvent
            else if (page == "SpecialEvent")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblSpecialEvent where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("spe_ID", dr["spe_ID"].ToString());
                        ht.Add("spe_date", dr["spe_date"].ToString());
                        ht.Add("spe_time", dr["spe_time"].ToString());
                        ht.Add("spe_title", dr["spe_title"].ToString());
                        ht.Add("spe_venue", dr["spe_venue"].ToString());
                        ht.Add("spe_brief", dr["spe_brief"].ToString());
                        ht.Add("spe_image", dr["spe_image"].ToString());
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Innovation
            else if (page == "Innovation")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tblInnovation where deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("Ino_ID", dr["Ino_ID"].ToString());
                        ht.Add("Ino_productName", dr["Ino_productName"].ToString());
                        ht.Add("Ino_QCategories", dr["Ino_QCategories"].ToString());
                        ht.Add("Ino_productInfo", dr["Ino_productInfo"].ToString());
                        //ht.Add("Ino_productPic", !string.IsNullOrEmpty(dr["Ino_productPic"].ToString()) ? (!dr["Ino_productPic"].ToString().Contains("http://") && !dr["Ino_productPic"].ToString().Contains("https://") ? Constant.URL + Constant.filesavingurlview + dr["Ino_productPic"].ToString() : dr["Ino_productPic"].ToString())  : "");
                        ht.Add("Ino_productPic", string.IsNullOrEmpty(dr["Ino_productPic"].ToString()) ? "" : Constant.URL + dr["Ino_productPic"].ToString());
                        ht.Add("Ino_certification", dr["Ino_certification"].ToString());
                        ht.Add("Ino_region", dr["Ino_region"].ToString());
                        ht.Add("Ino_rating", dr["Ino_rating"].ToString());
                        //string exh = daldata.getexhibitingcompanybyID(dr["Ino_exhID"].ToString());
                        ht.Add("Ino_exhID", dr["Ino_exhID"].ToString());// exh == "0" ? "" : exh);
                        ht.Add("deleteFlag", dr["deleteFlag"].ToString());
                        ht.Add("UpdatedDate", dr["UpdatedDate"].ToString());

                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                }
            }
            #endregion

            #region Configuration
            if (page == "Configuration")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tb_Configuration";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    Hashtable ht = new Hashtable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ht = new Hashtable();
                        ht.Add("config_id", dr["config_id"].ToString());
                        ht.Add("systemLanguage", dr["config_language"].ToString());
                        ht.Add("showName", dr["config_name"].ToString());
                        ht.Add("showStartDate", dr["config_startDate"].ToString());
                        ht.Add("showEndDate", dr["config_endDate"].ToString());
                        ht.Add("enableAccount", dr["config_enableAccount"].ToString());
                        ht.Add("appIcon", dr["config_appIcon"].ToString());
                        ht.Add("showVenue", dr["config_venue"].ToString());
                        ht.Add("surveyStartDate", dr["config_surveyStartDate"].ToString());
                        ht.Add("surveyEndDate", dr["config_surveyEndDate"].ToString());
                        ht.Add("visitorRegURL", dr["config_visRegURL"].ToString());
                        ht.Add("homePageTemplate", dr["config_homePageLayout"].ToString());
                        ht.Add("iOSVersion", dr["config_iOSVersion"].ToString());
                        ht.Add("iOSDownloadLink", dr["config_iOsDownloadLink"].ToString());
                        ht.Add("AndroidVersion", dr["config_AndroidVersion"].ToString());
                        ht.Add("AndroidDownloadLink", dr["config_AndroidDownloadLink"].ToString());
                        ht.Add("timestamp", dr["updatedDate"].ToString());
                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    htt.Add("data", rtnlist);

                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    //htt = new Hashtable();
                    //htt.Add("Status", "204");
                    //htt.Add("message", "no content");
                    //htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region Menu
            else if (page == "Menu")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tb_Menu where Status <> 'Deleted'";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    Hashtable ht = new Hashtable();
                    string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                    string uri = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                    int c = uri.Length - 1;
                    string mp = uri.Remove(c) + HttpContext.Current.Request.ApplicationPath;
                    foreach (DataRow dr in dt.Rows)
                    {
                        ht = new Hashtable();
                        ht.Add("m_id", dr["m_id"]);
                        ht.Add("m_TitleEn", dr["m_TitleEn"].ToString());
                        ht.Add("m_TitleCn", dr["m_TitleCn"].ToString());
                        ht.Add("m_OrderID", dr["m_OrderID"].ToString());
                        if (!string.IsNullOrEmpty(dr["m_IconUrl"].ToString()))
                        {
                            ht.Add("m_IconUrl", mp + "/sys_Images/Mobile/" + dr["m_IconUrl"].ToString());
                        }
                        else
                        {
                            ht.Add("m_IconUrl", dr["m_IconUrl"].ToString());
                        }
                        ht.Add("m_PageType", dr["m_PageType"].ToString());
                        ht.Add("m_parentMenuID", dr["m_parentMenuID"].ToString());
                        ht.Add("m_permissioncontrol", dr["m_permissioncontrol"].ToString());
                        ht.Add("m_menuType", dr["m_menuType"].ToString());
                        ht.Add("m_weblink", dr["m_weblink"].ToString());
                        ht.Add("timestamp", dr["updatedDate"].ToString());
                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    htt.Add("data", rtnlist);

                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    //htt = new Hashtable();
                    //htt.Add("Status", "204");
                    //htt.Add("message", "no content");
                    //htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region ColorScheme
            if (page == "ColorScheme")
            {
                isExistAPI += 1;
                sql = string.Empty;
                sql = "select * from tb_ColorScheme";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + page + "'", "dataversion");
                if (dt.Rows.Count > 0)
                {
                    Hashtable ht = new Hashtable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        ht = new Hashtable();
                        ht.Add("color_background_1", dr["CS_BG1"].ToString());
                        ht.Add("color_background_2", dr["CS_BG2"].ToString());
                        ht.Add("color_button_1", dr["CS_B1"].ToString());
                        ht.Add("color_button_2", dr["CS_B2"].ToString());
                        ht.Add("color_font_1", dr["CS_F1"].ToString());
                        ht.Add("color_font_2", dr["CS_F2"].ToString());
                        ht.Add("color_font_3", dr["CS_F3"].ToString());
                        ht.Add("color_column_1", dr["CS_C1"].ToString());
                        ht.Add("color_column_2", dr["CS_C2"].ToString());
                        ht.Add("color_margin_1", dr["CS_M1"].ToString());
                        ht.Add("color_margin_2", dr["CS_M2"].ToString());
                        ht.Add("timestamp", dr["updatedDate"].ToString());
                        rtnlist.Add(ht);
                    }
                    htt.Add("dataversion", dataversion);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    htt.Add("data", rtnlist);

                }
                else
                {
                    htt.Add("dataversion", dataversion);
                    htt.Add("data", rtnlist);
                    htt.Add("Status", "200");
                    htt.Add("message", "success");
                    //htt = new Hashtable();
                    //htt.Add("Status", "204");
                    //htt.Add("message", "no content");
                    //htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region Login
            //if (page == "Login")
            //{
            //    string compName = string.Empty;
            //    string compEmail = string.Empty;
            //    string SurveyStatus = "0";
            //    string userType = string.Empty;

            //    if (Request.Form["username"] != null)
            //    {
            //        compName = Request.Form["username"].ToString();
            //    }
            //    if (Request.Form["password"] != null)
            //    {
            //        compEmail = Request.Form["password"].ToString();
            //    }
            //    if (Request.Form["loginType"] != null)
            //    {
            //        userType = Request.Form["loginType"].ToString();
            //    }
            //    string rtn = dalf.LoginMobile(compName, compEmail, userType); //f_c_rowid + "^" + acctype + "^" + name;

            //    if (string.IsNullOrEmpty(rtn) && userType == "V")
            //    {
            //        #region Create New Visitor
            //        List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
            //        lstBackendChild = dalc.SelectByPId(pid);
            //        string rkey = string.Empty; string rid = string.Empty;
            //        rkey = gk.getKey("RID");
            //        rid = "RID" + rkey;
            //        string rowid = rid;
            //        foreach (var l in lstBackendChild)
            //        {
            //            string content = string.Empty;
            //            if (l.c_colname == "a_type")
            //            {
            //                content = "Visitor";
            //            }
            //            else if (l.c_colname == "a_sal")
            //            {
            //                content = "Mr";
            //            }
            //            else if (l.c_colname == "a_fname")
            //            {
            //                content = compName;
            //            }
            //            else if (l.c_colname == "a_lname")
            //            {
            //                content = "";
            //            }
            //            else if (l.c_colname == "a_designation")
            //            {
            //                content = "Nil";
            //            }
            //            else if (l.c_colname == "a_email")
            //            {
            //                content = compEmail;
            //            }
            //            else if (l.c_colname == "a_company")
            //            {
            //                content = compName;
            //            }
            //            else if (l.c_colname == "a_addressOfc")
            //            {
            //                content = "Mr";
            //            }
            //            else if (l.c_colname == "a_addressHome")
            //            {
            //                content = "Nil";
            //            }
            //            else if (l.c_colname == "a_country")
            //            {
            //                content = "Nil";
            //            }
            //            else if (l.c_colname == "a_Mobile")
            //            {
            //                content = "Nil";
            //            }
            //            else if (l.c_colname == "a_loginName")
            //            {
            //                content = compName;
            //            }
            //            else if (l.c_colname == "a_Password")
            //            {
            //                content = compEmail;
            //            }
            //            else if (l.c_colname == "a_isActivated")
            //            {
            //                content = "Yes";
            //            }
            //            string fkey = string.Empty; string fid = string.Empty;
            //            fkey = gk.getKey("FE");
            //            fid = "FE" + fkey;
            //            FrontEnd fe = new FrontEnd()
            //            { f_id = fid, f_pid = pid, f_label = l.c_label, f_entrycount = 1, f_inputtype = l.c_inputtype, f_content = content, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = l.c_sortorder, f_c_id = l.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
            //            int isSuccess = dalf.Save(fe);
            //            gk.SaveKey("RID", int.Parse(rkey));
            //            gk.SaveKey("FE", int.Parse(fkey));
            //        }
            //        #region timestamp  
            //        DateTime updtime = DateTime.Now;
            //        string updatetimestamp = dalf.ConvertToTimestamp(updtime);
            //        int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
            //        #endregion
            //        #endregion

            //        #region return 
            //        rtn = rowid + "^Visitor^Mr " + compName;
            //        SurveyQA sqa = dalf.SelectSurveyQAByID(rowid);
            //        if (sqa != null)
            //        {
            //            SurveyStatus = "1";
            //        }
            //        else
            //        {
            //            SurveyStatus = "0";
            //        }
            //        htt = new Hashtable();
            //        htt.Add("UserID", rowid);
            //        htt.Add("UserType", "Visitor");
            //        htt.Add("SurveyQA", SurveyStatus);
            //        htt.Add("Status", "200");
            //        htt.Add("message", "success");
            //        #endregion
            //    }
            //    else if (string.IsNullOrEmpty(rtn) && userType != "V")
            //    {
            //        htt = new Hashtable();
            //        htt.Add("UserID", "");
            //        htt.Add("UserType", userType);
            //        htt.Add("SurveyQA", "");
            //        htt.Add("Status", "401");
            //        htt.Add("message", "fail");
            //    }
            //    else
            //    {
            //        #region return
            //        string rowid = rtn.Split('^')[0];
            //        string usertype = rtn.Split('^')[1];
            //        SurveyQA sqa = dalf.SelectSurveyQAByID(rowid);
            //        if (sqa != null)
            //        {
            //            SurveyStatus = "1";
            //        }
            //        else
            //        {
            //            SurveyStatus = "0";
            //        }
            //        htt = new Hashtable();
            //        htt.Add("UserID", rowid);
            //        htt.Add("UserType", usertype);
            //        htt.Add("SurveyQA", SurveyStatus);
            //        htt.Add("Status", "200");
            //        htt.Add("message", "success");
            //        #endregion
            //    }
            //}
            #endregion

            #region DelegateLogin
            if (page == "DelegateLogin")
            {
                isExistAPI += 1;
                string delEmail = string.Empty;
                string delPassword = string.Empty;
                if (Request.Form["Email"] != null)
                {
                    delEmail = Request.Form["Email"].ToString();
                }
                if (Request.Form["Password"] != null)
                {
                    delPassword = Request.Form["Password"].ToString();
                }

                if (Request.QueryString["Email"] != null)
                {
                    delEmail = Request.QueryString["Email"].ToString();
                }
                if (Request.QueryString["Password"] != null)
                {
                    delPassword = Request.QueryString["Password"].ToString();
                }
                Hashtable ht = new Hashtable();
                int rtn = daldata.DelegateLoginAuthenticate(delEmail, delPassword);
                if (rtn > 0)
                {
                    try
                    {
                        string userid = "";
                        string sqlUser = "Select a_ID,* From tblAccount Where a_email='" + delEmail + "' And a_Password='" + delPassword + "' And deleteFlag=0";
                        DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            userid = dt.Rows[0]["a_ID"].ToString();
                            string a_type = dt.Rows[0]["a_type"] != DBNull.Value ? dt.Rows[0]["a_type"].ToString() : "";
                            string a_fullname = dt.Rows[0]["a_fullname"] != DBNull.Value ? dt.Rows[0]["a_fullname"].ToString() : "";
                            string a_fname = dt.Rows[0]["a_fname"] != DBNull.Value ? dt.Rows[0]["a_fname"].ToString() : "";
                            string a_lname = dt.Rows[0]["a_lname"] != DBNull.Value ? dt.Rows[0]["a_lname"].ToString() : "";
                            string a_sal = dt.Rows[0]["a_sal"] != DBNull.Value ? dt.Rows[0]["a_sal"].ToString() : "";
                            string a_designation = dt.Rows[0]["a_designation"] != DBNull.Value ? dt.Rows[0]["a_designation"].ToString() : "";
                            string a_email = dt.Rows[0]["a_email"] != DBNull.Value ? dt.Rows[0]["a_email"].ToString() : "";
                            string a_company = dt.Rows[0]["a_company"] != DBNull.Value ? dt.Rows[0]["a_company"].ToString() : "";
                            string a_addressOfc = dt.Rows[0]["a_addressOfc"] != DBNull.Value ? dt.Rows[0]["a_addressOfc"].ToString() : "";
                            string a_addressHome = dt.Rows[0]["a_addressHome"] != DBNull.Value ? dt.Rows[0]["a_addressHome"].ToString() : "";
                            string a_country = dt.Rows[0]["a_country"] != DBNull.Value ? dt.Rows[0]["a_country"].ToString() : "";
                            string a_Tel = dt.Rows[0]["a_Tel"] != DBNull.Value ? dt.Rows[0]["a_Tel"].ToString() : "";
                            string a_Mobile = dt.Rows[0]["a_Mobile"] != DBNull.Value ? dt.Rows[0]["a_Mobile"].ToString() : "";
                            string a_loginName = dt.Rows[0]["a_loginName"] != DBNull.Value ? dt.Rows[0]["a_loginName"].ToString() : "";
                            string a_isActivated = dt.Rows[0]["a_isActivated"] != DBNull.Value ? dt.Rows[0]["a_isActivated"].ToString() : "";
                            string a_AttendDay = dt.Rows[0]["a_AttendDay"] != DBNull.Value ? dt.Rows[0]["a_AttendDay"].ToString() : "";
                            string a_profilepic = dt.Rows[0]["a_profilepic"] != DBNull.Value ? dt.Rows[0]["a_profilepic"].ToString() : "";
                            //string a_Password = dt.Rows[0]["a_Password"] != DBNull.Value ? dt.Rows[0]["a_Password"].ToString() : "";
                            ht.Add("UserID", userid);
                            ht.Add("UserType", a_type);
                            ht.Add("FullName", a_fullname);
                            ht.Add("FirstName", a_fname);
                            ht.Add("LastName", a_lname);
                            ht.Add("Salutation", a_sal);
                            ht.Add("Designation", a_designation);
                            ht.Add("Email", a_email);
                            ht.Add("Company", a_company);
                            ht.Add("AddressOfCompany", a_addressOfc);
                            ht.Add("AddressHome", a_addressHome);
                            ht.Add("Country", a_country);
                            ht.Add("Tel", a_Tel);
                            ht.Add("Mobile", a_Mobile);
                            //ht.Add("LoginName", a_loginName);
                            ht.Add("isActivated", a_isActivated);
                            ht.Add("AttendDay", a_AttendDay);
                            ht.Add("PhotoUrl", !string.IsNullOrEmpty(a_profilepic) ? Constant.URL + Constant.filesavingurlview + a_profilepic : "");
                            rtnlist.Add(ht);

                            htt.Add("dataversion", 1);
                            htt.Add("Status", "200");
                            htt.Add("message", "success");
                            htt.Add("data", rtnlist);
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "204");
                            htt.Add("message", "No data.");
                            htt.Add("data", rtnlist);
                        }
                        #region Log
                        try
                        {
                            string logInsert = "Insert Into tb_Log_Login (DelegateUserName, DelegateEmail, AccountID) Values ('" + delPassword + "','" + delEmail + "','" + userid + "')";
                            fn.ExecuteSQL(logInsert);
                        }
                        catch (Exception ex)
                        { }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "202");
                        htt.Add("message", "Error.");
                        htt.Add("data", rtnlist);
                    }
                }
                else
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "204");
                    htt.Add("message", "Invalid email or password.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region MobileLogin (parameters [page=MobileLogin, lang=1, Email=, Password=] from mobile side, will return back user details)
            if (page == "MobileLogin")
            {
                isExistAPI += 1;
                string delEmail = string.Empty;
                string delPassword = string.Empty;
                if (Request.Form["Email"] != null)
                {
                    delEmail = Request.Form["Email"].ToString();
                }
                if (Request.Form["Password"] != null)
                {
                    delPassword = Request.Form["Password"].ToString();
                }

                if (Request.QueryString["Email"] != null)
                {
                    delEmail = Request.QueryString["Email"].ToString();
                }
                if (Request.QueryString["Password"] != null)
                {
                    delPassword = Request.QueryString["Password"].ToString();
                }
                Hashtable ht = new Hashtable();
                int rtn = daldata.DelegateLoginAuthenticate(delEmail, delPassword);
                if (rtn > 0)
                {
                    try
                    {
                        string userid = "";
                        string sqlUser = "Select a_ID,* From tblAccount Where a_email='" + delEmail + "' And a_Password='" + delPassword + "' And deleteFlag=0 ";
                        DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            userid = dt.Rows[0]["a_ID"].ToString();
                            string a_type = dt.Rows[0]["a_type"] != DBNull.Value ? dt.Rows[0]["a_type"].ToString() : "";
                            string a_fullname = dt.Rows[0]["a_fullname"] != DBNull.Value ? dt.Rows[0]["a_fullname"].ToString() : "";
                            string a_fname = dt.Rows[0]["a_fname"] != DBNull.Value ? dt.Rows[0]["a_fname"].ToString() : "";
                            string a_lname = dt.Rows[0]["a_lname"] != DBNull.Value ? dt.Rows[0]["a_lname"].ToString() : "";
                            string a_sal = dt.Rows[0]["a_sal"] != DBNull.Value ? dt.Rows[0]["a_sal"].ToString() : "";
                            string a_designation = dt.Rows[0]["a_designation"] != DBNull.Value ? dt.Rows[0]["a_designation"].ToString() : "";
                            string a_email = dt.Rows[0]["a_email"] != DBNull.Value ? dt.Rows[0]["a_email"].ToString() : "";
                            string a_company = dt.Rows[0]["a_company"] != DBNull.Value ? dt.Rows[0]["a_company"].ToString() : "";
                            string a_addressOfc = dt.Rows[0]["a_addressOfc"] != DBNull.Value ? dt.Rows[0]["a_addressOfc"].ToString() : "";
                            string a_addressHome = dt.Rows[0]["a_addressHome"] != DBNull.Value ? dt.Rows[0]["a_addressHome"].ToString() : "";
                            string a_country = dt.Rows[0]["a_country"] != DBNull.Value ? dt.Rows[0]["a_country"].ToString() : "";
                            string a_Tel = dt.Rows[0]["a_Tel"] != DBNull.Value ? dt.Rows[0]["a_Tel"].ToString() : "";
                            string a_Mobile = dt.Rows[0]["a_Mobile"] != DBNull.Value ? dt.Rows[0]["a_Mobile"].ToString() : "";
                            string a_loginName = dt.Rows[0]["a_loginName"] != DBNull.Value ? dt.Rows[0]["a_loginName"].ToString() : "";
                            string a_isActivated = dt.Rows[0]["a_isActivated"] != DBNull.Value ? dt.Rows[0]["a_isActivated"].ToString() : "";
                            string a_AttendDay = dt.Rows[0]["a_AttendDay"] != DBNull.Value ? dt.Rows[0]["a_AttendDay"].ToString() : "";
                            string a_profilepic = dt.Rows[0]["a_profilepic"] != DBNull.Value ? dt.Rows[0]["a_profilepic"].ToString() : "";
                            //string a_Password = dt.Rows[0]["a_Password"] != DBNull.Value ? dt.Rows[0]["a_Password"].ToString() : "";
                            string iScanExhID = dt.Rows[0]["ExhID_iScan"] != DBNull.Value ? dt.Rows[0]["ExhID_iScan"].ToString() : "";
                            string RegPortalID = dt.Rows[0]["RegPortalID"] != DBNull.Value ? dt.Rows[0]["RegPortalID"].ToString() : "";
                            ht.Add("UserID", userid);
                            ht.Add("UserType", a_type);
                            ht.Add("FullName", a_fullname);
                            ht.Add("FirstName", a_fname);
                            ht.Add("LastName", a_lname);
                            ht.Add("Salutation", a_sal);
                            ht.Add("Designation", a_designation);
                            ht.Add("Email", a_email);
                            ht.Add("Company", a_company);
                            ht.Add("AddressOfCompany", a_addressOfc);
                            ht.Add("AddressHome", a_addressHome);
                            ht.Add("Country", a_country);
                            ht.Add("Tel", a_Tel);
                            ht.Add("Mobile", a_Mobile);
                            //ht.Add("LoginName", a_loginName);
                            ht.Add("isActivated", a_isActivated);
                            ht.Add("AttendDay", a_AttendDay);
                            //ht.Add("PhotoUrl", !string.IsNullOrEmpty(a_profilepic) ? Constant.URL + Constant.filesavingurlview + a_profilepic : "");
                            ht.Add("iScanExhID", iScanExhID);
                            ht.Add("ISiScanExhibitor", !string.IsNullOrEmpty(iScanExhID) ? "Yes" : "No");
                            ht.Add("RegPortalID", RegPortalID);
                            rtnlist.Add(ht);

                            htt.Add("dataversion", 1);
                            htt.Add("Status", "200");
                            htt.Add("message", "success");
                            htt.Add("data", rtnlist);
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "204");
                            htt.Add("message", "No data.");
                            htt.Add("data", rtnlist);
                        }
                        #region Log
                        try
                        {
                            string logInsert = "Insert Into tb_Log_Login (DelegateUserName, DelegateEmail, AccountID) Values ('" + delPassword + "','" + delEmail + "','" + userid + "')";
                            fn.ExecuteSQL(logInsert);
                        }
                        catch (Exception ex)
                        { }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "202");
                        htt.Add("message", "Error.");
                        htt.Add("data", rtnlist);
                    }
                }
                else
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "204");
                    htt.Add("message", "Invalid email or password.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region QRLogin (parameters [page=QRLogin, lang=1, Regno=] from mobile side, will return back user details)
            if (page == "QRLogin")
            {
                isExistAPI += 1;
                string regportalRegno = string.Empty;
                if (Request.Form["Regno"] != null)
                {
                    regportalRegno = Request.Form["Regno"].ToString();
                }

                if (Request.QueryString["Regno"] != null)
                {
                    regportalRegno = Request.QueryString["Regno"].ToString();
                }
                Hashtable ht = new Hashtable();
                int rtn = DelegateQRLoginAuthenticate(regportalRegno);
                if (rtn > 0)
                {
                    try
                    {
                        string userid = "";
                        string sqlUser = "Select a_ID,* From tblAccount Where RegPortalID='" + regportalRegno + "' And deleteFlag=0";
                        DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            userid = dt.Rows[0]["a_ID"].ToString();
                            string a_type = dt.Rows[0]["a_type"] != DBNull.Value ? dt.Rows[0]["a_type"].ToString() : "";
                            string a_fullname = dt.Rows[0]["a_fullname"] != DBNull.Value ? dt.Rows[0]["a_fullname"].ToString() : "";
                            string a_fname = dt.Rows[0]["a_fname"] != DBNull.Value ? dt.Rows[0]["a_fname"].ToString() : "";
                            string a_lname = dt.Rows[0]["a_lname"] != DBNull.Value ? dt.Rows[0]["a_lname"].ToString() : "";
                            string a_sal = dt.Rows[0]["a_sal"] != DBNull.Value ? dt.Rows[0]["a_sal"].ToString() : "";
                            string a_designation = dt.Rows[0]["a_designation"] != DBNull.Value ? dt.Rows[0]["a_designation"].ToString() : "";
                            string a_email = dt.Rows[0]["a_email"] != DBNull.Value ? dt.Rows[0]["a_email"].ToString() : "";
                            string a_company = dt.Rows[0]["a_company"] != DBNull.Value ? dt.Rows[0]["a_company"].ToString() : "";
                            string a_addressOfc = dt.Rows[0]["a_addressOfc"] != DBNull.Value ? dt.Rows[0]["a_addressOfc"].ToString() : "";
                            string a_addressHome = dt.Rows[0]["a_addressHome"] != DBNull.Value ? dt.Rows[0]["a_addressHome"].ToString() : "";
                            string a_country = dt.Rows[0]["a_country"] != DBNull.Value ? dt.Rows[0]["a_country"].ToString() : "";
                            string a_Tel = dt.Rows[0]["a_Tel"] != DBNull.Value ? dt.Rows[0]["a_Tel"].ToString() : "";
                            string a_Mobile = dt.Rows[0]["a_Mobile"] != DBNull.Value ? dt.Rows[0]["a_Mobile"].ToString() : "";
                            string a_loginName = dt.Rows[0]["a_loginName"] != DBNull.Value ? dt.Rows[0]["a_loginName"].ToString() : "";
                            string a_isActivated = dt.Rows[0]["a_isActivated"] != DBNull.Value ? dt.Rows[0]["a_isActivated"].ToString() : "";
                            string a_AttendDay = dt.Rows[0]["a_AttendDay"] != DBNull.Value ? dt.Rows[0]["a_AttendDay"].ToString() : "";
                            string a_profilepic = dt.Rows[0]["a_profilepic"] != DBNull.Value ? dt.Rows[0]["a_profilepic"].ToString() : "";
                            //string a_Password = dt.Rows[0]["a_Password"] != DBNull.Value ? dt.Rows[0]["a_Password"].ToString() : "";
                            string iScanExhID = dt.Rows[0]["ExhID_iScan"] != DBNull.Value ? dt.Rows[0]["ExhID_iScan"].ToString() : "";
                            string RegPortalID = dt.Rows[0]["RegPortalID"] != DBNull.Value ? dt.Rows[0]["RegPortalID"].ToString() : "";
                            ht.Add("UserID", userid);
                            ht.Add("UserType", a_type);
                            ht.Add("FullName", a_fullname);
                            ht.Add("FirstName", a_fname);
                            ht.Add("LastName", a_lname);
                            ht.Add("Salutation", a_sal);
                            ht.Add("Designation", a_designation);
                            ht.Add("Email", a_email);
                            ht.Add("Company", a_company);
                            ht.Add("AddressOfCompany", a_addressOfc);
                            ht.Add("AddressHome", a_addressHome);
                            ht.Add("Country", a_country);
                            ht.Add("Tel", a_Tel);
                            ht.Add("Mobile", a_Mobile);
                            //ht.Add("LoginName", a_loginName);
                            ht.Add("isActivated", a_isActivated);
                            ht.Add("AttendDay", a_AttendDay);
                            //ht.Add("PhotoUrl", !string.IsNullOrEmpty(a_profilepic) ? Constant.URL + Constant.filesavingurlview + a_profilepic : "");
                            ht.Add("iScanExhID", iScanExhID);
                            ht.Add("ISiScanExhibitor", !string.IsNullOrEmpty(iScanExhID) ? "Yes" : "No");
                            ht.Add("RegPortalID", RegPortalID);
                            rtnlist.Add(ht);

                            htt.Add("dataversion", 1);
                            htt.Add("Status", "200");
                            htt.Add("message", "success");
                            htt.Add("data", rtnlist);
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "204");
                            htt.Add("message", "No data.");
                            htt.Add("data", rtnlist);
                        }
                        #region Log
                        try
                        {
                            string logInsert = "Insert Into tb_Log_Login (DelegateUserName, DelegateEmail, AccountID) Values ('" + regportalRegno + "','" + regportalRegno + "','" + userid + "')";
                            fn.ExecuteSQL(logInsert);
                        }
                        catch (Exception ex)
                        { }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "202");
                        htt.Add("message", "Error.");
                        htt.Add("data", rtnlist);
                    }
                }
                else
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "204");
                    htt.Add("message", "Invalid user.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region NFCScan (parameters [page=NFCScan, lang=1, NFCID=, ExhDeviceCode=] from mobile side, will return back string formatted like QR code)(search (reg)data by NFCID and return/send back QR code string to Mobile [QR string format is "vSunnyHu^Corpit^CEO^sunnyhu@corpit.com.sg^12345678^SINGAPORE^38810178"])
            if (page == "NFCScan")
            {
                isExistAPI += 1;
                string NFCID = string.Empty;
                string exhDeviceCode = string.Empty;
                string source = "NFC";
                if (Request.Form["NFCID"] != null)
                {
                    NFCID = Request.Form["NFCID"].ToString();
                }
                if (Request.Form["ExhDeviceCode"] != null)
                {
                    exhDeviceCode = Request.Form["ExhDeviceCode"].ToString();
                }

                if (Request.QueryString["NFCID"] != null)
                {
                    NFCID = Request.QueryString["NFCID"].ToString();
                }
                if (Request.QueryString["ExhDeviceCode"] != null)
                {
                    exhDeviceCode = Request.QueryString["ExhDeviceCode"].ToString();
                }
                if (!string.IsNullOrEmpty(NFCID) && !string.IsNullOrEmpty(exhDeviceCode))
                {
                    Hashtable ht = new Hashtable();
                    int rtn = DelegateNFCScanAuthenticate(NFCID);
                    if (rtn > 0)
                    {
                        try
                        {
                            /*vSunnyHu^Corpit^CEO^sunnyhu@corpit.com.sg^12345678^SINGAPORE^38810178*/
                            /*vName^email^Company^Country^JobTitle^Contact^Regno*//*changed on 14-2-2020 according to TC msg*/
                            string userid = "";
                            string sqlUser = "Select a_ID,* From tblAccount Where NFCID='" + NFCID + "' And deleteFlag=0";
                            DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                userid = dt.Rows[0]["a_ID"].ToString();
                                string a_type = dt.Rows[0]["a_type"] != DBNull.Value ? dt.Rows[0]["a_type"].ToString() : "";
                                string a_fullname = dt.Rows[0]["a_fullname"] != DBNull.Value ? dt.Rows[0]["a_fullname"].ToString() : "";
                                string a_fname = dt.Rows[0]["a_fname"] != DBNull.Value ? dt.Rows[0]["a_fname"].ToString() : "";
                                string a_lname = dt.Rows[0]["a_lname"] != DBNull.Value ? dt.Rows[0]["a_lname"].ToString() : "";
                                string a_sal = dt.Rows[0]["a_sal"] != DBNull.Value ? dt.Rows[0]["a_sal"].ToString() : "";
                                string a_designation = dt.Rows[0]["a_designation"] != DBNull.Value ? dt.Rows[0]["a_designation"].ToString() : "";
                                string a_email = dt.Rows[0]["a_email"] != DBNull.Value ? dt.Rows[0]["a_email"].ToString() : "";
                                string a_company = dt.Rows[0]["a_company"] != DBNull.Value ? dt.Rows[0]["a_company"].ToString() : "";
                                string a_addressOfc = dt.Rows[0]["a_addressOfc"] != DBNull.Value ? dt.Rows[0]["a_addressOfc"].ToString() : "";
                                string a_addressHome = dt.Rows[0]["a_addressHome"] != DBNull.Value ? dt.Rows[0]["a_addressHome"].ToString() : "";
                                string a_country = dt.Rows[0]["a_country"] != DBNull.Value ? dt.Rows[0]["a_country"].ToString() : "";
                                string a_Tel = dt.Rows[0]["a_Tel"] != DBNull.Value ? dt.Rows[0]["a_Tel"].ToString() : "";
                                string a_Mobile = dt.Rows[0]["a_Mobile"] != DBNull.Value ? dt.Rows[0]["a_Mobile"].ToString() : "";
                                string a_loginName = dt.Rows[0]["a_loginName"] != DBNull.Value ? dt.Rows[0]["a_loginName"].ToString() : "";
                                string a_isActivated = dt.Rows[0]["a_isActivated"] != DBNull.Value ? dt.Rows[0]["a_isActivated"].ToString() : "";
                                string a_AttendDay = dt.Rows[0]["a_AttendDay"] != DBNull.Value ? dt.Rows[0]["a_AttendDay"].ToString() : "";
                                string a_profilepic = dt.Rows[0]["a_profilepic"] != DBNull.Value ? dt.Rows[0]["a_profilepic"].ToString() : "";
                                string RegPortalID = dt.Rows[0]["RegPortalID"] != DBNull.Value ? dt.Rows[0]["RegPortalID"].ToString() : "";

                                //ExhibitorProductObj exhObj = daliScan.getExhibitorProductByExhDeviceCode(exhDeviceCode);
                                //if (exhObj != null)
                                {
                                    string strQRCodeString = "v" + a_fullname + "^" + a_email + "^" + a_company + "^" + a_country + "^" + a_designation + "^" + a_Mobile + "^" + RegPortalID;

                                    #region Log Scanning in Mobile Site
                                    //string logInsert = string.Format("Insert Into tblVisitorExhibitorProduct (Regno, ExhID, MexhID, ExhName, ExhCountry, ExhContactPerson, "
                                    //+ " ExhEmail, ExhBoothNo, ExhProductID, ExhProductName, ExhProductDescription, Source) "
                                    //+ " Values ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}')"
                                    //, RegPortalID, exhObj.ExhID, exhObj.MexhID, exhObj.ExhName, exhObj.ExhCountry, exhObj.ExhContactPerson
                                    //, exhObj.ExhEmail, exhObj.ExhBoothNo, exhObj.ExhProductID, exhObj.ExhProductName, exhObj.ExhProductDescription, source);
                                    //fn.ExecuteSQL(logInsert);
                                    #endregion

                                    ht.Add("QRCodeString", strQRCodeString);
                                    ht.Add("UserType", a_type);
                                    rtnlist.Add(ht);

                                    htt.Add("dataversion", 1);
                                    htt.Add("Status", "200");
                                    htt.Add("message", "success");
                                    htt.Add("data", rtnlist);
                                }
                            }
                            else
                            {
                                htt.Add("dataversion", 1);
                                htt.Add("Status", "204");
                                htt.Add("message", "No data.");
                                htt.Add("data", rtnlist);
                            }
                        }
                        catch (Exception ex)
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "202");
                            htt.Add("message", "Error in logging.");
                            htt.Add("data", rtnlist);
                        }
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "Invalid NFCID.");
                        htt.Add("data", rtnlist);
                    }
                }
                else
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "204");
                    htt.Add("message", "Invalid NFCID.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region ProductScan (parameters [page=ProductScan, lang=1, RegPortalID=, ProductCode=] from mobile side, will return back exhibitor details and product desc)
            if (page == "ProductScan")
            {
                isExistAPI += 1;
                string regPortalID = string.Empty;
                string proCode = string.Empty;
                if (Request.Form["RegPortalID"] != null)
                {
                    regPortalID = Request.Form["RegPortalID"].ToString();
                }
                if (Request.Form["ProductCode"] != null)
                {
                    proCode = Request.Form["ProductCode"].ToString();
                }

                if (Request.QueryString["RegPortalID"] != null)
                {
                    regPortalID = Request.QueryString["RegPortalID"].ToString();
                }
                if (Request.QueryString["ProductCode"] != null)
                {
                    proCode = Request.QueryString["ProductCode"].ToString();
                }
                Hashtable ht = new Hashtable();
                try
                {
                    string sqlUser = "Select * From tb_ProductTable as p Inner Join tblExhibitingCompany as e On p.MexhID=e.ec_ID Where p.Pro_ID='" + proCode + "' And e.deleteFlag=0";
                    DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        #region Log
                        string logInsert = "Insert Into tb_RegistrationProducts (VisitorRegNo, PCode) Values ('" + regPortalID + "','" + proCode + "')";
                        fn.ExecuteSQL(logInsert);
                        #endregion
                        string ec_name = dt.Rows[0]["ec_name"] != DBNull.Value ? dt.Rows[0]["ec_name"].ToString() : "";
                        string ec_ID = dt.Rows[0]["ec_ID"] != DBNull.Value ? dt.Rows[0]["ec_ID"].ToString() : "";
                        string ec_country = dt.Rows[0]["ec_country"] != DBNull.Value ? dt.Rows[0]["ec_country"].ToString() : "";
                        string ec_contactno = dt.Rows[0]["ec_contactno"] != DBNull.Value ? dt.Rows[0]["ec_contactno"].ToString() : "";
                        string ec_email = dt.Rows[0]["ec_email"] != DBNull.Value ? dt.Rows[0]["ec_email"].ToString() : "";
                        string boothID = dt.Rows[0]["ec_booth"] != DBNull.Value ? dt.Rows[0]["ec_booth"].ToString() : "";
                        string sqlbooth = "Select vb_name From tblVenue_Booth Where vb_ID='" + boothID + "'";
                        string booth = fn.GetDataByCommand(sqlbooth, "vb_name");
                        string boothNo = booth == "0" ? "" : booth;
                        string Pro_description = dt.Rows[0]["Pro_description"] != DBNull.Value ? dt.Rows[0]["Pro_description"].ToString() : "";
                        ht.Add("ExhibitorID", ec_ID);
                        ht.Add("Exhibitorname", ec_name);
                        ht.Add("Contact", ec_contactno);
                        ht.Add("Country", ec_country);
                        ht.Add("Email", ec_email);
                        ht.Add("BoothNo", boothNo);
                        ht.Add("ProductDetails", Pro_description);
                        rtnlist.Add(ht);

                        htt.Add("dataversion", 1);
                        htt.Add("Status", "200");
                        htt.Add("message", "success");
                        htt.Add("data", rtnlist);
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "No data.");
                        htt.Add("data", rtnlist);
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region ProductInfo (parameters [page=ProductScan, lang=1, RegPortalID=, MexhID=] from mobile side, will return back exhibitor details and product desc)
            if (page == "ProductInfo")
            {
                isExistAPI += 1;
                string regPortalID = string.Empty;
                string mexhID = string.Empty;
                if (Request.Form["RegPortalID"] != null)
                {
                    regPortalID = Request.Form["RegPortalID"].ToString();
                }
                if (Request.Form["MexhID"] != null)
                {
                    mexhID = Request.Form["MexhID"].ToString();
                }

                if (Request.QueryString["RegPortalID"] != null)
                {
                    regPortalID = Request.QueryString["RegPortalID"].ToString();
                }
                if (Request.QueryString["MexhID"] != null)
                {
                    mexhID = Request.QueryString["MexhID"].ToString();
                }
                Hashtable ht = new Hashtable();
                try
                {
                    string sqlUser = "Select * From tb_ProductTable as p Inner Join tblExhibitingCompany as e On p.MexhID=e.ec_ID "
                                    + " Left Join tb_RegistrationProducts as rp On p.pro_id=rp.PCode Where p.MexhID='" + mexhID + "' And e.deleteFlag=0";
                    DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        //string ec_name = dt.Rows[0]["ec_name"] != DBNull.Value ? dt.Rows[0]["ec_name"].ToString() : "";
                        string ec_ID = dt.Rows[0]["ec_ID"] != DBNull.Value ? dt.Rows[0]["ec_ID"].ToString() : "";
                        //string ec_country = dt.Rows[0]["ec_country"] != DBNull.Value ? dt.Rows[0]["ec_country"].ToString() : "";
                        //string ec_contactno = dt.Rows[0]["ec_contactno"] != DBNull.Value ? dt.Rows[0]["ec_contactno"].ToString() : "";
                        //string ec_email = dt.Rows[0]["ec_email"] != DBNull.Value ? dt.Rows[0]["ec_email"].ToString() : "";
                        //string boothID = dt.Rows[0]["ec_booth"] != DBNull.Value ? dt.Rows[0]["ec_booth"].ToString() : "";
                        //string sqlbooth = "Select vb_name From tblVenue_Booth Where vb_ID='" + boothID + "'";
                        //string booth = fn.GetDataByCommand(sqlbooth, "vb_name");
                        //string boothNo = booth == "0" ? "" : booth;
                        string Pro_description = dt.Rows[0]["Pro_description"] != DBNull.Value ? dt.Rows[0]["Pro_description"].ToString() : "";
                        string Pro_name = dt.Rows[0]["Pro_name"] != DBNull.Value ? dt.Rows[0]["Pro_name"].ToString() : "";
                        string PCode = dt.Rows[0]["PCode"] != DBNull.Value ? dt.Rows[0]["PCode"].ToString() : "";
                        string VisitorRegNo = dt.Rows[0]["VisitorRegNo"] != DBNull.Value ? dt.Rows[0]["VisitorRegNo"].ToString() : "";
                        ht.Add("ExhibitorID", ec_ID);
                        //ht.Add("Exhibitorname", ec_name);
                        //ht.Add("Contact", ec_contactno);
                        //ht.Add("Country", ec_country);
                        //ht.Add("Email", ec_email);
                        //ht.Add("BoothNo", boothNo);
                        ht.Add("ProductName", Pro_name);
                        ht.Add("ProductDetails", Pro_description);
                        ht.Add("Sanned", (!string.IsNullOrEmpty(PCode) && VisitorRegNo == regPortalID ? "True" : "False"));
                        rtnlist.Add(ht);

                        htt.Add("dataversion", 1);
                        htt.Add("Status", "200");
                        htt.Add("message", "success");
                        htt.Add("data", rtnlist);
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "No data.");
                        htt.Add("data", rtnlist);
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region ProductInfo (parameters [page=ScannedExhProductInfo, lang=1, RegPortalID=, MexhIDs=] from mobile side, will return back exhibitor details and product desc)
            if (page == "ScannedExhProductInfo")
            {
                isExistAPI += 1;
                string regPortalID = string.Empty;
                string mexhIDs = string.Empty;
                string mexhIDsWhereClause = string.Empty;
                if (Request.Form["RegPortalID"] != null)
                {
                    regPortalID = Request.Form["RegPortalID"].ToString();
                }
                if (Request.Form["MexhIDs"] != null)
                {
                    mexhIDs = Request.Form["MexhIDs"].ToString();
                }

                if (Request.QueryString["RegPortalID"] != null)
                {
                    regPortalID = Request.QueryString["RegPortalID"].ToString();
                }
                if (Request.QueryString["MexhIDs"] != null)
                {
                    mexhIDs = Request.QueryString["MexhIDs"].ToString();
                }
                try
                {
                    if (!string.IsNullOrEmpty(mexhIDs))
                    {
                        mexhIDsWhereClause = " And e.ec_ID Not In ('" + mexhIDs.Replace(",", "','") + "')";
                    }
                    string sqlUser = "Select Distinct ec_ID,Pro_description,Pro_name,PCode,VisitorRegNo,e.ec_name,e.ec_country,e.ec_booth,'True' as Scanned "
                                        + " From tb_RegistrationProducts as rp Inner Join tb_ProductTable as p On rp.PCode=p.Pro_id "
                                        + " Inner Join tblExhibitingCompany as e On p.MexhID=e.ec_ID Where rp.VisitorRegNo='" + regPortalID + "' " + mexhIDsWhereClause + " And e.deleteFlag=0";
                    DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dre in dt.Rows)
                        {
                            Hashtable ht = new Hashtable();
                            string ec_name = dre["ec_name"] != DBNull.Value ? dre["ec_name"].ToString() : "";
                            string ec_ID = dre["ec_ID"] != DBNull.Value ? dre["ec_ID"].ToString() : "";
                            string ec_country = dre["ec_country"] != DBNull.Value ? dre["ec_country"].ToString() : "";
                            //string ec_contactno = dre["ec_contactno"] != DBNull.Value ? dre["ec_contactno"].ToString() : "";
                            //string ec_email = dre["ec_email"] != DBNull.Value ? dre["ec_email"].ToString() : "";
                            string boothID = dre["ec_booth"] != DBNull.Value ? dre["ec_booth"].ToString() : "";
                            string sqlbooth = "Select vb_name From tblVenue_Booth Where vb_ID='" + boothID + "'";
                            string booth = fn.GetDataByCommand(sqlbooth, "vb_name");
                            string boothNo = booth == "0" ? "" : booth;
                            string Pro_description = dre["Pro_description"] != DBNull.Value ? dre["Pro_description"].ToString() : "";
                            string Pro_name = dre["Pro_name"] != DBNull.Value ? dre["Pro_name"].ToString() : "";
                            string PCode = dre["PCode"] != DBNull.Value ? dre["PCode"].ToString() : "";
                            string VisitorRegNo = dre["VisitorRegNo"] != DBNull.Value ? dre["VisitorRegNo"].ToString() : "";
                            string Scanned = dre["Scanned"] != DBNull.Value ? dre["Scanned"].ToString() : "";
                            ht.Add("ExhibitorID", ec_ID);
                            //ht.Add("Exhibitorname", ec_name);
                            //ht.Add("Contact", ec_contactno);
                            //ht.Add("Country", ec_country);
                            //ht.Add("Email", ec_email);
                            //ht.Add("BoothNo", boothNo);
                            ht.Add("Exhibitorname", ec_name);
                            ht.Add("Country", ec_country);
                            ht.Add("BoothNo", boothNo);
                            ht.Add("ProductName", Pro_name);
                            ht.Add("ProductDetails", Pro_description);
                            ht.Add("Sanned", Scanned);
                            rtnlist.Add(ht);
                        }
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "200");
                        htt.Add("message", "success");
                        htt.Add("data", rtnlist);
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "No data.");
                        htt.Add("data", rtnlist);
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion
            
            #region ProductInfo (parameters [page=ProductScan, lang=1, RegPortalID=, MexhID=] from mobile side, will return back exhibitor details and product desc)
            if (page == "ExhInfoWhoScannedMyProduct")
            {
                isExistAPI += 1;
                string exhIDs = string.Empty;
                string exhIDsWhereClause = "";
                if (Request.Form["ExhIDs"] != null)
                {
                    exhIDs = Request.Form["ExhIDs"].ToString();
                }
                if (Request.QueryString["ExhIDs"] != null)
                {
                    exhIDs = Request.QueryString["ExhIDs"].ToString();
                }

                try
                {
                    if (!string.IsNullOrEmpty(exhIDs))
                    {
                        exhIDsWhereClause = " Where p.ExhID In ('" + exhIDs.Replace(",", "','") + "')";
                    }
                    string sqlUser = "Select r.VisitorRegNo,p.ExhID,* From tb_RegistrationProducts as r Inner Join tb_ProductTable as p On r.PCode=p.Pro_id "
                                        + " Inner Join tblExhibitingCompany as e On p.MexhID=e.ec_ID Left Join tblAccount as ac On r.VisitorRegNo=ac.RegPortalID " + exhIDsWhereClause;
                    DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            Hashtable ht = new Hashtable();
                            //string ec_name = dr["ec_name"] != DBNull.Value ? dr["ec_name"].ToString() : "";
                            string ec_ID = dr["ExhID"] != DBNull.Value ? dr["ExhID"].ToString() : "";
                            //string ec_country = dr["ec_country"] != DBNull.Value ? dr["ec_country"].ToString() : "";
                            //string ec_contactno = dr["ec_contactno"] != DBNull.Value ? dr["ec_contactno"].ToString() : "";
                            //string ec_email = dr["ec_email"] != DBNull.Value ? dr["ec_email"].ToString() : "";
                            //string boothID = dr["ec_booth"] != DBNull.Value ? dr["ec_booth"].ToString() : "";
                            //string sqlbooth = "Select vb_name From tblVenue_Booth Where vb_ID='" + boothID + "'";
                            //string booth = fn.GetDataByCommand(sqlbooth, "vb_name");
                            //string boothNo = booth == "0" ? "" : booth;
                            //string Pro_description = dr["Pro_description"] != DBNull.Value ? dr["Pro_description"].ToString() : "";
                            //string Pro_name = dr["Pro_name"] != DBNull.Value ? dr["Pro_name"].ToString() : "";
                            //string PCode = dr["PCode"] != DBNull.Value ? dr["PCode"].ToString() : "";
                            string VisitorRegNo = dr["VisitorRegNo"] != DBNull.Value ? dr["VisitorRegNo"].ToString() : "";
                            string CreatedDate = dr["CreatedDate"] != DBNull.Value ? dr["CreatedDate"].ToString() : "";

                            string a_fullname = dr["a_fullname"] != DBNull.Value ? dr["a_fullname"].ToString() : "";
                            string a_Mobile = dr["a_Mobile"] != DBNull.Value ? dr["a_Mobile"].ToString() : "";
                            string a_email = dr["a_email"] != DBNull.Value ? dr["a_email"].ToString() : "";
                            string a_designation = dr["a_designation"] != DBNull.Value ? dr["a_designation"].ToString() : "";
                            string a_company = dr["a_company"] != DBNull.Value ? dr["a_company"].ToString() : "";
                            string a_country = dr["a_country"] != DBNull.Value ? dr["a_country"].ToString() : "";
                            ht.Add("ExhibitorID", ec_ID);
                            //ht.Add("Exhibitorname", ec_name);
                            //ht.Add("Contact", ec_contactno);
                            //ht.Add("Country", ec_country);
                            //ht.Add("Email", ec_email);
                            //ht.Add("BoothNo", boothNo);
                            //ht.Add("ProductName", Pro_name);
                            //ht.Add("ProductDetails", Pro_description);
                            ht.Add("VisitorRegNo", VisitorRegNo);
                            ht.Add("CreatedDate", CreatedDate);

                            ht.Add("Visitor_name", a_fullname);
                            ht.Add("visitor_Phone", a_Mobile);
                            ht.Add("visitor_Email", a_email);
                            ht.Add("visitor_designation", a_designation);
                            ht.Add("visitor_company", a_company);
                            ht.Add("visitor_contact", a_country);

                            rtnlist.Add(ht);
                        }

                        htt.Add("dataversion", 1);
                        htt.Add("Status", "200");
                        htt.Add("message", "success");
                        htt.Add("data", rtnlist);
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "No data.");
                        htt.Add("data", rtnlist);
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region SubmitInterestedExhibitor
            if (page == "SubmitInterestedExhibitor")
            {
                isExistAPI += 1;
                string userID = string.Empty;
                string exhibitorList = string.Empty;
                if (Request.Form["UserID"] != null && Request.Form["ExhibitorList"] != null)
                {
                    userID = Request.Form["UserID"].ToString();
                    exhibitorList = Request.Form["ExhibitorList"].ToString();
                }
                if (Request.QueryString["UserID"] != null && Request.QueryString["ExhibitorList"] != null)
                {
                    userID = Request.QueryString["UserID"].ToString();
                    exhibitorList = Request.QueryString["ExhibitorList"].ToString();
                }
                DataTable dtUser = daldata.getAccountlstByID(userID);
                if (dtUser.Rows.Count > 0)
                {
                    try
                    {
                        string username = dtUser.Rows[0]["a_fullname"] != DBNull.Value ? dtUser.Rows[0]["a_fullname"].ToString() : "";
                        string interestedExhList = "";
                        string[] arrStr1 = exhibitorList.Split('@');
                        if (arrStr1 != null && arrStr1.Length > 0)
                        {
                            string organizerEmail = "yangkai@corpit.com.sg";
                            interestedExhList = "<table>";
                            for (int i = 0; i < arrStr1.Length; i++)
                            {
                                string exhName = arrStr1[i].ToString();
                                string sqlInsert = "Insert Into tb_BusinessMatchInterestedList (VisitorCode, ExhName) Values ('" 
                                    + userID + "','" + exhName + "')";
                                fn.ExecuteSQL(sqlInsert);
                                interestedExhList += "<tr><td><span style='font-family:arial,helvetica,sans-serif;'><span style='font-size: 13px;'>" + (i + 1) + "</span></td>"
                                                    + "<td><span style='font-family:arial,helvetica,sans-serif;'><span style='font-size: 13px;'>" + exhName + "</span></td></tr>";
                            }
                            interestedExhList += "</table>";
                            SendEmail sEmail = new SendEmail();
                            string isOK = sEmail.sEmailSendInterestedExhList(username, interestedExhList, organizerEmail);
                            if(isOK == "T")
                            {
                                htt.Add("dataversion", 1);
                                htt.Add("Status", "200");
                                htt.Add("message", "Success.");
                            }
                            else
                            {
                                htt.Add("dataversion", 1);
                                htt.Add("Status", "204");
                                htt.Add("message", "Email sending fail.");
                            }
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "206");
                            htt.Add("message", "No exhibitor(s).");
                        }
                    }
                    catch (Exception ex)
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "202");
                        htt.Add("message", "Error.");
                    }
                }
                else
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "204");
                    htt.Add("message", "Invalid user id.");
                }
            }
            #endregion

            #region InterestedExhibitorList
            if (page == "InterestedExhibitorList")
            {
                isExistAPI += 1;
                string userID = string.Empty;
                string delPassword = string.Empty;
                if (Request.Form["UserID"] != null)
                {
                    userID = Request.Form["UserID"].ToString();
                }
                if (Request.QueryString["UserID"] != null)
                {
                    userID = Request.QueryString["UserID"].ToString();
                }
                Hashtable ht = new Hashtable();
                try
                {
                    if (!string.IsNullOrEmpty(userID))
                    {
                        string sqlUser = "Select * From tb_BusinessMatchInterestedList as b Inner Join tblAccount as a On b.VisitorCode=a.a_ID Where a_ID='" + userID + "'";
                        DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                        if (dt.Rows.Count > 0)
                        {
                            string userid = dt.Rows[0]["a_ID"].ToString();
                            //string a_type = dt.Rows[0]["a_type"] != DBNull.Value ? dt.Rows[0]["a_type"].ToString() : "";
                            string a_fullname = dt.Rows[0]["a_fullname"] != DBNull.Value ? dt.Rows[0]["a_fullname"].ToString() : "Delegate";
                            //string a_designation = dt.Rows[0]["a_designation"] != DBNull.Value ? dt.Rows[0]["a_designation"].ToString() : "";
                            //string a_email = dt.Rows[0]["a_email"] != DBNull.Value ? dt.Rows[0]["a_email"].ToString() : "";
                            //string a_company = dt.Rows[0]["a_company"] != DBNull.Value ? dt.Rows[0]["a_company"].ToString() : "";
                            ht.Add("UserID", userid);
                            //ht.Add("UserType", a_type);
                            ht.Add("FullName", a_fullname);
                            //ht.Add("Designation", a_designation);
                            //ht.Add("Email", a_email);
                            //ht.Add("Company", a_company);
                            ArrayList rtnlistInterestedExhList = new ArrayList();
                            foreach (DataRow drTopExhList in dt.Rows)
                            {
                                Hashtable htInterestedExhList = new Hashtable();
                                string exhName = drTopExhList["ExhName"] != DBNull.Value ? drTopExhList["ExhName"].ToString() : "";
                                htInterestedExhList.Add("ExhibitorName", exhName);
                                rtnlistInterestedExhList.Add(htInterestedExhList);
                            }
                            ht.Add("InterestedExhibitorList", rtnlistInterestedExhList);

                            rtnlist.Add(ht);

                            htt.Add("dataversion", 1);
                            htt.Add("Status", "200");
                            htt.Add("message", "success");
                            htt.Add("data", rtnlist);
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "206");
                            htt.Add("message", "No data.");
                            htt.Add("data", rtnlist);
                        }
                    }
                    else
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "204");
                        htt.Add("message", "Invalid user id.");
                        htt.Add("data", rtnlist);
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                    htt.Add("data", rtnlist);
                }
            }
            #endregion

            #region SendEmail
            if (page == "SendEmail")
            {
                isExistAPI += 1;
                string UserID = string.Empty;
                string toemailAddress = string.Empty;
                string emailTitle = string.Empty;
                string emailContent = string.Empty;
                string deviceName = string.Empty;
                string productID = string.Empty;
                if (Request.Form["UserID"] != null)
                {
                    UserID = Request.Form["UserID"].ToString();
                }
                if (Request.Form["Email"] != null)
                {
                    toemailAddress = Request.Form["Email"].ToString();
                }
                if (Request.Form["EmailTitle"] != null)
                {
                    emailTitle = Request.Form["EmailTitle"].ToString();
                }
                if (Request.Form["EmailContent"] != null)
                {
                    emailContent = Request.Form["EmailContent"].ToString();
                }

                if (Request.QueryString["UserID"] != null)
                {
                    UserID = Request.QueryString["UserID"].ToString();
                }
                if (Request.QueryString["Email"] != null)
                {
                    toemailAddress = Request.QueryString["Email"].ToString();
                }
                if (Request.QueryString["EmailTitle"] != null)
                {
                    emailTitle = Request.QueryString["EmailTitle"].ToString();
                }
                if (Request.QueryString["EmailContent"] != null)
                {
                    emailContent = Request.QueryString["EmailContent"].ToString();
                }
                #region details
                if (Request.Form["deviceName"] != null)
                {
                    deviceName = Request.Form["deviceName"].ToString();
                }
                if (Request.QueryString["deviceName"] != null)
                {
                    deviceName = Request.QueryString["deviceName"].ToString();
                }
                #endregion

                DataTable dtAccount = daldata.getAccountlstByID(UserID);
                if (dtAccount.Rows.Count > 0)
                {
                    string name = dtAccount.Rows[0]["a_fullname"] != DBNull.Value ? dtAccount.Rows[0]["a_fullname"].ToString() : "";
                    string replyemail = dtAccount.Rows[0]["a_email"] != DBNull.Value ? dtAccount.Rows[0]["a_email"].ToString() : "";
                    if (!string.IsNullOrEmpty(toemailAddress) && !string.IsNullOrEmpty(emailTitle) && !string.IsNullOrEmpty(emailContent))
                    {
                        string isEmailSend = "";
                        try
                        {
                            SendEmail sEamil = new SendEmail();
                            isEmailSend = sEamil.sEmailSendAPI(emailTitle, emailContent, toemailAddress, replyemail);
                            if (isEmailSend == "T")
                            {
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                            else
                            {
                                htt.Add("Status", "205");
                                htt.Add("message", "email sending fail");
                            }
                        }
                        catch(Exception ex)
                        {
                            htt.Add("Status", "205");
                            htt.Add("message", "email sending fail");
                        }

                        #region Log
                        try
                        {
                            string logInsert = "Insert Into tb_Log_Email (EmailTitle, EmailContent, ToEmailAddress, AccountID, SendStatus) Values ('"
                                + emailTitle + "','" + emailContent + "','" + toemailAddress + "','" + UserID + "','" + isEmailSend + "')";
                            fn.ExecuteSQL(logInsert);
                        }
                        catch(Exception ex)
                        { }
                        #endregion
                    }
                    else
                    {
                        htt.Add("Status", "202");
                        htt.Add("message", "no email address");
                    }
                }
                else
                {
                    htt.Add("Status", "404");
                    htt.Add("message", "invalid id");
                }
            }
            #endregion

            #region setBookmark
            //if (page == "setBookmark")
            //{
            //    string pid = string.Empty;
            //    if (page == "setBookmark")
            //    {
            //        var pp = "Bookmark";
            //        pid = dalp.SelectByName(pp).p_id;
            //        if (Request.Form["personID"] != null && Request.Form["Eid"] != null)
            //        {
            //            string VisitorID = Request.Form["personID"].ToString();
            //            string ExhibitorID = Request.Form["Eid"].ToString();
            //            List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
            //            lstBackendChild = dalc.SelectByPId(pid);
            //            string rkey = string.Empty; string rid = string.Empty;
            //            rkey = gk.getKey("RID");
            //            rid = "RID" + rkey;
            //            string rowid = rid;

            //            int isSuccess = 0;
            //            int checkExist = dalf.CheckBookmark(VisitorID, ExhibitorID);
            //            if (checkExist == 0)
            //            {
            //                //bmark_userid  bmark_exhid
            //                foreach (var l in lstBackendChild)
            //                {
            //                    string content = string.Empty;

            //                    if (l.c_colname == "bmark_userid")
            //                    {
            //                        content = VisitorID;
            //                    }
            //                    else if (l.c_colname == "bmark_exhid")
            //                    {
            //                        content = ExhibitorID;
            //                    }
            //                    string fkey = string.Empty; string fid = string.Empty;
            //                    fkey = gk.getKey("FE");
            //                    fid = "FE" + fkey;

            //                    FrontEnd fe = new FrontEnd()
            //                    { f_id = fid, f_pid = pid, f_label = l.c_label, f_entrycount = 1, f_inputtype = l.c_inputtype, f_content = content, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = l.c_sortorder, f_c_id = l.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
            //                    isSuccess = dalf.Save(fe);
            //                    gk.SaveKey("RID", int.Parse(rkey));
            //                    gk.SaveKey("FE", int.Parse(fkey));
            //                }

            //                #region timestamp  
            //                DateTime updtime = DateTime.Now;
            //                string updatetimestamp = dalf.ConvertToTimestamp(updtime);
            //                int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
            //                #endregion

            //                if (isSuccess == 1)
            //                {
            //                    htt = new Hashtable();
            //                    htt.Add("Status", "ok");
            //                }

            //            }
            //            else
            //            {
            //                htt = new Hashtable();
            //                htt.Add("Status", "have");
            //            }
            //        }
            //    }
            //}
            #endregion

            #region removeBookmark

            //if (page == "removeBookmark")
            //{
            //    string pid = string.Empty;
            //    var pp = "Bookmark";
            //    pid = dalp.SelectByName(pp).p_id;
            //    if (Request.Form["personID"] != null && Request.Form["Eid"] != null)
            //    {
            //        string VisitorID = Request.Form["personID"].ToString();
            //        string ExhibitorID = Request.Form["Eid"].ToString();
            //        List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
            //        lstBackendChild = dalc.SelectByPId(pid);

            //        int checkExist = dalf.CheckBookmark(VisitorID, ExhibitorID);
            //        if (checkExist != 0)
            //        {
            //            var bmark_userid = (from aa in db.FrontEnds
            //                                where aa.f_content == VisitorID && aa.f_pid == "BP10040"
            //                                select aa.f_c_rowid).FirstOrDefault();

            //            var bmark_exhid = (from aa in db.FrontEnds
            //                               where aa.f_content == ExhibitorID && aa.f_pid == "BP10040"
            //                               select aa.f_c_rowid).FirstOrDefault();
            //            if (bmark_userid == bmark_exhid)
            //            {
            //                string delrowid = bmark_userid;
            //                dalf.deleteByRowID(delrowid);
            //            }

            //        }

            //        htt = new Hashtable();
            //        htt.Add("Status", "ok");
            //    }
            //}
            #endregion

            #region MyBookmark
            //else if (page == "myBookmark")
            //{
            //    if (Request.Form["Uid"] != null)
            //    {
            //        string pid = dalp.SelectByName("Bookmark").p_id;
            //        string vid = Request.Form["Uid"].ToString();
            //        DataTable dt = dalf.BindTableForGridByCondition(pid, "User ", vid);
            //        if (dt.Rows.Count > 0)
            //        {
            //            ArrayList arrCol = new ArrayList();


            //            foreach (DataRow dr in dt.Rows)
            //            {
            //                Hashtable ht = new Hashtable();
            //                foreach (DataColumn dc in dt.Columns)
            //                {
            //                    ht.Add(dc.ColumnName, dr[dc.ColumnName]);
            //                }

            //                rtnlist.Add(ht);
            //            }
            //            htt.Add("data", rtnlist);
            //            htt.Add("Status", "200");
            //            htt.Add("message", "success");
            //        }
            //    }
            //}
            #endregion

            #region Forget Password
            //if (page == "forgetpassword")
            //{
            //    htt = new Hashtable();
            //    htt.Add("Status", "401");
            //    htt.Add("message", "fail");

            //    string pid = string.Empty;
            //    var pp = "Account";
            //    pid = dalp.SelectByName(pp).p_id;
            //    if (Request.Form["email"] != null || Request.QueryString["email"] != null)
            //    {
            //        string EmailAddress = string.Empty;
            //        if (Request.Form["email"] != null)
            //        {
            //            EmailAddress = Request.Form["email"].ToString();
            //        }

            //        if (Request.QueryString["email"] != null)
            //        {
            //            EmailAddress = Request.QueryString["email"].ToString();
            //        }

            //        if (!string.IsNullOrEmpty(EmailAddress))
            //        {
            //            List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
            //            lstBackendChild = dalc.SelectByPId(pid);

            //            string column_id = string.Empty;

            //            foreach (var l in lstBackendChild)
            //            {
            //                if (l.c_label == "Email")
            //                {
            //                    column_id = l.c_id;
            //                }
            //            }

            //            List<FrontEnd> lstFrontEnd = dalf.selectbyPIDCID(pid, column_id);

            //            FrontEnd fResult = new FrontEnd();
            //            foreach (var f in lstFrontEnd)
            //            {
            //                if (f.f_content.ToLower() == EmailAddress.ToLower())
            //                {
            //                    fResult = f;
            //                }
            //            }

            //            if (fResult != null)
            //            {
            //                var lstresult2 = dalf.SelectByRowidPiD(fResult.f_c_rowid, pid);
            //                if (lstresult2.Count > 0)
            //                {
            //                    string fullname = string.Empty;
            //                    string username = string.Empty;
            //                    string password = string.Empty;
            //                    foreach (var l in lstresult2)
            //                    {
            //                        if (l.f_label == "Full Name")
            //                        {
            //                            fullname = l.f_content;
            //                        }

            //                        if (l.f_label == "Login Name")
            //                        {
            //                            username = l.f_content;
            //                        }

            //                        if (l.f_label == "Password")
            //                        {
            //                            password = l.f_content;
            //                        }
            //                    }

            //                    string userID = string.Empty;
            //                    string status = string.Empty;
            //                    string parameters = string.Empty;
            //                    SendEmail se = new SendEmail();
            //                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            //                    {
            //                        parameters = "@Name|" + fullname + "^@loginName|" + username + "^@loginPassword|" + password;

            //                        status = se.SendEmailViaPortal(userID, EmailAddress, parameters, "ACC", "", "");
            //                        if (status == "OK")
            //                        {
            //                            htt = new Hashtable();
            //                            htt.Add("Status", "200");
            //                            htt.Add("message", "success");
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //    }
            //}
            #endregion

            #region NFCReaderInfoSaving
            if (page == "NFCReaderInfoSaving")
            {
                isExistAPI += 1;
                string regno = string.Empty;
                string nfcID_autogeneratedID = string.Empty;
                string nfcID_NDEFID = string.Empty;
                if (Request.Form["Regno"] != null)
                {
                    regno = Request.Form["Regno"].ToString();
                }
                if (Request.Form["NFCAugeneratedID"] != null)
                {
                    nfcID_autogeneratedID = Request.Form["NFCAugeneratedID"].ToString();
                }
                if (Request.Form["NFCNDFID"] != null)
                {
                    nfcID_NDEFID = Request.Form["NFCNDFID"].ToString();
                }

                if (Request.QueryString["Regno"] != null)
                {
                    regno = Request.QueryString["Regno"].ToString();
                }
                if (Request.QueryString["NFCAugeneratedID"] != null)
                {
                    nfcID_autogeneratedID = Request.QueryString["NFCAugeneratedID"].ToString();
                }
                if (Request.QueryString["NFCNDFID"] != null)
                {
                    nfcID_NDEFID = Request.QueryString["NFCNDFID"].ToString();
                }
                Hashtable ht = new Hashtable();
                try
                {
                    string sqlUser = "Select * From tblAccountNFCInfo Where Regno='" + regno + "'";
                    DataTable dt = fn.GetDatasetByCommand(sqlUser, "ds").Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        htt.Add("dataversion", 1);
                        htt.Add("Status", "409");
                        htt.Add("message", "Already assigned.");
                    }
                    else
                    {
                        int isSuccess = saveNFCInfo(regno, nfcID_autogeneratedID, nfcID_NDEFID);
                        if (isSuccess > 0)
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "200");
                            htt.Add("message", "Success.");
                        }
                        else
                        {
                            htt.Add("dataversion", 1);
                            htt.Add("Status", "202");
                            htt.Add("message", "Error.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    htt.Add("dataversion", 1);
                    htt.Add("Status", "202");
                    htt.Add("message", "Error.");
                }
            }
            #endregion

            if (isExistAPI == 0)
            {
                htt = new Hashtable();
                htt.Add("Status", "204");
                htt.Add("message", "no api");
                htt.Add("data", rtnlist);
            }

            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(htt);
            Response.Write(jsonStr);

            #region OLD
            /*
             if (page == "Conference Speakers")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_ConferenceSpeaker", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();
                                    ht.Add("ID", dr["GUID"].ToString());
                                    ht.Add("cspeaker_category", dr["cspeaker_category"]);
                                    ht.Add("cspeaker_conferenceID", dr["cspeaker_conferenceID"]);
                                    ht.Add("cspeaker_speakerID", dr["cspeaker_speakerID"]);
                                    ht.Add("cspeaker_confcategory", dr["cspeaker_confcategory"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }

                        }
                        else if (page == "Conference")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_Conference", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();

                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("c_date", dr["c_date"]);
                                    ht.Add("c_year", dr["c_year"]);
                                    ht.Add("c_cc", dr["c_cc"]);
                                    ht.Add("c_title", dr["c_title"]);
                                    ht.Add("c_brief", dr["c_brief"]);
                                    ht.Add("c_liveQA", dr["c_liveQA"]);
                                    ht.Add("c_stime", dr["c_stime"]);
                                    ht.Add("c_etime", dr["c_etime"]);
                                    ht.Add("c_parentconfID", dr["c_parentconfID"]);
                                    ht.Add("c_venue", dr["c_venue"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);


                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else if(page == "Event")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_Event", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();

                                    ht.Add("c_brief", dr["c_brief"]);
                                    ht.Add("c_image", dr["c_image"]);
                                    ht.Add("c_title", dr["c_title"]);
                                    ht.Add("c_date", dr["c_date"]);
                                    ht.Add("c_venue", dr["c_venue"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("c_time", dr["c_time"]);
                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else if (page == "Exhibiting Company")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_ExhibitingCompany", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();

                                    ht.Add("ec_website", dr["ec_website"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    ht.Add("ec_country", dr["ec_country"]);
                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("ec_booth", dr["ec_booth"]);
                                    ht.Add("ec_name", dr["ec_name"]);
                                    ht.Add("ec_ecompanytype", dr["ec_ecompanytype"]);
                                    ht.Add("ec_companyprofile", dr["ec_companyprofile"]);
                                    ht.Add("ec_product", dr["ec_product"]);
                                    ht.Add("ec_email", dr["ec_email"]);
                                    ht.Add("c_hall", dr["c_hall"]);
                                    ht.Add("ec_logo", dr["ec_logo"]);
                                    ht.Add("ec_brochure", dr["ec_brochure"]);
                                    ht.Add("ec_address", dr["ec_address"]);
                                    ht.Add("contactpersonID", dr["contactpersonID"]);
                                    ht.Add("ec_contactno", dr["ec_contactno"]);

                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else if (page == "Venue (Booth)")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_VenueBooth", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();

                                    ht.Add("vb_y", dr["vb_y"]);
                                    ht.Add("vb_x", dr["vb_x"]);
                                    ht.Add("vb_name", dr["vb_name"]);
                                    ht.Add("vb_location", dr["vb_location"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    ht.Add("vb_height", dr["vb_height"]);
                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("vb_width", dr["vb_width"]);


                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else if (page == "Product")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_Product", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    ht.Add("p_category", dr["p_category"]);
                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("p_name", dr["p_name"]);

                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else if (page == "Speaker")
                        {
                            DataTable dt = fn.GetDatasetByCommand("select * from tb_Speaker", "sdt").Tables[0];
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    Hashtable ht = new Hashtable();
                                    ht.Add("s_bio", dr["s_bio"]);
                                    ht.Add("s_companay", dr["s_companay"]);
                                    ht.Add("s_country", dr["s_country"]);
                                    ht.Add("s_profilepic", dr["s_profilepic"]);
                                    ht.Add("s_email", dr["s_email"]);
                                    ht.Add("s_fullname", dr["s_fullname"]);
                                    ht.Add("UpdatedTime", dr["UpdatedTime"]);
                                    ht.Add("s_job", dr["s_job"]);
                                    ht.Add("ID", dr["ID"]);
                                    ht.Add("s_address", dr["s_address"]);
                                    ht.Add("s_mobile", dr["s_mobile"]);


                                    rtnlist.Add(ht);
                                }
                                htt.Add("data", rtnlist);
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                        else
                        {
                            #region Dynamic
                            if (page != "Configuration" && page != "Menu" && page != "ColorScheme" && page != "Login" && page != "setBookmark" && page != "removeBookmark" && page != "myBookmark" && page != "forgetpassword")
                            {
                                if (!string.IsNullOrEmpty(page))
                                {
                                    string pid = string.Empty;
                                    if (page == "PollingMobile")
                                    {
                                        var pp = "Conference";
                                        pid = dalp.SelectByName(pp).p_id;
                                    }
                                    else if (page == "BusinessMatching-Apply" || page == "BusinessMatching-Reply" || page == "BusinessMatchingListByVisitorID" || page == "BusinessMatchingListByExhibitorID")
                                    {
                                        var pp = "Business Matching";
                                        pid = dalp.SelectByName(pp).p_id;
                                    }
                                    else
                                    {
                                        pid = dalp.SelectByName(page).p_id;
                                    }


                                    #region BusinessMatching-Apply
                                    if (page == "BusinessMatching-Apply")
                                    {
                                        if (Request.Form["VisitorID"] != null && Request.Form["ExhibitorID"] != null && Request.Form["Date"] != null && Request.Form["Time"] != null)
                                        {
                                            #region Add Business Matching
                                            string VisitorID = Request.Form["VisitorID"].ToString();
                                            string ExhibitorID = Request.Form["ExhibitorID"].ToString();
                                            string Date = Request.Form["Date"].ToString();
                                            string Time = Request.Form["Time"].ToString();
                                            string ExhibitorProduct = Request.Form["ExhibitorProduct"] == null ? "" : Request.Form["ExhibitorProduct"].ToString();
                                            string VisitorComment = Request.Form["VisitorComment"] == null ? "" : Request.Form["VisitorComment"].ToString();
                                            List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
                                            lstBackendChild = dalc.SelectByPId(pid);

                                            string rkey = gk.getKey("RID");
                                            string rowid = "RID" + rkey;

                                            int isSuccess = 0;
                                            int checkExist = dalf.CheckBM(VisitorID, ExhibitorID, Date, Time);
                                            if (checkExist == 0)
                                            {
                                                foreach (var l in lstBackendChild)
                                                {
                                                    string content = string.Empty;

                                                    if (l.c_colname == "bm_vid")
                                                    {
                                                        content = VisitorID;
                                                    }
                                                    else if (l.c_colname == "bm_eid")
                                                    {
                                                        content = ExhibitorID;
                                                    }
                                                    else if (l.c_colname == "bm_date")
                                                    {
                                                        content = Date;
                                                    }
                                                    else if (l.c_colname == "bm_time")
                                                    {
                                                        content = Time;
                                                    }
                                                    else if (l.c_colname == "bm_status")
                                                    {
                                                        content = "0";
                                                    }
                                                    else if (l.c_colname == "bm_ecomment")
                                                    {
                                                        content = "";
                                                    }
                                                    else if (l.c_colname == "bm_vcomment")
                                                    {
                                                        content = VisitorComment;
                                                    }
                                                    else if (l.c_colname == "bm_interestedProduct")
                                                    {
                                                        content = ExhibitorProduct;
                                                    }
                                                    string fkey = string.Empty; string fid = string.Empty;
                                                    fkey = gk.getKey("FE");
                                                    fid = "FE" + fkey;
                                                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = l.c_label, f_entrycount = 1, f_inputtype = l.c_inputtype, f_content = content, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = l.c_sortorder, f_c_id = l.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
                                                    isSuccess = dalf.Save(fe);
                                                    gk.SaveKey("FE", int.Parse(fkey));
                                                    gk.SaveKey("RID", int.Parse(rkey));
                                                }

                                                #region timestamp  
                                                DateTime updtime = DateTime.Now;
                                                string updatetimestamp = dalf.ConvertToTimestamp(updtime);
                                                int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
                                                #endregion

                                                if (isSuccess == 1)
                                                {
                                                    htt = new Hashtable();
                                                    htt.Add("Status", "ok");
                                                }

                                            }
                                            else
                                            {
                                                htt = new Hashtable();
                                                htt.Add("Status", "have");
                                            }


                                            #endregion
                                        }


                                    }
                                    #endregion

                                    #region BusinessMatching-Reply
                                    else if (page == "BusinessMatchingApply")
                                    {
                                        if (Request.Form["BMID"] != null && Request.Form["Status"] != null)
                                        {
                                            #region Update Business Matching
                                            string BMID = Request.Form["BMID"].ToString();//rowid
                                            string Status = Request.Form["Status"].ToString();
                                            string ExhibitorComment = Request.Form["ExhibitorComment"].ToString() == null ? "" : Request.Form["ExhibitorComment"].ToString();
                                            string rowid = BMID;

                                            List<FrontEnd> lstFrontEnd = new List<FrontEnd>();
                                            lstFrontEnd = dalf.SelectByRowidPiD(rowid, pid);
                                            lstFrontEnd = dalf.selectFrontEndFromList(lstFrontEnd);
                                            if (lstFrontEnd.Count == 2)
                                            {
                                                int isSuccess = 0;
                                                int i = 0;
                                                foreach (var l in lstFrontEnd)
                                                {
                                                    if (i == 0)
                                                    {
                                                        FrontEnd fe = new FrontEnd()
                                                        { f_id = l.f_id, f_pid = l.f_pid, f_label = l.f_label, f_entrycount = 1, f_inputtype = l.f_inputtype, f_content = Status, f_c_isFinish = true, f_c_rowid = l.f_c_rowid, f_sortorder = l.f_sortorder, f_c_id = l.f_c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
                                                        isSuccess = dalf.update(fe);
                                                    }
                                                    else if (i == 1)
                                                    {
                                                        FrontEnd fe = new FrontEnd()
                                                        { f_id = l.f_id, f_pid = l.f_pid, f_label = l.f_label, f_entrycount = 1, f_inputtype = l.f_inputtype, f_content = ExhibitorComment, f_c_isFinish = true, f_c_rowid = l.f_c_rowid, f_sortorder = l.f_sortorder, f_c_id = l.f_c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
                                                        isSuccess = dalf.update(fe);
                                                    }
                                                    i++;
                                                }
                                                #region timestamp  
                                                DateTime updtime = DateTime.Now;
                                                string updatetimestamp = dalf.ConvertToTimestamp(updtime);
                                                int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
                                                #endregion

                                                if (isSuccess == 1)
                                                {
                                                    htt = new Hashtable();
                                                    htt.Add("Status", "ok");
                                                }
                                                else
                                                {
                                                    htt = new Hashtable();
                                                    htt.Add("Status", "fail");
                                                }

                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion

                                    #region BusinessMatchingListByVisitorID
                                    else if (page == "BusinessMatchingListByVisitorID")
                                    {
                                        if (Request.Form["VisitorID"] != null)
                                        {
                                            string vid = Request.Form["VisitorID"].ToString();
                                            DataTable dt = dalf.BindTableForGridByCondition(pid, "Visitor ID", vid);
                                            if (dt.Rows.Count > 0)
                                            {
                                                ArrayList arrCol = new ArrayList();


                                                foreach (DataRow dr in dt.Rows)
                                                {
                                                    Hashtable ht = new Hashtable();
                                                    foreach (DataColumn dc in dt.Columns)
                                                    {
                                                        ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                    }

                                                    rtnlist.Add(ht);
                                                }
                                                htt.Add("data", rtnlist);
                                                htt.Add("Status", "200");
                                                htt.Add("message", "success");
                                            }
                                        }
                                    }

                                    #endregion

                                    #region BusinessMatchingListByExhibitorID
                                    else if (page == "BusinessMatchingListByExhibitorID")
                                    {
                                        if (Request.Form["ExhibitorID"] != null)
                                        {
                                            string eid = Request.Form["ExhibitorID"].ToString();
                                            DataTable dt = dalf.BindTableForGridByCondition(pid, "Exhibitor ID", eid);
                                            if (dt.Rows.Count > 0)
                                            {
                                                ArrayList arrCol = new ArrayList();


                                                foreach (DataRow dr in dt.Rows)
                                                {
                                                    Hashtable ht = new Hashtable();
                                                    foreach (DataColumn dc in dt.Columns)
                                                    {
                                                        ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                    }

                                                    rtnlist.Add(ht);
                                                }
                                                htt.Add("data", rtnlist);
                                                htt.Add("Status", "200");
                                                htt.Add("message", "success");
                                            }
                                        }
                                    }
                                    #endregion

                                    #region Others                    
                                    else
                                    {
                                        DataTable dt = dalf.BindTableForGrid(pid, lang);
                                        if (dt.Rows.Count > 0)
                                        {
                                            ArrayList arrCol = new ArrayList();


                                            foreach (DataRow dr in dt.Rows)
                                            {
                                                Hashtable ht = new Hashtable();
                                                foreach (DataColumn dc in dt.Columns)
                                                {
                                                    if (page == "Configuration Images")
                                                    {
                                                        string a = dc.ColumnName.Replace(" ", "");
                                                        string b = dr[dc.ColumnName].ToString().Replace(" ", "");

                                                        ht.Add(a, b);
                                                    }
                                                    else if (page == "Sponsor")
                                                    {
                                                        ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                        if (dc.ColumnName == "s_category")
                                                        {
                                                            var aa = dr[dc.ColumnName].ToString();

                                                            string catid = dalf.selectIDbyContent(aa);
                                                            var seq = dalf.selectSponsorCategorySeqByID(catid);
                                                            ht.Add("s_cateogrysequence", seq);
                                                        }
                                                    }
                                                    else if (page == "Conference")
                                                    {
                                                        ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                        if (dc.ColumnName == "c_date")
                                                        {
                                                            var aa = dr[dc.ColumnName].ToString();
                                                            if (!string.IsNullOrEmpty(aa))
                                                            {
                                                                var yy = dalf.getyearbydate(aa);
                                                                ht.Add("c_year", yy);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                    }
                                                }
                                                if (page == "Inbox")
                                                {
                                                    ht.Add("from", "OTC Asia");
                                                }
                                                rtnlist.Add(ht);
                                            }
                                            htt.Add("data", rtnlist);
                                            htt.Add("Status", "200");
                                            htt.Add("message", "success");
                                        }
                                    }
                                    #endregion

                                }
                            }
                            #endregion

                            #region Static
                            else
                            {
                                #region Configuration
                                if (page == "Configuration")
                                {
                                    List<tb_Configuration> lstconfig = dalf.selectAllConfig();

                                    if (lstconfig.Count > 0)
                                    {
                                        Hashtable ht = new Hashtable();
                                        foreach (var item in lstconfig)
                                        {
                                            ht = new Hashtable();
                                            ht.Add("config_id", dr["config_id);
                                            ht.Add("systemLanguage", dr["config_language);
                                            ht.Add("showName", dr["config_name);
                                            ht.Add("showStartDate", dr["config_startDate);
                                            ht.Add("showEndDate", dr["config_endDate);
                                            ht.Add("enableAccount", dr["config_enableAccount);
                                            ht.Add("appIcon", dr["config_appIcon);
                                            ht.Add("showVenue", dr["config_venue);
                                            ht.Add("surveyStartDate", dr["config_surveyStartDate);
                                            ht.Add("surveyEndDate", dr["config_surveyEndDate);
                                            ht.Add("visitorRegURL", dr["config_visRegURL);
                                            ht.Add("homePageTemplate", dr["config_homePageLayout);
                                            ht.Add("iOSVersion", dr["config_iOSVersion);
                                            ht.Add("iOSDownloadLink", dr["config_iOsDownloadLink);
                                            ht.Add("AndroidVersion", dr["config_AndroidVersion);
                                            ht.Add("AndroidDownloadLink", dr["config_AndroidDownloadLink);
                                            ht.Add("timestamp", dr["updatedDate);
                                            rtnlist.Add(ht);
                                        }

                                        htt.Add("Status", "200");
                                        htt.Add("message", "success");
                                        htt.Add("data", rtnlist);

                                    }
                                    else
                                    {
                                        htt = new Hashtable();
                                        htt.Add("Status", "204");
                                        htt.Add("message", "no content");
                                        htt.Add("data", rtnlist);
                                    }
                                }
                                #endregion

                                #region Menu
                                else if (page == "Menu")
                                {
                                    List<tb_Menu> lstconfig = dalf.selectMobileMenu();

                                    if (lstconfig.Count > 0)
                                    {
                                        Hashtable ht = new Hashtable();
                                        string strPathAndQuery = HttpContext.Current.Request.Url.PathAndQuery;
                                        string uri = HttpContext.Current.Request.Url.AbsoluteUri.Replace(strPathAndQuery, "/");
                                        int c = uri.Length - 1;
                                        string mp = uri.Remove(c) + HttpContext.Current.Request.ApplicationPath;
                                        foreach (var item in lstconfig)
                                        {
                                            ht = new Hashtable();
                                            ht.Add("m_id", dr["m_id);
                                            ht.Add("m_TitleEn", dr["m_TitleEn);
                                            ht.Add("m_TitleCn", dr["m_TitleCn);
                                            ht.Add("m_OrderID", dr["m_OrderID);
                                            if (!string.IsNullOrEmpty(dr["m_IconUrl))
                                            {
                                                ht.Add("m_IconUrl", mp + "/sys_Images/Mobile/" + dr["m_IconUrl);
                                            }
                                            else
                                            {
                                                ht.Add("m_IconUrl", dr["m_IconUrl);
                                            }
                                            ht.Add("m_PageType", dr["m_PageType);
                                            ht.Add("m_parentMenuID", dr["m_parentMenuID);
                                            ht.Add("m_permissioncontrol", dr["m_permissioncontrol);
                                            ht.Add("m_menuType", dr["m_menuType);
                                            ht.Add("timestamp", dr["updatedDate);
                                            rtnlist.Add(ht);
                                        }

                                        htt.Add("Status", "200");
                                        htt.Add("message", "success");
                                        htt.Add("data", rtnlist);

                                    }
                                    else
                                    {
                                        htt = new Hashtable();
                                        htt.Add("Status", "204");
                                        htt.Add("message", "no content");
                                        htt.Add("data", rtnlist);
                                    }
                                }
                                #endregion

                                #region ColorScheme
                                if (page == "ColorScheme")
                                {
                                    List<tb_ColorScheme> lstconfig = dalf.SelectAllColorScheme();

                                    if (lstconfig.Count > 0)
                                    {
                                        Hashtable ht = new Hashtable();
                                        foreach (var item in lstconfig)
                                        {
                                            ht = new Hashtable();
                                            ht.Add("color_background_1", dr["CS_BG1);
                                            ht.Add("color_background_2", dr["CS_BG1);
                                            ht.Add("color_button_1", dr["CS_B1);
                                            ht.Add("color_button_2", dr["CS_B2);
                                            ht.Add("color_font_1", dr["CS_F1);
                                            ht.Add("color_font_2", dr["CS_F2);
                                            ht.Add("color_font_3", dr["CS_F3);
                                            ht.Add("color_column_1", dr["CS_C1);
                                            ht.Add("color_column_2", dr["CS_C2);
                                            ht.Add("color_margin_1", dr["CS_M1);
                                            ht.Add("color_margin_2", dr["CS_M2);
                                            ht.Add("timestamp", dr["updatedDate);
                                            rtnlist.Add(ht);
                                        }

                                        htt.Add("Status", "200");
                                        htt.Add("message", "success");
                                        htt.Add("data", rtnlist);

                                    }
                                    else
                                    {
                                        htt = new Hashtable();
                                        htt.Add("Status", "204");
                                        htt.Add("message", "no content");
                                        htt.Add("data", rtnlist);
                                    }
                                }
                                #endregion

                                #region Icon Images (Unused)
                                //if (page == "configImages")
                                //{
                                //    var lstconfigimg = dalf.SelectAllImages();
                                //    if (lstconfigimg.Count > 0)
                                //    {
                                //        ArrayList arrCol = new ArrayList();


                                //        foreach (var l in lstconfigimg)
                                //        {
                                //            Hashtable ht = new Hashtable();
                                //            ht.Add("imageURL", l.f_content);
                                //            ht.Add("imageKey", l.f_label);
                                //            //ht.Add("formname", dalp.SelectNameByID(l.f_pid.Value));
                                //            rtnlist.Add(ht);
                                //        }
                                //        htt.Add("data", rtnlist);
                                //        htt.Add("Status", "200");
                                //        htt.Add("message", "success");


                                //    }
                                //}                
                                #endregion

                                #region Login
                                if (page == "Login")
                                {
                                    string compName = string.Empty;
                                    string compEmail = string.Empty;
                                    string SurveyStatus = "0";
                                    string userType = string.Empty;

                                    string pid = dalp.SelectByName("Account").p_id;
                                    if (Request.Form["username"] != null)
                                    {
                                        compName = Request.Form["username"].ToString();
                                    }
                                    if (Request.Form["password"] != null)
                                    {
                                        compEmail = Request.Form["password"].ToString();
                                    }
                                    if (Request.Form["loginType"] != null)
                                    {
                                        userType = Request.Form["loginType"].ToString();
                                    }
                                    string rtn = dalf.LoginMobile(compName, compEmail, userType); //f_c_rowid + "^" + acctype + "^" + name;

                                    if (string.IsNullOrEmpty(rtn) && userType == "V")
                                    {
                                        #region Create New Visitor
                                        List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
                                        lstBackendChild = dalc.SelectByPId(pid);
                                        string rkey = string.Empty; string rid = string.Empty;
                                        rkey = gk.getKey("RID");
                                        rid = "RID" + rkey;
                                        string rowid = rid;
                                        foreach (var l in lstBackendChild)
                                        {
                                            string content = string.Empty;
                                            if (l.c_colname == "a_type")
                                            {
                                                content = "Visitor";
                                            }
                                            else if (l.c_colname == "a_sal")
                                            {
                                                content = "Mr";
                                            }
                                            else if (l.c_colname == "a_fname")
                                            {
                                                content = compName;
                                            }
                                            else if (l.c_colname == "a_lname")
                                            {
                                                content = "";
                                            }
                                            else if (l.c_colname == "a_designation")
                                            {
                                                content = "Nil";
                                            }
                                            else if (l.c_colname == "a_email")
                                            {
                                                content = compEmail;
                                            }
                                            else if (l.c_colname == "a_company")
                                            {
                                                content = compName;
                                            }
                                            else if (l.c_colname == "a_addressOfc")
                                            {
                                                content = "Mr";
                                            }
                                            else if (l.c_colname == "a_addressHome")
                                            {
                                                content = "Nil";
                                            }
                                            else if (l.c_colname == "a_country")
                                            {
                                                content = "Nil";
                                            }
                                            else if (l.c_colname == "a_Mobile")
                                            {
                                                content = "Nil";
                                            }
                                            else if (l.c_colname == "a_loginName")
                                            {
                                                content = compName;
                                            }
                                            else if (l.c_colname == "a_Password")
                                            {
                                                content = compEmail;
                                            }
                                            else if (l.c_colname == "a_isActivated")
                                            {
                                                content = "Yes";
                                            }
                                            string fkey = string.Empty; string fid = string.Empty;
                                            fkey = gk.getKey("FE");
                                            fid = "FE" + fkey;
                                            FrontEnd fe = new FrontEnd()
                                            { f_id = fid, f_pid = pid, f_label = l.c_label, f_entrycount = 1, f_inputtype = l.c_inputtype, f_content = content, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = l.c_sortorder, f_c_id = l.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
                                            int isSuccess = dalf.Save(fe);
                                            gk.SaveKey("RID", int.Parse(rkey));
                                            gk.SaveKey("FE", int.Parse(fkey));
                                        }
                                        #region timestamp  
                                        DateTime updtime = DateTime.Now;
                                        string updatetimestamp = dalf.ConvertToTimestamp(updtime);
                                        int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
                                        #endregion
                                        #endregion

                                        #region return 
                                        rtn = rowid + "^Visitor^Mr " + compName;
                                        SurveyQA sqa = dalf.SelectSurveyQAByID(rowid);
                                        if (sqa != null)
                                        {
                                            SurveyStatus = "1";
                                        }
                                        else
                                        {
                                            SurveyStatus = "0";
                                        }
                                        htt = new Hashtable();
                                        htt.Add("UserID", rowid);
                                        htt.Add("UserType", "Visitor");
                                        htt.Add("SurveyQA", SurveyStatus);
                                        htt.Add("Status", "200");
                                        htt.Add("message", "success");
                                        #endregion
                                    }
                                    else if (string.IsNullOrEmpty(rtn) && userType != "V")
                                    {
                                        htt = new Hashtable();
                                        htt.Add("UserID", "");
                                        htt.Add("UserType", userType);
                                        htt.Add("SurveyQA", "");
                                        htt.Add("Status", "401");
                                        htt.Add("message", "fail");
                                    }
                                    else
                                    {
                                        #region return
                                        string rowid = rtn.Split('^')[0];
                                        string usertype = rtn.Split('^')[1];
                                        SurveyQA sqa = dalf.SelectSurveyQAByID(rowid);
                                        if (sqa != null)
                                        {
                                            SurveyStatus = "1";
                                        }
                                        else
                                        {
                                            SurveyStatus = "0";
                                        }
                                        htt = new Hashtable();
                                        htt.Add("UserID", rowid);
                                        htt.Add("UserType", usertype);
                                        htt.Add("SurveyQA", SurveyStatus);
                                        htt.Add("Status", "200");
                                        htt.Add("message", "success");
                                        #endregion
                                    }
                                }
                                #endregion

                                #region setBookmark
                                if (page == "setBookmark")
                                {
                                    string pid = string.Empty;
                                    if (page == "setBookmark")
                                    {
                                        var pp = "Bookmark";
                                        pid = dalp.SelectByName(pp).p_id;
                                        if (Request.Form["personID"] != null && Request.Form["Eid"] != null)
                                        {
                                            string VisitorID = Request.Form["personID"].ToString();
                                            string ExhibitorID = Request.Form["Eid"].ToString();
                                            List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
                                            lstBackendChild = dalc.SelectByPId(pid);
                                            string rkey = string.Empty; string rid = string.Empty;
                                            rkey = gk.getKey("RID");
                                            rid = "RID" + rkey;
                                            string rowid = rid;

                                            int isSuccess = 0;
                                            int checkExist = dalf.CheckBookmark(VisitorID, ExhibitorID);
                                            if (checkExist == 0)
                                            {
                                                //bmark_userid  bmark_exhid
                                                foreach (var l in lstBackendChild)
                                                {
                                                    string content = string.Empty;

                                                    if (l.c_colname == "bmark_userid")
                                                    {
                                                        content = VisitorID;
                                                    }
                                                    else if (l.c_colname == "bmark_exhid")
                                                    {
                                                        content = ExhibitorID;
                                                    }
                                                    string fkey = string.Empty; string fid = string.Empty;
                                                    fkey = gk.getKey("FE");
                                                    fid = "FE" + fkey;

                                                    FrontEnd fe = new FrontEnd()
                                                    { f_id = fid, f_pid = pid, f_label = l.c_label, f_entrycount = 1, f_inputtype = l.c_inputtype, f_content = content, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = l.c_sortorder, f_c_id = l.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false, f_lang = 1 };
                                                    isSuccess = dalf.Save(fe);
                                                    gk.SaveKey("RID", int.Parse(rkey));
                                                    gk.SaveKey("FE", int.Parse(fkey));
                                                }

                                                #region timestamp  
                                                DateTime updtime = DateTime.Now;
                                                string updatetimestamp = dalf.ConvertToTimestamp(updtime);
                                                int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
                                                #endregion

                                                if (isSuccess == 1)
                                                {
                                                    htt = new Hashtable();
                                                    htt.Add("Status", "ok");
                                                }

                                            }
                                            else
                                            {
                                                htt = new Hashtable();
                                                htt.Add("Status", "have");
                                            }
                                        }
                                    }
                                }
                                #endregion

                                #region removeBookmark

                                if (page == "removeBookmark")
                                {
                                    string pid = string.Empty;
                                    var pp = "Bookmark";
                                    pid = dalp.SelectByName(pp).p_id;
                                    if (Request.Form["personID"] != null && Request.Form["Eid"] != null)
                                    {
                                        string VisitorID = Request.Form["personID"].ToString();
                                        string ExhibitorID = Request.Form["Eid"].ToString();
                                        List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
                                        lstBackendChild = dalc.SelectByPId(pid);

                                        int checkExist = dalf.CheckBookmark(VisitorID, ExhibitorID);
                                        if (checkExist != 0)
                                        {
                                            var bmark_userid = (from aa in db.FrontEnds
                                                                where aa.f_content == VisitorID && aa.f_pid == "BP10040"
                                                                select aa.f_c_rowid).FirstOrDefault();

                                            var bmark_exhid = (from aa in db.FrontEnds
                                                               where aa.f_content == ExhibitorID && aa.f_pid == "BP10040"
                                                               select aa.f_c_rowid).FirstOrDefault();
                                            if (bmark_userid == bmark_exhid)
                                            {
                                                string delrowid = bmark_userid;
                                                dalf.deleteByRowID(delrowid);
                                            }

                                        }

                                        htt = new Hashtable();
                                        htt.Add("Status", "ok");
                                    }
                                }
                                #endregion

                                #region MyBookmark
                                else if (page == "myBookmark")
                                {
                                    if (Request.Form["Uid"] != null)
                                    {
                                        string pid = dalp.SelectByName("Bookmark").p_id;
                                        string vid = Request.Form["Uid"].ToString();
                                        DataTable dt = dalf.BindTableForGridByCondition(pid, "User ", vid);
                                        if (dt.Rows.Count > 0)
                                        {
                                            ArrayList arrCol = new ArrayList();


                                            foreach (DataRow dr in dt.Rows)
                                            {
                                                Hashtable ht = new Hashtable();
                                                foreach (DataColumn dc in dt.Columns)
                                                {
                                                    ht.Add(dc.ColumnName, dr[dc.ColumnName]);
                                                }

                                                rtnlist.Add(ht);
                                            }
                                            htt.Add("data", rtnlist);
                                            htt.Add("Status", "200");
                                            htt.Add("message", "success");
                                        }
                                    }
                                }
                                #endregion

                                #region Forget Password
                                if (page == "forgetpassword")
                                {
                                    htt = new Hashtable();
                                    htt.Add("Status", "401");
                                    htt.Add("message", "fail");

                                    string pid = string.Empty;
                                    var pp = "Account";
                                    pid = dalp.SelectByName(pp).p_id;
                                    if (Request.Form["email"] != null || Request.QueryString["email"] != null)
                                    {
                                        string EmailAddress = string.Empty;
                                        if (Request.Form["email"] != null)
                                        {
                                            EmailAddress = Request.Form["email"].ToString();
                                        }

                                        if (Request.QueryString["email"] != null)
                                        {
                                            EmailAddress = Request.QueryString["email"].ToString();
                                        }

                                        if (!string.IsNullOrEmpty(EmailAddress))
                                        {
                                            List<BackEnd_Child> lstBackendChild = new List<BackEnd_Child>();
                                            lstBackendChild = dalc.SelectByPId(pid);

                                            string column_id = string.Empty;

                                            foreach (var l in lstBackendChild)
                                            {
                                                if (l.c_label == "Email")
                                                {
                                                    column_id = l.c_id;
                                                }
                                            }

                                            List<FrontEnd> lstFrontEnd = dalf.selectbyPIDCID(pid, column_id);

                                            FrontEnd fResult = new FrontEnd();
                                            foreach (var f in lstFrontEnd)
                                            {
                                                if (f.f_content.ToLower() == EmailAddress.ToLower())
                                                {
                                                    fResult = f;
                                                }
                                            }

                                            if (fResult != null)
                                            {
                                                var lstresult2 = dalf.SelectByRowidPiD(fResult.f_c_rowid, pid);
                                                if (lstresult2.Count > 0)
                                                {
                                                    string fullname = string.Empty;
                                                    string username = string.Empty;
                                                    string password = string.Empty;
                                                    foreach (var l in lstresult2)
                                                    {
                                                        if (l.f_label == "Full Name")
                                                        {
                                                            fullname = l.f_content;
                                                        }

                                                        if (l.f_label == "Login Name")
                                                        {
                                                            username = l.f_content;
                                                        }

                                                        if (l.f_label == "Password")
                                                        {
                                                            password = l.f_content;
                                                        }
                                                    }

                                                    string userID = string.Empty;
                                                    string status = string.Empty;
                                                    string parameters = string.Empty;
                                                    SendEmail se = new SendEmail();
                                                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                                                    {
                                                        parameters = "@Name|" + fullname + "^@loginName|" + username + "^@loginPassword|" + password;

                                                        status = se.SendEmailViaPortal(userID, EmailAddress, parameters, "ACC", "", "");
                                                        if (status == "OK")
                                                        {
                                                            htt = new Hashtable();
                                                            htt.Add("Status", "200");
                                                            htt.Add("message", "success");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            #endregion
                        }
            */
            #endregion
        }
    }

    public int DelegateQRLoginAuthenticate(string RegPortalID)
    {
        int result = 0;
        string sql = "Select * From tblAccount Where RegPortalID='" + RegPortalID + "' And deleteFlag=0 And a_type In ('D','V')";
        DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        result = dt.Rows.Count;
        return result;
    }
    public int DelegateNFCScanAuthenticate(string NFCID)
    {
        int result = 0;
        string sql = "Select * From tblAccount Where NFCID='" + NFCID + "' And deleteFlag=0 And a_type In ('D','V')";
        DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
        result = dt.Rows.Count;
        return result;
    }
    private int saveNFCInfo(string regno, string nfcid, string ndefid)
    {
        int isSuccess = 0;
        try
        {
            string sql = "Insert Into tblAccountNFCInfo (Regno,NFCID_AutoGeneratedNumber,NDEFNumber_RegNFCID) "
                + " Values ('" + regno + "','" + nfcid + "','" + ndefid + "')";
            fn.ExecuteSQL(sql);
            isSuccess = 1;
        }
        catch(Exception ex)
        { }
        return isSuccess;
    }
}

