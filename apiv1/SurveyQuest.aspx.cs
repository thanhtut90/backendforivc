﻿using System;
using System.Collections;
using System.Data;
using System.Web.Script.Serialization;
using App.DAL;
public partial class QuesList : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    Functionality fn = new Functionality();
    protected static string tablename = "Survey";
    protected void Page_Load(object sender, EventArgs e)
    {
        bind();
    }

    public void bind()
    {
        ArrayList eventList = new ArrayList();
        Hashtable htt = new Hashtable();

        var cate = dalf.selectSurveyCate();
        if (cate.Count > 0)
        {
            foreach (var c in cate)
            {
                Hashtable ht = new Hashtable();
                ht.Add("SurveyCateId", c.SurveyCateId);
                ht.Add("SurveyCateTitle", c.SurveyCateTitle);
                ht.Add("SurveyCateType", c.SurveyCateType);
                ht.Add("SurveyCateenum", c.SurveyCateenum);
                ht.Add("UserType", c.UserType);
             
                var surlst = dalf.selectSurveyListByCateID(c.SurveyCateId);
                string Options = "";
                string KeyName = "";
                string KeyLink = "";
                if (surlst.Count > 0)
                {
                    foreach (var s in surlst)
                    {
                        if (KeyName != "")
                        {
                            KeyName = KeyName + "@" + s.SurveyKeyNo.ToString();
                        }
                        else
                        {
                            KeyName = s.SurveyKeyNo.ToString();
                        }
                        if (Options != "")
                        {
                            Options = Options + "@" + s.SurveyTitle.ToString();
                        }
                        else
                        {
                            Options = s.SurveyTitle.ToString();
                        }


                        if (KeyLink != "")
                        {
                            KeyLink = KeyLink + "@" + s.SurveyLink.ToString();
                        }
                        else
                        {
                            KeyLink = s.SurveyLink.ToString();
                        }
                            
                    }
                }
                ht.Add("Options", Options);
                ht.Add("KeyName", KeyName);
                ht.Add("KeyLink", KeyLink);
                eventList.Add(ht);
            }

            string dataversion = fn.GetDataByCommand("select a_dataVersion as dataversion from tblActivatedForms where page='" + tablename + "'", "dataversion");
            htt.Add("dataversion", dataversion);
            htt.Add("data", eventList);
            htt.Add("Status", "200");
            htt.Add("message", "success");
        }
        JavaScriptSerializer ser = new JavaScriptSerializer();
        String jsonStr = ser.Serialize(htt);//eventList
        Response.Write(jsonStr);
    }

}