﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Polling.aspx.cs" Inherits="apiv1_Polling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=0.5, maximum-scale=2.0, user-scalable=yes" />
    <meta http-equiv="refresh" content="15"/>
  <link rel="stylesheet" href="../Content/bootstrap/css/bootstrap.min.css"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/>
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css"/>
  <!-- daterange picker -->
  <link rel="stylesheet" href="../Content/plugins/daterangepicker/daterangepicker-bs3.css"/>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../Content/plugins/datepicker/datepicker3.css"/>
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../Content/plugins/iCheck/all.css"/>
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../Content/plugins/colorpicker/bootstrap-colorpicker.min.css"/>
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../Content/plugins/timepicker/bootstrap-timepicker.min.css"/>
  <!-- Select2 -->
  <link rel="stylesheet" href="../Content/plugins/select2/select2.min.css"/>
  <!-- Theme style -->
  <link rel="stylesheet" href="../Content/dist/css/AdminLTE.min.css"/>
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../Content/dist/css/skins/_all-skins.min.css"/>
    <script src="../Content/plugins/jQuery/jQuery-2.2.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function generateUUID() {
            var nav = window.navigator;
            var screen = window.screen;
            var guid = nav.mimeTypes.length;
            guid += nav.userAgent.replace(/\D+/g, '');
            guid += nav.plugins.length;
            guid += screen.height || '';
            guid += screen.width || '';
            guid += screen.pixelDepth || '';

            return guid;
        };

        $(function () {
            timestamp = 0;
            var userid = $("#<%=Uid.ClientID%>").val();
            if (!userid && userid == '' && userid.length <= 0) {
                var ip = generateUUID();
                $("#<%=Uid.ClientID%>").val(ip);
            }
        })
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <div class="container">
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4"> 
                  <h4 style="text-align:justify;color:#1C3F95;font-weight:bold"><asp:Label ID="lblSession" runat="server" Width="100%"></asp:Label></h4>             
                  
              </div>
              <div class="col-sm-4"></div>
            </div>
            <div class="row">
              <div class="col-sm-4"></div>
              <div class="col-sm-4"> 
                  <label style="color:#1C3F95"><%=PQTitle %></label>             
                  <br /><br />
                  <%=queslist %>
                  <fieldset id="div1" runat="server"></fieldset>
              </div>
              <div class="col-sm-4">
                    <asp:TextBox ID="Uid" runat="server" style="display:none;"></asp:TextBox></div>
            </div>
        </div>
 
    </form>
    <script src="../Content/plugins/jQuery/jQuery-2.2.0.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../Content/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../Content/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../Content/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../Content/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../Content/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="../Content/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../Content/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="../Content/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../Content/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="../Content/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../Content/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../Content/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../Content/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../Content/dist/js/demo.js"></script>
<!-- Page script -->
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
        function (start, end) {
          $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
    );

    //Date picker
    $('#sdatepicker').datepicker({
      autoclose: true
    });

    $('#edatepicker').datepicker({
        autoclose: true
    });

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
</body>
</html>
