﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class commentrating : System.Web.UI.Page
{
    string comment = "";
    string userID = "";
    float rating = 0;
    string posterID = "";
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ArrayList eventList = new ArrayList();
            Hashtable ht = new Hashtable();

            if (Request.Form["PosterID"] != null)
            {
                posterID = Request.Form["PosterID"].ToString();// int.Parse(Request.Form["PosterID"].ToString());
                userID = Request.Form["UserID"].ToString() != "" ? Request.Form["UserID"].ToString() : "";
                comment = Request.Form["Comment"].ToString() != "" ? Request.Form["Comment"].ToString() : "";
                rating = float.Parse(Request.Form["Rating"].ToString() != "" ? Request.Form["Rating"].ToString() : "0");

                string sql = "insert into CommentRating (cr_pID,cr_comment,cr_rating,cr_Uid,cr_datetime) values (@cr_pID,@cr_comment,@cr_rating,@cr_Uid, @cr_datetime)";
                SqlParameter[] parms ={

                                        new SqlParameter("@cr_pID",SqlDbType.NVarChar),
                                        new SqlParameter("@cr_comment",SqlDbType.NVarChar),
                                        new SqlParameter("@cr_rating",SqlDbType.Float),
                                        new SqlParameter("@cr_Uid",SqlDbType.NVarChar),
                                        new SqlParameter("@cr_datetime",SqlDbType.DateTime)
                                      };

                parms[0].Value = posterID;
                parms[1].Value = comment.ToString().Trim();
                parms[2].Value = rating;
                parms[3].Value = userID.ToString().Trim();
                parms[4].Value = DateTime.Now;
                int status = fn.ExecuteNonQuery(sql, parms);
                if (status == 1)
                {
                    ht.Add("Status", "200");
                }
                else
                {
                    ht.Add("Status", "401");
                }
                eventList.Add(ht);
            }
            else
            {
                ht.Add("Status", "100");
            }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(eventList);
            Response.Write(jsonStr);
        }
    }
}