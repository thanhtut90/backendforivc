﻿using System;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;
using System.Data;
using System.Text;
using System.Collections.Generic;
using System.Web.UI;

public partial class apiv1_Polling : System.Web.UI.Page
{
    DAL_Data daldata = new DAL_Data();
    protected string PQTitle, queslist;
    protected void Page_init(object sender, EventArgs e)
    {
        string sessionid = string.Empty;
        if (Request.QueryString["SessionID"] != null)
        {
            sessionid = Request.QueryString["SessionID"].ToString();
        }

        if (Request.Form["SessionID"] != null)
        {
            sessionid = Request.Form["SessionID"].ToString();
        }

        if (!string.IsNullOrEmpty(sessionid))
        {
            string sessionname = daldata.getConferenceTitleByID(sessionid);
            if (!string.IsNullOrEmpty(sessionname))
            {
                lblSession.Text = sessionname;
            }
        }
        BindQuestion(sessionid);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string userrreference = string.Empty, sessionid = string.Empty;
            if (Request.QueryString["SessionID"] != null)
            {
                sessionid = Request.QueryString["SessionID"].ToString();
            }
            if (Request.QueryString["UserID"] != null)
            {
                userrreference = Request.QueryString["UserID"].ToString();
                Uid.Text = userrreference.Trim();
            }

            if (Request.Form["SessionID"] != null)
            {
                sessionid = Request.Form["SessionID"].ToString();
            }
            if (Request.Form["UserID"] != null)
            {
                userrreference = Request.Form["UserID"].ToString();
                Uid.Text = userrreference.Trim();
            }

            if (!string.IsNullOrEmpty(sessionid))
            {
                string sessionname = daldata.getConferenceTitleByID(sessionid);
                if (!string.IsNullOrEmpty(sessionname))
                {
                    lblSession.Text = sessionname;
                }
            }
        }
    }

    private void BindQuestion(string Sid)
    {
        Functionality fn = new Functionality();
        DataTable dtv2 = fn.GetDatasetByCommand("select * from PollingQandA where sessionID='" + Sid + "' and pollingStatus=1;", "sdt").Tables[0];
        if (dtv2.Rows.Count > 0)
        {
            string questionID = dtv2.Rows[0]["questionID"].ToString();
            int acount = int.Parse(dtv2.Rows[0]["noOfAns"].ToString());
            PQTitle = dtv2.Rows[0]["question"].ToString();
            
            
            StringBuilder tb = new StringBuilder();
            if (dtv2.Rows[0]["answer1"].ToString() != "")
            {
                LinkButton lnk1 = new LinkButton();
                lnk1.ID = "LinkButton1";
                lnk1.CommandArgument = "1";
                lnk1.CommandName = "Vote";                
                lnk1.Text = dtv2.Rows[0]["answer1"].ToString();
                lnk1.Style.Add("width", "100%");
                lnk1.Style.Add("color", "#1C3F95");
                lnk1.Attributes.Add("class", "btn btn-default");
                lnk1.Click += new EventHandler(this.Vote_Onclick);
                this.div1.Controls.Add(lnk1);
                this.div1.Controls.Add(new LiteralControl("<br/><br/>"));

            }
            if (dtv2.Rows[0]["answer2"].ToString() != "")
            {
                LinkButton lnk2 = new LinkButton();
                lnk2.ID = "LinkButton2";
                lnk2.CommandArgument = "2";
                lnk2.CommandName = "Vote";
                lnk2.Text = dtv2.Rows[0]["answer2"].ToString();
                lnk2.Style.Add("width", "100%");
                lnk2.Style.Add("color", "#1C3F95");
                lnk2.Attributes.Add("class", "btn btn-default");
                lnk2.Click += new EventHandler(this.Vote_Onclick);
                this.div1.Controls.Add(lnk2);
                this.div1.Controls.Add(new LiteralControl("<br/><br/>"));
            }
            if (dtv2.Rows[0]["answer3"].ToString() != "")
            {
                LinkButton lnk3 = new LinkButton();
                lnk3.ID = "LinkButton3";
                lnk3.CommandArgument = "3";
                lnk3.CommandName = "Vote";
                lnk3.Text = dtv2.Rows[0]["answer3"].ToString();
                lnk3.Style.Add("width", "100%");
                lnk3.Style.Add("color", "#1C3F95");
                lnk3.Attributes.Add("class", "btn btn-default");
                lnk3.Click += new EventHandler(this.Vote_Onclick);
                this.div1.Controls.Add(lnk3);
                this.div1.Controls.Add(new LiteralControl("<br/><br/>"));
            }

            if (dtv2.Rows[0]["answer4"].ToString() != "")
            {
                LinkButton lnk4 = new LinkButton();
                lnk4.ID = "LinkButton4";
                lnk4.CommandArgument = "4";
                lnk4.CommandName = "Vote";
                lnk4.Text = dtv2.Rows[0]["answer4"].ToString();
                lnk4.Style.Add("width", "100%");
                lnk4.Style.Add("color", "#1C3F95");
                lnk4.Attributes.Add("class", "btn btn-default");
                lnk4.Click += new EventHandler(this.Vote_Onclick);
                this.div1.Controls.Add(lnk4);
                this.div1.Controls.Add(new LiteralControl("<br/><br/>"));
            }

            if (dtv2.Rows[0]["answer5"].ToString() != "")
            {
                LinkButton lnk5 = new LinkButton();
                lnk5.ID = "LinkButton5";
                lnk5.CommandArgument = "5";
                lnk5.CommandName = "Vote";
                lnk5.Text = dtv2.Rows[0]["answer5"].ToString();
                lnk5.Style.Add("width", "100%");
                lnk5.Style.Add("color", "#1C3F95");
                lnk5.Attributes.Add("class", "btn btn-default");
                lnk5.Click += new EventHandler(this.Vote_Onclick);
                this.div1.Controls.Add(lnk5);
                this.div1.Controls.Add(new LiteralControl("<br/><br/>"));
            }

         
            queslist = tb.ToString();
        }
    }

    protected void Vote_Onclick(object sender, EventArgs e)
    {
        string userrreference = string.Empty, sessionid = string.Empty;
        if (Request.QueryString["SessionID"] != null)
        {
            sessionid = Request.QueryString["SessionID"].ToString();
        }
        if (Request.QueryString["UserID"] != null)
        {
            userrreference = Request.QueryString["UserID"].ToString();
        }

        if (Request.Form["SessionID"] != null)
        {
            sessionid = Request.Form["SessionID"].ToString();
        }
        if (Request.Form["UserID"] != null)
        {
            userrreference = Request.Form["UserID"].ToString();
        }

        if(string.IsNullOrEmpty(userrreference) || string.IsNullOrWhiteSpace(userrreference))
        {
            userrreference = Uid.Text.Trim();
        }

        if (!string.IsNullOrEmpty(userrreference) && !string.IsNullOrEmpty(sessionid))
        {
            LinkButton lnk = sender as LinkButton;
            if (lnk.CommandName == "Vote")
            {
                string NO = lnk.CommandArgument.ToString();
                vote(userrreference, int.Parse(NO), sessionid);
            }
        }
    }   
    protected void vote(string userreference,int voteno,string sessionID)
    {
        try
        {
            int answer = 0;         
            answer = voteno;
            string question = daldata.SelectQuestionBySession(sessionID);
            string CheckExist = daldata.CheckPollingAnswer(userreference, question);

            DataTable dt = new DataTable();
            dt.Columns.Add("PR_ID");
            dt.Columns.Add("PR_UserRef");
            dt.Columns.Add("PR_Session");
            dt.Columns.Add("PR_Question");
            dt.Columns.Add("PR_Answer");
            dt.Columns.Add("PR_Time");

            if (!string.IsNullOrEmpty(CheckExist) && CheckExist != "0")
            {
                #region Update
                DataRow dr = dt.NewRow();
                dr["PR_ID"] = CheckExist;
                dr["PR_UserRef"] = userreference;
                dr["PR_Session"] = sessionID;
                dr["PR_Question"] = question;
                dr["PR_Answer"] = answer;
                dr["PR_Time"] = DateTime.Now;

                daldata.UpdatePollingResult(dr);

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Submitted!')", true);
                #endregion
            }
            else
            {
                #region Insert
                GenKey gk = new GenKey();
                string fkey = string.Empty; string fid = string.Empty;
                fkey = gk.getKey("PR");
                fid = "PR" + fkey;
      
                DataRow dr = dt.NewRow();
                dr["PR_ID"] = fid;
                dr["PR_UserRef"] = userreference;
                dr["PR_Session"] = sessionID;
                dr["PR_Question"] = question;
                dr["PR_Answer"] = answer;
                dr["PR_Time"] = DateTime.Now;

                daldata.SavePollingResult(dr);

                gk.SaveKey("PR", int.Parse(fkey));

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Submitted!')", true);
                #endregion
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }       
    }

}
