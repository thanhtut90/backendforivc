﻿using System;
using System.Drawing;
using App.Common;
using App.DAL;
using System.Web.Script.Serialization;
using System.Collections;
using System.IO;
using System.Drawing.Imaging;
public partial class apiv1_momentPostRequest : System.Web.UI.Page
{
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    GenKey gk = new GenKey();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ArrayList eventList = new ArrayList();
            Hashtable ht = new Hashtable();
            string comment = "";
            string img = "";
            string imgpath = string.Empty;
            int res = 0;
            string filename = "";

            if (Request.QueryString["comment"] != null)
            {
                comment = Request.QueryString["comment"].ToString();
            }
            if (Request.QueryString["image"] != null)
            {
                img = Request.QueryString["image"].ToString();
            }
            if (Request.Form["comment"] != null)
            {
                comment = Request.Form["comment"].ToString();
            }
            if (Request.Form["image"] != null)
            {
                img = Request.Form["image"].ToString();
            }
            if (!string.IsNullOrEmpty(img))
            {
                filename = System.DateTime.Now.ToString("yyyMMddhhmmss") + ".jpg";
                imgpath = Server.MapPath("~/Images/FrontEnd_Photos/") + filename;
                SaveByteArrayAsImage(imgpath, img);
            }
            #region Save
            if (!string.IsNullOrEmpty(comment) || !string.IsNullOrEmpty(imgpath))
            {
                var path = filename;
                res = SaveMoment(comment, path);
            }

            #endregion

            #region Return
            if (res > 0)
            {
                ht.Add("Status", "200");
                ht.Add("Message", "Success");
                eventList.Add(ht);
            }
            else
            {
                ht.Add("Status", "400");
                ht.Add("Message", "Fail");
                eventList.Add(ht);
            }
            #endregion

            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(ht);
            Response.Write(jsonStr);
        }
    }

    private int SaveMoment(string comment, string imgpath)
    {
        string rkey = string.Empty; string rid = string.Empty;
        rkey = gk.getKey("RID");
        rid = "RID" + rkey;
        string fcrowid = rid;
        string pid = "BP10042";
        int res = 0;
        for (int i=0;i<4;i++)
        {
            string addedtime = dalf.ConvertToTimestamp(DateTime.Now);
            string status = "Approved";
            FrontEnd obj = new FrontEnd();
            obj.f_pid = pid;
            obj.f_c_id = (i == 0 ? "BC10203" : i == 1 ? "BC10204" : i == 2 ? "BC10205" : "BC10206");
            obj.f_inputtype = (i == 0 ? 1 : i == 1 ? 13 : i == 2 ? 6 : 1);
            obj.f_label = (i == 0 ? "Text Comment" : i == 1 ? "Photo Comment" : i == 2 ? "Status" : "Added Time");
            obj.f_sortorder = i+1;
            obj.f_content = (i == 0 ? comment : i == 1 ? imgpath : i == 2 ? status : addedtime);
            obj.f_entrycount = 1;
            obj.f_c_rowid = fcrowid;
            obj.f_c_isFinish = true;
            obj.f_createddate = addedtime;
            res = dalf.Save(obj);
        }
        #region timestamp  
        DateTime updtime = DateTime.Now;
        string updatetimestamp = dalf.ConvertToTimestamp(updtime);
        int isSuc = dalf.updatetimestamp(pid, fcrowid, updatetimestamp);
        #endregion
        return res;
       
    }

    private void SaveByteArrayAsImage(string fullOutputPath, string base64String)
    {
        byte[] bytes = Convert.FromBase64String(base64String);
        Image image;
        using (MemoryStream ms = new MemoryStream(bytes))
        {
            image = Image.FromStream(ms);
            SaveJpeg(fullOutputPath, image, 100);
        }
    }
    public static void SaveJpeg(string path, Image img, int quality)
    {
        EncoderParameter qualityParam
        = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);

        ImageCodecInfo jpegCodec = GetEncoderInfo(@"image/jpeg");

        EncoderParameters encoderParams = new EncoderParameters(1);

        encoderParams.Param[0] = qualityParam;

        System.IO.MemoryStream mss = new System.IO.MemoryStream();

        System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Create
        , System.IO.FileAccess.ReadWrite);

        img.Save(mss, jpegCodec, encoderParams);
        byte[] matriz = mss.ToArray();
        fs.Write(matriz, 0, matriz.Length);

        mss.Close();
        fs.Close();
    }
    private static ImageCodecInfo GetEncoderInfo(String mimeType)
    {
        int j;
        ImageCodecInfo[] encoders;
        encoders = ImageCodecInfo.GetImageEncoders();
        for (j = 0; j < encoders.Length; ++j)
        {
            if (encoders[j].MimeType == mimeType)
                return encoders[j];
        }
        return null;
    }
}