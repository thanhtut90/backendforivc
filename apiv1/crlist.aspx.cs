﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Web.Script.Serialization;

public partial class crlist : System.Web.UI.Page
{
    Functionality fn = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ArrayList crList = new ArrayList();

            if (Request.QueryString["PosterID"] != null)
            {
                string sql = "SELECT * from CommentRating where cr_pID='" + Request.QueryString["PosterID"] + "' order by cr_ID asc";

                DataTable dtv = fn.GetDatasetByCommand(sql, "ds").Tables[0];
                if (dtv.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtv.Rows)
                    {
                        Hashtable ht = new Hashtable();

                        ht.Add("PosterID", dr["cr_pID"]);
                        ht.Add("Comment", dr["cr_comment"]);
                        ht.Add("Rating", dr["cr_rating"]);
                        ht.Add("UserID", dr["cr_UiD"]);
                        ht.Add("DateTime", String.Format("{0:MM/dd/yyyy}", dr["cr_dateTime"]));

                        crList.Add(ht);
                    }
                }
            }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(crList);
            Response.Write(jsonStr);
        }
    }
}