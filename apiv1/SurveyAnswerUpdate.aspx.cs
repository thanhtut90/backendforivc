﻿using System;
using System.Linq;
using App.DAL;
using System.Collections;
using System.Data;
using System.Web.Script.Serialization;

public partial class apiv1_SurveyAnswerUpdate : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    Functionality dl = new Functionality();
    protected void Page_Load(object sender, EventArgs e)
    {
        ArrayList eventList = new ArrayList();
        Hashtable ht = new Hashtable();
                
        if (Request.Form["Uid"] != null && Request.Form["QuesValue"] != null && Request.Form["QuesKey"] != null)
        {
            if (!IsPostBack)
            {
                string Uid = Request.Form["Uid"].ToString();
                string QuesValue = Request.Form["QuesValue"].ToString();
                string QuesKey = Request.Form["QuesKey"].ToString();
                string sql = "SELECT  * from SurveyQA where UserReference=" + Uid + "";
                DataTable dt = dl.GetDatasetByCommand(sql,"sdt").Tables[0];

                if (dt.Rows.Count > 0)
                {
                    string[] arrStr1 = QuesKey.Split('@');
                    string[] arrStr2 = QuesValue.Split('@');
                    for (int i = 0; i < arrStr1.Length; i++)
                    {
                        string sql4 = "update SurveyQA set " + arrStr1[i].ToString() + "='" + arrStr2[i].ToString().Replace("!#", "@") + "' where UserReference=" + Uid;

                        dl.ExecuteSQL(sql4);

                    }
                    ht.Add("result", "1");
                    eventList.Add(ht);
                }
                else
                {
                    string sql1 = "insert into SurveyQA (UserReference) values (" + Uid + ")";
                    dl.ExecuteSQL(sql1);
                    string sql2 = "SELECT  * from SurveyQA where UserReference=" + Uid + "";
                    DataTable dt2 = dl.GetDatasetByCommand(sql2,"sdt").Tables[0];

                    if (dt2.Rows.Count > 0)
                    {
                        string[] arrStr1 = QuesKey.Split('@');
                        string[] arrStr2 = QuesValue.Split('@');
                        for (int i = 0; i < arrStr1.Length; i++)
                        {
                            string sql4 = "update SurveyQA set " + arrStr1[i] + "='" + arrStr2[i].ToString().Replace("!#", "@") + "' where UserReference=" + Uid;
                            dl.ExecuteSQL(sql4);

                        }
                        ht.Add("result", "1");
                        eventList.Add(ht);
                    }


                }
            }

        }
        else
        {
            ht.Add("result", "2");
            eventList.Add(ht);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        String jsonStr = ser.Serialize(eventList);
        Response.Write(jsonStr);
    }
}