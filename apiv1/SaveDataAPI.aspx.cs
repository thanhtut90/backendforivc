﻿using App.Common;
using App.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class apiv1_SaveDataAPI : System.Web.UI.Page
{
    #region Declaration
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Child dalc = new DAL_Child();
    DAL_Parent dalp = new DAL_Parent();
    DAL dal = new DAL();
    GenKey gk = new GenKey();
    int lang = 0;
    string serverPath = HttpContext.Current.Server.MapPath("~/");

    static string page_account = "Account";
    static string colname_fullname = "contactpersonID";
    static string colname_emailaddress = "a_email";
    static string colname_loginname = "a_loginName";
    static string colname_loginpassword = "a_Password";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ArrayList rtnlist = new ArrayList();
            Hashtable htt = new Hashtable();
            string page = string.Empty;
            string langcode = string.Empty;
            int lang = 0;

            if (this.Request.QueryString["Page"] != null && this.Request.QueryString["Lang"] != null)
            {
                page = this.Request.QueryString["Page"].ToString();
                langcode = this.Request.QueryString["Lang"].ToString();
            }
            if (this.Request.Form["Page"] != null && this.Request.Form["Lang"] != null)
            {
                page = this.Request.Form["Page"].ToString();
                langcode = this.Request.Form["Lang"].ToString();
            }
            lang = dal.getLangIDBycode(langcode);

            htt = new Hashtable();
            htt.Add("Status", "401");
            htt.Add("message", "fail");

            if (!string.IsNullOrEmpty(page) && lang > 0)
            {
                string pid = dalp.SelectByName(page).p_id;
                if(!string.IsNullOrEmpty(pid))
                {
                    var lstchild = dalc.SelectByPId(pid);

                    if (lstchild.Count > 0)
                    {
                        bool isMatch = compareParamChild(lstchild);

                        if (isMatch)
                        {
                            //Response.End();

                            string rkey = string.Empty; string rowid = string.Empty;
                            rkey = gk.getKey("RID");
                            rowid = "RID" + rkey;

                            int entry_count = 1;
                            int SaveNoSuccess = 0;

                            string accName = string.Empty;
                            string emailAddress = string.Empty;
                            string loginName = string.Empty;
                            string loginPassword = string.Empty;
                            SaveNoSuccess = insertData(lstchild, entry_count, pid, rowid, "", lang, ref accName, ref emailAddress, ref loginName, ref loginPassword);
                            if (SaveNoSuccess == 1)
                            {
                                SaveControlsNoSuccess();
                            }
                            else
                            {
                                gk.SaveKey("RID", int.Parse(rkey));

                                if(page == page_account)
                                {
                                    string userID = string.Empty;
                                    string status = string.Empty;
                                    string parameters = string.Empty;
                                    SendEmail se = new SendEmail();
                                    if (!string.IsNullOrEmpty(loginName) && !string.IsNullOrEmpty(loginPassword) && !string.IsNullOrEmpty(emailAddress))
                                    {
                                        parameters = "@Name|" + accName + "^@loginName|" + loginName + "^@loginPassword|" + loginPassword;

                                        status = se.SendEmailViaPortal(userID, emailAddress, parameters, "CREATEACC", "", "");
                                        if (status == "OK")
                                        {
                                            htt = new Hashtable();
                                            htt.Add("Status", "200");
                                            htt.Add("message", "success");
                                        }
                                    }
                                }

                                htt = new Hashtable();
                                htt.Add("Status", "200");
                                htt.Add("message", "success");
                            }
                        }
                    }
                }
            }

            JavaScriptSerializer ser = new JavaScriptSerializer();
            String jsonStr = ser.Serialize(htt);
            Response.Write(jsonStr);
        }
    }

    #region compare param count and child count
    private bool compareParamChild(List<BackEnd_Child> lstChild)
    {
        bool isMatch = true;
        try
        {
            List<string> lstLabels = new List<string>();
            foreach (var chitem in lstChild)
            {
                lstLabels.Add(chitem.c_colname);
            }

            #region Request.Form
            if (this.Request.Form.Count > 0)
            {
                List<string> lstParams = new List<string>();

                //*check against Backend_Child
                for (int i=0; i< this.Request.Form.Count; i++)
                {
                    if (!string.IsNullOrEmpty(this.Request.Form.GetKey(i)))
                    {
                        if (this.Request.Form.GetKey(i).ToLower() != "page" && this.Request.Form.GetKey(i).ToLower() != "lang")
                        {
                            lstParams.Add(this.Request.Form.GetKey(i));

                            if (!lstLabels.Contains(this.Request.Form.GetKey(i)))
                            {
                                isMatch = false;
                            }
                        }
                    }
                }

                //*check against Request.Form
                foreach (var chitem in lstChild)
                {
                    if (!lstParams.Contains(chitem.c_colname))
                    {
                        isMatch = false;
                    }
                }
            }
            #endregion

            #region Request.QueryString
            if (this.Request.QueryString.Count > 0)
            {
                List<string> lstParams = new List<string>();

                //*check against Backend_Child
                for (int i = 0; i < this.Request.QueryString.Count; i++)
                {
                    if (!string.IsNullOrEmpty(this.Request.QueryString.GetKey(i)))
                    {
                        if (this.Request.QueryString.GetKey(i).ToLower() != "page" && this.Request.QueryString.GetKey(i).ToLower() != "lang")
                        {
                            lstParams.Add(this.Request.QueryString.GetKey(i));

                            if (!lstLabels.Contains(this.Request.QueryString.GetKey(i)))
                            {
                                isMatch = false;
                            }
                        }
                    }
                }

                //*check against Request.QueryString
                foreach (var chitem in lstChild)
                {
                    if (!lstParams.Contains(chitem.c_colname))
                    {
                        isMatch = false;
                    }
                }
            }
            #endregion
        }
        catch (Exception ex)
        {
            isMatch = false;
        }

        return isMatch;
    }
    #endregion

    #region insert data
    private int insertData(List<BackEnd_Child> lstchild, int entry_count, string pid, string rowid, string img_str, int lang, ref string accName, ref string emailAddress, ref string loginName, ref string loginPassword)
    {
        int NoSuccess = 0;
        try
        {
            #region Request.Form
            if (this.Request.Form.Count > 0)
            {
                int Int32_KeyCount = 0;
                foreach (string String_Key in this.Request.Form.Keys)
                {
                    string paramKey = String_Key;
                    string paramValue = this.Request.Form.Get(Int32_KeyCount).ToString();//this.Request.Form[Int32_KeyCount].ToString();

                    Int32_KeyCount += 1;
                    if (!string.IsNullOrEmpty(paramKey))
                    {
                        if (paramKey.ToLower() != "page" && paramKey.ToLower() != "lang")
                        {
                            foreach (var chitem in lstchild)
                            {
                                if (paramKey == chitem.c_colname)
                                {
                                    int isSuccess = 0;
                                    string fkey = string.Empty; string fid = string.Empty;
                                    fkey = gk.getKey("FE");
                                    fid = "FE" + fkey;

                                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = chitem.c_label, f_lang = lang, f_entrycount = entry_count, f_inputtype = chitem.c_inputtype, f_content = paramValue, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = chitem.c_sortorder, f_c_id = chitem.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                                    isSuccess = dalf.Save(fe);
                                    if (isSuccess == 0)
                                    {
                                        NoSuccess = 1;
                                    }
                                    else
                                    {
                                        gk.SaveKey("FE", int.Parse(fkey));
                                        ViewState["front_childID"] += fid + ",";
                                    }

                                    #region get Email Params
                                    if (chitem.c_colname == colname_fullname)
                                    {
                                        accName = paramValue;
                                    }

                                    if (chitem.c_colname == colname_emailaddress)
                                    {
                                        emailAddress = paramValue;
                                    }

                                    if (chitem.c_colname == colname_loginname)
                                    {
                                        loginName = paramValue;
                                    }

                                    if (chitem.c_colname == colname_loginpassword)
                                    {
                                        loginPassword = paramValue;
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            #region Request.QueryString
            if (this.Request.QueryString.Count > 0)
            {
                int Int32_KeyCount = 0;
                foreach (string String_Key in this.Request.QueryString.Keys)
                {
                    string paramKey = String_Key;
                    string paramValue = this.Request.QueryString.Get(Int32_KeyCount).ToString();//this.Request.QueryString[Int32_KeyCount].ToString();

                    Int32_KeyCount += 1;
                    if (!string.IsNullOrEmpty(paramKey))
                    {
                        if (paramKey.ToLower() != "page" && paramKey.ToLower() != "lang")
                        {
                            foreach (var chitem in lstchild)
                            {
                                if (paramKey == chitem.c_colname)
                                {
                                    int isSuccess = 0;
                                    string fkey = string.Empty; string fid = string.Empty;
                                    fkey = gk.getKey("FE");
                                    fid = "FE" + fkey;

                                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = chitem.c_label, f_lang = lang, f_entrycount = entry_count, f_inputtype = chitem.c_inputtype, f_content = paramValue, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = chitem.c_sortorder, f_c_id = chitem.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                                    isSuccess = dalf.Save(fe);
                                    if (isSuccess == 0)
                                    {
                                        NoSuccess = 1;
                                    }
                                    else
                                    {
                                        gk.SaveKey("FE", int.Parse(fkey));
                                        ViewState["front_childID"] += fid + ",";
                                    }

                                    #region get Email Params
                                    if (chitem.c_colname == colname_fullname)
                                    {
                                        accName = paramValue;
                                    }

                                    if (chitem.c_colname == colname_emailaddress)
                                    {
                                        emailAddress = paramValue;
                                    }

                                    if (chitem.c_colname == colname_loginname)
                                    {
                                        loginName = paramValue;
                                    }

                                    if (chitem.c_colname == colname_loginpassword)
                                    {
                                        loginPassword = paramValue;
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                }
            }
            #endregion
        }
        catch (Exception ex)
        {
            NoSuccess = 1;
        }

        return NoSuccess;
    }
    #endregion

    #region SaveControlsNoSuccess
    private void SaveControlsNoSuccess()
    {
        if (ViewState["front_childID"] != null)
        {
            var lastcoma = (ViewState["front_childID"].ToString()).Length - 1;
            string selecteditems = (ViewState["front_childID"].ToString()).Remove(lastcoma);
            string[] strchlIDs = (selecteditems).Split(',');
            if (strchlIDs.Length > 0 && strchlIDs != null)
            {
                foreach (var IDitem in strchlIDs)
                {
                    int success = 0;
                    string itemid = string.Empty;
                    itemid = IDitem.ToString();
                    success = dalf.Delete(itemid);
                }
            }
        }
    }
    #endregion

}