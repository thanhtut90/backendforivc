﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;


public partial class msgInsert : System.Web.UI.Page
{
    Functionality dl = new Functionality();
    GenKey gk = new GenKey();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["msg"] != null && Request.QueryString["meetid"] != null && Request.QueryString["Uid"] != null)
            {
                string UserReference = Request.QueryString["Uid"].ToString();
                DateTime dtime = DateTime.Now;
                string msg = Request.QueryString["msg"].ToString();
                string meetid = Request.QueryString["meetid"].ToString();
                string fkey = string.Empty; string fid = string.Empty;
                fkey = gk.getKey("LQA");
                fid = "LQA" + fkey;
                StringBuilder str = new StringBuilder();
                str.Append("insert into LQA (SessionID,LQATitle,dtime,UserReference,LQAID)");
                    str.Append(" values ");
                str.Append("(@SessionID,@LQATitle,@dtime,@UserReference,@LQAID)");
                    SqlParameter[] parms1 ={ 
                                       new SqlParameter("@SessionID",SqlDbType.VarChar),
                                       new SqlParameter("@LQATitle",SqlDbType.VarChar),
                                       new SqlParameter("@dtime",SqlDbType.DateTime),
                                       new SqlParameter("@UserReference",SqlDbType.VarChar),
                                       new SqlParameter("@LQAID",SqlDbType.VarChar)
                                  };

                    parms1[0].Value = meetid;
                    parms1[1].Value = msg;
                    parms1[2].Value = dtime;
                    parms1[3].Value = UserReference;
                    parms1[4].Value = fid;

                int addcount = dl.ExecuteNonQuery(str.ToString(), parms1);
                gk.SaveKey("LQA", int.Parse(fkey));
            }
        }
    }
}