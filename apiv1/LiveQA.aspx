﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LiveQA.aspx.cs" Inherits="SQA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
<head runat="server">
    <title></title>
    <%--<link href="Content/css/mobilelayout.css" rel="stylesheet" type="text/css" />--%>
         <link rel="stylesheet" href="../Content/bootstrap/css/bootstrap.min.css" />
        <style type="text/css">
                .footer {
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                }
                body, html {
                    height: 100% !important;
                    overflow: auto;
                }
                .container {
                    overflow-x:auto;
                    overflow-y: auto;
                    height: 100%;
                    margin-bottom: 161px; /*add the footer height*/
                }
        </style>
        <script src="../Content/plugins/jQuery/jQuery-2.2.0.min.js" type="text/javascript"></script>
    
        <script type="text/javascript">
            function bin2hex(s) {
                var i, l, o = '',
                    n;
                s += '';
                for (i = 0, l = s.length; i < l; i++) {
                    n = s.charCodeAt(i)
                        .toString(16);
                    o += n.length < 2 ? '0' + n : n;
                }
                return o;
            }

            function generateUUID() {
                var nav = window.navigator;
				var screen = window.screen;
				var guid = nav.mimeTypes.length;
				guid += nav.userAgent.replace(/\D+/g, '');
				guid += nav.plugins.length;
				guid += screen.height || '';
				guid += screen.width || '';
				guid += screen.pixelDepth || '';

				return guid;
            };

            $(function () {
                timestamp = 0;
                var userid = $("#Uid").val();
                if (!userid && userid == '' && userid.length <= 0)
                {
                    var ip = generateUUID();
                    $("#Uid").val(ip);
                }
                //alert($("#Uid").val());

                updateMsg();
            })
function sendMsg() {  
    var Msgmess = $("#msg").val();
    Msgmess = Msgmess.replace(/\+/g, "%2B");
    Msgmess = Msgmess.replace(/\&/g, "%26");
    if (Msgmess == "") {       
        alert("Please Enter Message");

        return false;
    }
    else {       
        $.ajax({
            type: "POST",
            url: "msgInsert.aspx?msg=" + Msgmess + "&meetid=" + $("#meetid").val() + "&Uid=" + $("#Uid").val(),
            data: "msg=" + Msgmess + "&meetid=" + $("#meetid").val() + "&Uid=" + $("#Uid").val(),
            success: function (v) {
                $("#msg").val("");

            }

        });
    }
            }
function updateMsg() {   
                $.post("../Handle/msg.ashx?meetid=" + $("#meetid").val() + "&Uid=" + $("#Uid").val(), {
                    time: timestamp
                }, function (xml) {
                    $("#loading").remove();
                    addMessages(xml);
                });
                setTimeout('updateMsg()', 15000);  
            }
          

            function addMessages(xml) {
                if ($("status", xml).text() == "2") return;
                timestamp = $("time", xml).text(); 
                var htmlcode = "";
                $("message", xml).each(function () {

                    var QAID = $("QAID", this).text();
                    var SessionID = $("SessionID", this).text();
                    var QATitle = $("QATitle", this).text();
                    var QANum = $("QANum", this).text();
                    if (QANum > 1) {
                        QANum += " Votes";
                    }
                    else {
                        QANum += " Vote";
                    }
                   
                    htmlcode += "<div class='row'><div class='col-sm-1'><div class='col-sm-10'><label for='example-text-input'>" + QANum + "</label>"
                    if ($("Likes", this).text() == "1") {
                        
                        htmlcode += "<a href='#' class='btn btn-default btn-md'><span class='glyphicon glyphicon-thumbs-up'></span> Vote(s)</a>";
                    }
                    else {
                        htmlcode += "<a onclick=\"sendVote('" + QAID + "')\" class='btn btn-info btn-md'><span class='glyphicon glyphicon-thumbs-up'></span> Vote(s)</a>";
                        
                    }                    
                    htmlcode += "<div class='col-10'><label for='example-text-input' class='col-10 col-form-label'>" + QATitle + "</label><hr/></div></div></div></div>";
                   
                });
                $("#messagewindow").html(htmlcode);
            }
            

            function sendVote(QAID) {
                $.ajax({
                    type: "POST",
                    url: "../Handle/SQAVoteInsert.ashx",

                    data: "Uid=" + $("#Uid").val() + "&QAID=" + QAID+"&showid="+$("#showid").val(),

                    success: function (data) {
                        if (data == "2") {
                            alert("Submitted!");

                            return false;
                        }
                        else if (data == "1") {
                           //alert("Repetitive operation");

                            return false;
                        } 

                    }
                });
            }
        </script>
</head>
<body>
    <div class="container">
        <br />
        <div class="row">
            <div class="col-sm-12">
                <textarea class="form-control" cols="1" id="msg" onKeyDown="if (this.value.length>=250){event.returnValue=false}" placeholder="(Maximum 250 characters)" rows="3"></textarea>
                <label for="exampleTextarea">This page will refresh every 15 seconds</label><br /><button type="button" class="btn btn-info" onclick="sendMsg()" style="float:right;">Submit</button>
            </div>
        </div>
        <div id="messagewindow"><span id="loading">Loading...</span></div>&nbsp;&nbsp;
        <input type="hidden" name="meetid" id="meetid" value="<%=meetid%>" />
        <input  type="hidden" name="Uid" id="Uid" value="<%=Uid%>"/>
        <input  type="hidden" name="LID" id="LID" />
    </div>
</body>
</html>