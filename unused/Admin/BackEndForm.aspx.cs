﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Object;
using App.DAL;
using App.Common;

public partial class BackEnd_BackEndForm : System.Web.UI.Page
{
    #region Declaration
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_Parent dalp = new DAL_Parent();
    DAL_Child dalc = new DAL_Child();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    ChildModel cmodel = new ChildModel();
    ParentModel pmodel = new ParentModel();
    BackEnd_Parent Pobj = new BackEnd_Parent();
    BackEnd_Child Cobj = new BackEnd_Child();
    GenKey gk = new GenKey();

    string pid = string.Empty, cid = string.Empty;
    string content = "", content1 = "", content2 = "", content3 = "", content4 = "", content5 = "", comma = "", pname = "";
    #endregion

    #region Class
    class ShowData
    {
        public string tbname { get; set; }

    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //BindGrid();
            //BindDDL();
            BindDDLRole();
            if(Request.QueryString["id"] != null)
            {
                ct.Visible = true;
                string cid = Request.QueryString["id"].ToString();
                string pid = dalc.selectPIDByCID(cid);
                BackEnd_Parent bp = dalp.SelectById(pid);
                txtftitle.Text = bp.p_title;
                txtptitle.Text = bp.p_name;
                rdoftype.SelectedValue = bp.p_type;
                BackEnd_Child bc = dalc.SelectById(cid);
                ctt.SelectedIndex = bc.c_inputtype.Value-1;
                chkRequired.Checked = bc.c_isRequired.Value;
                txtlbl.Text = bc.c_label;
                txtdatacol.Text = bc.c_colname;
                txtso.Text = bc.c_sortorder.ToString();
                txtso.Enabled = false;
                string datavaluefield = string.Empty;
                string datatextfield = string.Empty;
                
                if (!string.IsNullOrEmpty(bc.c_content))
                {
                    var content = bc.c_content;
                    if(content.Contains(':'))
                    {
                        //Bind From Outside Table
                    }
                    else
                    {
                        // Bind From FrontEnd Table
                        if(content.Contains('^'))
                        {
                            if (bc.c_inputtype == 7 || bc.c_inputtype == 9 || bc.c_inputtype == 11)
                            {
                                string[] arr = content.Split('^');
                                datavaluefield = arr[0].ToString();
                                string varpid = dalp.SelectByName(datavaluefield).p_id;
                                ddlRole1.SelectedValue = varpid.ToString();

                                datatextfield = arr[1].ToString();
                                divdt.Visible = true;
                                ddlRole1.Visible = true;
                                divdc.Visible = true;
                                ddldcol.Visible = true;
                                BindDDLCol(varpid);
                                string varcid = dalc.selectCIDByPIDLabel(varpid, datatextfield);
                                ddldcol.SelectedValue = varcid;
                            }
                        }
                    }
                }
                //if (bc.c_inputtype == 7 || bc.c_inputtype == 9 || bc.c_inputtype == 11)
                //{
                //    ddlRole1.Visible = true;
                //    ddlRole1.SelectedValue = datavaluefield;

                //    ddldcol.SelectedValue=
                //}
                ////if(bc.c_inputtype == 11)
                ////{
                ////    ddlRole2.Visible = true;

                ////    ddlRole2.SelectedValue = datavaluefield;
                ////}

                if(!string.IsNullOrEmpty(bc.c_content))
                {
                    string[] arr = bc.c_content.Split(',');
                    if (arr.Count() > 0)
                    {
                        lblcontent.Visible = true;
                        txtcontent1.Visible = true;
                        btnadd1.Visible = true;
                        txtcontent1.Text = arr[0].ToString();
                    }
                    if (arr.Count() > 1)
                    {
                        txtcontent2.Visible = true;
                        btnadd2.Visible = true;
                        txtcontent2.Text = arr[1].ToString();
                    }
                    if (arr.Count() > 2)
                    {
                        txtcontent3.Visible = true;
                        btnadd3.Visible = true;
                        txtcontent3.Text = arr[2].ToString();
                    }
                    if (arr.Count() > 3)
                    {
                        txtcontent4.Visible = true;
                        btnadd4.Visible = true;
                        txtcontent4.Text = arr[3].ToString();
                    }
                    if (arr.Count() > 4)
                    {
                        txtcontent5.Visible = true;
                        btnadd5.Visible = true;
                        txtcontent5.Text = arr[4].ToString();
                    }
                }
               
                btncreateForm.Visible = false;
                btnsave.Text = "Update";
                btnCreate.Text = "Update Form Name";

            }
        }
    }
    #endregion   

    #region ParentTable Create

    #region ParentForm Button Click
    protected void btnCreate_OnClick(object sender, EventArgs e)
    {
        //ct.Visible = true;
        try
        {
            string pkey = string.Empty; string pid = string.Empty;
            

            txtftitle.Enabled = false;
            txtptitle.Enabled = false;
            btnCreate.Enabled = false;        
            if(Request.QueryString["id"] != null)
            {
                #region Update Parent
                string cid = Request.QueryString["id"].ToString();
                pid = dalc.selectPIDByCID(cid);
                var lst = dalp.SelectById(pid);
                if(lst != null)
                Pobj.p_name = txtptitle.Text;
                Pobj.p_title = txtftitle.Text;
                Pobj.p_type = rdoftype.SelectedValue;
                Pobj.p_id = pid;

                dalp.updateInfo(Pobj);
                btnCreate.Visible = false;
                #endregion
            }
            else
            {
                #region Save Parent

                pkey = gk.getKey("BP");
                pid = "BP" + pkey;

                Pobj.p_id = pid;
                Pobj.p_name = txtptitle.Text;
                Pobj.p_title = txtftitle.Text;
                Pobj.p_type = rdoftype.SelectedValue;
                Pobj.p_isFinish = false;
                int count = dalp.CheckExist(Pobj);
                if (count == 0)
                {
                    int isSuccess = dalp.Save(Pobj);
                    if (isSuccess > 0)
                    {
                        ct.Visible = true;
                        gk.SaveKey("BP", int.Parse(pkey));
                    }
                }
           
                #endregion
            }
        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region UpdateData
    //private bool UpdateData()
    //{
    //    bool isSuccess;
    //    ParentForm p = new ParentForm
    //    {
    //        p_id = Convert.ToInt32(hfpid.Value),
    //        p_title = txtptitle.Text.Trim(),
    //        p_isFinish = true
    //    };
    //    bllpt.pt = p;
    //    try
    //    {
    //        bllpt.update();
    //        isSuccess = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        isSuccess = false;
    //    }
    //    return isSuccess;
    //}
    #endregion

    #region InsertData
    //private bool InsertData()
    //{
    //    bool isSuccess;
    //    Model.partentTable p = new Model.partentTable
    //    {
    //        p_title = txtptitle.Text.Trim(),
    //        p_isFinish = false
    //    };
    //    bllpt.pt = p;
    //    try
    //    {
    //        bllpt.save();
    //        isSuccess = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        isSuccess = false;
    //    }
    //    return isSuccess;
    //}
    #endregion

    #endregion

    #region Add Button onclick
    protected void btnadd1_onclick(object sender, EventArgs e)
    {
        if (btnadd1.Text == "Add")
        {
            btnadd1.Text = "Delete";
            btnadd2.Visible = true;
            txtcontent2.Visible = true;
        }
        else if (btnadd1.Text == "Delete")
        {
            txtcontent1.Text = "";
            btnadd1.Text = "Add";
            btnadd1.Visible = false;
            txtcontent1.Visible = false;

        }
    }
    protected void btnadd2_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        if (btnadd2.Text == "Add")
        {
            btnadd2.Text = "Delete";
            btnadd3.Visible = true;
            txtcontent3.Visible = true;
        }
        else if (btnadd2.Text == "Delete")
        {
            txtcontent2.Text = "";
            btnadd2.Text = "Add";
            btnadd2.Visible = false;
            txtcontent2.Visible = false;

        }
    }
    protected void btnadd3_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        btnadd2.Visible = true;
        txtcontent2.Visible = true;
        if (btnadd3.Text == "Add")
        {
            btnadd3.Text = "Delete";
            btnadd4.Visible = true;
            txtcontent4.Visible = true;
        }
        else if (btnadd3.Text == "Delete")
        {
            txtcontent3.Text = "";
            btnadd3.Text = "Add";
            btnadd3.Visible = false;
            txtcontent3.Visible = false;

        }
    }
    protected void btnadd4_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        txtcontent2.Visible = true;
        btnadd2.Visible = true;
        txtcontent3.Visible = true;
        btnadd3.Visible = true;
        if (btnadd4.Text == "Add")
        {
            btnadd4.Text = "Delete";
            btnadd5.Visible = true;
            txtcontent5.Visible = true;
        }
        else if (btnadd4.Text == "Delete")
        {
            txtcontent4.Text = "";
            btnadd4.Text = "Add";
            btnadd4.Visible = false;
            txtcontent4.Visible = false;
        }
    }
    protected void btnadd5_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        txtcontent2.Visible = true;
        btnadd2.Visible = true;
        txtcontent3.Visible = true;
        btnadd3.Visible = true;
        txtcontent4.Visible = true;
        btnadd4.Visible = true;
        if (btnadd5.Text == "Add")
        {
            btnadd5.Text = "Delete";
        }
        else if (btnadd5.Text == "Delete")
        {
            txtcontent5.Text = "";
            btnadd5.Text = "Add";
            btnadd5.Visible = false;
            txtcontent5.Visible = false;

        }
    }
    #endregion

    #region rdo selectedindexchange   
    protected void rdolst_SelectedIndexChanged(object sender, EventArgs e)
    {
        divdt.Visible = true;
        ddlRole1.Visible = false;
        //ddlRole2.Visible = false;
        if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
        {
            lblcontent.Visible = true;
            txtcontent1.Visible = true;
            btnadd1.Visible = true;
        }
        else
        {
            lblcontent.Visible = false;
            txtcontent1.Visible = false;
            btnadd1.Visible = false;
        }

        if (ctt.SelectedValue == "11" || ctt.SelectedValue == "9" || ctt.SelectedValue == "7")
        {
            if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            {
                ddlRole1.Visible = true;
                
            }
            //else if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            //{
            //    ddlRole1.Visible = false;
            //    ddlRole2.Visible = true;
            //}
        }
        else
        {
            ddlRole1.Visible = false;
            //ddlRole2.Visible = false;
        }
    }
    #endregion

    #region rdo rdoCheckDynamic_SelectedIndexChanged
    protected void rdoCheckDynamic_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDDLRole();
        if (ctt.SelectedValue == "11" || ctt.SelectedValue == "9" || ctt.SelectedValue == "7")
        {
            ddlRole1.Visible = true;
            //if (ctt.SelectedValue == "11")
            //{
            //    ddlRole1.Visible = true;
            //    ddlRole2.Visible = false;
            //}
            //else if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9")
            //{
            //    ddlRole1.Visible = false;
            //    ddlRole2.Visible = true;
            //}
        }
        else
        {
            ddlRole1.Visible = false;
            //ddlRole2.Visible = false;
        }
    }
    protected void ddlRole1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(ddlRole1.SelectedValue.ToString()))
        {
            if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            {
                divdc.Visible = true;
                ddldcol.Visible = true;
                string id = ddlRole1.SelectedValue.ToString();
                BindDDLCol(id);
            }
            else
            {
                txtcontent1.Text = ddlRole1.SelectedItem.ToString();
            }
        }
        
    }
    //protected void ddlRole2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(ddlRole2.SelectedValue.ToString()))
    //    {
    //        if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9")
    //        {
    //            divdc.Visible = true;
    //            ddldcol.Visible = true;
    //            int id = int.Parse(ddlRole2.SelectedValue.ToString());
    //            BindDDLCol(id);                
    //        }
    //        else
    //        {
    //            txtcontent1.Text = ddlRole2.SelectedItem.ToString();
    //        }
    //    }

    //}
    protected void ddldcol_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(ddldcol.SelectedValue.ToString()))
        {
            string str = ddlRole1.SelectedItem.ToString() + "^" + ddldcol.SelectedItem.ToString();
            txtcontent1.Text = str;
        }
    }
    
    #endregion

    #region BindDDLRole
    private void BindDDLRole()
    {
        var lstParent=dalp.SelectAllData();
        #region ddlRole1
        ddlRole1.DataSource = lstParent;
        ddlRole1.DataTextField = "p_name";
        ddlRole1.DataValueField = "p_id";
        ddlRole1.DataBind();
        ListItem lst = new ListItem("Select Table", "");
        ddlRole1.Items.Insert(ddlRole1.Items.Count - ddlRole1.Items.Count, lst);
        
        #endregion

        #region ddlRole2
        //ddlRole2.DataSource = lstParent;
        //ddlRole2.DataTextField = "p_name";
        //ddlRole2.DataValueField = "p_id";
        //ListItem lst2 = new ListItem("Select Table", "");
        //ddlRole2.Items.Insert(ddlRole2.Items.Count - ddlRole2.Items.Count, lst2);
        //ddlRole2.DataBind();
        #endregion
    }
    #endregion

    #region BindDDLColumn
    private void BindDDLCol(string tbid)
    {
        var lstchild = dalc.SelectByPId(tbid);
        ddldcol.DataSource = lstchild;
        ddldcol.DataValueField = "c_id";
        ddldcol.DataTextField = "c_label";
        ddldcol.DataBind();
        ListItem lst = new ListItem("Choose Column to Bind", "");
        ddldcol.Items.Insert(ddldcol.Items.Count - ddldcol.Items.Count, lst);
        

    }
    #endregion

    #region childTable Save

    #region Button Click
    protected void btnsave_OnClick(object sender, EventArgs e)
    {
        try
        {
            #region AddButton
            if (txtcontent1.Text != "" && (txtcontent2.Text != "" || txtcontent3.Text != "" || txtcontent4.Text != "" || txtcontent5.Text != ""))
            {
                content1 = txtcontent1.Text.Trim() + ",";
            }
            else
            {
                content1 = txtcontent1.Text.Trim();
            }

            if (txtcontent2.Text != "")
            {
                content2 = txtcontent2.Text.Trim() + ",";
            }
            else if (content2 == "")
            {
                content1 = txtcontent1.Text.Trim();
            }

            if (txtcontent3.Text != "")
            {
                content3 = txtcontent3.Text.Trim() + ",";
            }
            else if (content3 == "")
            {
                content2 = txtcontent2.Text.Trim();
            }

            if (txtcontent4.Text != "")
            {
                content4 = txtcontent4.Text.Trim() + ",";
            }
            else if (content4 == "")
            {
                content3 = txtcontent3.Text.Trim();
            }

            if (txtcontent5.Text != "")
            {
                content5 = txtcontent5.Text.Trim();
            }
            else if (content5 == "")
            {
                content4 = txtcontent4.Text.Trim();
            }
            #endregion

            #region Add Infos
            content = content1 + content2 + content3 + content4 + content5;

            bool isreq;
            if (chkRequired.Checked == true)
            {
                isreq = true;
            }
            else
            {
                isreq = false;
            }

            pname = txtptitle.Text;
            var res = dalp.SelectByName(pname);

            pid = res.p_id;

            string rbt = ctt.SelectedValue.ToString();//GetCheckedRadio(ctt);
            string str = rbt.Split('_').LastOrDefault();

            if (str == "11" || str == "9" || str == "7")
            {
                content = txtcontent1.Text.Trim();
            }
            #endregion
            if (Request.QueryString["id"] != null)
            {
                #region Update
                string cid = Request.QueryString["id"].ToString();
                var lst = dalc.SelectByPId(cid);
                if(lst != null)
                {
                    Cobj.c_pid = pid;
                    Cobj.c_inputtype = Convert.ToInt32(str);
                    Cobj.c_label = txtlbl.Text.Trim();
                    Cobj.c_colname = txtdatacol.Text.Trim();
                    Cobj.c_content = content;
                    Cobj.c_sortorder = Convert.ToInt32(txtso.Text.Trim());
                    Cobj.c_isRequired = isreq;
                    Cobj.c_deleteflag = false;
                    Cobj.c_id = cid;
                    dalc.update(Cobj);                   
                }
                var lstf = dalf.selectbyPIDCID(pid,cid);
                if(lstf.Count>0)
                {
                    foreach(FrontEnd fe in lstf)
                    {
                        fe.f_label = txtlbl.Text.Trim();
                        fe.f_inputtype= Convert.ToInt32(str);
                        fe.f_sortorder= Convert.ToInt32(txtso.Text.Trim());
                        fe.f_id = fe.f_id;
                        dalf.updateForBackend(fe);
                    }
                }
                Response.Redirect("~/Admin/BackEndFormList.aspx");
                #endregion
            }
            else
            {
                #region Save
                string ckey = string.Empty; string cid = string.Empty;
                ckey = gk.getKey("BC");
                cid = "BC" + ckey;

                Cobj.c_pid = pid;
                Cobj.c_id = cid;
                Cobj.c_inputtype = Convert.ToInt32(str);
                Cobj.c_label = txtlbl.Text.Trim();
                Cobj.c_colname = txtdatacol.Text.Trim();
                Cobj.c_content = content;
                Cobj.c_sortorder = Convert.ToInt32(txtso.Text.Trim());
                Cobj.c_isRequired = isreq;
                Cobj.c_deleteflag = false;
                //int count = dalc.CheckExist(Cobj);
                //if (count == 0)
                //{
                int isSuccess = dalc.Save(Cobj);
                if (isSuccess > 0)
                {
                    gk.SaveKey("BC", int.Parse(ckey));
                    ct.Visible = true;
                    ResetChild();
                }
                else
                {

                }
                //}
                #endregion
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.InnerException);
        }
    }
    #endregion

    #region BindChildGrid
    //private void BindChildGrid()
    //{
    //    List<ChildModel> lst = new List<ChildModel>();
    //    int pid = Convert.ToInt32(hfpid.Value);
    //    var res = dalc.SelectByPId(pid);

    //    ChildModel k;
    //    foreach (var i in res)
    //    {
    //        k = new ChildModel
    //        {
    //            c_pid = i.c_pid,
    //            c_id = i.c_id,
    //            c_label = i.c_label,
    //            inputtype = i.c_inputtype == 1 ? "TextBox" : i.c_inputtype == 2 ? "TextArea" : i.c_inputtype == 3 ? "RadioButton" : i.c_inputtype == 4 ? "CheckBox" : i.c_inputtype == 5 ? "DropDownList" : i.c_inputtype == 6 ? "Date" : i.c_inputtype == 7 ? "Dropdown Data Bind" : i.c_inputtype == 8 ? "HiddenField for Int" : i.c_inputtype == 9 ? "Hidden Field" :i.c_inputtype == 10 ? "CKEditor" : "",
    //            c_content = i.c_content,
    //            c_sortorder = i.c_sortorder
    //        };
    //        lst.Add(k);
    //    }
    //    grdCgrid.DataSource = lst;
    //    grdCgrid.DataBind();
    //    grdCgrid.Visible = true;
    //}
    #endregion

    #region ResetChild
    private void ResetChild()
    {
        ctt.SelectedValue = "1";
        //rdoTextBox_1.Checked = true;
        //rdoTextArea_2.Checked = false;
        //rdoRadioButton_3.Checked = false;
        //rdoCheckBox_4.Checked = false;
        //rdoddl_5.Checked = false;
        //rdoDate_6.Checked = false;
        //rdoddl_7.Checked = false;
        //rdohiddenint_8.Checked = false;
        //rdoHidden_9.Checked = false;
        //rdoCKEditor_10.Checked = false;
        //rdoCheckDynamic_11.Checked = false;
        //rdoUpload_12.Checked = false;
        chkRequired.Checked = false;
        ddlRole1.SelectedIndex = 0;
        //ddlRole2.SelectedIndex = 0;
        txtlbl.Text = "";
        txtcontent1.Text = "";
        txtcontent2.Text = "";
        txtcontent3.Text = "";
        txtcontent4.Text = "";
        txtcontent5.Text = "";
        txtso.Text = "";

        txtcontent1.Visible = false;
        txtcontent2.Visible = false;
        txtcontent3.Visible = false;
        txtcontent4.Visible = false;
        txtcontent5.Visible = false;
        btnadd1.Visible = false;
        btnadd2.Visible = false;
        btnadd3.Visible = false;
        btnadd4.Visible = false;
        btnadd5.Visible = false;
        ddlRole1.Visible = false;
        //ddlRole2.Visible = false;
        btnadd1.Text = "Add";
        btnadd2.Text = "Add";
        btnadd3.Text = "Add";
        btnadd4.Text = "Add";
        btnadd5.Text = "Add";

    }
    #endregion

    String GetCheckedRadio(Control ct)
    {
        foreach (var control in ct.Controls)
        {
            RadioButton radio = control as RadioButton;

            if (radio != null && radio.Checked)
            {
                return radio.ID;
            }
        }

        return null;
    }
    #endregion

    #region Create Form
    protected void btncreateForm_OnClick(object sender, EventArgs e)
    {
        try
        {
            string p_name = txtptitle.Text;
            var res = dalp.SelectByName(p_name);
            if (res != null)
            {
                pid = res.p_id;
            }
            hfpid.Value = pid.ToString();

            if (Update(pid))
            {
                txtftitle.Enabled = true;
                txtptitle.Enabled = true;
                btnCreate.Enabled = true;                
                //grdCgrid.DataSource = null;
                //grdCgrid.DataBind();
                //grdCgrid.Visible = false;
                ResetForm();
            }
        }
        catch (Exception ex) { }
    }

    #region Update
    private bool Update(string pid)
    {
        bool isSuccess;

        Pobj.p_id = pid;
        Pobj.p_isFinish = true;

        try
        {
            dalp.update(Pobj);
            isSuccess = true;
        }
        catch (Exception ex)
        {
            isSuccess = false;
        }
        return isSuccess;
    }
    #endregion

    #region ResetForm
    private void ResetForm()
    {
        txtptitle.Text = "";
        txtftitle.Text = "";
        hfpid.Value = "0";
        ct.Visible = false;

    }
    #endregion
    #endregion

    #region grdView_PageIndexChanging
    //protected void grdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    grdView.PageIndex = e.NewPageIndex;
    //    BindGrid();
    //    grdView.DataBind();
    //    div1.Visible = false;
    //}
    //protected void grdCgrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    grdCgrid.PageIndex = e.NewPageIndex;
    //    BindChildGrid();
    //    grdCgrid.DataBind();
    //    //div1.Visible = false;
    //}
    #endregion

    #region Delete
    protected void lnkDelete_Onclick(object sender, EventArgs e)
    {

        string id = string.Empty;
        id = ((LinkButton)sender).CommandArgument;

        try
        {
            dalc.Delete(id);
            //div1.Visible = true;
            //lblmsg.Text = "Delete Successful !!!";
            //div1.Attributes.Add("class", "alert-box success radius");

        }
        catch (Exception ex)
        {
            //div1.Visible = true;
            //lblmsg.Text = "Delete Fail !!!";
            //div1.Attributes.Add("class", "alert-box alert radius");
        }
    }
    #endregion
}