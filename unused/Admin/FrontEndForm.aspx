﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="FrontEndForm.aspx.cs" Inherits="Admin_FrontEndForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
<%--    <script type="text/javascript">
        function RowDataBound(sender, e) {
         var colorder = Convert.ToInt32("1");
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var rowid = e.Row.Cells[colorder].Text;
            e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a href='FrontEndForm_Add.aspx' class='btn btn-primary'></a>"));
        }
}
    </script>--%>
    <style type="text/css">
        .spaced input[type="radio"]
        {
           margin-left: 50px; /* Or any other value */
           margin-right: 5px; /* Or any other value */
        }
    </style>
    
     <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdtelerik" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                        <asp:HiddenField ID="hdPID" runat="server" />
                        <asp:Label ID="hdRowCount" runat="server" Visible="false"></asp:Label>
                        <h3 class="box-title"><asp:Label ID="lblformname" runat="server"></asp:Label></h3>
                        <div style="float:right">                            
                            <asp:LinkButton ID="lnkAdd" runat="server" class="btn btn-info" OnClick="lnkAdd_OnClick"><span class="fa fa-plus">&nbsp; Add New</span></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-info" OnClick="lnkDownload_OnClick"><span class="fa fa-download">&nbsp; Download</span></asp:LinkButton>
                        </div>                        
                </div>                                       
                <div class="box-body"> 
                    <div class="row" style="margin-bottom:10PX;"> 
                        <div class="col-md-5">
                        <asp:TextBox ID="txtKeyword" class="form-control" placeholder="Enter Keyword" runat="server"></asp:TextBox>                    
                        </div>
                        <div class="col-md-1">                        
                            <asp:Button ID="btnSearch" OnClick="btnSearch_Click" class="btn btn-info" runat="server" Text="Search" />   
                        </div>
                        <div class="col-md-6">
                        <asp:RadioButtonList ID="chkLang" RepeatDirection="Horizontal" CssClass="spaced" OnSelectedIndexChanged="chkLang_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:RadioButtonList>
                        </div>
                        
                    </div>  
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="table-responsive">
                                <asp:GridView ID="grdDynamic" PageSize="2000" GridLines="Horizontal" Font-Size="Small"
                                AutoGenerateColumns="False" runat="server" class="table" Width="100%"  onrowdatabound="GridView_RowDataBound" OnRowCommand="GridView_RowCommand"
                                AllowPaging="True" onpageindexchanging="grdDynamic_PageIndexChanging" HeaderStyle-HorizontalAlign="Left" CellPadding="4" BackColor="White" BorderColor="#336666" BorderWidth="1px" BorderStyle="None">
                                <EmptyDataTemplate>
                                    <label>There is no data to display.</label>
                                </EmptyDataTemplate>
                                     <FooterStyle BackColor="White" ForeColor="#333333" />

                                     <HeaderStyle HorizontalAlign="Left" BackColor="#336666" Font-Bold="True" ForeColor="White"></HeaderStyle>
                                     <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                                     <RowStyle BackColor="White" ForeColor="#333333" />
                                     <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                                     <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                     <SortedAscendingHeaderStyle BackColor="#487575" />
                                     <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                     <SortedDescendingHeaderStyle BackColor="#275353" />
                                </asp:GridView>
                                <%--<telerik:RadGrid RenderMode="Lightweight" ID="grdtelerik"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Visible="true" AllowPaging="true" PageSize="5" Font-Size="Small"
                                    PagerStyle-AlwaysVisible="true" AllowSorting="true" ClientSettings-ClientEvents-OnRowDataBound="GridView_RowDataBound" OnNeedDataSource="grdtelerik_NeedDSource" AutoGenerateColumns="true" HeaderStyle-BackColor="#507CD1" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true">
                                    <ClientSettings AllowKeyboardNavigation="true">
                                        <Selecting AllowRowSelect="true"></Selecting>
                                    </ClientSettings>
                                    <MasterTableView AutoGenerateColumns="true" DataKeyNames="No" CommandItemDisplay="Top">
                                        <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                                    </MasterTableView>
                                </telerik:RadGrid>--%>
                            </div>   
                        </div> 
                    </div>
                </div>
          </div>
        </div>
    </section>
    
</asp:Content>

