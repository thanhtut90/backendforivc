﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/MasterPages/Site.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="FrontEndForm_Add.aspx.cs" Inherits="Admin_FrontEndForm_Add" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  
    <link rel="stylesheet" href="../Content/plugins/datepicker/datepicker3.css">
    <script src="../Content/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
    <script src="../Content/plugins/datepicker/bootstrap-datepicker.js"></script>    
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>

      <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">
                <asp:Label ID="lblHead" runat="server" Font-Bold="true" Font-Size="X-Large"></asp:Label>
              </h3>              
            </div>
            <div class="form-horizontal">
                    <div class="form-group"> 
                      <div class="col-sm-12">
                          <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <center><asp:Label ID="lblMessage" runat="server"></asp:Label></center>
                            <asp:Label ID="lblBackID" runat="server" Visible="false"></asp:Label>
                          </div>   
                      </div>
                    </div>
                    <fieldset id="div1" runat="server"></fieldset>
                    <div class="box-footer">                                
                        <input type="Submit" id="btnCreate"  class="btn btn-primary" runat="server" value="Save" onserverclick="btnCreate_OnClick" />&nbsp;&nbsp;
                        <input type="button" id="btnCancel"  class="btn btn-default" runat="server" value="Cancel" onserverclick="btnbtnCancel_OnClick"/>&nbsp;&nbsp;
                        <asp:Button ID="btnDelete" class="btn btn-danger" OnClick="btnDelete_OnClick" OnClientClick="return confirm('Are you sure you want to delete this item ?')" runat="server" Text="Delete"></asp:Button>&nbsp;&nbsp;
                    </div>   
            </div>
        </div>
    </div>




    <br />
    
    <br />
    <br />
    
    <%--<asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
        <form class="form-horizontal">
            
            <br />
            <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">                                           
                
            </div>
          </div>

           
    </form>--%>
    <script type="text/javascript">
        //Date picker
        $('#MainContent_datepicker').datepicker({
            autoclose: true
        });
    </script>
</asp:Content>
