﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using Telerik.Web.UI;
using App.DAL;
using App.Common;
using App.Object;
using System.Data;

public partial class Admin_FrontEndForm_Add : System.Web.UI.Page
{
    #region Declaration
    IsabellaFormEntities dbe = new IsabellaFormEntities();
    DAL_Parent dalbp = new DAL_Parent();
    DAL_Child dalbc = new DAL_Child();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    GenKey gk = new GenKey();
    DAL dal = new DAL();
    int lang = 0;
    string serverPath = HttpContext.Current.Server.MapPath("~/");
    #endregion

    #region Class
    class CombineFrontBack : FrontEnd
    {
        public string id { get; set; }
        public string pid { get; set; }
        public int? inputtype { get; set; }
        public string label { get; set; }
        public string content { get; set; }
        public int? sortorder { get; set; }
        public bool? isRequired { get; set; }
        public bool? deleteflag { get; set; }
        public string fcontent { get; set; }
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {            
            ViewState["front_childID"] = null;
            string pid = "";
            if (Request.QueryString["id"] != null)
            {                
                btnDelete.Visible = false;

                if (Request.QueryString["rid"] != null)
                {
                    btnDelete.Visible = true;                   
                    btnCreate.Value = "Update";
                }

                pid = Request.QueryString["id"].ToString();
                var lstParent = dalbp.SelectById(pid);
                if (lstParent != null)
                {
                    lblHead.Text = lstParent.p_name;
                    if (lblHead.Text.Contains("/"))
                    {
                        lblHead.Text = lblHead.Text.Replace("/", " ");
                    }
                    DirectoryInfo directory = new DirectoryInfo(serverPath + "\\files\\" + lblHead.Text);

                    if (!directory.Exists)
                    {
                        directory.Create();
                    }
                }                
                ViewState["entry_count"] = 1;
                lang= Convert.ToInt32(Request.QueryString["lang"].ToString());
                BindControls("",lang);
            }
            ////for password request
            //if (Request.QueryString["id"] == "40" && Request.QueryString["s"] == "suc")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Successfully Requested !! \n Please wait for the reply from Administraion Department.')", true);
            //}
            //if (Request.QueryString["id"] == "41" && Request.QueryString["s"] == "suc")
            //{
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Successfully Sent !! \n Please wait for the reply from Administraion Department.')", true);
            //}
        }
        else
        {
            btnCreate.Disabled = false;
        }
    }
    #endregion
    
    #region Only BindControls
    private void OnlyBindControls(List<CombineFrontBack> lstchild, string attach, string backpid, string entrycount)
    {
        try
        {
            if (Request.QueryString["id"] != null && Request.QueryString["rid"] == null)
            {
                #region Simple Bind
                if (lstchild.Count > 0)
                {
                    int datepicker_count = 1;
                    foreach (var item in lstchild)
                    {
                        if (item.inputtype == 1)
                        {
                            #region TextBox
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            //this.div1.Controls.Add(new LiteralControl("<div class='input-control text' data-role='input-control'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            
                            this.div1.Controls.Add(txt);
                            
                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;"+item.label;
                                
                            }
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));                            
                            #endregion
                        }
                        else if (item.inputtype == 2)// Password
                        {
                            #region TextBox Password
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            txt.TextMode = TextBoxMode.Password;

                            this.div1.Controls.Add(txt);

                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;

                            }
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 3)// email
                        {
                            #region TextBox email
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            txt.TextMode = TextBoxMode.Email;

                            this.div1.Controls.Add(txt);

                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;

                            }
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 4)
                        {
                            #region TextArea
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.TextMode = TextBoxMode.MultiLine;
                            txt.Attributes.Add("class", "form-control");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 5)
                        {
                            #region Telerik Editor
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            //RadEditor editor = new RadEditor();
                            //editor.ID = "editor" + textid;
                            //editor.EditModes = EditModes.Design;
                            //editor.Attributes.Add("runat", "server");
                            //editor.Skin = "Metro";
                            //this.div1.Controls.Add(editor);
                            TextBox ckeditor = new TextBox();
                            ckeditor.ID = "cke" + textid;
                            ckeditor.Attributes.Add("class", "ckeditor");
                            ckeditor.TextMode = TextBoxMode.MultiLine;
                            this.div1.Controls.Add(ckeditor);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 6)
                        {
                            #region RadioButton
                            string rid = item.id;
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;
                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                this.div1.Controls.Add(lbl);

                                int first_rbt = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    first_rbt++;
                                    var name = rad.ToString();
                                    //rid++;
                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    RadioButton childrd = new RadioButton();
                                    childrd.ID = item.inputtype.ToString();
                                    childrd.Attributes.Add("Value", rad.ToString());

                                    if (first_rbt == 1)
                                    {
                                        childrd.Checked = true;
                                    }

                                    childrd.GroupName = "RbtGroup";

                                    this.div1.Controls.Add(childrd);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + rad.ToString() + "</label>"));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                }
                                if(item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 7)
                        {
                            #region RadioButton (Dynamic Data)
                            string rid = item.id;
                            
                            if (item.content != null && item.content != "")
                            {
                                string[] str = new string[0];
                                
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
                                string formname = string.Empty, colname = string.Empty, condition = string.Empty; condition = string.Empty;
                                //OutsideTB:tb_Menu^m_TitleEn^m_pageType=NewList
                                string[] arr = new string[0];
                                if (item.content.Contains(':'))
                                {
                                    string[] ck = item.content.Split(':');
                                    arr = ck[1].Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }

                                    #region
                                    DataTable cData = new DataTable();
                                    cData = dal.selectMenuByPageType(condition);
                                    this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                    Label lbl = new Label();
                                    lbl.Text = item.label;
                                    lbl.Attributes.Add("class", "col-sm-2 control-label");
                                    this.div1.Controls.Add(lbl);

                                    int first_rbt = 0;
                                    this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                    foreach (DataRow rad in cData.Rows)
                                    {
                                        first_rbt++;
                                        var name = rad["m_titleEn"].ToString();
                                        //rid++;
                                        this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                        RadioButton childrd = new RadioButton();
                                        childrd.ID = item.inputtype.ToString();
                                        childrd.Attributes.Add("Value", rad["m_id"].ToString());

                                        if (first_rbt == 1)
                                        {
                                            childrd.Checked = true;
                                        }

                                        childrd.GroupName = "RbtGroup";

                                        this.div1.Controls.Add(childrd);
                                        this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + name + "</label>"));
                                        this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    }
                                    if (item.isRequired == true)
                                    {
                                        lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                    }
                                    this.div1.Controls.Add(new LiteralControl("</div></div>"));
                                    #endregion
                                }
                                else
                                {
                                    arr = item.content.Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }

                                    #region
                                    var pData = dalbp.SelectByName(formname);

                                    if (pData != null)
                                    {
                                        List<FrontEndModel> cData = new List<FrontEndModel>();
                                        if (arr.Count() > 2)
                                        {
                                            cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                        }
                                        else
                                        {
                                            cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                        }
                                        this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                        Label lbl = new Label();
                                        lbl.Text = item.label;
                                        lbl.Attributes.Add("class", "col-sm-2 control-label");
                                        this.div1.Controls.Add(lbl);

                                        int first_rbt = 0;
                                        this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                        foreach (var rad in cData)
                                        {
                                            first_rbt++;
                                            var name = rad.ToString();
                                            //rid++;
                                            this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                            RadioButton childrd = new RadioButton();
                                            childrd.ID = item.inputtype.ToString();
                                            childrd.Attributes.Add("Value", rad.f_c_rowid.ToString());

                                            if (first_rbt == 1)
                                            {
                                                childrd.Checked = true;
                                            }

                                            childrd.GroupName = "RbtGroup";

                                            this.div1.Controls.Add(childrd);
                                            this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + rad.Data.ToString() + "</label>"));
                                            this.div1.Controls.Add(new LiteralControl("<br/>"));
                                        }
                                        if (item.isRequired == true)
                                        {
                                            lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                        }
                                        this.div1.Controls.Add(new LiteralControl("</div></div>"));
                                    }
                                    #endregion
                                }


                            }
                            #endregion
                        }
                        else if (item.inputtype == 8)
                        {
                            #region CheckBox
                            string rid = item.id;
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;                             

                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                
                                this.div1.Controls.Add(lbl);                               

                                int count = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    var name = rad.ToString();

                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    CheckBox chkIndividual = new CheckBox();
                                    //chkIndividual.CssClass= "icheckbox_minimal-blue checked";
                                    chkIndividual.ID = "chk" + rid.ToString() + "$" + count.ToString();
                                    this.div1.Controls.Add(chkIndividual);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + rad.ToString() + "</label> "));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    count++;
                                }
                                if (item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 9)
                        {
                            #region CheckBox (Dynamic Data)

                            string rid = item.id;
                            string[] str = new string[0];
                            string fcontent = "";
                            int c = 0;
                            if (item.content != null && item.content != "")
                            {
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
                                string formname = string.Empty, colname = string.Empty, condition = string.Empty; condition = string.Empty;
                                string[] arr = new string[0];
                                if (item.content.Contains(':'))
                                {
                                    string[] ck = item.content.Split(':');
                                    arr = ck[1].Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region 
                                    DataTable cData = new DataTable();
                                    cData = dal.selectMenuByPageType(condition);
                                    c = 1;
                                    foreach (DataRow i in cData.Rows)
                                    {
                                        if (c != cData.Rows.Count)
                                        {
                                            fcontent += i["m_titleEn"].ToString() + ",";
                                        }
                                        else
                                        {
                                            fcontent += i["m_titleEn"].ToString();
                                        }
                                        c++;
                                    }
                                    str = fcontent.Split(',');
                                    #endregion
                                }
                                else
                                {
                                    arr = item.content.Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region 
                                    var pData = dalbp.SelectByName(formname);
                                    if (pData != null)
                                    {
                                        List<FrontEndModel> cData = new List<FrontEndModel>();
                                        if (arr.Count() > 2)
                                        {
                                            cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                        }
                                        else
                                        {
                                            cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                        }
                                        c = 1;
                                        foreach (var i in cData)
                                        {
                                            if (c != cData.Count)
                                            {
                                                fcontent += i.Data + ",";
                                            }
                                            else
                                            {
                                                fcontent += i.Data;
                                            }
                                            c++;
                                        }
                                    }
                                    str = fcontent.Split(',');
                                    #endregion
                                }


                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;
                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                this.div1.Controls.Add(lbl);

                                int count = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    var name = rad.ToString();

                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    CheckBox chkIndividual = new CheckBox();

                                    chkIndividual.ID = "chk" + rid.ToString() + "$" + count.ToString();
                                    this.div1.Controls.Add(chkIndividual);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span> " + rad.ToString() + "</label> "));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    count++;
                                }
                                if (item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 10)
                        {
                            #region DropDownList
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);
                            //this.div1.Controls.Add(new LiteralControl("<br/>"));

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            DropDownList ddl = new DropDownList();

                            ddl.ID = "ddl" + textid;

                            ddl.Attributes.Add("runat", "server");
                            ddl.Attributes.Add("class", "form-control");
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                int index = 0;
                                foreach (var ddlitem in str)
                                {
                                    ListItem lstitem = new ListItem();

                                    if (item.isRequired == true)
                                    {
                                        lstitem = new ListItem(ddlitem.ToString(), ddlitem.ToString());
                                    }
                                    else
                                    {
                                        lstitem = new ListItem(ddlitem.ToString(), ddlitem.ToString());
                                    }
                                    ddl.Items.Insert(index, lstitem);
                                    index++;
                                }
                            }
                            ListItem lstt = new ListItem("--- Please Select One --- ", "");
                            ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                            if (item.isRequired == true)
                            {
                                ddl.Attributes.Add("required", "required");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            ddl.SelectedIndex = 0;
                            
                            this.div1.Controls.Add(ddl);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }                       
                        else if (item.inputtype == 11)
                        {
                            #region DropDownList(Dynamic)

                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            DropDownList ddl = new DropDownList();
                            ddl.ID = "ddl" + textid;
                            ddl.Attributes.Add("runat", "server");
                            ddl.Attributes.Add("class", "form-control");
                            
                            string formname = string.Empty, colname = string.Empty, condition = string.Empty; condition = string.Empty;

                            string[] arr = new string[0];
                            if (item.content.Contains(':'))
                            {
                                string[] ck = item.content.Split(':');
                                arr = ck[1].Split('^');

                                if (arr.Count() > 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                    condition = arr[2];
                                }
                                else if (arr.Count() == 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                }
                                #region
                                DataTable cData = new DataTable();
                                cData = dal.selectMenuByPageType(condition);
                                ddl.DataSource = cData;
                                ddl.DataTextField = "m_titleEn";
                                ddl.DataValueField = "m_id";
                                ddl.DataBind();

                                ListItem lstt = new ListItem("---Select " + item.label + " --- ", "");
                                ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                                if (item.isRequired == true)
                                {
                                    ddl.Attributes.Add("required", "required");
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                #endregion
                            }
                            else
                            {
                                arr = item.content.Split('^');

                                if (arr.Count() > 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                    condition = arr[2];
                                }
                                else if (arr.Count() == 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                }
                                #region
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
                                var pData = dalbp.SelectByName(formname);
                                if (pData != null)
                                {
                                    List<FrontEndModel> cData = new List<FrontEndModel>();
                                    if (arr.Count() > 2)
                                    {
                                        cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                    }
                                    else
                                    {
                                        cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                    }
                                    ddl.DataSource = cData;
                                    ddl.DataTextField = "Data";
                                    ddl.DataValueField = "f_id";
                                    ddl.DataBind();

                                    ListItem lstt = new ListItem("---Select " + item.label + " --- ", "");
                                    ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                                    if (item.isRequired == true)
                                    {
                                        ddl.Attributes.Add("required", "required");
                                        lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                    }
                                }
                                
                                #endregion
                            }
                            ddl.SelectedIndex = 0;

                            this.div1.Controls.Add(ddl);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));


                            #endregion
                        }
                        else if (item.inputtype == 12)
                        {
                            #region DatePicker

                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            TextBox txt = new TextBox();

                            if (item.isRequired == true)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'><div class='input-append date' required=''  id='datepicker" + datepicker_count.ToString() + "' >"));
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            else
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'><div class='input-append date' id='datepicker" + datepicker_count.ToString() + "' >"));
                            }

                            txt.ID = "datepicker";// "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("class", "form-control_date");
                            txt.Attributes.Add("data-format", "dd/MM/yyyy");
                            txt.Attributes.Add("placeholder", item.label);
                            //<input id = "reservationtime" class="form-control pull-right" type="text" name="n_test"/>

                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("<span class='add-on' style='height:39px;'><i class='icon-calendar' style='padding-top:5px;'></i></span>"));
                            this.div1.Controls.Add(new LiteralControl("</div></div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 13)
                        {
                            #region Upload
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            string fupname = "upload" + textid.ToString();// means id
                            this.div1.Controls.Add(new LiteralControl("<input type='file' id='" + fupname + "' name='" + fupname + "'/>"));
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 14)
                        {
                            #region HiddenField For Int
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            //lbl.Attributes.Add("style", "display:none;");
                            //lbl.Attributes.Add("class", "col-sm-2 control-label");
                            lbl.Visible = false;
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div>"));
                            TextBox txt = new TextBox();
                            txt.ID = "txt" + textid;
                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            //txt.Attributes.Add("style", "display:none;");
                            txt.Visible = false;
                            //if (item.isRequired == true)
                            //{
                            //    txt.Attributes.Add("required", "");
                            //}
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 15)
                        {
                            #region HiddenField
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            //lbl.Attributes.Add("style", "display:none;");
                            //lbl.Visible = false;
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();
                            txt.ID = "txt" + textid;
                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            //txt.Attributes.Add("style", "display:none;");
                            txt.Visible = true;
                            //if (item.isRequired == true)
                            //{
                            //    txt.Attributes.Add("required", "");
                            //}
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        
                        datepicker_count++;
                    }
                }
                #endregion
            }
            else if (Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
            {
                #region Edit
                if (lstchild.Count > 0)
                {
                    int datepicker_count = 1;

                    foreach (var item in lstchild)
                    {
                        if (item.inputtype == 1)
                        {
                            #region TextBox
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Text = item.fcontent;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 2)// Password
                        {
                            #region TextBox Password
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'>"));
                            TextBox txt = new TextBox();
                            txt.TextMode = TextBoxMode.Password;
                            txt.ID = "txt" + textid;

                            txt.Text = item.fcontent;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");

                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 3)// email
                        {
                            #region TextBox Password
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            txt.TextMode = TextBoxMode.Email;
                            txt.Text = item.fcontent;
                            this.div1.Controls.Add(txt);

                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;

                            }
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 4)
                        {
                            #region TextArea
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();

                            txt.ID = "txt" + textid;

                            txt.Attributes.Add("runat", "server");
                            txt.TextMode = TextBoxMode.MultiLine;
                            txt.Text = item.fcontent;
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 5)
                        {
                            #region Telerik Editor
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            //RadEditor editor = new RadEditor();
                            //editor.ID = "editor" + textid;
                            //editor.EditModes = EditModes.Design;
                            //editor.Attributes.Add("runat", "server");
                            //editor.Skin = "Metro";
                            //editor.Content = item.fcontent;
                            ////editor.Content = System.Web.HttpUtility.HtmlEncode(item.fcontent); 
                            //this.div1.Controls.Add(editor);
                            TextBox ckeditor = new TextBox();
                            ckeditor.ID = "cke" + textid;
                            ckeditor.Attributes.Add("class", "ckeditor");
                            ckeditor.TextMode = TextBoxMode.MultiLine;
                            ckeditor.Text = item.fcontent;
                            this.div1.Controls.Add(ckeditor);

                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion                            
                        }
                        else if (item.inputtype == 6)
                        {
                            #region RadioButton
                            string rid = item.id;
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;
                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                this.div1.Controls.Add(lbl);

                                string selectText = item.fcontent;//***

                                int first_rbt = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    first_rbt++;
                                    var name = rad.ToString();
                                    //rid++;
                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    RadioButton childrd = new RadioButton();
                                    childrd.ID = item.inputtype.ToString();
                                    childrd.Attributes.Add("Value", rad.ToString());

                                    if (rad.ToString() == selectText)//***
                                    {
                                        childrd.Checked = true;
                                    }

                                    childrd.GroupName = "RbtGroup";

                                    this.div1.Controls.Add(childrd);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span>" + rad.ToString() + "</label>"));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                }
                                if (item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 7)
                        {
                            #region RadioButton (Dynamic Data)
                            string rid = item.id;
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
                                string formname = string.Empty, colname = string.Empty, condition = string.Empty; condition = string.Empty;
                                string[] arr = new string[0];
                                if (item.content.Contains(':'))
                                {
                                    string[] ck = item.content.Split(':');
                                    arr = ck[1].Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region
                                    DataTable cData = dal.selectMenuByPageType(condition);
                                    this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                    Label lbl = new Label();
                                    lbl.Text = item.label;
                                    lbl.Attributes.Add("class", "col-sm-2 control-label");
                                    this.div1.Controls.Add(lbl);
                                    string selectText = item.fcontent;//***
                                    int first_rbt = 0;
                                    this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                    foreach (DataRow rad in cData.Rows)
                                    {
                                        first_rbt++;
                                        var name = rad.ToString();
                                        //rid++;
                                        this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                        RadioButton childrd = new RadioButton();
                                        childrd.ID = item.inputtype.ToString();
                                        childrd.Attributes.Add("Value", rad["m_id"].ToString());

                                        if (rad["m_id"].ToString() == selectText)//***
                                        {
                                            childrd.Checked = true;
                                        }
                                        childrd.GroupName = "RbtGroup";

                                        this.div1.Controls.Add(childrd);
                                        this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + rad["m_titleEn"].ToString() + "</label>"));
                                        this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    }
                                    if (item.isRequired == true)
                                    {
                                        lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                    }
                                    this.div1.Controls.Add(new LiteralControl("</div></div>"));
                                    #endregion
                                }
                                else
                                {
                                    arr = item.content.Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region
                                    var pData = dalbp.SelectByName(formname);

                                    if (pData != null)
                                    {
                                        List<FrontEndModel> cData = new List<FrontEndModel>();
                                        if (arr.Count() > 2)
                                        {
                                            cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                        }
                                        else
                                        {
                                            cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                        }
                                        this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                        Label lbl = new Label();
                                        lbl.Text = item.label;
                                        lbl.Attributes.Add("class", "col-sm-2 control-label");
                                        this.div1.Controls.Add(lbl);
                                        string selectText = item.fcontent;//***
                                        int first_rbt = 0;
                                        this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                        foreach (var rad in cData)
                                        {
                                            first_rbt++;
                                            var name = rad.ToString();
                                            //rid++;
                                            this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                            RadioButton childrd = new RadioButton();
                                            childrd.ID = item.inputtype.ToString();
                                            childrd.Attributes.Add("Value", rad.f_id.ToString());

                                            if (rad.f_id.ToString() == selectText)//***
                                            {
                                                childrd.Checked = true;
                                            }
                                            childrd.GroupName = "RbtGroup";

                                            this.div1.Controls.Add(childrd);
                                            this.div1.Controls.Add(new LiteralControl("<span class='check'></span>&nbsp;" + rad.Data.ToString() + "</label>"));
                                            this.div1.Controls.Add(new LiteralControl("<br/>"));
                                        }
                                        if (item.isRequired == true)
                                        {
                                            lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                        }
                                        this.div1.Controls.Add(new LiteralControl("</div></div>"));
                                    }
                                    #endregion
                                }
                                
                            }
                            #endregion
                        }
                        else if (item.inputtype == 8)
                        {
                            #region CheckBox
                            string rid = item.id;
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;
                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                this.div1.Controls.Add(lbl);
                                int count = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    var name = rad.ToString();

                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    CheckBox chkIndividual = new CheckBox();

                                    chkIndividual.ID = "chk" + rid.ToString() + "$" + count.ToString();

                                    if ((item.fcontent.Split(',')).Contains(name))//***
                                    {
                                        chkIndividual.Checked = true;
                                    }
                                    //chkIndividual.ID = "chk" + rid.ToString() + "$" + count.ToString();//+"_"+rad.ToString();
                                    this.div1.Controls.Add(chkIndividual);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span>" + rad.ToString() + "</label> "));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    count++;
                                }
                                if (item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 9)
                        {
                            #region CheckBox (Dynamic)
                            string rid = item.id;
                            string[] str = new string[0];
                            string fcontent = "";
                            int c = 0;
                            if (item.content != null && item.content != "")
                            {
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
                                string formname = string.Empty, colname = string.Empty, condition = string.Empty; condition = string.Empty;
                                string[] arr = new string[0];
                                if (item.content.Contains(':'))
                                {
                                    string[] ck = item.content.Split(':');
                                    arr = ck[1].Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region
                                    DataTable cData = dal.selectMenuByPageType(condition);
                                    foreach (DataRow i in cData.Rows)
                                    {
                                        if (c != cData.Rows.Count)
                                        {
                                            fcontent += i["m_titleEn"].ToString() + ",";
                                        }
                                        else
                                        {
                                            fcontent += i["m_titleEn"].ToString();
                                        }
                                        c++;
                                    }
                                
                                str = fcontent.Split(',');
                                #endregion
                            }
                                else
                                {
                                    arr = item.content.Split('^');

                                    if (arr.Count() > 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                        condition = arr[2];
                                    }
                                    else if (arr.Count() == 2)
                                    {
                                        formname = arr[0];
                                        colname = arr[1];
                                    }
                                    #region
                                    var pData = dalbp.SelectByName(formname);
                                    if (pData != null)
                                    {
                                        List<FrontEndModel> cData = new List<FrontEndModel>();
                                        if (arr.Count() > 2)
                                        {
                                            cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                        }
                                        else
                                        {
                                            cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                        }
                                        foreach (var i in cData)
                                        {
                                            if (c != cData.Count-1)
                                            {
                                                fcontent += i.Data + "^";
                                            }
                                            else
                                            {
                                                fcontent += i.Data;
                                            }
                                            c++;
                                        }
                                    }
                                    str = fcontent.Split('^');
                                    #endregion
                                }

 
                            }
                            if (str != null && str.Length != 0)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                                Label lbl = new Label();
                                lbl.Text = item.label;
                                lbl.Attributes.Add("class", "col-sm-2 control-label");
                                this.div1.Controls.Add(lbl);

                                int count = 0;
                                this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                                foreach (var rad in str)
                                {
                                    var name = rad.ToString();

                                    this.div1.Controls.Add(new LiteralControl("<label class='inline-block'>"));
                                    CheckBox chkIndividual = new CheckBox();
                                    chkIndividual.ID = "chk" + rid.ToString() + "$" + count.ToString();
                                    var nid = dalf.selectIDbyContent(name);
                                    if ((item.fcontent.Split(',')).Contains(nid.ToString()))//***
                                    {
                                        chkIndividual.Checked = true;
                                    }
                                    this.div1.Controls.Add(chkIndividual);
                                    this.div1.Controls.Add(new LiteralControl("<span class='check'></span>" + rad.ToString() + "</label> "));
                                    this.div1.Controls.Add(new LiteralControl("<br/>"));
                                    count++;
                                }
                                if (item.isRequired == true)
                                {
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }
                                this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            }
                            #endregion
                        }
                        else if (item.inputtype == 10)
                        {
                            #region DropDownList
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            DropDownList ddl = new DropDownList();

                            ddl.ID = "ddl" + textid;

                            ddl.Attributes.Add("runat", "server");
                            ddl.Attributes.Add("class", "form-control");
                            string[] str = new string[0];
                            if (item.content != null && item.content != "")
                            {
                                str = item.content.Split(',');
                            }
                            if (str != null && str.Length != 0)
                            {
                                int index = 0;
                                foreach (var ddlitem in str)
                                {
                                    ListItem lstitem = new ListItem();

                                    if (item.isRequired == true)
                                    {
                                        //if (index == 0)
                                        //{
                                        //    lstitem = new ListItem(ddlitem.ToString(), "");
                                        //}
                                        //else
                                        //{
                                            lstitem = new ListItem(ddlitem.ToString(), ddlitem.ToString());
                                        //}
                                    }
                                    else
                                    {
                                        lstitem = new ListItem(ddlitem.ToString(), ddlitem.ToString());
                                    }
                                    ddl.Items.Insert(index, lstitem);
                                    index++;
                                }
                            }
                            if (item.fcontent != null && item.fcontent != "")
                            {
                                //***add on 14-3-2018
                                ListItem listItem = ddl.Items.FindByValue(item.fcontent);
                                if (listItem != null)
                                {
                                    ddl.ClearSelection();//14-3-2018
                                    listItem.Selected = true;
                                }
                                //***add on 14-3-2018
                                //ddl.Items.FindByValue(item.fcontent).Selected = true;///***comment on 14-3-2018
                            }
                            else
                            {
                                ddl.SelectedIndex = 0;
                            }
                            ListItem lstt = new ListItem("--- Please Select One --- ", "");
                            ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(ddl);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }                        
                        else if (item.inputtype == 11)
                        {
                            #region DropDownList(Dynamic)

                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            DropDownList ddl = new DropDownList();
                            ddl.ID = "ddl" + textid;
                            ddl.Attributes.Add("runat", "server");
                            ddl.Attributes.Add("class", "form-control");
                            
                            string formname = string.Empty, colname = string.Empty, condition = string.Empty;

                            string[] arr = new string[0];
                            if (item.content.Contains(':'))
                            {
                                string[] ck = item.content.Split(':');
                                arr = ck[1].Split('^');

                                if (arr.Count() > 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                    condition = arr[2];
                                }
                                else if (arr.Count() == 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                }
                                #region 
                                DataTable cData = new DataTable();
                                cData = dal.selectMenuByPageType(condition);
                                ddl.DataSource = cData;
                                ddl.DataTextField = "m_titleEn";
                                ddl.DataValueField = "m_id";
                                ddl.DataBind();

                                ListItem lstt = new ListItem("---Select " + item.label + " --- ", "");
                                ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                                if (item.isRequired == true)
                                {
                                    ddl.Attributes.Add("required", "required");
                                    lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                }

                                //***add on 14-3-2018
                                string selectedvalue = item.f_content == null ? "" : item.f_content;
                                ListItem listItem = ddl.Items.FindByValue(selectedvalue);
                                if (listItem != null)
                                {
                                    ddl.ClearSelection();//14-3-2018
                                    listItem.Selected = true;
                                }
                                //***add on 14-3-2018
                                ////13-3-2018
                                //ddl.SelectedValue = item.f_content == null ? "" : item.f_content;//***comment on 14-3-2018
                                #endregion
                            }
                            else
                            {
                                arr = item.content.Split('^');

                                if (arr.Count() > 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                    condition = arr[2];
                                }
                                else if (arr.Count() == 2)
                                {
                                    formname = arr[0];
                                    colname = arr[1];
                                }
                                #region
                                List<BackEnd_Parent> lst = new List<BackEnd_Parent>();

                                var pData = dalbp.SelectByName(formname);
                                if (pData != null)
                                {
                                    List<FrontEndModel> cData = new List<FrontEndModel>();
                                    if (arr.Count() > 2)
                                    {
                                        cData = dalf.SelectByPIdforDataBindCondition(colname, pData.p_id, condition,lang);
                                    }
                                    else
                                    {
                                        cData = dalf.SelectByPIdforDataBind(colname, pData.p_id,lang);
                                    }
                                    ddl.DataSource = cData;
                                    ddl.DataTextField = "Data";
                                    ddl.DataValueField = "f_id";
                                    ddl.DataBind();

                                    ListItem lstt = new ListItem("---Select " + formname + " --- ", "");
                                    ddl.Items.Insert(ddl.Items.Count - ddl.Items.Count, lstt);
                                    if (item.isRequired == true)
                                    {
                                        ddl.Attributes.Add("required", "required");
                                        lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                                    }
                                    //13-3-2018
                                    if (!string.IsNullOrEmpty(item.content))
                                    {
                                        //***add on 14-3-2018
                                        string selectedvalue = item.f_content == null ? "" : item.f_content;
                                        ListItem listItem = ddl.Items.FindByValue(selectedvalue);
                                        if (listItem != null)
                                        {
                                            ddl.ClearSelection();//14-3-2018
                                            listItem.Selected = true;
                                        }
                                        //***add on 14-3-2018
                                        //ddl.SelectedValue = item.f_content == null ? "" : item.f_content;//***comment on 14-3-2018
                                    }

                                }
                                #endregion
                            }
                           
                            if (item.fcontent != null && item.fcontent != "")
                            {  
                                if(string.IsNullOrEmpty(ddl.SelectedValue))  //13-3-2018
                                {
                                    //***add on 14-3-2018
                                    ListItem listItem = ddl.Items.FindByValue(item.fcontent);
                                    if (listItem != null)
                                    {
                                        ddl.ClearSelection();//14-3-2018
                                        listItem.Selected = true;
                                    }
                                    //***add on 14-3-2018
                                    //ddl.Items.FindByValue(item.fcontent).Selected = true;//***comment on 14-3-2018
                                }
                            }
                            else
                            {
                                ddl.SelectedIndex = 0;
                            }
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(ddl);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }                       
                        else if (item.inputtype == 12)
                        {
                            #region DatePicker

                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            TextBox txt = new TextBox();

                            if (item.isRequired == true)
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'><div class='input-append date' required=''  id='datepicker" + datepicker_count.ToString() + "' >"));
                                txt.Attributes.Add("required", "");
                            }
                            else
                            {
                                this.div1.Controls.Add(new LiteralControl("<div class='col-sm-6'><div class='input-append date' id='datepicker" + datepicker_count.ToString() + "' >"));
                            }

                            txt.ID = "datepicker";// "txt" + textid;

                            txt.Text = item.fcontent;//***

                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("class", "form-control_date");
                            txt.Attributes.Add("data-format", "dd/MM/yyyy");
                            txt.Attributes.Add("placeholder", item.label);
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("<span class='add-on' style='height:39px;'><i class='icon-calendar' style='padding-top:5px;'></i></span>"));
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 13)
                        {
                            #region Upload
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));

                            string fupname = "upload" + textid.ToString();// means id
                            this.div1.Controls.Add(new LiteralControl("<input type='file' id='" + fupname + "' name='" + fupname + "'/>"));

                            
                            if (item.fcontent.Contains("jbg") || item.fcontent.Contains("png") || item.fcontent.Contains("jpeg"))
                            {
                                System.Web.UI.WebControls.Image myImage = new System.Web.UI.WebControls.Image();
                                myImage.Width = 70;
                                myImage.Height = 70;
                                myImage.ImageUrl = "~/Images/FrontEnd_Photos/" + item.fcontent;
                                this.div1.Controls.Add(myImage);
                            }
                            else
                            {
                                Label lbl2 = new Label();
                                lbl2.Text = item.fcontent;
                                this.div1.Controls.Add(lbl2);
                            }
                            if (item.isRequired == true)
                            {
                                lbl.Text = "<font style='color:red'>*</font>&nbsp;" + item.label;
                            }
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 14)
                        {
                            #region HiddenField For Int
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Visible = false;
                            //lbl.Attributes.Add("class", "col-sm-2 control-label");
                            //lbl.Attributes.Add("style", "display:none;");
                            this.div1.Controls.Add(lbl);

                            this.div1.Controls.Add(new LiteralControl(" <div>"));
                            TextBox txt = new TextBox();
                            txt.ID = "txt" + textid;
                            txt.Text = item.fcontent;
                            txt.Visible = false;
                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("style", "display:none;");
                            if (item.isRequired == true)
                            {
                                txt.Attributes.Add("required", "");
                            }
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        else if (item.inputtype == 15)
                        {
                            #region HiddenField
                            var textid = item.id.ToString();
                            this.div1.Controls.Add(new LiteralControl("<div class='form-group'>"));
                            Label lbl = new Label();
                            lbl.Text = item.label;
                            lbl.Attributes.Add("class", "col-sm-2 control-label");
                            //lbl.Attributes.Add("style", "display:none;");
                            //lbl.Visible = false;
                            this.div1.Controls.Add(lbl);


                            this.div1.Controls.Add(new LiteralControl(" <div class='col-sm-6'>"));
                            TextBox txt = new TextBox();
                            txt.ID = "txt" + textid;
                            txt.Text = item.fcontent;
                            txt.Attributes.Add("runat", "server");
                            txt.Attributes.Add("placeholder", "Please insert " + item.label);
                            txt.Attributes.Add("class", "form-control");
                            //txt.Attributes.Add("style", "display:none;");
                            txt.Visible = true;
                            //if (item.isRequired == true)
                            //{
                            //    txt.Attributes.Add("required", "");
                            //}
                            this.div1.Controls.Add(txt);
                            this.div1.Controls.Add(new LiteralControl("</div></div>"));
                            #endregion
                        }
                        datepicker_count++;

                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Bind Controls(Page Load)
    private void BindControls(string attach,int lang)
    {
        string pid = string.Empty;
        string rowid = string.Empty;
        if (Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
        {
            #region Edit
            pid = Request.QueryString["id"].ToString();
            rowid = Request.QueryString["rid"].ToString();

            if (pid != null)//lst
            {
                var lstchl = dalbc.SelectByPId(pid);

                FrontEnd f_end = new FrontEnd { f_pid = pid, f_c_rowid = rowid ,f_lang=lang};
                var lstfchl = dalf.SelectByPIDRowID(f_end);

                List<CombineFrontBack> lstchild = new List<CombineFrontBack>();
                if (lstchl.Count > 0 && lstfchl.Count > 0)
                {
                    int count = 1;
                    foreach (var item1 in lstchl)
                    {
                        var lstContent = dalf.SelectByCount(lstfchl, count);
                        if (lstContent != null)
                        {
                            CombineFrontBack tmp = new CombineFrontBack()
                            {
                                id = item1.c_id,
                                pid = item1.c_pid,
                                inputtype = item1.c_inputtype,
                                label = item1.c_label,
                                content = item1.c_content,
                                fcontent = lstContent.f_content,
                                sortorder = item1.c_sortorder,
                                deleteflag = item1.c_deleteflag,
                                isRequired = item1.c_isRequired
                            };
                            count++;
                            lstchild.Add(tmp);
                        }
                    }

                    OnlyBindControls(lstchild, attach, pid, rowid);
                }
            }
            #endregion
        }
        else if (Request.QueryString["id"] != null && Request.QueryString["rid"] == null)
        {
            #region New
            pid = Request.QueryString["id"].ToString();

            var lst = dalbc.SelectByPId(pid);

            if (lst.Count > 0)
            {
                var lstchild = (from l in lst
                                select new CombineFrontBack
                                {
                                    id = l.c_id,
                                    pid = l.c_pid,
                                    inputtype = l.c_inputtype,
                                    label = l.c_label,
                                    content = l.c_content,
                                    //fcontent = lstContent.f_content,
                                    sortorder = l.c_sortorder,
                                    deleteflag = l.c_deleteflag,
                                    isRequired = l.c_isRequired
                                }).ToList();
                OnlyBindControls(lstchild, attach, pid, "0");
            }
            #endregion
        }
    }
    #endregion

    #region btnAdd_Click
    public void add(object sender, EventArgs e)//protected
    {
        lang = Convert.ToInt32(Request.QueryString["lang"].ToString());
        int SaveNoSuccess = 0;
        string Checked = "False";
        string pid = string.Empty;
        bool DateSuccess = false;

        try
        {
            if (Request.QueryString["id"] != null && Request.QueryString["rid"] == null)
            {
                #region SetUp
                lblMessage.Text = "";

                pid = Request.QueryString["id"];

                var lstparent = dalbp.SelectById(pid);
                if (lstparent != null)
                {
                    var lstchild = dalbc.SelectByPId(pid);

                    if (lstchild.Count > 0)
                    {
                        int entry_count = 1;
                        if (ViewState["entry_count"] != null)
                        {
                            entry_count = Convert.ToInt32(ViewState["entry_count"].ToString());
                        }

                        string rkey = string.Empty; string rowid = string.Empty;
                        rkey = gk.getKey("RID");
                        rowid = "RID" + rkey;
                        
                        SaveNoSuccess = SaveControls(lstchild, null, entry_count, pid, rowid, "",lang);
                        if (SaveNoSuccess == 1)
                        {                            
                            SaveControlsNoSuccess();
                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&s=fls&lang="+lang);
                        }
                        else
                        {
                            gk.SaveKey("RID", int.Parse(rkey));
                        }
                    }
                }
                #endregion
            }
            else if (Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
            {
                #region Edit
                lblMessage.Text = "";
                string rid = Request.QueryString["rid"].ToString();

                pid = Request.QueryString["id"].ToString();

                string img_str = "";
                var lst = dalf.SelectForUpload(pid, rid);
                int indiv_count = 1;
                if (lst.Count > 0)
                {
                    foreach (var data_item in lst)
                    {
                        if (indiv_count == lst.Count())
                        {
                            img_str += data_item.f_label + "_" + data_item.f_content;
                        }
                        else
                        {
                            img_str += data_item.f_label + "_" + data_item.f_content + ",";
                        }
                    }
                }


                var lstparent = dalbp.SelectById(pid);
                if (lstparent != null)
                {
                    var lstchild = dalbc.SelectByPId(pid);

                    if (lstchild.Count > 0)
                    {
                        int entry_count = 1;
                        if (ViewState["entry_count"] != null)
                        {
                            entry_count = Convert.ToInt32(ViewState["entry_count"].ToString());
                        }

                        string rowid = Request.QueryString["rid"].ToString();
                        var lstfchild = dalf.SelectByRowidPiD(rowid, pid);
                        //bool Success = DeleteByRowID(pid, rid);
                        //if (Success == true)
                        //{

                        //}
                        SaveNoSuccess = UpdateControls(lstchild, lstfchild, entry_count, pid, rowid, img_str,lang);
                        if (SaveNoSuccess == 1)
                        {
                            SaveControlsNoSuccess();
                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=fls&lang="+lang);
                        }
                        #region Close
                        //var lstDateCheck = dalbc.SelectForDateCheck(lstchild);

                        //var lstCheckBox = dalbc.SelectForCheckBoxCheck(lstchild);

                        //if (lstDateCheck.Count > 0)
                        //{
                        //    DateSuccess = CheckDateTime(lstDateCheck);
                        //    if (DateSuccess == false)
                        //    {
                        //        string uncheck_name = Checked.Split('_').LastOrDefault();
                        //        var lastcoma = uncheck_name.Length - 1;
                        //        lblMessage.Text = "*Please select date";
                        //        lblMessage.ForeColor = Color.Red;
                        //    }
                        //    else
                        //    {
                        //        #region CheckBox Required
                        //        if (lstCheckBox.Count > 0)
                        //        {
                        //            Checked = Check_CheckBoxRequired();
                        //            if (Checked.Contains("True"))
                        //            {
                        //                bool Success = DeleteByRowID(pid, rid);
                        //                if (Success == true)
                        //                {
                        //                    SaveNoSuccess = SaveControls(lstchild, null, entry_count, pid, rowid, img_str);
                        //                    if (SaveNoSuccess == 1)
                        //                    {
                        //                        SaveControlsNoSuccess();
                        //                        Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=fls");
                        //                    }
                        //                }
                        //            }
                        //            else
                        //            {
                        //                string uncheck_name = Checked.Split('_').LastOrDefault();
                        //                var lastcoma = uncheck_name.Length - 1;
                        //                lblMessage.Text = "*Please select at least one " + uncheck_name.Remove(lastcoma) + " Check Items.";
                        //                lblMessage.ForeColor = Color.Red;
                        //            }
                        //        }
                        //        #endregion

                        //        #region No CheckBox Required
                        //        else
                        //        {
                        //            bool Success = DeleteByRowID(pid, rid);
                        //            if (Success == true)
                        //            {
                        //                SaveNoSuccess = SaveControls(lstchild, null, entry_count, pid, rowid, img_str);
                        //                if (SaveNoSuccess == 1)
                        //                {
                        //                    SaveControlsNoSuccess();
                        //                    Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=fls");
                        //                }
                        //            }
                        //        }
                        //        #endregion
                        //    }
                        //}
                        //else
                        //{
                        //    #region CheckBox Required
                        //    if (lstCheckBox.Count > 0)
                        //    {
                        //        Checked = Check_CheckBoxRequired();
                        //        if (Checked.Contains("True"))
                        //        {
                        //            bool Success = DeleteByRowID(pid, rid);
                        //            if (Success == true)
                        //            {
                        //                SaveNoSuccess = SaveControls(lstchild, null, entry_count, pid, rowid, img_str);
                        //                if (SaveNoSuccess == 1)
                        //                {
                        //                    SaveControlsNoSuccess();
                        //                    Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=fls");
                        //                }
                        //            }

                        //        }
                        //        else
                        //        {
                        //            string uncheck_name = Checked.Split('_').LastOrDefault();
                        //            var lastcoma = uncheck_name.Length - 1;
                        //            lblMessage.Text = "*Please select at least one " + uncheck_name.Remove(lastcoma) + " Check Items.";
                        //            lblMessage.ForeColor = Color.Red;
                        //        }
                        //    }
                        //    #endregion

                        //    #region No CheckBox Required
                        //    else
                        //    {
                        //        bool Success = DeleteByRowID(pid, rid);
                        //        if (Success == true)
                        //        {
                        //            SaveNoSuccess = SaveControls(lstchild, null, entry_count, pid, rowid, img_str);
                        //            if (SaveNoSuccess == 1)
                        //            {
                        //                SaveControlsNoSuccess();
                        //                Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=fls");
                        //            }
                        //        }
                        //    }
                        //    #endregion
                    }
                        #endregion

                    }
                }
                #endregion
            }
        
        catch (Exception ex)
        {
        }
        //BindControls("AddButton");
    }
    #endregion

    #region SaveControls(btnAdd_Click) & UpdateControls
    private int SaveControls(List<BackEnd_Child> lstchild, List<FrontEnd> lstfchild, int entry_count, string pid, string rowid, string img_str, int lang)
    {
        int NoSuccess = 0;
        try
        {
            foreach (var item in lstchild)
            {
                if (item.c_inputtype == 1 || item.c_inputtype == 2 || item.c_inputtype == 3)
                {
                    #region TextBox
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                    }
                    int isSuccess = 0;
                    string fkey = string.Empty; string fid = string.Empty;
                    fkey = gk.getKey("FE");
                    fid = "FE" + fkey;
                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                    isSuccess = dalf.Save(fe);
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;                        
                    }
                    else
                    {
                        gk.SaveKey("FE", int.Parse(fkey));
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 4)
                {
                    #region TextArea
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                    }
                    string fkey = string.Empty; string fid = string.Empty;
                    fkey = gk.getKey("FE");
                    fid = "FE" + fkey;
                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                    isSuccess = dalf.Save(fe);

                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        gk.SaveKey("FE", int.Parse(fkey));
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 5)
                {
                    #region TelerikEditor
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$cke" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    if (isSuccess == 0)
                    {
                        var lstCheckCKEditor = dalbc.SelectForCKEditorCheck(lstchild);
                        if (lstCheckCKEditor.Count > 0)
                        {
                            NoSuccess = 1;
                        }
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 6)
                {
                    #region RadioButton
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$RbtGroup";
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        int isSuccess = 0;
                        int type = Convert.ToInt32(item.c_inputtype);
                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 7)
                {
                    #region RadioButton (Dynamic Data)
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$RbtGroup";
                    string tt = "";
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        int isSuccess = 0;
                        int type = Convert.ToInt32(item.c_inputtype);
                        string fkey = string.Empty; string fid = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 8)
                {
                    #region CheckBox
                    string rid = item.c_id;

                    string[] str = new string[0];
                    if (item.c_content != null && item.c_content != "")
                    {
                        str = item.c_content.Split(',');
                    }
                    if (str != null && str.Length != 0)
                    {
                        string labelText = item.c_label;

                        int isSuccess = 0;

                        int itemid = 0;
                        var lastid = str.Last();
                        int count = str.Count();
                        string selecteditems = "";
                        foreach (var rad in str)
                        {
                            var name = rad.ToString();

                            var textid = "ctl00$MainContent$chk" + rid + "$" + itemid;
                            string tt = "";
                            if (Request.Form[textid] != null && Request.Form[textid].ToString() == "on")
                            {
                                tt = name;
                                selecteditems += tt + ",";
                            }
                            if (name == lastid)
                            {
                                if (selecteditems != "")
                                {
                                    var lastcoma = selecteditems.Length - 1;
                                    selecteditems = selecteditems.Remove(lastcoma);
                                }
                            }

                            itemid++;
                        }
                        string fkey = string.Empty; string fid = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;

                        FrontEnd fe = new FrontEnd() {f_id=fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = selecteditems, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);

                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            gk.SaveKey("FE", int.Parse(fkey));
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 9)
                {
                    #region CheckBox (Dynamic)
                    string rid = item.c_id;
                    var ccontent = dalbc.SelectById(rid);

                    var res = dalf.SelectByPIdforChkDataBind(ccontent.c_content);
                    string fcontent = "";
                    int i = 1;
                    foreach (var r in res)
                    {
                        if (i != res.Count())
                        {
                            fcontent += r.f_id + ",";
                        }
                        else
                        {
                            fcontent += r.f_id;
                        }
                        i++;
                    }
                    string[] str = new string[0];
                    if (fcontent != null && fcontent != "")
                    {
                        str = fcontent.Split(',');
                    }
                    if (str != null && str.Length != 0)
                    {
                        string labelText = item.c_label;

                        int isSuccess = 0;

                        int itemid = 0;
                        var lastid = str.Last();
                        int count = str.Count();
                        string selecteditems = "";
                        foreach (var rad in str)
                        {
                            var name = rad.ToString();

                            var textid = "ctl00$MainContent$chk" + rid + "$" + itemid;
                            string tt = "";
                            if (Request.Form[textid] != null && Request.Form[textid].ToString() == "on")
                            {
                                tt = name;
                                selecteditems += tt + ",";
                            }
                            if (name == lastid)
                            {
                                if (selecteditems != "")
                                {
                                    var lastcoma = selecteditems.Length - 1;
                                    selecteditems = selecteditems.Remove(lastcoma);
                                }
                            }

                            itemid++;
                        }
                        string fkey = string.Empty; string fid = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id=fid,f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = selecteditems, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);

                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            gk.SaveKey("FE", int.Parse(fkey));
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 10)
                {
                    #region DropDownList
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$ddl" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id=fid,f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }               
                else if (item.c_inputtype == 11)
                {
                    #region DropDownList(Dynamic)
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$ddl" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 12)
                {
                    #region DatePicker
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$datepicker";// + item.c_id; //"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));

                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 13)
                {
                    #region Upload
                    string labelText = item.c_label;
                    DateTime dt = DateTime.Now;
                    var textid = "upload" + item.c_id;
                    int isSuccess = 0;
                    string filename = "";
                    //string pname = dalbp.SelectNameByID(pid);

                    HttpPostedFile file = Request.Files[textid];
                    if (!string.IsNullOrEmpty(file.FileName))
                    {
                        string path = Server.MapPath("~/Images/FrontEnd_Photos/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string[] filesplit = (file.FileName).Split('.');

                        if (filesplit.Length > 0 && filesplit != null)
                        {
                            filename = filesplit[0] + "_" + DateTime.Now.Ticks + "." + filesplit[1];
                            filename = filename.Replace(" ", "");
                        }

                        file.SaveAs(path + filename);
                    }
                    string fkey = string.Empty; string fid = string.Empty;
                    fkey = gk.getKey("FE");
                    fid = "FE" + fkey;
                    FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = filename, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                    isSuccess = dalf.Save(fe);
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        gk.SaveKey("FE", int.Parse(fkey));
                        ViewState["front_childID"] += fid + ",";
                    }

                    //

                    #endregion
                }
                else if (item.c_inputtype == 14)
                {
                    #region HiddenField for Int
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != "")
                    {
                        tt = Request.Form[textid].ToString();
                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 15)
                {
                    #region HiddenField
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        string fkey = string.Empty;
                        fkey = gk.getKey("FE");
                        fid = "FE" + fkey;
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id, f_createddate = dalf.ConvertToTimestamp(DateTime.Now), f_deleteflag = false };
                        isSuccess = dalf.Save(fe);
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
            }

            #region timestamp  
            DateTime updtime = DateTime.Now;
            string updatetimestamp = dalf.ConvertToTimestamp(updtime);
            int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
            #endregion
        }

        catch (Exception ex)
        {
            throw ex;
        }

        return NoSuccess;

    }

    private int UpdateControls(List<BackEnd_Child> lstchild, List<FrontEnd> lstfchild, int entry_count, string pid, string rowid, string img_str,int lang)
    {
        int NoSuccess = 0;
        try
        {
            foreach (var item in lstchild)
            {
                if (item.c_inputtype == 1 || item.c_inputtype == 2 || item.c_inputtype == 3)
                {
                    #region TextBox
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$txt" + item.c_id;
                    string tt = "";
                    int isSuccess = 0;
                    string fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang==lang).f_id;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 4)
                {
                    #region TextArea
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang==lang).f_id;
                        tt = Request.Form[textid].ToString();
                        FrontEnd fe = new FrontEnd() {f_id=fid,f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }

                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 5)
                {
                    #region TelerikEditor
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$cke" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang==lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText, f_lang = lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        var lstCheckCKEditor = dalbc.SelectForCKEditorCheck(lstchild);
                        if (lstCheckCKEditor.Count > 0)
                        {
                            NoSuccess = 1;
                        }
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 6)
                {
                    #region RadioButton
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$RbtGroup";
                    string tt = "";
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        int isSuccess = 0;
                        int type = Convert.ToInt32(item.c_inputtype);
                        string fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd(){f_id=fid,f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                        if(pid== "BP10014" && item.c_id == "BC10064")
                        {
                            string sql= "update tb_Conference set c_liveQA='"+ tt + "' where ID='"+ rowid + "'";
                            Functionality fn = new Functionality();
                            fn.ExecuteSQL(sql);
                        }
                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 7)
                {
                    #region RadioButton (Dynamic Data)
                    string labelText = item.c_label;

                    var textid = "ctl00$MainContent$RbtGroup";
                    string tt = "";
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        int isSuccess = 0;
                        int type = Convert.ToInt32(item.c_inputtype);
                        string fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);

                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 8)
                {
                    #region CheckBox
                    string rid = item.c_id;

                    string[] str = new string[0];
                    if (item.c_content != null && item.c_content != "")
                    {
                        str = item.c_content.Split(',');
                    }
                    if (str != null && str.Length != 0)
                    {
                        string labelText = item.c_label;

                        int isSuccess = 0;

                        int itemid = 0;
                        var lastid = str.Last();
                        int count = str.Count();
                        string selecteditems = "";
                        foreach (var rad in str)
                        {
                            var name = rad.ToString();

                            var textid = "ctl00$MainContent$chk" + rid + "$" + itemid;
                            string tt = "";
                            if (Request.Form[textid] != null && Request.Form[textid].ToString() == "on")
                            {
                                tt = name;
                                selecteditems += tt + ",";
                            }
                            if (name == lastid)
                            {
                                if (selecteditems != "")
                                {
                                    var lastcoma = selecteditems.Length - 1;
                                    selecteditems = selecteditems.Remove(lastcoma);
                                }
                            }

                            itemid++;
                        }
                        string fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = selecteditems, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);

                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 9)
                {
                    #region CheckBox (Dynamic)
                    string rid = item.c_id;
                    var ccontent = dalbc.SelectById(rid);

                    var res = dalf.SelectByPIdforChkDataBind(ccontent.c_content);
                    string fcontent = "";
                    int i = 1;
                    foreach (var r in res)
                    {
                        if (i != res.Count())
                        {
                            fcontent += r.f_id + ",";
                        }
                        else
                        {
                            fcontent += r.f_id;
                        }
                        i++;
                    }
                    string[] str = new string[0];
                    if (fcontent != null && fcontent != "")
                    {
                        str = fcontent.Split(',');
                    }
                    if (str != null && str.Length != 0)
                    {
                        string labelText = item.c_label;

                        int isSuccess = 0;

                        int itemid = 0;
                        var lastid = str.Last();
                        int count = str.Count();
                        string selecteditems = "";
                        foreach (var rad in str)
                        {
                            var name = rad.ToString();

                            var textid = "ctl00$MainContent$chk" + rid + "$" + itemid;
                            string tt = "";
                            if (Request.Form[textid] != null && Request.Form[textid].ToString() == "on")
                            {
                                tt = name;
                                selecteditems += tt + ",";
                            }
                            if (name == lastid)
                            {
                                if (selecteditems != "")
                                {
                                    var lastcoma = selecteditems.Length - 1;
                                    selecteditems = selecteditems.Remove(lastcoma);
                                }
                            }

                            itemid++;
                        }
                        string fid = lstfchild.FirstOrDefault(ii => ii.f_pid == pid && ii.f_c_rowid == rowid && ii.f_c_id == item.c_id && ii.f_inputtype == item.c_inputtype && ii.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = selecteditems, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };

                        isSuccess = dalf.update(fe);

                        if (isSuccess == 0)
                        {
                            NoSuccess = 1;
                        }
                        else
                        {
                            ViewState["front_childID"] += fid + ",";
                        }
                    }
                    #endregion
                }
                else if (item.c_inputtype == 10)
                {
                    #region DropDownList
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$ddl" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 11)
                {
                    #region DropDownList(Dynamic)
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$ddl" + item.c_id;
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 12)
                {
                    #region DatePicker
                    string labelText = item.c_label;

                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$datepicker";// + item.c_id; //"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();
                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);

                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 13)
                {
                    #region Upload
                    string labelText = item.c_label;
                    DateTime dt = DateTime.Now;
                    var textid = "upload" + item.c_id;
                    int isSuccess = 0;
                    string filename = "";
                    //string pname = dalbp.SelectNameByID(pid);

                    string fid = string.Empty;
                    HttpPostedFile file = Request.Files[textid];
                    if (!string.IsNullOrEmpty(file.FileName))
                    {
                        string path = Server.MapPath("~/Images/FrontEnd_Photos/");
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string[] filesplit = (file.FileName).Split('.');

                        if (filesplit.Length > 0 && filesplit != null)
                        {
                            filename = filesplit[0] + "_" + DateTime.Now.Ticks + "." + filesplit[1];
                            filename = filename.Replace(" ", "");
                        }

                        file.SaveAs(path + filename);

                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = filename, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };

                        isSuccess = dalf.update(fe);
                    }

                    

                    if (isSuccess == 0 && !string.IsNullOrEmpty(file.FileName))
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        isSuccess = 1;
                    }
                    if (isSuccess == 1)
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    //

                    #endregion
                }
                else if (item.c_inputtype == 14)
                {
                    #region HiddenField for Int
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != "")
                    {
                        tt = Request.Form[textid].ToString();
                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }
                else if (item.c_inputtype == 15)
                {
                    #region HiddenField
                    string labelText = item.c_label;
                    int isSuccess = 0;
                    var textid = "ctl00$MainContent$txt" + item.c_id;//"ctl00$MainContent$" + "txt" + item.c_id;**Foundation
                    string tt = "";
                    string fid = string.Empty;
                    if (Request.Form[textid] != null)
                    {
                        tt = Request.Form[textid].ToString();

                        fid = lstfchild.FirstOrDefault(i => i.f_pid == pid && i.f_c_rowid == rowid && i.f_c_id == item.c_id && i.f_inputtype == item.c_inputtype && i.f_lang == lang).f_id;

                        FrontEnd fe = new FrontEnd() { f_id = fid, f_pid = pid, f_label = labelText,f_lang=lang, f_entrycount = entry_count, f_inputtype = item.c_inputtype, f_content = tt, f_c_isFinish = true, f_c_rowid = rowid, f_sortorder = item.c_sortorder, f_c_id = item.c_id };
                        isSuccess = dalf.update(fe);
                    }
                    if (isSuccess == 0)
                    {
                        NoSuccess = 1;
                    }
                    else
                    {
                        ViewState["front_childID"] += fid + ",";
                    }
                    #endregion
                }          
            }
            #region timestamp  
            DateTime updtime = DateTime.Now;
            string updatetimestamp = dalf.ConvertToTimestamp(updtime);
            int isSuc = dalf.updatetimestamp(pid, rowid, updatetimestamp);
            #endregion
        }

        catch (Exception ex)
        {
            throw ex;
        }

        return NoSuccess;
    }
    #endregion

    #region SaveData for btnAdd_Click
    private bool SavebtnAdd()
    {
        bool Successful = false;
        return Successful;
    }
    #endregion

    #region DateTime required
    private bool CheckDateTime(List<BackEnd_Child> lstChild)
    {
        bool Success = false;
        try
        {
            int blankCount = 0;
            foreach (var item in lstChild)
            {
                var textid = "ctl00$ContentPlaceHolder1$txt" + item.c_id;
                if (Request.Form[textid] == null || Request.Form[textid] == "")
                {
                    blankCount = 1;
                }
            }
            if (blankCount == 1)
            {
                Success = false;
            }
            else if (blankCount == 0)
            {
                Success = true;
            }
        }
        catch (Exception ex)
        {
        }
        return Success;
    }
    #endregion

    #region Check Checkbox required
    private string Check_CheckBoxRequired()//bool
    {
        bool IsChecked = false;
        string Name = "False";
        string pid = string.Empty;
        string uncheckName = "";

        try
        {
            if (Request.QueryString["id"] != null)
            {
                pid = Request.QueryString["id"].ToString();
                

                var lstchild = dalbc.SelectByPId(pid);

                if (lstchild.Count > 0)
                {
                    //foreach (var item in lstchild)
                    //{
                    var lstCheckBox = (from cc in lstchild
                                       where cc.c_inputtype == 4 && cc.c_isRequired == true
                                       select cc).ToList();
                    if (lstCheckBox.Count > 0)
                    {
                        int IsUncheck = 0;
                        foreach (var checkitem in lstCheckBox)
                        {
                            if (checkitem.c_inputtype == 4)
                            {
                                string rid = checkitem.c_id;
                                string[] str = new string[0];
                                if (checkitem.c_content != null && checkitem.c_content != "")
                                {
                                    str = checkitem.c_content.Split(',');
                                }
                                if (str != null && str.Length != 0)
                                {
                                    int count = 0;
                                    int isCount = 0;//*************
                                    foreach (var rad in str)
                                    {
                                        var name = rad.ToString();
                                        var textid = "MainContent_chk" + rid + "$" + count;
                                       
                                        if (Request.Form[textid] != null)
                                        {                                           
                                            isCount = 1;                                            
                                        }
                                        count++;
                                    }
                                    if (isCount == 1)
                                    {                                        
                                        IsChecked = true;
                                        Name = "True";
                                    }
                                    if (isCount == 0)
                                    {
                                        IsChecked = false;
                                        Name = "False";
                                        IsUncheck = 1;
                                    }
                                }
                            }
                            if (IsChecked == false)
                            {
                                uncheckName += checkitem.c_label + ",";
                            }
                        }
                        if (IsUncheck == 1)
                        {
                            IsChecked = false;
                            Name = "False";
                        }
                    }
                    //}
                }
            }
        }
        catch (Exception ex)
        {
        }
        return Name + "_" + uncheckName;
    }

    
    #endregion

    #region SaveControlsNoSuccess
    private void SaveControlsNoSuccess()
    {
        if (ViewState["front_childID"] != null)
        {
            var lastcoma = (ViewState["front_childID"].ToString()).Length - 1;
            string selecteditems = (ViewState["front_childID"].ToString()).Remove(lastcoma);
            string[] strchlIDs = (selecteditems).Split(',');
            if (strchlIDs.Length > 0 && strchlIDs != null)
            {
                foreach (var IDitem in strchlIDs)
                {
                    int success = 0;
                    string itemid = string.Empty;
                    itemid = IDitem.ToString();
                    success = dalf.Delete(itemid);
                }
            }
        }
    }
    #endregion

    #region Button Click(btnCreate_OnClick)
    protected void btnCreate_OnClick(object sender, EventArgs e)
    {
        string pid = string.Empty;
        try
        {
            if (Request.QueryString["id"] != null)
            {
                pid = Request.QueryString["id"].ToString();
                
                if(Validation(pid))
                {
                    add(sender, e);
                    if (lblMessage.Text == "*Please select date" || lblMessage.Text.Contains("*Please select at least one"))
                    {
                        //BindControls("");
                    }
                    else
                    {
                        lblMessage.Text = "";
                        if (Request.QueryString["id"] != null && Request.QueryString["rid"] == null)
                        {
                            ViewState["front_childID"] = null;
                            ViewState["entry_count"] = null;

                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&s=suc&lang="+lang);
                        }

                        else if (Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
                        {
                            ViewState["front_childID"] = null;
                            ViewState["entry_count"] = null;

                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&u=suc&lang=" + lang);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region Validateion
    private bool Validation(string pid)
    {
        int SaveNoSuccess = 0;
        bool rtn = false;
        string Checked = "False";
        var lstparent = dalbp.SelectById(pid);
        if (lstparent != null)
        {
            var lstchild = dalbc.SelectByPId(pid);

            if (lstchild.Count > 0)
            {
                int entry_count = 1;
                if (ViewState["entry_count"] != null)
                {
                    entry_count = Convert.ToInt32(ViewState["entry_count"].ToString());
                }
                //int rowid = 1;
                //var lstRowID = dalf.SelectLastRowID();
                //if (lstRowID != null)
                //{
                //    rowid = lstRowID + 1;
                //}
                #region Add when required
                var lstCheckBox = dalbc.SelectForCheckBoxCheck(lstchild);
                #region CheckBox Required
                if (lstCheckBox.Count > 0)
                {
                    Checked = Check_CheckBoxRequired();
                    if (Checked.Contains("True"))
                    {
                        rtn = true;
                    }
                    else
                    {
                        rtn = false;
                    }
                }
                else
                {
                    rtn = true;
                }
                #endregion
                #endregion
            }
        }
        rtn = true;
        return rtn;
    }
    #endregion

    #region Button Click(btnDelete_OnClick)
    protected void btnDelete_OnClick(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
        {
            string pid = Request.QueryString["id"].ToString();
            string rowid = Request.QueryString["rid"].ToString();

            bool Success = DeleteByRowID(pid, rowid);
            if (Success == true)
            {
                Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&d=suc&lang=" + lang);
            }
            else
            {
                Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&d=fls&lang=" + lang);
            }
        }
    }
    #endregion

    #region DeleteByRowID(From Edit)
    private bool DeleteByRowID(string parentid, string rid)
    {
        bool Success = false;
        FrontEnd fe = new FrontEnd() { f_pid = parentid, f_c_rowid = rid };
        var lst = dalf.SelectByPIDRowID(fe);
        int count = 0;
        int totalcount = 0;
        if (lst.Count > 0)
        {
            totalcount = lst.Count();
            foreach (var item in lst)
            {
                int isSuccess = 0;
                string frontID = item.f_id;
                isSuccess = dalf.Delete(frontID);
                if (isSuccess > 0)
                {
                    count += 1;
                }
            }
        }

        if (count == totalcount)
        {
            Success = true;
        }

        return Success;
    }
    #endregion

    #region Button Click(btnbtnCancel_OnClick)
    protected void btnbtnCancel_OnClick(object sender, EventArgs e)
    {
        int pid = 0;

        try
        {
            if (Request.QueryString["id"] != null)
            {
                pid = Convert.ToInt32(Request.QueryString["id"].ToString());
                ViewState["front_childID"] = null;
                ViewState["entry_count"] = null;

                Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid +"&lang = "+lang);
            }
        }
        catch (Exception ex)
        {
        }


    }
    #endregion
    
}