﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using App.Common;
using App.DAL;
public partial class Admin_BackEndFormList : System.Web.UI.Page
{
    DAL_Child dalc = new DAL_Child();
    DAL_Parent dalp = new DAL_Parent();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["msg"] != null)
            {
                string msgtxt = Request.QueryString["msg"].ToString();
                msg.Text = "";
                MessageShow(msgtxt, msgtxt);
            }
        }
    }
    protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            List<BackEnd_Child> lstc = dalc.SelectAllData();            
            grdFML.DataSource = lstc;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    protected void grdFML_NeedDSource()
    {
        try
        {
            List<BackEnd_Child> lstc = dalc.SelectAllData();
            grdFML.DataSource = lstc;
            grdFML.DataBind();
            
        }
        catch (Exception ex) { }
    }
    protected void lnkEdit_Onclick(object sender, EventArgs e)
    {
        string id = ((LinkButton)sender).CommandArgument;
        if (!string.IsNullOrEmpty(id))
        {
            Response.Redirect("BackEndForm.aspx?id=" + id);
        }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        LinkButton del = sender as LinkButton;
        if (del.CommandName == "Delete")
        {
            string ID = del.CommandArgument.ToString();
            int chkFE = dalf.selectbyCID(ID);
            if(chkFE ==0)
            {
                int res = dalc.Delete(ID);
                if (res == 1)
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-success alert-dismissable");
                    msg.Text = "Success !";
                    //ClearData();
                }
                else
                {
                    noti.Visible = true;
                    noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
                    msg.Text = "Fail !";
                }
            }
            else
            {
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                msg.Text = "This item is used in FrontEnd. CANNOT BE DELETED !";
            }
            
            grdFML_NeedDSource();
        }
    }
    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }
    public string getParentName(string pid)
    {
        string rtn = string.Empty;
        rtn = dalp.SelectNameByID(pid);
        return rtn;
    }
    public string getInputtype(string inputtypeID)
    {
        string rtn = string.Empty;
        if(inputtypeID =="1")
        {
            rtn = "TextBox (Normal Text)";
        }
        else if (inputtypeID == "2")
        {
            rtn = "TextBox (Password)";
        }
        else if (inputtypeID == "3")
        {
            rtn = "TextBox (Email)";
        }
        else if (inputtypeID == "4")
        {
            rtn = "TextArea";
        }
        else if (inputtypeID == "5")
        {
            rtn = "Text Editor";
        }
        else if (inputtypeID == "6")
        {
            rtn = "RadioButton";
        }
        else if (inputtypeID == "7")
        {
            rtn = "RadioButton (Dynamic Data)";
        }
        else if (inputtypeID == "8")
        {
            rtn = "CheckBox";
        }
        else if (inputtypeID == "9")
        {
            rtn = "CheckBox (Dynamic Data)";
        }
        else if (inputtypeID == "10")
        {
            rtn = "DropDownList";
        }
        else if (inputtypeID == "11")
        {
            rtn = "DropDownList (Dynamic Data)";
        }
        else if (inputtypeID == "12")
        {
            rtn = "Datepicker";
        }
        else if (inputtypeID == "13")
        {
            rtn = "File Upload";
        }
        else if (inputtypeID == "14")
        {
            rtn = "Hidden (Integer)";
        }
        else if (inputtypeID == "15")
        {
            rtn = "Hidden (Character)";
        }
        return rtn;
    }
}