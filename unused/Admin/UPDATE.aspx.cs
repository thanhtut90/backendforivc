﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_UPDATE : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btn_click(object sender,EventArgs e)
    {
        #region Conference Speaker
        //Functionality fn = new Functionality();
        //string filename = "dupspeaker.xlsx";
        //string savePath = Server.MapPath(("~\\upfiles\\") + filename);
        //DataTable dt = getDataTableFromExcel(savePath, filename);
        //int rowsnum = dt.Rows.Count;
        //if (rowsnum > 0)
        //{
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        string spname = dr["SName"].ToString();
        //        string sqlsnamelst = "select * from FrontEnd where f_pid = 'BP10038' and f_c_id = 'BC10169' and f_content = '" + spname + "'";
        //        DataTable dtsnameID = fn.GetDatasetByCommand(sqlsnamelst, "Sdt").Tables[0];
        //        if (dtsnameID.Rows.Count > 0)
        //        {
        //            string sID = string.Empty;
        //            for (int i = 0; i < dtsnameID.Rows.Count; i++)
        //            {
        //                if (i == 0)
        //                {
        //                    sID = dtsnameID.Rows[i]["f_id"].ToString();
        //                }
        //                else
        //                {
        //                    string sqldelete = "update FrontEnd set f_deleteflag=1 where f_c_rowid in (select f_c_rowid from FrontEnd where f_id='" + dtsnameID.Rows[i]["f_id"].ToString() + "')";
        //                    fn.ExecuteSQL(sqldelete);
        //                    string sqlupdate = "update FrontEnd set f_content='" + sID + "' where f_pid='BP10017' and f_c_id='BC10081' and f_content='" + dtsnameID.Rows[i]["f_id"].ToString() + "'";
        //                    fn.ExecuteSQL(sqldelete);
        //                }
        //            }

        //        }
        //    }
        //}
        #endregion

        #region Conference
        //string failID = "";
        //int failcount = 0;
        //Functionality fn = new Functionality();
        //string filename = "conference_bak.xlsx";
        //string savePath = Server.MapPath(("~\\upfiles\\") + filename);
        //DataTable dt = getDataTableFromExcel(savePath, filename);
        //int rowsnum = dt.Rows.Count;
        //if (rowsnum > 0)
        //{
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        string c_year = dr["c_year"].ToString();
        //        string c_brief = dr["c_brief"].ToString();
        //        string c_title = dr["c_title"].ToString();
        //        string c_date = dr["c_date"].ToString();
        //        string c_liveQA = dr["c_liveQA"].ToString();
        //        string c_stime = dr["c_stime"].ToString();
        //        string c_etime = dr["c_etime"].ToString();
        //        string c_parentconfID = dr["c_parentconfID"].ToString();
        //        string c_venue = dr["c_venue"].ToString();
        //        string UpdatedTime = dr["UpdatedTime"].ToString();
        //        string ID = dr["ID"].ToString();
        //        string c_cc = dr["c_cc"].ToString();

        //        string sql = "insert into tb_Conference (c_year,	c_brief,	c_title,	c_date,	c_liveQA,	c_stime,	c_etime,	c_parentconfID,	c_venue,	UpdatedTime,	ID,	c_cc)";
        //        sql += " values (@c_year,	@c_brief,	@c_title,	@c_date,	@c_liveQA,	@c_stime,	@c_etime,	@c_parentconfID,	@c_venue,	@UpdatedTime,	@ID,	@c_cc)";
        //        SqlParameter[] parms = {
        //            new SqlParameter("@c_year",SqlDbType.NVarChar),
        //            new SqlParameter("@c_brief",SqlDbType.NVarChar),
        //            new SqlParameter("@c_title",SqlDbType.NVarChar),
        //            new SqlParameter("@c_date",SqlDbType.NVarChar),
        //            new SqlParameter("@c_liveQA",SqlDbType.NVarChar),
        //            new SqlParameter("@c_stime",SqlDbType.NVarChar),
        //            new SqlParameter("@c_etime",SqlDbType.NVarChar),
        //            new SqlParameter("@c_parentconfID",SqlDbType.NVarChar),
        //            new SqlParameter("@c_venue",SqlDbType.NVarChar),
        //            new SqlParameter("@UpdatedTime",SqlDbType.NVarChar),
        //            new SqlParameter("@ID",SqlDbType.NVarChar),
        //            new SqlParameter("@c_cc",SqlDbType.NVarChar)
        //        };
        //        parms[0].Value = c_year;
        //        parms[1].Value = c_brief;
        //        parms[2].Value = c_title;
        //        parms[3].Value = c_date;
        //        parms[4].Value = c_liveQA;
        //        parms[5].Value = c_stime;
        //        parms[6].Value = c_etime;
        //        parms[7].Value = c_parentconfID;
        //        parms[8].Value = c_venue;
        //        parms[9].Value = UpdatedTime;
        //        parms[10].Value = ID;
        //        parms[11].Value = c_cc;

        //        int res = fn.ExecuteNonQuery(sql, parms);
        //        if(res <0)
        //        {
        //            failID += ID + ",";
        //            failcount++;
        //        }

        //    }
        //}
        //lblcount.Text = failcount.ToString();
        //lblIDs.Text = failID.ToString();
        #endregion

        #region Exhibiting Company
        //string failID = "";
        //int failcount = 0;
        //Functionality fn = new Functionality();
        //string filename = "ExhibitingCompany.xlsx";
        //string savePath = Server.MapPath(("~\\upfiles\\") + filename);
        //DataTable dt = getDataTableFromExcel(savePath, filename);
        //int rowsnum = dt.Rows.Count;
        //if (rowsnum > 0)
        //{
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        string ec_website = dr["ec_website"].ToString();
        //        string UpdatedTime = dr["UpdatedTime"].ToString();
        //        string ec_country = dr["ec_country"].ToString();
        //        string ID = dr["ID"].ToString();
        //        string ec_booth = dr["ec_booth"].ToString();
        //        string ec_name = dr["ec_name"].ToString();
        //        string ec_ecompanytype = dr["ec_ecompanytype"].ToString();
        //        string ec_companyprofile = dr["ec_companyprofile"].ToString();
        //        string ec_product = dr["ec_product"].ToString();
        //        string ec_email = dr["ec_email"].ToString();
        //        string c_hall = dr["c_hall"].ToString();
        //        string ec_logo = dr["ec_logo"].ToString();
        //        string ec_brochure = dr["ec_brochure"].ToString();
        //        string ec_address = dr["ec_address"].ToString();
        //        string contactpersonID = dr["contactpersonID"].ToString();
        //        string ec_contactno = dr["ec_contactno"].ToString();


        //        string sql = "insert into tb_ExhibitingCompany (ec_website,	UpdatedTime,	ec_country,	ID,	ec_booth,	ec_name,	ec_ecompanytype,	ec_companyprofile,	ec_product,	ec_email,	c_hall,	ec_logo,	ec_brochure,	ec_address,	contactpersonID,	ec_contactno)";
        //        sql += " values (@ec_website,	@UpdatedTime,	@ec_country,	@ID,	@ec_booth,	@ec_name,	@ec_ecompanytype,	@ec_companyprofile,	@ec_product,	@ec_email,	@c_hall,	@ec_logo,	@ec_brochure,	@ec_address,	@contactpersonID,	@ec_contactno)";
        //        SqlParameter[] parms = {
        //            new SqlParameter("@ec_website",SqlDbType.NVarChar),
        //            new SqlParameter("@UpdatedTime",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_country",SqlDbType.NVarChar),
        //            new SqlParameter("@ID",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_booth",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_name",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_ecompanytype",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_companyprofile",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_product",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_email",SqlDbType.NVarChar),
        //            new SqlParameter("@c_hall",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_logo",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_brochure",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_address",SqlDbType.NVarChar),
        //            new SqlParameter("@contactpersonID",SqlDbType.NVarChar),
        //            new SqlParameter("@ec_contactno",SqlDbType.NVarChar)

        //        };
        //        parms[0].Value = ec_website;
        //        parms[1].Value = UpdatedTime;
        //        parms[2].Value = ec_country;
        //        parms[3].Value = ID;
        //        parms[4].Value = ec_booth;
        //        parms[5].Value = ec_name;
        //        parms[6].Value = ec_ecompanytype;
        //        parms[7].Value = ec_companyprofile;
        //        parms[8].Value = ec_product;
        //        parms[9].Value = ec_email;
        //        parms[10].Value = c_hall;
        //        parms[11].Value = ec_logo;
        //        parms[12].Value = ec_brochure;
        //        parms[13].Value = ec_address;
        //        parms[14].Value = contactpersonID;
        //        parms[15].Value = ec_contactno;


        //        int res = fn.ExecuteNonQuery(sql, parms);
        //        if (res < 0)
        //        {
        //            failID += ID + ",";
        //            failcount++;
        //        }

        //    }
        //}
        //lblcount.Text = failcount.ToString();
        //lblIDs.Text = failID.ToString();
        #endregion

        #region Speaker
        string failID = "";
        int failcount = 0;
        Functionality fn = new Functionality();
        string filename = "speaker.xlsx";
        string savePath = Server.MapPath(("~\\upfiles\\") + filename);
        DataTable dt = getDataTableFromExcel(savePath, filename);
        int rowsnum = dt.Rows.Count;
        if (rowsnum > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                string s_bio = dr["s_bio"].ToString();
                string s_companay = dr["s_companay"].ToString();
                string s_country = dr["s_country"].ToString();
                string s_profilepic = dr["s_profilepic"].ToString();
                string s_email = dr["s_email"].ToString();
                string s_fullname = dr["s_fullname"].ToString();
                string UpdatedTime = dr["UpdatedTime"].ToString();
                string s_job = dr["s_job"].ToString();
                string ID = dr["ID"].ToString();
                string s_address = dr["s_address"].ToString();
                string s_mobile = dr["s_mobile"].ToString();


                string sql = "insert into tb_Speaker (s_bio,	s_companay,	UpdatedTime,	s_job,	ID,	s_address,	s_mobile)";
                sql += " values (@s_bio,	@s_companay,	@UpdatedTime,	@s_job,	@ID,	@s_address,	@s_mobile)";
                SqlParameter[] parms = {
                    new SqlParameter("@s_bio", SqlDbType.NVarChar),
                    new SqlParameter("@s_companay", SqlDbType.NVarChar),
                    new SqlParameter("@s_country", SqlDbType.NVarChar),
                    new SqlParameter("@s_profilepic", SqlDbType.NVarChar),
                    new SqlParameter("@s_email", SqlDbType.NVarChar),
                    new SqlParameter("@s_fullname", SqlDbType.NVarChar),
                    new SqlParameter("@UpdatedTime", SqlDbType.NVarChar),
                    new SqlParameter("@s_job", SqlDbType.NVarChar),
                    new SqlParameter("@ID", SqlDbType.NVarChar),
                    new SqlParameter("@s_address", SqlDbType.NVarChar),
                    new SqlParameter("@s_mobile", SqlDbType.NVarChar)

                };
                parms[0].Value = s_bio;
                parms[1].Value = s_companay;
                parms[2].Value = s_country;
                parms[3].Value = s_profilepic;
                parms[4].Value = s_email;
                parms[5].Value = s_fullname;
                parms[6].Value = UpdatedTime;
                parms[7].Value = s_job;
                parms[8].Value = ID;
                parms[9].Value = s_address;
                parms[10].Value = s_mobile;


                int res = fn.ExecuteNonQuery(sql, parms);
                if (res < 0)
                {
                    failID += ID + ",";
                    failcount++;
                }

            }
        }
        lblcount.Text = failcount.ToString();
        lblIDs.Text = failID.ToString();
        #endregion
    }
    private System.Data.DataTable getDataTableFromExcel(string filePath, string filename)
    {
        DataTable dt = new DataTable();
        Microsoft.Office.Interop.Excel.Application appExl;
        Microsoft.Office.Interop.Excel.Workbook workbook;
        Microsoft.Office.Interop.Excel.Worksheet NwSheet;
        Microsoft.Office.Interop.Excel.Range ShtRange;
        appExl = new Microsoft.Office.Interop.Excel.ApplicationClass();
        try
        {

            workbook = appExl.Workbooks.Open(filePath);

            NwSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets.get_Item(1);
            int Cnum = 0;
            int Rnum = 0;

            ShtRange = NwSheet.UsedRange;
            #region Conference Speaker
            //dt.Columns.Add("SName");
            #endregion

            #region Conference
            //dt.Columns.Add("c_year");
            //dt.Columns.Add("c_brief");
            //dt.Columns.Add("c_title");
            //dt.Columns.Add("c_date");
            //dt.Columns.Add("c_liveQA");
            //dt.Columns.Add("c_stime");
            //dt.Columns.Add("c_etime");
            //dt.Columns.Add("c_parentconfID");
            //dt.Columns.Add("c_venue");
            //dt.Columns.Add("UpdatedTime");
            //dt.Columns.Add("ID");
            //dt.Columns.Add("c_cc");
            #endregion

            #region Speaker
            dt.Columns.Add("s_bio");
            dt.Columns.Add("s_companay");
            dt.Columns.Add("s_country");
            dt.Columns.Add("s_profilepic");
            dt.Columns.Add("s_email");
            dt.Columns.Add("s_fullname");
            dt.Columns.Add("UpdatedTime");
            dt.Columns.Add("s_job");
            dt.Columns.Add("ID");
            dt.Columns.Add("s_address");
            dt.Columns.Add("s_mobile");

            #endregion
            #region Exhibiting Company
            //dt.Columns.Add("ec_website");
            //dt.Columns.Add("UpdatedTime");
            //dt.Columns.Add("ec_country");
            //dt.Columns.Add("ID");
            //dt.Columns.Add("ec_booth");
            //dt.Columns.Add("ec_name");
            //dt.Columns.Add("ec_ecompanytype");
            //dt.Columns.Add("ec_companyprofile");
            //dt.Columns.Add("ec_product");
            //dt.Columns.Add("ec_email");
            //dt.Columns.Add("c_hall");
            //dt.Columns.Add("ec_logo");
            //dt.Columns.Add("ec_brochure");
            //dt.Columns.Add("ec_address");
            //dt.Columns.Add("contactpersonID");
            //dt.Columns.Add("ec_contactno");

            #endregion
            int i = 1;
            for (Rnum = 2; Rnum <= ShtRange.Rows.Count; Rnum++)
            {
                i = 1;
                DataRow dr = dt.NewRow();
                for (Cnum = 1; Cnum <= ShtRange.Columns.Count; Cnum++)
                {
                    try
                    {
                        if ((ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString() != "")
                        {
                         
                            dr[Cnum - i] = (ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString();
                        }
                        else
                        {
                            i = 2;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();

            }

            workbook.Close();
            appExl.Quit();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

        

        return dt;
    }
    public static void RemoveNullColumnFromDataTable(DataTable dt)
    {
        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            if (dt.Rows[i][1] == DBNull.Value)
                dt.Rows[i].Delete();
        }
        dt.AcceptChanges();
    }
}