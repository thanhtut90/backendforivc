﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="AddNewFormColumn.aspx.cs" Inherits="Admin_AddNewFormColumn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
        <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdCln" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Adding New Form Column</h3>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Form Name</label>
                    <div class="col-sm-5">                
                        <asp:DropDownList ID="ddlform" CssClass="form-control" OnSelectedIndexChanged="ddlform_selectedindexchanged" AutoPostBack="true" runat="server"></asp:DropDownList>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                
                <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                            <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                            PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="true" PageSize="20">
                            <ClientSettings AllowKeyboardNavigation="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="c_id" CommandItemDisplay="Top">
                                    <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                                <Columns> 
                                    <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex+1 %>
                                                </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn  DataField="c_label" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Label" SortExpression="c_label" UniqueName="c_label" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="c_colname" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Column Name" SortExpression="c_colname" UniqueName="c_colname" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="c_inputtype" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Input Type" SortExpression="c_inputtype" UniqueName="c_inputtype" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="c_content" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Content" SortExpression="c_content" UniqueName="c_content" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn> 
                                    <telerik:GridBoundColumn  DataField="c_isRequired" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Is Mandatory" SortExpression="c_isRequired" UniqueName="c_isRequired" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>                                           
                                                                            
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                                
                    </div>
                </div>
                </div>                     
                  
                <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Choose Input Type</label>
                      <div class="col-sm-4">
                        <asp:HiddenField ID="hfcid" runat="server" />
                        <asp:RadioButtonList ID="ctt" OnSelectedIndexChanged="rdolst_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="1">&nbsp;TextBox</asp:ListItem>  
                            <asp:ListItem Value="2">&nbsp;Password</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Email</asp:ListItem>                       
                            <asp:ListItem Value="4">&nbsp;TextArea</asp:ListItem>
                            <asp:ListItem Value="5">&nbsp;Telerik Editor</asp:ListItem>
                            <asp:ListItem Value="6">&nbsp;RadioButton</asp:ListItem>
                            <asp:ListItem Value="7">&nbsp;RadioButton (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="8">&nbsp;CheckBox</asp:ListItem>
                            <asp:ListItem Value="9">&nbsp;CheckBox List (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="10">&nbsp;Dropdown List</asp:ListItem>
                            <asp:ListItem Value="11">&nbsp;Dropdown List (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="12">&nbsp;Datepicker</asp:ListItem>
                            <asp:ListItem Value="13">&nbsp;Fileupload</asp:ListItem> 
                            <asp:ListItem Value="14">&nbsp;Hidden value (Integer)</asp:ListItem>
                            <asp:ListItem Value="15">&nbsp;Hidden Value (Character)</asp:ListItem>
                          </asp:RadioButtonList>
                      </div>
                    <div class="col-sm-6">
                        <div class="form-group" id ="divdt" runat="server" Visible="false">                            
                            <div class="col-sm-6">
                                <label for="inputEmail3">Data Table</label><br />
                                <asp:DropDownList ID="ddlRole1" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlRole1_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--<asp:DropDownList ID="ddlRole2" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlRole2_SelectedIndexChanged"></asp:DropDownList>--%>
                            </div>
                          
                        </div>
                        <div class="form-group" id ="divdc" runat="server" Visible="false">                            
                            <div class="col-sm-6">
                                <label for="inputEmail3">Data Column</label><br />
                                <asp:DropDownList ID="ddldcol" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddldcol_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            
                        </div>
                    </div>
                    </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Is Mandatory</label>
                    <div class="col-sm-6">
                    <asp:CheckBox ID="chkRequired" runat="server" />
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Data Column Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtdatacol" CssClass="form-control" placeholder="Insert Data Column Name" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Label</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtlbl" CssClass="form-control" placeholder="Insert Label Name" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sort Order</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtso" Enabled="false" CssClass="form-control" Width="10%"  runat="server" onkeypress="return NumberOnly(event)"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><asp:Label id="lblcontent" runat="server" Visible="false">Content</asp:Label></label>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent1" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>                    
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd1" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd1_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtcontent2" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd2" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd2_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent3" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd3" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd3_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent4" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd4" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd4_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent5" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd5" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd5_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <asp:LinkButton ID="btnsave" class="btn btn-primary" style="text-align:left" OnClick="btnsave_OnClick"
                    runat="server"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp; Save</asp:LinkButton>
        </div>
    </div>
</asp:Content>

