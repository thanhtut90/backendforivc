﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Object;
using App.DAL;
using App.Common;
using System.Collections.Generic;

public partial class Admin_AddNewFormColumn : System.Web.UI.Page
{
    #region Declaration
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_Parent dalp = new DAL_Parent();
    DAL_Child dalc = new DAL_Child();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    ChildModel cmodel = new ChildModel();
    ParentModel pmodel = new ParentModel();
    BackEnd_Parent Pobj = new BackEnd_Parent();
    BackEnd_Child Cobj = new BackEnd_Child();
    GenKey gk= new GenKey();
    string pid = string.Empty, cid = string.Empty;
    string content = "", content1 = "", content2 = "", content3 = "", content4 = "", content5 = "", comma = "", pname = "";
    #endregion

    #region Class
    class ShowData
    {
        public string tbname { get; set; }

    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindDDLRole();
            BindForm();
        }
    }
    #endregion

    #region BindFrom
    private void BindForm()
    {
        List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
        lst = dalp.SelectAllData();
        ddlform.DataSource = lst;
        ddlform.DataTextField = "p_name";
        ddlform.DataValueField = "p_id";
        ddlform.DataBind();
        ListItem a = new ListItem("Select Form", "");
        ddlform.Items.Insert(ddlform.Items.Count - ddlform.Items.Count, a);
    }
    #endregion

    #region ddlform_selectedindexchange
    protected void ddlform_selectedindexchanged(object sender,EventArgs e)
    {
        if(!string.IsNullOrEmpty(ddlform.SelectedValue))
        {
            grdFML_NeedDSource();
            pid = ddlform.SelectedValue.ToString();
            BindSortOrder();

        }
    }

    private void BindSortOrder()
    {
        int sortorder = dalc.selectSortOrderByPID(pid);
        txtso.Text = sortorder.ToString();
    }
    #endregion

    #region Add Button onclick
    protected void btnadd1_onclick(object sender, EventArgs e)
    {
        if (btnadd1.Text == "Add")
        {
            btnadd1.Text = "Delete";
            btnadd2.Visible = true;
            txtcontent2.Visible = true;
        }
        else if (btnadd1.Text == "Delete")
        {
            txtcontent1.Text = "";
            btnadd1.Text = "Add";
            btnadd1.Visible = false;
            txtcontent1.Visible = false;

        }
    }
    protected void btnadd2_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        if (btnadd2.Text == "Add")
        {
            btnadd2.Text = "Delete";
            btnadd3.Visible = true;
            txtcontent3.Visible = true;
        }
        else if (btnadd2.Text == "Delete")
        {
            txtcontent2.Text = "";
            btnadd2.Text = "Add";
            btnadd2.Visible = false;
            txtcontent2.Visible = false;

        }
    }
    protected void btnadd3_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        btnadd2.Visible = true;
        txtcontent2.Visible = true;
        if (btnadd3.Text == "Add")
        {
            btnadd3.Text = "Delete";
            btnadd4.Visible = true;
            txtcontent4.Visible = true;
        }
        else if (btnadd3.Text == "Delete")
        {
            txtcontent3.Text = "";
            btnadd3.Text = "Add";
            btnadd3.Visible = false;
            txtcontent3.Visible = false;

        }
    }
    protected void btnadd4_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        txtcontent2.Visible = true;
        btnadd2.Visible = true;
        txtcontent3.Visible = true;
        btnadd3.Visible = true;
        if (btnadd4.Text == "Add")
        {
            btnadd4.Text = "Delete";
            btnadd5.Visible = true;
            txtcontent5.Visible = true;
        }
        else if (btnadd4.Text == "Delete")
        {
            txtcontent4.Text = "";
            btnadd4.Text = "Add";
            btnadd4.Visible = false;
            txtcontent4.Visible = false;
        }
    }
    protected void btnadd5_onclick(object sender, EventArgs e)
    {
        btnadd1.Visible = true;
        txtcontent1.Visible = true;
        txtcontent2.Visible = true;
        btnadd2.Visible = true;
        txtcontent3.Visible = true;
        btnadd3.Visible = true;
        txtcontent4.Visible = true;
        btnadd4.Visible = true;
        if (btnadd5.Text == "Add")
        {
            btnadd5.Text = "Delete";
        }
        else if (btnadd5.Text == "Delete")
        {
            txtcontent5.Text = "";
            btnadd5.Text = "Add";
            btnadd5.Visible = false;
            txtcontent5.Visible = false;

        }
    }
    #endregion

    #region  Save

    #region Button Click
    protected void btnsave_OnClick(object sender, EventArgs e)
    {
        try
        {
            #region AddButton
            if (txtcontent1.Text != "" && (txtcontent2.Text != "" || txtcontent3.Text != "" || txtcontent4.Text != "" || txtcontent5.Text != ""))
            {
                content1 = txtcontent1.Text.Trim() + ",";
            }
            else
            {
                content1 = txtcontent1.Text.Trim();
            }

            if (txtcontent2.Text != "")
            {
                content2 = txtcontent2.Text.Trim() + ",";
            }
            else if (content2 == "")
            {
                content1 = txtcontent1.Text.Trim();
            }

            if (txtcontent3.Text != "")
            {
                content3 = txtcontent3.Text.Trim() + ",";
            }
            else if (content3 == "")
            {
                content2 = txtcontent2.Text.Trim();
            }

            if (txtcontent4.Text != "")
            {
                content4 = txtcontent4.Text.Trim() + ",";
            }
            else if (content4 == "")
            {
                content3 = txtcontent3.Text.Trim();
            }

            if (txtcontent5.Text != "")
            {
                content5 = txtcontent5.Text.Trim();
            }
            else if (content5 == "")
            {
                content4 = txtcontent4.Text.Trim();
            }
            #endregion

            #region Add Infos
            content = content1 + content2 + content3 + content4 + content5;

            bool isreq;
            if (chkRequired.Checked == true)
            {
                isreq = true;
            }
            else
            {
                isreq = false;
            }
            //pid = int.Parse(ddlform.SelectedValue.ToString());

            //string rbt = ctt.SelectedValue.ToString();//GetCheckedRadio(ctt);
            //string str = rbt.Split('_').LastOrDefault();
            //if (str == "11" || str == "9" || str == "7")
            //{
            //    if (str == "11")
            //    {
            //        if(!string.IsNullOrEmpty(ddlRole1.SelectedValue.ToString()))
            //        {
            //            content = ddlRole1.SelectedValue + "^" + ddlRole1.SelectedItem;
            //        }

            //    }
            //    else if (str == "7" || str == "9")
            //    {
            //        if (!string.IsNullOrEmpty(ddlRole2.SelectedValue.ToString()))
            //        {
            //            content = ddlRole2.SelectedValue;
            //        }                        
            //    }
            //}
            
            pid = ddlform.SelectedValue.ToString();

            string rbt = ctt.SelectedValue.ToString();//GetCheckedRadio(ctt);
            string str = rbt.Split('_').LastOrDefault();

            if (str == "11" || str == "9" || str == "7")
            {
                content = txtcontent1.Text.Trim();
            }
            #endregion

            #region Save
            string ckey = string.Empty; string cid = string.Empty;
            ckey = gk.getKey("BC");
            cid = "BC" + ckey;

            Cobj.c_id = cid;
            Cobj.c_pid = pid;
            Cobj.c_inputtype = Convert.ToInt32(str);
            Cobj.c_label = txtlbl.Text.Trim();
            Cobj.c_colname = txtdatacol.Text.Trim();
            Cobj.c_content = content;
            Cobj.c_sortorder = Convert.ToInt32(txtso.Text.Trim());
            Cobj.c_isRequired = isreq;
            Cobj.c_deleteflag = false;
            //int count = dalc.CheckExist(Cobj);
            //if (count == 0)
            //{
            int isSuccess = dalc.Save(Cobj);
            if (isSuccess > 0)
            {
                gk.SaveKey("BC",int.Parse(ckey));
                cid = dalc.selectCIDByPIDLabel(pid, txtlbl.Text.Trim());
                SettleForFrontEnd(pid, cid, Convert.ToInt32(str), txtlbl.Text.Trim(), Convert.ToInt32(txtso.Text.Trim()));
                ResetChild();
                grdFML_NeedDSource();
                BindSortOrder();
                
            }
            else
            {

            }
            //}
            #endregion
        }
        catch (Exception ex)
        {
            Response.Write(ex.InnerException);
        }
    }

    private void SettleForFrontEnd(string pid, string cid,int inputtype,string label,int sortorder)
    {
        List<FrontEnd> lstFrontEnd = new List<FrontEnd>();
        lstFrontEnd = dalf.SelectByPId(pid);       
        if(lstFrontEnd.Count>0)
        {
            foreach (var l in lstFrontEnd)
            {
                if (l.f_sortorder == 1)
                {
                    string fkey = string.Empty; string fid = string.Empty;
                    fkey = gk.getKey("FE");
                    fid = "FE" + fkey;
                    FrontEnd fe = new FrontEnd()
                    { f_id=fid,f_pid = l.f_pid, f_c_id = cid, f_label = label, f_entrycount = 1, f_inputtype = inputtype, f_content = "", f_c_isFinish = true, f_c_rowid = l.f_c_rowid, f_sortorder = sortorder, f_createddate = dalf.ConvertToTimestamp(DateTime.Now),f_deleteflag=false,f_lang=1 };
                    int isSuccess = dalf.Save(fe);
                    if(isSuccess ==1)
                    {
                        gk.SaveKey("FE", int.Parse(fkey));
                    }
                    #region timestamp  
                    DateTime updtime = DateTime.Now;
                    string updatetimestamp = dalf.ConvertToTimestamp(updtime);
                    int isSuc = dalf.updatetimestamp(pid, l.f_c_rowid, updatetimestamp);
                    #endregion
                }

            }
        }
    }
    #endregion

    #region BindGrid 

    protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            List<ChildModel> lst = new List<ChildModel>();
            string pid = ddlform.SelectedValue.ToString();
            var res = dalc.SelectByPId(pid);

            ChildModel k;
            foreach (var i in res)
            {
                k = new ChildModel
                {                    
                    c_pid = i.c_pid,
                    c_id = i.c_id,
                    c_label = i.c_label,
                    inputtype = i.c_inputtype == 1 ? "TextBox" : i.c_inputtype == 2 ? "Password" : i.c_inputtype == 3 ? "Email" : i.c_inputtype == 4 ? "TextArea" : i.c_inputtype == 5 ? "Text Editor" : i.c_inputtype == 6 ? "RadioButton" : i.c_inputtype == 7 ? "RadioButton (Dynamic Data Bind)" : i.c_inputtype == 8 ? "CheckBox" : i.c_inputtype == 9 ? "CheckBox List (Dynamic Data Bind)" : i.c_inputtype == 10 ? "Dropdown List" : i.c_inputtype == 10 ? "Dropdown List (Dynamic Data Bind)" : i.c_inputtype == 10 ? "Datepicker" : i.c_inputtype == 10 ? "Fileupload" : i.c_inputtype == 10 ? "Hidden value (Integer)" : i.c_inputtype == 10 ? " Hidden Value (Character)" : "",
                    c_content = i.c_content,
                    c_isRequired = i.c_isRequired,
                    c_sortorder = i.c_sortorder
                };
                lst.Add(k);
            }
            grdFML.DataSource = lst;
          
        }
        catch (Exception ex) { }
    }
    protected void grdFML_NeedDSource()
    {
        try
        {
            List<ChildModel> lst = new List<ChildModel>();
            string pid = ddlform.SelectedValue.ToString();
            var res = dalc.SelectByPId(pid);

            ChildModel k;
            foreach (var i in res)
            {
                k = new ChildModel
                {
                    c_pid = i.c_pid,
                    c_id = i.c_id,
                    c_label = i.c_label,
                    c_colname=i.c_colname,
                    inputtype = i.c_inputtype == 1 ? "TextBox" : i.c_inputtype == 2 ? "Password" : i.c_inputtype == 3 ? "Email" : i.c_inputtype == 4 ? "TextArea" : i.c_inputtype == 5 ? "Text Editor" : i.c_inputtype == 6 ? "RadioButton" : i.c_inputtype == 7 ? "RadioButton (Dynamic Data Bind)" : i.c_inputtype == 8 ? "CheckBox" : i.c_inputtype == 9 ? "CheckBox List (Dynamic Data Bind)" : i.c_inputtype == 10 ? "Dropdown List" : i.c_inputtype == 10 ? "Dropdown List (Dynamic Data Bind)" : i.c_inputtype == 10 ? "Datepicker" : i.c_inputtype == 10 ? "Fileupload" : i.c_inputtype == 10 ? "Hidden value (Integer)" : i.c_inputtype == 10 ? " Hidden Value (Character)" : "",
                    c_content = i.c_content,
                    c_isRequired = i.c_isRequired,
                    c_sortorder = i.c_sortorder
                };
                lst.Add(k);
            }
            grdFML.DataSource = lst;

            grdFML.DataBind();

        }
        catch (Exception ex) { }
    }
    #endregion

    #region ResetChild
    private void ResetChild()
    {
        ctt.SelectedValue = "1";       
        chkRequired.Checked = false;
        ddlRole1.SelectedIndex = 0;
        //ddlRole2.SelectedIndex = 0;
        txtlbl.Text = "";
        txtcontent1.Text = "";
        txtcontent2.Text = "";
        txtcontent3.Text = "";
        txtcontent4.Text = "";
        txtcontent5.Text = "";
        txtso.Text = "";

        txtcontent1.Visible = false;
        txtcontent2.Visible = false;
        txtcontent3.Visible = false;
        txtcontent4.Visible = false;
        txtcontent5.Visible = false;
        btnadd1.Visible = false;
        btnadd2.Visible = false;
        btnadd3.Visible = false;
        btnadd4.Visible = false;
        btnadd5.Visible = false;
        ddlRole1.Visible = false;
        //ddlRole2.Visible = false;
        btnadd1.Text = "Add";
        btnadd2.Text = "Add";
        btnadd3.Text = "Add";
        btnadd4.Text = "Add";
        btnadd5.Text = "Add";

    }
    #endregion

    String GetCheckedRadio(Control ct)
    {
        foreach (var control in ct.Controls)
        {
            RadioButton radio = control as RadioButton;

            if (radio != null && radio.Checked)
            {
                return radio.ID;
            }
        }

        return null;
    }
    #endregion

    #region rdo selectedindexchange   
    protected void rdolst_SelectedIndexChanged(object sender, EventArgs e)
    {
        divdt.Visible = true;
        ddlRole1.Visible = false;
        //ddlRole2.Visible = false;
        if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
        {
            lblcontent.Visible = true;
            txtcontent1.Visible = true;
            btnadd1.Visible = true;
        }
        else
        {
            lblcontent.Visible = false;
            txtcontent1.Visible = false;
            btnadd1.Visible = false;
        }

        if (ctt.SelectedValue == "11" || ctt.SelectedValue == "9" || ctt.SelectedValue == "7")
        {
            if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            {
                ddlRole1.Visible = true;

            }
            //else if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            //{
            //    ddlRole1.Visible = false;
            //    ddlRole2.Visible = true;
            //}
        }
        else
        {
            ddlRole1.Visible = false;
            //ddlRole2.Visible = false;
        }
    }
    #endregion

    #region rdo rdoCheckDynamic_SelectedIndexChanged
    protected void rdoCheckDynamic_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindDDLRole();
        if (ctt.SelectedValue == "11" || ctt.SelectedValue == "9" || ctt.SelectedValue == "7")
        {
            ddlRole1.Visible = true;
            //if (ctt.SelectedValue == "11")
            //{
            //    ddlRole1.Visible = true;
            //    ddlRole2.Visible = false;
            //}
            //else if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9")
            //{
            //    ddlRole1.Visible = false;
            //    ddlRole2.Visible = true;
            //}
        }
        else
        {
            ddlRole1.Visible = false;
            //ddlRole2.Visible = false;
        }
    }
    protected void ddlRole1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddlRole1.SelectedValue.ToString()))
        {
            if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9" || ctt.SelectedValue == "11")
            {
                divdc.Visible = true;
                ddldcol.Visible = true;
                string id =ddlRole1.SelectedValue.ToString();
                BindDDLCol(id);
            }
            else
            {
                txtcontent1.Text = ddlRole1.SelectedItem.ToString();
            }
        }

    }
    //protected void ddlRole2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(ddlRole2.SelectedValue.ToString()))
    //    {
    //        if (ctt.SelectedValue == "7" || ctt.SelectedValue == "9")
    //        {
    //            divdc.Visible = true;
    //            ddldcol.Visible = true;
    //            int id = int.Parse(ddlRole2.SelectedValue.ToString());
    //            BindDDLCol(id);                
    //        }
    //        else
    //        {
    //            txtcontent1.Text = ddlRole2.SelectedItem.ToString();
    //        }
    //    }

    //}
    protected void ddldcol_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(ddldcol.SelectedValue.ToString()))
        {
            string str = ddlRole1.SelectedItem.ToString() + "^" + ddldcol.SelectedItem.ToString();
            txtcontent1.Text = str;
        }
    }

    #endregion

    #region BindDDLRole
    private void BindDDLRole()
    {
        var lstParent = dalp.SelectAllData();
        #region ddlRole1
        ddlRole1.DataSource = lstParent;
        ddlRole1.DataTextField = "p_name";
        ddlRole1.DataValueField = "p_id";
        ddlRole1.DataBind();
        ListItem lst = new ListItem("Select Table", "");
        ddlRole1.Items.Insert(ddlRole1.Items.Count - ddlRole1.Items.Count, lst);

        #endregion

        #region ddlRole2
        //ddlRole2.DataSource = lstParent;
        //ddlRole2.DataTextField = "p_name";
        //ddlRole2.DataValueField = "p_id";
        //ListItem lst2 = new ListItem("Select Table", "");
        //ddlRole2.Items.Insert(ddlRole2.Items.Count - ddlRole2.Items.Count, lst2);
        //ddlRole2.DataBind();
        #endregion
    }
    #endregion

    #region BindDDLColumn
    private void BindDDLCol(string tbid)
    {
        var lstchild = dalc.SelectByPId(tbid);
        ddldcol.DataSource = lstchild;
        ddldcol.DataValueField = "c_id";
        ddldcol.DataTextField = "c_label";
        ddldcol.DataBind();
        ListItem lst = new ListItem("Choose Column to Bind", "");
        ddldcol.Items.Insert(ddldcol.Items.Count - ddldcol.Items.Count, lst);


    }
    #endregion




}