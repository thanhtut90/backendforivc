﻿using System;
using System.Data;
using App.Object;
using App.DAL;
using App.Common;
public partial class Admin_UploadBackEndForms : System.Web.UI.Page
{
    DAL dal = new DAL();
    DAL_Parent dalp = new DAL_Parent();
    DAL_Child dalc = new DAL_Child();
    
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    ChildModel cmodel = new ChildModel();
    ParentModel pmodel = new ParentModel();
    BackEnd_Parent Pobj = new BackEnd_Parent();
    BackEnd_Child Cobj = new BackEnd_Child();
    GenKey gk = new GenKey();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["DT"] != null)
            {
                string pkey = string.Empty, ckey = string.Empty;
                DataTable dt = Session["DT"] as DataTable;
                if (dt.Rows.Count > 0)
                {
                    string form = string.Empty;string pid = string.Empty; string cid = string.Empty;
                    pkey = gk.getKey("BP");
                    pid = "BP" + pkey;
                   
                    foreach (DataRow dr in dt.Rows)
                    {
                        string type = dr["Type"].ToString();
                        string title = dr["Title"].ToString();
                        string inputtype = getinputtypeID(dr["InputType"].ToString());
                        string colname = dr["ColumnName"].ToString();
                        string label = dr["DisplayLabel"].ToString();
                        string content = dr["Content"].ToString();
                        string sortorder = dr["SortOrder"].ToString();
                        string isrequired = dr["Mandatory"].ToString()=="Yes"?"True":"False";

                        if(title != form)
                        {
                            Pobj.p_id = pid;
                            Pobj.p_name = title;
                            Pobj.p_title = title;
                            Pobj.p_isFinish = true;
                            Pobj.p_type = type;

                            int pisExist = dalp.CheckExist(Pobj);
                            if (pisExist != 1)
                            {
                                dalp.Save(Pobj);                               
                            }
                            
                            gk.SaveKey("BP", int.Parse(pkey));

                            ckey = gk.getKey("BC");
                            cid = "BC" + ckey;

                            Cobj.c_id = cid;
                            Cobj.c_pid = pid;
                            Cobj.c_inputtype = int.Parse(inputtype);
                            Cobj.c_label = label;
                            Cobj.c_content = content;
                            Cobj.c_colname = colname;
                            Cobj.c_sortorder = int.Parse(sortorder);
                            Cobj.c_isRequired = Convert.ToBoolean(isrequired);
                            Cobj.c_deleteflag = false;
                            int cisExit = dalc.CheckExist(Cobj);
                            if (cisExit != 1)
                            {
                                dalc.Save(Cobj);
                            }
                            gk.SaveKey("BC", int.Parse(ckey));
                            form = title;
                        }
                        else
                        {
                            ckey = string.Empty;cid = string.Empty;
                            ckey = gk.getKey("BC");
                            cid = "BC" + ckey;

                            Cobj.c_id = cid;
                            Cobj.c_pid = pid;
                            Cobj.c_inputtype = int.Parse(inputtype);
                            Cobj.c_label = label;
                            Cobj.c_content = content;
                            Cobj.c_colname = colname;
                            Cobj.c_sortorder = int.Parse(sortorder);
                            Cobj.c_isRequired = Convert.ToBoolean(isrequired);
                            Cobj.c_deleteflag = false;
                            int cisExit = dalc.CheckExist(Cobj);
                            if (cisExit != 1)
                            {
                                dalc.Save(Cobj);
                            }
                            form = title;
                        }                       
                        
                    }
                }
                grdForm.Visible = false;
                btnSave.Visible = false;
            }
        }
        catch(Exception ex) { }
    }

    public string getinputtypeID(string inputtype)
    {      
        string rtn = string.Empty;
        if(inputtype == "TextBox (Normal Text)")
        {
            rtn = "1";
        }
        else if (inputtype == "TextBox (Password)")
        {
            rtn = "2";
        }
        else if (inputtype == "TextBox (Email)")
        {
            rtn = "3";
        }
        else if (inputtype == "TextArea")
        {
            rtn = "4";
        }
        else if (inputtype == "TextEditor")
        {
            rtn = "5";
        }
        else if (inputtype == "RadioButton")
        {
            rtn = "6";
        }
        else if (inputtype == "RadioButton (Dynamic Data)")
        {
            rtn = "7";
        }
        else if (inputtype == "CheckBox")
        {
            rtn = "8";
        }
        else if (inputtype == "CheckBox (Dynamic Data)")
        {
            rtn = "9";
        }
        else if (inputtype == "DropDownList")
        {
            rtn = "10";
        }
        else if (inputtype == "DropDownList (Dynamic Data)")
        {
            rtn = "11";
        }
        else if (inputtype == "Datepicker")
        {
            rtn = "12";
        }
        else if (inputtype == "File Upload")
        {
            rtn = "13";
        }
        else if (inputtype == "Hidden (Integer)")
        {
            rtn = "14";
        }
        else if (inputtype == "Hidden (Character)")
        {
            rtn = "15";
        }
        return rtn;
    }
    protected void btnImportData_Click(object sender, EventArgs e)
    {
        try
        {
            if (fupForm.HasFile == false)
            {
                msg.Text = "No Records for Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                return;
            }
            else
            {
                noti.Visible = false;
            }

            string IsXls = System.IO.Path.GetExtension(fupForm.FileName).ToString().ToLower();
            if (IsXls != ".xls" && IsXls != ".xlsx")
            {
                msg.Text = "No Records for Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                return;
            }
            else
            {
                noti.Visible = false;
            }
            //ViewState["qnaire_desc"] = fupForm.FileName.Replace(IsXls, "");
            string filename = System.IO.Path.GetFileNameWithoutExtension(fupForm.FileName) + DateTime.Now.ToString("yyy-MM-dd-hh-mm-ss") + IsXls;

            string CreatePath = System.Web.HttpContext.Current.Server.MapPath("~\\upfiles").ToString();
            if (!System.IO.Directory.Exists(CreatePath))
            {
                System.IO.Directory.CreateDirectory(CreatePath);
            }

            string savePath = Server.MapPath(("~\\upfiles\\") + filename);
            fupForm.SaveAs(savePath);

            DataTable dt = getDataTableFromExcel(savePath, filename);
            int rowsnum = dt.Rows.Count;
            if (rowsnum == 0)
            {
                msg.Text = "Invalid Excel File";
                noti.Visible = true;
                noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                btnSave.Visible = false;
                grdForm.DataSource = null;
                grdForm.DataBind();
            }
            else
            {
                Session["DT"] = dt;

                grdForm.DataSource = dt;
                grdForm.DataBind();
                btnSave.Visible = true;
                noti.Visible = false;
                grdForm.Visible = true;

            }
        }
        catch (Exception ex)
        {
            Response.Write("<script language='javascript'>alert('" + ex.Message.ToString() + "');window.location.href='UploadQuestion.aspx';</script>");
            Response.End();
            throw;
        }
    }

    #region getDataTableFromExcel
    private System.Data.DataTable getDataTableFromExcel(string filePath, string filename)
    {
        DataTable dt = new DataTable();
        Microsoft.Office.Interop.Excel.Application appExl;
        Microsoft.Office.Interop.Excel.Workbook workbook;
        Microsoft.Office.Interop.Excel.Worksheet NwSheet;
        Microsoft.Office.Interop.Excel.Range ShtRange;
        appExl = new Microsoft.Office.Interop.Excel.ApplicationClass();
        try
        {

            workbook = appExl.Workbooks.Open(filePath);

            NwSheet = (Microsoft.Office.Interop.Excel.Worksheet)workbook.Sheets.get_Item(1);
            int Cnum = 0;
            int Rnum = 0;

            ShtRange = NwSheet.UsedRange;

            //No	Title	Input Type	Column Name	Display Label	Content	Sort Order	Mandatory

            dt.Columns.Add("No");
            dt.Columns.Add("Type");
            dt.Columns.Add("Title");
            dt.Columns.Add("InputType");
            dt.Columns.Add("ColumnName");
            dt.Columns.Add("DisplayLabel");
            dt.Columns.Add("Content");
            dt.Columns.Add("SortOrder");
            dt.Columns.Add("Mandatory");

            int i = 1;
            for (Rnum = 2; Rnum <= ShtRange.Rows.Count; Rnum++)
            {
                i = 1;
                //if (ShtRange.Columns.Count == dt.Columns.Count)
                //{
                DataRow dr = dt.NewRow();
                for (Cnum = 1; Cnum <= ShtRange.Columns.Count; Cnum++)
                {
                    try
                    {
                        if ((ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString() != "")
                        {
                            dr[Cnum - i] = (ShtRange.Cells[Rnum, Cnum] as Microsoft.Office.Interop.Excel.Range).Value.ToString();
                        }
                        else
                        {
                            i = 2;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                dt.Rows.Add(dr);
                dt.AcceptChanges();
                //}
                //else
                //{
                //    noti.Visible = true;
                //    msg.Text = "Invalid Excel File";
                //    noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
                //}

            }

            workbook.Close();
            appExl.Quit();
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

        RemoveNullColumnFromDataTable(dt);

        return dt;
    }
    public static void RemoveNullColumnFromDataTable(DataTable dt)
    {
        for (int i = dt.Rows.Count - 1; i >= 0; i--)
        {
            if (dt.Rows[i][1] == DBNull.Value)
                dt.Rows[i].Delete();
        }
        dt.AcceptChanges();
    }

    #endregion
    protected void grdForm_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
    {
        //Session["pgno"] = grdForm.CurrentPageIndex + 1;
    }
    protected void grdForm_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            DataTable dt = new DataTable();//dal.getAllDoctors();
            grdForm.DataSource = dt;
        }
        catch (Exception ex) { }
    }
    protected void grdForm_NeedDSource()
    {
        try
        {
            DataTable dt = new DataTable();// dal.getAllDoctors();
            grdForm.DataSource = dt;
            grdForm.DataBind();
        }
        catch (Exception ex) { }
    }
}