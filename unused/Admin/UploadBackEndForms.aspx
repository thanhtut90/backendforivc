﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="UploadBackEndForms.aspx.cs" Inherits="Admin_UploadBackEndForms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <section class="content">
        <div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                        <h3 class="box-title"><asp:Label ID="lblformname" Text="Forms Uploading" runat="server"></asp:Label></h3>
                </div>            
                <div class="box-body"> 
                    <div class="row">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                         <div class="form-group">                      
                               <div class="col-sm-12">
                               <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                               <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                               </div>   
                               </div>
                            </div>
                         
                            <div class="form-group">
                               <label for="inputEmail3" class="col-sm-12 control-label"><a href="../Template/UploadFormTemplate.xlsx">Download Template Here</a></label>        
                            </div>
                            <br />
                            <div class="form-group">                                                              
                                <div class="col-sm-3">
                                    <asp:FileUpload ID="fupForm" runat="server" />
                                </div>
                                <div class="col-sm-9">
                                    <asp:Button ID="btnImport" OnClick="btnImportData_Click" class="btn btn-info" runat="server" Text="Import" />
                                </div>
                            </div>
                        </div>      
                    </div>                          
                    <div class="row">
                     <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="dataTable_wrapper"><br />
                                <telerik:RadGrid RenderMode="Lightweight" ID="grdForm"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small" Visible="false"
                            PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdForm_NeedDSource" OnPageIndexChanged="grdForm_PageIndexChanged" AllowPaging="true" PageSize="15">
                            <ClientSettings AllowKeyboardNavigation="true">
                                <Selecting AllowRowSelect="true"></Selecting>
                            </ClientSettings>
                            <MasterTableView AutoGenerateColumns="False" DataKeyNames="No" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="false" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                                <Columns> 
                                    <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex+1 %>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>                                    
                                    <telerik:GridBoundColumn  DataField="Title" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Title" SortExpression="Title" UniqueName="Title" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="InputType" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Input Type" SortExpression="Input Type" UniqueName="InputType" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="ColumnName" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Column Name" SortExpression="ColumnName" UniqueName="ColumnName" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="DisplayLabel" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Display Label" SortExpression="DisplayLabel" UniqueName="DisplayLabel" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>                                           
                                    <telerik:GridBoundColumn  DataField="Content" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Content" SortExpression="Content" UniqueName="Content" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="SortOrder" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Sort Order" SortExpression="SortOrder" UniqueName="SortOrder" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn  DataField="Mandatory" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                        HeaderText="Mandatory" SortExpression="Mandatory" UniqueName="Mandatory" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                    </telerik:GridBoundColumn>                                                                                         
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>      
                            </div>
                        </div>      
                    </div>
                </div>
                <div class="box-footer">                                
                <asp:Button ID="btnSave" OnClick="btnSubmit_Click" class="btn btn-info" runat="server" Text="Submit" visible="false"/>
              </div>
          </div>
        </div>
    </section>
</asp:Content>

