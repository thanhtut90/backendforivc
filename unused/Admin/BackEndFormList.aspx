﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="BackEndFormList.aspx.cs" Inherits="Admin_BackEndFormList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="true" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdCln" />    
        </TargetControls>
    </telerik:RadSkinManager>
     <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">BackEnd Form List</h3>            
            <div class="box-title pull-right"><a href="AddNewFormColumn.aspx"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New Form Column</a>
                &nbsp;&nbsp;
                <a href="BackEndForm.aspx"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New Form</a></div>              
        </div>
        <div class="box-body">
            <div class="form-group">                      
                <div class="col-sm-12">
                <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                </div>   
                </div>
            </div>             
            <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                        <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="true" PageSize="20">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="c_id" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="Editspp" CommandArgument='<%#Eval("c_id") %>' onclick="lnkEdit_Onclick"><i class="fa fa-pencil-square-o"></i></asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                    <asp:LinkButton ID="lnkDelete" runat="server" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("c_id") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>                     
                                </telerik:GridTemplateColumn>                                                                               
                                <telerik:GridTemplateColumn  DataField="c_pid" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Form Name">
                                    <ItemTemplate>
                                            <%# getParentName(Eval("c_pid").ToString()) %>
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>      
                                                          
                                <telerik:GridBoundColumn  DataField="c_label" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Fields" SortExpression="c_label" UniqueName="c_label" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn  DataField="c_inputtype" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Input Type">
                                     <ItemTemplate>
                                            <%# getInputtype(Eval("c_inputtype").ToString()) %>
                                        </ItemTemplate>
                                </telerik:GridTemplateColumn>    
                                <telerik:GridBoundColumn  DataField="c_isRequired" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Is Mandatory" SortExpression="c_isRequired" UniqueName="c_isRequired" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                           
                                <telerik:GridBoundColumn  DataField="c_content" FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Content" SortExpression="c_content" UniqueName="c_content" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith">
                                </telerik:GridBoundColumn>                                                                                  
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    </div>              
                </div>
            </div>
        </div>
     </div>
</asp:Content>

