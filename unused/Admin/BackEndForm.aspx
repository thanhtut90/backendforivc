﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/Site.master" AutoEventWireup="true" CodeFile="BackEndForm.aspx.cs" Inherits="BackEnd_BackEndForm" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
     <script type="text/jscript">
     function NumberOnly(evt) {
         //evt = (evt) ? evt : window.event;
         var charCode = (evt.which) ? evt.which : evt.keyCode;
         if ((charCode >= 48 && charCode <= 57) || (charCode >= 4160 && charCode <= 4169) || charCode == 8 || charCode == 46) {
             return true;
         }
         else {
             return false;
         }
     }
        </script><div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">BackEnd Form Creation</h3>
        </div>
        <div class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Form Type</label>
                    <div class="col-sm-5">                
                        <asp:RadioButtonList ID="rdoftype" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="reference">&nbsp;Reference</asp:ListItem>
                            <asp:ListItem Value="data">&nbsp;Data</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Form Title</label>
                    <div class="col-sm-5">         
                        <asp:HiddenField ID="hfpid" runat="server" />           
                       <asp:TextBox ID="txtftitle" placeholder="Enter Form Title (Long)" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Form Name</label>
                    <div class="col-sm-5">
                    <asp:TextBox ID="txtptitle" placeholder="Enter Form Name (short)" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"><asp:LinkButton runat="server" ID="btnCreate" OnClick="btnCreate_OnClick" class="btn btn-primary"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Create</asp:LinkButton></div>
                </div>
                <div class="form-group">
                     <asp:GridView ID="grdCgrid"  DataKeyNames="c_id" PageSize="30" GridLines="None" visible="false"
                        AutoGenerateColumns="false" runat="server" class="table" Width="100%" 
                        AllowPaging="True" >
                        <Columns>                      
                            <asp:TemplateField HeaderText="ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="txtpid" runat="server" Text='<%# Eval("c_pid") %>'></asp:Label>
                                    <asp:Label ID="txtcid" runat="server" Text='<%# Eval("c_id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                
                            <asp:TemplateField HeaderText="Label" HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <%# Eval("c_label") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Input Type" HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <%# Eval("inputtype")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Content" HeaderStyle-HorizontalAlign="left">
                                <ItemTemplate>
                                    <%# Eval("c_content") %>
                                </ItemTemplate>
                            </asp:TemplateField>             
                        </Columns>
                    </asp:GridView>
                </div>   
                <div id="ct" runat="server" visible="false">

                <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Choose Input Type</label>
                      <div class="col-sm-3">
                        <asp:HiddenField ID="hfcid" runat="server" />
                        <asp:RadioButtonList ID="ctt" OnSelectedIndexChanged="rdolst_SelectedIndexChanged" AutoPostBack="true" runat="server">
                            <asp:ListItem Value="1">&nbsp;TextBox</asp:ListItem>  
                            <asp:ListItem Value="2">&nbsp;Password</asp:ListItem>
                            <asp:ListItem Value="3">&nbsp;Email</asp:ListItem>                       
                            <asp:ListItem Value="4">&nbsp;TextArea</asp:ListItem>
                            <asp:ListItem Value="5">&nbsp;Telerik Editor</asp:ListItem>
                            <asp:ListItem Value="6">&nbsp;RadioButton</asp:ListItem>
                            <asp:ListItem Value="7">&nbsp;RadioButton (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="8">&nbsp;CheckBox</asp:ListItem>
                            <asp:ListItem Value="9">&nbsp;CheckBox List (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="10">&nbsp;Dropdown List</asp:ListItem>
                            <asp:ListItem Value="11">&nbsp;Dropdown List (Dynamic Data Bind)</asp:ListItem>
                            <asp:ListItem Value="12">&nbsp;Datepicker</asp:ListItem>
                            <asp:ListItem Value="13">&nbsp;Fileupload</asp:ListItem> 
                            <asp:ListItem Value="14">&nbsp;Hidden value (Integer)</asp:ListItem>
                            <asp:ListItem Value="15">&nbsp;Hidden Value (Character)</asp:ListItem>
                          </asp:RadioButtonList>
                      </div>
                    <div class="col-sm-6">
                        <div class="form-group" id ="divdt" runat="server" Visible="false">                            
                            <div class="col-sm-6">
                                <label for="inputEmail3">Data Table</label><br />
                                <asp:DropDownList ID="ddlRole1" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlRole1_SelectedIndexChanged">
                                </asp:DropDownList>
                                <%--<asp:DropDownList ID="ddlRole2" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddlRole2_SelectedIndexChanged"></asp:DropDownList>--%>
                            </div>
                          
                        </div>
                        <div class="form-group" id ="divdc" runat="server" Visible="false">                            
                            <div class="col-sm-6">
                                <label for="inputEmail3">Data Column</label><br />
                                <asp:DropDownList ID="ddldcol" runat="server" required="" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="ddldcol_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            
                        </div>
                    </div>
                    </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Is Mandatory</label>
                    <div class="col-sm-6">
                    <asp:CheckBox ID="chkRequired" runat="server" />
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                     <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Data Column Name</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtdatacol" CssClass="form-control" placeholder="Insert Data Column Name" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Label</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtlbl" CssClass="form-control" placeholder="Insert Label Name" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div> 
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sort Order</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="txtso" CssClass="form-control" Width="10%"  runat="server" onkeypress="return NumberOnly(event)"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label"><asp:Label id="lblcontent" runat="server" Visible="false">Content</asp:Label></label>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent1" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>                    
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd1" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd1_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="txtcontent2" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd2" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd2_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent3" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd3" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd3_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent4" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd4" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd4_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-3">
                    <asp:TextBox ID="txtcontent5" CssClass="form-control" placeholder="Insert Content Name" runat="server" Visible="false"></asp:TextBox>  
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btnadd5" class="btn btn-default" runat="server" style="text-align:left" Visible ="false" OnClick="btnadd5_onclick" Text="Add"></asp:Button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-6">
                    <asp:LinkButton ID="btnsave" class="btn btn-primary" style="text-align:left" OnClick="btnsave_OnClick"
                    runat="server"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp; Save</asp:LinkButton>
                    </div>
                    <div class="col-sm-1"></div>
                </div>

                </div>
            </div>
        </div>
        <div class="box-footer">
            <asp:LinkButton ID="btncreateForm" class="btn btn-primary" OnClick="btncreateForm_OnClick" runat="server"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;&nbsp;Create Form</asp:LinkButton>
        </div>
    </div>
        
</asp:Content>

