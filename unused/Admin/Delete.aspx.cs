﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;

public partial class Admin_Delete : System.Web.UI.Page
{
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_FrontEnd dalfp = new DAL_FrontEnd();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            if(Request.QueryString["id"] != null && Request.QueryString["rid"] != null)
            {
                string pid = Request.QueryString["id"].ToString();
                string rowid = Request.QueryString["rid"].ToString();
                int CheckUsed = dalfp.CheckItemUsed(rowid);
                if (CheckUsed == 0)
                {
                    int res = dalfp.DeleteRecord(rowid);
                    if (res == 1)
                    {
                        if (Request.QueryString["id"] != null)
                        {
                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&d=suc");
                        }
                    }
                    else
                    {
                        if (Request.QueryString["id"] != null)
                        {
                            Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&d=fls");
                        }
                    }
                }
                else
                {
                    Response.Redirect("~/Admin/FrontEndForm.aspx?id=" + pid + "&&d=used");
                }
            }
        }
    }
    #region DeleteByRowID(From Edit)
    private bool DeleteByRowID(string parentid, string rid)
    {
        bool Success = false;
        FrontEnd fe = new FrontEnd() { f_pid = parentid, f_c_rowid = rid };
        var lst = dalfp.SelectByPIDRowID(fe);
        int count = 0;
        int totalcount = 0;
        if (lst.Count > 0)
        {
            totalcount = lst.Count();
            foreach (var item in lst)
            {
                int isSuccess = 0;
                string frontID = item.f_id;
                isSuccess = dalfp.Delete(frontID);
                if (isSuccess > 0)
                {
                    count += 1;
                }
            }
        }

        if (count == totalcount)
        {
            Success = true;
        }

        return Success;
    }
    #endregion
}