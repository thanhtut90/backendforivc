﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.DAL;
using App.Common;
using App.Object;
using Telerik.Web.UI;

public partial class Admin_FrontEndForm : System.Web.UI.Page
{
    #region Declaration
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_FrontEnd dalfp = new DAL_FrontEnd();
    DAL_Parent dalp = new DAL_Parent();
    DAL_Child dalc = new DAL_Child();
    ChildModel cmodel = new ChildModel();
    ParentModel pmodel = new ParentModel();
    BackEnd_Parent Pobj = new BackEnd_Parent();
    BackEnd_Child Cobj = new BackEnd_Child();
    DAL dal = new DAL();
    List<BackEnd_Child> ShowHeaderList = new List<BackEnd_Child>();
    int lang = 0;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            if (Request.QueryString["id"] != null)
            {
                if(Request.QueryString["lang"] !=null)
                {
                    lang = int.Parse(Request.QueryString["lang"].ToString());
                }
                BindLanguage();
                chkLang.SelectedValue = "1";
                BindForm(lang);                
                #region Alert Message
                if (Request.QueryString["s"] != null)
                {
                    if (Request.QueryString["s"].ToString() == "suc")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Save Successful!')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Save Fail!')", true);
                    }
                }
                else if (Request.QueryString["u"] != null)
                {
                    if (Request.QueryString["u"].ToString() == "suc")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Update Successful!')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Update Fail!')", true);
                    }
                }
                else if (Request.QueryString["d"] != null)
                {
                    if (Request.QueryString["d"].ToString() == "suc")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Delete Successful!')", true);
                    }
                    else if (Request.QueryString["d"].ToString() == "used")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Delete Fail.This is used in some page!!')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Delete Fail!')", true);
                    }
                }
                else if (Request.QueryString["n"] != null)
                {
                    if (Request.QueryString["n"].ToString() == "suc")
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Nortification Successful!')", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('Nortification Fail!')", true);
                    }
                }
                #endregion               
            }
        }
    }
    #endregion

    #region Bind Form
    public void BindForm(int lang)
    {
        string pid = string.Empty;       
        if(lang !=0)
        {
            chkLang.SelectedValue = lang.ToString();
        }
        else
        {
            lang = 0;
            if (!string.IsNullOrEmpty(chkLang.SelectedValue))
            {
                lang = int.Parse(chkLang.SelectedValue.ToString());
            }
            else
            {
                lang = 1;
            }
        }
       
        
        pid=Request.QueryString["id"].ToString();
        hdPID.Value = pid.ToString();
        ShowHeader(pid);
        BindTableForGrid(pid, lang);
    }
    #endregion

    #region Language
    private void BindLanguage()
    {
        DataTable dt = dal.getLang();
        if (dt.Rows.Count > 0)
        {
            chkLang.DataSource = dt;
            chkLang.DataValueField = "ID";
            chkLang.DataTextField = "Language";
            chkLang.DataBind();
        }
    }
    protected void chkLang_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindForm(lang);
    }
    #endregion

    #region ShowHeader
    private void ShowHeader(string pid)
    {
        try
        {
            ShowHeaderList = dalc.SelectByPId(pid);
            hdRowCount.Text = (ShowHeaderList.Count() + 1).ToString();

            lblformname.Text = dalp.SelectNameByID(pid);
        }
        catch (Exception ex)
        {
        }
    }
    #endregion

    #region DynamicGridHeader
    private void DynamicGridHeader()
    {
        grdDynamic.Columns.Clear();

        BoundField bfield1 = new BoundField();
        bfield1.HeaderText = "No";
        bfield1.DataField = "No";
        bfield1.HeaderStyle.Width = new Unit("5%");
        grdDynamic.Columns.Add(bfield1);
        BoundField bfield2 = new BoundField();
        bfield2.HeaderStyle.Width = new Unit("15%");
        bfield2.HeaderText = "Action";
        bfield2.DataField = "Action";
        grdDynamic.Columns.Add(bfield2);
        foreach (var item in ShowHeaderList)
        {
            BoundField bfield = new BoundField();
            bfield.HeaderText = item.c_label;
            bfield.DataField = item.c_label;            
            grdDynamic.Columns.Add(bfield);
        }

        
    }
    #endregion

    #region BindTableForGrid
    private DataTable BindTableForGrid(string pid,int lang)
    {
        DataTable dt = new DataTable();
        try
        {
            int no = 1;
            
            DataRow dr;
            if (ShowHeaderList.Count > 0)
            {
                int count = ShowHeaderList.Count();
                dt.Columns.Add("No", typeof(string));
                dt.Columns.Add("Action", typeof(string));
                
                foreach (var hd in ShowHeaderList)
                {
                    dt.Columns.Add(hd.c_label, typeof(string));
                }
                
                var lstresult = dalfp.SelectByPId(pid,lang);
                var lstresult1 = dalfp.SelectDistinctRowID(lstresult);

                if (lstresult1.Count > 0)
                {
                    foreach (var en in lstresult1)
                    {
                        var lstresult2 = dalfp.SelectByParRowID(lstresult, pid, en.f_c_rowid);
                        if (lstresult2.Count > 0)
                        {
                            int fieldCount = 0;
                            if (ShowHeaderList.Count > 0)
                            {
                                fieldCount = ShowHeaderList.Count();
                            }

                            #region Real Data into DataTable
                            if (fieldCount > 0)
                            {
                                var lstRealResult = dalfp.SelectByFieldCount(lstresult2, fieldCount);

                                if (lstRealResult.Count > 0)
                                {
                                    string rowid = string.Empty;
                                    foreach (var oneRow in lstRealResult)
                                    {
                                        if (oneRow.Count > 0)
                                        {
                                            dr = dt.NewRow();
                                            dr["No"] = no;
                                            foreach (var chl in oneRow)
                                            {
                                                rowid = chl.f_c_rowid;
                                                //if (chl.f_inputtype == 13)
                                                //{
                                                    
                                                //    if (!string.IsNullOrEmpty(chl.f_content))
                                                //    {
                                                //        string chkcontent = chl.f_content.ToLower();
                                                //        if(chkcontent.Contains("jpg") || chkcontent.Contains("png") || chkcontent.Contains("jpeg"))
                                                //        {
                                                //            string img = "<img src='../Images/FrontEnd_Photos/" + chl.f_content + "' width='150' height='150'/>";                                                            
                                                //            img = img.Replace("'", "\"");
                                                //            dr[chl.f_label] = img;

                                                //        }
                                                //        else
                                                //        {
                                                //            dr[chl.f_label] = chl.f_content;
                                                //        }
                                                //    }
                                                //    else
                                                //    {
                                                //        dr[chl.f_label] = chl.f_content;
                                                //    }
                                                    
                                                    
                                                //}
                                                //else 
                                                if (chl.f_inputtype == 2)
                                                {
                                                    dr[chl.f_label] = "xxxxxx";
                                                }
                                                else if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
                                                {
                                                    if (!string.IsNullOrEmpty(chl.f_content))
                                                    {
                                                        string fcontent = string.Empty;
                                                        if (chl.f_content.Contains(','))
                                                        {
                                                            string[] arr = chl.f_content.Split(',');
                                                            for (int i = 0; i < arr.Count(); i++)
                                                            {
                                                                string fid = arr[i];
                                                                fcontent += dalfp.selectContentByID(fid);
                                                                if (string.IsNullOrEmpty(fcontent))
                                                                {
                                                                    fcontent += dal.getMenuTitleByID(fid);
                                                                }
                                                                if (string.IsNullOrEmpty(fcontent))
                                                                {
                                                                    fcontent += dal.getMenuTitleByID(fid);
                                                                }
                                                                if (i < arr.Count() - 1)
                                                                {
                                                                    fcontent += ",";
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string fid = chl.f_content;
                                                            fcontent = dalfp.selectContentByID(fid);
                                                            if (string.IsNullOrEmpty(fcontent))
                                                            {
                                                                fcontent += dal.getMenuTitleByID(fid.ToString());
                                                            }
                                                            if (string.IsNullOrEmpty(fcontent))
                                                            {
                                                                fcontent += dal.getMenuTitleByID(fid.ToString());
                                                            }
                                                        }

                                                        dr[chl.f_label] = fcontent;
                                                    }
                                                    else
                                                    {
                                                        dr[chl.f_label] = "";
                                                    }
                                                }
                                                else if (chl.f_inputtype == 5)
                                                {
                                                    if (!string.IsNullOrEmpty(chl.f_content))
                                                    {
                                                        string decodedText = HttpUtility.HtmlDecode(chl.f_content);

                                                        dr[chl.f_label] = decodedText;
                                                    }
                                                    else
                                                    {
                                                        dr[chl.f_label] = "";
                                                    }
                                                }
                                                else
                                                {
                                                    dr[chl.f_label] = chl.f_content;
                                                }
                                                

                                            }
                                            no++;
                                            dr["Action"] = rowid;
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
                grdDynamic.DataSource = null;
                DynamicGridHeader();
                grdDynamic.DataSource = dt;
                grdDynamic.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
        return dt;
    }
    #endregion

    #region GridView_RowDataBound
    protected void GridView_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        //***hdPID.Value=BackendParentID
        if(Request.QueryString["id"] !=null)
        {
            lang = int.Parse(chkLang.SelectedValue.ToString());
            int colorder = Convert.ToInt32("1");
            bool chkshow = dalp.SelectById(Request.QueryString["id"].ToString()).p_includePush;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string rowid = e.Row.Cells[colorder].Text;
                if (chkshow == true)
                {
                    e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a runat='server' class='btn btn-success btn-sm'"
                   + "href='PushNortification.aspx?id=" + hdPID.Value + "&&rid=" + rowid
                   + "&&lang=" + lang
                   + "'><i class='fa fa-fw fa-bell-o'></i>Push</a>&nbsp;&nbsp;"));
                }

                e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a runat='server' class='btn btn-info btn-sm'"
                    + "href='FrontEndForm_Add.aspx?id=" + hdPID.Value + "&&rid=" + rowid
                    + "&&lang=" + lang
                    + "'><i class='fa fa-fw fa-pencil'></i>Edit</a>&nbsp;&nbsp;"));
                e.Row.Cells[colorder].Controls.Add(new LiteralControl("<a runat='server' class='btn btn-warning btn-sm'"
                    + "href='Delete.aspx?id=" + hdPID.Value + "&&rid=" + rowid
                    + "'><i class='fa fa-fw fa-trash-o'></i>Delete</a>&nbsp;&nbsp;"));
            }
        }        
    }
    #endregion

    #region grdDynamic_PageIndexChanging
    protected void grdDynamic_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string pid = string.Empty;
        if (Request.QueryString["id"] != null)
        {
            pid = Request.QueryString["id"].ToString();
        }
        grdDynamic.PageIndex = e.NewPageIndex;
        lang = 0;
        if (!string.IsNullOrEmpty(chkLang.SelectedValue))
        {
            lang = int.Parse(chkLang.SelectedValue.ToString());
        }
        else
        {
            lang = 1;
        }
        BindTableForGrid(pid, lang);
        grdDynamic.DataBind();

    }
    #endregion

    #region lnkAdd_OnClick
    protected void lnkAdd_OnClick(object sender, EventArgs e)
    {
        string pid = string.Empty;
        if (Request.QueryString["id"] != null)// && Request.QueryString["t"]!=null
        {
            lang = Convert.ToInt32(chkLang.SelectedValue.ToString());
            pid = Request.QueryString["id"].ToString();
            Response.Redirect("~/Admin/FrontEndForm_Add.aspx?id=" + pid+"&lang="+lang);
        }
    }
    #endregion

    #region lnkDelete_OnClick NOT USE => Delete.aspx
    protected void GridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            string rowid = e.CommandArgument.ToString();
            int CheckUsed = dalfp.CheckItemUsed(rowid);
            if (CheckUsed == 0)
            {
                int res = dalfp.DeleteRecord(rowid);
                if (res == 1)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int pid = Convert.ToInt32(Request.QueryString["id"].ToString());
                        Response.Redirect("~/Admin/FrontEnd.aspx?id=" + pid);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('This cannot be deleted. This is used in some page!')", true);
            }
        }
    }
    protected void lnkDelete_OnClick(object sender, EventArgs e)
    {
        LinkButton del = sender as LinkButton;
        if (del.CommandName == "Delete")
        {
            string rowid = del.CommandArgument.ToString();
            int CheckUsed = dalfp.CheckItemUsed(rowid);
            if (CheckUsed == 0)
            {
                int res = dalfp.DeleteRecord(rowid);
                if (res == 1)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int pid = Convert.ToInt32(Request.QueryString["id"].ToString());
                        Response.Redirect("~/Admin/FrontEnd.aspx?id=" + pid);
                    }
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "msg", @"alert('This cannot be deleted. This is used in some page!')", true);
            }
            
        }
    }
    #endregion

    #region lnkDownload_OnClick
    protected void lnkDownload_OnClick(object sender, EventArgs e)
    {
        if(Request.QueryString["id"] != null)
        {            
            string pid = Request.QueryString["id"].ToString();
            ShowHeader(pid);
            lang = 0;
            if (!string.IsNullOrEmpty(chkLang.SelectedValue))
            {
                lang = int.Parse(chkLang.SelectedValue.ToString());
            }
            else
            {
                lang = 1;
            }

            DataTable dt = dalfp.BindTableForGridforDownload(pid,lang);
            
            string pname = dalp.SelectNameByID(pid);   
            pname= pname.Replace(" ", "");
            dalfp.DataTableToExcel(dt, pname);
        }
    }
    #endregion

    #region btnSearch_Click
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtKeyword.Text.Trim()))
        {
            string pid = string.Empty; ; string searchkey = string.Empty;
            if (Request.QueryString["id"] != null)
            {
                pid=Request.QueryString["id"].ToString();
                searchkey = txtKeyword.Text.Trim();
                List<FrontEnd> lstplan = new List<FrontEnd>();
                List<FrontEnd> lstplanT = new List<FrontEnd>();
                //plan A
                lstplan = dalfp.selectByPIDContent(pid, searchkey);
                if(lstplan.Count==0)
                {
                    //plan B
                    string _f_id = dalfp.selectIDbyContent(searchkey);                    
                    
                    if(!string.IsNullOrEmpty(_f_id))
                    {
                        List<string> lstrowid = dalfp.selectRowIDByContent(_f_id);
                        foreach(var r in lstrowid)
                        {
                            lstplanT = dalfp.SelectByRowidPiD(r, pid);
                            foreach(var l in lstplanT)
                            {
                                if (!lstplan.Contains(l))
                                {
                                    lstplan.Add(l);
                                }
                            }
                        }
                    }
                }
                BindTableForGridForSearch(lstplan);

            }
        }
    }
    #endregion

    #region BindTableForGridForSearch
    private DataTable BindTableForGridForSearch(List<FrontEnd> lstresult)
    {
        DataTable dt = new DataTable();
        try
        {
            string pid = string.Empty;
            pid=Request.QueryString["id"].ToString();
            ShowHeader(pid);
            int no = 1;

            DataRow dr;
            if (ShowHeaderList.Count > 0)
            {
                int count = ShowHeaderList.Count();
                dt.Columns.Add("No", typeof(string));
                dt.Columns.Add("Action", typeof(string));

                foreach (var hd in ShowHeaderList)
                {
                    dt.Columns.Add(hd.c_label, typeof(string));
                }

                if (lstresult.Count > 0)
                {
                    int fieldCount = 0;
                    if (ShowHeaderList.Count > 0)
                    {
                        fieldCount = ShowHeaderList.Count();
                    }

                    #region Real Data into DataTable
                    if (fieldCount > 0)
                    {
                        var lstRealResult = dalfp.SelectByFieldCount(lstresult, fieldCount);

                        if (lstRealResult.Count > 0)
                        {
                            string rowid = string.Empty;
                            foreach (var oneRow in lstRealResult)
                            {
                                if (oneRow.Count > 0)
                                {
                                    dr = dt.NewRow();
                                    dr["No"] = no;
                                    foreach (var chl in oneRow)
                                    {
                                        rowid = chl.f_c_rowid;
                                        if (chl.f_inputtype == 13)
                                        {

                                            if (!string.IsNullOrEmpty(chl.f_content))
                                            {
                                                if (chl.f_content.Contains("jpg") || chl.f_content.Contains("png") || chl.f_content.Contains("jpeg"))
                                                {
                                                    string img = "<img src='../Images/FrontEnd_Photos/" + chl.f_content + "' width='150' height='150'/>";
                                                    //string img = "<a href='../Images/FrontEnd_Photos/" + chl.f_content + "' target='_blank'>View</a>";
                                                    img = img.Replace("'", "\"");
                                                    dr[chl.f_label] = img;

                                                }
                                                else
                                                {
                                                    dr[chl.f_label] = chl.f_content;
                                                }
                                            }
                                            else
                                            {
                                                dr[chl.f_label] = chl.f_content;
                                            }


                                        }
                                        else if (chl.f_inputtype == 2)
                                        {
                                            dr[chl.f_label] = "xxxxxx";
                                        }
                                        else if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
                                        {
                                            if (!string.IsNullOrEmpty(chl.f_content))
                                            {
                                                string fid = chl.f_content;
                                                string fcontent = dalfp.selectContentByID(fid);
                                                dr[chl.f_label] = fcontent;
                                            }
                                            else
                                            {
                                                dr[chl.f_label] = "";
                                            }
                                        }
                                        else
                                        {
                                            dr[chl.f_label] = chl.f_content;
                                        }


                                    }
                                    no++;
                                    dr["Action"] = rowid;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                    }
                    #endregion
                }
                grdDynamic.DataSource = null;
                DynamicGridHeader();
                grdDynamic.DataSource = dt;
                grdDynamic.DataBind();
            }
        }
        catch (Exception ex)
        {
        }
        return dt;
    }
    #endregion
    
}