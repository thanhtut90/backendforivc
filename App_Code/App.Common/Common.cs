﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using App.Object;
using App.DAL;
using App.Object;

using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.Reflection;

namespace App.Common
{

    public static class Common
    {

        static readonly string PasswordHash = "P@@Sw0rd";
        static readonly string SaltKey = "S@LT&KEY";
        static readonly string VIKey = "@1B2c3D4e5F6g7H8";
        public static string emailURL = "http://203.127.83.146/StandardEmailPortal/MailRequest";

        #region ChangeNumberEngToMyanmar
        public static string ChangeNumberEngToMyanmar(string str)
        {
            string data = "";
            try
            {
                for (int i = 0; i < str.Length; i++)
                {
                    switch (str[i].ToString())
                    {
                        case "1": data += "၁"; break;
                        case "2": data += "၂"; break;
                        case "3": data += "၃"; break;
                        case "4": data += "၄"; break;
                        case "5": data += "၅"; break;
                        case "6": data += "၆"; break;
                        case "7": data += "၇"; break;
                        case "8": data += "၈"; break;
                        case "9": data += "၉"; break;
                        case "0": data += "၀"; break;
                        default: data += str[i].ToString(); break;
                    }
                }
            }
            catch { }
            return data;
        }
        #endregion

        #region CreateFileWithUniqueName
        public static string CreateFileWithUniqueName(string folder, string fileName)
        {
            string str = "";
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");

            try
            {
                var fileBase = Path.GetFileNameWithoutExtension(fileName);
                var ext = Path.GetExtension(fileName);

                string[] files = Directory.GetFiles(folder);

                foreach (string file in files)
                {
                    DataRow dr = dt.NewRow();
                    dr["Name"] = Path.GetFileName(file);
                    dt.Rows.Add(dr);
                }

                DataRow[] dr_data = dt.Select("Name like'" + fileBase + "%'");

                if (dr_data.Length > 1)
                {
                    for (int i = dr_data.Length; i >= 1; i--)
                    {
                        var check_ext = Path.GetExtension(dr_data[i - 1]["Name"].ToString());
                        var check_fileBase = Path.GetFileNameWithoutExtension(dr_data[i - 1]["Name"].ToString());

                        if (check_fileBase.Contains('_'))
                        {
                            int index = check_fileBase.LastIndexOf("_");
                            check_fileBase = check_fileBase.Substring(index);
                            string[] s = check_fileBase.Split('_');
                            if (s.Length > 0)
                            {
                                int count = Convert.ToInt32(s[1]) + 1;
                                str = fileBase + "_" + count + ext;
                                break;
                            }
                        }
                        else
                        {
                            str = fileBase + "_1" + ext;
                        }

                    }
                }
                else if (dr_data.Length == 1)
                {
                    str = fileBase + "_1" + ext;
                }
                else
                {
                    str = fileBase + ext;
                }
            }
            catch { }
            return str;
        }
        #endregion

        #region GetMonths
        public static DataTable GetMonths()
        {
            DataRow dr;
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");
            try
            {
                dr = dt.NewRow();
                dr["ID"] = 0;
                dr["Name"] = "--- Choose Month ---";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Name"] = "January";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 2;
                dr["Name"] = "February";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 3;
                dr["Name"] = "March";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 4;
                dr["Name"] = "April";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 5;
                dr["Name"] = "May";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 6;
                dr["Name"] = "June";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 7;
                dr["Name"] = "July";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 8;
                dr["Name"] = "August";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 9;
                dr["Name"] = "September";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 10;
                dr["Name"] = "October";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 11;
                dr["Name"] = "November";
                dt.Rows.Add(dr);

                dr = dt.NewRow();
                dr["ID"] = 12;
                dr["Name"] = "December";
                dt.Rows.Add(dr);
            }
            catch { }
            return dt;
        }
        #endregion

        #region generatePager
        public static List<ListItem> generatePager(int totalRowCount, int pageSize, int currentPage)
        {
            int totalLinkInPage = 5;
            int totalPageCount = (int)Math.Ceiling((decimal)totalRowCount / pageSize);

            int startPageLink = Math.Max(currentPage - (int)Math.Floor((decimal)totalLinkInPage / 2), 1);
            int lastPageLink = Math.Min(startPageLink + totalLinkInPage - 1, totalPageCount);

            if ((startPageLink + totalLinkInPage - 1) > totalPageCount)
            {
                lastPageLink = Math.Min(currentPage + (int)Math.Floor((decimal)totalLinkInPage / 2), totalPageCount);
                startPageLink = Math.Max(lastPageLink - totalLinkInPage + 1, 1);
            }

            List<ListItem> pageLinkContainer = new List<ListItem>();

            if (startPageLink != 1)
                pageLinkContainer.Add(new ListItem("&laquo;", "1", currentPage != 1));
            for (int i = startPageLink; i <= lastPageLink; i++)
            {
                pageLinkContainer.Add(new ListItem(i.ToString(), i.ToString(), currentPage != i));
            }
            if (lastPageLink != totalPageCount)
                pageLinkContainer.Add(new ListItem("&raquo;", totalPageCount.ToString(), currentPage != totalPageCount));

            return pageLinkContainer;
        }
        #endregion

        #region ChangeFormatMontDayYear
        public static string ChangeFormatMontDayYear(string date)
        {
            string rtnDate = "";
            try
            {
                string[] str = date.Split('/');
                if (str.Length > 0)
                {
                    rtnDate = (str[1] + "/" + str[0] + "/" + str[2]).ToString();
                }
            }
            catch { }
            return rtnDate;
        }
        #endregion

        #region ChangeFormatDayMonthYear
        public static string ChangeFormatDayMonthYear(string date)
        {
            string rtnDate = "";
            try
            {
                string[] str = date.Split(' ');
                string[] str1 = str[0].Split('/');
                if (str1.Length > 0)
                {
                    string year = str1[2].ToString();
                    string month = (Convert.ToInt32(str1[0]) < 10) ? str1[0].ToString() : str1[0].ToString();
                    string day = (Convert.ToInt32(str1[1]) < 10) ? str1[1].ToString() : str1[1].ToString();

                    rtnDate = (day + "/" + month + "/" + year + " " + str[1] + " " + str[2]).ToString();
                }
            }
            catch { }
            return rtnDate;
        }
        #endregion

        #region ChangeFormatYearMonthDay
        public static string ChangeFormatYearMonthDay(string date)
        {
            string rtnDate = "";
            try
            {
                string[] str = date.Split('/');
                if (str.Length > 0)
                {
                    rtnDate = (str[1] + "/" + str[0] + "/" + str[2]).ToString();
                }
            }
            catch { }
            return rtnDate;
        }
        #endregion

        #region BindFromMonths
        public static DataTable BindFromMonths()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["Name"] = "From Month";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Name"] = "January";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 2;
            dr["Name"] = "February";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 3;
            dr["Name"] = "March";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 4;
            dr["Name"] = "April";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 5;
            dr["Name"] = "May";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 6;
            dr["Name"] = "June";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 7;
            dr["Name"] = "July";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 8;
            dr["Name"] = "August";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 9;
            dr["Name"] = "September";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 10;
            dr["Name"] = "October";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 11;
            dr["Name"] = "November";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 12;
            dr["Name"] = "December";
            dt.Rows.Add(dr);

            return dt;
        }
        #endregion

        #region BindToMonths
        public static DataTable BindToMonths()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");

            DataRow dr = dt.NewRow();
            dr["ID"] = 0;
            dr["Name"] = "To Month";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 1;
            dr["Name"] = "January";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 2;
            dr["Name"] = "February";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 3;
            dr["Name"] = "March";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 4;
            dr["Name"] = "April";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 5;
            dr["Name"] = "May";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 6;
            dr["Name"] = "June";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 7;
            dr["Name"] = "July";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 8;
            dr["Name"] = "August";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 9;
            dr["Name"] = "September";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 10;
            dr["Name"] = "October";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 11;
            dr["Name"] = "November";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["ID"] = 12;
            dr["Name"] = "December";
            dt.Rows.Add(dr);

            return dt;
        }
        #endregion

        #region getMonthName
        public static string getMonthName(int month)
        {
            string str = "";
            switch (month)
            {
                case 1: str = "Jan"; break;
                case 2: str = "Feb"; break;
                case 3: str = "Mar"; break;
                case 4: str = "Apr"; break;
                case 5: str = "May"; break;
                case 6: str = "Jun"; break;
                case 7: str = "July"; break;
                case 8: str = "Aug"; break;
                case 9: str = "Sept"; break;
                case 10: str = "Oct"; break;
                case 11: str = "Nov"; break;
                case 12: str = "Dec"; break;
            }
            return str;
        }
        #endregion

        #region Encrypt
        public static string Encrypt(string plainText)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes).Replace("/","^").Replace("+","!");
        }
        #endregion

        #region Decrypt
        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText.Replace("^","/").Replace("!","+"));
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
        #endregion

        #region ConvertListToDataTable
        public static DataTable ToDataTable<T>(List<T> items)
        {

            DataTable dataTable = new DataTable(typeof(T).Name);

            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)
            {

                //Setting column names as Property names

                dataTable.Columns.Add(prop.Name, prop.PropertyType);

            }

            foreach (T item in items)
            {

                var values = new object[Props.Length];

                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }

                dataTable.Rows.Add(values);

            }
            return dataTable;

        }
        #endregion

        #region ConvertDataTableToList
        /*Converts DataTable To List*/
        public static List<T> ToList<T>(DataTable dataTable) where T : new()
        {
            var dataList = new List<T>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in typeof(T).GetProperties(flags)
                                 select new
                                 {
                                     Name = aProp.Name,
                                     Type = Nullable.GetUnderlyingType(aProp.PropertyType) ?? aProp.PropertyType
                                 }).ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                                     select new { Name = aHeader.ColumnName, Type = aHeader.DataType }).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();
            
            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var aT = new T();
                foreach (var aField in commonFields)
                {
                    PropertyInfo propertyInfos = aT.GetType().GetProperty(aField.Name);
                    propertyInfos.SetValue(aT, dataRow[aField.Name], null);
                }
                dataList.Add(aT);
            }
            return dataList;
        }
        #endregion

    }
}