﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Global
/// </summary>
public class Global
{
    static string _appkey;
    static string _masterscret;
    static string _isProduction;
    static string _NotificationTile;
    static string _EventDuration;

    public static string AppKey
    {
        get
        {
            return _appkey;
        }
        set
        {
            _appkey = value;
        }
    }

    public static string MasterScretKey
    {
        get
        {
            return _masterscret;
        }
        set
        {
            _masterscret = value;
        }
    }

    public static string isProduction
    {
        get
        {
            return _isProduction;
        }
        set
        {
            _isProduction = value;
        }
    }
    public static string NotificationTile
    {
        get
        {
            return _NotificationTile;
        }
        set
        {
            _NotificationTile = value;
        }
    }
    public static string EventDuration
    {
        get
        {
            return _EventDuration;
        }
        set
        {
            _EventDuration = value;
        }
    }
}