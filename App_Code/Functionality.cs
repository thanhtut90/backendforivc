﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Linq;

public class Functionality
{
    CultureInfo provider = CultureInfo.GetCultureInfo("en-GB");

    public Functionality()
	{
	//
	// TODO: Add constructor logic here
	//
	}
    string ConnectionString = ConfigurationManager.ConnectionStrings["CMSConnString"].ConnectionString;
    private int mint_CommandTimeout = 30;
    private SqlConnection mobj_SqlConnection;
    private SqlCommand mobj_SqlCommand;
    private string mstr_ConnectionString;

    public string replaceForSQL(string str)
    {
        return str.Replace("'", "");
           
    }

    public string solveSQL(string str)
    {
        return str.Replace("'", "''");
    }

    public void ExecuteSQL(string sCommand)
    {
        try
        {
            mstr_ConnectionString = ConnectionString;

            mobj_SqlConnection = new SqlConnection(mstr_ConnectionString);
            mobj_SqlCommand = new SqlCommand();
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
            mobj_SqlCommand.Connection = mobj_SqlConnection;

            mobj_SqlCommand.CommandText = sCommand;
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;

            mobj_SqlConnection.Open();

            mobj_SqlCommand.ExecuteNonQuery();

            //DataSet ds = new DataSet();
            //adpt.Fill(ds, sDatatable);
            //return ds;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            CloseConnection();
        }
    }

    public Int32 ExecuteSQLGetID(string sCommand)
    {
        using (var con = new SqlConnection(ConnectionString))
        {
            int newID;
            var cmd = sCommand;
            using (var insertCommand = new SqlCommand(cmd, con))
            {
                insertCommand.Parameters.AddWithValue("@Value", "bar");
                con.Open();
                newID = (int)insertCommand.ExecuteScalar();
            }

            return newID;
        }
    }

    public string ExecuteSQL2(string sCommand)
    {
        try
        {
            string id = "";
            mstr_ConnectionString = ConnectionString;

            mobj_SqlConnection = new SqlConnection(mstr_ConnectionString);
            mobj_SqlCommand = new SqlCommand();
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
            mobj_SqlCommand.Connection = mobj_SqlConnection;

            mobj_SqlCommand.CommandText = sCommand;
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;

            mobj_SqlConnection.Open();

            id = mobj_SqlCommand.ExecuteNonQuery().ToString();
            return id;

            //DataSet ds = new DataSet();
            //adpt.Fill(ds, sDatatable);
            //return ds;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            CloseConnection();
        }
            
    }

    public String CreateRandomCode(int passwordLength) //generate random characters
    {
        string allowedChars = "ABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
        char[] chars = new char[passwordLength];
        Random rd = new Random();

        for (int i = 0; i < passwordLength; i++)
        {
            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
        }

        return new string(chars);
    }
    public DataSet GetDatasetByCommand(string sCommand, string sDatatable)
    {
        try
        {
            mstr_ConnectionString = ConnectionString;

            mobj_SqlConnection = new SqlConnection(mstr_ConnectionString);
            mobj_SqlCommand = new SqlCommand(); 
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
            mobj_SqlCommand.Connection = mobj_SqlConnection;

            mobj_SqlCommand.CommandText = sCommand;
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;

            mobj_SqlConnection.Open();

            SqlDataAdapter adpt = new SqlDataAdapter(mobj_SqlCommand);
            DataSet ds = new DataSet();
            adpt.Fill(ds, sDatatable);
            return ds;
        }
        catch (System.Data.SqlClient.SqlException ex)
        {
            throw ex;
        }
        finally
        {
            CloseConnection();
        }
    }
    public int ExecuteNonQuery(string str, SqlParameter[] prams)
    {
        int ret;
        mstr_ConnectionString = ConnectionString;
        using (SqlConnection connection = new SqlConnection(mstr_ConnectionString))
        {
            SqlCommand cmd = new SqlCommand(str, connection);

            cmd.Parameters.AddRange(prams);
            try
            {

                if (connection.State == System.Data.ConnectionState.Closed)
                {
                    connection.Open();
                }
                else if (connection.State == System.Data.ConnectionState.Broken)
                {
                    connection.Close();
                    connection.Open();
                }

                ret = cmd.ExecuteNonQuery();
            }
            catch (SqlException E)
            {
                throw new Exception(E.Message);
            }
            finally
            {
                cmd.Dispose();
                connection.Close();
            }
        }
        return ret;
    }
    public String encrypt(String str, int key)
    {
        String result = "";
        for (int i = 0; i < str.Length; i++)
        {
            if ((int)str[i] + key > 126)
                result = result + Convert.ToChar((((int)str[i] + key) - 127) + 32);//((int (word[x]) + key) - 127) + 32
            else
                result = result + Convert.ToChar(((int)str[i] + key));
        }
        return result;
    }

    public String CreateRandomNumber(int passwordLength)
    {
        string allowedChars = "0123456789";
        char[] chars = new char[passwordLength];
        Random rd = new Random();

        for (int i = 0; i < passwordLength; i++)
        {
            chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
        }

        return new string(chars);
    }

    public void CloseConnection()
    {
        if (mobj_SqlConnection.State != ConnectionState.Closed) mobj_SqlConnection.Close();
    }

    public String GetDataByCommand(string Command, string FieldName)
    {
        try
        {
            mstr_ConnectionString = ConnectionString;

            mobj_SqlConnection = new SqlConnection(mstr_ConnectionString);
            mobj_SqlCommand = new SqlCommand();
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;
            mobj_SqlCommand.Connection = mobj_SqlConnection;

            mobj_SqlCommand.CommandText = Command;
            mobj_SqlCommand.CommandTimeout = mint_CommandTimeout;

            mobj_SqlConnection.Open();

            SqlDataReader reader = mobj_SqlCommand.ExecuteReader();
            string svalue = "";
            if (reader.Read())
            {
                svalue = reader[FieldName].ToString();
            }
            else
            {
                svalue = "0";
            }
            return svalue;
                
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            CloseConnection();
        }
    }

    public bool IsValidEmail(string email)
    {
        //regular expression pattern for valid email
        string pattern = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        //Regular expression object
        System.Text.RegularExpressions.Regex check = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
        bool valid = false;

        if (string.IsNullOrEmpty(email))
        {
            valid = true;
        }
        else
        {
            valid = check.IsMatch(email);
        }
        return valid;
    }

    public bool IsValidNumeric(string sValue)
    {
        //regular expression pattern for valid email
        string pattern = @"^\d+$";
        //Regular expression object
        System.Text.RegularExpressions.Regex check = new System.Text.RegularExpressions.Regex(pattern, System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace);
        bool valid = false;

        if (string.IsNullOrEmpty(sValue))
        {
            valid = true;
        }
        else
        {
            valid = check.IsMatch(sValue);
        }
        return valid;
    }

    public string getDecryptURL(string sParam)
    {
        string sEncoded = sParam.Replace("'", "");
        sEncoded = sEncoded.Replace("||", "+");
        sParam = Core_App.RijndaelSimple.Decrypt(sEncoded, "Pas5pr@se", "s@1tValue", "MD5", 2, "@1B2c3D4e5F6g7H8", 256);
        return sParam;
    }

    public string setEncryptURL(string sParam)
    {
        string sEncoded = sParam.Replace("'", "");
        sParam = Core_App.RijndaelSimple.Encrypt(sEncoded, "Pas5pr@se", "s@1tValue", "MD5", 2, "@1B2c3D4e5F6g7H8", 256);
        return sParam.Replace("+", "||");
    }

    
    public void binddropdown(DropDownList ddl, string dataKeyValue, string dataKeyText, string dstable, string ordercolumn)
    {
        DataSet ds = new DataSet();
        ds = GetDatasetByCommand("Select * from " + dstable + " order by " + ordercolumn, "ds");
        if (ds.Tables[0].Rows.Count != 0)
        {
            ddl.Items.Add("--Please Select--");
            ddl.Items[0].Value = "0.0";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                ddl.Items.Add(ds.Tables[0].Rows[i][dataKeyText].ToString());
                ddl.Items[i + 1].Value = ds.Tables[0].Rows[i][dataKeyValue].ToString();
            }
        }

    }

    public bool IsFaculty(string UserCategory)
    {
        bool IsFac = false;
        if (UserCategory == GetConfigAppValue("FacultyValue"))
            IsFac = true;
        return IsFac;
    }

    public string GetConfigAppValue(string Key)
    {
        string value = "";
        try
        {
            value = ConfigurationManager.AppSettings[Key];
        }
        catch (Exception ex) { }
        return value;
    }

    public bool IsCategorySpeaker(string category)
    {
        Boolean isFault = false;
        try
        {
            if (category != null)
            {
                string configvalue = GetConfigAppValue("SpeakerCategory");
                if (configvalue.Contains(category))
                {
                    isFault = true;
                }
            }
        }
        catch (Exception) { }

        return isFault;
    }

    public Boolean isvisitor(string regno)
    {
        Boolean visitor = false;
        //int catid = Convert.ToInt16(GetDataByCommand("select con_CategoryId from tb_registration where Regno=" + regno,"con_CategoryId"));
        //if (catid == -1)
        //{
        //    visitor = true;
        //}
        return visitor;
    }

    public int GetBadgeCount(string userid)
    {
        int badgecount = 0;

        DataSet ds = new DataSet();
        //ds = GetDatasetByCommand("Select * from TB_BADGE where Regno=" + userid, "dsds");
        ds = GetDatasetByCommand("Select * from tb_Invoice where GroupRegID=" + userid, "dsds");

        badgecount = ds.Tables[0].Rows.Count;

        return badgecount;

    }

    public string GetCountryName(string countrycode)
    {
        string countryname = "";

        countryname = GetDataByCommand("Select countryen from tb_country where countrycode = '" + countrycode.Trim() + "' ", "countryen");

        return countryname;
    }


    public Boolean validateDate(string date)
    {
        Boolean msg = true;

        //Array monthDays = new Array[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        int[] monthDays = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        string dateValue = date;

        if (dateValue.Length != 10)
        {
            msg = false;

            return msg;
        }

        // dd/mm/yyyy format... 
        var valueArray = dateValue.Split('/');

        if (valueArray.Length != 3)
        {
            msg = false;

            return msg;
        }

        string monthValue = valueArray[1];
        string dayValue = valueArray[0];
        string yearValue = valueArray[2];

        if ((monthValue.All(Char.IsDigit) == false) || (dayValue.All(Char.IsDigit) == false) || (yearValue.All(Char.IsDigit) == false))
        {
            msg = false;

            return msg;
        }

        Double yearValue_1 = Convert.ToDouble(yearValue);
        Double monthValue_1 = Convert.ToDouble(monthValue);
        Double dayValue_1 = Convert.ToDouble(dayValue);
        if (((yearValue_1 % 4) == 0) && (((yearValue_1 % 100) != 0) || ((yearValue_1 % 400) == 0)))
            monthDays[1] = 29;
        else
            monthDays[1] = 28;

        if ((monthValue_1 < 1) || (monthValue_1 > 12))
        {
            msg = false;

            return msg;
        }

        Int32 monthDaysArrayIndex = Convert.ToInt32(monthValue_1 - 1);
        if ((dayValue_1 < 1) || (dayValue_1 > monthDays[monthDaysArrayIndex]))
        {
            msg = false;

            return msg;
        }

        return msg;
    }
    public DateTime ParseDateTime(string sParam)
    {
        DateTime rtnValue = DateTime.Now;
        try
        {
            rtnValue = DateTime.Parse(sParam);
        }
        catch { rtnValue = DateTime.Parse("01/01/1999"); }
        return rtnValue;
    }

}