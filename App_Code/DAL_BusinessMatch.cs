﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for DAL_BusinessMatch
/// </summary>
public class DAL_BusinessMatch
{
    Functionality fn = new Functionality();
    public DAL_BusinessMatch()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public string getTopExhList(string regno)
    {
        string topExhList = string.Empty;
        if (!string.IsNullOrEmpty(regno))
        {
            string checkBusMatch = "Select * From tb_BusinessMatchReult Where VisitorCode='" + regno + "'";
            DataTable dtBusMatch = fn.GetDatasetByCommand(checkBusMatch, "ds").Tables[0];
            int busCount = 1;
            if (dtBusMatch.Rows.Count == 0)
            {
                string sql = "select ExhName ,count(*) total "
                                + " from tb_ExhibitorProduct t where  EXISTS "
                                + " (select * from tb_RegistrationProducts where VisitorRegNo='" + regno + "' and EPCode=Pcode ) group by ExhName "
                                + " order by total desc, NEWID()";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //insert and get top 10 exhibitors into business match
                    busCount = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (busCount <= 10)
                        {
                            string exhName = dr["ExhName"].ToString();

                            //***insert into tb_BusinessMatchReult table
                            string insertBusMatch = string.Format("Insert Into tb_BusinessMatchReult (ExhName, VisitorCode) Values ('{0}', '{1}')", exhName, regno);
                            fn.ExecuteSQL(insertBusMatch);
                            //***insert into tb_BusinessMatchReult table

                            //***get exhibitor name and booth no.
                            DataTable dtExh = fn.GetDatasetByCommand("Select * From tb_ExhibitorProduct Where ExhName Like '" + exhName + "'", "dsExh").Tables[0];
                            if (dtExh.Rows.Count > 0)
                            {
                                topExhList += dtExh.Rows[0]["Booth"].ToString() + "!" + replaceSpecialCharacter(dtExh.Rows[0]["ExhName"].ToString()) + "~";
                            }
                            //***get exhibitor name and booth no.
                        }
                        else
                        {
                            break;
                        }

                        busCount++;
                    }
                }
            }
            else
            {
                DataTable dtExhList = fn.GetDatasetByCommand("Select * From tb_BusinessMatchReult Where VisitorCode='" + regno + "'", "dsExh").Tables[0];
                if(dtExhList.Rows.Count > 0)
                {
                     busCount = 1;
                    foreach (DataRow dr in dtExhList.Rows)
                    {
                        string exhName = dr["ExhName"].ToString();

                        if (busCount <= 10)
                        {
                            //***get exhibitor name and booth no.
                            DataTable dtExh = fn.GetDatasetByCommand("Select * From tb_ExhibitorProduct Where ExhName Like '" + exhName + "'", "dsExh").Tables[0];
                            if (dtExh.Rows.Count > 0)
                            {
                                topExhList += dtExh.Rows[0]["Booth"].ToString() + "!" + replaceSpecialCharacter(dtExh.Rows[0]["ExhName"].ToString()) + "~";
                            }
                            //***get exhibitor name and booth no.
                        }
                        else
                        {
                            break;
                        }

                        busCount++;
                    }
                }
            }

            while (busCount <= 10)
            {
                topExhList += ".!.~";
                busCount++;
            }
        }

        topExhList = topExhList.TrimEnd('~');

        return topExhList;
    }

    public DataTable getTopExhListDataTable(string regno)
    {
        DataTable topExhList = new DataTable();
        topExhList.Columns.Add("Booth");
        topExhList.Columns.Add("ExhibitorName");
        if (!string.IsNullOrEmpty(regno))
        {
            string checkBusMatch = "Select * From tb_BusinessMatchReult Where VisitorCode='" + regno + "'";
            DataTable dtBusMatch = fn.GetDatasetByCommand(checkBusMatch, "ds").Tables[0];
            int busCount = 1;
            if (dtBusMatch.Rows.Count == 0)
            {
                string sql = "select ExhName ,count(*) total "
                                + " from tb_ExhibitorProduct t where  EXISTS "
                                + " (select * from tb_RegistrationProducts where VisitorRegNo='" + regno + "' and EPCode=Pcode ) group by ExhName "
                                + " order by total desc, NEWID()";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    //insert and get top 10 exhibitors into business match
                    busCount = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (busCount <= 10)
                        {
                            string exhName = dr["ExhName"].ToString();

                            //***insert into tb_BusinessMatchReult table
                            string insertBusMatch = string.Format("Insert Into tb_BusinessMatchReult (ExhName, VisitorCode) Values ('{0}', '{1}')", exhName, regno);
                            fn.ExecuteSQL(insertBusMatch);
                            //***insert into tb_BusinessMatchReult table

                            //***get exhibitor name and booth no.
                            DataTable dtExh = fn.GetDatasetByCommand("Select * From tb_ExhibitorProduct Where ExhName Like '" + exhName + "'", "dsExh").Tables[0];
                            if (dtExh.Rows.Count > 0)
                            {
                                DataRow drTopExhList = topExhList.NewRow();
                                drTopExhList["Booth"] = dtExh.Rows[0]["Booth"].ToString();
                                drTopExhList["ExhibitorName"] = dtExh.Rows[0]["ExhName"].ToString();
                                topExhList.Rows.Add(drTopExhList);
                            }
                            //***get exhibitor name and booth no.
                        }
                        else
                        {
                            break;
                        }

                        busCount++;
                    }
                }
            }
            else
            {
                DataTable dtExhList = fn.GetDatasetByCommand("Select * From tb_BusinessMatchReult Where VisitorCode='" + regno + "'", "dsExh").Tables[0];
                if (dtExhList.Rows.Count > 0)
                {
                    busCount = 1;
                    foreach (DataRow dr in dtExhList.Rows)
                    {
                        string exhName = dr["ExhName"].ToString();

                        if (busCount <= 10)
                        {
                            //***get exhibitor name and booth no.
                            DataTable dtExh = fn.GetDatasetByCommand("Select * From tb_ExhibitorProduct Where ExhName Like '" + exhName + "'", "dsExh").Tables[0];
                            if (dtExh.Rows.Count > 0)
                            {
                                DataRow drTopExhList = topExhList.NewRow();
                                drTopExhList["Booth"] = dtExh.Rows[0]["Booth"].ToString();
                                drTopExhList["ExhibitorName"] = dtExh.Rows[0]["ExhName"].ToString();
                                topExhList.Rows.Add(drTopExhList);
                            }
                            //***get exhibitor name and booth no.
                        }
                        else
                        {
                            break;
                        }

                        busCount++;
                    }
                }
            }

            while (busCount <= 10)
            {
                busCount++;
            }
        }

        return topExhList;
    }

    #region replaceSpecialCharacter
    private string replaceSpecialCharacter(string strValue)
    {
        string result = strValue;
        try
        {
            //result = Regex.Replace(strValue, @"([^a-zA-Z0-9\s@&()':.,/_-]|^)", string.Empty);
            result = Regex.Replace(strValue, @"([^a-zA-Z0-9\s@&().,/-]|^)", string.Empty);
        }
        catch (Exception ex)
        { }

        return result;
    }
    #endregion
}