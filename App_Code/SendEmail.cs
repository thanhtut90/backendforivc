﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using App.Common;

/// <summary>
/// Summary description for SendEmail
/// </summary>
public class SendEmail
{
    /*
    string param = "@ExhCompany|" + ExhCompany + "^@Day|" + DateTitle + "^@Time|" + TimeTitle;
    string EmailType="P1";// P1,P2,T1,T2
    se.SendEmailViaPortal(Eid.ToString(), email, param,EmailType);
    */
    public string SendEmailViaPortal(string userID, string email, string parameters, string EmailType, string attachment,string ccemail)
    {
        string status = string.Empty;
        //var httpWebRequest = (HttpWebRequest)WebRequest.Create(Common.emailURL);
        //httpWebRequest.ContentType = "application/json";
        //httpWebRequest.Method = "POST";

        //ArrayList maineventList = new ArrayList();
        //Hashtable htm = new Hashtable();
        //ArrayList eventList = new ArrayList();
        //ArrayList attachmentlist = new ArrayList();
        //ArrayList cclist = new ArrayList();
        //if (EmailType == "ACC")// Account Login Forgot Password Credentials Email
        //{
        //    htm.Add("ERefKey", "STDMOBAPP10001");//RefKey from Email Portal
        //}
        //else if (EmailType == "CREATEACC")// Account Login Credentials Email
        //{
        //    htm.Add("ERefKey", "STDMOBAPP10002");//RefKey from Email Portal
        //}
        //htm.Add("ShowID", "18");
        ////htm.Add("RefRegno", userID);
        //htm.Add("To", email);
        ////if(!string.IsNullOrEmpty(ccemail))
        ////{
        ////    htm.Add("CC", ccemail);
        ////}
        //#region Parameters
        //string para = string.Empty, fieldname = string.Empty, value = string.Empty;
        //if (!string.IsNullOrEmpty(parameters))
        //{
        //    string[] arrpara = parameters.Split('^');
        //    int count = arrpara.Count();
        //    for (int i = 0; i < count; i++)
        //    {
        //        //@companyname|ABC Company^@Date|2374
        //        para = arrpara[i].Split('|')[0];
        //        value = arrpara[i].Split('|')[1];
        //        var key = para.Replace("@", "");
        //        Hashtable hts = new Hashtable();
        //        hts.Add("Key", key);
        //        hts.Add("PValue", value);
        //        eventList.Add(hts);
        //    }
        //}

        //#endregion

        //htm.Add("Pramz", eventList);
        //if (!string.IsNullOrEmpty(attachment))
        //{
        //    Hashtable hta = new Hashtable();
        //    hta.Add("Path", attachment);
        //    attachmentlist.Add(hta);
        //}
        //if (!string.IsNullOrEmpty(ccemail))
        //{
        //    Hashtable hta = new Hashtable();
        //    hta.Add("Email", ccemail);
        //    cclist.Add(hta);
        //}

        //htm.Add("Attachz", attachmentlist);
        //System.Web.Script.Serialization.JavaScriptSerializer ser = new System.Web.Script.Serialization.JavaScriptSerializer();
        //String jsonStr = ser.Serialize(htm);


        //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        //{
        //    streamWriter.Write(jsonStr);
        //    streamWriter.Flush();
        //    streamWriter.Close();
        //}

        //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
        //{
        //    var result = streamReader.ReadToEnd();
        //    result = result.Split('}')[0];
        //    result = result.Replace("[{", "");
        //    status = result.Split(':')[1].Replace('"', ' ').Trim();
        //}
        return status;
    }

    #region sEmailSendInterestedExhList
    public string sEmailSendInterestedExhList(string username, string interestedExhibitorList, string emailaddress)
    {
        string isSuccess = "F";
        try
        {
            Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
            Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
            string template = HttpContext.Current.Server.MapPath("~/Template/EmailTemplate_InterestedExhibitorList.html");
            var tokenValues = new Dictionary<string, string>
            {
                {"username",username}, 
                {"exhibitorlist",interestedExhibitorList}
            };
            MailMessage message = factory
            .WithTokenValues(tokenValues)
            .WithHtmlBodyFromFile(template)
            .Create();

            var from = new MailAddress("corpitmail@event-admin.biz", "Bex");
            message.From = from;
            message.To.Add(emailaddress);
            message.Subject = "Interested Exhibitor List";
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
            SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
            System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("corpitmail@event-admin.biz", "c0rpitblasT");
            emailClient.UseDefaultCredentials = false;
            emailClient.EnableSsl = true;
            emailClient.Credentials = SMTPUserInfo;
            emailClient.Port = 587;//***
            emailClient.Send(message);

            isSuccess = "T";
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
    #endregion

    #region sEmailSendAPI
    public string sEmailSendAPI(string emailTitle, string emailContent, string emailaddress, string replyemail)
    {
        string isSuccess = "F";
        try
        {
            if (!string.IsNullOrEmpty(emailaddress))
            {
                Alpinely.TownCrier.TemplateParser tm = new Alpinely.TownCrier.TemplateParser();
                Alpinely.TownCrier.MergedEmailFactory factory = new Alpinely.TownCrier.MergedEmailFactory(tm);
                var tokenValues = new Dictionary<string, string>
                {
                    //{"fullname",name},
                    //{"productinfo",productinfo},
                };
                MailMessage message = factory
                .WithTokenValues(tokenValues).WithHtmlBody(emailContent)
                .Create();
                /*.WithHtmlBodyFromFile(emailContent)*/

                //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;//***
                System.Net.ServicePointManager.SecurityProtocol = (System.Net.SecurityProtocolType)3072;//***

                var from = new MailAddress("corpitmail@event-admin.biz", "CET2019");
                message.From = from;
                message.To.Add(emailaddress);
                message.Subject = emailTitle;
                if (!string.IsNullOrEmpty(replyemail))
                {
                    message.ReplyToList.Add(replyemail);
                }
                SmtpClient emailClient = new SmtpClient("smtp.gmail.com");
                System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("corpitmail@event-admin.biz", "c0rpitblasT");
                emailClient.UseDefaultCredentials = false;
                emailClient.EnableSsl = true;
                emailClient.Port = 587;
                emailClient.Credentials = SMTPUserInfo;
                emailClient.Send(message);

                isSuccess = "T";
            }
        }
        catch (Exception ex)
        {
            isSuccess = ex.Message;// "F";
        }
        return isSuccess;
    }
    #endregion
}