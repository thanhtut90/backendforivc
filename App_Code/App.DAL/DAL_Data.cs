﻿using App.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace App.DAL
{
    public class DAL_Data
    {
        Functionality fn = new Functionality();
        public DAL_Data()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable Login(string username,string password)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = string.Format("select * from Administrator where admin_username='{0}' and admin_password='{1}' and admin_isdeleted=0",username,password);
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            }
            catch(Exception ex) { }
            return dt;
        }

        #region ConvertToTimestamp
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public string ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return elapsedTime.TotalSeconds.ToString();
        }
        #endregion
        public DataTable GetColumnNamesByTable(string tablename)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("SELECT COLUMN_NAME");
                str.Append(" FROM INFORMATION_SCHEMA.COLUMNS");
                str.Append(" WHERE TABLE_NAME = N'" + tablename + "' and COLUMN_NAME not in ('deleteFlag','UpdatedDate')");

                dt = fn.GetDatasetByCommand(str.ToString(), "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceForPolling()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConference where c_Polling=1 and deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceForSurvey()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConference where c_Survey=1 and deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceForLiveQA()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConference where c_LiveQA=1 and deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getsponsorcategory()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblSponsorCategory where deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getsponsorcategoryNameByID(string id)
        {
            string spnid = string.Empty;
            try
            {
                spnid = fn.GetDataByCommand("select sponsorCat_name from tblSponsorCategory where deleteFlag=0 And sponsorCat_ID='" + id + "'", "sponsorCat_name");
            }
            catch (Exception ex) { }
            return spnid;
        }
        public string getsponsorcategoryIDByName(string name)
        {
            string spnid = string.Empty;
            try
            {
                spnid = fn.GetDataByCommand("select sponsorCat_ID from tblSponsorCategory where deleteFlag=0 And sponsorCat_name='" + name + "'", "sponsorCat_ID");
            }
            catch (Exception ex) { }
            return spnid;
        }
        public int SaveSponsorCategoryGetID(DataRow dr, ref string spncatID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SPOC");
                ID = "SPOC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSponsorCategory ");
                str.Append("(sponsorCat_ID,sponsorCat_name,sponsorCat_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@sponsorCat_ID,@sponsorCat_name,@sponsorCat_seq,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("sponsorCat_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sponsorCat_name",SqlDbType.NVarChar),
                                            new SqlParameter("sponsorCat_seq",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                spncatID = ID;
                parms[0].Value = ID;
                parms[1].Value = dr["sponsorCat_name"].ToString();
                parms[2].Value = dr["sponsorCat_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SPOC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getMainConf()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConference where c_parentConfID='' and deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable selectSponsornMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_PageType='Sponsor' and Status <> 'Deleted'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getmaingeneralinfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select gi_ID,gi_title,gi_content,gi_seq,gi_mainID,m.m_TitleEn as menuID from tblGeneralInfo g left join tb_Menu m on g.menuID=m.m_id where g.deleteFlag=0 and g.gi_mainID=''", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable selectGeneralInfoMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_PageType='MoreInformation' and Status <> 'Deleted'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int UpdateEvent(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {

                StringBuilder str = new StringBuilder();
                str.Append("update tblEvent set ");
                str.Append(" e_image = @e_image,e_date = @e_date,e_time = @e_time,e_title = @e_title,e_venue = @e_venue,e_brief = @e_brief,e_eventcategory=@e_eventcategory,lang = @lang,deleteFlag = @deleteFlag,UpdatedDate = @UpdatedDate");
                str.Append(" where e_ID=@e_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("e_ID",SqlDbType.NVarChar),
                                    new SqlParameter("e_image",SqlDbType.NVarChar),
                                    new SqlParameter("e_date",SqlDbType.NVarChar),
                                    new SqlParameter("e_time",SqlDbType.NVarChar),
                                    new SqlParameter("e_title",SqlDbType.NVarChar),
                                    new SqlParameter("e_venue",SqlDbType.NVarChar),
                                    new SqlParameter("e_brief",SqlDbType.NVarChar),
                                    new SqlParameter("e_eventcategory",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["e_ID"].ToString();
                parms[1].Value = dr["e_image"].ToString();
                parms[2].Value = dr["e_date"].ToString();
                parms[3].Value = dr["e_time"].ToString();
                parms[4].Value = dr["e_title"].ToString();
                parms[5].Value = dr["e_venue"].ToString();
                parms[6].Value = dr["e_brief"].ToString();
                parms[7].Value = dr["e_eventcategory"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;
                
                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable selectFloorplanMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_PageType='Floorplan' and Status <> 'Deleted'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable selectMenuByID(string menuid)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_id='" + menuid + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteMenuByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tb_Menu set Status='Deleted' where m_id=@m_id";
                SqlParameter[] parms = { new SqlParameter("@m_id", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getHall()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select f.l_ID,f.l_name,f.l_image,m.m_TitleEn as menuID from tblLocation_Floorplan f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0", "sdt").Tables[0];
            }catch(Exception ex) { }
            return dt;
        }

        public int UpdateConference(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {

                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConference SET ");
                str.Append(" c_cc = @c_cc,c_date = @c_date,c_stime = @c_stime,c_etime = @c_etime,c_title = @c_title,c_venue = @c_venue,c_brief = @c_brief,c_liveQA = @c_liveQA,c_parentConfID = @c_parentConfID,lang = @lang,deleteFlag = @deleteFlag,UpdatedDate = @UpdatedDate,c_polling=@c_polling,c_survey=@c_survey,c_TimeZone=@c_TimeZone,ZoomId=@ZoomId,c_Password=@c_Password,ConfigId=@ConfigId");
                str.Append(" WHERE c_ID=@c_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("c_ID",SqlDbType.NVarChar),
                                    new SqlParameter("c_cc",SqlDbType.NVarChar),
                                    new SqlParameter("c_date",SqlDbType.NVarChar),
                                    new SqlParameter("c_stime",SqlDbType.NVarChar),
                                    new SqlParameter("c_etime",SqlDbType.NVarChar),
                                    new SqlParameter("c_title",SqlDbType.NVarChar),
                                    new SqlParameter("c_venue",SqlDbType.NVarChar),
                                    new SqlParameter("c_brief",SqlDbType.NVarChar),
                                    new SqlParameter("c_liveQA",SqlDbType.Bit),
                                    new SqlParameter("c_parentConfID",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("c_polling",SqlDbType.NVarChar),
                                    new SqlParameter("c_survey",SqlDbType.NVarChar),
                                    new SqlParameter("c_TimeZone",SqlDbType.NVarChar),
                                    new SqlParameter("ZoomId",SqlDbType.NVarChar),
                                    new SqlParameter("c_Password",SqlDbType.NVarChar),
                                    new SqlParameter("ConfigId",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["c_ID"].ToString();
                parms[1].Value = dr["c_cc"].ToString();
                parms[2].Value = dr["c_date"].ToString();
                parms[3].Value = dr["c_stime"].ToString();
                parms[4].Value = dr["c_etime"].ToString();
                parms[5].Value = dr["c_title"].ToString();
                parms[6].Value = dr["c_venue"].ToString();
                parms[7].Value = dr["c_brief"].ToString();
                parms[8].Value = !string.IsNullOrEmpty(dr["c_liveQA"].ToString()) ? (dr["c_liveQA"].ToString() == "1" ? true : false) : false;
                parms[9].Value = dr["c_parentConfID"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;
                parms[13].Value = !string.IsNullOrEmpty(dr["c_polling"].ToString()) ? (dr["c_polling"].ToString() == "1" ? true : false) : false;
                parms[14].Value = !string.IsNullOrEmpty(dr["c_survey"].ToString()) ? (dr["c_survey"].ToString() == "1" ? true : false) : false;
                parms[15].Value = dr["c_TimeZone"].ToString();
                parms[16].Value = dr["ZoomId"].ToString();
                parms[17].Value = dr["c_Password"].ToString();
                parms[18].Value = dr["ConfigId"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public int SaveEvent(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("E");
                ID = "E" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblEvent ");
                str.Append("(e_ID,e_image,e_date,e_time,e_title,e_venue,e_brief,e_eventcategory,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@e_ID,@e_image,@e_date,@e_time,@e_title,@e_venue,@e_brief,@e_eventcategory,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                    new SqlParameter("e_ID",SqlDbType.NVarChar),
                                    new SqlParameter("e_image",SqlDbType.NVarChar),
                                    new SqlParameter("e_date",SqlDbType.NVarChar),
                                    new SqlParameter("e_time",SqlDbType.NVarChar),
                                    new SqlParameter("e_title",SqlDbType.NVarChar),
                                    new SqlParameter("e_venue",SqlDbType.NVarChar),
                                    new SqlParameter("e_brief",SqlDbType.NVarChar),
                                    new SqlParameter("e_eventcategory",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),


            };
                parms[0].Value = ID;
                parms[1].Value = dr["e_image"].ToString();
                parms[2].Value = dr["e_date"].ToString();
                parms[3].Value = dr["e_time"].ToString();
                parms[4].Value = dr["e_title"].ToString();
                parms[5].Value = dr["e_venue"].ToString();
                parms[6].Value = dr["e_brief"].ToString();
                parms[7].Value = dr["e_eventcategory"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("E", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getEventbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select e.e_ID,e.e_brief,e.e_date,e.e_image,e.e_time,e.e_title,vb.vb_name as e_venue from tblEvent e left join tblVenue_Booth vb on e.e_venue=vb.vb_ID where e.deleteFlag=0 and e_title like '%" + searchkey + "%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int UpdateBaristas(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblBaristas SET ");
                str.Append(" s_fullname = @s_fullname,s_job = @s_job,s_company = @s_company,s_email = @s_email,s_mobile = @s_mobile,s_address = @s_address,s_country = @s_country,s_bio = @s_bio,s_profilepic = @s_profilepic,lang = @lang,deleteFlag = @deleteFlag,UpdatedDate = @UpdatedDate ");
                str.Append(" WHERE s_ID=@s_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("s_job",SqlDbType.NVarChar),
                                    new SqlParameter("s_company",SqlDbType.NVarChar),
                                    new SqlParameter("s_email",SqlDbType.NVarChar),
                                    new SqlParameter("s_mobile",SqlDbType.NVarChar),
                                    new SqlParameter("s_address",SqlDbType.NVarChar),
                                    new SqlParameter("s_country",SqlDbType.NVarChar),
                                    new SqlParameter("s_bio",SqlDbType.NVarChar),
                                    new SqlParameter("s_profilepic",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["s_ID"].ToString();
                parms[1].Value = dr["s_fullname"].ToString();
                parms[2].Value = dr["s_job"].ToString();
                parms[3].Value = dr["s_company"].ToString();
                parms[4].Value = dr["s_email"].ToString();
                parms[5].Value = dr["s_mobile"].ToString();
                parms[6].Value = dr["s_address"].ToString();
                parms[7].Value = dr["s_country"].ToString();
                parms[8].Value = dr["s_bio"].ToString();
                parms[9].Value = dr["s_profilepic"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getEvent()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select e.e_ID,e.e_brief,e.e_date,e.e_image,e.e_time,e.e_title,vb.vb_name as e_venue from tblEvent e left join tblVenue_Booth vb on e.e_venue=vb.vb_ID where e.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getEventByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblEvent where deleteFlag = 0 and e_ID='" + id + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getEventIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("SELECT e_ID FROM tblEvent where deleteFlag=0 and e_title='" + name + "'", "e_ID");
            }
            catch (Exception ex) { }
            return result;
        }

        public int DeleteEventByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblEvent set deleteFlag=1 where e_ID=@e_ID";
                SqlParameter[] parms = { new SqlParameter("@e_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public int SaveConference(DataRow dr, ref string ID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty;
                fkey = gk.getKey("C");
                ID = "C" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConference ");
                str.Append("(c_ID,c_cc,c_date,c_stime,c_etime,c_title,c_venue,c_brief,c_liveQA,c_parentConfID,lang,deleteFlag,UpdatedDate,c_polling,c_survey,c_TimeZone,ZoomId,c_Password,ConfigId)");
                str.Append(" values");
                str.Append("(@c_ID,@c_cc,@c_date,@c_stime,@c_etime,@c_title,@c_venue,@c_brief,@c_liveQA,@c_parentConfID,@lang,@deleteFlag,@UpdatedDate,@c_polling,@c_survey,@c_TimeZone,@ZoomId,@c_Password,@ConfigId)");

                SqlParameter[] parms = {
                                    new SqlParameter("c_ID",SqlDbType.NVarChar),
                                    new SqlParameter("c_cc",SqlDbType.NVarChar),
                                    new SqlParameter("c_date",SqlDbType.NVarChar),
                                    new SqlParameter("c_stime",SqlDbType.NVarChar),
                                    new SqlParameter("c_etime",SqlDbType.NVarChar),
                                    new SqlParameter("c_title",SqlDbType.NVarChar),
                                    new SqlParameter("c_venue",SqlDbType.NVarChar),
                                    new SqlParameter("c_brief",SqlDbType.NVarChar),
                                    new SqlParameter("c_liveQA",SqlDbType.Bit),
                                    new SqlParameter("c_parentConfID",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("c_polling",SqlDbType.NVarChar),
                                    new SqlParameter("c_survey",SqlDbType.NVarChar),
                                    new SqlParameter("c_TimeZone",SqlDbType.NVarChar),
                                    new SqlParameter("ZoomId",SqlDbType.NVarChar),
                                    new SqlParameter("c_Password",SqlDbType.NVarChar),
                                    new SqlParameter("ConfigId",SqlDbType.NVarChar)
                                };
                parms[0].Value = ID;
                parms[1].Value = dr["c_cc"].ToString();
                parms[2].Value = dr["c_date"].ToString();
                parms[3].Value = dr["c_stime"].ToString();
                parms[4].Value = dr["c_etime"].ToString();
                parms[5].Value = dr["c_title"].ToString();
                parms[6].Value = dr["c_venue"].ToString();
                parms[7].Value = dr["c_brief"].ToString();
                parms[8].Value = !string.IsNullOrEmpty(dr["c_liveQA"].ToString()) ? (dr["c_liveQA"].ToString() == "1" ? true : false) : false;
                parms[9].Value = dr["c_parentConfID"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;
                parms[13].Value = !string.IsNullOrEmpty(dr["c_polling"].ToString()) ? (dr["c_polling"].ToString() == "1" ? true : false) : false;
                parms[14].Value = !string.IsNullOrEmpty(dr["c_survey"].ToString()) ? (dr["c_survey"].ToString() == "1" ? true : false) : false;
                parms[15].Value = dr["c_TimeZone"].ToString();
                parms[16].Value = dr["ZoomId"].ToString();
                parms[17].Value = dr["c_Password"].ToString();
                parms[18].Value = dr["ConfigId"].ToString();
                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("C", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public int UpdateSpeaker(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblSpeaker SET ");
                str.Append(" s_fullname = @s_fullname,s_job = @s_job,s_company = @s_company,s_email = @s_email,s_mobile = @s_mobile,s_address = @s_address,s_country = @s_country,s_bio = @s_bio,s_profilepic = @s_profilepic,lang = @lang,deleteFlag = @deleteFlag,UpdatedDate = @UpdatedDate ");
                str.Append(" WHERE s_ID=@s_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("s_job",SqlDbType.NVarChar),
                                    new SqlParameter("s_company",SqlDbType.NVarChar),
                                    new SqlParameter("s_email",SqlDbType.NVarChar),
                                    new SqlParameter("s_mobile",SqlDbType.NVarChar),
                                    new SqlParameter("s_address",SqlDbType.NVarChar),
                                    new SqlParameter("s_country",SqlDbType.NVarChar),
                                    new SqlParameter("s_bio",SqlDbType.NVarChar),
                                    new SqlParameter("s_profilepic",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["s_ID"].ToString();
                parms[1].Value = dr["s_fullname"].ToString();
                parms[2].Value = dr["s_job"].ToString();
                parms[3].Value = dr["s_company"].ToString();
                parms[4].Value = dr["s_email"].ToString();
                parms[5].Value = dr["s_mobile"].ToString();
                parms[6].Value = dr["s_address"].ToString();
                parms[7].Value = dr["s_country"].ToString();
                parms[8].Value = dr["s_bio"].ToString();
                parms[9].Value = dr["s_profilepic"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getConferencebyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("select c.c_ID,c.c_brief,cc.cc_categoryname as c_cc,c.c_date,c.c_stime,c.c_etime,c.c_liveQA,c.c_title,ccc.c_title as c_parentConfID,vb.vb_name as c_venue from tblConference c");
                str.Append(" left join tblVenue_Booth vb on c.c_venue=vb.vb_ID");
                str.Append(" left join tblConferenceCategory cc on c.c_cc=cc.cc_ID");
                str.Append(" left join tblConference ccc on c.c_parentConfID=ccc.c_ID");
                str.Append(" where c.deleteFlag=0 and c.c_title like '%"+searchkey+"%'");
                dt = fn.GetDatasetByCommand(str.ToString(), "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getConference()
        {
            DataTable dt = new DataTable();
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("select c.c_ID,c.c_brief,cc.cc_categoryname as c_cc,c.c_date,c.c_stime,c.c_etime,c.c_liveQA,c.c_title,ccc.c_title as c_parentConfID,vb.vb_name as c_venue,c.updatedDate from tblConference c");
                str.Append(" left join tblVenue_Booth vb on c.c_venue=vb.vb_ID");
                str.Append(" left join tblConferenceCategory cc on c.c_cc=cc.cc_ID");
                str.Append(" left join tblConference ccc on c.c_parentConfID=ccc.c_ID");
                str.Append(" where c.deleteFlag=0 Order By c.c_title Asc");
                dt = fn.GetDatasetByCommand(str.ToString(), "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getConferenceByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblConference where deleteFlag = 0 and c_ID='" + id + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int SaveBaristas(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("BRT");
                ID = "BRT" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblBaristas ");
                str.Append("(s_ID,s_fullname,s_job,s_company,s_email,s_mobile,s_address,s_country,s_bio,s_profilepic,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@s_ID,@s_fullname,@s_job,@s_company,@s_email,@s_mobile,@s_address,@s_country,@s_bio,@s_profilepic,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("s_job",SqlDbType.NVarChar),
                                    new SqlParameter("s_company",SqlDbType.NVarChar),
                                    new SqlParameter("s_email",SqlDbType.NVarChar),
                                    new SqlParameter("s_mobile",SqlDbType.NVarChar),
                                    new SqlParameter("s_address",SqlDbType.NVarChar),
                                    new SqlParameter("s_country",SqlDbType.NVarChar),
                                    new SqlParameter("s_bio",SqlDbType.NVarChar),
                                    new SqlParameter("s_profilepic",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["s_fullname"].ToString();
                parms[2].Value = dr["s_job"].ToString();
                parms[3].Value = dr["s_company"].ToString();
                parms[4].Value = dr["s_email"].ToString();
                parms[5].Value = dr["s_mobile"].ToString();
                parms[6].Value = dr["s_address"].ToString();
                parms[7].Value = dr["s_country"].ToString();
                parms[8].Value = dr["s_bio"].ToString();
                parms[9].Value = dr["s_profilepic"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("BRT", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public int DeleteConferenceByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConference set deleteFlag=1 where c_ID=@c_ID";
                SqlParameter[] parms = { new SqlParameter("@c_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getBooth()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select vb_ID,vb_name,l.l_name as vb_location,vb_width,vb_height,vb_x,vb_y,vb_ID+'/'+l_image as IDs from tblVenue_Booth b left join tblLocation_Floorplan l on b.vb_location=l.l_ID where b.deleteFlag = 0 Order By vb_name,l.l_name", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getBoothByHallID(string hallid)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select vb_ID,vb_name,l.l_name as vb_location,vb_width,vb_height,vb_x,vb_y from tblVenue_Booth b left join tblLocation_Floorplan l on b.vb_location=l.l_ID where b.deleteFlag = 0 and b.vb_location='"+hallid+"' Order By vb_name,l.l_name", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSpeaker()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblSpeaker where deleteFlag = 0 Order By s_FullName Asc", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int UpdateSponsor(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {

                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblSponsor SET ");
                str.Append("s_exhID = @s_exhID,menuID = @menuID,s_name = @s_name,s_content = @s_content,s_logo = @s_logo,s_category = @s_category,s_seq = @s_seq,lang = @lang,deleteFlag = @deleteFlag,UpdatedDate = @UpdatedDate");
                str.Append(" WHERE s_ID=@s_ID");                

                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_exhID",SqlDbType.NVarChar),
                                    new SqlParameter("menuID",SqlDbType.NVarChar),
                                    new SqlParameter("s_name",SqlDbType.NVarChar),
                                    new SqlParameter("s_content",SqlDbType.NVarChar),
                                    new SqlParameter("s_logo",SqlDbType.NVarChar),
                                    new SqlParameter("s_category",SqlDbType.NVarChar),
                                    new SqlParameter("s_seq",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),

            };
                parms[0].Value = dr["s_ID"].ToString();
                parms[1].Value = dr["s_exhID"].ToString();
                parms[2].Value = dr["menuID"].ToString();
                parms[3].Value = dr["s_name"].ToString();
                parms[4].Value = dr["s_content"].ToString();
                parms[5].Value = dr["s_logo"].ToString();
                parms[6].Value = dr["s_category"].ToString();
                parms[7].Value = dr["s_seq"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getBaristasbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblBaristas where deleteFlag = 0 and s_fullname like '%" + searchkey + "%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getBaristas()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblBaristas where deleteFlag = 0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getBaristasByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblBaristas where deleteFlag = 0 and s_ID='" + id + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSpeakerByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblSpeaker where deleteFlag = 0 and s_ID='"+id+"'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getSpeakerIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("SELECT s_ID FROM tblSpeaker where deleteFlag=0 and s_fullname='" + name + "'", "s_ID");
            }
            catch (Exception ex) { }
            return result;
        }
        public int DeleteBaristasByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblBaristas set deleteFlag=1 where s_ID=@s_ID";
                SqlParameter[] parms = { new SqlParameter("@s_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public string getBaristasIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("SELECT s_ID FROM tblBaristas where deleteFlag=0 and s_fullname='" + name + "'", "s_ID");
            }
            catch (Exception ex) { }
            return result;
        }

        public int DeleteSpeakerByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblSpeaker set deleteFlag=1 where s_ID=@s_ID";
                SqlParameter[] parms = { new SqlParameter("@s_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public int DeleteSpeakerImageByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblSpeaker set s_profilepic='' where s_ID=@s_ID";
                SqlParameter[] parms = { new SqlParameter("@s_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getSpeakerbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("SELECT * FROM tblSpeaker where deleteFlag = 0 and s_fullname like '%" + searchkey+"%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int SaveSpeaker(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("S");
                ID = "S" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSpeaker ");
                str.Append("(s_ID,s_fullname,s_job,s_company,s_email,s_mobile,s_address,s_country,s_bio,s_profilepic,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@s_ID,@s_fullname,@s_job,@s_company,@s_email,@s_mobile,@s_address,@s_country,@s_bio,@s_profilepic,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("s_job",SqlDbType.NVarChar),
                                    new SqlParameter("s_company",SqlDbType.NVarChar),
                                    new SqlParameter("s_email",SqlDbType.NVarChar),
                                    new SqlParameter("s_mobile",SqlDbType.NVarChar),
                                    new SqlParameter("s_address",SqlDbType.NVarChar),
                                    new SqlParameter("s_country",SqlDbType.NVarChar),
                                    new SqlParameter("s_bio",SqlDbType.NVarChar),
                                    new SqlParameter("s_profilepic",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["s_fullname"].ToString();
                parms[2].Value = dr["s_job"].ToString();
                parms[3].Value = dr["s_company"].ToString();
                parms[4].Value = dr["s_email"].ToString();
                parms[5].Value = dr["s_mobile"].ToString();
                parms[6].Value = dr["s_address"].ToString();
                parms[7].Value = dr["s_country"].ToString();
                parms[8].Value = dr["s_bio"].ToString();
                parms[9].Value = dr["s_profilepic"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("S", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getSubProduct()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblProduct where deleteFlag=0 and p_mainproductID <>'' And p_mainproductID Is Not Null", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int UpdateGeneralInfo(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblGeneralInfo set ");
                str.Append(" gi_title=@gi_title,gi_content=@gi_content,gi_icon=@gi_icon,gi_seq=@gi_seq,gi_mainID=@gi_mainID,menuID=@menuID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate");
                str.Append(" where gi_ID=@gi_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("gi_ID",SqlDbType.NVarChar),
                                    new SqlParameter("gi_title",SqlDbType.NVarChar),
                                    new SqlParameter("gi_content",SqlDbType.NVarChar),
                                    new SqlParameter("gi_icon",SqlDbType.NVarChar),
                                    new SqlParameter("gi_seq",SqlDbType.NVarChar),
                                    new SqlParameter("gi_mainID",SqlDbType.NVarChar),
                                    new SqlParameter("menuID",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["gi_ID"].ToString();
                parms[1].Value = dr["gi_title"].ToString();
                parms[2].Value = dr["gi_content"].ToString();
                parms[3].Value = dr["gi_icon"].ToString();
                parms[4].Value = dr["gi_seq"].ToString();
                parms[5].Value = dr["gi_mainID"].ToString();
                parms[6].Value = dr["menuID"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = dr["deleteFlag"].ToString();
                parms[9].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getSponsorByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblSponsor where deleteFlag=0 and s_ID='"+id+"'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getSponsorIDByName(string name)
        {
            string result=string.Empty;
            try
            {
                result = fn.GetDataByCommand("select s_ID from tblSponsor where deleteFlag=0 and s_name='" + name + "'", "s_ID");
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getSponsorbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select s_ID,s_exhID,m.m_TitleEn,s_content,s_logo,sc.sponsorCat_name,s_seq from tblSponsor s left join tblSponsorCategory sc on s.s_category= sc.sponsorCat_ID left join tb_Menu m on s.menuID=m.m_id where s.deleteFlag=0 and s_name like '%"+ searchkey + "%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int DeleteSponsorByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblSponsor set deleteFlag=1 where s_ID=@s_ID";
                SqlParameter[] parms = { new SqlParameter("@s_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getSponsor()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select s_ID,s_exhID,m.m_TitleEn,s_content,s_logo,sc.sponsorCat_name,s_seq,s_name from tblSponsor s left join tblSponsorCategory sc on s.s_category= sc.sponsorCat_ID left join tb_Menu m on s.menuID=m.m_id where s.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int SaveSponsor(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SPN");
                ID = "SPN" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSponsor ");
                str.Append("(s_ID,s_exhID,menuID,s_name,s_content,s_logo,s_category,s_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@s_ID,@s_exhID,@menuID,@s_name,@s_content,@s_logo,@s_category,@s_seq,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("s_ID",SqlDbType.NVarChar),
                                    new SqlParameter("s_exhID",SqlDbType.NVarChar),
                                    new SqlParameter("menuID",SqlDbType.NVarChar),
                                    new SqlParameter("s_name",SqlDbType.NVarChar),
                                    new SqlParameter("s_content",SqlDbType.NVarChar),
                                    new SqlParameter("s_logo",SqlDbType.NVarChar),
                                    new SqlParameter("s_category",SqlDbType.NVarChar),
                                    new SqlParameter("s_seq",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),

            };
                parms[0].Value = ID;
                parms[1].Value = dr["s_exhID"].ToString();
                parms[2].Value = dr["menuID"].ToString();
                parms[3].Value = dr["s_name"].ToString();
                parms[4].Value = dr["s_content"].ToString();
                parms[5].Value = dr["s_logo"].ToString();
                parms[6].Value = dr["s_category"].ToString();
                parms[7].Value = dr["s_seq"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;




                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SPN", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public int UpdateBooth(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblVenue_Booth set");
                str.Append(" vb_name=@vb_name,vb_location=@vb_location,vb_width=@vb_width,vb_height=@vb_height,vb_x=@vb_x,vb_y=@vb_y,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate");
                str.Append(" where vb_ID=@vb_ID");


                SqlParameter[] parms = {
                                    new SqlParameter("vb_ID",SqlDbType.NVarChar),
                                    new SqlParameter("vb_name",SqlDbType.NVarChar),
                                    new SqlParameter("vb_location",SqlDbType.NVarChar),
                                    new SqlParameter("vb_width",SqlDbType.NVarChar),
                                    new SqlParameter("vb_height",SqlDbType.NVarChar),
                                    new SqlParameter("vb_x",SqlDbType.NVarChar),
                                    new SqlParameter("vb_y",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),


            };
                parms[0].Value = dr["vb_ID"].ToString();
                parms[1].Value = dr["vb_name"].ToString();
                parms[2].Value = dr["vb_location"].ToString();
                parms[3].Value = dr["vb_width"].ToString();
                parms[4].Value = dr["vb_height"].ToString();
                parms[5].Value = dr["vb_x"].ToString();
                parms[6].Value = dr["vb_y"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = dr["deleteFlag"].ToString();
                parms[9].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateBoothCoordinator(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblVenue_Booth set ");
                str.Append("vb_width=@vb_width,vb_height=@vb_height,vb_x=@vb_x,vb_y=@vb_y,UpdatedDate=@UpdatedDate");
                str.Append(" where vb_ID=@vb_ID");


                SqlParameter[] parms = {
                                    new SqlParameter("vb_ID",SqlDbType.NVarChar),
                                    new SqlParameter("vb_width",SqlDbType.NVarChar),
                                    new SqlParameter("vb_height",SqlDbType.NVarChar),
                                    new SqlParameter("vb_x",SqlDbType.NVarChar),
                                    new SqlParameter("vb_y",SqlDbType.NVarChar),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),


            };
                parms[0].Value = dr["vb_ID"].ToString();
                parms[1].Value = dr["vb_width"].ToString();
                parms[2].Value = dr["vb_height"].ToString();
                parms[3].Value = dr["vb_x"].ToString();
                parms[4].Value = dr["vb_y"].ToString();
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public void UpdatePollingResult(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE PollingResult SET ");
                str.Append(" PR_UserRef = @PR_UserRef,PR_Session = @PR_Session,PR_Question = @PR_Question,PR_Answer = @PR_Answer,PR_Time = @PR_Time");
                str.Append(" WHERE PR_ID=@PR_ID");

                SqlParameter[] parms = {
                                        new SqlParameter("PR_ID",SqlDbType.NVarChar),
                                        new SqlParameter("PR_UserRef",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Session",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Question",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Answer",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Time",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["PR_ID"].ToString();
                parms[1].Value = dr["PR_UserRef"].ToString();
                parms[2].Value = dr["PR_Session"].ToString();
                parms[3].Value = dr["PR_Question"].ToString();
                parms[4].Value = dr["PR_Answer"].ToString();
                parms[5].Value = dr["PR_Time"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
        }

        public int SaveGeneralInfo(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("G");
                ID = "G" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblGeneralInfo ");
                str.Append("(gi_ID,gi_title,gi_content,gi_icon,gi_seq,gi_mainID,menuID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@gi_ID,@gi_title,@gi_content,@gi_icon,@gi_seq,@gi_mainID,@menuID,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("gi_ID",SqlDbType.NVarChar),
                                    new SqlParameter("gi_title",SqlDbType.NVarChar),
                                    new SqlParameter("gi_content",SqlDbType.NVarChar),
                                    new SqlParameter("gi_icon",SqlDbType.NVarChar),
                                    new SqlParameter("gi_seq",SqlDbType.NVarChar),
                                    new SqlParameter("gi_mainID",SqlDbType.NVarChar),
                                    new SqlParameter("menuID",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["gi_title"].ToString();
                parms[2].Value = dr["gi_content"].ToString();
                parms[3].Value = dr["gi_icon"].ToString();
                parms[4].Value = dr["gi_seq"].ToString();
                parms[5].Value = dr["gi_mainID"].ToString();
                parms[6].Value = dr["menuID"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = dr["deleteFlag"].ToString();
                parms[9].Value = timestamp;



                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("G", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public void SavePollingResult(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("PR");
                ID = "PR" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into PollingResult ");
                str.Append("(PR_ID,PR_UserRef,PR_Session,PR_Question,PR_Answer,PR_Time)");                
                str.Append(" values");
                str.Append("(@PR_ID,@PR_UserRef,@PR_Session,@PR_Question,@PR_Answer,@PR_Time)");

                SqlParameter[] parms = {
                                        new SqlParameter("PR_ID",SqlDbType.NVarChar),
                                        new SqlParameter("PR_UserRef",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Session",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Question",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Answer",SqlDbType.NVarChar),
                                        new SqlParameter("PR_Time",SqlDbType.DateTime)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["PR_UserRef"].ToString();
                parms[2].Value = dr["PR_Session"].ToString();
                parms[3].Value = dr["PR_Question"].ToString();
                parms[4].Value = dr["PR_Answer"].ToString();
                parms[5].Value = dr["PR_Time"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("PR", int.Parse(fkey));
            }
            catch (Exception ex) { }           
        }

        public int UpdateHall(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblLocation_Floorplan SET l_name=@l_name,l_image=@l_image,menuID=@menuID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where l_ID=@l_ID");
         
                SqlParameter[] parms = {
                                    new SqlParameter("@l_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@l_name",SqlDbType.NVarChar),
                                    new SqlParameter("@l_image",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["l_ID"].ToString();
                parms[1].Value = dr["l_name"].ToString();
                parms[2].Value = dr["l_image"].ToString();
                parms[3].Value = dr["menuID"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                
            }
            catch (Exception ex) { }
            return res;
        }

      

        public DataTable getGeneralInfobyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select gi_ID,gi_title,gi_content,gi_seq,gi_mainID,m.m_TitleEn as menuID from tblGeneralInfo g left join tb_Menu m on g.menuID=m.m_id where g.deleteFlag=0 and gi_title like '%" + searchkey + "%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getGeneralInfo()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select gi_ID,gi_title,gi_content,gi_seq,gi_mainID,m.m_TitleEn as menuID from tblGeneralInfo g left join tb_Menu m on g.menuID=m.m_id where g.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int SaveBooth(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("VB");
                ID = "VB" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblVenue_Booth ");
                str.Append("(vb_ID,vb_name,vb_location,vb_width,vb_height,vb_x,vb_y,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@vb_ID,@vb_name,@vb_location,@vb_width,@vb_height,@vb_x,@vb_y,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("vb_ID",SqlDbType.NVarChar),
                                    new SqlParameter("vb_name",SqlDbType.NVarChar),
                                    new SqlParameter("vb_location",SqlDbType.NVarChar),
                                    new SqlParameter("vb_width",SqlDbType.NVarChar),
                                    new SqlParameter("vb_height",SqlDbType.NVarChar),
                                    new SqlParameter("vb_x",SqlDbType.NVarChar),
                                    new SqlParameter("vb_y",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),


            };
                parms[0].Value = ID;
                parms[1].Value = dr["vb_name"].ToString();
                parms[2].Value = dr["vb_location"].ToString();
                parms[3].Value = dr["vb_width"].ToString();
                parms[4].Value = dr["vb_height"].ToString();
                parms[5].Value = dr["vb_x"].ToString();
                parms[6].Value = dr["vb_y"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = dr["deleteFlag"].ToString();
                parms[9].Value = timestamp;



                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("VB", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getGeneralInfoByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select gi_ID,gi_title,gi_content,gi_seq,gi_mainID,m.m_TitleEn as menuName,g.menuID from tblGeneralInfo g left join tb_Menu m on g.menuID=m.m_id where g.deleteFlag=0 and gi_ID='" + id + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int DeleteGeneralInfoByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblGeneralInfo set deleteFlag=1 where gi_ID=@gi_ID";
                SqlParameter[] parms = { new SqlParameter("@gi_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getExhibitingCompanies()
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblExhibitingCompany where deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;

                        //string ec_product = dr["ec_product"].ToString();
                        //if (ec_product.Contains(";"))
                        //{
                        //    string[] arrecproduct = ec_product.Split(';');

                        //    for (int i = 0; i < arrecproduct.Count(); i++)
                        //    {
                        //        productlist += getproductbyID(arrecproduct[i].ToString()) + ";";
                        //    }
                        //    productlist = productlist.TrimEnd(';');
                        //    dr["ec_product"] = productlist;
                        //}

                        if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                        {
                            hallID = getHallByID(dr["ec_hall"].ToString());
                            dr["ec_hall"] = hallID;
                        }
                        if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                        {
                            boothno = getBoothByID(dr["ec_booth"].ToString());
                            dr["ec_booth"] = boothno;
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }

        public int SaveHall(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("LF");
                ID = "LF" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblLocation_Floorplan ");
                str.Append("(l_ID,l_name,l_image,menuID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@l_ID,@l_name,@l_image,@menuID,@lang,@deleteFlag,@UpdatedDate)");

                
                SqlParameter[] parms = {
                                    new SqlParameter("@l_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@l_name",SqlDbType.NVarChar),
                                    new SqlParameter("@l_image",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["l_name"].ToString();
                parms[2].Value = dr["l_image"].ToString();
                parms[3].Value = dr["menuID"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;
              

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("LF", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getBoothlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblVenue_Booth where deleteFlag=0 and vb_ID='" + id + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public bool checksaveExistBooth(string boothname, string location)
        {
            bool isExist = false;
            try
            {
                DataTable dt = fn.GetDatasetByCommand("select * from tblVenue_Booth where deleteFlag=0 and vb_name='" + boothname + "' And vb_location='" + location + "'", "sdt").Tables[0];
                if(dt.Rows.Count > 0)
                {
                    isExist = true;
                }
                else
                {
                    isExist = false;
                }
            }
            catch (Exception ex) { }
            return isExist;
        }
        public bool checkupdateExistBooth(string boothname, string location, string boothID)
        {
            bool isExist = false;
            try
            {
                DataTable dt = fn.GetDatasetByCommand("select * from tblVenue_Booth where deleteFlag=0 and vb_name='" + boothname + "' And vb_location='" + location + "' And vb_ID<>'" + boothID + "'", "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    isExist = true;
                }
                else
                {
                    isExist = false;
                }
            }
            catch (Exception ex) { }
            return isExist;
        }

        public DataTable getBoothbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select vb_ID,vb_name,l.l_name as vb_location,vb_width,vb_height,vb_x,vb_y from tblVenue_Booth b left join tblLocation_Floorplan l on b.vb_location=l.l_ID where b.deleteFlag = 0 and vb_name like '%" + searchkey + "%'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int DeleteBoothByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblVenue_Booth set deleteFlag=1 where vb_ID=@vb_ID";
                SqlParameter[] parms = { new SqlParameter("@vb_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getProductCategoryMarketSeg()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblProductCategory where deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        public int DeleteHallByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblLocation_Floorplan set deleteFlag=1 where l_ID=@l_ID";
                SqlParameter[] parms = { new SqlParameter("@l_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }

  

        public DataTable getAllActivatedTable()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblActivatedForms where a_isActivated=1 and deleteFlag=0", "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getExhibitingCompaniesbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblExhibitingCompany where deleteFlag=0 and ec_name like '%" + searchkey + "%' or ec_booth like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;

                        //string ec_product = dr["ec_product"].ToString();
                        //if (ec_product.Contains(";"))
                        //{
                        //    string[] arrecproduct = ec_product.Split(';');

                        //    for (int i = 0; i < arrecproduct.Count(); i++)
                        //    {
                        //        productlist += getproductbyID(arrecproduct[i].ToString()) + ";";
                        //    }
                        //    productlist = productlist.TrimEnd(';');
                        //    dr["ec_product"] = productlist;
                        //}

                        if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                        {
                            hallID = getHallByID(dr["ec_hall"].ToString());
                            dr["ec_hall"] = hallID;
                        }
                        if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                        {
                            boothno = getBoothByID(dr["ec_booth"].ToString());
                            dr["ec_booth"] = boothno;
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return dt;
        }

        public DataTable getHallbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select f.l_ID,f.l_name,f.l_image,m.m_TitleEn as menuID from tblLocation_Floorplan f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0 and f.l_name like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
               
            }
            catch (Exception ex) { }
            return dt;
        }

  

        public string getBoothByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select vb_name from tblVenue_Booth where vb_ID ='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "vb_name").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public string getHallByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select l_name from tblLocation_Floorplan where l_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "l_name").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable GetRefForms()
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblActivatedForms where a_type='Reference' and deleteFlag=0 and a_isActivated=1";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }catch(Exception ex) { }
            return dt;
        }

        public DataTable GetDataForms()
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblActivatedForms where a_type='Data' and deleteFlag=0 and a_isActivated=1";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        public string getproductbyID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select p_name from tblProduct where p_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "p_name").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public bool CheckExist(string tablename, string checkkey, string checkvalue, string whereclause=null)
        {
            bool result = false;
            try
            {
                string sql = "select * from " + tablename + " where " + checkkey + "= '" + checkvalue + "' And deleteFlag=0" + whereclause;
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public bool CheckExistExhGetID(string tablename, string checkkey, string checkvalue, string whereclause, ref string ecID)
        {
            bool result = false;
            try
            {
                string sql = "select * from " + tablename + " where " + checkkey + "= '" + checkvalue + "' And deleteFlag=0" + whereclause;
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = true;
                    ecID = dt.Rows[0]["ec_ID"].ToString();
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public bool CheckExistExhByClientID(string tablename, string whereclause)
        {
            bool result = false;
            try
            {
                string sql = "select * from " + tablename + " where deleteFlag=0" + whereclause;
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public bool CheckExistConference(string conName, string condate, string concat)
        {
            bool result = false;
            try
            {
                string sql = "select * from tblConference where c_title='" + conName + "' And c_date='" + condate + "' And c_Cc='" + concat + "' And deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public string GetConferenceSpeakerID(string conID, string spkID, string cat, string concat)
        {
            string result = string.Empty;
            try
            {
                string sql = "select * from tblConferenceSpeakers where cspeaker_conferenceID='" + conID + "' And cspeaker_speakerID='" + spkID 
                    + "' And cspeaker_category='" + cat + "' And cspeaker_confcategory='" + concat + "' And deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0]["cspeaker_ID"].ToString();
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception ex) { }
            return result;
        }
        public string GetConferenceSponsorID(string conID, string spnID, string spncat)
        {
            string result = string.Empty;
            try
            {
                string sql = "select * from tblConferenceSponsors where csponsor_conferenceID='" + conID + "' And csponsor_sponsorID='" + spnID
                    + "' And csponsor_scategory='" + spncat + "' And deleteFlag=0";
                DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    result = dt.Rows[0]["csponsor_ID"].ToString();
                }
                else
                {
                    result = "";
                }
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable getExhibitingCompanyByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblexhibitingCompany where ec_ID='"+id+"'and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getexhibitingcompanybyID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select ec_name from tblexhibitingCompany where ec_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "ec_name").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public string getexhibitingcompanyIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                var sql = "select ec_ID from tblexhibitingCompany where ec_name='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "ec_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }


        public int UpdateExhibitingCompany(DataRow dr, bool isUpload=false)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblExhibitingCompany set ");
                str.Append("ec_name=@ec_name,ec_address=@ec_address,ec_country=@ec_country,ec_email=@ec_email,ec_contactno=@ec_contactno,ec_website=@ec_website,ec_brochure=@ec_brochure,ec_logo=@ec_logo,ec_companyprofile=@ec_companyprofile,ec_companyType=@ec_companyType"
                    + " ,ec_product=@ec_product,lang=@lang,updatedDate=@updatedDate,ec_marketseg=@ec_marketseg,ec_trends=@ec_trends"
                    + (!isUpload ? ",ec_hall=@ec_hall,ec_booth=@ec_booth,contactpersonID=@contactpersonID" : ""));
                if (!isUpload)
                {
                    str.Append(" where ec_ID=@ec_ID");
                }
                else
                {
                    str.Append(" where clientID=@clientID");
                }

                string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;

                string ec_product = dr["ec_product"].ToString();
                //if (ec_product.Contains(";"))
                //{
                //    string[] arrecproduct = ec_product.Split(';');

                //    for (int i = 0; i < arrecproduct.Count(); i++)
                //    {
                //        productlist += getproductIDbyName(arrecproduct[i].ToString()) + ";";
                //    }
                //    productlist = productlist.TrimEnd(';');

                //}

                if (isUpload)
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = dr["ec_hall"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = dr["ec_booth"].ToString();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = getHallIDByName(dr["ec_hall"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = getBoothIDByName(dr["ec_booth"].ToString());
                    }
                }

                if (!isUpload)
                {
                    SqlParameter[] parms = {
                                    new SqlParameter("ec_ID",SqlDbType.NVarChar),
                                    new SqlParameter("ec_name",SqlDbType.NVarChar),
                                    new SqlParameter("ec_address",SqlDbType.NVarChar),
                                    new SqlParameter("ec_country",SqlDbType.NVarChar),
                                    new SqlParameter("ec_email",SqlDbType.NVarChar),
                                    new SqlParameter("ec_contactno",SqlDbType.NVarChar),
                                    new SqlParameter("ec_website",SqlDbType.NVarChar),
                                    new SqlParameter("ec_brochure",SqlDbType.NVarChar),
                                    new SqlParameter("ec_logo",SqlDbType.NVarChar),
                                    new SqlParameter("ec_companyprofile",SqlDbType.NVarChar),
                                    new SqlParameter("ec_companyType",SqlDbType.NVarChar),
                                    new SqlParameter("ec_hall", SqlDbType.NVarChar),
                                    new SqlParameter("ec_booth", SqlDbType.NVarChar),
                                    new SqlParameter("contactpersonID", SqlDbType.NVarChar),
                                    new SqlParameter("ec_product",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("ec_marketseg",SqlDbType.NVarChar),
                                    new SqlParameter("ec_trends",SqlDbType.NVarChar)
                    };
                    parms[0].Value = dr["ec_ID"].ToString();
                    parms[1].Value = dr["ec_name"].ToString();
                    parms[2].Value = dr["ec_address"].ToString();
                    parms[3].Value = dr["ec_country"].ToString();
                    parms[4].Value = dr["ec_email"].ToString();
                    parms[5].Value = dr["ec_contactno"].ToString();
                    parms[6].Value = dr["ec_website"].ToString();
                    parms[7].Value = dr["ec_brochure"].ToString();
                    parms[8].Value = dr["ec_logo"].ToString();
                    parms[9].Value = dr["ec_companyprofile"].ToString();
                    parms[10].Value = dr["ec_companyType"].ToString();
                    parms[11].Value = hallID;
                    parms[12].Value = boothno;
                    parms[13].Value = dr["contactpersonID"].ToString();
                    parms[14].Value = ec_product;
                    parms[15].Value = dr["lang"].ToString();
                    parms[16].Value = false;
                    parms[17].Value = timestamp;
                    parms[18].Value = dr["ec_marketseg"];
                    parms[19].Value = dr["ec_trends"];

                    res = fn.ExecuteNonQuery(str.ToString(), parms);
                }
                else
                {
                    SqlParameter[] parms = {
                                    new SqlParameter("ec_name",SqlDbType.NVarChar),
                                    new SqlParameter("ec_address",SqlDbType.NVarChar),
                                    new SqlParameter("ec_country",SqlDbType.NVarChar),
                                    new SqlParameter("ec_email",SqlDbType.NVarChar),
                                    new SqlParameter("ec_contactno",SqlDbType.NVarChar),
                                    new SqlParameter("ec_website",SqlDbType.NVarChar),
                                    new SqlParameter("ec_brochure",SqlDbType.NVarChar),
                                    new SqlParameter("ec_logo",SqlDbType.NVarChar),
                                    new SqlParameter("ec_companyprofile",SqlDbType.NVarChar),
                                    new SqlParameter("ec_companyType",SqlDbType.NVarChar),
                                    new SqlParameter("ec_product",SqlDbType.NVarChar),
                                    new SqlParameter("lang",SqlDbType.NVarChar),
                                    new SqlParameter("deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("ec_marketseg",SqlDbType.NVarChar),
                                    new SqlParameter("ec_trends",SqlDbType.NVarChar),
                                    new SqlParameter("clientID",SqlDbType.NVarChar)
                    };
                    parms[0].Value = dr["ec_name"].ToString();
                    parms[1].Value = dr["ec_address"].ToString();
                    parms[2].Value = dr["ec_country"].ToString();
                    parms[3].Value = dr["ec_email"].ToString();
                    parms[4].Value = dr["ec_contactno"].ToString();
                    parms[5].Value = dr["ec_website"].ToString();
                    parms[6].Value = dr["ec_brochure"].ToString();
                    parms[7].Value = dr["ec_logo"].ToString();
                    parms[8].Value = dr["ec_companyprofile"].ToString();
                    parms[9].Value = dr["ec_companyType"].ToString();
                    parms[10].Value = ec_product;
                    parms[11].Value = dr["lang"].ToString();
                    parms[12].Value = false;
                    parms[13].Value = timestamp;
                    parms[14].Value = dr["ec_marketseg"];
                    parms[15].Value = dr["ec_trends"];
                    parms[16].Value = dr["clientID"];

                    res = fn.ExecuteNonQuery(str.ToString(), parms);
                }
            }
            catch (Exception ex) { }
            return res;
        }

        public int UpdateExhibitingCompanyBooth(DataRow dr, bool isUpload = false)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblExhibitingCompany set ");
                str.Append("ec_booth=@ec_booth");
                str.Append(" where ec_ID=@ec_ID And ec_hall=@ec_hall");

                string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;

                if (isUpload)
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = dr["ec_hall"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = dr["ec_booth"].ToString();
                    }
                }

                SqlParameter[] parms = {
                                    new SqlParameter("ec_ID",SqlDbType.NVarChar),
                                    new SqlParameter("ec_hall",SqlDbType.NVarChar),
                                    new SqlParameter("ec_booth",SqlDbType.NVarChar)
                };
                parms[0].Value = dr["ec_ID"].ToString();
                parms[1].Value = hallID;
                parms[2].Value = boothno;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public int UpdateExhibitingClientExhID(DataRow dr, bool isUpload = false)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("update tblExhibitingCompany set ");
                str.Append("clientID=@clientID");
                str.Append(" where ec_ID=@ec_ID And ec_hall=@ec_hall");

                string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;
                string onlineClientExhID = string.Empty;

                if (isUpload)
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = dr["ec_hall"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = dr["ec_booth"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["clientID"].ToString()))
                    {
                        onlineClientExhID = dr["clientID"].ToString();
                    }
                }

                SqlParameter[] parms = {
                                    new SqlParameter("ec_ID",SqlDbType.NVarChar),
                                    new SqlParameter("ec_hall",SqlDbType.NVarChar),
                                    new SqlParameter("clientID",SqlDbType.NVarChar)
                };
                parms[0].Value = dr["ec_ID"].ToString();
                parms[1].Value = hallID;
                parms[2].Value = onlineClientExhID;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }

        public DataTable getHalllstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblLocation_Floorplan where l_ID='"+id+"' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

      

        public int DeleteExhibitingCompany(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblexhibitingCompany set deleteFlag=1 where ec_ID=@ec_ID";
                SqlParameter[] parms = { new SqlParameter("@ec_ID",SqlDbType.NVarChar)};
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }catch(Exception ex) { }
            return result;
        }

        public int SaveExhibitingCompany(DataRow dr, bool isUpload=false)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("EC");
                ID = "EC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblExhibitingCompany ");
                str.Append("(ec_ID,ec_name,ec_address,ec_country,ec_email,ec_contactno,ec_website,ec_brochure,ec_logo,ec_companyprofile,ec_companyType,ec_hall,ec_booth,contactpersonID,ec_product,lang,deleteFlag,updatedDate,ec_marketseg,ec_trends,clientID)");
                str.Append(" values");
                str.Append("(@ec_ID,@ec_name,@ec_address,@ec_country,@ec_email,@ec_contactno,@ec_website,@ec_brochure,@ec_logo,@ec_companyprofile,@ec_companyType,@ec_hall,@ec_booth,@contactpersonID,@ec_product,@lang,@deleteFlag,@updatedDate,@ec_marketseg,@ec_trends,@clientID)");

                string productlist = string.Empty; string hallID = string.Empty; string boothno = string.Empty;

                string ec_product = dr["ec_product"].ToString();
                //if (ec_product.Contains(";"))
                //{
                //    string[] arrecproduct = ec_product.Split(';');

                //    for (int i = 0; i < arrecproduct.Count(); i++)
                //    {
                //        productlist += getproductIDbyName(arrecproduct[i].ToString()) + ";";
                //    }
                //    productlist = productlist.TrimEnd(';');
                //}            
                //else
                //{
                //    productlist = getproductIDbyName(ec_product);
                //}

                string clientID = string.Empty;
                if (isUpload)
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = dr["ec_hall"].ToString();
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = dr["ec_booth"].ToString();
                    }
                    clientID = dr["clientID"].ToString();
                }
                else
                {
                    if (!string.IsNullOrEmpty(dr["ec_hall"].ToString()))
                    {
                        hallID = getHallIDByName(dr["ec_hall"].ToString());
                    }
                    if (!string.IsNullOrEmpty(dr["ec_booth"].ToString()))
                    {
                        boothno = getBoothIDByName(dr["ec_booth"].ToString());
                    }
                }
                SqlParameter[] parms = {
                                    new SqlParameter("@ec_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_name",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_address",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_country",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_email",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_contactno",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_website",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_brochure",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_logo",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_companyprofile",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_companyType",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_hall",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_booth",SqlDbType.NVarChar),
                                    new SqlParameter("@contactpersonID",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_product",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_marketseg",SqlDbType.NVarChar),
                                    new SqlParameter("@ec_trends",SqlDbType.NVarChar),
                                    new SqlParameter("@clientID",SqlDbType.NVarChar)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["ec_name"].ToString();
                parms[2].Value = dr["ec_address"].ToString();
                parms[3].Value = dr["ec_country"].ToString();
                parms[4].Value = dr["ec_email"].ToString();
                parms[5].Value = dr["ec_contactno"].ToString();
                parms[6].Value = dr["ec_website"].ToString();
                parms[7].Value = dr["ec_brochure"].ToString();
                parms[8].Value = dr["ec_logo"].ToString();
                parms[9].Value = dr["ec_companyprofile"].ToString();
                parms[10].Value = dr["ec_companyType"].ToString();
                parms[11].Value = hallID;
                parms[12].Value = boothno;
                parms[13].Value = dr["contactpersonID"].ToString();
                parms[14].Value = ec_product;
                parms[15].Value = dr["lang"].ToString();
                parms[16].Value = false;
                parms[17].Value = timestamp;
                parms[18].Value = dr["ec_marketseg"];
                parms[19].Value = dr["ec_trends"];
                parms[20].Value = clientID;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("EC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }

        public string getBoothIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select vb_ID from tblVenue_Booth where vb_name ='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "vb_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public string getBoothIDByNameAndLocationHall(string name, string hallID)
        {
            string result = string.Empty;
            try
            {
                string sql = "select vb_ID from tblVenue_Booth where vb_name ='" + name + "' and vb_location='" + hallID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "vb_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public string getHallIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select l_ID from tblLocation_Floorplan where l_name ='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "l_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public string getproductIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select p_ID from tblProduct where p_name ='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "p_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public string getproductcategorybyID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select pc_name from tblProductCategory where pc_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "pc_name").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public string getgeneralinfiByID(string ID)
        {
            string result = string.Empty;
            try
            {                
                var sql = "select gi_title from tblGeneralInfo where gi_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "gi_title").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        public string replaceForSQL(string str)
        {
            return fn.replaceForSQL(str);
        }
        public string SelectQuestionBySession(string sessionID)
        {
            string questionID = string.Empty;
            try
            {
                questionID = fn.GetDataByCommand("select questionID from PollingQandA where sessionID='" + sessionID + "' and pollingStatus=1", "questionID");
            }
            catch(Exception ex) { }
            return questionID;
        }
        public string CheckPollingAnswer(string userRef, string question)
        {
            string ID = string.Empty;
            try
            {
                ID = fn.GetDataByCommand("select PR_ID from PollingResult where PR_Question='"+ question + "' and PR_UserRef='"+ userRef +"'", "PR_ID");
            }
            catch(Exception ex) { }
            return ID;
        }
        public DataTable SelectLQAVoteByQAIDUF(string uid, string qaid)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from LQAVote where UserReference='"+uid+"' and LQAID ='"+qaid+"'", "sdt").Tables[0];// order by QAType desc
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable selectLQABYSIDStatus(string sessionID, string status)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from LQA where SessionID='"+ sessionID + "' and LQAStatus ='"+ status + "' order by QAType desc", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable SelectLiveQAByID(string qAID)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from LQA where LQAID='" + qAID + "'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public void UpdateLQAType(string LQAID,string QAType)
        {         
            try
            {
                DataTable dt = fn.GetDatasetByCommand("select * from LQA where LQAID='" + LQAID + "'", "sdt").Tables[0];
                if(dt.Rows.Count >0)
                {
                    StringBuilder str = new StringBuilder();
                    str.Append("update LQA set QAType='" + QAType + "' where LQAID='" + LQAID + "'");
                    fn.ExecuteSQL(str.ToString());

                }
            }catch(Exception ex) { }
        }
        public void UpdateLQAStatus(string LQAID, string LQAStatus)
        {
            try
            {
                DataTable dt = fn.GetDatasetByCommand("select * from LQA where LQAID='" + LQAID + "'", "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    StringBuilder str = new StringBuilder();
                    str.Append("update LQA set LQAStatus='" + LQAStatus + "' where LQAID='" + LQAID + "'");
                    fn.ExecuteSQL(str.ToString());

                }
            }
            catch (Exception ex) { }
        }
        public DataTable selectLQABYSIDStatusType(string sessionID, string status, string Type)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select top 4 * from LQA where sessionID='" + sessionID + "' and LQAStatus='" + status + "' and QAType ='" + Type + "' order by LQANum desc", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getConferenceTitleByID(string id)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("select c_title from tblConference where c_ID='"+id+"' and deleteFlag=0", "c_title");
            }catch(Exception ex) { }
            return result;
        }
        public string getConferenceIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("select c_ID from tblConference where c_title='" + name + "' and deleteFlag=0", "c_ID");
            }
            catch (Exception ex) { }
            return result;
        }
        public string getConferenceIDByNameDate(string name, string date, string concat)
        {
            string result = string.Empty;
            try
            {
                result = fn.GetDataByCommand("select c_ID from tblConference where c_title='" + name + "' and c_date='" + date + "' and c_cc='" + concat + "' and deleteFlag=0", "c_ID");
            }
            catch (Exception ex) { }
            return result;
        }
        public void UpdateLQATitle(string LQAID, string LQATitle)
        {
            try
            {
                DataTable dt = fn.GetDatasetByCommand("select * from LQA where LQAID='" + LQAID + "'", "sdt").Tables[0];
                if (dt.Rows.Count > 0)
                {
                    StringBuilder str = new StringBuilder();
                    str.Append("update LQA set LQATitle='" + LQATitle + "' where LQAID='" + LQAID + "'");
                    fn.ExecuteSQL(str.ToString());

                }
            }
            catch (Exception ex) { }
        }
        public void UpdateLQA(string QAID)
        {
            //var res = (from aa in db.LQAs where aa.LQAID == objl.LQAID select aa).FirstOrDefault();
            //if (res != null)
            //{
            //    res.LQANum = res.LQANum + 1;
            //    db.SaveChanges();
            //}
        }
        public void SaveLQAVote(string QAID, string Uid)
        {
            //db.LQAVotes.Add(obj);
            //db.SaveChanges();
        }

        //***Day
        public int SaveDayGetID(DataRow dr, ref string DayID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("D");
                ID = "D" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblDay ");
                str.Append("(d_ID,d_key,d_date,d_year,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@d_ID,@d_key,@d_date,@d_year,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("d_ID",SqlDbType.NVarChar),
                                            new SqlParameter("d_key",SqlDbType.NVarChar),
                                            new SqlParameter("d_date",SqlDbType.NVarChar),
                                            new SqlParameter("d_year",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                DayID = ID;
                parms[0].Value = ID;
                parms[1].Value = dr["d_key"].ToString();
                parms[2].Value = dr["d_date"].ToString();
                parms[3].Value = dr["d_year"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("D", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int SaveDayGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("D");
                ID = "D" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblDay ");
                str.Append("(d_ID,d_key,d_date,d_year,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@d_ID,@d_key,@d_date,@d_year,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("d_ID",SqlDbType.NVarChar),
                                            new SqlParameter("d_key",SqlDbType.NVarChar),
                                            new SqlParameter("d_date",SqlDbType.NVarChar),
                                            new SqlParameter("d_year",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["d_key"].ToString();
                parms[2].Value = dr["d_date"].ToString();
                parms[3].Value = dr["d_year"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("D", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateDay(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblDay SET d_key=@d_key,d_date=@d_date,d_year=@d_year,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where d_ID=@d_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@d_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@d_key",SqlDbType.NVarChar),
                                    new SqlParameter("@d_date",SqlDbType.NVarChar),
                                    new SqlParameter("@d_year",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["d_ID"].ToString();
                parms[1].Value = dr["d_key"].ToString();
                parms[2].Value = dr["d_date"].ToString();
                parms[3].Value = dr["d_year"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteDayByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblDay set deleteFlag=1 where d_ID=@d_ID";
                SqlParameter[] parms = { new SqlParameter("@d_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getDay()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblDay where deleteFlag=0 Order By d_date";
                result = fn.GetDatasetByCommand(sql, "sqlDay").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getDayIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select d_ID from tblDay where d_date='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "d_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getDaybyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblDay where deleteFlag=0 and (d_date like '%" + searchkey + "%' or d_year like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getDaylstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblDay where d_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtDay").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***News Type
        public int SaveNewsTypeGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("NT");
                ID = "NT" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblNewsType ");
                str.Append("(nt_ID,nt_name,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@nt_ID,@nt_name,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("nt_ID",SqlDbType.NVarChar),
                                            new SqlParameter("nt_name",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["nt_name"].ToString();
                parms[2].Value = dr["lang"].ToString();
                parms[3].Value = false;
                parms[4].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("NT", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateNewsType(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblNewsType SET nt_name=@nt_name,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where nt_ID=@nt_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@nt_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@nt_name",SqlDbType.NVarChar),                                  
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["nt_ID"].ToString();
                parms[1].Value = dr["nt_name"].ToString();
                parms[2].Value = dr["lang"].ToString();
                parms[3].Value = false;
                parms[4].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteNewsTypeByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblNewsType set deleteFlag=1 where nt_ID=@nt_ID";
                SqlParameter[] parms = { new SqlParameter("@nt_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getNewsType()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblNewsType where deleteFlag=0";
                result = fn.GetDatasetByCommand(sql, "sqlNewsType").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getNewsTypeIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select nt_ID from tblNewsType where nt_name='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "nt_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getNewsTypebyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNewsType where deleteFlag=0 and nt_name like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdtNewsType").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getNewsTypelstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNewsType where nt_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtNewsType").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }



        //***Speaker Category
        public int SaveSpeakerCategoryGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SPKC");
                ID = "SPKC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSpeakerCategory ");
                str.Append("(speakerCat_ID,speakerCat_name,speakerCat_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@speakerCat_ID,@speakerCat_name,@speakerCat_seq,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("speakerCat_ID",SqlDbType.NVarChar),
                                            new SqlParameter("speakerCat_name",SqlDbType.NVarChar),
                                            new SqlParameter("speakerCat_seq",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["speakerCat_name"].ToString();
                parms[2].Value = dr["speakerCat_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SPKC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateSpeakerCategory(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblSpeakerCategory SET speakerCat_name=@speakerCat_name,speakerCat_seq=@speakerCat_seq,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where speakerCat_ID=@speakerCat_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@speakerCat_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@speakerCat_name",SqlDbType.NVarChar),
                                    new SqlParameter("@speakerCat_seq",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["speakerCat_ID"].ToString();
                parms[1].Value = dr["speakerCat_name"].ToString();
                parms[2].Value = dr["speakerCat_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteSpeakerCategoryByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblSpeakerCategory set deleteFlag=1 where speakerCat_ID=@speakerCat_ID";
                SqlParameter[] parms = { new SqlParameter("@speakerCat_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSpeakerCategory()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblSpeakerCategory where deleteFlag=0  order by speakerCat_seq";
                result = fn.GetDatasetByCommand(sql, "sqlSpeakerCategory").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSpeakerCategoryAllSequence()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select speakerCat_seq from tblSpeakerCategory where deleteFlag=0  order by speakerCat_seq desc";
                result = fn.GetDatasetByCommand(sql, "sqlSpeakerCategorySequence").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getLastSpeakerCategorySequence()
        {
            string result = string.Empty;
            try
            {
                string sql = "select top 1 speakerCat_seq from tblSpeakerCategory where deleteFlag=0  order by speakerCat_seq desc";
                result = fn.GetDataByCommand(sql, "speakerCat_seq");
            }
            catch (Exception ex) { }
            return result;
        }
        public string getSpeakerCategoryIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select speakerCat_ID from tblSpeakerCategory where speakerCat_name='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "speakerCat_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSpeakerCategorybyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblSpeakerCategory where deleteFlag=0 and (speakerCat_name like '%" + searchkey + "%' or speakerCat_seq like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdtSpeakerCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSpeakerCategorylstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblSpeakerCategory where speakerCat_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtSpeakerCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Sponsor Category
        public int SaveSponsorCategoryGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SPOC");
                ID = "SPOC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSponsorCategory ");
                str.Append("(sponsorCat_ID,sponsorCat_name,sponsorCat_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@sponsorCat_ID,@sponsorCat_name,@sponsorCat_seq,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("sponsorCat_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sponsorCat_name",SqlDbType.NVarChar),
                                            new SqlParameter("sponsorCat_seq",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["sponsorCat_name"].ToString();
                parms[2].Value = dr["sponsorCat_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SPOC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateSponsorCategory(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblSponsorCategory SET sponsorCat_name=@sponsorCat_name,sponsorCat_seq=@sponsorCat_seq,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where sponsorCat_ID=@sponsorCat_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@sponsorCat_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@sponsorCat_name",SqlDbType.NVarChar),
                                    new SqlParameter("@sponsorCat_seq",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["sponsorCat_ID"].ToString();
                parms[1].Value = dr["sponsorCat_name"].ToString();
                parms[2].Value = dr["sponsorCat_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteSponsorCategoryByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblSponsorCategory set deleteFlag=1 where sponsorCat_ID=@sponsorCat_ID";
                SqlParameter[] parms = { new SqlParameter("@sponsorCat_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSponsorCategory()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblSponsorCategory where deleteFlag=0  order by sponsorCat_seq";
                result = fn.GetDatasetByCommand(sql, "sqlSponsorCategory").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSponsorCategoryAllSequence()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select sponsorCat_seq from tblSponsorCategory where deleteFlag=0  order by sponsorCat_seq desc";
                result = fn.GetDatasetByCommand(sql, "sqlSponsorCategorySequence").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getLastSponsorCategorySequence()
        {
            string result = string.Empty;
            try
            {
                string sql = "select top 1 sponsorCat_seq from tblSponsorCategory where deleteFlag=0  order by sponsorCat_seq desc";
                result = fn.GetDataByCommand(sql, "sponsorCat_seq");
            }
            catch (Exception ex) { }
            return result;
        }
        public string getSponsorCategoryIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select sponsorCat_ID from tblSponsorCategory where sponsorCat_name='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "sponsorCat_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getSponsorCategorybyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblSponsorCategory where deleteFlag=0 and (sponsorCat_name like '%" + searchkey + "%' or sponsorCat_seq like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdtSponsorCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSponsorCategorylstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblSponsorCategory where sponsorCat_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtSponsorCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }


        //***Product Category
        public int SaveProductCategoryGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("PC");
                ID = "PC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblProductCategory ");
                str.Append("(pc_ID,pc_name,sortorder,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@pc_ID,@pc_name,@sortorder,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("pc_ID",SqlDbType.NVarChar),
                                            new SqlParameter("pc_name",SqlDbType.NVarChar),
                                            new SqlParameter("sortorder",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["pc_name"].ToString();
                parms[2].Value = dr["sortorder"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("PC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateProductCategory(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblProductCategory SET pc_name=@pc_name,sortorder=@sortorder,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where pc_ID=@pc_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@pc_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@pc_name",SqlDbType.NVarChar),
                                    new SqlParameter("@sortorder",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["pc_ID"].ToString();
                parms[1].Value = dr["pc_name"].ToString();
                parms[2].Value = dr["sortorder"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteProductCategoryByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblProductCategory set deleteFlag=1 where pc_ID=@pc_ID";
                SqlParameter[] parms = { new SqlParameter("@pc_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getProductCategory()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblProductCategory where deleteFlag=0  order by sortorder";
                result = fn.GetDatasetByCommand(sql, "sqlProductCategory").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getProductCategoryAllSequence()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select sortorder from tblProductCategory where deleteFlag=0  order by sortorder desc";
                result = fn.GetDatasetByCommand(sql, "sqlProductCategorySequence").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getLastProductCategorySequence()
        {
            string result = string.Empty;
            try
            {
                string sql = "select top 1 sortorder from tblProductCategory where deleteFlag=0  order by sortorder desc";
                result = fn.GetDataByCommand(sql, "sortorder");
            }
            catch (Exception ex) { }
            return result;
        }
        public string getProductCategoryIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select pc_ID from tblProductCategory where pc_name='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "pc_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getProductCategorybyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblProductCategory where deleteFlag=0 and (pc_name like '%" + searchkey + "%' or sortorder like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdtProductCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getProductCategorylstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblProductCategory where pc_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtProductCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Time List
        public int SaveTimeListGetID(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("T");
                ID = "T" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblTimeList ");
                str.Append("(tl_ID,tl_slot,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@tl_ID,@tl_slot,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("tl_ID",SqlDbType.NVarChar),
                                            new SqlParameter("tl_slot",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["tl_slot"].ToString();
                parms[2].Value = dr["lang"].ToString();
                parms[3].Value = false;
                parms[4].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("T", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateTimeList(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblTimeList SET tl_slot=@tl_slot,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where tl_ID=@tl_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@tl_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@tl_slot",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["tl_ID"].ToString();
                parms[1].Value = dr["tl_slot"].ToString();
                parms[2].Value = dr["lang"].ToString();
                parms[3].Value = false;
                parms[4].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteTimeListByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblTimeList set deleteFlag=1 where tl_ID=@tl_ID";
                SqlParameter[] parms = { new SqlParameter("@tl_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getTimeList()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select * from tblTimeList where deleteFlag=0";
                result = fn.GetDatasetByCommand(sql, "sqlTimeList").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getTimeListIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select tl_ID from tblTimeList where tl_slot='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "tl_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getTimeListbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblTimeList where deleteFlag=0 and tl_slot like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdtTimeList").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getTimeListlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblTimeList where tl_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtTimeList").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }


        //***Conference Speaker
        public int SaveConferenceSpeaker(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("CSK");
                ID = "CSK" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConferenceSpeakers ");
                str.Append("(cspeaker_ID,cspeaker_category,cspeaker_conferenceID,cspeaker_speakerID,cspeaker_confcategory,lang,deleteFlag,UpdatedDate,sorting)");
                str.Append(" values");
                str.Append("(@cspeaker_ID,@cspeaker_category,@cspeaker_conferenceID,@cspeaker_speakerID,@cspeaker_confcategory,@lang,@deleteFlag,@UpdatedDate,@sorting)");

                SqlParameter[] parms = {
                                            new SqlParameter("cspeaker_ID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_category",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_conferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_speakerID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_confcategory",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                            new SqlParameter("sorting",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["cspeaker_category"].ToString();
                parms[2].Value = dr["cspeaker_conferenceID"].ToString();
                parms[3].Value = dr["cspeaker_speakerID"].ToString();
                parms[4].Value = dr["cspeaker_confcategory"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;
                parms[8].Value = dr["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dr["sorting"].ToString()) ? dr["sorting"].ToString() : "99") : "99";

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("CSK", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateConferenceSpeaker(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConferenceSpeakers SET cspeaker_category=@cspeaker_category,cspeaker_conferenceID=@cspeaker_conferenceID,cspeaker_speakerID=@cspeaker_speakerID,cspeaker_confcategory=@cspeaker_confcategory,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate,sorting=@sorting where cspeaker_ID=@cspeaker_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("cspeaker_ID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_category",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_conferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_speakerID",SqlDbType.NVarChar),
                                            new SqlParameter("cspeaker_confcategory",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                            new SqlParameter("sorting",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["cspeaker_ID"].ToString();
                parms[1].Value = dr["cspeaker_category"].ToString();
                parms[2].Value = dr["cspeaker_conferenceID"].ToString();
                parms[3].Value = dr["cspeaker_speakerID"].ToString();
                parms[4].Value = dr["cspeaker_confcategory"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;
                parms[8].Value = !string.IsNullOrEmpty(dr["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dr["sorting"].ToString()) ? dr["sorting"].ToString() : "99";

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteConferenceSpeakerByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConferenceSpeakers set deleteFlag=1 where cspeaker_ID=@cspeaker_ID";
                SqlParameter[] parms = { new SqlParameter("@cspeaker_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConferenceSpeaker()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select cs.*,c.c_title,cc.cc_categoryname,s.s_fullname,sc.speakerCat_name From tblConferenceSpeakers as cs"
                    + " Inner Join tblConference as c On cs.cspeaker_conferenceID=c.c_ID"
                    + " Left Join tblConferenceCategory as cc On cs.cspeaker_confcategory=cc.cc_ID"
                    + " Inner Join tblSpeaker as s On cs.cspeaker_speakerID=s.s_ID"
                    + " Inner Join tblSpeakerCategory as sc On cs.cspeaker_category=sc.speakerCat_ID"
                    + " Where cs.deleteFlag=0;"//cc.deleteFlag=0 And And c.deleteFlag=0 And s.deleteFlag=0 And sc.deleteFlag=0
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceSpeakerlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConferenceSpeakers where cspeaker_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceSpeakerbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select cs.*,c.c_title,cc.cc_categoryname,s.s_fullname,sc.speakerCat_name From tblConferenceSpeakers as cs"
                    + " Inner Join tblConference as c On cs.cspeaker_conferenceID=c.c_ID"
                    + " Left Join tblConferenceCategory as cc On cs.cspeaker_confcategory=cc.cc_ID"
                    + " Inner Join tblSpeaker as s On cs.cspeaker_speakerID=s.s_ID"
                    + " Inner Join tblSpeakerCategory as sc On cs.cspeaker_category=sc.speakerCat_ID"
                    + " Where cs.deleteFlag=0 And c.deleteFlag=0 And s.deleteFlag=0"//cc.deleteFlag=0 And 
                    + " And (sc.speakerCat_name Like '%" + searchkey + "%' Or c.c_title Like '%" + searchkey + "%' Or cc.cc_categoryname Like '%" + searchkey + "%' Or s.s_fullname Like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getspeakercategory()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblSpeakerCategory where deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        //***Conference Sponsor
        public int SaveConferenceSponsor(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("CSP");
                ID = "CSP" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConferenceSponsors ");
                str.Append("(csponsor_ID,csponsor_sponsorID,csponsor_scategory,csponsor_conferenceID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@csponsor_ID,@csponsor_sponsorID,@csponsor_scategory,@csponsor_conferenceID,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("csponsor_ID",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_sponsorID",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_scategory",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_conferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["csponsor_sponsorID"].ToString();
                parms[2].Value = dr["csponsor_scategory"].ToString();
                parms[3].Value = dr["csponsor_conferenceID"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("CSP", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateConferenceSponsor(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConferenceSponsors SET csponsor_sponsorID=@csponsor_sponsorID,csponsor_scategory=@csponsor_scategory,csponsor_conferenceID=@csponsor_conferenceID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where csponsor_ID=@csponsor_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("csponsor_ID",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_sponsorID",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_scategory",SqlDbType.NVarChar),
                                            new SqlParameter("csponsor_conferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["csponsor_ID"].ToString();
                parms[1].Value = dr["csponsor_sponsorID"].ToString();
                parms[2].Value = dr["csponsor_scategory"].ToString();
                parms[3].Value = dr["csponsor_conferenceID"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteConferenceSponsorByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConferenceSponsors set deleteFlag=1 where csponsor_ID=@csponsor_ID";
                SqlParameter[] parms = { new SqlParameter("@csponsor_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConferenceSponsor()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select cs.*,c.c_title,s.s_name,sc.sponsorCat_name From tblConferenceSponsors as cs"
                    + " Inner Join tblConference as c On cs.csponsor_conferenceID=c.c_ID"
                    + " Inner Join tblSponsor as s On cs.csponsor_sponsorID=s.s_ID"
                    + " Left Join tblSponsorCategory as sc On cs.csponsor_scategory=sc.sponsorCat_ID"
                    + " Where cs.deleteFlag=0 And c.deleteFlag=0 And s.deleteFlag=0"// And sc.deleteFlag=0
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceSponsorlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConferenceSponsors where csponsor_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceSponsorbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select cs.*,c.c_title,s.s_name,sc.sponsorCat_name From tblConferenceSponsors as cs"
                    + " Inner Join tblConference as c On cs.csponsor_conferenceID=c.c_ID"
                    + " Inner Join tblSponsor as s On cs.csponsor_sponsorID=s.s_ID"
                    + " Left Join tblSponsorCategory as sc On cs.csponsor_scategory=sc.sponsorCat_ID"
                    + " Where cs.deleteFlag=0 And c.deleteFlag=0 And s.deleteFlag=0"// And sc.deleteFlag=0
                    + " And (c.c_title Like '%" + searchkey + "%' Or s.s_name Like '%" + searchkey + "%' Or sc.sponsorCat_name Like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Product
        public int SaveProduct(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("P");
                ID = "P" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblProduct ");
                str.Append("(p_ID,sororder,p_name,p_category,p_mainproductID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@p_ID,@sororder,@p_name,@p_category,@p_mainproductID,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("p_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sororder",SqlDbType.NVarChar),
                                            new SqlParameter("p_name",SqlDbType.NVarChar),
                                            new SqlParameter("p_category",SqlDbType.NVarChar),
                                            new SqlParameter("p_mainproductID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["sororder"].ToString();
                parms[2].Value = dr["p_name"].ToString();
                parms[3].Value = dr["p_category"].ToString();
                parms[4].Value = dr["p_mainproductID"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("P", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateProduct(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblProduct SET sororder=@sororder,p_name=@p_name,p_category=@p_category,p_mainproductID=@p_mainproductID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where p_ID=@p_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("p_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sororder",SqlDbType.NVarChar),
                                            new SqlParameter("p_name",SqlDbType.NVarChar),
                                            new SqlParameter("p_category",SqlDbType.NVarChar),
                                            new SqlParameter("p_mainproductID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["p_ID"].ToString();
                parms[1].Value = dr["sororder"].ToString();
                parms[2].Value = dr["p_name"].ToString();
                parms[3].Value = dr["p_category"].ToString();
                parms[4].Value = dr["p_mainproductID"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteProductByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblProduct set deleteFlag=1 where p_ID=@p_ID";
                SqlParameter[] parms = { new SqlParameter("@p_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getProduct()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select p.*,mp.p_name as mainproduct,pc.pc_name From tblProduct as p"
                    + " Left Join tblProduct as mp On p.p_mainproductID=mp.p_ID"
                    + " Left Join tblProductCategory as pc On p.p_category=pc.pc_ID"
                    + " Where p.deleteFlag=0 Order By p.p_mainproductID"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getProductlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblProduct where p_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getProductbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select p.*,mp.p_name as mainproduct,pc.pc_name From tblProduct as p"
                    + " Left Join tblProduct as mp On p.p_mainproductID=mp.p_ID"
                    + " Left Join tblProductCategory as pc On p.p_category=pc.pc_ID"
                    + " Where p.deleteFlag=0"
                    + " And (p.p_name Like '%" + searchkey + "%' Or mp.p_name Like '%" + searchkey + "%' Or pc.pc_name Like '%" + searchkey + "%')"
                    + " Order By p.p_mainproductID";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getMainProduct()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblProduct as p"
                    + " Inner Join tblProductCategory as pc On p.p_category=pc.pc_ID"
                    + " where p.deleteFlag=0 and p.p_category <>'' And p.p_category Is Not Null"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }

        //***Scrolling Banner
        public int SaveScrollingBanner(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SB");
                ID = "SB" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblScrollingBanner ");
                str.Append("(sb_ID,sb_title,sb_image,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@sb_ID,@sb_title,@sb_image,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("sb_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sb_title",SqlDbType.NVarChar),
                                            new SqlParameter("sb_image",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["sb_title"].ToString();
                parms[2].Value = dr["sb_image"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SB", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateScrollingBanner(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblScrollingBanner SET sb_title=@sb_title,sb_image=@sb_image,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where sb_ID=@sb_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("sb_ID",SqlDbType.NVarChar),
                                            new SqlParameter("sb_title",SqlDbType.NVarChar),
                                            new SqlParameter("sb_image",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["sb_ID"].ToString();
                parms[1].Value = dr["sb_title"].ToString();
                parms[2].Value = dr["sb_image"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteScrollingBannerByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblScrollingBanner set deleteFlag=1 where sb_ID=@sb_ID";
                SqlParameter[] parms = { new SqlParameter("@sb_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getScrollingBanner()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblScrollingBanner Where deleteFlag=0"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getScrollingBannerlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblScrollingBanner where sb_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getScrollingBannerbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblScrollingBanner Where deleteFlag=0"
                    + " And sb_title Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Bottom Banner
        public int SaveBottomBanner(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("BB");
                ID = "BB" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblBottomBanner ");
                str.Append("(bb_ID,bb_title,bb_image,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@bb_ID,@bb_title,@bb_image,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("bb_ID",SqlDbType.NVarChar),
                                            new SqlParameter("bb_title",SqlDbType.NVarChar),
                                            new SqlParameter("bb_image",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["bb_title"].ToString();
                parms[2].Value = dr["bb_image"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("BB", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateBottomBanner(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblBottomBanner SET bb_title=@bb_title,bb_image=@bb_image,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where bb_ID=@bb_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("bb_ID",SqlDbType.NVarChar),
                                            new SqlParameter("bb_title",SqlDbType.NVarChar),
                                            new SqlParameter("bb_image",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["bb_ID"].ToString();
                parms[1].Value = dr["bb_title"].ToString();
                parms[2].Value = dr["bb_image"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteBottomBannerByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblBottomBanner set deleteFlag=1 where bb_ID=@bb_ID";
                SqlParameter[] parms = { new SqlParameter("@bb_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getBottomBanner()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblBottomBanner Where deleteFlag=0"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getBottomBannerlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblBottomBanner where bb_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getBottomBannerbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblBottomBanner Where deleteFlag=0"
                    + " And bb_title Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Innovation
        public int SaveInnovation(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("INO");
                ID = "INO" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblInnovation ");
                str.Append("(Ino_ID,Ino_productName,Ino_QCategories,Ino_productInfo,Ino_productPic,Ino_certification,Ino_region,Ino_rating,Ino_exhID,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@Ino_ID,@Ino_productName,@Ino_QCategories,@Ino_productInfo,@Ino_productPic,@Ino_certification,@Ino_region,@Ino_rating,@Ino_exhID,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("Ino_ID",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productName",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_QCategories",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productInfo",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productPic",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_certification",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_region",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_rating",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_exhID",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["Ino_productName"].ToString();
                parms[2].Value = dr["Ino_QCategories"].ToString();
                parms[3].Value = dr["Ino_productInfo"].ToString();
                parms[4].Value = dr["Ino_productPic"].ToString();
                parms[5].Value = dr["Ino_certification"].ToString();
                parms[6].Value = dr["Ino_region"].ToString();
                parms[7].Value = dr["Ino_rating"].ToString();
                parms[8].Value = dr["Ino_exhID"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("INO", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateInnovation(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblInnovation SET Ino_productName=@Ino_productName,Ino_QCategories=@Ino_QCategories,Ino_productInfo=@Ino_productInfo,Ino_productPic=@Ino_productPic,Ino_certification=@Ino_certification,Ino_region=@Ino_region"
                    + ",Ino_rating=@Ino_rating,Ino_exhID=@Ino_exhID,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where Ino_ID=@Ino_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("Ino_ID",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productName",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_QCategories",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productInfo",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_productPic",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_certification",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_region",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_rating",SqlDbType.NVarChar),
                                            new SqlParameter("Ino_exhID",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["Ino_ID"].ToString();
                parms[1].Value = dr["Ino_productName"].ToString();
                parms[2].Value = dr["Ino_QCategories"].ToString();
                parms[3].Value = dr["Ino_productInfo"].ToString();
                parms[4].Value = dr["Ino_productPic"].ToString();
                parms[5].Value = dr["Ino_certification"].ToString();
                parms[6].Value = dr["Ino_region"].ToString();
                parms[7].Value = dr["Ino_rating"].ToString();
                parms[8].Value = dr["Ino_exhID"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteInnovationByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblInnovation set deleteFlag=1 where Ino_ID=@Ino_ID";
                SqlParameter[] parms = { new SqlParameter("@Ino_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getInnovation()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select i.*,ec.ec_name From tblInnovation as i"
                    + " Left Join tblExhibitingCompany as ec On i.Ino_exhID=ec.ec_ID"
                    + " Where i.deleteFlag=0"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getInnovationlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblInnovation where Ino_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getInnovationbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select i.*,ec.ec_name From tblInnovation as i"
                    + " Left Join tblExhibitingCompany as ec On i.Ino_exhID=ec.ec_ID"
                    + " Where i.deleteFlag=0"
                    + " And (i.Ino_productName Like '%" + searchkey + "%' Or (';' + i.Ino_QCategories + ';') like '%;" + searchkey + ";%'"
                    + " Or (';' + i.Ino_certification + ';') like '%;" + searchkey + ";%'"
                    + " Or (';' + i.Ino_region + ';') like '%;" + searchkey + ";%')";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getInnovationIDByName(string name)
        {
            string result = string.Empty;
            try
            {
                var sql = "select Ino_ID from tblInnovation where Ino_productName='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "Ino_ID");

            }
            catch (Exception ex) { }
            return result;
        }

        //***Inbox
        public int SaveInbox(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("I");
                ID = "I" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblInbox ");
                str.Append("(i_ID,i_title,i_content,i_date,i_time,i_isPush,lang,deleteFlag,UpdatedDate,organizer)");
                str.Append(" values");
                str.Append("(@i_ID,@i_title,@i_content,@i_date,@i_time,@i_isPush,@lang,@deleteFlag,@UpdatedDate,@organizer)");

                SqlParameter[] parms = {
                                            new SqlParameter("i_ID",SqlDbType.NVarChar),
                                            new SqlParameter("i_title",SqlDbType.NVarChar),
                                            new SqlParameter("i_content",SqlDbType.NVarChar),
                                            new SqlParameter("i_date",SqlDbType.NVarChar),
                                            new SqlParameter("i_time",SqlDbType.NVarChar),
                                            new SqlParameter("i_isPush",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                            new SqlParameter("organizer",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["i_title"].ToString();
                parms[2].Value = dr["i_content"].ToString();
                parms[3].Value = dr["i_date"].ToString();
                parms[4].Value = dr["i_time"].ToString();
                parms[5].Value = dr["i_isPush"].ToString();
                parms[6].Value = dr["lang"].ToString();
                parms[7].Value = false;
                parms[8].Value = timestamp;
                parms[9].Value = dr["organizer"] != DBNull.Value ? !string.IsNullOrEmpty(dr["organizer"].ToString()) ? dr["organizer"].ToString() : "" : "";

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("I", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateInbox(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblInbox SET i_title=@i_title,i_content=@i_content,i_date=@i_date,i_time=@i_time,i_isPush=@i_isPush,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate,organizer=@organizer where i_ID=@i_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("i_ID",SqlDbType.NVarChar),
                                            new SqlParameter("i_title",SqlDbType.NVarChar),
                                            new SqlParameter("i_content",SqlDbType.NVarChar),
                                            new SqlParameter("i_date",SqlDbType.NVarChar),
                                            new SqlParameter("i_time",SqlDbType.NVarChar),
                                            new SqlParameter("i_isPush",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar),
                                            new SqlParameter("organizer",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["i_ID"].ToString();
                parms[1].Value = dr["i_title"].ToString();
                parms[2].Value = dr["i_content"].ToString();
                parms[3].Value = dr["i_date"].ToString();
                parms[4].Value = dr["i_time"].ToString();
                parms[5].Value = dr["i_isPush"].ToString();
                parms[6].Value = dr["lang"].ToString();
                parms[7].Value = false;
                parms[8].Value = timestamp;
                parms[9].Value = dr["organizer"] != DBNull.Value ? !string.IsNullOrEmpty(dr["organizer"].ToString()) ? dr["organizer"].ToString() : "" : "";

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteInboxByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblInbox set deleteFlag=1 where i_ID=@i_ID";
                SqlParameter[] parms = { new SqlParameter("@i_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getInbox()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblInbox Where deleteFlag=0"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getInboxlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblInbox where i_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getInboxbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblInbox Where deleteFlag=0 And i_title Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }


      
      
        public int SaveTimeGetID(DataRow dr, ref string timeID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("T");
                ID = "T" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblTimeList ");
                str.Append("(tl_ID,tl_slot,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@tl_ID,@tl_slot,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("tl_ID",SqlDbType.NVarChar),
                                            new SqlParameter("tl_slot",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                timeID = ID;
                parms[0].Value = ID;
                parms[1].Value = dr["tl_slot"].ToString();
                parms[2].Value = dr["lang"].ToString();
                parms[3].Value = false;
                parms[4].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("T", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public string getTimeIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select tl_ID from tblTimeList where tl_slot='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "tl_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }

        //***Conference Category
        public int SaveConferenceCategoryGetID(DataRow dr, ref string concatID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("CC");
                ID = "CC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConferenceCategory ");
                str.Append("(cc_ID,cc_categoryname,cc_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@cc_ID,@cc_categoryname,@cc_seq,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("cc_ID",SqlDbType.NVarChar),
                                            new SqlParameter("cc_categoryname",SqlDbType.NVarChar),
                                            new SqlParameter("cc_seq",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                concatID = ID;
                parms[0].Value = ID;
                parms[1].Value = dr["cc_categoryname"].ToString();
                parms[2].Value = dr["cc_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("CC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateConferenceCategory(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConferenceCategory SET cc_categoryname=@cc_categoryname,cc_seq=@cc_seq,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where cc_ID=@cc_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@cc_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@cc_categoryname",SqlDbType.NVarChar),
                                    new SqlParameter("@cc_seq",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["cc_ID"].ToString();
                parms[1].Value = dr["cc_categoryname"].ToString();
                parms[2].Value = dr["cc_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getConferenceCategory()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConferenceCategory where deleteFlag=0 order by cc_seq", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public string getConferenceCategoryIDbyName(string name)
        {
            string result = string.Empty;
            try
            {
                string sql = "select cc_ID from tblConferenceCategory where cc_categoryname='" + name + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "cc_ID").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public string getConferenceCategoryNamebyID(string ID)
        {
            string result = string.Empty;
            try
            {
                string sql = "select cc_categoryname from tblConferenceCategory where cc_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "cc_categoryname").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConferenceCategorybyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConferenceCategory where deleteFlag=0 and (cc_categoryname like '%" + searchkey + "%' or cc_seq like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdtConferenceCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceCategorylstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConferenceCategory where cc_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtConferenceCategory").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteConferenceCategoryByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConferenceCategory set deleteFlag=1 where cc_ID=@cc_ID";
                SqlParameter[] parms = { new SqlParameter("@cc_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConferenceCategoryAllSequence()
        {
            DataTable result = new DataTable();
            try
            {
                string sql = "select cc_seq from tblConferenceCategory where deleteFlag=0 order by cc_seq desc";
                result = fn.GetDatasetByCommand(sql, "cc_seq").Tables[0];
            }
            catch (Exception ex) { }
            return result;
        }
        public string getLastConferenceCategorySequence()
        {
            string result = string.Empty;
            try
            {
                string sql = "select top 1 cc_seq from tblConferenceCategory where deleteFlag=0  order by cc_seq desc";
                result = fn.GetDataByCommand(sql, "cc_seq");
            }
            catch (Exception ex) { }
            return result;
        }

        //***Configuration Images
        public int SaveConfigurationImages(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("CI");
                ID = "CI" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConfigurationImages ");
                str.Append("(img_ID,img_key,img_file,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@img_ID,@img_key,@img_file,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("img_ID",SqlDbType.NVarChar),
                                            new SqlParameter("img_key",SqlDbType.NVarChar),
                                            new SqlParameter("img_file",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["img_key"].ToString();
                parms[2].Value = dr["img_file"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("CI", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateConfigurationImages(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConfigurationImages SET img_key=@img_key,img_file=@img_file,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where img_ID=@img_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("img_ID",SqlDbType.NVarChar),
                                            new SqlParameter("img_key",SqlDbType.NVarChar),
                                            new SqlParameter("img_file",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["img_ID"].ToString();
                parms[1].Value = dr["img_key"].ToString();
                parms[2].Value = dr["img_file"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteConfigurationImagesByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConfigurationImages set deleteFlag=1 where img_ID=@img_ID";
                SqlParameter[] parms = { new SqlParameter("@img_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConfigurationImages()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblConfigurationImages Where deleteFlag=0"
                    , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConfigurationImageslstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConfigurationImages where img_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConfigurationImagesbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblConfigurationImages Where deleteFlag=0"
                    + " And img_key Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Homepage
        public int SaveHomepage(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("H");
                ID = "H" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblHomePage ");
                str.Append("(hp_ID,hp_mtitle,hp_stitle,hp_content,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@hp_ID,@hp_mtitle,@hp_stitle,@hp_content,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("hp_ID",SqlDbType.NVarChar),
                                            new SqlParameter("hp_mtitle",SqlDbType.NVarChar),
                                            new SqlParameter("hp_stitle",SqlDbType.NVarChar),
                                            new SqlParameter("hp_content",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["hp_mtitle"].ToString();
                parms[2].Value = dr["hp_stitle"].ToString();
                parms[3].Value = dr["hp_content"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("H", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateHomepage(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblHomePage SET hp_mtitle=@hp_mtitle,hp_stitle=@hp_stitle,hp_content=@hp_content,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where hp_ID=@hp_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("hp_ID",SqlDbType.NVarChar),
                                            new SqlParameter("hp_mtitle",SqlDbType.NVarChar),
                                            new SqlParameter("hp_stitle",SqlDbType.NVarChar),
                                            new SqlParameter("hp_content",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["hp_ID"].ToString();
                parms[1].Value = dr["hp_mtitle"].ToString();
                parms[2].Value = dr["hp_stitle"].ToString();
                parms[3].Value = dr["hp_content"].ToString();
                parms[4].Value = dr["lang"].ToString();
                parms[5].Value = false;
                parms[6].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteHomepageByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblHomePage set deleteFlag=1 where hp_ID=@hp_ID";
                SqlParameter[] parms = { new SqlParameter("@hp_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getHomepage()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblHomePage Where deleteFlag=0"
                    , "sdtHomePage").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getHomepagelstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblHomePage where hp_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getHomepagebyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblHomePage Where deleteFlag=0 And hp_mtitle Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }


        //***Reward
        public int SaveReward(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("R");
                ID = "R" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblReward ");
                str.Append("(rw_ID,rw_title1,rw_title2,rw_image1,rw_image2,rw_password,rw_content1,rw_content2,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@rw_ID,@rw_title1,@rw_title2,@rw_image1,@rw_image2,@rw_password,@rw_content1,@rw_content2,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("rw_ID",SqlDbType.NVarChar),
                                            new SqlParameter("rw_title1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_title2",SqlDbType.NVarChar),
                                            new SqlParameter("rw_image1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_image2",SqlDbType.NVarChar),
                                            new SqlParameter("rw_password",SqlDbType.NVarChar),
                                            new SqlParameter("rw_content1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_content2",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["rw_title1"].ToString();
                parms[2].Value = dr["rw_title2"].ToString();
                parms[3].Value = dr["rw_image1"].ToString();
                parms[4].Value = dr["rw_image2"].ToString();
                parms[5].Value = dr["rw_password"].ToString();
                parms[6].Value = dr["rw_content1"].ToString();
                parms[7].Value = dr["rw_content2"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("R", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateReward(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblReward SET rw_title1=@rw_title1,rw_title2=@rw_title2,rw_image1=@rw_image1,rw_image2=@rw_image2,rw_password=@rw_password,rw_content1=@rw_content1,rw_content2=@rw_content2,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where rw_ID=@rw_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("rw_ID",SqlDbType.NVarChar),
                                            new SqlParameter("rw_title1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_title2",SqlDbType.NVarChar),
                                            new SqlParameter("rw_image1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_image2",SqlDbType.NVarChar),
                                            new SqlParameter("rw_password",SqlDbType.NVarChar),
                                            new SqlParameter("rw_content1",SqlDbType.NVarChar),
                                            new SqlParameter("rw_content2",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["rw_ID"].ToString();
                parms[1].Value = dr["rw_title1"].ToString();
                parms[2].Value = dr["rw_title2"].ToString();
                parms[3].Value = dr["rw_image1"].ToString();
                parms[4].Value = dr["rw_image2"].ToString();
                parms[5].Value = dr["rw_password"].ToString();
                parms[6].Value = dr["rw_content1"].ToString();
                parms[7].Value = dr["rw_content2"].ToString();
                parms[8].Value = dr["lang"].ToString();
                parms[9].Value = false;
                parms[10].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteRewardByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblReward set deleteFlag=1 where rw_ID=@rw_ID";
                SqlParameter[] parms = { new SqlParameter("@rw_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getReward()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblReward Where deleteFlag=0"
                    , "sdtReward").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getRewardlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblReward where rw_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdtReward").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getRewardbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From tblReward Where deleteFlag=0 And rw_title1 Like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***News
        public int SaveNews(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("N");
                ID = "N" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblNews ");
                str.Append("(n_ID,n_title,n_content,n_picture,menuID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@n_ID,@n_title,@n_content,@n_picture,@menuID,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("@n_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@n_title",SqlDbType.NVarChar),
                                    new SqlParameter("@n_content",SqlDbType.NVarChar),
                                    new SqlParameter("@n_picture",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["n_title"].ToString();
                parms[2].Value = dr["n_content"].ToString();
                parms[3].Value = dr["n_picture"].ToString();
                parms[4].Value = dr["menuID"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("N", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateNews(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblNews SET n_title=@n_title,n_content=@n_content,n_picture=@n_picture,menuID=@menuID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where n_ID=@n_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@n_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@n_title",SqlDbType.NVarChar),
                                     new SqlParameter("@n_content",SqlDbType.NVarChar),
                                    new SqlParameter("@n_picture",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["n_ID"].ToString();
                parms[1].Value = dr["n_title"].ToString();
                parms[2].Value = dr["n_content"].ToString();
                parms[3].Value = dr["n_picture"].ToString();
                parms[4].Value = dr["menuID"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getNews()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblNews f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteNewsByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblNews set deleteFlag=1 where n_ID=@n_ID";
                SqlParameter[] parms = { new SqlParameter("@n_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getNewsbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNews f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0 and f.n_title like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getNewsByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select n_title from tblNews where n_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "n_title").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable selectNewsMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_PageType='NewsList' and Status <> 'Deleted'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getNewslstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNews where n_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Newsletter
        public int SaveNewsletter(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("NL");
                ID = "NL" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblNewsletter ");
                str.Append("(nl_ID,nl_title,nl_issue,nl_doissue,nl_cover,nl_dlink,menuID,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@nl_ID,@nl_title,@nl_issue,@nl_doissue,@nl_cover,@nl_dlink,@menuID,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("@nl_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_title",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_issue",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_doissue",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_cover",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_dlink",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),                                 
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["nl_title"].ToString();
                parms[2].Value = dr["nl_issue"].ToString();
                parms[3].Value = dr["nl_doissue"].ToString();
                parms[4].Value = dr["nl_cover"].ToString();
                parms[5].Value = dr["nl_dlink"].ToString();
                parms[6].Value = dr["menuID"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = false;
                parms[9].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("NL", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateNewsletter(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblNewsletter SET nl_title=@nl_title,nl_issue=@nl_doissue,nl_doissue=@nl_issue,nl_cover=@nl_cover,nl_dlink=@nl_dlink,menuID=@menuID,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where nl_ID=@nl_ID");

                SqlParameter[] parms = {
                                   new SqlParameter("@nl_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_title",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_issue",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_doissue",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_cover",SqlDbType.NVarChar),
                                    new SqlParameter("@nl_dlink",SqlDbType.NVarChar),
                                    new SqlParameter("@menuID",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["nl_ID"].ToString();
                parms[1].Value = dr["nl_title"].ToString();
                parms[2].Value = dr["nl_issue"].ToString();
                parms[3].Value = dr["nl_doissue"].ToString();
                parms[4].Value = dr["nl_cover"].ToString();
                parms[5].Value = dr["nl_dlink"].ToString();
                parms[6].Value = dr["menuID"].ToString();
                parms[7].Value = dr["lang"].ToString();
                parms[8].Value = false;
                parms[9].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getNewsletter()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblNewsletter f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteNewsletterByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblNewsletter set deleteFlag=1 where nl_ID=@nl_ID";
                SqlParameter[] parms = { new SqlParameter("@nl_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getNewsletterbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNewsletter f  left join tb_Menu m on f.menuID=m.m_id where f.deleteFlag=0 and f.nl_title like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getNewsletterByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select nl_title from tblNewsletter where nl_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "nl_title").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable selectNewsletterMenu()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tb_Menu where m_PageType='NewsLetters' and Status <> 'Deleted'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getNewsletterlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblNewsletter where nl_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }


        //***Account
        public int SaveAccount(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("A");
                ID = "A" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblAccount ");
                str.Append("(a_ID,a_fullname,a_type,a_sal,a_fname,a_lname,a_designation,a_email,a_company,a_addressOfc,a_addressHome,a_country,a_Tel,a_Mobile,a_LoginName,a_Password,a_isActivated,lang,deleteFlag,UpdatedDate,a_AttendDay,Biography,ProfilePic)");
                str.Append(" values");
                str.Append("(@a_ID,@a_fullname,@a_type,@a_sal,@a_fname,@a_lname,@a_designation,@a_email,@a_company,@a_addressOfc,@a_addressHome,@a_country,@a_Tel,@a_Mobile,@a_LoginName,@a_Password,@a_isActivated,@lang,@deleteFlag,@UpdatedDate,@a_AttendDay,@Biography,@ProfilePic)");


                SqlParameter[] parms = {
                                    new SqlParameter("@a_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@a_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_type",SqlDbType.NVarChar),
                                    new SqlParameter("@a_sal",SqlDbType.NVarChar),
                                    new SqlParameter("@a_fname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_lname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_designation",SqlDbType.NVarChar),
                                    new SqlParameter("@a_email",SqlDbType.NVarChar),
                                    new SqlParameter("@a_company",SqlDbType.NVarChar),
                                    new SqlParameter("@a_addressOfc",SqlDbType.NVarChar),
                                    new SqlParameter("@a_addressHome",SqlDbType.NVarChar),
                                    new SqlParameter("@a_country",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Tel",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Mobile",SqlDbType.NVarChar),
                                    new SqlParameter("@a_LoginName",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Password",SqlDbType.NVarChar),
                                    new SqlParameter("@a_isActivated",SqlDbType.Bit),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("@a_AttendDay",SqlDbType.NVarChar),
                                    new SqlParameter("@Biography",SqlDbType.NVarChar),
                                    new SqlParameter("@ProfilePic",SqlDbType.NVarChar)
            };
                parms[0].Value = ID;
                parms[1].Value = dr["a_fullname"].ToString();
                parms[2].Value = dr["a_type"].ToString();
                parms[3].Value = dr["a_sal"].ToString();
                parms[4].Value = dr["a_fname"].ToString();
                parms[5].Value = dr["a_lname"].ToString();
                parms[6].Value = dr["a_designation"].ToString();
                parms[7].Value = dr["a_email"].ToString();
                parms[8].Value = dr["a_company"].ToString();
                parms[9].Value = dr["a_addressOfc"].ToString();
                parms[10].Value = dr["a_addressHome"].ToString();
                parms[11].Value = dr["a_country"].ToString();
                parms[12].Value = dr["a_Tel"].ToString();
                parms[13].Value = dr["a_Mobile"].ToString();
                parms[14].Value = dr["a_LoginName"].ToString();
                parms[15].Value = dr["a_Password"].ToString();
                parms[16].Value = bool.Parse(dr["a_isActivated"].ToString());
                parms[17].Value = dr["lang"].ToString();
                parms[18].Value = false;
                parms[19].Value = timestamp;
                parms[20].Value = dr["a_AttendDay"].ToString();
                parms[21].Value = dr["Biography"].ToString();
                parms[22].Value = dr["ProfilePic"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("A", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int DelegateLogin(string emailaddresss)
        {
            int result = 0;
            string sql = "Select * From tblAccount Where a_email='" + emailaddresss + "' And deleteFlag=0";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            result = dt.Rows.Count;
            return result;
        }
        public int checkAccountExistUpdate(string emailaddresss, string accountID)
        {
            int result = 0;
            string sql = "Select * From tblAccount Where a_email='" + emailaddresss + "' And a_ID!='" + accountID + "' And deleteFlag=0";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            result = dt.Rows.Count;
            return result;
        }
        public int DelegateLoginAuthenticate(string emailaddresss, string password)
        {
            int result = 0;
            string sql = "Select * From tblAccount Where a_email='" + emailaddresss + "' And a_Password='" + password + "' And deleteFlag=0";
            DataTable dt = fn.GetDatasetByCommand(sql, "ds").Tables[0];
            result = dt.Rows.Count;
            return result;
        }
        public int UpdateAccount(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblAccount SET a_fullname=@a_fullname,a_sal=@a_sal,a_type=@a_type,a_fname=@a_fname,a_lname=@a_lname,a_designation=@a_designation,");
                str.Append("a_email=@a_email,a_company=@a_company,a_addressOfc=@a_addressOfc,a_addressHome=@a_addressHome,a_country=@a_country,a_Tel=@a_Tel,a_Mobile=@a_Mobile,a_LoginName=@a_LoginName,");
                str.Append("a_Password=@a_Password,a_isActivated=@a_isActivated,lang =@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate,a_AttendDay=@a_AttendDay,Biography=@Biography,ProfilePic=@ProfilePic where a_ID=@a_ID");

                SqlParameter[] parms = {
                                   new SqlParameter("@a_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@a_fullname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_type",SqlDbType.NVarChar),
                                    new SqlParameter("@a_sal",SqlDbType.NVarChar),
                                    new SqlParameter("@a_fname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_lname",SqlDbType.NVarChar),
                                    new SqlParameter("@a_designation",SqlDbType.NVarChar),
                                    new SqlParameter("@a_email",SqlDbType.NVarChar),
                                    new SqlParameter("@a_company",SqlDbType.NVarChar),
                                    new SqlParameter("@a_addressOfc",SqlDbType.NVarChar),
                                    new SqlParameter("@a_addressHome",SqlDbType.NVarChar),
                                    new SqlParameter("@a_country",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Tel",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Mobile",SqlDbType.NVarChar),
                                    new SqlParameter("@a_LoginName",SqlDbType.NVarChar),
                                    new SqlParameter("@a_Password",SqlDbType.NVarChar),
                                    new SqlParameter("@a_isActivated",SqlDbType.Bit),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar),
                                    new SqlParameter("@a_AttendDay",SqlDbType.NVarChar),
                                    new SqlParameter("@Biography",SqlDbType.NVarChar),
                                    new SqlParameter("@ProfilePic",SqlDbType.NVarChar)
            };
                parms[0].Value = dr["a_ID"].ToString();
                parms[1].Value = dr["a_fullname"].ToString();
                parms[2].Value = dr["a_type"].ToString();
                parms[3].Value = dr["a_sal"].ToString();
                parms[4].Value = dr["a_fname"].ToString();
                parms[5].Value = dr["a_lname"].ToString();
                parms[6].Value = dr["a_designation"].ToString();
                parms[7].Value = dr["a_email"].ToString();
                parms[8].Value = dr["a_company"].ToString();
                parms[9].Value = dr["a_addressOfc"].ToString();
                parms[10].Value = dr["a_addressHome"].ToString();
                parms[11].Value = dr["a_country"].ToString();
                parms[12].Value = dr["a_Tel"].ToString();
                parms[13].Value = dr["a_Mobile"].ToString();
                parms[14].Value = dr["a_LoginName"].ToString();
                parms[15].Value = dr["a_Password"].ToString();
                parms[16].Value = bool.Parse(dr["a_isActivated"].ToString());
                parms[17].Value = dr["lang"].ToString();
                parms[18].Value = false;
                parms[19].Value = timestamp;
                parms[20].Value = dr["a_AttendDay"].ToString();
                parms[21].Value = dr["Biography"].ToString();
                parms[22].Value = dr["ProfilePic"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getAccount()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblAccount f where f.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteAccountByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblAccount set deleteFlag=1 where a_ID=@a_ID";
                SqlParameter[] parms = { new SqlParameter("@a_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public int DeleteAccountImageByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblAccount set ProfilePic='' where a_ID=@a_ID";
                SqlParameter[] parms = { new SqlParameter("@a_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getAccountbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblAccount f where f.deleteFlag=0 and f.a_fullname like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getAccountByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select a_fullname from tblAccount where a_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "a_fullname").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getAccountlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblAccount where a_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***EPoster
        public int SaveEPoster(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("EP");
                ID = "EP" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into [tblE-Poster] ");
                str.Append("(ep_ID,ep_title,ep_category,ep_author,ep_coauthor,ep_venue,ep_duration,ep_file,ep_abstract,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@ep_ID,@ep_title,@ep_category,@ep_author,@ep_coauthor,@ep_venue,@ep_duration,@ep_file,@ep_abstract,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("@ep_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_title",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_category",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_author",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_coauthor",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_venue",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_duration",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_file",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_abstract",SqlDbType.NVarChar),                                  
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["ep_title"].ToString();
                parms[2].Value = dr["ep_category"].ToString();
                parms[3].Value = dr["ep_author"].ToString();
                parms[4].Value = dr["ep_coauthor"].ToString();
                parms[5].Value = dr["ep_venue"].ToString();
                parms[6].Value = dr["ep_duration"].ToString();
                parms[7].Value = dr["ep_file"].ToString();
                parms[8].Value = dr["ep_abstract"].ToString();
                parms[9].Value = dr["lang"].ToString();
                parms[10].Value = false;
                parms[11].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("EP", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateEPoster(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE [tblE-Poster] SET ep_title=@ep_title,ep_author=@ep_author,ep_category=@ep_category,ep_coauthor=@ep_coauthor,ep_venue=@ep_venue,ep_duration=@ep_duration,");
                str.Append("ep_file=@ep_file,ep_abstract=@ep_abstract,");
                str.Append("lang =@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where ep_ID=@ep_ID");

                SqlParameter[] parms = {
                                   new SqlParameter("@ep_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_title",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_category",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_author",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_coauthor",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_venue",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_duration",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_file",SqlDbType.NVarChar),
                                    new SqlParameter("@ep_abstract",SqlDbType.NVarChar),                                   
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["ep_ID"].ToString();
                parms[1].Value = dr["ep_title"].ToString();
                parms[2].Value = dr["ep_category"].ToString();
                parms[3].Value = dr["ep_author"].ToString();
                parms[4].Value = dr["ep_coauthor"].ToString();
                parms[5].Value = dr["ep_venue"].ToString();
                parms[6].Value = dr["ep_duration"].ToString();
                parms[7].Value = dr["ep_file"].ToString();
                parms[8].Value = dr["ep_abstract"].ToString();
                parms[9].Value = dr["lang"].ToString();
                parms[10].Value = false;
                parms[11].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getEPoster()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from [tblE-Poster] e left join tblVenue_Booth vb on e.ep_venue=vb.vb_ID  Left OUTER JOIN tblSpeaker s on e.ep_author=s.s_ID where e.deleteFlag=0 and  vb.deleteFlag=0 and s.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteEPosterByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update [tblE-Poster] set deleteFlag=1 where ep_ID=@ep_ID";
                SqlParameter[] parms = { new SqlParameter("@ep_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getEPosterbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from [tblE-Poster] f where f.deleteFlag=0 and f.ep_title like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getEPosterByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select ep_title from [tblE-Poster]where ep_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "ep_title").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getEPosterlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from [tblE-Poster]  e left join tblVenue_Booth vb on e.ep_venue=vb.vb_ID  where ep_ID='" + id + "' and e.deleteFlag=0  and vb.deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***BusinessMatching
        public int SaveBusinessMatching(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("BM");
                ID = "BM" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblBusinessMatching");
                str.Append("(bm_ID,bm_vid,bm_eid,bm_date,bm_time,bm_status,bm_ecomment,bm_reqtime,bm_vcomment,bm_interestedProduct,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@bm_ID,@bm_vid,@bm_eid,@bm_date,@bm_time,@bm_status,@bm_ecomment,@bm_reqtime,@bm_vcomment,@bm_interestedProduct,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("@bm_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_vid",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_eid",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_date",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_time",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_status",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_ecomment",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_reqtime",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_vcomment",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_interestedProduct",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["bm_vid"].ToString();
                parms[2].Value = dr["bm_eid"].ToString();
                parms[3].Value = dr["bm_date"].ToString();
                parms[4].Value = dr["bm_time"].ToString();
                parms[5].Value = dr["bm_status"].ToString();
                parms[6].Value = dr["bm_ecomment"].ToString();
                parms[7].Value = dr["bm_reqtime"].ToString();
                parms[8].Value = dr["bm_vcomment"].ToString();
                parms[9].Value = dr["bm_interestedProduct"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("BM", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateBusinessMatching(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblBusinessMatching SET bm_vid=@bm_vid,bm_eid=@bm_eid,bm_date=@bm_date,bm_time=@bm_time,bm_status=@bm_status,");
                str.Append("bm_ecomment=@bm_ecomment,bm_reqtime=@bm_reqtime,bm_vcomment=@bm_vcomment,bm_interestedProduct=@bm_interestedProduct,");
                str.Append("lang =@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where bm_ID=@bm_ID");

                SqlParameter[] parms = {
                                   new SqlParameter("@bm_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_vid",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_eid",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_date",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_time",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_status",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_ecomment",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_reqtime",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_vcomment",SqlDbType.NVarChar),
                                    new SqlParameter("@bm_interestedProduct",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["bm_ID"].ToString();
                parms[1].Value = dr["bm_vid"].ToString();
                parms[2].Value = dr["bm_eid"].ToString();
                parms[3].Value = dr["bm_date"].ToString();
                parms[4].Value = dr["bm_time"].ToString();
                parms[5].Value = dr["bm_status"].ToString();
                parms[6].Value = dr["bm_ecomment"].ToString();
                parms[7].Value = dr["bm_reqtime"].ToString();
                parms[8].Value = dr["bm_vcomment"].ToString();
                parms[9].Value = dr["bm_interestedProduct"].ToString();
                parms[10].Value = dr["lang"].ToString();
                parms[11].Value = false;
                parms[12].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getBusinessMatching()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblBusinessMatching f where f.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteBusinessMatchingByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblBusinessMatching set deleteFlag=1 where bm_ID=@bm_ID";
                SqlParameter[] parms = { new SqlParameter("@bm_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getBusinessMatchingbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblBusinessMatching f where f.deleteFlag=0 and f.bm_vid like '%" + searchkey + "%' or f.bm_eid like '%"+searchkey+"%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getBusinessMatchingByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select bm_vid from tblBusinessMatching where bm_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "bm_vid").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public int SaveSpeakerCategoryGetID(DataRow dr, ref string spkcatID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("SPKC");
                ID = "SPKC" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblSpeakerCategory ");
                str.Append("(speakerCat_ID,speakerCat_name,speakerCat_seq,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@speakerCat_ID,@speakerCat_name,@speakerCat_seq,@lang,@deleteFlag,@UpdatedDate)");

                SqlParameter[] parms = {
                                            new SqlParameter("speakerCat_ID",SqlDbType.NVarChar),
                                            new SqlParameter("speakerCat_name",SqlDbType.NVarChar),
                                            new SqlParameter("speakerCat_seq",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.NVarChar)
                                        };
                spkcatID = ID;
                parms[0].Value = ID;
                parms[1].Value = dr["cc_categoryname"].ToString();
                parms[2].Value = dr["cc_seq"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("SPKC", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getBusinessMatchinglstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblBusinessMatching where bm_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

        //***Moments
        public int SaveMoments(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("M");
                ID = "M" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblMoments");
                str.Append("(m_ID,m_txtcomment,m_photocomment,m_status,m_addedtime,lang,deleteFlag,UpdatedDate)");
                str.Append(" values");
                str.Append("(@m_ID,@m_txtcomment,@m_photocomment,@m_status,@m_addedtime,@lang,@deleteFlag,@UpdatedDate)");


                SqlParameter[] parms = {
                                    new SqlParameter("@m_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@m_txtcomment",SqlDbType.NVarChar),
                                    new SqlParameter("@m_photocomment",SqlDbType.NVarChar),
                                    new SqlParameter("@m_status",SqlDbType.NVarChar),
                                    new SqlParameter("@m_addedtime",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = ID;
                parms[1].Value = dr["m_txtcomment"].ToString();
                parms[2].Value = dr["m_photocomment"].ToString();
                parms[3].Value = dr["m_status"].ToString();
                parms[4].Value = dr["m_addedtime"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;


                res = fn.ExecuteNonQuery(str.ToString(), parms);

                gk.SaveKey("M", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateMoments(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblMoments SET m_txtcomment=@m_txtcomment,m_photocomment=@m_photocomment,m_status=@m_status,m_addedtime=@m_addedtime,");
                str.Append("lang =@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate where m_ID=@m_ID");

                SqlParameter[] parms = {
                                    new SqlParameter("@m_ID",SqlDbType.NVarChar),
                                    new SqlParameter("@m_txtcomment",SqlDbType.NVarChar),
                                    new SqlParameter("@m_photocomment",SqlDbType.NVarChar),
                                    new SqlParameter("@m_status",SqlDbType.NVarChar),
                                    new SqlParameter("@m_addedtime",SqlDbType.NVarChar),
                                    new SqlParameter("@lang",SqlDbType.NVarChar),
                                    new SqlParameter("@deleteFlag",SqlDbType.Bit),
                                    new SqlParameter("@UpdatedDate",SqlDbType.NVarChar)

            };
                parms[0].Value = dr["m_ID"].ToString();
                parms[1].Value = dr["m_txtcomment"].ToString();
                parms[2].Value = dr["m_photocomment"].ToString();
                parms[3].Value = dr["m_status"].ToString();
                parms[4].Value = dr["m_addedtime"].ToString();
                parms[5].Value = dr["lang"].ToString();
                parms[6].Value = false;
                parms[7].Value = timestamp;

                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public DataTable getMoments()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblMoments f where f.deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int DeleteMomentsByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblMoments set deleteFlag=1 where m_ID=@m_ID";
                SqlParameter[] parms = { new SqlParameter("@m_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getMomentsbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblMoments f where f.deleteFlag=0 and f.m_txtcomment like '%" + searchkey + "%' or f.m_photocomment like '%" + searchkey + "%'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public string getMomentsByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select m_txtcomment from tblMoments where m_ID='" + ID + "' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "m_txtcomment").ToString();
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getMomentslstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblMoments where m_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }

      
        //**** Polling
        public DataTable selectPollingQABySID(string sessionID)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = string.Format("select * from PollingQandA where sessionID='{0}'", sessionID);
            dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        //***LiveQ&A
        public DataTable selectQASessions() { 
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from tblConference where c_liveQA=1 and deleteFlag=0", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSQAbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConference where c_title like '%" + searchkey + "%' and c_liveQA=1  and deleteFlag=0 ";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable selectQADetail(string SessionID)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("select * from LQA where SessionID='"+SessionID+"'", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getSQADetailbyKeywords(string searchkey,string SessionID)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from LQA where LQATitle like '%" + searchkey + "%' and SessionID='" + SessionID + "'";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getQAlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from LQA where LQAID='" + id + "'";
                dt = fn.GetDatasetByCommand(sql, "sdtSQA").Tables[0];

              
            }
            catch(Exception ex) { }
            return dt;
        }
        public DataTable selectPollingQABySIDPS1(string sessionID)
        {
           
            DataTable dt = new DataTable();
            try
            {
                string sql = string.Format("select * from PollingQandA where sessionID='{0}' and pollingStatus=1", sessionID);
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public int UpdateLQATitle(DataRow dr)
        {
            int res = 0;      
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE LQA SET LQATitle=@LQATitle,");
                str.Append("dtime=@dtime where LQAID=@LQAID");

                SqlParameter[] parms = {
                                    new SqlParameter("@LQAID",SqlDbType.NVarChar),
                                    new SqlParameter("@LQATitle",SqlDbType.NVarChar),
                                    new SqlParameter("@dtime",SqlDbType.DateTime)                             
            };
                parms[0].Value = dr["LQAID"].ToString();
                parms[1].Value = dr["LQATitle"].ToString();          
                parms[2].Value = dr["dtime"].ToString();

                res = fn.ExecuteNonQuery(str.ToString(), parms);


            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateStatus(string iD,string status)
        {
            int result = 0;
            try
            {
                var sql = "UPDATE LQA SET LQAStatus=@LQAStatus,dtime=@dtime,QAType=@QAType where LQAID=@LQAID";
                SqlParameter[] parms = {
                                    new SqlParameter("@LQAID",SqlDbType.NVarChar),
                                    new SqlParameter("@LQAStatus",SqlDbType.NVarChar),
                                    new SqlParameter("@dtime",SqlDbType.DateTime),
                                     new SqlParameter("@QAType",SqlDbType.NVarChar)
            };
                parms[0].Value = iD;
                parms[1].Value = status;
                parms[2].Value = DateTime.Now;
                parms[3].Value = "0";
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public string getQATypeByID(string ID)
        {
            string result = string.Empty;
            try
            {
                var sql = "select QAType from LQA where LQAID='" + ID + "'";
                result = fn.GetDataByCommand(sql, "QAType").ToString();
            }
            catch (Exception ex) { }
            return result;
        }


        public void UpdateDataVersion(string tablename)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                string result = string.Empty;
                var sql = "select a_dataVersion from tblActivatedForms where a_activatedFormName='"+tablename+"' and deleteFlag=0";
                result = fn.GetDataByCommand(sql, "a_dataVersion").ToString();
                if (!string.IsNullOrEmpty(result))
                {
                    int count = int.Parse(result);
                    count += 1;                  
                    str.Append("update tblActivatedForms set a_dataVersion=" + count + " where a_activatedFormName='" + tablename + "' and deleteFlag=0");
                }else
                {
                    str.Append("update tblActivatedForms set a_dataVersion=" + 1 + " where a_activatedFormName='" + tablename + "' and deleteFlag=0");
                }
                    fn.ExecuteSQL(str.ToString());
            }
            catch (Exception ex) { }
        }


        #region Conference Account
        //***Conference Account
        public int SaveConferenceAccount(DataRow dr, ref string confaccountID)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                GenKey gk = new GenKey();

                string fkey = string.Empty; string ID = string.Empty;
                fkey = gk.getKey("CA");
                ID = "CA" + fkey;

                StringBuilder str = new StringBuilder();
                str.Append("insert into tblConferenceAccount ");
                str.Append("(confaccount_ID,confaccount_ConferenceID,confaccount_AccountID,lang,deleteFlag,UpdatedDate,sorting)");
                str.Append(" values");
                str.Append("(@confaccount_ID,@confaccount_ConferenceID,@confaccount_AccountID,@lang,@deleteFlag,@UpdatedDate,@sorting)");

                SqlParameter[] parms = {
                                            new SqlParameter("confaccount_ID",SqlDbType.NVarChar),
                                            new SqlParameter("confaccount_ConferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("confaccount_AccountID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.DateTime),
                                            new SqlParameter("sorting",SqlDbType.NVarChar)
                                        };
                parms[0].Value = ID;
                parms[1].Value = dr["confaccount_ConferenceID"].ToString();
                parms[2].Value = dr["confaccount_AccountID"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = DateTime.Now;
                parms[6].Value = dr["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dr["sorting"].ToString()) ? dr["sorting"].ToString() : "99") : "99";

                res = fn.ExecuteNonQuery(str.ToString(), parms);
                confaccountID = ID;

                gk.SaveKey("CA", int.Parse(fkey));
            }
            catch (Exception ex) { }
            return res;
        }
        public int UpdateConferenceAccount(DataRow dr)
        {
            int res = 0;
            DateTime dtime = DateTime.Now;
            string timestamp = ConvertToTimestamp(dtime);
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConferenceAccount SET confaccount_ConferenceID=@confaccount_ConferenceID,confaccount_AccountID=@confaccount_AccountID"
                    + " ,lang=@lang,deleteFlag=@deleteFlag,UpdatedDate=@UpdatedDate,sorting=@sorting where confaccount_ID=@confaccount_ID");

                SqlParameter[] parms = {
                                            new SqlParameter("confaccount_ID",SqlDbType.NVarChar),
                                            new SqlParameter("confaccount_ConferenceID",SqlDbType.NVarChar),
                                            new SqlParameter("confaccount_AccountID",SqlDbType.NVarChar),
                                            new SqlParameter("lang",SqlDbType.NVarChar),
                                            new SqlParameter("deleteFlag",SqlDbType.NVarChar),
                                            new SqlParameter("UpdatedDate",SqlDbType.DateTime),
                                            new SqlParameter("sorting",SqlDbType.NVarChar)
                                        };
                parms[0].Value = dr["confaccount_ID"].ToString();
                parms[1].Value = dr["confaccount_ConferenceID"].ToString();
                parms[2].Value = dr["confaccount_AccountID"].ToString();
                parms[3].Value = dr["lang"].ToString();
                parms[4].Value = false;
                parms[5].Value = DateTime.Now;
                parms[6].Value = dr["sorting"] != DBNull.Value ? (!string.IsNullOrEmpty(dr["sorting"].ToString()) && !string.IsNullOrWhiteSpace(dr["sorting"].ToString()) ? dr["sorting"].ToString() : "99") : "99";

                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            catch (Exception ex) { }
            return res;
        }
        public int DeleteConferenceAccountByID(string iD)
        {
            int result = 0;
            try
            {
                var sql = "update tblConferenceAccount set deleteFlag=1 where confaccount_ID=@confaccount_ID";
                SqlParameter[] parms = { new SqlParameter("@confaccount_ID", SqlDbType.NVarChar) };
                parms[0].Value = iD;
                result = fn.ExecuteNonQuery(sql, parms);
            }
            catch (Exception ex) { }
            return result;
        }
        public DataTable getConferenceAccount()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select cs.*,c.c_title, "
                                        + " ( "
                                        + " Select '; '+ ac.a_fullname "
                                        + " From tblAccount ac "
                                        + " Where ','+ cs.confaccount_AccountID +',' like '%,'+ cast(ac.a_ID as varchar(10))+',%' "
                                        + " for xml path(''), type "
                                        + " ).value('substring(text()[1], 3)', 'varchar(max)') as a_fullname "
                                        + " From tblConferenceAccount as cs "
                                        + " Inner Join tblConference as c On cs.confaccount_ConferenceID=c.c_ID "
                                        + " Where cs.deleteFlag=0"
                                        , "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceAccountlstByID(string id)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "select * from tblConferenceAccount where confaccount_ID='" + id + "' and deleteFlag=0";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getConferenceAccountbyKeywords(string searchkey)
        {
            DataTable dt = new DataTable();
            try
            {
                var sql = "Select * From (Select cs.*,c.c_title, "
                        + " ( "
                        + " Select '; '+ ac.a_fullname "
                        + " From tblAccount ac "
                        + " Where ','+ cs.confaccount_AccountID +',' like '%,'+ cast(ac.a_ID as varchar(10))+',%' "
                        + " for xml path(''), type "
                        + " ).value('substring(text()[1], 3)', 'varchar(max)') as a_fullname "
                        + " From tblConferenceAccount as cs "
                        + " Inner Join tblConference as c On cs.confaccount_ConferenceID=c.c_ID "
                        + " Where cs.deleteFlag=0) as a"
                        + " Where (a.c_title Like '%" + searchkey + "%' Or a_fullname Like '%" + searchkey + "%')";
                dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            }
            catch (Exception ex) { }
            return dt;
        }
        #endregion
        #region added for zoom api
        public DataTable getZoomConfiguration()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tbl_ZoomConfiguration", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public DataTable getTimeZones()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = fn.GetDatasetByCommand("Select * From tblTimeZones", "sdt").Tables[0];
            }
            catch (Exception ex) { }
            return dt;
        }
        public void saveToken(string accessToken)
        {
            string sql = "Insert Into tblToken (AccessToken) Values ('" + accessToken + "')";
            fn.ExecuteSQL(sql);
        }
        public string getToken()
        {
            string accesstoken = "";
            try
            {
                string sql = "Select Top 1 AccessToken From tblToken Order By CreatedDate Desc";
                accesstoken = fn.GetDataByCommand(sql, "AccessToken");
                if(string.IsNullOrEmpty(accesstoken) || accesstoken == "0")
                {
                    accesstoken = "";
                }
            }
            catch(Exception ex)
            { }

            return accesstoken;
        }
        public int saveZoomConfID(string confID, string zoomConfID, string configurationID)
        {
            int isSuccess = 0;
            try
            {
                StringBuilder str = new StringBuilder();
                str.Append("UPDATE tblConference SET ");
                str.Append(" ZoomId='" + zoomConfID + "', ConfigId='" + configurationID + "'");
                str.Append(" WHERE c_ID='" + confID + "'");
                Functionality fn = new Functionality();
                fn.ExecuteSQL(str.ToString());
                isSuccess = 1;
            }
            catch (Exception ex)
            { }

            return isSuccess;
        }
        #endregion
    }
    
}
