﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.DAL;
using App.Common;
using App.Object;
using System.Data;
using System.Web;

namespace App.DAL
{
    public class DAL_FrontEnd
    {
        #region Delaration
        int success = 0;
        int count = 0;
        public int TotalCount { get; set; }

        public FrontEnd fe;
        IsabellaFormEntities db = new IsabellaFormEntities();

        List<BackEnd_Child> ShowHeaderList = new List<BackEnd_Child>();
        DAL_Child dalc = new DAL_Child();
        List<FrontEnd> lstfe = new List<FrontEnd>();
        List<FrontEndModel> lstfmodel = new List<FrontEndModel>();
        List<string> lstrelationpid = new List<string> { "BP10025", "BP10016", "BP10017", "BP10018", "BP10014" };       
        
        #endregion

        #region Dashboard
        public string SelectVisitorCount()
        {
            string rtn = string.Empty;

            //var res = (from a in db.FrontEnds
            //           where a.f_pid == "BP10001" && a.f_content == "Visitor"
            //           select a).ToList();
            //if(res.Count>0)
            //{
            //    rtn = res.Count.ToString();
            //}
            return rtn;
        }

        public string SelectActivatedUserCount()
        {
            string rtn = string.Empty;

            rtn = fn.GetDataByCommand("select count(a_ID) acount from tblAccount where a_isActivated=1 and deleteFlag=0", "acount");
            return rtn;
        }

        public string SelectFidByFCRowidLabel(string sessionid, string label)
        {
            var id = (from aa in db.FrontEnds
                      where aa.f_label == label && aa.f_c_rowid == sessionid
                      select aa).ToList();
            string rtnid=id[0].f_id.ToString();
            return rtnid;
                   
        }

        public List<PollingQandA> GetPollingQA(string sid)
        {
            
            var res = (from aa in db.PollingQandAs
                       where aa.pollingStatus == 1 && aa.sessionID == sid
                       select aa).ToList();
            return res;
        }

        public DataTable selectBusinessMatchingReportByDay(string v)
        {
            DataTable dt = new DataTable();
            //dt.Columns.Add("Total");
            //dt.Columns.Add("Desc");
            //var f_c_rowid = (from aa in db.FrontEnds
            //         where aa.f_pid == v
            //         select aa.f_c_rowid).ToList().Distinct();

            //var eventdate = (from bb in db.FrontEnds
            //         where f_c_rowid.Contains(bb.f_c_rowid) && bb.f_pid == v && bb.f_label == "Date"
            //         select bb).ToList();
            //List<string> lst = new List<string>();
            //foreach(var e in eventdate)
            //{
            //    if(!lst.Contains(e.f_content))
            //    {
            //        var a = (from aa in db.FrontEnds
            //                 where f_c_rowid.Contains(aa.f_c_rowid) && aa.f_pid == v && aa.f_label == "Date" && aa.f_content == e.f_content
            //                 select aa).ToList();
            //        DataRow drow = dt.NewRow();
            //        drow["Desc"] = e.f_content;
            //        drow["Total"] = a.Count;
            //        dt.Rows.Add(drow);
            //        lst.Add(e.f_content);
            //    }

            //}
            return dt;          
        }

        
        public string SelectExhibitorCount()
        {
            string rtn = string.Empty;

            string res = fn.GetDataByCommand("select count(ec_ID) ecount from tblExhibitingCompany where deleteFlag=0", "ecount");
            
            return res; 
        }

        public int CheckBM(string visitorID, string exhibitorID, string date, string time)
        {
            int rtn = 0;
            //bm_vid , bm_eid, bm_date, bm_time
            var bm_vid_rowid = (from aa in db.FrontEnds
                          where aa.f_content == visitorID
                          select aa.f_c_rowid).FirstOrDefault();

            var bm_eid_rowid = (from aa in db.FrontEnds
                         where aa.f_content == exhibitorID
                         select aa.f_c_rowid).FirstOrDefault();

            var bm_date_rowid = (from aa in db.FrontEnds
                          where aa.f_content == date
                          select aa.f_c_rowid).FirstOrDefault();
            var bm_time_rowid = (from aa in db.FrontEnds
                          where aa.f_content == time
                          select aa.f_c_rowid).FirstOrDefault();
            if(bm_vid_rowid == bm_eid_rowid && bm_date_rowid == bm_time_rowid && bm_vid_rowid == bm_date_rowid)
            {
                rtn = 1;
            }
            return rtn;
        }
        #endregion

        #region SelectAllData
        public List<FrontEnd> SelectAllData(int startIndex, int PageSize, out int TotalCount)
        {
            List<FrontEnd> lst = new List<FrontEnd>();
            TotalCount = 0;
            try
            {
                lst = (from Ab in db.FrontEnds
                       orderby Ab.f_id
                       select new FrontEnd
                       {
                           f_id = Ab.f_id,
                           f_pid = Ab.f_pid,
                           f_label = Ab.f_label,
                           f_content = Ab.f_content,
                           f_inputtype = Ab.f_inputtype,
                           f_sortorder = Ab.f_sortorder
                       }).ToList();
                TotalCount = lst.Count();
                lst = lst.Skip(PageSize * (startIndex - 1)).Take(PageSize).ToList();
                return lst.ToList();
            }
            catch (Exception e)
            {

            }
            return lst;
        }

        public string LoginMobile(string username, string password,string userType)
        {
            string rtn = string.Empty;
            if (userType == "V")
            {
                userType = "Visitor";
                var f_c_rowid = (from aa in db.FrontEnds
                                 where aa.f_content == username && aa.f_label == "Login Name"
                                 select aa.f_c_rowid).FirstOrDefault();
                var res1 = (from aa in db.FrontEnds
                            where aa.f_content == password && aa.f_c_rowid == f_c_rowid
                            select aa).FirstOrDefault();
                var res2 = (from aa in db.FrontEnds
                            where aa.f_content == userType && aa.f_c_rowid == f_c_rowid
                            select aa).FirstOrDefault();
                if (res1 != null && res2 != null && f_c_rowid == res1.f_c_rowid && f_c_rowid == res2.f_c_rowid && res1.f_c_rowid == res2.f_c_rowid && res1.f_pid == res2.f_pid)
                //if (res1 != null)
                {
                    var acctype = (from aa in db.FrontEnds
                                   where aa.f_c_rowid == f_c_rowid && aa.f_label == "Account Type"
                                   select aa.f_content).FirstOrDefault();
                    var sal = (from aa in db.FrontEnds
                               where aa.f_c_rowid == f_c_rowid && aa.f_label == "Salutation"
                               select aa.f_content).FirstOrDefault();
                    var fname = (from aa in db.FrontEnds
                                 where aa.f_c_rowid == f_c_rowid && aa.f_label == "First Name"
                                 select aa.f_content).FirstOrDefault();
                    var lname = (from aa in db.FrontEnds
                                 where aa.f_c_rowid == f_c_rowid && aa.f_label == "Last Name"
                                 select aa.f_content).FirstOrDefault();
                    var name = sal + " " + fname + " " + lname;
                    rtn = f_c_rowid + "^" + acctype + "^" + name;
                }
            }
            else if (userType == "U")
            {

                var f_c_rowid = (from aa in db.FrontEnds
                                 where aa.f_content == username && aa.f_label == "Login Name"
                                 select aa.f_c_rowid).FirstOrDefault();
                var res1 = (from aa in db.FrontEnds
                            where aa.f_content == password && aa.f_c_rowid == f_c_rowid
                            select aa).FirstOrDefault();            
             
                if (res1 != null)
                {
                    var acctype = (from aa in db.FrontEnds
                                   where aa.f_c_rowid == f_c_rowid && aa.f_label == "Account Type"
                                   select aa.f_content).FirstOrDefault();
                    var sal = (from aa in db.FrontEnds
                               where aa.f_c_rowid == f_c_rowid && aa.f_label == "Salutation"
                               select aa.f_content).FirstOrDefault();
                    var fname = (from aa in db.FrontEnds
                                 where aa.f_c_rowid == f_c_rowid && aa.f_label == "First Name"
                                 select aa.f_content).FirstOrDefault();
                    var lname = (from aa in db.FrontEnds
                                 where aa.f_c_rowid == f_c_rowid && aa.f_label == "Last Name"
                                 select aa.f_content).FirstOrDefault();
                    var name = sal + " " + fname + " " + lname;
                    rtn = f_c_rowid + "^" + acctype + "^" + name;
                }

            }
            return rtn;
        }

        public string Login(string username, string password)
        {

            string rtn = string.Empty;
            var f_c_rowid = (from aa in db.FrontEnds
                             where aa.f_content == username && aa.f_label == "Login Name"
                             select aa.f_c_rowid).FirstOrDefault();
            var res1 = (from aa in db.FrontEnds
                        where aa.f_content == password && aa.f_c_rowid == f_c_rowid
                        select aa).FirstOrDefault();
            
            if (res1 != null)
            {
                var acctype = (from aa in db.FrontEnds
                               where aa.f_c_rowid == f_c_rowid && aa.f_label == "Account Type"
                               select aa.f_content).FirstOrDefault();
                var sal = (from aa in db.FrontEnds
                           where aa.f_c_rowid == f_c_rowid && aa.f_label == "Salutation"
                           select aa.f_content).FirstOrDefault();
                var fname = (from aa in db.FrontEnds
                             where aa.f_c_rowid == f_c_rowid && aa.f_label == "First Name"
                             select aa.f_content).FirstOrDefault();
                var lname = (from aa in db.FrontEnds
                             where aa.f_c_rowid == f_c_rowid && aa.f_label == "Last Name"
                             select aa.f_content).FirstOrDefault();
                var name = sal + " " + fname + " " + lname;
                rtn = f_c_rowid + "^" + acctype + "^" + name;
            }
            return rtn;
        }
        #endregion

        #region Delete
        public int Delete(string ID)
        {
            success = 0;
            try
            {
                FrontEnd Ab = (from ls in db.FrontEnds where ls.f_id == ID select ls).First<FrontEnd>();
                if (Ab != null)
                {
                    Ab.f_deleteflag = true;
                    success = db.SaveChanges();

                }
            }
            catch { }
            return success;
        }
        #endregion

        #region Delete Record
        public int DeleteRecord(string rowid)
        {
            success = 0;
            try
            {
                var Ab = (from ls in db.FrontEnds where ls.f_c_rowid == rowid select ls).ToList();

                if (Ab != null)
                {
                    foreach (var frontend in Ab)
                    {
                        frontend.f_deleteflag = true;
                        success = db.SaveChanges();
                    }
                }
            }
            catch { }
            return success;
        }
        #endregion

        #region Save
        public int Save(FrontEnd obj)
        {
            success = 0;

            try
            {
                db.FrontEnds.Add(obj);
                success = db.SaveChanges();                
            }
            catch (Exception ex)
            { }
            return success;
        }

        public DataTable selectTop5AppointmentExhi()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ExhiID");
            dt.Columns.Add("Count");
            string sql = "select f_content as ExhiID, COUNT(f_content) as Count from FrontEnd where f_pid='BP10031' and f_c_id='BC10139' group by f_content";
            var tdt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            if(tdt.Rows.Count >0)
            {
                int i = 0;
                if (i < 5)
                {
                    foreach (DataRow dr in tdt.Rows)
                    {
                        DataRow drow = dt.NewRow();
                        var exhiname = selectContentByCIDRID("BC10119", dr["ExhiID"].ToString());
                        drow["ExhiID"] = exhiname;
                        drow["Count"] = dr["Count"];
                        dt.Rows.Add(drow);
                        i++;
                    }
                }                
               
            }

            return dt;

        }

        public DataTable selectlatestAppointment()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("vid");
            dt.Columns.Add("eid");
            dt.Columns.Add("date");
            dt.Columns.Add("time");
            dt.Columns.Add("status");
            string sqlrowid = "select f_c_rowid, max(f_createddate) from FrontEnd where f_pid=10031 GROUP BY f_c_rowid order by max(f_createddate) desc";
            var dtrowid = fn.GetDatasetByCommand(sqlrowid, "sdt").Tables[0];
            if(dtrowid.Rows.Count>0)
            {
                foreach(DataRow dr in dtrowid.Rows)
                {
                    string rid =dr["f_c_rowid"].ToString();
                    var res = (from aa in db.FrontEnds
                               where aa.f_c_rowid == rid
                               select aa).ToList();
                    if(res.Count>0)
                    {
                        DataRow drow = dt.NewRow();
                        drow["id"] = rid;
                        drow["vid"] = res[0].f_content.ToString();
                        drow["eid"] = res[1].f_content.ToString();
                        drow["date"] = res[2].f_content.ToString();
                        drow["time"] = res[3].f_content.ToString();
                        drow["status"] = res[4].f_content.ToString();
                        dt.Rows.Add(drow);
                    }
                }
                
            }

            return dt;
        }

        public string selectContentByCIDRID(string cid, string exhrowid)
        {
            var res = (from aa in db.FrontEnds
                       where aa.f_c_id == cid && aa.f_c_rowid == exhrowid
                       select aa.f_content).FirstOrDefault();
            return res;
        }

        public List<FrontEnd> selectFrontEndFromList(List<FrontEnd> lstFrontEnd)
        {
            List<FrontEnd> reslst = new List<FrontEnd>();
            var res1 = (from aa in lstFrontEnd
                       where aa.f_label == "Status"
                        select aa).FirstOrDefault();
            var res2 = (from aa in lstFrontEnd
                        where aa.f_label == "Exhibitor's Comment"
                        select aa).FirstOrDefault();
            reslst.Add(res1);
            reslst.Add(res2);
            return reslst;
        }

        public string getyearbydate(string content)
        {
            string year = string.Empty;
            var rowid = (from aa in db.FrontEnds
                       where aa.f_content == content
                       select aa.f_c_rowid).FirstOrDefault();
            if(rowid != null)
            {
                var res = (from bb in db.FrontEnds
                           where bb.f_c_rowid == rowid && bb.f_c_id == "BC10207"
                           select bb.f_content).FirstOrDefault();
                year = res.ToString();
            }
            return year;
        }

        #endregion

        #region CheckExist
        public int CheckExist(FrontEnd Pobj)
        {
            int count = 0;
            var query = (from dept in db.FrontEnds
                         where dept.f_id == Pobj.f_id
                         select dept).ToList();
            count = query.Count;

            return count;
        }
        #endregion

        #region Update
        public int update(FrontEnd Pobj)
        {
            int success = 0;
            var res = db.FrontEnds.FirstOrDefault(i => i.f_id == Pobj.f_id);
            if (res != null)
            {
                res.f_pid = Pobj.f_pid;
                res.f_inputtype = Pobj.f_inputtype;
                res.f_label = Pobj.f_label;
                res.f_sortorder = Pobj.f_sortorder;
                if (res.f_content != Pobj.f_content)
                {
                    res.f_content = Pobj.f_content;
                    success = db.SaveChanges();
                }
                else
                {
                    success = 1;
                }
            }
            return success;
        }
        public int updateForBackend(FrontEnd Pobj)
        {
            int success = 0;
            var res = db.FrontEnds.FirstOrDefault(i => i.f_id == Pobj.f_id);
            if (res != null)
            {
                res.f_pid = Pobj.f_pid;
                res.f_inputtype = Pobj.f_inputtype;
                res.f_label = Pobj.f_label;
                res.f_sortorder = Pobj.f_sortorder;
                success = db.SaveChanges();
            }
            return success;
        }
        #endregion

        #region SelectByPIdforEdit
        public List<ChildModel> SelectByPIdforEdit(string id)
        {

            List<ChildModel> lst = new List<ChildModel>();
            try
            {
                var query = from p in db.FrontEnds
                            where p.f_pid == id
                            select new { ID = p.f_id, InputType = p.f_inputtype, lblName = p.f_label, Content = p.f_content };
                lst = query.ToList().Select(r => new ChildModel
                {
                    c_id = r.ID,
                    c_inputtype = r.InputType,
                    c_label = r.lblName,
                    c_content = r.Content
                }).ToList();
            }
            catch (Exception e)
            {

            }
            return lst;
        }

        public int CheckItemUsed(string rowid)
        {
            int rtn = 0;

            var chklst = (from aa in db.FrontEnds
                          where aa.f_c_rowid == rowid
                          select aa).ToList();
            if(chklst.Count >0)
            {            
                foreach(var c in chklst)
                {
                    var chk =(from aa in db.FrontEnds
                               where aa.f_content.Contains(c.f_id.ToString()) && aa.f_deleteflag==false
                               select aa).ToList();
                    if(chk.Count>0)
                    {
                        rtn = 1;
                    }
                }
            }
            
            return rtn;

        }

        public string selectSponsorCategorySeqByID(string catid)
        {
            var rowid= (from a in db.FrontEnds
                        where a.f_id == catid
                        select a.f_c_rowid).FirstOrDefault();
            var aa = (from a in db.FrontEnds
                      where a.f_c_rowid == rowid && a.f_label == "Sequence"
                      select a.f_content).FirstOrDefault();
            return aa;
        }

        public List<tb_Menu> selectMobileMenu()
        {
            var res = (from aa in db.tb_Menu
                       where aa.Status != "Deleted"
                       select aa).ToList();
            return res;
        }
        #endregion

        #region SelectByPIdforDataBind
        public List<FrontEndModel> SelectByPIdforDataBind(string name, string id,int lang)
        {
            FrontEndModel tmp = new FrontEndModel();
            lstfmodel = (from z in db.FrontEnds
                         where z.f_pid == id && z.f_label == name && z.f_lang==lang && z.f_deleteflag == false
                         select new FrontEndModel
                         {
                             f_id = z.f_id,
                             f_c_rowid = z.f_c_rowid,
                             f_pid = z.f_pid,
                             Data = z.f_content
                         }).ToList();
            return lstfmodel;
        }
        public List<FrontEndModel> SelectByPIdforDataBindCondition(string name, string id, string conidition, int lang)
        {
            FrontEndModel tmp = new FrontEndModel();
            List<FrontEndModel> lst = new List<FrontEndModel>();
            var res = (from aa in db.FrontEnds
                       where aa.f_content == conidition
                       select aa.f_c_rowid).ToList();
            if(res.Count>0)
            {
                foreach(var r in res)
                {
                    lstfmodel = (from z in db.FrontEnds
                                 where z.f_pid == id && z.f_label == name && z.f_c_rowid == r && z.f_lang == lang && z.f_deleteflag==false
                                 select new FrontEndModel
                                 {
                                     f_id = z.f_id,
                                     f_c_rowid = z.f_c_rowid,
                                     f_pid = z.f_pid,
                                     Data = z.f_content
                                 }).ToList();
                    if(lstfmodel.Count>0)
                    {
                        foreach(var l in lstfmodel)
                        {
                            if(!lst.Contains(l))
                            {
                                lst.Add(l);
                            }
                        }
                    }
                }
            }
            
            return lst;
        }
        #endregion

        #region SelectByPIdforChkDataBind
        public List<FrontEnd> SelectByPIdforChkDataBind(string name)
        {
            string label = "";
            string pid = string.Empty;
            string tbname = name.Split('^')[0];
            string colname = name.Split('^')[1];
            var pData = db.BackEnd_Parent.FirstOrDefault(i => i.p_name == tbname);
            if (pData != null)
            {
                pid = pData.p_id;
            }

            return db.FrontEnds.Where(i => i.f_pid == pid && i.f_label == colname).ToList();
        }
        #endregion

        #region SelectByPIDRowID
        public List<FrontEnd> SelectByPIDRowID(FrontEnd fe)
        {
            return db.FrontEnds.Where(i => i.f_pid == fe.f_pid && i.f_c_rowid == fe.f_c_rowid && i.f_c_isFinish == true && i.f_lang==fe.f_lang).OrderBy(f=>f.f_sortorder).ToList();
        }
        #endregion

        #region SelectDistinctRowID
        public List<FrontEnd> SelectDistinctRowID(List<FrontEnd> lstFrontEnd)
        {
            //return (from t in lstFrontEnd group t by t.f_c_rowid into g select g).ToList();        
            return lstFrontEnd.GroupBy(a => a.f_c_rowid ).Select(a => new FrontEnd() { f_c_rowid = a.Key}).ToList();
        }
        #endregion

        #region SelectDistinctRowID
        public List<FrontEnd> SelectByParRowID(List<FrontEnd> lstFrontEnd, string pid, string rowid)
        {
            return lstFrontEnd.Where(a => a.f_pid == pid && a.f_c_rowid == rowid).Select(a => a).ToList();
        }
        #endregion

        #region SelectByFieldCount
        public List<List<FrontEnd>> SelectByFieldCount(List<FrontEnd> lstFrontEnd, int fieldCount)
        {
            return lstFrontEnd.Select((x, i) => new { Index = i, Value = x })
                            .GroupBy(x => x.Index / fieldCount)
                            .Select(x => x.Select(v => v.Value).ToList())
                            .ToList();
        }
        #endregion

        #region SelectByPIdforList
        public DataTable SelectByPIdforList(string id,int lang)
        {
            int no = 1;
            DataTable dt = new DataTable();
            DataRow dr;
            ShowHeader(id);
            if (ShowHeaderList.Count > 0)
            {
                int count = ShowHeaderList.Count();
                dt.Columns.Add("No", typeof(string));
                foreach (var hd in ShowHeaderList)
                {
                    dt.Columns.Add(hd.c_label, typeof(string));
                }
                var lstresult = SelectByPId(id,lang);
                var lstresult1 = SelectDistinctRowID(lstresult);

                if (lstresult1.Count > 0)
                {
                    foreach (var en in lstresult1)
                    {
                        var lstresult2 = SelectByParRowID(lstresult, id, en.f_c_rowid);
                        if (lstresult2.Count > 0)
                        {
                            int fieldCount = 0;
                            if (ShowHeaderList.Count > 0)
                            {
                                fieldCount = ShowHeaderList.Count();
                            }

                            #region Real Data into DataTable
                            if (fieldCount > 0)
                            {
                                var lstRealResult = SelectByFieldCount(lstresult2, fieldCount);

                                if (lstRealResult.Count > 0)
                                {
                                    foreach (var oneRow in lstRealResult)
                                    {
                                        if (oneRow.Count > 0)
                                        {
                                            dr = dt.NewRow();
                                            dr["No"] = no;
                                            foreach (var chl in oneRow)
                                            {
                                                dr[chl.f_label] = chl.f_content;
                                            }
                                            no++;
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }

                    }
                }

            }
            return dt;
        }

        public int DeleteMobileMenu(string ID)
        {
            success = 0;
            try
            {
                tb_Menu Ab = (from ls in db.tb_Menu where ls.m_id == ID select ls).First<tb_Menu>();
                if (Ab != null)
                {
                    Ab.Status = "Deleted";
                    
                    success = db.SaveChanges();

                }
            }
            catch { }
            return success;
        }

        public int CheckMenuUsage(string iD)
        {
            int rtn = 0;

            var cres = (from aa in db.BackEnd_Child
                        where aa.c_content.Contains("OutsideTB")
                        select aa).ToList();
            if(cres.Count >0)
            {
                foreach(var c in cres)
                {
                    var res = (from aa in db.FrontEnds
                               where aa.f_c_id == c.c_id && aa.f_content == iD
                               select aa).ToList();
                    rtn += res.Count;
                }
            }
            return rtn;
        }

        //public int CheckMenuUsage(string iD)
        //{

        //}
        #endregion

        #region ShowHeader
        public void ShowHeader(string pid)
        {
            try
            {
                ShowHeaderList = dalc.SelectByPId(pid);
                //hdRowCount.Text = (ShowListHeader.Count() + 1).ToString();
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        #region SelectByPId
        public List<FrontEnd> SelectByPId(string id,int lang)
        {
            //var r = (from res in db.FrontEnds
            //           where res.f_pid == id && res.f_lang == lang && res.f_deleteflag == false
            //           select res).ToList();
            return db.FrontEnds.Where(i => i.f_pid == id && i.f_lang==lang && i.f_deleteflag==false).ToList();

        }
        public List<FrontEnd> SelectByPId(string id)
        {
            return db.FrontEnds.Where(i => i.f_pid == id && i.f_deleteflag==false).ToList();
        }
        public List<FrontEnd> SelectByPIdFlblFcontnet(string id, string flbl, string fcon)
        {
            List<FrontEnd> lst = new List<FrontEnd>();
            var rowidlst = db.FrontEnds.Where(i => i.f_pid == id && i.f_label == flbl && i.f_content == fcon).ToList();
            if (rowidlst.Count > 0)
            {
                foreach (var r in rowidlst)
                {
                    var res = (from aa in db.FrontEnds where aa.f_c_rowid == r.f_c_rowid select aa).ToList();
                    if(res.Count>0)
                    {
                        foreach(var i in res)
                        {
                            if (!lst.Contains(i))
                            {
                                lst.Add(i);
                            }
                        }
                    }
                  
                }
            }
            return lst;
        }
        #endregion

        #region SelectByCount
        public FrontEnd SelectByCount(List<FrontEnd> lstfchl, int fcount)
        {
            return lstfchl.Take(fcount).OrderByDescending(i => i.f_id).FirstOrDefault();
        }
        #endregion

        #region SelectLastRowID
        public string SelectLastRowID()
        {
            string res =string.Empty;
            var aares = (from cc in db.FrontEnds orderby cc.f_c_rowid descending select cc.f_c_rowid).FirstOrDefault();
            if (aares != null)
            {
                res = aares;
            }
            return res;
        }
        #endregion

        #region SelectForUpload
        public List<FrontEnd> SelectForUpload(string pid, string rowid)
        {
            return (from aa in db.FrontEnds
                    where aa.f_pid == pid && aa.f_c_rowid == rowid &&
                    aa.f_inputtype == 12 && aa.f_c_isFinish == true
                    select aa).ToList();
        }

        public string SelectPNameByPID(string pid)
        {
            var pname = (from aa in db.BackEnd_Parent
                         where aa.p_id == pid
                         select aa.p_name).ToString();
            return pname;

        }

        public List<FrontEnd> SelectByRowidPiD(string rowid, string pid)
        {
            return (from aa in db.FrontEnds
                    where aa.f_pid == pid && aa.f_c_rowid == rowid && aa.f_deleteflag == false
                    select aa).ToList();
        }
        public int updatetimestamp(string pid, string rowid, string timestamp)
        {

            int success = 0;
            List<FrontEnd> lst = db.FrontEnds.Where(i => i.f_pid == pid && i.f_c_rowid == rowid).ToList();
            if (lst != null)
            {
                foreach (FrontEnd res in lst)
                {
                    //res.f_id = res.f_id;
                    //res.f_content = res.f_content;  
                    //res.f_c_id = res.f_c_id;
                    //res.f_c_isFinish = res.f_c_isFinish;
                    //res.f_c_rowid = res.f_c_rowid;
                    //res.f_entrycount = res.f_entrycount;
                    //res.f_inputtype = res.f_inputtype;
                    //res.f_label = res.f_label;
                    //res.f_pid = res.f_pid;
                    //res.f_sortorder = res.f_sortorder;
                    res.f_createddate = timestamp;
                    success = db.SaveChanges();
                }
            }
            return success;
        }

        public int CheckBookmark(string visitorID, string exhibitorID)
        {
            int rtn = 0;
            //bmark_exhid ,bmark_userid 
            var bmark_userid = (from aa in db.FrontEnds
                                where aa.f_content == visitorID && aa.f_pid == "BP10040"
                                select aa.f_c_rowid).FirstOrDefault();

            var bmark_exhid = (from aa in db.FrontEnds
                                where aa.f_content == exhibitorID && aa.f_pid == "BP10040"
                               select aa.f_c_rowid).FirstOrDefault();
            if (bmark_userid == bmark_exhid && bmark_userid != null && bmark_exhid != null)
            {
                rtn = 1;
            }
            return rtn;
        }
        #endregion

        #region SelectAllImages       
        public List<FrontEnd> SelectAllImages()
        {
            var res = (from aa in db.FrontEnds
                       where aa.f_inputtype == 13 && (aa.f_content.Contains(".jpg") || aa.f_content.Contains(".png") || aa.f_content.Contains(".jpeg"))
                       select aa).ToList();
            return res;
        }
        #endregion

        #region Misc
        public string selectIDbyContent(string content)
        {
            string rtn = string.Empty;
            var res = (from aa in db.FrontEnds
                       where aa.f_content == content && aa.f_deleteflag==false
                       select aa).FirstOrDefault();
            if (res != null)
            {
                rtn = res.f_id;
            }
            else
            {
                var res2 = (from aa in db.tb_Menu
                           where aa.m_TitleEn == content
                           select aa).FirstOrDefault();
                if (res2 != null)
                {
                    rtn = res2.m_id;
                }
            }
            return rtn;
        }
        public List<FrontEnd> selectByPIDContent(string pid, string searchkey)
        {
            List<FrontEnd> lst = new List<FrontEnd>();
            List<FrontEnd> newlst = new List<FrontEnd>();
            var res = (from aa in db.FrontEnds
                       where aa.f_pid == pid && aa.f_content.Contains(searchkey) && aa.f_deleteflag==false
                       select aa.f_c_rowid).ToList();
            if(res.Count>0)
            {
                foreach(var i in res)
                {
                    lst = (from aa in db.FrontEnds
                                where aa.f_c_rowid == i && aa.f_deleteflag == false
                           select aa).ToList();
                    foreach(var l in lst)
                    {
                        if(!newlst.Contains(l))
                        {
                            newlst.Add(l);
                        }
                        
                    }
                }
                
            }
            return newlst;
        }
        public List<string> selectRowIDByContent(string _f_id)
        {
            var res = (from aa in db.FrontEnds
                       where aa.f_content == _f_id.ToString()
                       select aa.f_c_rowid).ToList();
            return res;
        }
        public int selectmaxRowID()
        {
            int max = 0;
            var rtn = (from res in db.FrontEnds
                       where res.f_c_isFinish == true
                       select res).ToList();
            if (rtn.Count != 0)
            {
                var vmax = rtn.Max(r => r.f_c_rowid);
                max = int.Parse(vmax.ToString()) + 1;
            }
            else
            {
                max = 1;
            }

            return max;
        }
        public string selectContentByID(string fid)
        {
            string aa = string.Empty;
            var rtn = (from res in db.FrontEnds
                       where res.f_id == fid
                       select res).FirstOrDefault();
            if(rtn != null)
            {
                aa = rtn.f_content.ToString();
            }
            return aa.ToString();
        }
        
        public void deleteByRowID(string delrowid)
        {
            var delst = (from aa in db.FrontEnds where aa.f_c_rowid == delrowid select aa).ToList();
            if (delst.Count > 0)
            {
                foreach (var del in delst)
                {
                    db.FrontEnds.Remove(del);
                    db.SaveChanges();
                }
            }
        }

        public int selectbyCID(string cid)
        {
            success = 0;
            try
            {
                FrontEnd Ab = (from ls in db.FrontEnds where ls.f_c_id == cid select ls).First<FrontEnd>();
                if (Ab != null)
                {
                    success = 1;
                }
            }
            catch { }
            return success;
        }
        public List<FrontEnd> selectbyPIDCID(string pid, string cid)
        {
            var lst = (from res in db.FrontEnds
                       where res.f_c_id == cid && res.f_pid == pid
                       select res).ToList();
            return lst;
        }
        public List<tb_ColorScheme> SelectAllColorScheme()
        {
            var query = (from dept in db.tb_ColorScheme
                         where dept.Status != "Deleted" && dept.ID == 1
                         select dept).ToList();
            return query;
        }
        public List<tb_Configuration> selectAllConfig()
        {
            var query = (from dept in db.tb_Configuration
                         where dept.Status != "Deleted" && dept.config_id == 1
                         select dept).ToList();
            return query;
        }
        public List<BackEnd_Child> LstShowHeaderList(string pid)
        {
            ShowHeaderList = new List<BackEnd_Child>();
            try
            {
                ShowHeaderList = dalc.SelectByPId(pid);
            }
            catch (Exception ex)
            {
            }
            return ShowHeaderList;
        }
        public DataTable BindTableForGrid(string pid,int lang)
        {
            DataTable dt = new DataTable();
            try
            {
                int no = 1;

                DataRow dr;
                ShowHeaderList = new List<BackEnd_Child>();
                ShowHeaderList = LstShowHeaderList(pid);
                if (ShowHeaderList.Count > 0)
                {
                    int count = ShowHeaderList.Count();
                    dt.Columns.Add("ID", typeof(string));
                    foreach (var hd in ShowHeaderList)
                    {
                        dt.Columns.Add(hd.c_colname, typeof(string));
                    }

                    dt.Columns.Add("UpdatedTime", typeof(string));

                    var lstresult = SelectByPId(pid,lang);
                    var lstresult1 = SelectDistinctRowID(lstresult);

                    if (lstresult1.Count > 0)
                    {
                        foreach (var en in lstresult1)
                        {
                            var lstresult2 = SelectByParRowID(lstresult, pid, en.f_c_rowid);
                            if (lstresult2.Count > 0)
                            {
                                int fieldCount = 0;
                                if (ShowHeaderList.Count > 0)
                                {
                                    fieldCount = ShowHeaderList.Count();
                                }                           

                                #region Real Data into DataTable
                                if (fieldCount > 0)
                                {
                                    var lstRealResult = SelectByFieldCount(lstresult2, fieldCount);

                                    if (lstRealResult.Count > 0)
                                    {
                                        string rowid = string.Empty;
                                        foreach (var oneRow in lstRealResult)
                                        {
                                            if (oneRow.Count > 0)
                                            {
                                                dr = dt.NewRow();
                                                dr["ID"] = oneRow[0].f_c_rowid.ToString();
                                                foreach (var chl in oneRow)
                                                {
                                                    rowid = chl.f_c_rowid;
                                                    var colname = selectLableByColname(chl.f_label, pid);
                                                    if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
                                                    {
                                                        if(!lstrelationpid.Contains(pid))
                                                        {
                                                            #region 
                                                            if (!string.IsNullOrEmpty(chl.f_content))
                                                            {
                                                                string fcontent = string.Empty;
                                                                if (chl.f_content.Contains(','))
                                                                {
                                                                    string[] arr = chl.f_content.Split(',');
                                                                    for (int i = 0; i < arr.Count(); i++)
                                                                    {
                                                                        string fid = arr[i];
                                                                        fcontent += selectContentByID(fid);                                                                        
                                                                        if (string.IsNullOrEmpty(fcontent))
                                                                        {
                                                                            fcontent += chl.f_content;//getMenuTitleByID(fid.ToString());
                                                                        }

                                                                        if (i < arr.Count() - 1)
                                                                        {
                                                                            fcontent += ",";
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string fid = chl.f_content;
                                                                    fcontent = selectContentByID(fid);                                                                  
                                                                    if (string.IsNullOrEmpty(fcontent))
                                                                    {
                                                                        //fcontent += getMenuTitleByID(fid.ToString());
                                                                        fcontent += chl.f_content;
                                                                    }
                                                                    //fcontent = fid.ToString();
                                                                }

                                                                dr[colname] = fcontent;
                                                            }
                                                            else
                                                            {
                                                                dr[colname] = "";
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            if (!string.IsNullOrEmpty(chl.f_content))
                                                            {
                                                                string fcontent = string.Empty;
                                                                if (chl.f_content.Contains(','))
                                                                {
                                                                    string[] arr = chl.f_content.Split(',');
                                                                    for (int i = 0; i < arr.Count(); i++)
                                                                    {
                                                                        string fid = arr[i];
                                                                        string valrowid = selectrowidbyfid(fid);
                                                                        fcontent += valrowid;

                                                                        if (i < arr.Count() - 1)
                                                                        {
                                                                            fcontent += ",";
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (colname == "cspeaker_category" || colname == "csponsor_scategory" || colname == "c_cc" || colname == "c_date" || colname == "c_venue" || colname == "ec_product")
                                                                    {
                                                                        string fid = chl.f_content;
                                                                        fcontent = selectContentByID(fid);
                                                                    }
                                                                    else
                                                                    {
                                                                        fcontent = selectrowidbyfid(chl.f_content);
                                                                    }

                                                                }

                                                                dr[colname] = fcontent;
                                                            }
                                                            else
                                                            {
                                                                dr[colname] = "";
                                                            }
                                                           
                                                        }

                                                    }
                                                    else if(chl.f_inputtype == 13)
                                                    {
                                                        if(!string.IsNullOrEmpty(chl.f_content))
                                                        {
                                                            dr[colname] = "https://event-reg.biz/MobileCET2019/Images/FrontEnd_Photos/" + chl.f_content;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dr[colname] = chl.f_content;
                                                    }
                                                }
                                                no++;
                                                dr["UpdatedTime"] = oneRow[0].f_createddate.ToString();
                                                dt.Rows.Add(dr);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        public DataTable BindTableForGridforDownload(string pid, int lang)
        {
            DataTable dt = new DataTable();
            try
            {
                int no = 1;

                DataRow dr;
                ShowHeaderList = new List<BackEnd_Child>();
                ShowHeaderList = LstShowHeaderList(pid);
                if (ShowHeaderList.Count > 0)
                {
                    int count = ShowHeaderList.Count();
                    dt.Columns.Add("ID", typeof(string));
                    foreach (var hd in ShowHeaderList)
                    {
                        dt.Columns.Add(hd.c_label, typeof(string));
                    }

                    var lstresult = SelectByPId(pid, lang);
                    var lstresult1 = SelectDistinctRowID(lstresult);

                    if (lstresult1.Count > 0)
                    {
                        foreach (var en in lstresult1)
                        {
                            var lstresult2 = SelectByParRowID(lstresult, pid, en.f_c_rowid);
                            if (lstresult2.Count > 0)
                            {
                                int fieldCount = 0;
                                if (ShowHeaderList.Count > 0)
                                {
                                    fieldCount = ShowHeaderList.Count();
                                }
                                #region Real Data into DataTable
                                if (fieldCount > 0)
                                {
                                    var lstRealResult = SelectByFieldCount(lstresult2, fieldCount);

                                    if (lstRealResult.Count > 0)
                                    {
                                        string rowid = string.Empty;
                                        foreach (var oneRow in lstRealResult)
                                        {
                                            if (oneRow.Count > 0)
                                            {
                                                dr = dt.NewRow();

                                                foreach (var chl in oneRow)
                                                {
                                                    rowid = chl.f_c_rowid;

                                                    if (chl.f_inputtype == 2)
                                                    {
                                                        dr[chl.f_label] = "xxxxxx";
                                                    }
                                                    else if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
                                                    {
                                                        if (!string.IsNullOrEmpty(chl.f_content))
                                                        {
                                                            string fid = chl.f_content;
                                                            string fcontent = selectContentByID(fid);
                                                            if (string.IsNullOrEmpty(fcontent))
                                                            {
                                                                fcontent = getMenuTitleByID(fid.ToString());
                                                            }
                                                            dr[chl.f_label] = fcontent;
                                                        }
                                                        else
                                                        {
                                                            dr[chl.f_label] = "";
                                                        }
                                                    }
                                                    else if (chl.f_inputtype == 5)
                                                    {
                                                        if (!string.IsNullOrEmpty(chl.f_content))
                                                        {
                                                            string decodedText = HttpUtility.HtmlDecode(chl.f_content);

                                                            dr[chl.f_label] = decodedText;
                                                        }
                                                        else
                                                        {
                                                            dr[chl.f_label] = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dr[chl.f_label] = chl.f_content;
                                                    }


                                                }
                                                dr["ID"] = no;
                                                no++;                                               
                                                dt.Rows.Add(dr);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return dt;
        }

        public DataTable BindTableForGridByCondition(string pid,string flbl,string fcon)
        {
            DataTable dt = new DataTable();
            try
            {
                int no = 1;

                DataRow dr;
                ShowHeaderList = new List<BackEnd_Child>();
                ShowHeaderList = LstShowHeaderList(pid);
                if (ShowHeaderList.Count > 0)
                {
                    int count = ShowHeaderList.Count();
                    dt.Columns.Add("ID", typeof(string));
                    foreach (var hd in ShowHeaderList)
                    {
                        dt.Columns.Add(hd.c_colname, typeof(string));
                    }

                    dt.Columns.Add("UpdatedTime", typeof(string));
                    
                    var lstresult = SelectByPIdFlblFcontnet(pid, flbl, fcon);
                    var lstresult1= SelectDistinctRowID(lstresult);
                    if (lstresult1.Count > 0)
                    {
                        foreach (var en in lstresult1)
                        {
                            var lstresult2 = SelectByParRowID(lstresult, pid, en.f_c_rowid);
                            if (lstresult2.Count > 0)
                            {
                                int fieldCount = 0;
                                if (ShowHeaderList.Count > 0)
                                {
                                    fieldCount = ShowHeaderList.Count();
                                }

                                #region Real Data into DataTable
                                if (fieldCount > 0)
                                {
                                    var lstRealResult = SelectByFieldCount(lstresult2, fieldCount);

                                    if (lstRealResult.Count > 0)
                                    {
                                        string rowid = string.Empty;
                                        foreach (var oneRow in lstRealResult)
                                        {
                                            if (oneRow.Count > 0)
                                            {
                                                dr = dt.NewRow();
                                                dr["ID"] = oneRow[0].f_c_rowid.ToString();
                                                foreach (var chl in oneRow)
                                                {
                                                    rowid = chl.f_c_rowid;
                                                    var colname = selectLableByColname(chl.f_label, pid);
                                                    if (chl.f_inputtype == 7 || chl.f_inputtype == 9 || chl.f_inputtype == 11)
                                                    {
                                                        if (!lstrelationpid.Contains(pid))
                                                        {
                                                            #region 
                                                            if (!string.IsNullOrEmpty(chl.f_content))
                                                            {
                                                                string fcontent = string.Empty;
                                                                if (chl.f_content.Contains(','))
                                                                {
                                                                    string[] arr = chl.f_content.Split(',');
                                                                    for (int i = 0; i < arr.Count(); i++)
                                                                    {
                                                                        string fid = arr[i];
                                                                        fcontent += selectContentByID(fid);
                                                                        if (string.IsNullOrEmpty(fcontent))
                                                                        {
                                                                            fcontent += chl.f_content;//getMenuTitleByID(fid.ToString());
                                                                        }

                                                                        if (i < arr.Count() - 1)
                                                                        {
                                                                            fcontent += ",";
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    string fid = chl.f_content;
                                                                    fcontent = selectContentByID(fid);
                                                                    if (string.IsNullOrEmpty(fcontent))
                                                                    {
                                                                        //fcontent += getMenuTitleByID(fid.ToString());
                                                                        fcontent += chl.f_content;
                                                                    }
                                                                    //fcontent = fid.ToString();
                                                                }

                                                                dr[colname] = fcontent;
                                                            }
                                                            else
                                                            {
                                                                dr[colname] = "";
                                                            }
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            if (!string.IsNullOrEmpty(chl.f_content))
                                                            {
                                                                string fcontent = string.Empty;
                                                                if (chl.f_content.Contains(','))
                                                                {
                                                                    string[] arr = chl.f_content.Split(',');
                                                                    for (int i = 0; i < arr.Count(); i++)
                                                                    {
                                                                        string fid = arr[i];
                                                                        string valrowid = selectrowidbyfid(fid);
                                                                        fcontent += valrowid;

                                                                        if (i < arr.Count() - 1)
                                                                        {
                                                                            fcontent += ",";
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (colname == "cspeaker_category" || colname == "csponsor_scategory")
                                                                    {
                                                                        string fid = chl.f_content;
                                                                        fcontent = selectContentByID(fid);
                                                                    }
                                                                    else
                                                                    {
                                                                        string valrowid = selectrowidbyfid(chl.f_content);
                                                                        fcontent = valrowid.ToString();
                                                                    }

                                                                }

                                                                dr[colname] = fcontent;
                                                            }
                                                            else
                                                            {
                                                                dr[colname] = "";
                                                            }

                                                        }

                                                    }
                                                    else if (chl.f_inputtype == 13)
                                                    {
                                                        if (!string.IsNullOrEmpty(chl.f_content))
                                                        {
                                                            dr[colname] = "https://event-reg.biz/MobileCET2019/Images/FrontEnd_Photos/" + chl.f_content;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        dr[colname] = chl.f_content;
                                                    }
                                                }
                                                no++;
                                                dr["UpdatedTime"] = oneRow[0].f_createddate.ToString();
                                                dt.Rows.Add(dr);
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
            }
            return dt;
        }
        private string selectLableByColname(string f_label, string pid)
        {
            string rtn = string.Empty;
            rtn = (from aa in db.BackEnd_Child
                   where aa.c_pid == pid && aa.c_label == f_label && aa.c_deleteflag== false
                   select aa.c_colname).FirstOrDefault();
            return rtn;

        }

        Functionality fn = new Functionality();
        public string getMenuTitleByID(string menuID)
        {
            string sql = "select m_TitleEn from tb_Menu where m_id='" + menuID+"'";
            string dt = fn.GetDataByCommand(sql, "m_TitleEn").ToString();
            return dt;
        }
        public void DataTableToExcel(System.Data.DataTable dtData, String FileName)
        {
            System.Web.UI.WebControls.GridView dgExport = null;
            //Current conversation
            System.Web.HttpContext curContext = System.Web.HttpContext.Current;
            //IO is used to export the file and return to excel
            System.IO.StringWriter strWriter = null;
            System.Web.UI.HtmlTextWriter htmlWriter = null;

            if (dtData != null)
            {
                //Set the encoding and attachment formats

                curContext.Response.AddHeader("content-disposition", "attachment;filename=" + System.Web.HttpUtility.UrlEncode(FileName, System.Text.Encoding.UTF8) + ".xls");
                curContext.Response.ContentType = "application nd.ms-excel";
                curContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
                curContext.Response.Charset = "GB2312";

                //Export Excel File
                strWriter = new System.IO.StringWriter();
                htmlWriter = new System.Web.UI.HtmlTextWriter(strWriter);

                //In order to solve dgData possible in the case of a page, no need to redefine a paging GridView
                dgExport = new System.Web.UI.WebControls.GridView();
                dgExport.DataSource = dtData.DefaultView;
                //dgExport.HeaderStyle.BackColor = System.Drawing.Color.Black;
                //dgExport.HeaderStyle.ForeColor = System.Drawing.Color.White;
                dgExport.AllowPaging = false;
                dgExport.DataBind();

                //Downloaded to the client
                dgExport.RenderControl(htmlWriter);
                curContext.Response.Write(strWriter.ToString());
                curContext.Response.End();
            }
        }
        #endregion

        #region Website Config
        public int CheckExistWS(string fullname, string prefix)
        {
            int rtn = 0;
            var res = (from aa in db.WesiteConfigurations
                       where aa.config_projname == fullname && aa.config_prefixname == prefix
                       select aa).FirstOrDefault();
            if (res != null)
            {
                rtn = res.config_id;
            }
            return rtn;
        }

        public int insertWebsiteConfigData(WesiteConfiguration WCobj)
        {
            int id = 0;
            success = 0;

            try
            {
                db.WesiteConfigurations.Add(WCobj);
                success = db.SaveChanges();

                if (success > 0)
                {
                    id = WCobj.config_id;
                }
            }
            catch (Exception ex)
            { }
            return id;
        }

        public int UpdateWebsiteConfigData(WesiteConfiguration WCobj)
        {

            int success = 0;
            var res = db.WesiteConfigurations.FirstOrDefault(i => i.config_id == WCobj.config_id);
            if (res != null)
            {
                res.config_id = WCobj.config_id;
                res.config_projname = WCobj.config_projname;
                res.config_prefixname = WCobj.config_prefixname;
                res.config_startdate = WCobj.config_startdate;
                res.config_closingdate = WCobj.config_closingdate;
                res.config_closingmessage = WCobj.config_closingmessage;
                res.config_smtpclient = WCobj.config_smtpclient;
                res.config_smtpusername = WCobj.config_smtpusername;
                res.config_smtppassword = WCobj.config_smtppassword;
                res.config_banner = WCobj.config_banner;
                res.config_siteicon = WCobj.config_siteicon;
                res.config_isShowBanner = WCobj.config_isShowBanner;
                res.config_copyright = WCobj.config_copyright;
                res.config_menulayout = WCobj.config_menulayout;
                res.config_deleteflag = WCobj.config_deleteflag;
                res.config_createddate = WCobj.config_createddate;
                success = db.SaveChanges();
            }
            return success;
        }

        public WesiteConfiguration selectAllWebsiteConfig()
        {
            var rtn = (from res in db.WesiteConfigurations
                       where res.config_deleteflag == false
                       select res).FirstOrDefault();
            return rtn;
        }

        #endregion

        #region ConvertToTimestamp
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public string ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return elapsedTime.TotalSeconds.ToString();
        }
        #endregion

        #region Polling
        public int DeletePollingResult(string sessionID, string questionID)
        {
            int success = 0;
            var res = (from aa in db.PollingResults
                       where aa.PR_Session == sessionID && aa.PR_Question == questionID
                       select aa).FirstOrDefault();

            if (res != null)
            {
                db.PollingResults.Remove(res);
                success = db.SaveChanges();
            }
            return success;
        }
        
        public string selectrowidbyfid(string sessionID)
        {
            var res = (from aa in db.FrontEnds
                       where aa.f_id == sessionID
                       select aa.f_c_rowid).FirstOrDefault();
            return res;
        }
        public string SelectQuestionBySession(string sessionID)
        {
            string questionID = string.Empty;

            var res = (from aa in db.PollingQandAs
                       where aa.sessionID == sessionID && aa.pollingStatus == 1
                       select aa.questionID).FirstOrDefault();
            if (!string.IsNullOrEmpty(res))
            {
                questionID = res;
            }
            return questionID;
        }

        public string CheckPollingAnswer(string userRef, string question)
        {
            string ID = string.Empty;
            var res = (from aa in db.PollingResults
                       where aa.PR_UserRef == userRef && aa.PR_Question == question
                       select aa).FirstOrDefault();
            if (res != null)
            {
                ID = res.PR_ID;
            }
            return ID;
        }

        public string SelectAnswerIDBySessionVoteNo(string sessionID, string voteno)
        {
            string asnwerID = string.Empty;

            var rowid = (from res in db.FrontEnds
                         where res.f_content == "Start"
                         select res).FirstOrDefault().f_c_rowid;
            var aa = (from res in db.FrontEnds
                      where res.f_c_rowid == rowid && res.f_content == sessionID
                      select res);
            if (aa != null)
            {
                var bb = (from res in db.FrontEnds
                          where res.f_label.Contains(voteno) && res.f_c_rowid == rowid
                          select res).FirstOrDefault();
                if (bb != null)
                {
                    asnwerID = bb.f_id.ToString();
                }
            }
            return asnwerID;

        }

        public void SavePollingResult(PollingResult pRobj)
        {
            try
            {
                db.PollingResults.Add(pRobj);
                success = db.SaveChanges();
            }
            catch (Exception ex)
            { }
        }

        public void UpdatePollingResult(PollingResult pRobj)
        {
            var res = db.PollingResults.FirstOrDefault(i => i.PR_ID == pRobj.PR_ID);
            if (res != null)
            {
                res.PR_Question = pRobj.PR_Question;
                res.PR_Session = pRobj.PR_Session;
                res.PR_UserRef = pRobj.PR_UserRef;
                res.PR_Time = pRobj.PR_Time;
                if (res.PR_Answer != pRobj.PR_Answer)
                {
                    res.PR_Answer = pRobj.PR_Answer;
                    success = db.SaveChanges();
                }
            }
        }

        public PollingQandA SelectPollingQAbyQIDSID(string questionID, string sessionID)
        {
            var res = (from aa in db.PollingQandAs
                       where aa.questionID == questionID && aa.sessionID == sessionID
                       select aa).FirstOrDefault();
            return res;
        }
        public List<PollingResult> SelectPollingResutBySIDQID(string sessionID, string questionID)
        {
            var res = (from aa in db.PollingResults
                       where aa.PR_Session == sessionID && aa.PR_Question == questionID
                       select aa).ToList();
            return res;
        }

        public int PRAnsCount(string sessionID, string questionID, int questionAns)
        {
            var res = (from aa in db.PollingResults
                       where aa.PR_Session == sessionID && aa.PR_Question == questionID && aa.PR_Answer == questionAns
                       select aa.PR_ID).ToList();
            int count = res.Count;
            return count;
        }
        public string selectIDbyrowidlabel(string rowid, string label)
        {
            var fid = (from aa in db.FrontEnds
                       where aa.f_c_rowid == rowid && aa.f_label == label
                       select aa.f_id).FirstOrDefault();
            return fid;
        }
       
        public int DeletePollingQuestion(string sessionID, string questionID)
        {
            int success = 0;
            var res = (from aa in db.PollingQandAs
                       where aa.sessionID == sessionID && aa.questionID == questionID
                       select aa).FirstOrDefault();
            if (res != null)
            {
                db.PollingQandAs.Remove(res);
                success = db.SaveChanges();
            }
            return success;
        }
        public void SavePollingQA(PollingQandA pobj)
        {
            try
            {
                db.PollingQandAs.Add(pobj);
                success = db.SaveChanges();
            }
            catch (Exception ex)
            { }
        }
        public void UpdatePollingQA(PollingQandA Pobj)
        {
            var res = db.PollingQandAs.FirstOrDefault(i => i.questionID == Pobj.questionID);
            if (res != null)
            {
                res.question = Pobj.question;
                res.noOfAns = Pobj.noOfAns;
                res.sessionID = Pobj.sessionID;
                res.pollingStatus = Pobj.pollingStatus;
                res.answer1 = Pobj.answer1;
                res.answer2 = Pobj.answer2;
                res.answer3 = Pobj.answer3;
                res.answer4 = Pobj.answer4;
                res.answer5 = Pobj.answer5;
                res.answer6 = Pobj.answer6;
                res.answer7 = Pobj.answer7;
                res.answer8 = Pobj.answer8;
                res.answer9 = Pobj.answer9;
                db.SaveChanges();
            }
        }
        public void UpdatePollingResultStatus(int uPType, string questionID)
        {
            var res = (from aa in db.PollingQandAs
                       where aa.questionID == questionID
                       select aa).FirstOrDefault();
            if (res != null)
            {
                res.pollingStatus = uPType;
                db.SaveChanges();
            }
        }
        #endregion

        #region Live Q&A
        public List<FrontEnd> selectQASessions()
        {
            var lst = (from bb in db.FrontEnds where bb.f_label == "Use for Live Q & A" && bb.f_content == "Yes" select bb.f_c_rowid).ToList();
            var res = (from aa in db.FrontEnds
                       where lst.Contains(aa.f_c_rowid) && aa.f_label == "Conferernce Title"
                       select aa).ToList();
            return res;
        }
      
        
        public void SaveLQAVote(LQAVote obj)
        {
            db.LQAVotes.Add(obj);
            db.SaveChanges();
        }
       
        
        public void UpdateLQA(LQA objl)
        {
            var res = (from aa in db.LQAs where aa.LQAID == objl.LQAID select aa).FirstOrDefault();
            if(res != null)
            {
                res.LQANum = res.LQANum + 1;
                db.SaveChanges();
            }
        }
       
        
        
        #endregion

        #region Survey
        public int updateSurveyCate(SurveyCate obj)
        {
            int rtn = 0;
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateId == obj.SurveyCateId
                       select aa).FirstOrDefault();
            if (res != null)
            {
                res.SurveyCateenum = obj.SurveyCateenum;
                res.SurveyCateTitle = obj.SurveyCateTitle;
                res.SurveyCateType = obj.SurveyCateType;
                res.UserType = obj.UserType;
                res.SurveyCateId = obj.SurveyCateId;
                rtn = db.SaveChanges();
            }
            return rtn;
        }
        public int saveSurveyCate(SurveyCate obj)
        {
            db.SurveyCates.Add(obj);
            int res = db.SaveChanges();
            return res;
        }
        public void SaveSurveyList(SurveyList obj)
        {
            db.SurveyLists.Add(obj);
            db.SaveChanges();
        }
        public string SelectSurveyQNo()
        {
            var str = (from aa in db.SurveyLists
                       select aa.SurveyKeyNo).ToList();

            var res = (from aa in db.QnoLists
                       where !str.Contains(aa.Qno)
                       select aa).OrderBy(i => i.QId).ToList();
            string rtn = res[0].Qno;
            return rtn;
        }
        public SurveyQA SelectSurveyQAByID(string uid)
        {
            var res = (from aa in db.SurveyQAs
                       where aa.UserReference == uid
                       select aa).FirstOrDefault();
            return res;
        }
        public List<SurveyCate> selectSurveyCate()
        {
            var res = (from aa in db.SurveyCates
                       select aa).ToList();
            return res;
        }
        public List<SurveyList> selectSurveyListByCateID(int surveyCateId)
        {
            var res = (from aa in db.SurveyLists
                       where aa.SurveyCateId == surveyCateId
                       select aa).ToList();
            return res;
        }
        public SurveyCate selectSurveyCateByID(int id)
        {
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateId == id
                       select aa).FirstOrDefault();
            return res;
        }
        public List<SurveyCate> selectSurveyCateByNumUserType(int? surveyCateNum, string userType)
        {
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateenum > surveyCateNum && aa.UserType == userType
                       select aa).OrderBy(i=>i.SurveyCateenum).ToList(); //order by VQACatenum asc
            return res;
        }
        public List<SurveyCate> selectSurveyCateByEqualNumUserType(int? surveyCateNum, string userType)
        {
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateenum == surveyCateNum && aa.UserType == userType
                       select aa).ToList().OrderBy(i=>i.SurveyCateenum).ToList(); //order by VQACatenum asc
            return res;
        }
        public SurveyList selectSurveyListByID(int id)
        {
            var res = (from aa in db.SurveyLists
                       where aa.SurveyId == id
                       select aa).FirstOrDefault();
            return res;
        }
        public int updateSurveyCateNum(SurveyCate obj,bool plusone,bool minusone)
        {
            int rtn = 0;
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateId == obj.SurveyCateId
                       select aa).FirstOrDefault();
            if (res != null)
            {
                if (plusone == true)
                {
                    res.SurveyCateenum = res.SurveyCateenum + 1;
                }
                else if(minusone == true)
                {
                    res.SurveyCateenum = res.SurveyCateenum - 1;
                }
                else
                {
                    res.SurveyCateenum = obj.SurveyCateenum;
                }
                
                res.SurveyCateId = obj.SurveyCateId;
                rtn = db.SaveChanges();
            }
            return rtn;
        }
        public string selectSurveyCateTitleByID(int? surveyCateId)
        {
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateId == surveyCateId
                       select aa.SurveyCateTitle).FirstOrDefault();
            return res;
        }
        public List<SurveyList> selectSurveyListByGreaterNumUserType(int surveyNum, int surveyCateId)
        {
            var res = (from aa in db.SurveyLists
                       where aa.SurveyNum > surveyNum && aa.SurveyCateId == surveyCateId
                       select aa).ToList().OrderBy(i => i.SurveyNum).ToList();//order by SurveyNum asc";
            return res;
        }

        public void UpdateSurveyList(SurveyList obj)
        {
            var res = (from aa in db.SurveyLists
                       where aa.SurveyId == obj.SurveyId
                       select aa).FirstOrDefault();
            if(res != null)
            {
                res.SurveyNum = obj.SurveyNum;
                res.SurveyId = obj.SurveyId;
                db.SaveChanges();                    
            }
        }
        public int CheckSurveyCateByTitle(string title)
        {
            int rtn = 0;
            var res = (from aa in db.SurveyCates
                       where aa.SurveyCateTitle == title
                       select aa).FirstOrDefault();
            if(res!=null)
            {
                rtn = 1;
            }
            return rtn;
        }
        public List<SurveyCate> selectSurveyCateByUserType(string userType)
        {
            var res = (from aa in db.SurveyCates
                       where aa.UserType == userType
                       select aa).OrderBy(i => i.SurveyCateenum).ToList();
            return res;
        }
        #endregion        
    }
}


