﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using App.Common;
using App.Object;

namespace App.DAL
{
    public class DAL_Parent
    {
        #region Delaration
        int success = 0;
        int count = 0;
        public int TotalCount { get; set; }
        public BackEnd_Parent cf;
        IsabellaFormEntities db = new IsabellaFormEntities();
        #endregion

        #region SelectAllData
        public List<BackEnd_Parent> SelectAllData()
        {
            List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
            TotalCount = 0;
            try
            {
                lst = db.BackEnd_Parent.Select(e=>e).Where(i => i.p_isFinish == true).OrderBy(j=>j.p_name).ToList();         
                return lst;
            }
            catch (Exception e)
            {

            }
            return lst;
        }
        #endregion

        #region Delete
        public int Delete(string ID)
        {
            success = 0;
            try
            {
                BackEnd_Parent Ab = (from ls in db.BackEnd_Parent where ls.p_id == ID select ls).First<BackEnd_Parent>();
                if (Ab != null)
                {
                    db.BackEnd_Parent.Remove(Ab);
                    success = db.SaveChanges();

                }
            }
            catch { }
            return success;
        }
        #endregion

        #region Save
        public int Save(BackEnd_Parent obj)
        {
            success = 0;
            try
            {
                db.BackEnd_Parent.Add(obj);
                success = db.SaveChanges();
            }
            catch (Exception)
            { }
            return success;
        }
        #endregion

        #region CheckExist
        public int CheckExist(BackEnd_Parent Pobj)
        {
            int count = 0;
            var query = (from dept in db.BackEnd_Parent
                         where dept.p_id != Pobj.p_id
                         && dept.p_name == Pobj.p_name
                         && dept.p_type == Pobj.p_type
                         && dept.p_isFinish==true
                         select dept).ToList();
            count = query.Count;
            
            return count;
        }
        #endregion

        #region Update
        public void update(BackEnd_Parent Pobj)
        {
            var res = db.BackEnd_Parent.FirstOrDefault(i => i.p_id == Pobj.p_id);
            if (res != null)
            {
                res.p_isFinish = true;
                db.SaveChanges();
            }
        }
        public void updateInfo(BackEnd_Parent Pobj)
        {
            var res = db.BackEnd_Parent.FirstOrDefault(i => i.p_id == Pobj.p_id);
            if (res != null)
            {
                res.p_name = Pobj.p_name;
                res.p_title = Pobj.p_title;
                res.p_type = Pobj.p_type;
                res.p_isFinish = true;
                db.SaveChanges();
            }
        }
        #endregion

        #region SelectByName
        public BackEnd_Parent SelectByName(string pname)
        {
            return db.BackEnd_Parent.FirstOrDefault(i => i.p_name == pname);
        }
        #endregion

        #region SelectById
        public BackEnd_Parent SelectById(string pid)
        {
            ParentModel oPModel = new ParentModel();

            try
            {
                oPModel = (from dept in db.BackEnd_Parent
                           where dept.p_id == pid
                           select new ParentModel
                           {
                               p_id = dept.p_id,
                               p_name = dept.p_name,
                               p_title = dept.p_title,
                               p_type = dept.p_type,
                               p_isFinish = dept.p_isFinish,
                               p_includePush = dept.p_includePush
                              }).First();

            }
            catch { }
            return oPModel;
        }
        #endregion

        #region SelectById
        public string SelectNameByID(string pid)
        {
            return db.BackEnd_Parent.FirstOrDefault(i => i.p_id == pid).p_title;
        }
        #endregion

        #region SelectByType
        public List<BackEnd_Parent> SelectByType(string type)
        {
            List<BackEnd_Parent> lst = new List<BackEnd_Parent>();
            TotalCount = 0;
            try
            {
                lst = db.BackEnd_Parent.Select(e => e).Where(i => i.p_isFinish == true && i.p_type==type).OrderBy(j => j.p_sortorder).ToList();                
                return lst;
            }
            catch (Exception e)
            {

            }
            return lst;
        }
        #endregion
    }
}