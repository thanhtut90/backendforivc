﻿using App.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace App.DAL
{
    public class DAL_DataIScan
    {
        FunctionalityISan fnIScan = new FunctionalityISan();
        public DAL_DataIScan()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public ExhibitorProductObj getExhibitorProductByExhDeviceCode(string exhCode)
        {
            ExhibitorProductObj exhObj = new ExhibitorProductObj();
            try
            {
                DataTable dt = new DataTable();
                string sql = string.Format("Select * From tb_Exhibitor as exh Inner Join tb_Company as com On exh.Comp_Id=com.Comp_ID "
                                + " Left Join tb_ProductTable as pro On exh.Exh_ID=pro.ExhID Where exh.Exh_Code='{0}'", exhCode);
                dt = fnIScan.GetDatasetByCommand(sql, "sdt").Tables[0];
                if(dt.Rows.Count > 0)
                {
                    exhObj.ExhID = dt.Rows[0]["Exh_ID"] != DBNull.Value ? dt.Rows[0]["Exh_ID"].ToString() : "";
                    exhObj.MexhID = dt.Rows[0]["MexhID"] != DBNull.Value ? dt.Rows[0]["MexhID"].ToString() : "";
                    exhObj.ExhName = dt.Rows[0]["Exh_Name"] != DBNull.Value ? dt.Rows[0]["Exh_Name"].ToString() : "";
                    exhObj.ExhCountry = dt.Rows[0]["ExhCountry"] != DBNull.Value ? dt.Rows[0]["ExhCountry"].ToString() : "";
                    exhObj.ExhContactPerson = dt.Rows[0]["Exh_ContPerson"] != DBNull.Value ? dt.Rows[0]["Exh_ContPerson"].ToString() : "";
                    exhObj.ExhEmail = dt.Rows[0]["Exh_Email"] != DBNull.Value ? dt.Rows[0]["Exh_Email"].ToString() : "";
                    exhObj.ExhBoothNo = dt.Rows[0]["Comp_BoothNo"] != DBNull.Value ? dt.Rows[0]["Comp_BoothNo"].ToString() : "";
                    exhObj.ExhProductID = dt.Rows[0]["Pro_id"] != DBNull.Value ? dt.Rows[0]["Pro_id"].ToString() : "";
                    exhObj.ExhProductName = dt.Rows[0]["Pro_name"] != DBNull.Value ? dt.Rows[0]["Pro_name"].ToString() : "";
                    exhObj.ExhProductDescription = dt.Rows[0]["Pro_description"] != DBNull.Value ? dt.Rows[0]["Pro_description"].ToString() : "";
                }
            }
            catch(Exception ex) { }
            return exhObj;
        }

        #region ConvertToTimestamp
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public string ConvertToTimestamp(DateTime value)
        {
            TimeSpan elapsedTime = value - Epoch;
            return elapsedTime.TotalSeconds.ToString();
        }
        #endregion
        public void UpdateDataVersion(string tablename)
        {
            StringBuilder str = new StringBuilder();
            try
            {
                string result = string.Empty;
                var sql = "select a_dataVersion from tblActivatedForms where a_activatedFormName='"+tablename+"' and deleteFlag=0";
                result = fnIScan.GetDataByCommand(sql, "a_dataVersion").ToString();
                if (!string.IsNullOrEmpty(result))
                {
                    int count = int.Parse(result);
                    count += 1;                  
                    str.Append("update tblActivatedForms set a_dataVersion=" + count + " where a_activatedFormName='" + tablename + "' and deleteFlag=0");
                }else
                {
                    str.Append("update tblActivatedForms set a_dataVersion=" + 1 + " where a_activatedFormName='" + tablename + "' and deleteFlag=0");
                }
                    fnIScan.ExecuteSQL(str.ToString());
            }
            catch (Exception ex) { }
        }
    }

    public class ExhibitorProductObj
    {
        public string ExhID { get; set; }
        public string MexhID { get; set; }
        public string ExhName { get; set; }
        public string ExhCountry { get; set; }
        public string ExhContactPerson { get; set; }
        public string ExhEmail { get; set; }
        public string ExhBoothNo { get; set; }
        public string ExhProductID { get; set; }
        public string ExhProductName { get; set; }
        public string ExhProductDescription { get; set; }
    }
    public class VisitorObj
    {
        public string VisitorRegno { get; set; }
        public string Name { get; set; }
        public string JobTitle { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Company { get; set; }
    }
}
