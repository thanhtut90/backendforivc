﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Common;
using App.Object;
using System.Data;

namespace App.DAL
{
    public class DAL_Email
    {
        #region Delaration
        int success = 0;
        int count = 0;
        public int TotalCount { get; set; }

        public FrontEnd fe;
        IsabellaFormEntities db = new IsabellaFormEntities();

        List<BackEnd_Child> ShowHeaderList = new List<BackEnd_Child>();
        DAL_Child dalc = new DAL_Child();
        List<FrontEnd> lstfe = new List<FrontEnd>();
        List<FrontEndModel> lstfmodel = new List<FrontEndModel>();
        Functionality fn = new Functionality();
        #endregion

        public int UpdateConfigEmail(tb_ConfigEmail obj)
        {
            int rtn = 0;
            var res = (from aa in db.tb_ConfigEmail
                       where aa.em_id == obj.em_id
                       select aa).FirstOrDefault();
            if (res != null)
            {
                res.em_id = res.em_id;
                res.em_Title = obj.em_Title;
                res.em_Subject = obj.em_Subject;
                res.em_FromEmail = obj.em_FromEmail;
                res.em_FromName = obj.em_FromName;
                res.em_Content = obj.em_Content;
                res.em_cc = obj.em_cc;
                res.em_bcc = obj.em_bcc;
                res.em_parameters = obj.em_parameters;
                rtn = db.SaveChanges();
            }
            return rtn;
        }

        public tb_ConfigEmail selectEmailTemplateByID(int em_id)
        {
            tb_ConfigEmail obj = (from aa in db.tb_ConfigEmail
                                  where aa.em_id == em_id
                                  select aa).FirstOrDefault();
            return obj;
        }

        public List<tb_ConfigEmail> SelectAllEmailTemplate()
        {
            List<tb_ConfigEmail> lst = (from aa in db.tb_ConfigEmail
                                        select aa).ToList();
            return lst;
        }

        public int SaveConfigEmail(tb_ConfigEmail obj)
        {
            db.tb_ConfigEmail.Add(obj);
            int rtn = db.SaveChanges();
            return rtn;
        }

        public int SendEmail(string TemplateName,string recevingEmail, string attachment,string sqlcmd)
        {
            //parameter format = key:value^seperator
            int isSuc = 0;
            string Subject = string.Empty;
            string FromName = string.Empty;
            string FromEmail = string.Empty;
            string CC = string.Empty;
            string BCC = string.Empty;
            string Para = string.Empty;
            string Content = string.Empty;
            DataTable dt = fn.GetDatasetByCommand(sqlcmd, "sdt").Tables[0];
            if(dt.Rows.Count > 0)
            {
                foreach(DataRow dr in dt.Rows)
                {

                }
            }

            return isSuc;
        }
    }
}
