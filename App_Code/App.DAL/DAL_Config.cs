﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Common;
using App.Object;
using System.Data;

namespace App.DAL
{
    public class DAL_Config
    {
        IsabellaFormEntities db = new IsabellaFormEntities();

        public Administrator SelectAdminByNP(Administrator obj)
        {
            var res = (from aa in db.Administrators
                       where aa.admin_username == obj.admin_username && aa.admin_password == obj.admin_password && aa.admin_isdeleted==false
                       select aa).FirstOrDefault();
            return res;
        }
    }
}

