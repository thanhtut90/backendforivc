﻿using System;
using System.Collections.Generic;
using System.Linq;
using App.Object;
using App.Common;

namespace App.DAL
{
    public class DAL_Child
    {
        #region Delaration
        int success = 0;
        int count = 0;
        public int TotalCount { get; set; }        
        public BackEnd_Child cf;
        IsabellaFormEntities db = new IsabellaFormEntities();

        public string selectPIDByCID(string cid)
        {
            string rtncid = string.Empty;
            var rtn = (from res in db.BackEnd_Child where res.c_id == cid select res).FirstOrDefault().c_pid;
            if( rtn != null && rtn !="")
            {
                rtncid = rtn;
            }
            return rtncid;
        }

        #endregion
        #region SelectByName
        public BackEnd_Child SelectByName(string clabel,string pid)
        {
            return db.BackEnd_Child.FirstOrDefault(i => i.c_label == clabel && i.c_pid==pid);
        }

        public int selectSortOrderByPID(string pid)
        {
            var res = (from aa in db.BackEnd_Child
                       where aa.c_pid == pid
                       orderby aa.c_sortorder descending
                       select aa.c_sortorder).First();
            if(res==null)
            {
                res = 1;
            }
            else
            {
                res = res + 1;
            }
            return res.Value;
        }
        #endregion

        #region SelectAllData
        //public List<BackEnd_Child> SelectAllData(int startIndex, int PageSize, out int TotalCount)
        //{
        //    List<BackEnd_Child> lst = new List<BackEnd_Child>();
        //    TotalCount = 0;
        //    try
        //    {
        //        lst = (from Ab in db.BackEnd_Child
        //               orderby Ab.c_id
        //               where Ab.c_deleteflag == false
        //               select new BackEnd_Child
        //               {
        //                   c_id = Ab.c_id,
        //                   c_pid=Ab.c_pid,
        //                   c_label=Ab.c_label,
        //                   c_content=Ab.c_content,
        //                   c_inputtype=Ab.c_inputtype,
        //                   c_sortorder=Ab.c_sortorder ,                          
        //                   c_isRequired=Ab.c_isRequired,
        //                   c_deleteflag=Ab.c_deleteflag
        //               }).ToList();
        //        TotalCount = lst.Count();
        //        lst = lst.Skip(PageSize * (startIndex - 1)).Take(PageSize).ToList();
        //        return lst.ToList();
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    return lst;
        //}
        public List<BackEnd_Child> SelectAllData()
        {  
            var lst = (from res in db.BackEnd_Child
                orderby res.c_pid,res.c_sortorder 
                where res.c_deleteflag == false
                select res).ToList();
            return lst;
        }
        #endregion

        #region Delete
        public int Delete(string ID)
        {
            success = 0;
            try
            {
                BackEnd_Child Ab = (from ls in db.BackEnd_Child where ls.c_id == ID select ls).First<BackEnd_Child>();
                if (Ab != null)
                {
                    db.BackEnd_Child.Remove(Ab);
                    success = db.SaveChanges();

                }
            }
            catch { }
            return success;
        }
        #endregion

        #region Save
        public int Save(BackEnd_Child obj)
        {
            success = 0;
            try
            {
                db.BackEnd_Child.Add(obj);
                success = db.SaveChanges();
            }
            catch (Exception)
            { }
            return success;
        }
        #endregion

        #region CheckExist
        public int CheckExist(BackEnd_Child Pobj)
        {
            int count = 0;
            var query = (from dept in db.BackEnd_Child
                         where dept.c_pid == Pobj.c_pid && dept.c_label == Pobj.c_label
                         select dept).ToList();
            count = query.Count;

            return count;
        }
        #endregion

        #region Update
        public void update(BackEnd_Child Pobj)
        {
            var res = db.BackEnd_Child.FirstOrDefault(i => i.c_id == Pobj.c_id);
            if (res != null)
            {
                res.c_pid = Pobj.c_pid;
                res.c_inputtype = Pobj.c_inputtype;
                res.c_label = Pobj.c_label;
                res.c_content = Pobj.c_content;                
                res.c_sortorder = Pobj.c_sortorder;
                res.c_isRequired = Pobj.c_isRequired;
                res.c_deleteflag = Pobj.c_deleteflag;
                db.SaveChanges();
            }
        }
        #endregion

        #region SelectById
        public List<BackEnd_Child> SelectByPId(string id)
        {
            return db.BackEnd_Child.Where(i => i.c_pid == id && i.c_deleteflag==false).ToList();
        }
        public List<ChildModel> SelectByPIdforEdit(string id)
        {

            List<ChildModel> lst = new List<ChildModel>();
            try
            {
                var query = from p in db.BackEnd_Child
                            where p.c_pid == id
                            select new { ID = p.c_id, InputType = p.c_inputtype, lblName = p.c_label, Content = p.c_content };
                lst = query.ToList().Select(r => new ChildModel
                {
                    c_id = r.ID,
                    c_inputtype = r.InputType,
                    c_label = r.lblName,
                    c_content = r.Content
                }).ToList();
            }
            catch (Exception e)
            {

            }
            return lst;
        }
        #endregion

        #region SelectById
        public BackEnd_Child SelectById(string rid)
        {
            return db.BackEnd_Child.FirstOrDefault(i => i.c_id == rid);
        }
        #endregion

        #region SelectForDateCheck
        public List<BackEnd_Child> SelectForDateCheck(List<BackEnd_Child> lstchild)
        {
            return (from aa in lstchild
                    where aa.c_inputtype == 5 && aa.c_isRequired == true
                    select aa).ToList();
        }
        #endregion

        #region SelectForCheckBoxCheck
        public List<BackEnd_Child> SelectForCheckBoxCheck(List<BackEnd_Child> lstchild)
        {
            return (from aa in lstchild
                    where aa.c_inputtype == 4 && aa.c_isRequired == true
                    select aa).ToList();
        }

        public List<BackEnd_Child> SelectForCKEditorCheck(List<BackEnd_Child> lstchild)
        {
            return (from aa in lstchild
                    where aa.c_inputtype == 10 && aa.c_isRequired == true
                    select aa).ToList();
        }

        public string selectCIDByPIDLabel(string pid, string c_label)
        {
            var res = (from aa in db.BackEnd_Child
                       where aa.c_pid == pid && aa.c_label == c_label
                       select aa.c_id).FirstOrDefault();
            return res;
        }
        #endregion
    }
}