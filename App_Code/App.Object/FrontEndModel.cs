﻿using App.Common;
namespace App.Object
{
    public class FrontEndModel:FrontEnd
    {
        public FrontEndModel()
        {
            Column = "";
            Data = "";
        }
        public string Column { get; set; }
        public string Data { get; set; }
    }
}