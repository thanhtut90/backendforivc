﻿using App.Common;

namespace App.Object
{
    public class ChildModel:BackEnd_Child
    {
        public ChildModel()
        {
            no = "";
            pid = 0;
            pname = "";
            inputtype = "";
        }
        public string no { get; set; }
        public int pid { get; set; }
        public string pname { get; set; }
        public string inputtype { get; set; }
    }
}