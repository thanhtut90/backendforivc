﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClinicAppointmentMgmt.Startup))]
namespace ClinicAppointmentMgmt
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
