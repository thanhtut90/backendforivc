﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using App.Common;
using App.DAL;
using System.Globalization;
/// <summary>
/// Summary description for DAL
/// </summary>
public class DAL
{
    Functionality fn = new Functionality();
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    IsabellaFormEntities db = new IsabellaFormEntities();
    GenKey gk = new GenKey();
    CultureInfo culture2 = CultureInfo.CreateSpecificCulture("en-GB");
    public DataTable getLang()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from tb_Language";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }

    public int insertSysConfigData(string name, string language, string startdate, string enddate, string enableAccount, string appIcon, string venue, string SDSurvey, string EDSurvey, string visRegUrl, string layout, string iOSVersion, string iOSDownloadLink, string AndroidVersion, string AndroidDownloadLink,string dappkey,string dmastersecret, string pappkey, string pmastersecret)
    {
        int res = 0;
        try
        {
            DateTime sdt = DateTime.Parse(startdate, culture2);
            DateTime edt = DateTime.Parse(enddate, culture2);
            string id = fn.GetDataByCommand("select config_id from tb_Configuration", "config_id");
            if (!string.IsNullOrEmpty(id) && id != "0")
            {
                #region Update
                StringBuilder str = new StringBuilder();
                str.Append("Update tb_Configuration set config_language=@config_language,config_name=@config_name,config_startDate=@config_startDate,config_endDate=@config_endDate,config_enableAccount=@config_enableAccount,config_appIcon=@config_appIcon,config_venue=@config_venue,config_surveyStartDate=@config_surveyStartDate,config_surveyEndDate=@config_surveyEndDate,config_visRegURL=@config_visRegURL,config_homePageLayout=@config_homePageLayout,Status=@Status,config_iOSVersion=@config_iOSVersion,config_iOsDownloadLink=@config_iOsDownloadLink,config_AndroidVersion=@config_AndroidVersion,config_AndroidDownloadLink=@config_AndroidDownloadLink,config_dev_appkey=@config_dev_appkey,config_dev_mastersecretkey=@config_dev_mastersecretkey,config_product_appkey=@config_product_appkey where config_id=@config_id");

                SqlParameter[] parms =
                {
                     new SqlParameter("@config_language",SqlDbType.NVarChar),
                     new SqlParameter("@config_name",SqlDbType.NVarChar),
                     new SqlParameter("@config_startDate",SqlDbType.DateTime),
                     new SqlParameter("@config_endDate",SqlDbType.DateTime),
                     new SqlParameter("@config_enableAccount",SqlDbType.NVarChar),
                     new SqlParameter("@config_appIcon",SqlDbType.NVarChar),
                     new SqlParameter("@config_venue",SqlDbType.NVarChar),
                     new SqlParameter("@config_surveyStartDate",SqlDbType.NVarChar),
                     new SqlParameter("@config_surveyEndDate",SqlDbType.NVarChar),
                     new SqlParameter("@config_visRegURL",SqlDbType.NVarChar),
                     new SqlParameter("@config_homePageLayout",SqlDbType.NVarChar),
                     new SqlParameter("@Status",SqlDbType.NVarChar),
                     new SqlParameter("@updatedDate",SqlDbType.VarChar),
                     new SqlParameter("@config_id",SqlDbType.Int),
                     new SqlParameter("@config_iOSVersion",SqlDbType.NVarChar),
                     new SqlParameter("@config_iOsDownloadLink",SqlDbType.NVarChar),
                     new SqlParameter("@config_AndroidVersion",SqlDbType.NVarChar),
                     new SqlParameter("@config_AndroidDownloadLink",SqlDbType.NVarChar),
                     new SqlParameter("@config_dev_appkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_dev_mastersecretkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_product_appkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_product_mastersecretkey",SqlDbType.NVarChar)
                 };
                parms[0].Value = language;
                parms[1].Value = name;
                parms[2].Value = sdt;
                parms[3].Value = edt;
                parms[4].Value = enableAccount;
                parms[5].Value = appIcon;
                parms[6].Value = venue;
                parms[7].Value = SDSurvey;
                parms[8].Value = EDSurvey;
                parms[9].Value = visRegUrl;
                parms[10].Value = layout;
                parms[11].Value = "Update";
                parms[12].Value = dalf.ConvertToTimestamp(DateTime.Now);
                parms[13].Value = id;
                parms[14].Value = iOSVersion;
                parms[15].Value = iOSDownloadLink;
                parms[16].Value = AndroidVersion;
                parms[17].Value = AndroidDownloadLink;
                parms[18].Value = dappkey;
                parms[19].Value = dmastersecret;
                parms[20].Value = pappkey;
                parms[21].Value = pmastersecret;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                #endregion
            }
            else
            {
                #region Add New                
                StringBuilder str = new StringBuilder();

                str.Append("INSERT INTO tb_Configuration");
                str.Append("(config_language, config_name, config_startDate, config_endDate, config_enableAccount, config_appIcon, config_venue,config_surveyStartDate,config_surveyEndDate, config_visRegURL, config_homePageLayout, Status, updatedDate,config_iOSVersion,config_iOsDownloadLink,config_AndroidVersion,config_AndroidDownloadLink,config_dev_appkey,config_dev_mastersecretkey,config_product_appkey,config_product_mastersecretkey)");
                str.Append("VALUES");
                str.Append("(@config_language, @config_name, @config_startDate, @config_endDate, @config_enableAccount, @config_appIcon, @config_venue,@config_surveyStartDate,@config_surveyEndDate, @config_visRegURL, @config_homePageLayout, @Status, @updatedDate,@config_iOSVersion,@config_iOsDownloadLink,@config_AndroidVersion,@config_AndroidDownloadLink,@config_dev_appkey,@config_dev_mastersecretkey,@config_product_appkey,@config_product_mastersecretkey)");
                SqlParameter[] parms =
                {
                     new SqlParameter("@config_language",SqlDbType.NVarChar),
                     new SqlParameter("@config_name",SqlDbType.NVarChar),
                     new SqlParameter("@config_startDate",SqlDbType.DateTime),
                     new SqlParameter("@config_endDate",SqlDbType.DateTime),
                     new SqlParameter("@config_enableAccount",SqlDbType.NVarChar),
                     new SqlParameter("@config_appIcon",SqlDbType.NVarChar),
                     new SqlParameter("@config_venue",SqlDbType.NVarChar),
                     new SqlParameter("@config_surveyStartDate",SqlDbType.NVarChar),
                     new SqlParameter("@config_surveyEndDate",SqlDbType.NVarChar),
                     new SqlParameter("@config_visRegURL",SqlDbType.NVarChar),
                     new SqlParameter("@config_homePageLayout",SqlDbType.NVarChar),
                     new SqlParameter("@Status",SqlDbType.NVarChar),
                     new SqlParameter("@updatedDate",SqlDbType.VarChar),
                     new SqlParameter("@config_iOSVersion",SqlDbType.NVarChar),
                     new SqlParameter("@config_iOsDownloadLink",SqlDbType.NVarChar),
                     new SqlParameter("@config_AndroidVersion",SqlDbType.NVarChar),
                     new SqlParameter("@config_AndroidDownloadLink",SqlDbType.NVarChar),
                     new SqlParameter("@config_dev_appkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_dev_mastersecretkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_product_appkey",SqlDbType.NVarChar),
                     new SqlParameter("@config_product_mastersecretkey",SqlDbType.NVarChar)
                 };
                parms[0].Value = language;
                parms[1].Value = name;
                parms[2].Value = sdt;
                parms[3].Value = edt;
                parms[4].Value = enableAccount;
                parms[5].Value = appIcon;
                parms[6].Value = venue;
                parms[7].Value = SDSurvey == "" ? null : SDSurvey;
                parms[8].Value = EDSurvey == "" ? null : EDSurvey;
                parms[9].Value = visRegUrl;
                parms[10].Value = layout;
                parms[11].Value = "Add";
                parms[12].Value = dalf.ConvertToTimestamp(DateTime.Now);
                parms[13].Value = iOSVersion;
                parms[14].Value = iOSDownloadLink;
                parms[15].Value = AndroidVersion;
                parms[16].Value = AndroidDownloadLink;
                parms[17].Value = dappkey;
                parms[18].Value = dmastersecret;
                parms[19].Value = pappkey;
                parms[20].Value = pmastersecret;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                #endregion
            }
        }
        catch (Exception ex) { throw ex; }
        return res;
    }

    public DataTable selectAllInbox()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from tblInbox where deleteFlag=0 and i_isPush=0";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }

    public string CheckRecordExistByLine(string pid, string cid, string rowid, string lang)
    {
        string fid = string.Empty;
        try
        {
            string sql= "select f_id from FrontEnd where f_pid='"+ pid + "' and f_c_id='"+ cid + "' and f_c_rowid='"+ rowid + "' and f_lang=" + lang;
            fid = fn.GetDataByCommand(sql, "f_id").ToString();
        }
        catch (Exception ex) { throw ex; }
        return fid;
    }

    public int CheckRecordExist(string pid, string clientID,ref string fcrowid)
    {
        int rtnres = 0;
        try
        {
            string sql = "select f_c_rowid from FrontEnd where f_pid='"+ pid + "' and f_inputtype=15 and f_deleteflag=0 and f_content='"+ clientID + "'";
            string result = fn.GetDataByCommand(sql, "f_c_rowid").ToString();
            if(result == "0")
            {
                rtnres = 0;
            }
            else if(string.IsNullOrEmpty(result))
            {
                rtnres = 0;
            }
            else
            {
                rtnres = 1;
                fcrowid = result;
            }
        }
        catch(Exception ex) { throw ex; }
        return rtnres;
    }

    public int getLangIDBycode(string langcode)
    {
        var res = (from aa in db.tb_Language
                   where aa.LangCode == langcode
                   select aa.ID).FirstOrDefault();
        return res;
    }
    public DataTable getPageTypeForEdit(string iD)
    {
        DataTable table = new DataTable();
        try
        {
            DataTable dtMenu = fn.GetDatasetByCommand("Select * From tb_Menu Where m_id='" + iD + "' and Status <> 'delete'", "ds").Tables[0];

            string sql = "SELECT * from PageType where PT_HasUsed=0 and Status <> 'delete' order by ChangedID asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];

            if (dtMenu.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(dtMenu.Rows[0]["m_PageType"].ToString()))
                {
                    DataTable pt = fn.GetDatasetByCommand("SELECT * from PageType where PT_Name='" + dtMenu.Rows[0]["m_PageType"].ToString() + "'", "dsP").Tables[0];
                    if (pt.Rows.Count > 0)
                    {
                        table.Merge(pt);
                    }
                }
            }
        }
        catch (Exception ex) { throw ex; }

        return table;
    }

    //public List<PageType> getPageTypeForEdit(string iD)
    //{
    //    PageType pt = new PageType();
    //    var a = (from aa in db.tb_Menu
    //             where aa.m_id == iD
    //             select aa).FirstOrDefault();


    //    var res = (from aa in db.PageTypes
    //               where aa.PT_HasUsed == 0
    //               orderby aa.changedID ascending
    //               select aa).ToList();

    //    if (!string.IsNullOrEmpty(a.m_PageType))
    //    {
    //        pt = (from bb in db.PageTypes
    //              where bb.PT_Name == a.m_PageType.ToString()

    //              select bb).FirstOrDefault();
    //        if (pt != null)
    //        {
    //            res.Add(pt);
    //        }

    //    }


    //    return res;

    //}

    public string selectPageTypeIDByName(string name)
    {
        string result = string.Empty;
        try
        {
            result = fn.GetDataByCommand("select ID from PageType where PT_Name='" + name + "' and Status <> 'Deleted'", "ID");
        }
        catch (Exception ex) { }
        return result;
    }
    public string selectMenuIDByTitle(string title)
    {
        var res = (from aa in db.PageTypes
                   where aa.PT_Name == title
                   select aa.ID).FirstOrDefault();
        return res.ToString();
    }

    public DataTable selectMenuByID(string id)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = fn.GetDatasetByCommand("Select * From tb_Menu Where m_id='" + id + "' And Status<>'deleted'", "ds").Tables[0];
        }
        catch (Exception ex)
        { }
        return dt;
    }
    //public tb_Menu selectMenuByID(string id)
    //{
    //    var res = (from aa in db.tb_Menu
    //               where aa.m_id == id
    //               select aa).FirstOrDefault();
    //    return res;
    //}

    public string selectPageTypeByMenuID(string id)
    {
        string result=string.Empty;
        try
        {
            result = fn.GetDataByCommand("Select m_PageType From tb_Menu Where m_id='" + id + "' And Status<>'deleted'", "m_PageType");
            if(result == "0")
            {
                result = "";
            }
        }
        catch (Exception ex)
        { }
        return result;
    }

    public DataTable getWebsiteMenuByID(string id)
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from WebsiteMenu where m_id="+id+" order by m_sortorder asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }

    public string getWebParentMenuByID(string parentMenuID)
    {
        string name = string.Empty;
        try
        {
            name = fn.GetDataByCommand("select m_title from WebsiteMenu where m_id=" + parentMenuID, "m_title");
        }
        catch (Exception ex) { throw ex; }
        return name;
    }

    public DataTable getWebsiteMenu()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from WebsiteMenu where m_deleteflag=0 order by m_sortorder asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }

    public string getMenuTitleByID(string menuID)
    {
        string sql = "select m_TitleEn from tb_Menu where m_id='"+menuID+ "' And Status<>'deleted'";
        string dt = fn.GetDataByCommand(sql, "m_TitleEn").ToString();
        return dt;
    }

    public DataTable selectMenu()
    {
        string sql = "select * from tb_Menu where status <> 'deleted' and m_parentMenuID='' order by m_OrderID asc";
        DataTable dt = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        return dt;
    }

    public int insertWebsiteMenu(string id, string menutitle, string redirectURL,string ordernum, string parentID, string micon)
    {
        int res = 0;
        try
        {
            if (!string.IsNullOrEmpty(id) && id != "0")
            {
                #region Update
                StringBuilder str = new StringBuilder();
                str.Append("Update WebsiteMenu set m_title=@m_title,m_pagetoredirect=@m_pagetoredirect,m_parentmenuID=@m_parentmenuID,m_sortorder=@m_sortorder,m_IconUrl=@m_IconUrl,m_deleteflag=@m_deleteflag,m_createdate=@m_createdate where m_id=@m_id");

                SqlParameter[] parms =
                {
                     new SqlParameter("@m_title",SqlDbType.NVarChar),
                     new SqlParameter("@m_pagetoredirect",SqlDbType.NVarChar),                     
                     new SqlParameter("@m_parentmenuID",SqlDbType.NVarChar),
                     new SqlParameter("@m_sortorder",SqlDbType.Int),                     
                     new SqlParameter("@m_IconUrl",SqlDbType.NVarChar),
                     new SqlParameter("@m_deleteflag",SqlDbType.Bit),
                     new SqlParameter("@m_createdate",SqlDbType.DateTime),
                     new SqlParameter("@m_id",SqlDbType.Int)
                 };
                parms[0].Value = menutitle;
                parms[1].Value = redirectURL;
                parms[2].Value = parentID;
                parms[3].Value = ordernum;
                parms[4].Value =micon ;
                parms[5].Value = false; 
                parms[6].Value = DateTime.Now;
                parms[7].Value = id;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                #endregion
            }
            else
            {
                #region Add New                
                StringBuilder str = new StringBuilder();

                str.Append("INSERT INTO WebsiteMenu (m_title,m_pagetoredirect,m_parentmenuID,m_sortorder,m_IconUrl,m_deleteflag,m_createdate)");
                str.Append("VALUES (@m_title,@m_pagetoredirect,@m_parentmenuID,@m_sortorder,@m_IconUrl,@m_deleteflag,@m_createdate)");
                SqlParameter[] parms =
                {
                     new SqlParameter("@m_title",SqlDbType.NVarChar),
                     new SqlParameter("@m_pagetoredirect",SqlDbType.NVarChar),
                     new SqlParameter("@m_parentmenuID",SqlDbType.NVarChar),
                     new SqlParameter("@m_sortorder",SqlDbType.Int),
                     new SqlParameter("@m_IconUrl",SqlDbType.NVarChar),
                     new SqlParameter("@m_deleteflag",SqlDbType.Bit),
                     new SqlParameter("@m_createdate",SqlDbType.DateTime)
                 };
                parms[0].Value = menutitle;
                parms[1].Value = redirectURL;
                parms[2].Value = parentID;
                parms[3].Value = ordernum;
                parms[4].Value = micon;
                parms[5].Value = false;
                parms[6].Value = DateTime.Now;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                #endregion
            }
        }
        catch (Exception ex) { throw ex; }
        return res;
    }

    public void updatePageTypeUsage(string pagetype)
    {
        #region Update Page Type            
        if (pagetype != "NewsList" && pagetype != "NewsLetters" && pagetype != "News" && pagetype != "Sponsor" && pagetype != "MoreInformation" && pagetype != "Floorplan")
        {
            StringBuilder str = new StringBuilder();
            str.Append("update PageType set PT_HasUsed=0 where PT_Name=@PT_Name");

            SqlParameter[] parms =
            {
                     new SqlParameter("@PT_Name",SqlDbType.NVarChar)
                 };
            parms[0].Value = pagetype;
            var res = fn.ExecuteNonQuery(str.ToString(), parms);
        }
        #endregion
    }

    public int DeleteWebsiteMenu(int ID)
    {
        int rtn = 0;
        var sql = "Delete from WebsiteMenu where m_id=" + ID;
        string res = fn.ExecuteSQL2(sql);
        if(!string.IsNullOrEmpty(res) && res != "0")
        {
            rtn = 1;
        }
        return rtn;
    }

    public int CheckUsage(string iD)
    {
        int rtn = 0;  
        try
        {
            DataTable table = new DataTable();
            string sql = "select * from WebsiteMenu where m_parentMenuID="+iD;
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
            if(table.Rows.Count>0)
            {
                rtn = 1;
            }            
        }
        catch (Exception ex) { throw ex; }

        return rtn;
    }

    public int insertMenu(string id,string nameen, string namecn, string ordernum, string parentID, string pagetype, string menutype, string micon, string htmlstr, string htmlstr2, string permission,string weblink)
    {
        int res = 0;
        try
        {         
            if (!string.IsNullOrEmpty(id) && id != "0")
            {
                #region Update
                StringBuilder str = new StringBuilder();
                str.Append("Update tb_Menu set m_TitleEn=@m_TitleEn,m_TitleCn=@m_TitleCn,m_OrderID=@m_OrderID,m_IconUrl=@m_IconUrl,m_PageType=@m_PageType,m_parentMenuID=@m_parentMenuID,m_menuType=@m_menuType,m_htmlString=@m_htmlString,m_htmlString2=@m_htmlString2,Status=@Status,updatedDate=@updatedDate,m_permissioncontrol=@m_permissioncontrol,m_weblink=@m_weblink where m_id=@m_id");

                SqlParameter[] parms =
                {
                     new SqlParameter("@m_TitleEn",SqlDbType.NVarChar),
                     new SqlParameter("@m_TitleCn",SqlDbType.NVarChar),
                     new SqlParameter("@m_OrderID",SqlDbType.Int),
                     new SqlParameter("@m_IconUrl",SqlDbType.NVarChar),
                     new SqlParameter("@m_PageType",SqlDbType.NVarChar),
                     new SqlParameter("@m_parentMenuID",SqlDbType.NVarChar),
                     new SqlParameter("@m_menuType",SqlDbType.NVarChar),
                     new SqlParameter("@m_htmlString",SqlDbType.NVarChar),
                     new SqlParameter("@m_htmlString2",SqlDbType.NVarChar),
                     new SqlParameter("@Status",SqlDbType.NVarChar),
                     new SqlParameter("@updatedDate",SqlDbType.VarChar),
                     new SqlParameter("@m_id",SqlDbType.NVarChar),
                     new SqlParameter("@m_permissioncontrol",SqlDbType.NVarChar),
                     new SqlParameter("@m_weblink",SqlDbType.NVarChar)

                 };
                parms[0].Value = nameen;
                parms[1].Value = namecn;
                parms[2].Value = ordernum;
                parms[3].Value = micon;
                parms[4].Value = pagetype;
                parms[5].Value = parentID;
                parms[6].Value = menutype;
                parms[7].Value = htmlstr;
                parms[8].Value = htmlstr2;
                parms[9].Value = "Updated";
                parms[10].Value = dalf.ConvertToTimestamp(DateTime.Now);
                parms[11].Value = id;
                parms[12].Value = permission;
                parms[13].Value = weblink;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                #endregion
            }
            else
            {
                #region Add New                
                StringBuilder str = new StringBuilder();
                string mkey = string.Empty; string mid = string.Empty;
                mkey = gk.getKey("MU");
                mid = "MU" + mkey;
                str.Append("INSERT INTO tb_Menu (m_TitleEn,m_TitleCn,m_OrderID,m_IconUrl,m_PageType,m_parentMenuID,m_menuType,m_htmlString,m_htmlString2,Status,updatedDate,m_permissioncontrol,m_id,m_weblink)");
                str.Append("VALUES (@m_TitleEn,@m_TitleCn,@m_OrderID,@m_IconUrl,@m_PageType,@m_parentMenuID,@m_menuType,@m_htmlString,@m_htmlString2,@Status,@updatedDate,@m_permissioncontrol,@m_id,@m_weblink)");
                SqlParameter[] parms =
                {
                     new SqlParameter("@m_TitleEn",SqlDbType.NVarChar),
                     new SqlParameter("@m_TitleCn",SqlDbType.NVarChar),
                     new SqlParameter("@m_OrderID",SqlDbType.Int),
                     new SqlParameter("@m_IconUrl",SqlDbType.NVarChar),
                     new SqlParameter("@m_PageType",SqlDbType.NVarChar),
                     new SqlParameter("@m_parentMenuID",SqlDbType.NVarChar),
                     new SqlParameter("@m_menuType",SqlDbType.NVarChar),
                     new SqlParameter("@m_htmlString",SqlDbType.NVarChar),
                     new SqlParameter("@m_htmlString2",SqlDbType.NVarChar),
                     new SqlParameter("@Status",SqlDbType.NVarChar),
                     new SqlParameter("@updatedDate",SqlDbType.VarChar),
                     new SqlParameter("@m_permissioncontrol",SqlDbType.NVarChar),
                     new SqlParameter("@m_id",SqlDbType.NVarChar),
                     new SqlParameter("@m_weblink",SqlDbType.NVarChar)
                 };
                parms[0].Value = nameen;
                parms[1].Value = namecn;
                parms[2].Value = ordernum;
                parms[3].Value = micon;
                parms[4].Value = pagetype;
                parms[5].Value = parentID;
                parms[6].Value = menutype;
                parms[7].Value = htmlstr;
                parms[8].Value = htmlstr2;
                parms[9].Value = "Add";
                parms[10].Value = dalf.ConvertToTimestamp(DateTime.Now);
                parms[11].Value = permission;
                parms[12].Value = mid;
                parms[13].Value = weblink;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
                gk.SaveKey("MU", int.Parse(mkey));
                #endregion
            }

            #region Update Page Type            
            if (pagetype != "NewsList" && pagetype != "NewsLetters" && pagetype != "News" && pagetype != "Sponsor" && pagetype != "MoreInformation" && pagetype != "Floorplan" && pagetype != "Blank" && pagetype != "Weblink")
            {
                StringBuilder str = new StringBuilder();
                str.Append("update PageType set PT_HasUsed=1 where PT_Name=@PT_Name");

                SqlParameter[] parms =
                {
                     new SqlParameter("@PT_Name",SqlDbType.NVarChar)
                 };
                parms[0].Value = pagetype;
                res = fn.ExecuteNonQuery(str.ToString(), parms);
            }
            
            #endregion
        }
        catch (Exception ex) { throw ex; }
        return res;
    }

    public DataTable selectChildMenuByPID(string pmenuID)
    {
        DataTable dt = new DataTable();
        try
        {
            dt = fn.GetDatasetByCommand("Select * From tb_Menu Where m_parentMenuID='" + pmenuID + "' And Status<>'deleted'", "ds").Tables[0];
        }
        catch(Exception ex)
        { }
        return dt;
    }
    //public List<tb_Menu> selectChildMenuByPID(string pmenuID)
    //{
    //    var res = (from aa in db.tb_Menu
    //               where aa.m_parentMenuID == pmenuID
    //               orderby aa.m_OrderID
    //               select aa).ToList();
    //    return res;
    //}

    public DataTable selectMenuByPageType(string condition)
    {
        string sql = "select * from tb_Menu where m_PageType='" + condition + "' and Status <> 'Deleted'";
        var aa = fn.GetDatasetByCommand(sql, "dt").Tables[0];
        return aa;
    }

    public string getMenuOrderNumSub(string parentID)
    {
        int num = 0;
        try
        {
            var aa = fn.GetDataByCommand("select m_Id from tb_menu where m_parentMenuID='"+ parentID + "'", "m_Id");
            if (aa != "0")
            {
                num = int.Parse(fn.GetDataByCommand("select max(m_OrderID) as max from tb_Menu where m_parentMenuID='"+ parentID + "'", "max"));
                num = num + 1;
            }
            else
            {
                num = 1;
            }

        }
        catch (Exception ex) { throw ex; }
        return num.ToString();
    }
    public string getMenuOrderNum()
    {
        int num = 0;
        try
        {
            var aa = fn.GetDataByCommand("select m_Id from tb_menu", "m_Id");
            if(aa != "0")
            {
                num = int.Parse(fn.GetDataByCommand("select max(m_OrderID) as max from tb_Menu where m_parentMenuID=''", "max"));
                num = num + 1;
            }
            else
            {
                num = 1;
            }
            
        }
        catch(Exception ex) { throw ex; }
        return num.ToString();
    }
    public string getWebsiteMenuOrderNumSub(string parentID)
    {
        int num = 0;
        try
        {
            var aa = fn.GetDataByCommand("select m_Id from WebsiteMenu where m_parentMenuID='" + parentID + "'", "m_Id");
            if (aa != "0")
            {
                num = int.Parse(fn.GetDataByCommand("select max(m_sortorder) as max from WebsiteMenu where m_parentMenuID='" + parentID + "'", "max"));
                num = num + 1;
            }
            else
            {
                num = 1;
            }

        }
        catch (Exception ex) { throw ex; }
        return num.ToString();
    }
    public string getWebsiteMenuOrderNum()
    {
        int num = 0;
        try
        {
            var aa = fn.GetDataByCommand("select m_Id from WebsiteMenu", "m_Id");
            if (aa != "0")
            {
                num = int.Parse(fn.GetDataByCommand("select max(m_sortorder) as max from WebsiteMenu where m_parentMenuID=0", "max"));
                num = num + 1;
            }
            else
            {
                num = 1;
            }

        }
        catch (Exception ex) { throw ex; }
        return num.ToString();
    }
    public DataTable getPageType()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "SELECT * from PageType where PT_HasUsed <> 1 and Status <> 'delete' order by ChangedID asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }
    public DataTable getParentMenu()
    {        
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from tb_Menu where m_parentMenuID = '' and status <>'Deleted' order by m_TitleEn asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }
    public DataTable getWebsiteParentMenu()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from WebsiteMenu where m_parentmenuID = 0 and m_deleteflag=0 order by m_title asc";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }

        return table;
    }
    public DataTable getSysConfigData()
    {
        DataTable table = new DataTable();
        try
        {
            string sql = "select * from tb_Configuration";
            table = fn.GetDatasetByCommand(sql, "sdt").Tables[0];
        }
        catch (Exception ex) { throw ex; }
        
        return table;
    }
}