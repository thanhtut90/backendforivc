﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using OnBarcode.Barcode;
using BusinessRefinery.Barcode;
using System.IO;
using System.Drawing;
using ThoughtWorks;
using ThoughtWorks.QRCode;
using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using System.Text;

/// <summary>
/// Summary description for GenQRCode
/// </summary>
public class GenQRCode
{
    //public string genenerateQRCode(string fname, string lname, string company, string country, string regno, string jobtitle, string imgsrc)
    //{
    //    string filename = null;
    //    try
    //    {
    //        string concatStr = fname + "^" + lname + "^" + company + "^" + country + "^" + regno + "^" + jobtitle;
    //        string[] strings = { fname, lname, company, country, regno, jobtitle };
    //        filename = createQRGen(concatStr, imgsrc,regno);
    //    }
    //    catch (Exception ex) { }
    //    return filename;
    //}

    //public string createQRGen(string concatStr, string imgsrc,string regno)
    //{
    //    string filename = null;
    //    try
    //    {
    //        // Create a ASP.NET QR Code object
    //        QRCode qrCode = new QRCode();

    //        // Customize QR Code barcode data
    //        qrCode.Data = concatStr;
    //        // Customize QR Code data mode
    //        qrCode.DataMode = QRCodeDataMode.Auto;

    //        // Customize QR Code barcode size
    //        qrCode.X = 3;
    //        qrCode.LeftMargin = 8;
    //        qrCode.RightMargin = 8;
    //        qrCode.TopMargin = 8;
    //        qrCode.BottomMargin = 8;

    //        // Encode created QR Code to png image file and save it to C:\
    //        qrCode.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
    //        string fnamelname = regno;
    //        filename = HttpContext.Current.Server.MapPath(imgsrc + fnamelname + ".png");
    //        qrCode.drawBarcode(filename);
    //    }
    //    catch(Exception ex) { }
    //    return filename;

    //}

    //public string genQR(string fname, string lname, string company, string country, string regno, string jobtitle, string imgsrc)
    //{
    //    string filename = null;
    //    try
    //    {
    //        string concatStr = fname + "^" + lname + "^" + company + "^" + country + "^" + regno + "^" + jobtitle;
    //        string file_path = HttpContext.Current.Server.MapPath(imgsrc + regno + ".Png");

    //        if (File.Exists(file_path))
    //        {
    //            File.Delete(file_path);
    //        }

    //        QRCode barcode = new QRCode();
    //        barcode.Code = concatStr;
    //        barcode.Resolution = 104;
    //        barcode.Rotate = Rotate.Rotate180;
    //        barcode.Format = System.Drawing.Imaging.ImageFormat.Png;
    //        barcode.drawBarcode2ImageFile(file_path);
    //        filename = imgsrc + regno + ".Png";
    //    }
    //    catch (Exception ex) { }
    //    return filename;
    //}

    public string genQR(string name, string company, string jobtitle, string country, string regno, string imgsrc)
    {
        string filename = null;
        Bitmap bt;
        string enCodeString = name + "^" + company + "^" + country + "^" + jobtitle + "^" + regno;
        try
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeScale = 2;
            qrCodeEncoder.QRCodeVersion = 0;//7;
            bt = qrCodeEncoder.Encode(enCodeString, Encoding.UTF8);

            string file_path = HttpContext.Current.Server.MapPath(imgsrc + regno + ".Png");
            //filename = filename.Replace(" ", "");

            //filename = filename.Replace(":", "");

            //filename = filename.Replace("-", "");

            //filename = filename.Replace(".", "");

            //bt.Save(HttpContext.Current.Server.MapPath("~/QRcode/") + filename + ".jpg");
            bt.Save(file_path);

            filename = imgsrc + regno + ".Png";
        }
        catch(Exception ex)
        {
        }

        return filename;
    }

    public string genQROnlyOneParam(string param, string cid, string imgsrc)
    {
        string filename = null;
        Bitmap bt;
        string enCodeString = param;
        try
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeScale = 2;
            qrCodeEncoder.QRCodeVersion = 0;//7;
            bt = qrCodeEncoder.Encode(enCodeString, Encoding.UTF8);

            string file_path = HttpContext.Current.Server.MapPath(imgsrc + cid + ".Png");
            //filename = filename.Replace(" ", "");

            //filename = filename.Replace(":", "");

            //filename = filename.Replace("-", "");

            //filename = filename.Replace(".", "");

            //bt.Save(HttpContext.Current.Server.MapPath("~/QRcode/") + filename + ".jpg");
            bt.Save(file_path);

            filename = imgsrc + cid + ".Png";
        }
        catch (Exception ex)
        {
        }

        return filename;
    }

    public string genQR_encrypt(string name, string emailaddress, string company, string country, string jobtitle, string mob, string regno, string imgsrc)
    {
        string filename = null;
        Bitmap bt;

        name = string.IsNullOrEmpty(name) ? "." : name;
        emailaddress = string.IsNullOrEmpty(emailaddress) ? "." : emailaddress;
        company = string.IsNullOrEmpty(company) ? "." : company;
        country = string.IsNullOrEmpty(country) ? "." : country;
        jobtitle = string.IsNullOrEmpty(jobtitle) ? "." : jobtitle;
        mob = string.IsNullOrEmpty(mob) ? "." : mob;

        //vYEOH KEAT  CHUAN ^ Siewkhim@edb.gov.sg ^ KINGSMEN EXHIBITS PTE LTD ^ SINGAPOREh ^ MANAGING DIRECTOR ^ 68326832 ^ B11010
        string enCodeString = "v" + name + "^" + emailaddress + "^" + company + "^" + country + "^" + jobtitle + "^" + mob + "^" + regno;//"v" means "visitor"
        try
        {
            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            qrCodeEncoder.QRCodeScale = 2;
            qrCodeEncoder.QRCodeVersion = 0;//7;
            enCodeString = GetEncodeQRcode(enCodeString);
            bt = qrCodeEncoder.Encode(enCodeString, Encoding.UTF8);

            string file_path = HttpContext.Current.Server.MapPath(imgsrc + regno + ".Png");
            bt.Save(file_path);

            filename = imgsrc + regno + ".Png";
        }
        catch (Exception ex)
        {
        }

        return filename;
    }

    private string GetEncodeQRcode(string value)
    {
        int KeyValue = 7;
        int vLength = value.Length;
        string eCryptedValue = "";
        int tmpValue = 0;
        int asciValue = 0;
        foreach (char charValue in value)
        {

            asciValue = (int)charValue;
            int asciPlusKey = asciValue + KeyValue;
            char asciPlsuKeyChar = (char)asciPlusKey;
            if (asciPlusKey > 126)
            {
                tmpValue = (asciPlusKey - 127 + 32);
                char cValue = (char)tmpValue;
                if (Char.IsWhiteSpace(cValue))
                    eCryptedValue += ' ';
                else if (cValue == '!')
                    eCryptedValue += "!";
                else if (cValue == '"')
                    eCryptedValue += "\"";
                else if (cValue == '#')
                    eCryptedValue += "#";
                else if (cValue == '$')
                    eCryptedValue += "$";
                else if (cValue == '%')
                    eCryptedValue += "%";
                else
                    eCryptedValue += tmpValue;

            }
            else if (asciPlsuKeyChar == '"')
            {
                eCryptedValue += "\"";
            }
            else
                eCryptedValue += asciPlsuKeyChar;


        }

        return eCryptedValue;
    }

}