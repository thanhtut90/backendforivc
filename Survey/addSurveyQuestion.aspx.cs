﻿using System;
using System.Web.UI;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using App.DAL;
using App.Common;


public partial class organizer_AddQACate : System.Web.UI.Page
{
    protected string UType;
    protected static string tablename = "SurveyCate";
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data daldata = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UType = Request.QueryString["UserType"].ToString();
            if (Request.QueryString["surveycateid"] != null)
            {
                int surveycateid = int.Parse(Request.QueryString["surveycateid"].ToString());
                
                var eobj = dalf.selectSurveyCateByID(surveycateid);
                if (eobj != null)
                {
                    SurveyTitle.Text = eobj.SurveyCateTitle;
                    SurveyCateType.SelectedValue = eobj.SurveyCateType;
                }
            }
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["UserType"] != null && Request.QueryString["surveycateid"] != null)
        {
            #region Update
            int surveycateid = int.Parse(Request.QueryString["surveycateid"].ToString());
            string UserType = Request.QueryString["UserType"].ToString();
            var dtv = dalf.CheckSurveyCateByTitle(FilterSpecial(SurveyTitle.Text.Trim()));
            if (dtv <= 0)
            {
                var lstsurveycate = dalf.selectSurveyCateByUserType(UserType);
                int sort = lstsurveycate.Count + 1;
                SurveyCate obj = new SurveyCate();
                obj.SurveyCateTitle = FilterSpecial(SurveyTitle.Text.Trim());
                obj.SurveyCateType = SurveyCateType.SelectedValue.ToString();
                obj.SurveyCateenum = sort;
                obj.UserType = UserType;
                obj.SurveyCateId = surveycateid;
                int res = dalf.updateSurveyCate(obj);
                if (res > 0)
                {
                    daldata.UpdateDataVersion(tablename);
                    Response.Redirect("Surveylist.aspx?UserType=" + UserType + "");
                }
            }
            else
            {
                MessageShow("Duplicate Title", "warning");
            }
            #endregion
        }
        if (Request.QueryString["UserType"] != null)
        {
            string UserType = Request.QueryString["UserType"].ToString();
            var dtv = dalf.CheckSurveyCateByTitle(FilterSpecial(SurveyTitle.Text.Trim()));
            if (dtv <= 0)
            {
                var lstsurveycate = dalf.selectSurveyCateByUserType(UserType);
                int sort = lstsurveycate.Count + 1;
                SurveyCate obj = new SurveyCate();
                obj.SurveyCateTitle = FilterSpecial(SurveyTitle.Text.Trim());
                obj.SurveyCateType = SurveyCateType.SelectedValue.ToString();
                obj.SurveyCateenum = sort;
                obj.UserType = UserType;

                int res = dalf.saveSurveyCate(obj);
                if (res > 0)
                {
                    daldata.UpdateDataVersion(tablename);
                    Response.Redirect("Surveylist.aspx?UserType=" + UserType + "");
                }
            }
            else
            {
                MessageShow("Duplicate Title", "warning");
            }
        }
        
    }
    public string FilterSpecial(string str)
    {
        if (str == "")
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");

            return str;
        }
    }
    public void MessageShow(string msgtxt, string msgclass)
    {
        noti.Visible = true;
        msg.Text = msgtxt;
        if (msgclass == "Success")
            noti.Attributes.Add("class", "alert alert-success alert-dismissable");
        else if (msgclass == "Info")
            noti.Attributes.Add("class", "alert alert-info alert-dismissable");
        else if(msgclass =="warning")
            noti.Attributes.Add("class", "alert alert-warning alert-dismissable");
        else
            noti.Attributes.Add("class", "alert alert-danger alert-dismissable");
    }
}