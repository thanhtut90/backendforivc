﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="SurveyList.aspx.cs" Inherits="organizer_VQACate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Default">
        <TargetControls>
            <telerik:TargetControl ControlID="grdCln" />    
        </TargetControls>
    </telerik:RadSkinManager>
     <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title"><%=username %></h3>            
            <h3 class="box-title pull-right"><a href="addSurveyQuestion.aspx?UserType=<%=UType%>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New</a></h3>              
        </div>
        <div class="box-body">
            <div class="form-group">                      
                <div class="col-sm-12">
                <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                </div>   
                </div>
            </div>             
            <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                        <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="true" PageSize="20">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="SurveyCateId" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%" >
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                    <a href="addSurveyQuestion.aspx?surveycateid=<%#Eval("SurveyCateId") %>&UserType=<%=UType%>"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                               <asp:LinkButton ID="lnkdel" runat="server" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("SurveyCateId") %>' onclick="linkdel_Click"><i class="fa fa-trash"></i></asp:LinkButton>
                                    </ItemTemplate>                     
                                </telerik:GridTemplateColumn>                                                                               
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Question">
                                    <ItemTemplate>
                                        <%#Eval("SurveyCateTitle")%>        
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>      
                                <telerik:GridTemplateColumn  FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Type">
                                    <ItemTemplate>
                                        <%#Cateshow(Eval("SurveyCateType").ToString())%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>   
                                <telerik:GridTemplateColumn  FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Sort Order">
                                    <ItemTemplate>
                                        <%#Eval("SurveyCateenum")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>   
                                <telerik:GridTemplateColumn  FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Answer">
                                    <ItemTemplate>
                                         <%#answerlist(Eval("SurveyCateId").ToString())%>
                                        <a href="addSurveyAsnwer.aspx?SurveyCateId=<%#Eval("SurveyCateId") %>&UserType=<%=UType%>">Add Answer</a>
                                        
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn> 
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Up">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkup" runat="server" OnClientClick='return confirm("Up?")'  CommandName="up" CommandArgument='<%#Eval("SurveyCateId") %>' onclick="linkup_Click">
                                            <i class="fa fa-arrow-circle-up"></i>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Down">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkdown" runat="server" OnClientClick='return confirm("Down?")'  CommandName="down" CommandArgument='<%#Eval("SurveyCateId") %>' onclick="linkdown_Click">
                                            <i class="fa fa-arrow-circle-down"></i>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>                                                                                
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    </div>              
                </div>
            </div>
        </div>
     </div>
</asp:Content>


