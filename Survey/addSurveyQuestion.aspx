﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="addSurveyQuestion.aspx.cs" Inherits="organizer_AddQACate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>    
   <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Add Survey Question</h3>      
        </div>   
       <div class="form-horizontal">
            <div class="box-body">
                <div class="form-group">                      
                    <div class="col-sm-2">
                        <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                        </div>   
                    </div>
                </div> 
                      
                <div class="form-group">
                    <label for="inputEmail1" class="col-sm-2 control-label">Survey Question</label>
                    <div class="col-sm-6">
                    <asp:TextBox ID="SurveyTitle" placeholder ="Please insert Survey Question" class="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail2" class="col-sm-2 control-label">Survey Type</label>
                    <div class="col-sm-6">
                    <asp:DropDownList ID="SurveyCateType" class="form-control" runat="server">
                        <asp:ListItem Value="1">Single choice</asp:ListItem>
                        <asp:ListItem Value="2">Multiple choice</asp:ListItem>
                        <asp:ListItem Value="3">Rating scale</asp:ListItem>
                        <asp:ListItem Value="4">Comment box</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div class="box-footer">
                    <asp:Button ID="Button1" class="btn btn-info" runat="server" Text="Proceed" onclick="Button1_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               
                    <a href="Surveylist.aspx?UserType=<%=UType%>" class="btn btn-default">Back</a>
                </div>           
            </div>
       </div>

     </div>
</asp:Content>

