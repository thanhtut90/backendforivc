﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using App.DAL;
using App.Common;

public partial class organizer_SurveyList : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    IsabellaFormEntities db = new IsabellaFormEntities();
    protected string SurveyCateID;
    protected string UType;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string UserType = Request.QueryString["UserType"].ToString();
            string SurveyCateId = Request.QueryString["SurveyCateId"].ToString();
            UType = UserType;
            lblQuest.Text = dalf.selectSurveyCateByID(int.Parse(SurveyCateId)).SurveyCateTitle.ToString();
        }
    }
    protected void linkdel_Click(object sender, EventArgs e)
    {
        string UserType = Request.QueryString["UserType"].ToString();       
        int SurveyCateId = int.Parse(Request.QueryString["SurveyCateId"].ToString());

        LinkButton del = sender as LinkButton;
        if (del.CommandName == "Delete")
        {
            string SurveyId = del.CommandArgument.ToString();
            var surveylst = dalf.selectSurveyListByID(Convert.ToInt32(SurveyId));
            var SurveyNum = Convert.ToInt32(surveylst.SurveyNum);

            db.SurveyLists.Remove(surveylst);
            int cmdcount = db.SaveChanges();
            var tbl = dalf.selectSurveyListByGreaterNumUserType(SurveyNum, SurveyCateId);

            foreach(var t in tbl)
            {
                int _SurveyId = Convert.ToInt32(t.SurveyId);
                int _SurveyNum = Convert.ToInt32(t.SurveyNum) - 1;

                SurveyList obj = new SurveyList();
                obj.SurveyNum = _SurveyNum;
                obj.SurveyId = _SurveyId;
                dalf.UpdateSurveyList(obj);                
            }

            if (cmdcount >= 1)
            {
                Response.Redirect("SurveyList.aspx?SurveyCateId=" + SurveyCateId + "&UserType="+UserType+"");
            }
        }
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string UserType = Request.QueryString["UserType"].ToString();      
        int SurveyCateId = int.Parse(Request.QueryString["SurveyCateId"].ToString());

        var lstSurveylist = dalf.selectSurveyListByCateID(SurveyCateId);
        int sort = lstSurveylist.Count + 1;

        string SurveyKeyNo = dalf.SelectSurveyQNo();
        string Otherstxt = "";
        if (chkcmt.Checked == true)
        {
            Otherstxt = "#";
        }
        else
        {
            Otherstxt = "0";
        }
        

        SurveyList obj = new SurveyList();
        obj.SurveyTitle = FilterSpecial(SurveyTitle.Text.Trim());
        obj.SurveyCateId = SurveyCateId;
        obj.SurveyNum = sort;
        obj.SurveyKeyNo = SurveyKeyNo;
        obj.SurveyLink = Otherstxt;

        dalf.SaveSurveyList(obj);
        Response.Redirect("addSurveyAsnwer.aspx?SurveyCateId=" + SurveyCateId + "&UserType=" + UserType + "");
        
    }    
    public string FilterSpecial(string str)
    {
        if (str == "") //如果字符串为空，直接返回。
        {
            return str;
        }
        else
        {
            str = str.Replace("'", "''");

            return str;
        }
    }
    protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            int SurveyCateId = int.Parse(Request.QueryString["SurveyCateId"].ToString());
            List<SurveyList> lstc = dalf.selectSurveyListByCateID(SurveyCateId);
            grdFML.DataSource = lstc;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    protected void grdFML_NeedDSource()
    {
        try
        {
            int SurveyCateId = int.Parse(Request.QueryString["SurveyCateId"].ToString());
            List<SurveyList> lstc = dalf.selectSurveyListByCateID(SurveyCateId);
            grdFML.DataSource = lstc;
            grdFML.DataBind();

        }
        catch (Exception ex) { }
    }
}