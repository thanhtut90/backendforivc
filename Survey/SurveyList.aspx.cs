﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using App.DAL;
using App.Common;
using System.Data.Entity;

public partial class organizer_VQACate : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    IsabellaFormEntities db = new IsabellaFormEntities();
    DAL_Data daldata = new DAL_Data();
    protected string UType;
    protected string username;
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            string UserType = Request.QueryString["UserType"].ToString();
            UType = UserType;
            username = UserType + " Survey Questions";
            if (UserType != "Exhibitor" && UserType != "Visitor" && UserType != "Delegate" && UserType != "Guest")
            {
                string sessionID= Request.QueryString["UserType"].ToString();
                string sessionTitle = daldata.getConferenceByID(sessionID).Rows[0]["c_title"].ToString();
                username = "Conference Survey Questions : " + sessionTitle;
            }
        }
    }
    
    protected void linkdel_Click(object sender, EventArgs e)
    {
        string UserType = Request.QueryString["UserType"].ToString();
        LinkButton del = sender as LinkButton;

        if (del.CommandName == "Delete")
        {
            string SurveyCateId = del.CommandArgument.ToString();

            var SurveyCateOBJ = dalf.selectSurveyCateByID(int.Parse(SurveyCateId));
            var SurveyList = dalf.selectSurveyListByCateID(int.Parse(SurveyCateId));
            var SurveyCateNum = SurveyCateOBJ.SurveyCateenum;
            if (SurveyList.Count > 0)
            {
                foreach (var i in SurveyList)
                {
                    var entry = db.Entry(i);
                    if (entry.State == EntityState.Detached)
                        db.SurveyLists.Attach(i);
                    db.SurveyLists.Remove(i);                                
                    int success = db.SaveChanges();
                }
            }
                       
            var entry1 = db.Entry(SurveyCateOBJ);
            if (entry1.State == EntityState.Detached)
                db.SurveyCates.Attach(SurveyCateOBJ);
            db.SurveyCates.Remove(SurveyCateOBJ);
            int success1 = db.SaveChanges();

            var surveycatelst = dalf.selectSurveyCateByNumUserType(SurveyCateNum, UserType);

            foreach (var s in surveycatelst)
            {
                int _SurveyCateId = Convert.ToInt32(s.SurveyCateId);
                int _SurveyCateenum = Convert.ToInt32(s.SurveyCateenum) - 1;
                SurveyCate obj = new SurveyCate();
                obj.SurveyCateId = _SurveyCateId;
                obj.SurveyCateenum = _SurveyCateenum;

                int i = dalf.updateSurveyCateNum(obj, false, false);
                if (i > 0)
                {
                    Response.Redirect("Surveylist.aspx?UserType=" + UserType + "");
                }
            }
        }
    }
    public string answerlist(string SurveyCateId)
    {
        StringBuilder tb = new StringBuilder();
        var lst = dalf.selectSurveyListByCateID(int.Parse(SurveyCateId));        
        if (lst.Count > 0)
        {
            tb.Append("<table width='450' border='0' cellspacing='2' cellpadding='2'>");
            foreach (var l in lst)
            {
                tb.Append("<tr>");
                tb.Append("<td vlign='top' sytle='padding-left:5px'>" + l.SurveyTitle + "</td>");
                tb.Append("</tr>");
            }
            tb.Append("</table>");
        }
        return tb.ToString();
    }
    protected void linkup_Click(object sender, EventArgs e)
    {
        string UserType = Request.QueryString["UserType"].ToString();
        LinkButton del = sender as LinkButton;
        if (del.CommandName == "up")
        {
            string SurveyCateId = del.CommandArgument.ToString();
            var dt = dalf.selectSurveyCateByID(int.Parse(SurveyCateId));
            if (dt!=null)
            {
                int SurveyCateenum = int.Parse(dt.SurveyCateenum.ToString());
                if (SurveyCateenum > 1)
                {
                    int quesnum = SurveyCateenum - 1;
                   
                    var dt2 = dalf.selectSurveyCateByEqualNumUserType(quesnum, UserType);
                    if (dt2.Count > 0)
                    {
                        foreach(var a in dt2)
                        {
                            int _SurveyCateId = int.Parse(a.SurveyCateId.ToString());
                            SurveyCate obj = new SurveyCate();
                            obj.SurveyCateId = _SurveyCateId;                            

                            int i = dalf.updateSurveyCateNum(obj,true,false);
                            if (i > 0)
                            {                               
                                SurveyCate objc = new SurveyCate();                                
                                objc.SurveyCateId = int.Parse(SurveyCateId);
                                int res = dalf.updateSurveyCateNum(objc, false, true);
                               
                                if (res >0)
                                {
                                    Response.Redirect("Surveylist.aspx?UserType=" + UserType + "");
                                }
                            }
                           
                        }
                        
                    }
                }
            }
        }
    }
    protected void linkdown_Click(object sender, EventArgs e)
    {
        string UserType = Request.QueryString["UserType"].ToString();
        LinkButton del = sender as LinkButton;
        if (del.CommandName == "down")
        {
            string SurveyCateId = del.CommandArgument.ToString();
            var dt = dalf.selectSurveyCateByID(int.Parse(SurveyCateId));
            if (dt != null)
            {
                int SurveyCateenum = int.Parse(dt.SurveyCateenum.ToString());
                int quesnum = SurveyCateenum + 1;
                var dt2 = dalf.selectSurveyCateByEqualNumUserType(quesnum, UserType);
                if (dt2.Count > 0)
                {
                    foreach(var d in dt2)
                    {
                        int Qno = int.Parse(d.SurveyCateId.ToString());
                        SurveyCate obj = new SurveyCate();
                        obj.SurveyCateId = Qno;
                        int i = dalf.updateSurveyCateNum(obj, false, true);
                        if (i >0)
                        {
                            SurveyCate objc = new SurveyCate();
                            objc.SurveyCateId = int.Parse(SurveyCateId);
                            int c = dalf.updateSurveyCateNum(objc, true, false);
                            if (c >0 )
                            {
                                Response.Redirect("Surveylist.aspx?UserType=" + UserType + "");
                            }
                        }
                    }
                }
            }
        }

    }
    public string Cateshow(string VQACateType)
    { 
        string CateText="";
          switch (VQACateType)
        {
            case "1":
                CateText = "Single choice";
                break;
            case "2":
                 CateText = "Multiple choice";
                break;
           case "3":
                 CateText = "Rating scale";
                break;

            case "4":
                 CateText = "Comment box";
                break;
            default:
                 CateText = "";
                break;
        }
          return CateText;
    }
    protected void grdFML_NeedDSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            string UserType = Request.QueryString["UserType"].ToString();
            List<SurveyCate> lstc = dalf.selectSurveyCateByUserType(UserType);            
            grdFML.DataSource = lstc;
            msg.Text = "";
            noti.Visible = false;
        }
        catch (Exception ex) { }
    }
    protected void grdFML_NeedDSource()
    {
        try
        {
            string UserType = Request.QueryString["UserType"].ToString();
            List<SurveyCate> lstc = dalf.selectSurveyCateByUserType(UserType);
            grdFML.DataSource = lstc;
            grdFML.DataBind();

        }
        catch (Exception ex) { }
    }
}