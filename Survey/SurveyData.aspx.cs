﻿using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Web.Script.Serialization;
//using System.Data.OleDb;
//using System.Data.SqlClient;
using App.DAL;
using System.Data.SqlClient;

public partial class Upques : System.Web.UI.Page
{
    DAL_FrontEnd dalf = new DAL_FrontEnd();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                ArrayList eventList = new ArrayList();
                Hashtable ht = new Hashtable();
                if (Request.Form["Uid"] != null && Request.Form["QuesValue"] != null && Request.Form["QuesKey"] != null)
                {

                    string Uid = Request.Form["Uid"].ToString();
                    string QuesValue = Request.Form["QuesValue"].ToString();
                    string QuesKey = Request.Form["QuesKey"].ToString();

                    var lst = dalf.SelectSurveyQAByID(Uid);

                    if (lst != null)
                    {
                        string[] arrStr1 = QuesKey.Split('@');
                        string[] arrStr2 = QuesValue.Split('@');

                        for (int i = 0; i <= arrStr1.Length - 1; i++)
                        {
                            string sql4 = "update SurveyQA set " + arrStr1[i].ToString() + "='" + arrStr2[i].ToString().Replace("!#", "@") + "' where UserReference=" + Uid;

                            ExecuteSQL(sql4);
                            sql4 = string.Empty;

                        }
                        ht.Add("result", "1");
                        eventList.Add(ht);
                    }
                    else
                    {
                        StringBuilder str = new StringBuilder();
                        str.Append("insert into SurveyQA (UserReference) values (" + Uid + ")");
                        ExecuteSQL(str.ToString());

                        var lst1 = dalf.SelectSurveyQAByID(Uid);

                        if (lst1 != null)
                        {
                            string[] arrStr1 = QuesKey.Split('@');
                            string[] arrStr2 = QuesValue.Split('@');
                            for (int i = 0; i <= arrStr1.Length - 1; i++)
                            {
                                string sql4 = "update SurveyQA set " + arrStr1[i] + "='" + arrStr2[i].ToString().Replace("!#", "@") + "' where UserReference=" + Uid;
                                ExecuteSQL(sql4);

                            }
                            ht.Add("result", "1");
                            eventList.Add(ht);
                        }


                    }
                }
                else
                {
                    ht.Add("result", "2");
                    eventList.Add(ht);
                }


                JavaScriptSerializer ser = new JavaScriptSerializer();
                String jsonStr = ser.Serialize(eventList);
                Response.Write(jsonStr);
            }
            catch(Exception ex) { }
        }
    }
    protected static string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["CMSConnString"].ConnectionString;
    protected static SqlConnection conn = new SqlConnection();
    protected static SqlCommand comm = new SqlCommand();
    public static void ExecuteSQL(string SqlString)
    {
        try
        {
            if (conn.State != ConnectionState.Open)
            {
                conn.Close();
                conn.ConnectionString = connectionstring;
                comm.Connection = conn;
                
                conn.Open();
                comm.CommandType = CommandType.Text;
                comm.CommandText = SqlString;
                comm.ExecuteNonQuery();
               
            }
            
        }
        catch (Exception ex)
        {
            try
            {
                conn.Dispose();
                conn.Close();
                comm.Dispose();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            throw new Exception(ex.Message);
        }
        finally
        {
            conn.Dispose();
            conn.Close();
            comm.Dispose();
        }
    }
}