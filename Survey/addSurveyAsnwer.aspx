﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPages/Site.master" CodeFile="addSurveyAsnwer.aspx.cs" Inherits="organizer_SurveyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:ScriptManager ID="scrpmgr" runat="server"></asp:ScriptManager>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" ShowChooser="false" Skin="Windows7">
        <TargetControls>
            <telerik:TargetControl ControlID="grdFML" />    
        </TargetControls>
    </telerik:RadSkinManager>
    <div class="form-horizontal">
        <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Add Answer</h3>            
            <h3 class="box-title pull-right"><a href="addSurveyQuestion.aspx?UserType=<%=UType%>"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;&nbsp;Add New</a></h3>              
        </div>
        <div class="box-body">
            <div class="form-group">                      
                <div class="col-sm-12">
                    <div id="noti" runat="server" class="alert alert-info alert-dismissable" visible="false">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center><asp:Label ID="msg" runat="server"></asp:Label></center>
                    </div>   
                </div>
            </div>
           <div class="form-group">                
                <label for="inputEmail3" class="col-sm-2 control-label">Question</label>
                <div class="col-sm-6">
                <asp:Label ID="lblQuest" runat="server"></asp:Label>
                </div>
           </div>             
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Answer</label>
                <div class="col-sm-6">
                <asp:TextBox ID="SurveyTitle" placeholder ="Please insert Answer" class="form-control" runat="server"></asp:TextBox>
                </div>
           </div>
           <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Is Comment Box ?</label>
                <div class="col-sm-6">
                <asp:CheckBox ID="chkcmt" runat="server" />
                </div>
           </div>
           <div class="box-footer">
               <asp:Button ID="Button2" runat="server" Text="Submit" OnClientClick="return validnum()" onclick="Button1_Click" CssClass="btn btn-info"/>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               
               <a href="Surveylist.aspx?UserType=<%=UType%>" class="btn btn-default">Back</a>
           </div>
           <div class="col-sm-12">
                <div class="row">                
                    <div class="table-responsive">
                        <telerik:RadGrid RenderMode="Lightweight" ID="grdFML"  runat="server" EnableHeaderContextMenu="true" ShowStatusBar="true" Font-Size="X-Small"
                        PagerStyle-AlwaysVisible="true" AllowSorting="true" OnNeedDataSource="grdFML_NeedDSource" AllowPaging="true" PageSize="20">
                        <ClientSettings AllowKeyboardNavigation="true">
                            <Selecting AllowRowSelect="true"></Selecting>
                        </ClientSettings>
                        <MasterTableView AutoGenerateColumns="False" DataKeyNames="SurveyId" CommandItemDisplay="Top">
                                <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" ShowRefreshButton="false"/>
                            <Columns> 
                                <telerik:GridTemplateColumn HeaderText="Sr No"  HeaderStyle-Font-Bold="true" HeaderStyle-Width="3%">
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex+1 %>
                                            </ItemTemplate>
                                </telerik:GridTemplateColumn>  
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true"
                                    HeaderText="Actions" AutoPostBackOnFilter="true" CurrentFilterFunction="StartsWith" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                    <asp:LinkButton ID="linkdel" runat="server" OnClientClick='return confirm("Delete?")'  CommandName="Delete" CommandArgument='<%#Eval("SurveyCateId") %>' onclick="linkdel_Click"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                    </ItemTemplate>                     
                                </telerik:GridTemplateColumn>   
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Prefix"  HeaderStyle-Font-Bold="true" HeaderText="Answer">
                                    <ItemTemplate>
                                        <%# Eval("SurveyTitle")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>                                                                                                                 
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                    </div>              
                </div>
            </div> 
        </div>
     </div>
    </div>
     
</asp:Content>

