﻿using System;
using App.DAL;
using App.Common;
using System.Data;

public partial class Login : System.Web.UI.Page
{
    //DAL_Config dalconfig = new DAL_Config();
    //Administrator obj = new Administrator();
    //DAL_FrontEnd dalf = new DAL_FrontEnd();
    DAL_Data dal = new DAL_Data();
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            Session.Abandon();
        }
    }
    protected void btnSignin_Click(object sender, EventArgs e)
    {
        if(!string.IsNullOrEmpty(txtusername.Text.Trim()) && !string.IsNullOrEmpty(txtpassword.Text.Trim()))
        {            
            //obj.admin_username = txtusername.Text.Trim();
            //obj.admin_password = txtpassword.Text.Trim();
            //Administrator administratorobj = dalconfig.SelectAdminByNP(obj);
            string username= txtusername.Text.Trim();
            string password= txtpassword.Text.Trim();
            DataTable dt = dal.Login(username, password);
            if (dt.Rows.Count>0)
            {
                Session["id"] = dt.Rows[0]["admin_id"].ToString();
                Session["name"] = dt.Rows[0]["admin_name"].ToString();
                Session["role"] = dt.Rows[0]["admin_role"].ToString();
                Session["img"] = dt.Rows[0]["admin_profilepic"].ToString() == null ? "../sys_Images/noimage.png" : "../downimg/" + dt.Rows[0]["admin_profilepic"].ToString();         
                if(dt.Rows[0]["admin_role"].ToString() == "4")
                {
                    Response.Redirect("Admin/OrgDashboard.aspx");
                }
                else
                {
                    Response.Redirect("Admin/Dashboard.aspx");
                }
                
            }
            /*else
            {
                string rowidacctypename = dalf.Login(txtusername.Text.Trim(), txtpassword.Text.Trim());
                if (!string.IsNullOrEmpty(rowidacctypename))
                {
                    string[] arr = rowidacctypename.Split('^');
                    Session["id"] = arr[0];
                    Session["role"] = arr[1];
                    Session["name"] = arr[2];
                    Session["img"] = "../sys_Images/noimage.png";
                    Response.Redirect("Admin/OrgDashboard.aspx");
                }
            }*/
        }
    }
}